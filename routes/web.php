<?php

// Exception routes
Route::get('exception/index', 'ExceptionController@index');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
// Login Routes...
Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
//Route::post('login', function (Request $request) {
//    dd($request->request);
//});
Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
Route::get('cptes/print/{tipo}/{id}', 'ComprobantesController@getprint');
Route::get('cptes/printview/{tipo}/{id}', 'ComprobantesController@printview')->name('printcpte');
Route::get('view/layout2in1', function(){ return view('comprobantes.remitos.layout2in1');})->name('viewlayout');
Route::get('print/layout2in1', 'ComprobantesController@printlayout');

Route::middleware('auth')->group(function () {
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('role:1');
    Route::get('/','HomeController@index');
    Route::get('/','HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/cheques', 'ChequesController@index')->middleware('role:1');
    Route::get('/promociones', function(){return view('promociones');});
    Route::get('/inventario', 'InventarioController@index')->middleware('role:1');
    Route::get('/configuracion/geografica', 'HomeController@confgeo')->name('confgeo')->middleware('role:1,2');
    Route::get('/inventario/configuracion/deposito', 'InventarioController@confdep')->name('confdep')->middleware('role:1');
    Route::get('/masvendidos', 'HomeController@masvendidos')->middleware('role:1,2');

    /* Errors ------------------------------------------------------------------------*/
    Route::get('/error/acceso-denegado', function(){
        return view('errors.sinpermiso');
    });
    /* Errors ------------------------------------------------------------------------*/

    /* ventas ------------------------------------------------------------------------*/
    Route::get('/ventas/sabana', 'VentasController@sabana')->middleware('role:1');
    Route::get('ventas/getsabana', 'VentasController@getsabana')->middleware('role:1');
    Route::get('/ventas/{meses?}/{sucursal?}', 'VentasController@ventas')->name('ventas');
    Route::get('/ventas/getdatadashboards/{meses}', 'VentasController@getdatadashboards');
    /* ventas ------------------------------------------------------------------------*/

    /* compras ------------------------------------------------------------------------*/
    Route::get('/compras', function(){
        return view('compras.index',
            ['itemnav' => '','titulo'=>'Compras']);
    })->middleware('role:1,2');
    /* compras ------------------------------------------------------------------------*/

    /* presupuestos ------------------------------------------------------------------------*/
    Route::get('/presupuestos', function(){
        return view('oportunidades.index',
            ['itemnav' => '','titulo'=>'Oportunidades de Venta']);
    })->middleware('role:1,2,3');
    /* presupuestos ------------------------------------------------------------------------*/


    /* gastos ------------------------------------------------------------------------*/
    Route::get('/gastos', function(){
        return view('gastos.index',
            ['itemnav' => '','titulo'=>'Gastos']);
    })->middleware('role:1,2');
    /* gastos ------------------------------------------------------------------------*/


    /* Caja ------------------------------------------------------------------------*/
    Route::get('caja', 'CajasController@index')->middleware('role:1,2,3');
    Route::post('caja/store/arqueo', 'CajasController@storeArqueo')->middleware('role:1,2,3');
    Route::get('caja/delete/arqueo/{id}', 'CajasController@deleteArqueo')->middleware('role:1,2,3');
    Route::get('caja/cerrar', 'CajasController@cerrar')->middleware('role:1,2,3');
    Route::get('caja/store/aj/{signo}', 'CajasController@ajustar')->middleware('role:1,2,3');
    Route::get('caja/store/mov/{signo}', 'CajasController@movefectivo')->middleware('role:1,2,3');
    Route::get('caja/total', 'CajasController@indexall')->middleware('role:1,2,3');
    Route::get('caja/cajasanteriores', 'CajasController@indexold')->middleware('role:1');
    Route::get('caja/detalle/{id}', 'CajasController@detalle')->middleware('role:1');
    Route::get('caja/sabana', 'CajasController@sabana')->middleware('role:1');
    Route::get('caja/getsabana', 'CajasController@getsabana')->middleware('role:1');
    /* Caja ------------------------------------------------------------------------*/


    /* Inventario ------------------------------------------------------------------------*/
    Route::post('sectores/store', 'SectoresController@store')->middleware('role:1');
    Route::post('sectores/update/{id}', 'SectoresController@update')->middleware('role:1');
    Route::get('sectores/delete/{id}', 'SectoresController@delete')->middleware('role:1');

    Route::post('ubicaciones/store', 'UbicacionesController@store')->middleware('role:1');
    Route::post('ubicaciones/update/{id}', 'UbicacionesController@update')->middleware('role:1');
    Route::get('ubicaciones/delete/{id}', 'UbicacionesController@delete')->middleware('role:1');

    Route::post('posiciones/store', 'PosicionesController@store')->middleware('role:1');
    Route::post('posiciones/update/{id}', 'PosicionesController@update')->middleware('role:1');
    Route::get('posiciones/delete/{id}', 'PosicionesController@delete')->middleware('role:1');
    Route::post('posiciones/removeselected', 'PosicionesController@removeselected')->middleware('role:1');
    Route::post('posiciones/store/masivo', 'PosicionesController@storeMasivo')->middleware('role:1');

    Route::get('/inventario/movimientos/index', 'MovimientosStockController@index')->middleware('role:1,2,3,4');
    Route::get('/inventario/movimientos/create', 'MovimientosStockController@create')->middleware('role:1,2,3,4');
    Route::post('/inventario/movimientos/store', 'MovimientosStockController@store')->middleware('role:1,2,3,4');
    Route::post('/ajax/movimientos/store', 'MovimientosStockController@ajaxstore')->middleware('role:1,2,3,4');

    Route::get('/inventario/stock/index', 'ListadosStockController@index')->middleware('role:1,2,3,4');
    Route::get('/inventario/stock/detalle/{id}', 'ListadosStockController@detalle')->middleware('role:1,2,3,4');

    Route::get('/inventario/controles/index', 'ControlesStockController@index')->middleware('role:1,2,3,4');
    Route::get('/inventario/controles/create', 'ControlesStockController@create')->middleware('role:1,2,3,4');
    Route::get('/inventario/stock/generarcontrol/{id}', 'ControlesStockController@nuevocontrol')->middleware('role:1,2,3,4');
    Route::post('/inventario/controles/store', 'ControlesStockController@store')->middleware('role:1,2,3,4');
    Route::get('/inventario/controles/delete/{id}', 'ControlesStockController@delete')->middleware('role:1,2,3,4');
    Route::get('/inventario/controles/ajustar/{id}', 'ControlesStockController@ajustar')->middleware('role:1,2,3,4');
    Route::post('/inventario/controles/ajustarselected', 'ControlesStockController@ajustarselected')->middleware('role:1,2,3,4');
    Route::get('/inventario/controles/conteo/{id}', 'ControlesStockController@conteoedit')->middleware('role:1,2,3,4');
    Route::get('/inventario/controles/detalle/{id}', 'ControlesStockController@detalle')->middleware('role:1,2,3,4');
    Route::post('/inventario/controles/conteo/store/{id?}', 'ControlesStockController@conteostore')->middleware('role:1,2,3,4');
    Route::get('/inventario/controles/admin/{id}', 'ControlesStockController@controladmin')->middleware('role:1,2,3,4');
    Route::get('/inventario/controles/{id}', 'ControlesStockController@control')->middleware('role:1,2,3,4');
    Route::get('/inventario/controles/{id}/conteo/create', 'ControlesStockController@conteocreate')->middleware('role:1,2,3,4');
    /* Inventario ------------------------------------------------------------------------*/


    /* Begin CRUD Comprobantes ------------------------------------------------------------------------*/
    Route::get('cptes/create/{tipo}', 'ComprobantesController@create')->middleware('role:1,2,3');
    Route::post('cptes/store/{tipo}', 'ComprobantesController@store')->middleware('role:1,2,3');
    Route::get('cptes/detalle/{id}', 'ComprobantesController@detalle')->middleware('role:1,2,3');
    Route::get('cptes/gettitulares/{id}', 'ComprobantesController@titulares')->middleware('role:1,2,3');
    Route::get('cptes/delete/{id}', 'ComprobantesController@delete')->middleware('role:1,2');
    Route::post('cptes/removeselected', 'ComprobantesController@removeselected')->middleware('role:1,2');
    Route::get('cptes/show/{tipo}/{id}', 'ComprobantesController@show')->middleware('role:1,2,3');
    Route::get('cptes/edit/{tipo}/{id}', 'ComprobantesController@edit')->middleware('role:1,2,3');
    Route::post('cptes/update/{tipo}/{id}', 'ComprobantesController@update')->middleware('role:1,2,3');
    Route::get('cptes/desint', 'ComprobantesController@getdescint')->middleware('role:1,2,3');
    Route::get('cptes/gethistorico', 'ComprobantesController@gethistorico')->middleware('role:1,2,3');
    Route::get('cptes/{tipo}', 'ComprobantesController@index')->middleware('role:1,2,3');
    /* --------------------------------------------------------------------End Routes CRUD Comprobantes */

    /* Precios------------ ------------------------------------------------------------------------*/
    Route::get('precios/', 'PreciosController@home')->middleware('role:1,2');
    Route::get('precios/actualizacion', 'PreciosController@index')->middleware('role:1,2');
    Route::get('precios/actualizacion/create/{tipo}', 'PreciosController@create')->middleware('role:1,2');
    Route::post('precios/actualizacion/store/1', 'PreciosController@store1')->middleware('role:1,2');
    Route::post('precios/actualizacion/store/2', 'PreciosController@store2')->middleware('role:1,2');
    Route::post('precios/actualizacion/store/3', 'PreciosController@store3')->middleware('role:1,2');
    Route::get('precios/actualizacion/detalle/{id}', 'PreciosController@detalle')->middleware('role:1,2');
    Route::get('precios/actualizacion/buscarproductos', 'PreciosController@getproductos')->middleware('role:1,2');
    Route::get('precios/descint', 'DescintController@index')->middleware('role:1,2');
    Route::get('precios/descint/create', 'DescintController@create')->middleware('role:1,2');
    Route::post('precios/descint/store', 'DescintController@store')->middleware('role:1,2');
    Route::get('precios/descint/show/{id}', 'DescintController@show')->middleware('role:1,2');
    Route::get('precios/descint/edit/{id}', 'DescintController@edit')->middleware('role:1,2');
    Route::get('precios/gananciafija', 'GananciaFijaController@index')->middleware('role:1,2');
    Route::get('precios/gananciafija/create', 'GananciaFijaController@create')->middleware('role:1,2');
    Route::post('precios/gananciafija/store', 'GananciaFijaController@store')->middleware('role:1,2');
    Route::get('precios/gananciafija/show/{id}', 'GananciaFijaController@show')->middleware('role:1,2');
    Route::get('precios/gananciafija/edit/{id}', 'GananciaFijaController@edit')->middleware('role:1,2');
    Route::get('precios/ajustes', 'AjustesPrecioController@index')->middleware('role:1,2');
    Route::get('precios/ajustes/create', 'AjustesPrecioController@create')->middleware('role:1,2');
    Route::post('precios/ajustes/store', 'AjustesPrecioController@store')->middleware('role:1,2');
    Route::get('precios/ajustes/show/{id}', 'AjustesPrecioController@show')->middleware('role:1,2');
    Route::get('precios/ajustes/edit/{id}', 'AjustesPrecioController@edit')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes Precios */

    /* GetData------------ ------------------------------------------------------------------------*/
    Route::get('mediospago/getdata/{id}', 'GetdataController@getmp')->middleware('role:1,2,3,4');
    Route::get('datos/getorigen/{iditem}', 'GetdataController@getorigen')->middleware('role:1,2,3,4');
    Route::get('datos/getmarcas/{iditem?}', 'GetdataController@getmarcas')->middleware('role:1,2,3,4');
    Route::get('datos/getrubros/{iditem?}', 'GetdataController@getrubros')->middleware('role:1,2,3,4');
    Route::get('datos/getsubrubros/{iditem?}', 'GetdataController@getsubrubros')->middleware('role:1,2,3,4');
    Route::get('datos/getsectores/{iditem?}', 'GetdataController@getsectores')->middleware('role:1,2,3,4');
    Route::get('datos/getubicaciones/{iditem?}', 'GetdataController@getubicaciones')->middleware('role:1,2,3,4');
    Route::get('datos/getposiciones/{iditem?}', 'GetdataController@getposiciones')->middleware('role:1,2,3,4');
    Route::get('datos/getrubros_marca/{idmarca?}', 'GetdataController@getrubrosmarca')->middleware('role:1,2,3,4');
    Route::get('datos/getsubrubros_marca/{idmarca?}', 'GetdataController@getsubrubrosmarca')->middleware('role:1,2,3,4');
    Route::get('datos/getsectores_marca/{idmarca?}', 'GetdataController@getsectoresmarca')->middleware('role:1,2,3,4');
    Route::get('datos/getubicaciones_marca/{idmarca?}', 'GetdataController@getubicacionesmarca')->middleware('role:1,2,3,4');
    Route::get('datos/getposiciones_marca/{idmarca?}', 'GetdataController@getposicionesmarca')->middleware('role:1,2,3,4');
    Route::get('datos/getmarcas_rubro/{idrubro?}', 'GetdataController@getmarcasrubro')->middleware('role:1,2,3,4');
    Route::get('datos/getsubrubros_rubro/{idrubro?}', 'GetdataController@getsubrubrosrubro')->middleware('role:1,2,3,4');
    Route::get('data/getsubrubros_rubro/{idrubro?}', 'GetdataController@getconfsubrubrosrubro')->middleware('role:1,2,3,4');
    Route::get('datos/getsectores_rubro/{idrubro?}', 'GetdataController@getsectoresrubro')->middleware('role:1,2,3,4');
    Route::get('datos/getubicaciones_rubro/{idrubro?}', 'GetdataController@getubicacionesrubro')->middleware('role:1,2,3,4');
    Route::get('datos/getposiciones_rubro/{idrubro?}', 'GetdataController@getposicionesrubro')->middleware('role:1,2,3,4');
    Route::get('datos/getmarcas_subrubro/{idsubrubro?}', 'GetdataController@getmarcassubrubro')->middleware('role:1,2,3,4');
    Route::get('datos/getrubros_subrubro/{idsubrubro?}', 'GetdataController@getrubrossubrubro')->middleware('role:1,2,3,4');
    Route::get('datos/getsectores_subrubro/{idsubrubro?}', 'GetdataController@getsectoressubrubro')->middleware('role:1,2,3,4');
    Route::get('datos/getubicaciones_subrubro/{idsubrubro?}', 'GetdataController@getubicacionessubrubro')->middleware('role:1,2,3,4');
    Route::get('datos/getposiciones_subrubro/{idsubrubro?}', 'GetdataController@getposicionessubrubro')->middleware('role:1,2,3,4');
    Route::get('datos/getubicaciones_sector/{idsector?}', 'GetdataController@getubicacionessector')->middleware('role:1,2,3,4');
    Route::get('datos/getposiciones_sector/{idsector?}', 'GetdataController@getposicionessector')->middleware('role:1,2,3,4');
    Route::get('datos/getsectores_ubicacion/{idubicacion?}', 'GetdataController@getsectoresubicacion')->middleware('role:1,2,3,4');
    Route::get('datos/getposiciones_ubicacion/{idubicacion?}', 'GetdataController@getposicionesubicacion')->middleware('role:1,2,3,4');
    Route::get('datos/getsectores_posicion/{idposicion?}', 'GetdataController@getsectoresposicion')->middleware('role:1,2,3,4');
    Route::get('datos/getubicaciones_posicion/{idposicion?}', 'GetdataController@getubicacionesposicion')->middleware('role:1,2,3,4');
    Route::get('datos/getoc_titular/{idtitular?}', 'GetdataController@getoctitular')->middleware('role:1,2,3,4');
    Route::get('datos/getoc_detalle/{idoc}', 'GetdataController@getocdetalle')->middleware('role:1,2,3,4');
    Route::get('datos/getfaltantes_prov/{idtitular}', 'GetdataController@getfaltantes')->middleware('role:1,2,3,4');
    Route::get('data/getitems_prov/{idtitular}', 'GetdataController@getitemsproveedor')->middleware('role:1,2,3,4');
    Route::get('data/getrem_tit/{idtitular}', 'GetdataController@getremitostitular')->middleware('role:1,2,3,4');
    Route::get('data/getremfact', 'GetdataController@getremitosyfacturas')->middleware('role:1,2,3,4');
    Route::get('data/getsaldo_tit/{idtitular}/{id?}', 'GetdataController@getsaldotitular')->middleware('role:1,2,3,4');
    Route::get('inventario/movimientos/getsectores/{iditem?}', 'GetdataController@getsectoresitem')->middleware('role:1,2,3,4');
    Route::get('inventario/movimientos/getubicaciones/{iditem}/{idsector}', 'GetdataController@getubicacionesitem')->middleware('role:1,2,3,4');
    Route::get('inventario/movimientos/getposiciones/{iditem}/{idsector?}', 'GetdataController@getposicionesitem')->middleware('role:1,2,3,4');
    Route::get('inventario/movimientos/getubicaciones_sector/{idsector}', 'GetdataController@getubicaciones_sector')->middleware('role:1,2,3,4');
    Route::get('inventario/movimientos/getposiciones_sector/{idsector}/{idubi?}', 'GetdataController@getposiciones_sector')->middleware('role:1,2,3,4');
    Route::get('datos/getproductos', 'GetdataController@getproductos')->middleware('role:1,2,3,4');
    Route::get('datos/getproductosrem', 'GetdataController@getproductosrem')->middleware('role:1,2,3,4');
    Route::get('datos/getproductos_v2', 'GetdataController@getproductos_v2')->middleware('role:1,2,3,4');
    Route::get('datos/ctrolstk/getproductos/{idcontrol}/{idconteo?}', 'GetdataController@getproductosctrolstk')->middleware('role:1,2,3,4');
    Route::get('datos/gettitulares/{consumidorfinal?}', 'GetdataController@gettitulares')->middleware('role:1,2,3,4');
    Route::get('data/vencidos/gettotales', 'GetdataController@getvencidostotales')->middleware('role:1,2,3');
    /* --------------------------------------------------------------------End Routes GetData */


    /* Begin CRUD Paises ------------------------------------------------------------------------*/
    Route::post('paises/store', 'PaisesController@store')->middleware('role:1,2');
    Route::post('paises/update/{id}', 'PaisesController@update')->middleware('role:1,2');
    Route::get('paises/delete/{id}', 'PaisesController@delete')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD Paises */

    /* Begin CRUD Provincias ------------------------------------------------------------------------*/
    Route::get('provincias', 'ProvinciasController@index')->middleware('role:1,2');
    Route::get('provincias/create', 'ProvinciasController@create')->middleware('role:1,2');
    Route::post('provincias/store', 'ProvinciasController@store')->middleware('role:1,2');
    Route::get('provincias/show/{id}', 'ProvinciasController@show')->middleware('role:1,2');
    Route::get('provincias/edit/{id}', 'ProvinciasController@edit')->middleware('role:1,2');
    Route::post('provincias/update/{id}', 'ProvinciasController@update')->middleware('role:1,2');
    Route::get('provincias/delete/{id}', 'ProvinciasController@delete')->middleware('role:1,2');
    Route::post('provincias/removeselected', 'ProvinciasController@removeselected')->middleware('role:1,2');
    Route::get('provincias/import', function () {
        return view('master.provincias.import',  ['titulo' => 'Provincias', 'urlkey' => 'provincias']);
    })->middleware('role:1,2');
    Route::post('provincias/importExcel', 'ProvinciasController@importExcel')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD Provincias */

    /* Begin CRUD Localidades ------------------------------------------------------------------------*/
    Route::get('localidades', 'LocalidadesController@index')->middleware('role:1,2');
    Route::get('localidades/create', 'LocalidadesController@create')->middleware('role:1,2');
    Route::post('localidades/store', 'LocalidadesController@store')->middleware('role:1,2');
    Route::get('localidades/show/{id}', 'LocalidadesController@show')->middleware('role:1,2');
    Route::get('localidades/edit/{id}', 'LocalidadesController@edit')->middleware('role:1,2');
    Route::post('localidades/update/{id}', 'LocalidadesController@update')->middleware('role:1,2');
    Route::get('localidades/delete/{id}', 'LocalidadesController@delete')->middleware('role:1,2');
    Route::post('localidades/removeselected', 'LocalidadesController@removeselected')->middleware('role:1,2');
    Route::get('localidades/import', function () {
        return view('master.localidades.import',  ['titulo' => 'Localidades', 'urlkey' => 'localidades']);
    })->middleware('role:1,2');
    Route::post('localidades/importExcel', 'LocalidadesController@importExcel')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD Provincias */

    /* Begin CRUD Marcas ------------------------------------------------------------------------*/
    Route::get('marcas', 'MarcasController@index')->middleware('role:1,2');
    Route::get('marcas/create', function () {
        return view('master.create',  ['titulo' => 'Marcas', 'urlkey' => 'marcas', 'item' => 'Marca']);
    })->middleware('role:1,2');
    Route::post('marcas/store', 'MarcasController@store')->middleware('role:1,2');
    Route::get('marcas/show/{id}', 'MarcasController@show')->middleware('role:1,2');
    Route::get('marcas/edit/{id}', 'MarcasController@edit')->middleware('role:1,2');
    Route::post('marcas/update/{id}', 'MarcasController@update')->middleware('role:1,2');
    Route::get('marcas/delete/{id}', 'MarcasController@delete')->middleware('role:1,2');
    Route::post('marcas/removeselected', 'MarcasController@removeselected')->middleware('role:1,2');
    Route::get('marcas/import', 'MarcasController@import')->middleware('role:1,2');
    Route::post('marcas/importExcel', 'MarcasController@importExcel')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD Marcas */

    /* Begin CRUD Rubros ------------------------------------------------------------------------*/
    Route::get('rubros', 'RubrosController@index')->middleware('role:1,2');
    Route::get('rubros/create', function () {
        return view('master.create',  ['titulo' => 'Rubros', 'urlkey' => 'rubros', 'item' => 'Rubro']);
    })->middleware('role:1,2');
    Route::post('rubros/store', 'RubrosController@store')->middleware('role:1,2');
    Route::get('rubros/show/{id}', 'RubrosController@show')->middleware('role:1,2');
    Route::get('rubros/edit/{id}', 'RubrosController@edit')->middleware('role:1,2');
    Route::post('rubros/update/{id}', 'RubrosController@update')->middleware('role:1,2');
    Route::get('rubros/delete/{id}', 'RubrosController@delete')->middleware('role:1,2');
    Route::post('rubros/removeselected', 'RubrosController@removeselected')->middleware('role:1,2');
    Route::get('rubros/import', 'RubrosController@import')->middleware('role:1,2');
    Route::post('rubros/importExcel', 'RubrosController@importExcel')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD Rubros */

    Route::get('subrubros', 'SubrubrosController@index')->middleware('role:1,2');
    Route::get('subrubros/create', 'SubrubrosController@create')->middleware('role:1,2');
    Route::post('subrubros/store', 'SubrubrosController@store')->middleware('role:1,2');
    Route::get('subrubros/show/{id}', 'SubrubrosController@show')->middleware('role:1,2');
    Route::get('subrubros/edit/{id}', 'SubrubrosController@edit')->middleware('role:1,2');
    Route::get('subrubros/delete/{id}', 'SubrubrosController@delete')->middleware('role:1,2');
    Route::post('subrubros/removeselected', 'SubrubrosController@removeselected')->middleware('role:1,2');
    Route::get('subrubros/import', 'SubrubrosController@import')->middleware('role:1,2');
    Route::post('subrubros/importExcel', 'SubrubrosController@importExcel')->middleware('role:1,2');
    Route::get('subrubros/vincular', 'SubrubrosController@vincular')->middleware('role:1,2');
    
    
    
    /* Begin CRUD Lineas ------------------------------------------------------------------------*/
    Route::get('lineas', 'LineasController@index')->middleware('role:1,2');
    Route::get('lineas/create', function () {
        return view('master.create',  ['titulo' => 'Lineas', 'urlkey' => 'lineas', 'item' => 'Linea']);
    })->middleware('role:1,2');
    Route::post('lineas/store', 'LineasController@store')->middleware('role:1,2');
    Route::get('lineas/show/{id}', 'LineasController@show')->middleware('role:1,2');
    Route::get('lineas/edit/{id}', 'LineasController@edit')->middleware('role:1,2');
    Route::post('lineas/update/{id}', 'LineasController@update')->middleware('role:1,2');
    Route::get('lineas/delete/{id}', 'LineasController@delete')->middleware('role:1,2');
    Route::post('lineas/removeselected', 'LineasController@removeselected')->middleware('role:1,2');
    Route::get('lineas/import', function () {
        return view('master.import',  ['titulo' => 'Lineas', 'urlkey' => 'lineas','itemnav' => 'otro']);
    })->middleware('role:1,2');
    Route::post('lineas/importExcel', 'LineasController@importExcel')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD Lineas */

    /* Begin CRUD Materiales ------------------------------------------------------------------------*/
    Route::get('materiales', 'MaterialesController@index')->middleware('role:1,2');
    Route::get('materiales/create', function () {
        return view('master.create',  ['titulo' => 'Materiales', 'urlkey' => 'materiales', 'item' => 'Material']);
    })->middleware('role:1,2');
    Route::post('materiales/store', 'MaterialesController@store')->middleware('role:1,2');
    Route::get('materiales/show/{id}', 'MaterialesController@show')->middleware('role:1,2');
    Route::get('materiales/edit/{id}', 'MaterialesController@edit')->middleware('role:1,2');
    Route::post('materiales/update/{id}', 'MaterialesController@update')->middleware('role:1,2');
    Route::get('materiales/delete/{id}', 'MaterialesController@delete')->middleware('role:1,2');
    Route::post('materiales/removeselected', 'MaterialesController@removeselected')->middleware('role:1,2');
    Route::get('materiales/import', function () {
        return view('master.import',  ['titulo' => 'Materiales', 'urlkey' => 'materiales','itemnav' => 'otro']);
    })->middleware('role:1,2');
    Route::post('materiales/importExcel', 'MaterialesController@importExcel')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD Materiales */

    /* Begin CRUD Origenes ------------------------------------------------------------------------*/
    Route::get('origenes', 'OrigenesController@index')->middleware('role:1,2');
    Route::get('origenes/create', function () {
        return view('master.create',  ['titulo' => 'Origenes de Fabricacion', 'urlkey' => 'origenes', 'item' => 'Origen de Fabricacion']);
    })->middleware('role:1,2');
    Route::post('origenes/store', 'OrigenesController@store')->middleware('role:1,2');
    Route::get('origenes/show/{id}', 'OrigenesController@show')->middleware('role:1,2');
    Route::get('origenes/edit/{id}', 'OrigenesController@edit')->middleware('role:1,2');
    Route::post('origenes/update/{id}', 'OrigenesController@update')->middleware('role:1,2');
    Route::get('origenes/delete/{id}', 'OrigenesController@delete')->middleware('role:1,2');
    Route::post('origenes/removeselected', 'OrigenesController@removeselected')->middleware('role:1,2');
    Route::get('origenes/import', function () {
        return view('master.import',  ['titulo' => 'Origenes de Fabricacion', 'urlkey' => 'origenes','itemnav' => 'otro']);
    })->middleware('role:1,2');
    Route::post('origenes/importExcel', 'OrigenesController@importExcel')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD Origenes */

    /* Begin CRUD UnidadesMedidas ------------------------------------------------------------------------*/
    Route::get('unidadesMedidas', 'UnidadesMedidasController@index')->middleware('role:1,2');
    Route::get('unidadesMedidas/create', function () {
        return view('master.create',  ['titulo' => 'Unidades de Medida', 'urlkey' => 'unidadesMedidas', 'item' => 'Unidad de Medida']);
    })->middleware('role:1,2');
    Route::post('unidadesMedidas/store', 'UnidadesMedidasController@store')->middleware('role:1,2');
    Route::get('unidadesMedidas/show/{id}', 'UnidadesMedidasController@show')->middleware('role:1,2');
    Route::get('unidadesMedidas/edit/{id}', 'UnidadesMedidasController@edit')->middleware('role:1,2');
    Route::post('unidadesMedidas/update/{id}', 'UnidadesMedidasController@update')->middleware('role:1,2');
    Route::get('unidadesMedidas/delete/{id}', 'UnidadesMedidasController@delete')->middleware('role:1,2');
    Route::post('unidadesMedidas/removeselected', 'UnidadesMedidasController@removeselected')->middleware('role:1,2');
    Route::get('unidadesMedidas/import', function () {
        return view('master.import',  ['titulo' => 'Unidades de Medida', 'urlkey' => 'unidadesMedidas','itemnav' => 'otro']);
    })->middleware('role:1,2');
    Route::post('unidadesMedidas/importExcel', 'UnidadesMedidasController@importExcel')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD UnidadesMedidas */

    /* Begin CRUD Bancos ------------------------------------------------------------------------*/
    Route::get('bancos', 'BancosController@index')->middleware('role:1,2');
    Route::get('bancos/create', function () {
        return view('master.create',  ['titulo' => 'Bancos', 'urlkey' => 'bancos', 'item' => 'Banco']);
    })->middleware('role:1,2');
    Route::post('bancos/store', 'BancosController@store')->middleware('role:1,2');
    Route::get('bancos/show/{id}', 'BancosController@show')->middleware('role:1,2');
    Route::get('bancos/edit/{id}', 'BancosController@edit')->middleware('role:1,2');
    Route::post('bancos/update/{id}', 'BancosController@update')->middleware('role:1,2');
    Route::get('bancos/delete/{id}', 'BancosController@delete')->middleware('role:1,2');
    Route::post('bancos/removeselected', 'BancosController@removeselected')->middleware('role:1,2');
    Route::get('bancos/import', function () {
        return view('master.import',  ['titulo' => 'Bancos', 'urlkey' => 'bancos']);
    })->middleware('role:1,2');
    Route::post('bancos/importExcel', 'BancosController@importExcel')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD Bancos */

    /* Begin CRUD Monedas ------------------------------------------------------------------------*/
    Route::get('monedas', 'MonedasController@index')->middleware('role:1');
    Route::get('monedas/create', function () {
        return view('master.create',  ['titulo' => 'Monedas', 'urlkey' => 'monedas', 'item' => 'Moneda']);
    })->middleware('role:1');
    Route::post('monedas/store', 'MonedasController@store')->middleware('role:1');
    Route::get('monedas/show/{id}', 'MonedasController@show')->middleware('role:1');
    Route::get('monedas/edit/{id}', 'MonedasController@edit')->middleware('role:1');
    Route::post('monedas/update/{id}', 'MonedasController@update')->middleware('role:1');
    Route::get('monedas/delete/{id}', 'MonedasController@delete')->middleware('role:1');
    Route::post('monedas/removeselected', 'MonedasController@removeselected')->middleware('role:1');
    Route::get('monedas/import', function () {
        return view('master.import',  ['titulo' => 'Monedas', 'urlkey' => 'monedas']);
    })->middleware('role:1');
    Route::post('monedas/importExcel', 'MonedasController@importExcel')->middleware('role:1');
    /* --------------------------------------------------------------------End Routes CRUD Monedas */

    /* --------------------------------------------------------------------------------------------*/
    /* --------------------------------------------------------------------------------------------*/

    /* Begin CRUD Productos ------------------------------------------------------------------------*/
    Route::get('productos', 'ProductosController@index')->middleware('role:1,2,3,4,5');
    Route::get('productos/create', 'ProductosController@create')->middleware('role:1,2,3,4');
    Route::post('productos/store', 'ProductosController@store')->middleware('role:1,2,3,4');
    Route::get('productos/show/{id}', 'ProductosController@show')->middleware('role:1,2,3,4');
    Route::get('productos/edit/{id}', 'ProductosController@edit')->middleware('role:1,2,3,4');
    Route::post('productos/update/{id}', 'ProductosController@update')->middleware('role:1,2,3,4');
    Route::get('productos/delete/{id}', 'ProductosController@delete')->middleware('role:1');
    Route::post('productos/removeselected', 'ProductosController@removeselected');
    Route::get('productos/import', function () {
        return view('items.productos.import',  ['titulo' => 'Catalogo de Productos', 'urlkey' => 'productos']);
    })->middleware('role:1,2,3,4');
    Route::post('productos/importExcel', 'ProductosController@importExcel')->middleware('role:1,2,3,4');
    Route::get('productos/eliminarup/{id}', 'ProductosController@removeup')->middleware('role:1,2');
    Route::get('productos/eliminarcross/{id}', 'ProductosController@removecross')->middleware('role:1,2');
    Route::get('productos/eliminarprov/{id}', 'ProductosController@removeprov')->middleware('role:1,2');
    Route::get('productos/preciocosto/{id}', 'ProductosController@getpcosto')->middleware('role:1,2,3');
    Route::get('productos/precio/{id}', 'ProductosController@getprecio')->middleware('role:1,2,3');
    Route::get('productos/sugerencias/{id}', 'ProductosController@getsugerencias')->middleware('role:1,2,3');
    Route::post('productos/popup/store', 'ProductosController@store')->middleware('role:1,2,3');
    Route::get('productos/eans', 'ProductosController@eanslist')->middleware('role:1,2,3,4');
    Route::get('productos/eans/update', 'ProductosController@eansupdate')->middleware('role:1,2,3,4');
    Route::get('productos/nuevamarca', 'ProductosController@nuevamarca')->middleware('role:1,2,3,4');
    Route::post('ajax/productos/store', 'ProductosController@ajaxstore')->middleware('role:1,2,3,4');
    Route::get('productos/vincular', 'ProductosController@vincular')->middleware('role:1,2,3,4');
    Route::get('productos/getmasvendidos', 'ProductosController@getmasvendidos')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD Productos */

    /* Begin CRUD insumos ------------------------------------------------------------------------*/
    Route::get('insumos', 'InsumosController@index')->middleware('role:1,2');
    Route::get('insumos/create', 'InsumosController@create')->middleware('role:1,2');
    Route::post('insumos/store', 'InsumosController@store')->middleware('role:1,2');
    Route::get('insumos/show/{id}', 'InsumosController@show')->middleware('role:1,2');
    Route::get('insumos/edit/{id}', 'InsumosController@edit')->middleware('role:1,2');
    Route::post('insumos/update/{id}', 'InsumosController@update')->middleware('role:1,2');
    Route::get('insumos/delete/{id}', 'InsumosController@delete')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD insumos */

    /* Begin CRUD bienes ------------------------------------------------------------------------*/
    Route::get('bienes', 'BienesController@index')->middleware('role:1,2');
    Route::get('bienes/create', 'BienesController@create')->middleware('role:1,2');
    Route::post('bienes/store', 'BienesController@store')->middleware('role:1,2');
    Route::get('bienes/show/{id}', 'BienesController@show')->middleware('role:1,2');
    Route::get('bienes/edit/{id}', 'BienesController@edit')->middleware('role:1,2');
    Route::post('bienes/update/{id}', 'BienesController@update')->middleware('role:1,2');
    Route::get('bienes/delete/{id}', 'BienesController@delete')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD bienes */

    /* Begin CRUD servicios ------------------------------------------------------------------------*/
    Route::get('servicios', 'ServiciosController@index')->middleware('role:1,2');
    Route::get('servicios/create', 'ServiciosController@create')->middleware('role:1,2');
    Route::post('servicios/store', 'ServiciosController@store')->middleware('role:1,2');
    Route::get('servicios/show/{id}', 'ServiciosController@show')->middleware('role:1,2');
    Route::get('servicios/edit/{id}', 'ServiciosController@edit')->middleware('role:1,2');
    Route::post('servicios/update/{id}', 'ServiciosController@update')->middleware('role:1,2');
    Route::get('servicios/delete/{id}', 'ServiciosController@delete')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD servicios */

    /* Begin CRUD inversiones ------------------------------------------------------------------------*/
    Route::get('inversiones', 'InversionesController@index')->middleware('role:1,2');
    Route::get('inversiones/create', 'InversionesController@create')->middleware('role:1,2');
    Route::post('inversiones/store', 'InversionesController@store')->middleware('role:1,2');
    Route::get('inversiones/show/{id}', 'InversionesController@show')->middleware('role:1,2');
    Route::get('inversiones/edit/{id}', 'InversionesController@edit')->middleware('role:1,2');
    Route::post('inversiones/update/{id}', 'InversionesController@update')->middleware('role:1,2');
    Route::get('inversiones/delete/{id}', 'InversionesController@delete')->middleware('role:1,2');
    /* --------------------------------------------------------------------End Routes CRUD inversiones */

    /* --------------------------------------------------------------------------------------------*/
    /* --------------------------------------------------------------------------------------------*/

    /*Begin CRUD Proveedores ----------------------------------------------------------------*/
    Route::get('proveedores', 'ProveedoresController@index')->middleware('role:1,2,3');
    Route::get('proveedores/create', 'ProveedoresController@create');
    Route::post('proveedores/store', 'ProveedoresController@store');
    Route::get('proveedores/show/{id}', 'ProveedoresController@show')->middleware('role:1,2,3');
    Route::get('proveedores/edit/{proveedor}', 'ProveedoresController@edit')->middleware('role:1,2,3');
    Route::post('proveedores/update/{proveedor}', 'ProveedoresController@update')->middleware('role:1,2,3');
    Route::get('proveedores/delete/{proveedor}', 'ProveedoresController@delete');
    Route::post('proveedores/removeselected', 'ProveedoresController@removeselected');
    Route::get('proveedores/import', function () {
        return view('titulares.proveedores.import',  ['titulo' => 'Proveedores', 'urlkey' => 'proveedores']);
    })->middleware('role:1,2,3');
    Route::get('proveedores/cuentacorriente', 'ProveedoresController@ctacte')->middleware('role:1,2,3');
    Route::post('proveedores/importExcel', 'ProveedoresController@importExcel')->middleware('role:1,2,3');
    /*---------------------------------------------------------End Routes CRUD Proveedores  */

    /*Begin CRUD Clientes ----------------------------------------------------------------*/
    Route::get('clientes', 'ClientesController@index')->middleware('role:1,2,3');
    Route::get('clientes/create', 'ClientesController@create');
    Route::post('clientes/store', 'ClientesController@store');
    Route::get('clientes/show/{id}', 'ClientesController@show')->middleware('role:1,2,3');
    Route::get('clientes/edit/{proveedor}', 'ClientesController@edit')->middleware('role:1,2,3');
    Route::post('clientes/update/{proveedor}', 'ClientesController@update')->middleware('role:1,2,3');
    Route::get('clientes/delete/{proveedor}', 'ClientesController@delete');
    Route::post('clientes/removeselected', 'ClientesController@removeselected');
    Route::get('clientes/import', function () {
        return view('titulares.clientes.import',  ['titulo' => 'Clientes', 'urlkey' => 'clientes']);
    })->middleware('role:1,2,3');
    Route::get('clientes/cc/repararestados/{id}', 'ClientesController@repararestados')->middleware('role:1,2');
    Route::get('clientes/cc/actualizarprecios/{id}', 'ClientesController@actualizarprecios')->middleware('role:1,2');
    Route::get('clientes/cc/print/{id}', 'ClientesController@getprint')->middleware('role:1,2,3');
    Route::get('clientes/cc/printview/{id}', 'ClientesController@printview')->name('printctacte')->middleware('role:1,2,3');
    Route::get('clientes/cuentacorriente/{id?}', 'ClientesController@ctacte')->middleware('role:1,2,3');
    Route::get('clientes/vencidos', 'ClientesController@vencidos')->middleware('role:1,2,3');
    Route::post('clientes/importExcel', 'ClientesController@importExcel')->middleware('role:1,2,3');
    /*---------------------------------------------------------End Routes CRUD Clientes  */

    /* Usuarios------------ ------------------------------------------------------------------------*/
    Route::get('usuarios/', 'UsuariosController@index')->middleware('role:1');
    Route::get('usuarios/create', 'UsuariosController@create')->middleware('role:1');
    Route::post('usuarios/store', 'UsuariosController@store')->middleware('role:1');
    Route::get('usuarios/show/{id}', 'UsuariosController@show')->middleware('role:1');
    Route::get('usuarios/edit/{id}', 'UsuariosController@edit')->middleware('role:1');
    Route::get('usuarios/cambiodeclave', 'UsuariosController@changePwd');
    Route::get('usuarios/resetearclave/{id}', 'UsuariosController@resetPwd');
    Route::get('usuarios/resumenmensual', 'UsuariosController@performance');
    /* Usuarios------------ ------------------------------------------------------------------------*/

    /* Notificaciones------------ ------------------------------------------------------------------------*/
    Route::get('notificaciones/', 'NotificacionesController@index');
    Route::get('notificaciones/marcar/{id}', 'NotificacionesController@marcar');
    Route::get('notificaciones/desmarcar/{id}', 'NotificacionesController@desmarcar');
    Route::get('ajax/notificaciones/leidos', 'NotificacionesController@setleidos');
    /* Notificaciones------------ ------------------------------------------------------------------------*/


    /*AJAX*/
    Route::post('ajax/titulares/store', 'TitularesController@ajaxstore')->middleware('role:1,2,3,4');
    Route::post('ajax/nuevacotizacion/store', 'MonedasController@ajaxstorecotizacion')->middleware('role:1,2');
    /*AJAX*/
});


