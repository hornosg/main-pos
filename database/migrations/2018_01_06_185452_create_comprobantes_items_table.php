<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cpte')->references('id')->on('comprobantes');
            $table->integer('id_item')->references('id')->on('items');
            $table->decimal('preciouni', 10, 2)->nullable();
            $table->decimal('cantidad', 6, 2)->nullable();
            $table->decimal('importe', 10, 2)->nullable();
            $table->timestamps();
			$table->integer('created_us')->references('id')->on('users')->nullable();
            $table->integer('updated_us')->references('id')->on('users')->nullable();
			$table->softDeletes();			
        });

        Schema::table('comprobantes_items', function (Blueprint $table) {
            $table->index('id_cpte','idx_comprobantes_items1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes_items');
    }
}
