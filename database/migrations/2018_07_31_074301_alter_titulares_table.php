<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTitularesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('titulares', function (Blueprint $table) {
            $table->integer('id_tipoiva')->references('id')->on('sys_tipoiva')->after('logo')->nullable();
            $table->integer('id_localidad')->references('id')->on('conf_localidades')->after('direccion')->nullable();
            $table->string('email')->after('web')->nullable();
        });
    }
}
