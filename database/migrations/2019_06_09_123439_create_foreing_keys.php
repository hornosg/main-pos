<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeingKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('cajas', function (Blueprint $table) {
            $table->unsignedInteger('id_sucursal')->change();
            $table->foreign('id_sucursal')->references('id')->on('sucursales');
        });

        Schema::table('cajas_arqueo', function (Blueprint $table) {
            $table->unsignedInteger('id_caja')->change();
            $table->foreign('id_caja')->references('id')->on('cajas');
        });
//        DELETE FROM `express`.`comprobantes` WHERE `id`='277928';
//        DELETE FROM `express`.`comprobantes` WHERE `id`='263771';
//        delete from comprobantes where id = 263772;
//        update  comprobantes set id_estado = 2 where id_tipocpte = 16;
//        delete from comprobantes where id in (268998,271725,273525,273845);

        Schema::table('comprobantes', function (Blueprint $table) {
            $table->unsignedInteger('id_tipocpte')->change();
            $table->unsignedInteger('id_titular')->change();
            $table->unsignedInteger('id_caja')->change();
            $table->unsignedInteger('id_ajuste')->change();
            $table->unsignedInteger('id_cpterel')->change();
            $table->unsignedInteger('id_estado')->change();
            $table->unsignedInteger('created_us')->change();
            $table->unsignedInteger('updated_us')->change();
            $table->foreign('id_tipocpte')->references('id')->on('sys_tipocomprobantes');
            $table->foreign('id_titular')->references('id')->on('titulares');
            $table->foreign('id_caja')->references('id')->on('cajas');
            $table->foreign('id_ajuste')->references('id')->on('conf_ajustes_precio');
            $table->foreign('id_cpterel')->references('id')->on('comprobantes');
            $table->foreign('id_estado')->references('id')->on('sys_estados');
            $table->foreign('created_us')->references('id')->on('users');
            $table->foreign('updated_us')->references('id')->on('users');
        });

        //delete from comprobantes_descint where id = 845;
        Schema::table('comprobantes_descint', function (Blueprint $table) {
            $table->unsignedInteger('id_cpte')->change();
            $table->unsignedInteger('id_descint')->change();
            $table->unsignedInteger('created_us')->change();
            $table->unsignedInteger('updated_us')->change();
            $table->foreign('id_cpte')->references('id')->on('comprobantes');
            $table->foreign('id_descint')->references('id')->on('conf_descint');
            $table->foreign('created_us')->references('id')->on('users');
            $table->foreign('updated_us')->references('id')->on('users');
        });

        Schema::table('comprobantes_envio', function (Blueprint $table) {
            $table->unsignedInteger('id_cpte')->change();
            $table->unsignedInteger('created_us')->change();
            $table->unsignedInteger('updated_us')->change();
            $table->foreign('id_cpte')->references('id')->on('comprobantes');
            $table->foreign('created_us')->references('id')->on('users');
            $table->foreign('updated_us')->references('id')->on('users');
        });

        //delete from comprobantes_items where id = 533996;
        Schema::table('comprobantes_items', function (Blueprint $table) {
            $table->unsignedInteger('id_cpte')->change();
            $table->unsignedInteger('id_item')->change();
            $table->unsignedInteger('id_movstk')->change();
            $table->unsignedInteger('created_us')->change();
            $table->unsignedInteger('updated_us')->change();
            $table->foreign('id_cpte')->references('id')->on('comprobantes');
            $table->foreign('id_item')->references('id')->on('items');
            $table->foreign('id_movstk')->references('id')->on('inv_movimientos_stock');
            $table->foreign('created_us')->references('id')->on('users');
            $table->foreign('updated_us')->references('id')->on('users');
        });

        //delete from comprobantes_mp where id = 263799;
        Schema::table('comprobantes_mp', function (Blueprint $table) {
            $table->unsignedInteger('id_cpte')->change();
            $table->unsignedInteger('id_mediopago')->change();
            $table->unsignedInteger('id_banco')->change();
            $table->unsignedInteger('created_us')->change();
            $table->unsignedInteger('updated_us')->change();
            $table->foreign('id_cpte')->references('id')->on('comprobantes');
            $table->foreign('id_mediopago')->references('id')->on('sys_medios_pago');
            $table->foreign('id_banco')->references('id')->on('conf_bancos');
            $table->foreign('created_us')->references('id')->on('users');
            $table->foreign('updated_us')->references('id')->on('users');
        });

        Schema::table('conf_descint', function (Blueprint $table) {
            $table->unsignedInteger('id_tipocliente')->change();
            $table->unsignedInteger('id_mediopago')->change();
            $table->foreign('id_tipocliente')->references('id')->on('sys_tipoclientes');
            $table->foreign('id_mediopago')->references('id')->on('sys_medios_pago');
        });

        Schema::table('conf_ganancia_fija', function (Blueprint $table) {
            $table->unsignedInteger('id_tipocliente')->change();
            $table->foreign('id_tipocliente')->references('id')->on('sys_tipoclientes');
        });

        Schema::table('conf_localidades', function (Blueprint $table) {
            $table->unsignedInteger('id_provincia')->change();
            $table->foreign('id_provincia')->references('id')->on('conf_provincias');
        });

        Schema::table('conf_provincias', function (Blueprint $table) {
            $table->unsignedInteger('id_pais')->change();
            $table->foreign('id_pais')->references('id')->on('conf_paises');
        });

        Schema::table('conf_subrubros', function (Blueprint $table) {
            $table->unsignedInteger('id_rubro')->change();
            $table->foreign('id_rubro')->references('id')->on('conf_subrubros');
        });

        Schema::table('inv_control_stock', function (Blueprint $table) {
            $table->unsignedInteger('id_marca')->change();
            $table->unsignedInteger('id_rubro')->change();
            $table->unsignedInteger('id_subrubro')->change();
            $table->unsignedInteger('id_item')->change();
            $table->unsignedInteger('id_sector')->change();
            $table->unsignedInteger('id_ubicacion')->change();
            $table->unsignedInteger('id_posicion')->change();
            $table->unsignedInteger('id_estado')->change();
            $table->foreign('id_marca')->references('id')->on('conf_marcas');
            $table->foreign('id_rubro')->references('id')->on('conf_rubros');
            $table->foreign('id_subrubro')->references('id')->on('conf_subrubros');
            $table->foreign('id_item')->references('id')->on('items');
            $table->foreign('id_sector')->references('id')->on('inv_sectores');
            $table->foreign('id_ubicacion')->references('id')->on('inv_ubicaciones');
            $table->foreign('id_posicion')->references('id')->on('inv_posiciones');
            $table->foreign('id_estado')->references('id')->on('sys_estados');
        });

        Schema::table('inv_control_stock_conteo', function (Blueprint $table) {
            $table->unsignedInteger('id_ctrolstk')->change();
            $table->unsignedInteger('id_item')->change();
            $table->unsignedInteger('id_sector')->change();
            $table->unsignedInteger('id_ubicacion')->change();
            $table->unsignedInteger('id_posicion')->change();
            $table->unsignedInteger('id_movimiento')->change();
            $table->foreign('id_ctrolstk')->references('id')->on('inv_control_stock');
            $table->foreign('id_item')->references('id')->on('items');
            $table->foreign('id_sector')->references('id')->on('inv_sectores');
            $table->foreign('id_ubicacion')->references('id')->on('inv_ubicaciones');
            $table->foreign('id_posicion')->references('id')->on('inv_posiciones');
            $table->foreign('id_movimiento')->references('id')->on('inv_movimientos_stock');
        });

        Schema::table('inv_movimientos_stock', function (Blueprint $table) {
            $table->unsignedInteger('id_motivo')->change();
            $table->unsignedInteger('id_cpteitem')->change();
            $table->unsignedInteger('id_item')->change();
            $table->unsignedInteger('id_sector_o')->change();
            $table->unsignedInteger('id_ubicacion_o')->change();
            $table->unsignedInteger('id_posicion_o')->change();
            $table->unsignedInteger('id_sector_d')->change();
            $table->unsignedInteger('id_ubicacion_d')->change();
            $table->unsignedInteger('id_posicion_d')->change();
            //$table->foreign('id_motivo')->references('id')->on('sys_motivos_mstock');
            //$table->foreign('id_cpteitem')->references('id')->on('comprobantes_items');
            $table->foreign('id_item')->references('id')->on('items');
            $table->foreign('id_sector_o')->references('id')->on('inv_sectores');
            $table->foreign('id_ubicacion_o')->references('id')->on('inv_ubicaciones');
            $table->foreign('id_posicion_o')->references('id')->on('inv_posiciones');
            $table->foreign('id_sector_d')->references('id')->on('inv_sectores');
            $table->foreign('id_ubicacion_d')->references('id')->on('inv_ubicaciones');
            $table->foreign('id_posicion_d')->references('id')->on('inv_posiciones');
        });

        Schema::table('inv_posiciones', function (Blueprint $table) {
            $table->unsignedInteger('id_ubicacion')->change();
            $table->foreign('id_ubicacion')->references('id')->on('inv_ubicaciones');
        });

        Schema::table('inv_ubicaciones', function (Blueprint $table) {
            $table->unsignedInteger('id_sector')->change();
            $table->foreign('id_sector')->references('id')->on('inv_sectores');
        });

        Schema::table('inv_sectores', function (Blueprint $table) {
            $table->unsignedInteger('id_sucursal')->change();
            $table->foreign('id_sucursal')->references('id')->on('sucursales');
        });

        Schema::table('inv_stock', function (Blueprint $table) {
            $table->unsignedInteger('id_item')->change();
            $table->unsignedInteger('id_sector')->change();
            $table->unsignedInteger('id_ubicacion')->change();
            $table->unsignedInteger('id_posicion')->change();
            $table->unsignedInteger('id_movstk')->change();
            $table->foreign('id_item')->references('id')->on('items');
            $table->foreign('id_sector')->references('id')->on('inv_sectores');
            $table->foreign('id_ubicacion')->references('id')->on('inv_ubicaciones');
            $table->foreign('id_posicion')->references('id')->on('inv_posiciones');
            $table->foreign('id_movstk')->references('id')->on('inv_movimientos_stock');
        });

        Schema::table('items', function (Blueprint $table) {
            $table->unsignedInteger('id_tipoitem')->change();
            $table->unsignedInteger('id_unidadventa')->change();
            $table->unsignedInteger('id_unidadcontneto')->change();
            $table->unsignedInteger('id_marca')->change();
            $table->unsignedInteger('id_rubro')->change();
            $table->unsignedInteger('id_subrubro')->change();
            $table->unsignedInteger('id_linea')->change();
            $table->unsignedInteger('id_material')->change();
            $table->unsignedInteger('idOrigen')->change();
            $table->unsignedInteger('id_moneda')->change();
            $table->foreign('id_tipoitem')->references('id')->on('sys_tipoitems');
            $table->foreign('id_unidadventa')->references('id')->on('conf_unidades_medida');
            $table->foreign('id_unidadcontneto')->references('id')->on('conf_unidades_medida');
            $table->foreign('id_marca')->references('id')->on('conf_marcas');
            $table->foreign('id_rubro')->references('id')->on('conf_rubros');
            $table->foreign('id_subrubro')->references('id')->on('conf_subrubros');
            $table->foreign('id_linea')->references('id')->on('conf_lineas');
            $table->foreign('id_material')->references('id')->on('conf_materiales');
            $table->foreign('idOrigen')->references('id')->on('conf_origen');
            $table->foreign('id_moneda')->references('id')->on('sys_monedas');
        });

        Schema::table('items_combo', function (Blueprint $table) {
            $table->unsignedInteger('id_item')->change();
            $table->unsignedInteger('id_itemcomponente')->change();
            $table->foreign('id_item')->references('id')->on('items');
            $table->foreign('id_itemcomponente')->references('id')->on('items');
        });

        Schema::table('items_cross_sell', function (Blueprint $table) {
            $table->unsignedInteger('id_item')->change();
            $table->unsignedInteger('id_itemcross')->change();
            $table->foreign('id_item')->references('id')->on('items');
            $table->foreign('id_itemcross')->references('id')->on('items');
        });

        Schema::table('items_up_sell', function (Blueprint $table) {
            $table->unsignedInteger('id_item')->change();
            $table->unsignedInteger('id_itemup')->change();
            $table->foreign('id_item')->references('id')->on('items');
            $table->foreign('id_itemup')->references('id')->on('items');
        });

        Schema::table('items_proveedores', function (Blueprint $table) {
            $table->unsignedInteger('id_item')->change();
            $table->unsignedInteger('id_titular')->change();
            $table->foreign('id_item')->references('id')->on('items');
            $table->foreign('id_titular')->references('id')->on('titulares');
        });

        Schema::table('notificaciones_estados', function (Blueprint $table) {
            $table->unsignedInteger('id_notificacion')->change();
            $table->unsignedInteger('id_user')->change();
            $table->unsignedInteger('id_estado')->change();
            $table->foreign('id_notificacion')->references('id')->on('notificaciones');
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_estado')->references('id')->on('sys_estados');
        });

        Schema::table('numeradores', function (Blueprint $table) {
            $table->unsignedInteger('id_tipocpte')->change();
            $table->unsignedInteger('id_sucursal')->change();
            $table->foreign('id_tipocpte')->references('id')->on('sys_tipocomprobantes');
            $table->foreign('id_sucursal')->references('id')->on('sucursales');
        });

        Schema::table('sucursales', function (Blueprint $table) {
            $table->unsignedInteger('id_localidad')->change();
            $table->foreign('id_localidad')->references('id')->on('conf_localidades');
        });

        Schema::table('sys_numcaja', function (Blueprint $table) {
            $table->unsignedInteger('id_sucursal')->change();
            $table->unsignedInteger('id_user')->change();
            $table->foreign('id_sucursal')->references('id')->on('sucursales');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('cajas', function (Blueprint $table) {
//            $table->dropforeign(['id_sucursal']);
//        });
//        Schema::table('cajas_arqueo', function (Blueprint $table) {
//            $table->dropforeign(['id_caja']);
//        });
//        Schema::table('comprobantes', function (Blueprint $table) {
//            $table->dropforeign(['id_tipocpte,id_titular,id_caja,id_ajuste,id_cpterel,id_estado,created_us,updated_us']);
//        });
//        Schema::table('comprobantes_descint', function (Blueprint $table) {
//            $table->dropforeign(['id_cpte,id_descint,created_us,updated_us']);
//        });
//        Schema::table('comprobantes_envio', function (Blueprint $table) {
//            $table->dropforeign(['id_cpte,created_us,updated_us']);
//        });
//        Schema::table('comprobantes_items', function (Blueprint $table) {
//            $table->dropforeign(['id_cpte,id_item,id_movstk,created_us,updated_us']);
//        });
//        Schema::table('comprobantes_mp', function (Blueprint $table) {
//            $table->dropforeign(['id_cpte,id_mediopago,id_banco,created_us,updated_us']);
//        });
//        Schema::table('conf_descint', function (Blueprint $table) {
//            $table->dropforeign(['id_tipocliente,id_mediopago']);
//        });
//        Schema::table('conf_ganancia_fija', function (Blueprint $table) {
//            $table->dropforeign(['id_tipocliente']);
//        });
//        Schema::table('conf_localidades', function (Blueprint $table) {
//            $table->dropforeign(['id_provincia']);
//        });
//        Schema::table('conf_provincias', function (Blueprint $table) {
//            $table->dropforeign(['id_pais']);
//        });
//        Schema::table('inv_control_stock', function (Blueprint $table) {
//            $table->dropforeign(['id_marca,id_rubro,id_subrubro,id_item,id_sector,id_ubicacion,id_posicion,id_estado']);
//        });
//        Schema::table('inv_control_stock_conteo', function (Blueprint $table) {
//                $table->dropforeign(['id_ctrolstk,id_item,id_sector,id_ubicacion,id_posicion,id_movimiento']);
//        });
//        Schema::table('inv_movimientos_stock', function (Blueprint $table) {
//            $table->dropforeign(['id_motivo,id_cpteitem,id_item,id_sector_o,id_ubicacion_o,id_posicion_o,id_sector_d,id_ubicacion_d,id_posicion_d']);
//        });
//        Schema::table('inv_stock', function (Blueprint $table) {
//            $table->dropforeign(['id_item,id_sector,id_ubicacion,id_posicion,id_movstk']);
//        });
//        Schema::table('inv_posiciones', function (Blueprint $table) {
//            $table->dropforeign(['id_ubicacion']);
//        });
//        Schema::table('inv_ubicaciones', function (Blueprint $table) {
//            $table->dropforeign(['id_sector']);
//        });
//        Schema::table('inv_sectores', function (Blueprint $table) {
//            $table->dropforeign(['id_sucursal']);
//        });
//        Schema::table('items', function (Blueprint $table) {
//            $table->dropforeign(['id_tipoitem,id_unidadventa,id_unidadcontneto,id_marca,id_rubro,id_subrubro,id_linea,id_material,idOrigen,id_moneda']);
//        });
//        Schema::table('items_combo', function (Blueprint $table) {
//            $table->dropforeign(['id_item,id_itemcomponente']);
//        });
//        Schema::table('items_cross_sell', function (Blueprint $table) {
//            $table->dropforeign(['id_item,id_itemcross']);
//        });
//        Schema::table('items_up_sell', function (Blueprint $table) {
//            $table->dropforeign(['id_item,id_itemup']);
//        });
//        Schema::table('items_proveedores', function (Blueprint $table) {
//            $table->dropforeign(['id_item,id_titular']);
//        });
//        Schema::table('notificaciones_estados', function (Blueprint $table) {
//            $table->dropforeign(['id_notificacion,id_user,id_estado']);
//        });
//        Schema::table('numeradores', function (Blueprint $table) {
//            $table->dropforeign(['id_tipocpte,id_sucursal']);
//        });
//        Schema::table('sucursales', function (Blueprint $table) {
//            $table->dropforeign(['id_localidad']);
//        });
//        Schema::table('sys_numcaja', function (Blueprint $table) {
//            $table->dropforeign(['id_sucursal,id_user']);
//        });
    }
}
