<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter2ComprobantesMPTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comprobantes_mp', function (Blueprint $table) {
            $table->dropColumn('referencia');
            $table->integer('cuotas')->after('id_mediopago')->nullable();
        });
    }
}
