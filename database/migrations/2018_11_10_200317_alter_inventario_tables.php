<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInventarioTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inv_stock', function (Blueprint $table) {
            $table->integer('id_movstk')->references('id')->on('inv_movimientos_stock')->after('cantidad');
        });
        Schema::table('comprobantes_items', function (Blueprint $table) {
            $table->integer('id_movstk')->references('id')->on('inv_movimientos_stock')->after('importe')->nullable();
        });
        Schema::table('inv_movimientos_stock', function (Blueprint $table) {
            $table->integer('id_motivo')->references('id')->on('sys_motivos_mstock')->after('id');
            $table->integer('id_cpteitem')->references('id')->on('comprobantes_items')->after('id_motivo')->nullable();
        });
    }
}
