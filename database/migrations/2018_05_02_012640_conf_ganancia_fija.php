<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConfGananciaFija extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('conf_ganancia_fija', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('id_tipocliente')->references('id')->on('sys_tipoclientes');
        $table->decimal('importe_fijo', 10, 2)->nullable();
        $table->decimal('porcentaje', 10, 2)->nullable();
        $table->boolean('activo')->default(0);
        $table->timestamps();
      });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('conf_ganancia_fija');
    }
}
