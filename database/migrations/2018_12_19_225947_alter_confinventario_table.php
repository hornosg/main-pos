<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterConfinventarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inv_sectores', function (Blueprint $table) {
            $table->string('alias')->after('id_sucursal')->nullable();
        });
        Schema::table('inv_ubicaciones', function (Blueprint $table) {
            $table->string('alias')->after('id_sector')->nullable();
        });
    }
}
