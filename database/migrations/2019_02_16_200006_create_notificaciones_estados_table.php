<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacionesEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificaciones_estados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_notificacion')->references('id')->on('notificaciones')->nullable();
            $table->integer('id_user')->references('id')->on('users')->nullable();
            $table->integer('id_estado')->references('id')->on('sys_estados')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificaciones_estados');
    }
}
