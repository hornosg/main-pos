<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOldsidsScript extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->integer('oldid')->nullable();
        });
        Schema::table('comprobantes', function (Blueprint $table) {
            $table->integer('oldid')->nullable();
        });
        Schema::table('titulares', function (Blueprint $table) {
            $table->integer('oldid')->nullable();
        });
    }
}
