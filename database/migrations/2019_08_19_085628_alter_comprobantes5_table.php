<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterComprobantes5Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comprobantes', function (Blueprint $table) {
            $table->decimal('saldototal', 10, 2)->after('total')->nullable();
            $table->decimal('saldocpte', 10, 2)->after('total')->nullable();
        });
    }
}
