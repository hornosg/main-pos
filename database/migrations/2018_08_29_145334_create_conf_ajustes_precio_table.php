<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfAjustesPrecioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conf_ajustes_precio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->string('nota');
            $table->decimal('porcentaje', 10, 2)->nullable();
            $table->integer('signo');
            $table->boolean('activo')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conf_ajustes_precio');
    }
}
