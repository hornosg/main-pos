<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW stock_view AS
                        (
                            SELECT i.id as item,
                                   su.id as sucursal,
                                   se.id as id_sector,
                                   ub.id as id_ubicacion,
                                   po.id as id_posicion,
                                   sum(s.cantidad) as cantidad
                             FROM inv_stock s
                             JOIN items i on i.id = s.id_item
                             JOIN inv_sectores se on se.id= s.id_sector
                             JOIN sucursales su on su.id= se.id_sucursal
                             LEFT JOIN inv_ubicaciones ub on ub.id= s.id_ubicacion
                             LEFT JOIN inv_posiciones po on po.id= s.id_posicion
                            WHERE s.id_sector IS NOT NULL
                            GROUP BY 1,2,3,4,5
                            HAVING sum(cantidad) > 0
                        )");
    }

    public function down()
    {
    //DB::statement('DROP VIEW IF EXISTS stock_view');
        Schema::dropIfExists('stock_view');
    }
}
