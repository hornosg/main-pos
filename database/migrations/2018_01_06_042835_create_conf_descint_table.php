<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfDescintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conf_descint', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->integer('id_tipocliente')->references('id')->on('sys_tipoclientes');
            $table->integer('id_mediopago')->references('id')->on('conf_medios_pago')->nullable();
            $table->decimal('importe_fijo', 10, 2)->nullable();
            $table->decimal('porcentaje', 10, 2)->nullable();
            $table->integer('signo');
            $table->boolean('activo')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conf_descint');
    }
}
