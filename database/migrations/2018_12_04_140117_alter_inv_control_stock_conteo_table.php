<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInvControlStockConteoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inv_control_stock_conteo', function (Blueprint $table) {
            $table->integer('id_ctrolstk')->references('id')->on('inv_control_stock')->after('id');
        });
    }
}
