<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysTipocomprobantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_tipocomprobantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->integer('id_tipotitular')->references('id')->on('sys_tipotitulares')->nullable();
            $table->boolean('activo')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_tipocomprobantes');
    }
}
