<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesMpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes_mp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cpte')->references('id')->on('comprobantes');
            $table->integer('id_mediopago')->references('id')->on('conf_medios_pagos');
            $table->string('numero_transaccion')->nullable();
            $table->string('referencia')->nullable();
            $table->date('fecha_cobro')->nullable();
            $table->date('fecha_vto')->nullable();
            $table->decimal('importe', 10, 2)->nullable();
			$table->timestamps();
			$table->integer('created_us')->references('id')->on('users')->nullable();
            $table->integer('updated_us')->references('id')->on('users')->nullable();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes_mp');
    }
}
