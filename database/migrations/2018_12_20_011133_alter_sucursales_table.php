<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSucursalesTable extends Migration
{
    public function up()
    {
        Schema::table('sucursales', function (Blueprint $table) {
            $table->string('alias')->after('descripcion')->nullable();
        });
    }
}
