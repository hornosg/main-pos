<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInventarios2Tables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inv_control_stock', function(Blueprint $table)
        {
            $table->integer('id_estado')->references('id')->on('sys_estados')->after('cantidad');
            $table->softDeletes();
        });
        Schema::table('inv_movimientos_stock', function(Blueprint $table)
        {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inv_control_stock', function(Blueprint $table)
        {
            $table->dropSoftDeletes();
        });
        Schema::table('inv_movimientos_stock', function(Blueprint $table)
        {
            $table->dropSoftDeletes();
        });
    }
}
