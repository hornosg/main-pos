<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesEnvioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes_envio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cpte')->references('id')->on('comprobantes');
            $table->dateTime('fecha');
            $table->string('direccion');
            $table->string('observaciones');
			$table->timestamps();
			$table->integer('created_us')->references('id')->on('users')->nullable();
            $table->integer('updated_us')->references('id')->on('users')->nullable();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes_envio');
    }
}
