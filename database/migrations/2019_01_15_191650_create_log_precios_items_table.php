<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogPreciosItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_precios_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_log');
            $table->unsignedInteger('id_item');
            $table->decimal('oldprecioLista', 10, 2)->nullable();
            $table->decimal('oldbonifProv', 10, 2)->nullable();
            $table->decimal('oldganancia', 10, 2)->nullable();
            $table->decimal('newprecioLista', 10, 2)->nullable();
            $table->decimal('newbonifProv', 10, 2)->nullable();
            $table->decimal('newganancia', 10, 2)->nullable();
            $table->timestamps();

            $table->foreign('id_log')->references('id')->on('log_precios');
            $table->foreign('id_item')->references('id')->on('items');
            $table->index('id_log','idx_log_precios_items1');
            $table->index('id_item','idx_log_precios_items2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_precios_items');
    }
}
