<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDeletedatTables extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('titulares', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('sucursales', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('items', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('titulares', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('sucursales', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }
}
