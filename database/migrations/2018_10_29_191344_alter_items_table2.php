<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItemsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->integer('id_moneda')->references('id')->on('sys_monedas')->after('stockMinimo')->default(1);
            $table->integer('iva')->after('bonifProv')->default(21);
        });
    }
}
