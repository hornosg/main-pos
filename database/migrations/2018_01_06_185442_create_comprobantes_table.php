<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipocpte')->references('id')->on('sys_tipocomprobantes');
            $table->integer('id_titular')->references('id')->on('titulares')->nullable();
            $table->integer('id_caja')->nullable();
            $table->integer('id_recargo')->references('id')->on('conf_recargos')->nullable();
            $table->integer('id_facturaelectronica')->nullable();
            $table->decimal('total', 10, 2)->nullable();
            $table->string('observaciones')->nullable();
            $table->integer('id_cpterel')->nullable();
            $table->boolean('id_estado')->nullable();
            $table->timestamps();
			$table->integer('created_us')->references('id')->on('users')->nullable();
            $table->integer('updated_us')->references('id')->on('users')->nullable();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes');
    }
}
