<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_moneda');
            $table->unsignedInteger('id_sucursal');
            $table->decimal('cambio', 10, 2);
            $table->unsignedInteger('id_user');
            $table->timestamps();

            $table->foreign('id_moneda')->references('id')->on('sys_monedas');
            $table->foreign('id_sucursal')->references('id')->on('sucursales');
            $table->foreign('id_user')->references('id')->on('users');
            $table->index(['id_moneda','id_sucursal'],'idx_cotizaciones1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizaciones');
    }
}
