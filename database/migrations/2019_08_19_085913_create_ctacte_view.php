<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCtacteView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW ctacte_view AS
                         SELECT c.id AS id,
                                c.id_tipocpte AS id_tipocpte,
                                c.id_titular AS id_titular,
                                c.numero AS numero,
                                c.id_caja AS id_caja,
                                c.id_facturaelectronica AS id_facturaelectronica,
                                c.total AS total,
                                c.observaciones AS observaciones,
                                c.id_cpterel AS id_cpterel,
                                c.id_estado AS id_estado,
                                DATE_FORMAT(c.created_at, \"%d/%m/%Y %H:%i:%s\") AS creado,
                                tc.descripcion AS tipo,
                                c.total AS saldo,
                                saldocpte,
                                saldototal,
                                t.descripcion AS cliente,
                                us.name AS usuario,
                                e.descripcion AS estado,
                                (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) AS pasaron
                           FROM comprobantes c
                           JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                      LEFT JOIN titulares t ON t.id = c.id_titular
                      LEFT JOIN users us ON us.id = c.created_us
                      LEFT JOIN sys_tipocomprobantes tc ON tc.id = c.id_tipocpte
                      LEFT JOIN sys_estados e ON e.id = c.id_estado
                          WHERE (
                                  ( (c.id_tipocpte=5) AND (c.id_cpterel IS NULL) ) OR
                                  (c.id_tipocpte=6) OR
                                  ( (c.id_tipocpte=3) AND (c.id_cpterel IS NULL) ) OR
                                  ( (c.id_tipocpte=3) AND ( (SELECT id_estado FROM comprobantes WHERE id_tipocpte=14 AND id=c.id_cpterel)=3 ) )
                                 )
                            AND c.deleted_at IS NULL
                            AND c.id_titular not in (1,2)
                          UNION
                         SELECT c.id AS id,
                                c.id_tipocpte AS id_tipocpte,
                                c.id_titular AS id_titular,
                                c.numero AS numero,
                                c.id_caja AS id_caja,
                                c.id_facturaelectronica AS id_facturaelectronica,
                                c.total AS total,
                                c.observaciones AS observaciones,
                                c.id_cpterel AS id_cpterel,
                                c.id_estado AS id_estado,
                                DATE_FORMAT(c.created_at, \"%d/%m/%Y %H:%i:%s\") AS creado,
                                tc.descripcion AS tipo,
                                (c.total * -(1)) AS saldo,
                                saldocpte,
                                saldototal,
                                t.descripcion AS cliente,
                                us.name AS usuario,
                                e.descripcion AS estado,
                                (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) AS pasaron
                           FROM comprobantes c
                      LEFT JOIN titulares t ON t.id = c.id_titular
                      LEFT JOIN users us ON us.id = c.created_us
                      LEFT JOIN sys_tipocomprobantes tc ON tc.id = c.id_tipocpte
                      LEFT JOIN sys_estados e ON e.id = c.id_estado
                          WHERE c.id_tipocpte IN (7,13,14)
                            AND c.deleted_at IS NULL
                            AND c.id_estado = 3
                            AND c.id_titular not in (1,2)
                      ORDER BY id ");
    }

    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS ctacte_view');
    }
}
