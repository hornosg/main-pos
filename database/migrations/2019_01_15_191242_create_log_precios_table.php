<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogPreciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_precios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipoactualizacion')->comments('1 Import, 2 Masiva, 3 Manual Comparativa, 4 Por Producto');
            $table->unsignedInteger('id_titular')->nullable();;
            $table->unsignedInteger('id_marca')->nullable();;
            $table->unsignedInteger('id_rubro')->nullable();;
            $table->unsignedInteger('id_subrubro')->nullable();;
            $table->unsignedInteger('id_linea')->nullable();;
            $table->unsignedInteger('id_material')->nullable();;
            $table->decimal('porcentaje_preciolista', 10, 2)->nullable();
            $table->decimal('porcentaje_ganancia', 10, 2)->nullable();
            $table->integer('signo')->comments('1 AUMENTO, -1 DESCUENTO')->nullable();
            $table->string('archivo')->nullable();
            $table->timestamps();
            $table->integer('created_us')->references('id')->on('users')->nullable();

            $table->foreign('id_titular')->references('id')->on('titulares');
            $table->foreign('id_marca')->references('id')->on('conf_marcas');
            $table->foreign('id_rubro')->references('id')->on('conf_rubros');
            $table->foreign('id_subrubro')->references('id')->on('conf_subrubros');
            $table->foreign('id_linea')->references('id')->on('conf_lineas');
            $table->foreign('id_material')->references('id')->on('conf_materiales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_precios');
    }
}
