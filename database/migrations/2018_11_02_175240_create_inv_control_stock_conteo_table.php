<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvControlStockConteoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_control_stock_conteo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_item')->references('id')->on('items')->nullable();
            $table->integer('id_sector')->references('id')->on('inv_sectores');
            $table->integer('id_ubicacion')->references('id')->on('inv_ubicaciones')->nullable();
            $table->integer('id_posicion')->references('id')->on('inv_posiciones')->nullable();
            $table->decimal('cantidad_fisica', 10, 1)->nullable();
            $table->decimal('cantidad_stock', 10, 1)->nullable();
            $table->integer('id_movimiento')->references('id')->on('inv_movimientos_stock')->nullable();
            $table->string('observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_control_stock_conteo');
    }
}
