<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSucursalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->boolean('activo')->default(0);
            $table->boolean('webvisible')->default(0);
            $table->string('direccion')->nullable();;
            $table->integer('id_localidad')->references('id')->on('conf_localidades')->nullable();
            $table->string('diasyhorarios')->nullable();;
            $table->string('telefono')->nullable();;
            $table->string('coordenadas')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sucursales');
    }
}
