<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->integer('id_tipoitem')->references('id')->on('sys_tipoitems');
            $table->string('ean')->nullable();
            $table->integer('id_unidadventa')->references('id')->on('conf_unidades_medida')->nullable();
            $table->decimal('contNeto', 10, 2)->nullable();
            $table->integer('id_unidadcontneto')->references('id')->on('conf_unidades_medida')->nullable();
            $table->decimal('peso', 10, 2)->nullable();
            $table->decimal('largo', 10, 2)->nullable();
            $table->decimal('ancho', 10, 2)->nullable();
            $table->decimal('alto', 10, 2)->nullable();
            $table->integer('id_marca')->references('id')->on('conf_marcas')->nullable();
            $table->integer('id_rubro')->references('id')->on('conf_rubros')->nullable();
            $table->integer('id_linea')->references('id')->on('conf_lineas')->nullable();
            $table->integer('id_material')->references('id')->on('conf_materiales')->nullable();
            $table->integer('idOrigen')->references('id')->on('conf_origen')->nullable();
            $table->decimal('puntoRepocicion', 10, 2)->nullable();
            $table->decimal('stockMinimo', 10, 2)->nullable();
            $table->decimal('precioLista', 10, 2)->nullable();
            $table->decimal('bonifProv', 10, 2)->nullable();
            $table->decimal('precioCosto', 10, 2)->nullable();
            $table->decimal('ganancia', 10, 2)->nullable();
            $table->decimal('precio', 10, 2)->nullable();
            $table->string('especificaciones')->nullable();
            $table->string('observaciones')->nullable();
            $table->boolean('activo')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
