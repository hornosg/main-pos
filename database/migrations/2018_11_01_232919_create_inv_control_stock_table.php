<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvControlStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_control_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_marca')->references('id')->on('conf_marcas')->nullable();
            $table->integer('id_rubro')->references('id')->on('conf_rubros')->nullable();
            $table->integer('id_subrubro')->references('id')->on('conf_subrubros')->nullable();
            $table->integer('id_item')->references('id')->on('items')->nullable();
            $table->integer('id_sector')->references('id')->on('inv_sectores')->nullable();
            $table->integer('id_ubicacion')->references('id')->on('inv_ubicaciones')->nullable();
            $table->integer('id_posicion')->references('id')->on('inv_posiciones')->nullable();
            $table->decimal('cantidad', 10, 1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_control_stock');
    }
}
