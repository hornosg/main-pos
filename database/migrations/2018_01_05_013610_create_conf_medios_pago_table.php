<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfMediosPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_medios_pago', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->boolean('cuotas')->default(0);
            $table->boolean('identificador')->default(0);
            $table->boolean('fecha_cobro')->default(0);
            $table->boolean('fecha_vto')->default(0);
            $table->boolean('activo')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_medios_pago');
    }
}
