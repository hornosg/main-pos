<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterComprobantesMPTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comprobantes_mp', function (Blueprint $table) {
            $table->integer('id_banco')->after('numero_transaccion')->references('id')->on('conf_bancos')->nullable();
        });
    }
}
