<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysNumcajaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_numcaja', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('id_sucursal')->references('id')->on('sys_tipoclientes');
            $table->dateTime('fechahora_apertura')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('fechahora_cierre')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('id_user')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_numcaja');
    }
}
