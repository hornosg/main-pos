<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOldid2Script extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conf_marcas', function (Blueprint $table) {
            $table->integer('oldid')->nullable();
        });
        Schema::table('conf_subrubros', function (Blueprint $table) {
            $table->integer('oldid')->nullable();
        });
    }
}
