<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvMovimientosStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_movimientos_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_item')->references('id')->on('items');
            $table->integer('id_sector_o')->references('id')->on('inv_sectores')->nullable();
            $table->integer('id_ubicacion_o')->references('id')->on('inv_ubicaciones')->nullable();
            $table->integer('id_posicion_o')->references('id')->on('inv_posiciones')->nullable();
            $table->integer('id_sector_d')->references('id')->on('inv_sectores')->nullable();
            $table->integer('id_ubicacion_d')->references('id')->on('inv_ubicaciones')->nullable();
            $table->integer('id_posicion_d')->references('id')->on('inv_posiciones')->nullable();
            $table->decimal('cantidad', 10, 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_movimientos_stock');
    }
}
