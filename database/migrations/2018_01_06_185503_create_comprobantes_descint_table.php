<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesDescintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes_descint', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cpte')->references('id')->on('comprobantes');
            $table->integer('id_descint')->references('id')->on('conf_descint');
            $table->decimal('importe', 10, 2)->nullable();
			$table->timestamps();
			$table->integer('created_us')->references('id')->on('users')->nullable();
            $table->integer('updated_us')->references('id')->on('users')->nullable();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes_descint');
    }
}
