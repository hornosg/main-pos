<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_proveedores', function (Blueprint $table) {
            $table->integer('id_item')->references('id')->on('items');
            $table->integer('id_titular')->references('id')->on('titulares');
            $table->string('codproducto');
        });

        Schema::table('items_proveedores', function (Blueprint $table) {
            $table->unique(['id_item','id_titular'],'idx_items_proveedores1');
            $table->index('id_item','idx_items_proveedores2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_proveedores');
    }
}
