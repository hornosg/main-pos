<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTitularesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('titulares', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->integer('id_tipotitular')->references('id')->on('sys_tipotitulares');
            $table->integer('id_tipocliente')->references('id')->on('sys_tipoclientes')->nullable();
            $table->integer('plan')->references('id')->on('sys_planes')->nullable();
            $table->string('logo')->nullable();
            $table->string('cuit')->nullable();
            $table->string('direccion')->nullable();
            $table->string('telefono')->nullable();
            $table->string('web')->nullable();
            $table->string('anotaciones')->nullable();
            $table->boolean('activo')->default(0);
            $table->timestamp('fecha_cumple')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('titulares');
    }
}
