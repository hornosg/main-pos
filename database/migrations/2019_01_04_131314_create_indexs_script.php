<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexsScript extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conf_marcas', function (Blueprint $table) {
            $table->index('oldid','idx_conf_marcas1');
        });
        Schema::table('conf_subrubros', function (Blueprint $table) {
            $table->index('oldid','idx_conf_subrubros1');
        });
        Schema::table('titulares', function (Blueprint $table) {
            $table->index('oldid','idx_titulares1');
        });
        Schema::table('items', function (Blueprint $table) {
            $table->index('oldid','idx_items1');
            $table->index(['id_tipoitem','activo'],'idx_items2');
        });
        Schema::table('comprobantes', function (Blueprint $table) {
            $table->index(['id_titular','id_tipocpte', 'id'],'idx_comprobantes5');
            $table->index(['id_titular','id_tipocpte'],'idx_comprobantes6');
            $table->index(['created_at'],'idx_comprobantes7');
            $table->index('oldid','idx_comprobantes8');
        });
        Schema::table('comprobantes_mp', function (Blueprint $table) {
            $table->index(['id_cpte'],'idx_comprobantes_mp1');
            $table->index(['id_cpte','id_mediopago'],'idx_comprobantes_mp2');
        });
        Schema::table('comprobantes_items', function (Blueprint $table) {
            $table->index(['id_cpte'],'idx_comprobantes_items8');
            $table->index(['id_item'],'idx_comprobantes_items9');
        });
    }
}
