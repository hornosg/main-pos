<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSysMediosPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_medios_pago', function (Blueprint $table) {
            $table->boolean('id_banco')->after('fecha_vto')->default(0);
        });
    }
}
