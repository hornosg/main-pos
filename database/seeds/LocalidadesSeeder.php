<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Localidad;

class LocalidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'LUJAN','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'JAUREGUI','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'CORTINES','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'OLIVERA','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'PUEBLO NUEVO','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'CAPITAL FEDERAL','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'LORETO','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'BECCAR','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'HURLINGHAM','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'HAEDO','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'S.A. DE PADUA','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'SAN MARTIN','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'MERCEDES','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'DEL VISO','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'VIRREY DEL PINO','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'CASTELAR','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'CARDALES','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'VICENTE LOPEZ','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'GRAL RODRIGUEZ','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'LA REJA','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'TANDIL','id_provincia'=>1,'activo'=>true],
            ['descripcion'=>'MORENO','id_provincia'=>1,'activo'=>true]
        ];

        foreach($items as $item){
            Localidad::create($item);
        }
    }
}
