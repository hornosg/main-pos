<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Plan;

class PlanesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'BASIC','activo'=>true ],
            ['descripcion'=>'ADVANCED','activo'=>true ],
            ['descripcion'=>'ENTERPRISE','activo'=>true ],
            ['descripcion'=>'ENTERPRISE PLUS','activo'=>true ]
        ];

        foreach($items as $item){
            Plan::create($item);
        }
    }
}
