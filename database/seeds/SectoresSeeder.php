<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Sector;

class SectoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'RECEPCION','id_sucursal'=>1,'alias'=>'S1-REC','activo'=>true],
            ['descripcion'=>'SALON DE VENTAS','id_sucursal'=>1,'alias'=>'S1-SVE','activo'=>true]
        ];

        foreach($items as $item){
            Sector::create($item);
        }
    }
}
