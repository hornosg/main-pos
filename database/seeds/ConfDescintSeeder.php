<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\DescInt;

class ConfDescintSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('conf_descint')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'8% EFECTIVO','id_tipocliente'=>1,'id_mediopago'=>1,'porcentaje'=>8,'signo'=>-1,'activo'=>true ],
            ['descripcion'=>'8% EFECTIVO','id_tipocliente'=>4,'id_mediopago'=>1,'porcentaje'=>8,'signo'=>-1,'activo'=>true ],
            ['descripcion'=>'10% INTERES EN 2 CUOTAS','id_tipocliente'=>1,'id_mediopago'=>4,'cuotas'=>2,'porcentaje'=>10,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'10% INTERES EN 2 CUOTAS','id_tipocliente'=>2,'id_mediopago'=>4,'cuotas'=>2,'porcentaje'=>10,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'10% INTERES EN 2 CUOTAS','id_tipocliente'=>3,'id_mediopago'=>4,'cuotas'=>2,'porcentaje'=>10,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'10% INTERES EN 2 CUOTAS','id_tipocliente'=>4,'id_mediopago'=>4,'cuotas'=>2,'porcentaje'=>10,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'13% INTERES EN 3 CUOTAS','id_tipocliente'=>1,'id_mediopago'=>4,'cuotas'=>3,'porcentaje'=>13,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'13% INTERES EN 3 CUOTAS','id_tipocliente'=>2,'id_mediopago'=>4,'cuotas'=>3,'porcentaje'=>13,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'13% INTERES EN 3 CUOTAS','id_tipocliente'=>3,'id_mediopago'=>4,'cuotas'=>3,'porcentaje'=>13,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'13% INTERES EN 3 CUOTAS','id_tipocliente'=>4,'id_mediopago'=>4,'cuotas'=>3,'porcentaje'=>13,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'17% INTERES EN 4 CUOTAS','id_tipocliente'=>1,'id_mediopago'=>4,'cuotas'=>4,'porcentaje'=>14,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'17% INTERES EN 4 CUOTAS','id_tipocliente'=>2,'id_mediopago'=>4,'cuotas'=>4,'porcentaje'=>14,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'17% INTERES EN 4 CUOTAS','id_tipocliente'=>3,'id_mediopago'=>4,'cuotas'=>4,'porcentaje'=>14,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'17% INTERES EN 4 CUOTAS','id_tipocliente'=>4,'id_mediopago'=>4,'cuotas'=>4,'porcentaje'=>14,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'20% INTERES EN 5 CUOTAS','id_tipocliente'=>1,'id_mediopago'=>4,'cuotas'=>5,'porcentaje'=>20,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'20% INTERES EN 5 CUOTAS','id_tipocliente'=>2,'id_mediopago'=>4,'cuotas'=>5,'porcentaje'=>20,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'20% INTERES EN 5 CUOTAS','id_tipocliente'=>3,'id_mediopago'=>4,'cuotas'=>5,'porcentaje'=>20,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'20% INTERES EN 5 CUOTAS','id_tipocliente'=>4,'id_mediopago'=>4,'cuotas'=>5,'porcentaje'=>20,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'24% INTERES EN 6 CUOTAS','id_tipocliente'=>1,'id_mediopago'=>4,'cuotas'=>6,'porcentaje'=>24,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'24% INTERES EN 6 CUOTAS','id_tipocliente'=>2,'id_mediopago'=>4,'cuotas'=>6,'porcentaje'=>24,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'24% INTERES EN 6 CUOTAS','id_tipocliente'=>3,'id_mediopago'=>4,'cuotas'=>6,'porcentaje'=>24,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'24% INTERES EN 6 CUOTAS','id_tipocliente'=>4,'id_mediopago'=>4,'cuotas'=>6,'porcentaje'=>24,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'16% INTERES EN 12 CUOTAS','id_tipocliente'=>1,'id_mediopago'=>4,'cuotas'=>12,'porcentaje'=>16,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'16% INTERES EN 12 CUOTAS','id_tipocliente'=>2,'id_mediopago'=>4,'cuotas'=>12,'porcentaje'=>16,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'16% INTERES EN 12 CUOTAS','id_tipocliente'=>3,'id_mediopago'=>4,'cuotas'=>12,'porcentaje'=>16,'signo'=>1,'activo'=>true ],
            ['descripcion'=>'16% INTERES EN 12 CUOTAS','id_tipocliente'=>4,'id_mediopago'=>4,'cuotas'=>12,'porcentaje'=>16,'signo'=>1,'activo'=>true ],
       ];

        foreach($items as $item){
            DescInt::create($item);
        }
    }
}
