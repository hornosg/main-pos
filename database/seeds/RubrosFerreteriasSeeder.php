<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Rubro;

class RubrosFerreteriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'FERRETERIA','activo'=>true],
            ['descripcion'=>'PINTURERIA','activo'=>true],
            ['descripcion'=>'MAT.ELECTRICOS','activo'=>true],
            ['descripcion'=>'ILUMINACION','activo'=>true],
            ['descripcion'=>'SANITARIOS','activo'=>true],
            ['descripcion'=>'MAQUINARIAS','activo'=>true],
            ['descripcion'=>'CONSTRUCCION','activo'=>true],
            ['descripcion'=>'CONSTRUCCION EN SECO','activo'=>true],
            ['descripcion'=>'AGUA','activo'=>true],
            ['descripcion'=>'GAS','activo'=>true],
            ['descripcion'=>'HERRAMIENTAS','activo'=>true],
            ['descripcion'=>'SEG.Y ROPA DE TRABAJO','activo'=>true],
            ['descripcion'=>'BULONERIA','activo'=>true],
            ['descripcion'=>'CALEFACCION Y VENTILACION','activo'=>true],
            ['descripcion'=>'ACCESORIOS PARA AUTOS','activo'=>true]
        ];

        foreach($items as $item){
            Rubro::create($item);
        }
    }
}
