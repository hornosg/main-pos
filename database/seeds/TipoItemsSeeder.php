<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\TipoItem;

class TipoItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'SERVICIOS PRESTADOS','activo'=>true ],
            ['descripcion'=>'SERVICIOS CONSUMIDOS','activo'=>true ],
            ['descripcion'=>'INSUMOS VARIOS','activo'=>true ],
            ['descripcion'=>'MATERIAS PRIMAS','activo'=>true ],
            ['descripcion'=>'PRODUCTOS','activo'=>true ],
            ['descripcion'=>'PRODUCTOS COMBO','activo'=>true ],
            ['descripcion'=>'BIENES DE USO','activo'=>true ],
            ['descripcion'=>'INVERSIONES','activo'=>true ]
        ];

        foreach($items as $item){
            TipoItem::create($item);
        }
    }
}
