<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Caja;

class CajasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cajas')->truncate();
        Model::unguard();
        $items = [
            ['id'=>1,'id_sucursal'=>1],
            ['id'=>1000,'id_sucursal'=>1]
        ];

        foreach($items as $item){
            Caja::create($item);
        }
    }
}
