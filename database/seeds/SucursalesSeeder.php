<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Sucursal;

class SucursalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('cotizaciones')->truncate();
//        DB::table('sucursales')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'SUCURSAL 1','alias'=>'S1','activo'=>true,'webvisible'=>false,'direccion'=>'...','id_localidad'=>1]
            //,['descripcion'=>'SUCURSAL 2','alias'=>'S1','activo'=>true,'webvisible'=>false,'direccion'=>'...','id_localidad'=>1]
        ];

        foreach($items as $item){
            Sucursal::create($item);
        }

    }
}
