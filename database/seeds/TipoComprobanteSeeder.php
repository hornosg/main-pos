<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\TipoComprobante;

class TipoComprobanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('sys_tipocomprobantes')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'ORDEN DE COMPRA','id_tipotitular'=>3,'activo'=>true ],
            ['descripcion'=>'RECEPCION','id_tipotitular'=>3,'activo'=>true ],
            ['descripcion'=>'NOTA DE DEBITO','id_tipotitular'=>3,'activo'=>true ],
            ['descripcion'=>'CANJE','id_tipotitular'=>3,'activo'=>true ],
            ['descripcion'=>'REMITO','id_tipotitular'=>2,'activo'=>true ],
            ['descripcion'=>'FACTURA','id_tipotitular'=>2,'activo'=>true ],
            ['descripcion'=>'RECIBO','id_tipotitular'=>2,'activo'=>true ],
            ['descripcion'=>'SALDO INICIAL','id_tipotitular'=>1,'activo'=>true ],
            ['descripcion'=>'AJUSTE POSITIVO','id_tipotitular'=>1,'activo'=>true ],
            ['descripcion'=>'AJUSTE NEGATIVO','id_tipotitular'=>1,'activo'=>true ],
            ['descripcion'=>'RETIRO','id_tipotitular'=>1,'activo'=>true ],
            ['descripcion'=>'GASTOS','id_tipotitular'=>1,'activo'=>true ],
            ['descripcion'=>'NOTA DE CREDITO','id_tipotitular'=>2,'activo'=>true ],
            ['descripcion'=>'COMISION','id_tipotitular'=>2,'activo'=>true ],
            ['descripcion'=>'INGRESO','id_tipotitular'=>1,'activo'=>true ],
            ['descripcion'=>'PRESUPUESTOS','id_tipotitular'=>2,'activo'=>true ],
            ['descripcion'=>'SIN USO','id_tipotitular'=>2,'activo'=>false ],
            ['descripcion'=>'PAGO','id_tipotitular'=>1,'activo'=>true ]
        ];

        foreach($items as $item){
            TipoComprobante::create($item);
        }
    }
}
