<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Titular;

class TitularSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::select('SET FOREIGN_KEY_CHECKS=0');
        DB::table('titulares')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'DEMO MAINPOS','id_tipotitular'=>1,'id_tipocliente'=>1,'plan'=>3,'logo'=>null,'id_tipoiva'=>null,'cuit'=>null,'direccion'=>'UGARTECHE 954','telefono'=>'1159514044','web'=>null,'anotaciones'=>null,'activo'=>true ],
            ['descripcion'=>'CONSUMIDOR FINAL','id_tipotitular'=>2,'id_tipocliente'=>1,'plan'=>null,'logo'=>null,'id_tipoiva'=>1,'cuit'=>null,'direccion'=>'ABCD 123','telefono'=>'123456789','web'=>null,'anotaciones'=>null,'activo'=>true ]
        ];

        foreach($items as $item){
            Titular::create($item);
        }
    }
}
