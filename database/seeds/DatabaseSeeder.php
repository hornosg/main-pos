<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(PlanesSeeder::class);
        $this->call(PaisesSeeder::class);
        $this->call(ProvinciasSeeder::class);
        $this->call(MonedasSeeder::class);

        $this->call(TipoClienteSeeder::class);
        $this->call(TipoComprobanteSeeder::class);
        $this->call(TipoItemsSeeder::class);
        $this->call(TipoIvaSeeder::class);
        $this->call(TipoTitularSeeder::class);
        $this->call(TitularSeeder::class);

        $this->call(EstadosSeeder::class);
        $this->call(BancosSeeder::class);
        $this->call(MediosPagoSeeder::class);
        $this->call(AjustesPrecioSeeder::class);
        $this->call(GananciaFijaSeeder::class);
        $this->call(ConfDescintSeeder::class);
        $this->call(NumeradoresSeeder::class);

        $this->call(SectoresSeeder::class);
        $this->call(MotivosMStockSeeder::class);

        $this->call(RolesSeeder::class);
        $this->call(UsuariosSeeder::class);
        $this->call(CajasSeeder::class);
        $this->call(SucursalesSeeder::class);
        $this->call(NumeradoresSeeder::class);

        Model::reguard();
    }
}
