<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Provincia;

class ProvinciasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'BUENOS AIRES','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'CATAMARCA','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'CHACO','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'CHUBUT','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'CÓRDOBA','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'CORRIENTES','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'ENTRE RÍOS','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'FORMOSA','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'JUJUY','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'LA PAMPA','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'LA RIOJA','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'MENDOZA','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'MISIONES','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'NEUQUÉN','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'RÍO NEGRO','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'SALTA','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'SAN JUAN','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'SAN LUIS','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'SANTA CRUZ','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'SANTA FE','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'SANTIAGO DEL ESTERO','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'TIERRA DEL FUEGO','id_pais'=>1,'activo'=>true],
            ['descripcion'=>'TUCUMÁN','id_pais'=>1,'activo'=>true]
        ];

        foreach($items as $item){
            Provincia::create($item);
        }
    }
}
