<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Rol;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'ADMINISTRADOR','activo'=>true ],
            ['descripcion'=>'ENCARGADO','activo'=>true ],
            ['descripcion'=>'VENDEDOR','activo'=>true ],
            ['descripcion'=>'STOCK','activo'=>true ],
            ['descripcion'=>'DISTRIBUIDOR','activo'=>true ]
        ];

        foreach($items as $item){
            Rol::create($item);
        }
    }
}
