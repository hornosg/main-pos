<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Numerador;

class NumeradoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['id_tipocpte'=>1,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>2,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>3,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>4,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>5,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>6,'serie'=>'A','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>6,'serie'=>'B','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>6,'serie'=>'C','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>7,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>8,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>9,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>10,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>11,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>12,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>13,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>14,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>15,'serie'=>'X','id_sucursal'=>1,'numero'=>1000],
            ['id_tipocpte'=>16,'serie'=>'X','id_sucursal'=>1,'numero'=>1000]
            //,['id_tipocpte'=>1,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>2,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>3,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>4,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>5,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>6,'serie'=>'A','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>6,'serie'=>'B','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>6,'serie'=>'C','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>7,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>8,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>9,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>10,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>11,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>12,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>13,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>14,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>15,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
            //,['id_tipocpte'=>16,'serie'=>'X','id_sucursal'=>2,'numero'=>200000]
        ];

        foreach($items as $item){
            Numerador::create($item);
        }
    }
}
