<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\TipoIva;

class TipoIvaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sys_tipoiva')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'IVA RESPONSABLE INSCRIPTO','activo'=>true ],
            ['descripcion'=>'IVA RESPONSABLE NO INSCRIPTO','activo'=>true ],
            ['descripcion'=>'IVA NO RESPONSABLE','activo'=>true ],
            ['descripcion'=>'IVA SUJETO EXENTO','activo'=>true ],
            ['descripcion'=>'CONSUMIDOR FINAL','activo'=>true ],
            ['descripcion'=>'RESPONSABLE MONOTRIBUTO','activo'=>true ],
            ['descripcion'=>'SUJETO NO CATEGORIZADO','activo'=>true ],
            ['descripcion'=>'PROVEEDOR DEL EXTERIOR','activo'=>true ],
            ['descripcion'=>'CLIENTE DEL EXTERIOR','activo'=>true ],
            ['descripcion'=>'IVA LIBERADO – LEY Nº 19.640','activo'=>true ],
            ['descripcion'=>'IVA RESPONSABLE INSCRIPTO – AGENTE DE PERCEPCIÓN','activo'=>true ],
            ['descripcion'=>'PEQUEÑO CONTRIBUYENTE EVENTUAL','activo'=>true ],
            ['descripcion'=>'MONOTRIBUTISTA SOCIAL','activo'=>true ],
            ['descripcion'=>'PEQUEÑO CONTRIBUYENTE EVENTUAL SOCIAL','activo'=>true ]
        ];

        foreach($items as $item){
            TipoIva::create($item);
        }
    }
}
