<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\GananciaFija;

class GananciaFijaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['id_tipocliente'=>2,'importe_fijo'=>null,'porcentaje'=>11,'activo'=>true ],
            ['id_tipocliente'=>3,'importe_fijo'=>null,'porcentaje'=>20,'activo'=>true ]
        ];

        foreach($items as $item){
            GananciaFija::create($item);
        }
    }
}
