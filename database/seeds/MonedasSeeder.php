<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Moneda;

class MonedasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'PESO','activo'=>true],
            ['descripcion'=>'DOLAR','activo'=>true]
        ];

        foreach($items as $item){
            Moneda::create($item);
        }
    }
}
