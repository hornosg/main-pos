<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Estado;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sys_estados')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'PAGO','activo'=>true ],
            ['descripcion'=>'PENDIENTE','activo'=>true ],
            ['descripcion'=>'REGISTRADO','activo'=>true ],
            ['descripcion'=>'ACREDITADO','activo'=>true ],
            ['descripcion'=>'SIN INICIAR','activo'=>true ],
            ['descripcion'=>'EN PROCESO','activo'=>true ],
            ['descripcion'=>'CON DIFERENCIAS','activo'=>true ],
            ['descripcion'=>'RESUELTO','activo'=>true ],
            ['descripcion'=>'FINALIZADO','activo'=>true ],
            ['descripcion'=>'ACEPTADO','activo'=>true ],
            ['descripcion'=>'CERRADO','activo'=>true ],
            ['descripcion'=>'RECHAZADO','activo'=>true ],
            ['descripcion'=>'NO SE USA','activo'=>true ],
            ['descripcion'=>'SIN REVISAR','activo'=>true ],
            ['descripcion'=>'REVISADO','activo'=>true ]
        ];

        foreach($items as $item){
            Estado::create($item);
        }
    }
}
