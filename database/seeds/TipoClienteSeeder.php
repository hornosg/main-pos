<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\TipoCliente;

class TipoClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('sys_tipoclientes')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'EVENTUAL','activo'=>true ],
            ['descripcion'=>'MAYORISTA','activo'=>true ],
            ['descripcion'=>'EMPRESA','activo'=>true ],
            ['descripcion'=>'PROFESIONAL','activo'=>true ]
        ];

        foreach($items as $item){
            TipoCliente::create($item);
        }
    }
}
