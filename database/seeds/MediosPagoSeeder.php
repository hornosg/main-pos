<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\MedioPago;

class MediosPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'EFECTIVO','cuotas'=>false,'identificador'=>false,'fecha_cobro'=>false,'fecha_vto'=>false,'id_banco'=>false, 'activo'=>true ],
            ['descripcion'=>'CTA CTE','cuotas'=>false,'identificador'=>false,'fecha_cobro'=>false,'fecha_vto'=>false,'id_banco'=>false, 'activo'=>true ],
            ['descripcion'=>'T.DEBITO','cuotas'=>false,'identificador'=>true,'fecha_cobro'=>false,'fecha_vto'=>false,'id_banco'=>false, 'activo'=>true ],
            ['descripcion'=>'T.CREDITO','cuotas'=>true,'identificador'=>true,'fecha_cobro'=>false,'fecha_vto'=>false,'id_banco'=>false, 'activo'=>true ],
            ['descripcion'=>'CHEQUE','cuotas'=>false,'identificador'=>true,'fecha_cobro'=>true,'fecha_vto'=>true,'id_banco'=>false, 'activo'=>true ],
            ['descripcion'=>'DEPOSITO','cuotas'=>false,'identificador'=>true,'fecha_cobro'=>false,'fecha_vto'=>false,'id_banco'=>false, 'activo'=>true ],
            ['descripcion'=>'TRANSFERENCIA','cuotas'=>false,'identificador'=>true,'fecha_cobro'=>false,'fecha_vto'=>false,'id_banco'=>false, 'activo'=>true ]
        ];

        foreach($items as $item){
            MedioPago::create($item);
        }
    }
}
