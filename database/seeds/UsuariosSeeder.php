<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Sucursal;
use App\User;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('users')->truncate();
        Model::unguard();
        $items = [
            ['name'=>'hornosg',
                'full_name'=>'Gustavo Hornos',
                'email'=>'hornosg@gmail.com',
                'password' => bcrypt('01129981..'),
                'id_sucursal'=>1,
                'id_rol' => 1
            ],
            ['name'=>'guille',
                'full_name'=>'Guillermo Bulfero',
                'email'=>'pintureriayelectroexpress@gmail.com',
                'password' => bcrypt('mainpos..demo'),
                'id_sucursal'=>1,
                'id_rol' => 1
            ],
            ['name'=>'regina',
                'full_name'=>'ADMIN',
                'email'=>'regi@mainpos.com.ar',
                'password' => bcrypt('mainpos..demo'),
                'id_sucursal'=>1,
                'id_rol' => 1
            ],
            ['name'=>'encargado',
                'full_name'=>'ENCARGADO',
                'email'=>'encargado@mainpos.com.ar',
                'password' => bcrypt('mainpos..test'),
                'id_sucursal'=>1,
                'id_rol' => 2
            ],
            ['name'=>'vendedor',
                'full_name'=>'VENDEDOR',
                'email'=>'vendedor@mainpos.com.ar',
                'password' => bcrypt('mainpos..test'),
                'id_sucursal'=>1,
                'id_rol' => 3
            ],
//            ['name'=>'felipe',
//            'full_name'=>'Felipe Fernandez',
//            'email'=>'felipe-f@live.com.ar',
//            'password' => bcrypt('mainpos'),
//            'id_sucursal'=>1,
//            'id_rol' => 3
//            ],
//            ['name'=>'user3',
//                'full_name'=>'User 3',
//                'email'=>'user@live.com.ar',
//                'password' => bcrypt('mainpos'),
//                'id_sucursal'=>1,
//                'id_rol' => 3
//            ]
//            ['name'=>'guille',
//            'full_name'=>'Guillermo Bulfero',
//            'email'=>'test2@gmail.com',
//            'password' => bcrypt('mainpos'),
//            'id_sucursal'=>1,
//            'id_rol' => 1
//            ],
//            ['name'=>'seba',
//                'full_name'=>'Sebastian Carabajal',
//                'email'=>'test7@gmail.com',
//                'password' => bcrypt('mainpos'),
//                'id_sucursal'=>1,
//                'id_rol' => 2
//            ],
//            ['name'=>'pela',
//            'full_name'=>'Pablo Cabrera',
//            'email'=>'test4@gmail.com',
//            'password' => bcrypt('mainpos'),
//            'id_sucursal'=>1,
//            'id_rol' => 3
//            ],
//            ['name'=>'negro',
//                'full_name'=>'Enrique Fuentes',
//                'email'=>'test6@gmail.com',
//                'password' => bcrypt('mainpos'),
//                'id_sucursal'=>1,
//                'id_rol' => 2
//            ],
//            ['name'=>'marcosch',
//                'full_name'=>'marcosch',
//                'email'=>'test9@gmail.com',
//                'password' => bcrypt('mainpos'),
//                'id_sucursal'=>1,
//                'id_rol' => 3
//            ],
//            ['name'=>'nino',
//            'full_name'=>'WALDINO',
//            'email'=>'test5@gmail.com',
//            'password' => bcrypt('mainpos'),
//            'id_sucursal'=>1,
//            'id_rol' => 3
//            ]
          ];

        foreach($items as $item){
            User::create($item);
        }
    }
}
