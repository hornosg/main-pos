<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\MotivoMStock;

class MotivosMStockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sys_motivos_mstock')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'RECEPCION','interno'=>true,'activo'=>true ],
            ['descripcion'=>'MOVIMIENTO INTERNO','interno'=>false,'activo'=>true ],
            ['descripcion'=>'DIFERENCIA EN CONTROL STOCK','interno'=>true,'activo'=>true ],
            ['descripcion'=>'AJUSTE MANUAL (+)','interno'=>false,'activo'=>true ],
            ['descripcion'=>'AJUSTE MANUAL (-)','interno'=>false,'activo'=>true ],
            ['descripcion'=>'DEVOLUCION','interno'=>true,'activo'=>true ],
            ['descripcion'=>'VENTAS','interno'=>true,'activo'=>true ],
            ['descripcion'=>'STOCK INICIAL','interno'=>true,'activo'=>true ]
        ];

        foreach($items as $item){
            MotivoMStock::create($item);
        }
    }
}
