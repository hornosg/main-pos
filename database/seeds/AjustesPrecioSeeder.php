<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\AjustePrecio;

class AjustesPrecioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('conf_ajustes_precio')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'CDE','nota'=>'CINCO % DE DESCUENTO','porcentaje'=>5,'signo'=>-1,'activo'=>true ],
            ['descripcion'=>'DOF','nota'=>'DOMINGOS O FERIADOS','porcentaje'=>5,'signo'=>1,'activo'=>true ]
        ];

        foreach($items as $item){
            AjustePrecio::create($item);
        }
    }
}
