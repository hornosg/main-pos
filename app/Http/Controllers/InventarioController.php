<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sector;
use App\Models\Ubicacion;
use App\Models\Posicion;
use DB;

class InventarioController extends Controller
{
    public function index()
    {
        $costo = DB::select('SELECT ROUND(sum(cantidad*precioCosto),2) as costo
                               FROM inv_stock s
                                JOIN items i on i.id = s.id_item
                               WHERE s.id_sector is not null');
        $pventa = DB::select('SELECT ROUND(sum(cantidad*precio),2) as precio
                               FROM inv_stock s
                                JOIN items i on i.id = s.id_item
                               WHERE s.id_sector is not null');
        $cantidad = DB::select('SELECT ROUND(sum(cantidad),0) as cantidad
                               FROM inv_stock s
                               JOIN items i on i.id = s.id_item
                              WHERE s.id_sector is not null');

        $arrayrubros[0] = array('nodo','nodopadre','costo');
        $sucs = DB::select('select count(*) as cant from sucursales');
        if ($sucs[0]->cant > 1){
            array_push($arrayrubros,array('SUCURSAL',null,0));
            $nodo1='SELECT su.descripcion as nodo,"SUCURSAL" as nodopadre,ROUND(sum(cantidad*precioCosto),2) as costo
                                FROM inv_stock s
                                JOIN items i on i.id = s.id_item
                                JOIN inv_sectores se on se.id = s.id_sector
                                JOIN sucursales su on su.id = se.id_sucursal
                                where s.id_sector is not null
                                group by 1,2
                                union';
        }else{
            $nodo1='';
            $suc = DB::select('select * from sucursales');
            array_push($arrayrubros,array($suc[0]->descripcion,null,0));
        }
        $datarubros = DB::select($nodo1.'
                                SELECT r.descripcion as nodo,su.descripcion as nodopadre,ROUND(sum(cantidad*precioCosto),2) as costo
                                FROM inv_stock s
                                JOIN items i on i.id = s.id_item
                                JOIN conf_rubros r on r.id = i.id_rubro
                                JOIN inv_sectores se on se.id = s.id_sector
                                JOIN sucursales su on su.id = se.id_sucursal
                                where s.id_sector is not null
                                group by 1,2
                                union
                                SELECT sr.descripcion as nodo,r.descripcion as nodopadre,ROUND(sum(cantidad*precioCosto),2) as costo
                                FROM inv_stock s
                                JOIN items i on i.id = s.id_item
                                JOIN conf_subrubros sr on sr.id = i.id_subrubro
                                JOIN conf_rubros r on r.id = sr.id_rubro
                                where s.id_sector is not null
                                group by 1,2
        ');
        foreach( $datarubros as $item){
            $item->costo = (int) $item->costo;
            array_push($arrayrubros,array_values(collect($item)->toArray()));
        }

        return view('inventario',[
            'pventa' => $pventa[0]->precio,
            'costo' => $costo[0]->costo,
            'cantidad' => $cantidad[0]->cantidad,
            'rubros' => $arrayrubros,
            'itemnav' => 'resumen'
        ]);
    }

    public function confdep()
    {
        $sectores = DB::select('select se.*,s.descripcion as sucursal,
                                       CONCAT (se.descripcion, \' (\',s.alias,\')\') as descripcion2
                                    from inv_sectores se
                                    left join sucursales s on s.id=se.id_sucursal');
        $ubicaciones = DB::select('select u.*,s.alias as sector,
                                          CONCAT (u.descripcion, \' (\',s.alias,\')\') as descripcion2
                                    from inv_ubicaciones u
                                    left join inv_sectores s on s.id=u.id_sector');
        $posiciones = DB::select('select p.id, UPPER(p.descripcion) as descripcion, p.activo, UPPER(u.alias) as ubicacion
                                    from inv_posiciones p
                                    left join inv_ubicaciones u on u.id=p.id_ubicacion');
        return view('inventario/configdep',[
            'titulo' => 'Configuracion de Deposito',
            'itemnav' => 'confdep',
            'sectores' => $sectores,
            'ubicaciones' => $ubicaciones,
            'posiciones' => $posiciones
        ]);
    }
}
