<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Item;
use App\Models\Marca;
use App\Models\Rubro;
use App\Models\UnidadMedida;
use App\Models\Material;
use App\Models\Linea;
use App\Models\CrossSell;
use App\Models\UpSell;
use DB;
use Illuminate\Http\Request;
use Excel;

class InsumosController extends Controller
{
    //
    public function index()
    {
        $insumos = DB::select('select p.*,ma.descripcion as marca, concat_ws(\' \', p.contNeto, um.descripcion) as contenidoNeto
                                    from items p
                                    left join conf_marcas ma on ma.id=p.id_marca
                                    left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                    where id_tipoitem=3');
        return view('items.insumos.index', ['titulo' => 'Catalogo de Insumos','urlkey' => 'insumos','items' => $insumos]);
    }

    public function create()
    {
        $marcas = Marca::all();
        $unidadesm = UnidadMedida::all();
        $proveedores = DB::select("select * from titulares t where id_tipotitular=3 and activo=1");
        return view('items.insumos.create', [ 'titulo' => 'Catalogo de Insumos',
                                                'urlkey' => 'insumos',
                                                'item' => 'Producto',
                                                'unidadesm' => $unidadesm,
                                                'marcas' => $marcas,
                                                'proveedores'=>$proveedores]
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255'
        ]);

        //dd($request->request);
        $producto = new Item;
        $producto->descripcion = strtoupper($request->descripcion);
        $producto->id_tipoitem = $request->id_tipoitem;
        $producto->ean = $request->ean;
        $producto->id_unidadventa = $request->id_unidadventa;
        $producto->contNeto = $request->contNeto;
        $producto->id_unidadcontneto = $request->id_unidadcontneto;
        $producto->peso = $request->peso;
        $producto->largo = $request->largo;
        $producto->ancho = $request->ancho;
        $producto->alto = $request->alto;
        $producto->id_marca = $request->id_marca;
        $producto->id_rubro = $request->id_rubro;
        $producto->id_linea = $request->id_linea;
        $producto->id_material = $request->id_material;
        //$producto->esCombo = $request->esCombo;
        $producto->puntoRepocicion = $request->puntoRepocicion;
        $producto->stockMinimo = $request->stockMinimo;
        $producto->precioLista = $request->precioLista;
        $producto->bonifProv = $request->bonifProv;
        $producto->precioCosto = $request->precioCosto;
        $producto->ganancia = $request->ganancia;
        $producto->precio = $request->precio;
        $producto->especificaciones = $request->especificaciones;
        $producto->observaciones = $request->observaciones;

        if (isset($request->activo)){
            $producto->activo=1;
        }else{
            $producto->activo=0;
        }

        $producto->save();

        if(isset($request->provs)){
            $provsarray=json_decode($request->provs);
            foreach ($provsarray as $prov) {
                $r = DB::table('items_proveedores')
                    ->where([['id_item','=',$producto->id],
                        ['id_titular','=',$prov->idproveedor]])->get();
                if($r->count()===0)
                    DB::table('items_proveedores')->insert(
                        ['id_item' => $producto->id, 'id_titular' => $prov->idproveedor, 'codproducto' => $prov->codproducto ]
                    );
            }
        }

        if ($request->action=='save'){
            return redirect('/insumos')->with('status', 'Item Creado Exitosamente!');
        }else{
            return redirect('/insumos/create')->with('status', 'Item Creado Exitosamente!');
        }
    }

    public function edit($id)
    {
        $producto = Item::find($id);
        $marcas = Marca::all();
        $unidadesm = UnidadMedida::all();
        $proveedores = DB::select("select * from titulares t where id_tipotitular=3 and activo=1");
        $provs =  DB::select("select ip.*, p.id as idproveedor, p.descripcion as proveedor
                                from items_proveedores ip
                                left join titulares p on id_tipotitular=3 and p.id = ip.id_titular
                                where ip.id_item= ".$id);

        return view('items.insumos.show-edit',
                    ['titulo' => 'Catalogo de Insumos',
                    'urlkey' => 'insumos',
                    'producto' => $producto,
                    'unidadesm' => $unidadesm,
                    'marcas' => $marcas,
                    'proveedores' => $proveedores,
                    'provs' => $provs,
                    'edit'=> 1 ]
        );

    }

    public function show($id)
    {
        $producto = Item::find($id);

        $marcas = Marca::all();
        $unidadesm = UnidadMedida::all();
        $proveedores = DB::select("select * from titulares t where id_tipotitular=3 and activo=1");
        $provs =  DB::select("select ip.*, p.id as idproveedor, p.descripcion as proveedor
                                from items_proveedores ip
                                left join titulares p on id_tipotitular=3 and p.id = ip.id_titular
                                where ip.id_item= ".$id);
        return view('items.insumos.show-edit',
            ['titulo' => 'Catalogo de Insumos',
                'urlkey' => 'insumos',
                'item' => 'Producto',
                'producto' => $producto,
                'unidadesm' => $unidadesm,
                'marcas' => $marcas,
                'proveedores' => $proveedores,
                'provs' => $provs,
                'edit'=> 0 ]
        );
    }


    public function update($id,Request $request)
    {
        //dd($request->request);
        $producto = Item::find($id);
        $producto->descripcion = strtoupper($request->descripcion);
        $producto->id_tipoitem = $request->id_tipoitem;
        $producto->ean = $request->ean;
        $producto->id_unidadventa = $request->id_unidadventa;
        $producto->contNeto = $request->contNeto;
        $producto->id_unidadcontneto = $request->id_unidadcontneto;
        $producto->peso = $request->peso;
        $producto->largo = $request->largo;
        $producto->ancho = $request->ancho;
        $producto->alto = $request->alto;
        $producto->id_marca = $request->id_marca;
        $producto->id_rubro = $request->id_rubro;
        $producto->id_linea = $request->id_linea;
        $producto->id_material = $request->id_material;
        $producto->puntoRepocicion = $request->puntoRepocicion;
        $producto->stockMinimo = $request->stockMinimo;
        $producto->precioLista = $request->precioLista;
        $producto->bonifProv = $request->bonifProv;
        $producto->precioCosto = $request->precioCosto;
        $producto->ganancia = $request->ganancia;
        $producto->precio = $request->precio;
        $producto->especificaciones = $request->especificaciones;
        $producto->observaciones = $request->observaciones;

        if (isset($request->activo)){
            $producto->activo=1;
        }else{
            $producto->activo=0;
        }

        $producto->save();

        if(isset($request->provs)){
            $provsarray=json_decode($request->provs);
            foreach ($provsarray as $prov) {
                $r = DB::table('items_proveedores')
                    ->where([['id_item','=',$producto->id],
                        ['id_titular','=',$prov->idproveedor]])->get();
                if($r->count()===0)
                    DB::table('items_proveedores')->insert(
                        ['id_item' => $producto->id, 'id_titular' => $prov->idproveedor, 'codproducto' => $prov->codproducto ]
                    );
            }
        }

        return redirect('/insumos')->with('status', 'Item Actualizado!');

    }

    public function delete($id)
    {
        $producto = Item::find($id);
        $producto->delete();
        return redirect('/insumos')->with('status', 'Item Eliminado!');
    }

    public function removeselected($idcodprod)
    {
        $delete=DB::table('items_proveedores')->where('codproducto', $idcodprod)->delete();
        return $delete;
    }
}
