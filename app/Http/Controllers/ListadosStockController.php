<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sector;
use App\Models\Ubicacion;
use App\Models\Posicion;
use App\Models\MovimientoStock;
use App\Models\Stock;
use Dompdf\Dompdf;
use DB;

class ListadosStockController extends Controller
{
    public function index()
    {
        $stock = DB::select('SELECT 	distinct(item) as id,
                                        concat("[",p.id,"] ",p.descripcion) as  descripcion,
                                        ma.descripcion as marca,
                                        ru.descripcion as rubro,
                                        sr.descripcion as subrubro,
                                        concat_ws(" " , p.contNeto, um.descripcion) as contenidoNeto,
                                        precioCosto as costo,
                                        precio,
                                        sum(cantidad) as cantidad,
                                        stockMinimo,
                                        CASE
                                            WHEN sum(cantidad) > stockMinimo THEN "SOBRE STOCK"
                                            WHEN sum(cantidad) = stockMinimo THEN "STOCK MINIMO"
                                            WHEN sum(cantidad) < stockMinimo THEN "EN FALTA"
                                        END as status
                                FROM stock_view s
                                join items p on p.id=s.item
                                left join conf_marcas ma on ma.id=p.id_marca
                                left join conf_rubros ru on ru.id=p.id_rubro
                                left join conf_subrubros sr on sr.id=p.id_subrubro
                                left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                where id_sector is not null
                                and p.id_tipoitem=5
                                group by 1,2,3,4,5,6,7');

        return view('inventario.listadosstock',[
            'titulo' => 'Listados de Stock',
            'items' => $stock,
            'itemnav' => 'liststk'
        ]);
    }

    public function detalle($id)
    {
        $detalle = DB::select("SELECT su.descripcion as sucursal,
                                      se.id as id_sector,
                                      concat(se.alias,' (',se.descripcion,')') as sector,
                                      ub.id as id_ubicacion,
                                      concat(ub.alias,' (',ub.descripcion,')') as ubicacion,
                                      po.id as id_posicion,
                                      po.descripcion as posicion,
                                      sum(s.cantidad) as cantidad
                                 FROM inv_stock s
                                 JOIN items i on i.id = s.id_item
                                 JOIN inv_sectores se on se.id= s.id_sector
                                 JOIN sucursales su on su.id= se.id_sucursal
                                 LEFT JOIN inv_ubicaciones ub on ub.id= s.id_ubicacion
                                 LEFT JOIN inv_posiciones po on po.id= s.id_posicion
                                WHERE s.id_sector IS NOT NULL
                                  AND id_item=".$id." 
                             GROUP BY 1,2,3,4,5,6,7
                             HAVING sum(cantidad) > 0");
        return $detalle;
    }
}
