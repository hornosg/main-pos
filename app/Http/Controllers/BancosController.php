<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Banco;
use DB;
use Illuminate\Http\Request;
use Excel;

class BancosController extends Controller
{
    //
    public function index()
    {
        $items = Banco::all();
        return view('master.index', ['titulo' => 'Bancos','urlkey' => 'bancos','items' => $items,'itemnav'=>'']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
        ]);


        $banco = $request->all();
        $banco['descripcion'] = strtoupper($banco['descripcion']);

        if (isset($banco['activo'])){
            $banco['activo']=1;
        }else{
            $banco['activo']=0;
        }
        $data=Banco::create($banco);
        if ($banco['activo']==1){
            $data['activo']='SI';
        }else{
            $data['activo']='NO';
        }
        $data['msg']="Banco Creada Exitosamente!";
        return $data;
    }

    public function edit($id)
    {
        $item = Banco::find($id);
        return view('master.edit', ['titulo' => 'Bancos','urlkey' => 'bancos' ,'item' => $item,'itemnav'=>'']);

    }

    public function show($id)
    {
        $item = Banco::find($id);
        return view('master.show', ['titulo' => 'Bancos','urlkey' => 'bancos' ,'item' => $item,'itemnav'=>'']);
    }


    public function update($id,Request $request)
    {
        $banco = Banco::find($id);
        $banco->descripcion = strtoupper($request->descripcion);

        if (isset($request->activo)){
            $banco->activo=1;
        }else{
            $banco->activo=0;
        }

        $banco->save();
        return redirect('/bancos')->with('status', 'Banco Actualizado!');

    }

    public function delete($id)
    {
        $banco = Banco::find($id);
        $banco->delete();
        return redirect('/bancos')->with('status', 'Banco Eliminado!');
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $banco = Banco::find($item);
            $banco->delete();
        }
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $items[] = ['descripcion' => $value->descripcion, 'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_bancos')->insert($items);
                    return redirect('/bancos')->with('status', 'Bancos Importados Correctamente!');
                }else{
                    return redirect('/bancos')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }

    }
}
