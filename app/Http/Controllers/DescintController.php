<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DescInt;
use DB;

class DescintController extends Controller
{
    public function index()
    {
        $items = DB::table('conf_descint as di')
            ->leftJoin('sys_tipoclientes as tc', 'tc.id', '=', 'di.id_tipocliente')
            ->leftJoin('sys_medios_pago as mp', 'mp.id', '=', 'di.id_mediopago')
            ->select('di.*', DB::raw('date_format(di.created_at, "%d/%m/%Y %H:%i:%s") as creado'),
                     'tc.descripcion as tipocliente','mp.descripcion as mediopago')
            ->get();

        return view('precios.descint.index',[
            'titulo' => 'Descuentos e Intereses',
            'urlkey' => 'precios/descint',
            'items' => $items,
            'itemnav' => 'descint'
        ]);
    }

    public function create()
    {
        return view('precios.descint.form',[
            'titulo' => 'Descuentos e Intereses',
            'tituloform' => 'Nuevo',
            'urlkey' => 'precios/descint',
            'edit'=> 1,
            'itemnav' => 'descint',
            'accion' => 'Crear'
        ]);
    }

    public function store(Request $request)
    {
//        dd($request->request);
        $this->validate($request, [
            'descripcion' => 'required',
            'id_tipo' => 'required',
            'id_mediopago' => 'required',
            'porcentaje' => 'required',
            'signo' => 'required'
        ]);

        if (empty($request->id)){
            $descint = new DescInt();
            $mensaje = 'Creado Exitosamente!';
        }else{
            $descint = DescInt::find($request->id);
            $mensaje = 'Editado Exitosamente!';
        }

        $descint->descripcion = strtoupper($request->descripcion);
        $descint->id_tipocliente =  $request->id_tipo;
        $descint->id_mediopago =  $request->id_mediopago;
        $descint->cuotas = $request->cuotas;
        $descint->porcentaje = $request->porcentaje;
        $descint->signo = $request->signo;
        $descint->activo = (!empty($request->activo)) ? 1 : 0;
        $descint->save();
        return redirect('/precios/descint')->with('status', $mensaje);
    }

    public function show($id)
    {
        $descint = DescInt::find($id);
        return view('precios.descint.form',[
            'titulo' => 'Descuentos e Intereses',
            'urlkey' => 'precios/descint',
            'edit'=> 0,
            'itemnav' => 'descint',
            'descint' => $descint,
            'accion' => 'Visualizar'
        ]);
    }

    public function edit($id)
    {
        $descint = DescInt::find($id);
        return view('precios.descint.form',[
            'titulo' => 'Descuentos e Intereses',
            'urlkey' => 'precios/descint',
            'edit'=> 1,
            'itemnav' => 'descint',
            'descint' => $descint,
            'accion' => 'Editar'
        ]);
    }
}
