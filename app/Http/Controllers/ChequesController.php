<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ChequesController extends Controller
{
    public function index()
    {
        $items = DB::select('select mp.id,
                                    mp.numero_transaccion as numero,
                                    b.descripcion as banco,
                                    mp.importe,
                                    mp.fecha_cobro,
                                    mp.fecha_vto,
                                    tc.descripcion as cpteorigen,
                                    c.numero as nrocpteorigen,
                                    tor.descripcion as titularorigen
                                from comprobantes_mp as mp
                                join comprobantes as c on c.id=mp.id_cpte
                                join sys_tipocomprobantes as tc on tc.id=c.id_tipocpte
                                join titulares as tor on tor.id=c.id_titular
                                left join conf_bancos as b on b.id = mp.id_banco
                                where id_mediopago=5');
        return view('caja.cheques', ['titulo' => 'Cheques','urlkey' => 'cheques','itemnav' => 'cheques','items' => $items]);
    }
}
