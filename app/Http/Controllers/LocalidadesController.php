<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Localidad;
use App\Models\Provincia;
use App\Models\Pais;
use DB;
use Illuminate\Http\Request;
use Excel;

class LocalidadesController extends Controller
{
    //
    public function index()
    {
//        $localidades = Localidad::all();
        $localidades = DB::select('select l.*,
                                          CONCAT (pr.descripcion, \' (\',pa.descripcion,\')\') as provincia
                                    from conf_localidades l
                                    left join conf_provincias pr on pr.id=l.id_provincia
                                    left join conf_paises pa on pa.id=pr.id_pais');
        return view('master.localidades.index', ['titulo' => 'Localidades','urlkey' => 'localidades','localidades' => $localidades]);
    }

    public function create()
    {
//        $provincias = Provincia::all();
        $provincias = DB::select('select  pr.id,
                                          CONCAT (pr.descripcion, \' (\', pa.descripcion, \')\') as descripcion,
                                          pr.activo
                                    from conf_provincias pr
                                    left join conf_paises pa on pa.id=pr.id_pais');
        return view('master.localidades.create', ['titulo' => 'Localidades','urlkey' => 'localidades','provincias' => $provincias]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255','id_provincia'=> 'required'
        ]);

        $localidad = $request->all();
        $localidad['descripcion'] = strtoupper($localidad['descripcion']);

        if (isset($localidad['activo'])){
            $localidad['activo']=1;
        }else{
            $localidad['activo']=0;
        }
        $data=Localidad::create($localidad);
        $data['provincia']=Provincia::find($data['id_provincia'])->descripcion.' ('.Pais::find(Provincia::find($data['id_provincia'])->id_pais)->descripcion.')';

        if ($localidad['activo']==1){
            $data['activo']='SI';
        }else{
            $data['activo']='NO';
        }
        $data['msg']="Provincia Creada Exitosamente!";
        return $data;
    }

    public function edit($id)
    {
        $localidad = Localidad::find($id);

        $provincia=Provincia::find($localidad->id_provincia);
        $pais=Pais::find($provincia->id_pais);
        $localidad->descripcion_provincia=$provincia->descripcion." (".$pais->descripcion.") ";

//        $provincias = Provincia::all();
        $provincias = DB::select('select  pr.id,
                                          CONCAT (pr.descripcion, \' (\', pa.descripcion, \')\') as descripcion,
                                          pr.activo
                                    from conf_provincias pr
                                    left join conf_paises pa on pa.id=pr.id_pais');
//        var_dump($provincias);exit;
        return view('master.localidades.edit', ['titulo' => 'Localidades','urlkey' => 'localidades','localidad' => $localidad,'provincias' => $provincias]);

    }

    public function show($id)
    {
        $localidad = Localidad::find($id);
        $localidad->descripcion_provincia=Provincia::find($localidad->id_provincia)->descripcion;
        return view('master.localidades.show', ['titulo' => 'Localidades','urlkey' => 'localidades','localidad' => $localidad]);
    }


    public function update($id,Request $request)
    {
        $localidad = Localidad::find($id);
        $localidad->descripcion = strtoupper($request->descripcion);
        $localidad->id_provincia = $request->id_provincia;

        if (isset($request->activo)){
            $localidad->activo=1;
        }else{
            $localidad->activo=0;
        }

        $localidad->save();
        $data=$localidad;
        $data['provincia']=Provincia::find($localidad->id_provincia)->descripcion.' ('.Pais::find(Provincia::find($localidad->id_provincia)->id_pais)->descripcion.')';
        $data['msg']="Provincia Modificada.";
        return $data;

    }

    public function delete($id)
    {
        $localidad = Localidad::find($id);
        $localidad->delete();
        $data['msg']="Provincia Eliminada!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $localidad = Localidad::find($item);
            $localidad->delete();
        }
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $Provincia = Provincia::where('descripcion', $value->provincia)->select('id')->first();
                    $items[] = ['descripcion' => $value->descripcion,
                                'id_provincia' => $Provincia['id'],
                                'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_localidades')->insert($items);
                    return redirect('/configuracion/geografica')->with('status', 'Localidades Importados Correctamente!');
                }else{
                    return redirect('/configuracion/geografica')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }

    }
}
