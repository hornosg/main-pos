<?php

namespace App\Http\Controllers;

use App\Models\Proveedor;
use App\Models\TipoIva;
use App\Models\Localidad;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Excel;


class ProveedoresController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proveedores = DB::select('select titulares.*, sys_tipoclientes.descripcion as tipo, conf_localidades.descripcion as localidad
                                  from titulares
                                  left join sys_tipoclientes on sys_tipoclientes.id = titulares.id_tipocliente
                                  left join conf_localidades on conf_localidades.id = titulares.id_localidad
                                 where id_tipotitular=3
                                   and deleted_at is null');
        return view('titulares.proveedores.index',[
            'titulo' => 'Proveedores',
            'urlkey' => 'proveedores',
            'proveedores' => $proveedores,
            'itemnav' => 'proveedores'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('titulares.proveedores.create',[
            'titulo' => ' Proveedores',
            'urlkey' => 'proveedores',
            'item' => 'Proveedor',
            'itemnav' => 'proveedores'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required',
            'direccion' => 'required',
            'telefono' =>'required'
        ]);
        
        $proveedor =  new Proveedor;
        $proveedor->descripcion = strtoupper($request->descripcion);
        $proveedor->id_tipotitular =  3;
        $proveedor->id_tipoiva =  $request->id_tipoiva;
        $proveedor->cuit =  $request->cuit;
        $proveedor->direccion = $request->direccion;
        $proveedor->id_localidad = $request->id_localidad;
        $proveedor->telefono = $request->telefono;
        $proveedor->web = $request->web;
        $proveedor->email = $request->email;
        $proveedor->anotaciones = $request->anotaciones;
        $proveedor->activo = $request->activo ? 1 : 0;

        $proveedor->save();

        if ($request->action=='save'){
            return redirect('/proveedores')->with('status', 'Item Creado Exitosamente!');
        }else{
            return redirect('/proveedores/create')->with('status', 'Item Creado Exitosamente!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proveedor = Proveedor::find($id);
        return view('titulares.proveedores.show-edit',[
            'titulo' => ' Proveedores',
            'urlkey' => 'proveedores',
            'item' => 'Proveedor',
            'proveedor' => $proveedor,
            'itemnav' => 'proveedores',
            'edit'=> 0 ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proveedor = Proveedor::find($id);
        return view('titulares.proveedores.show-edit',[
            'titulo' => ' Proveedores',
            'urlkey' => 'proveedores',
            'item' => 'Proveedor',
            'proveedor' => $proveedor,
            'itemnav' => 'proveedores',
            'edit'=> 1 ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $proveedor = Proveedor::find($id);
        $proveedor->descripcion = strtoupper($request->descripcion);
        $proveedor->id_tipoiva =  $request->id_tipoiva;
        $proveedor->cuit =  $request->cuit;
        $proveedor->direccion = $request->direccion;
        $proveedor->id_localidad = $request->id_localidad;
        $proveedor->telefono = $request->telefono;
        $proveedor->web = $request->web;
        $proveedor->email = $request->email;
        $proveedor->anotaciones = $request->anotaciones;
        $proveedor->activo = $request->activo ? 1 : 0;
        $proveedor->save();
        if ($request->action=='save'){
            return redirect('/proveedores')->with('status', 'Item Creado Exitosamente!');
        }else{
            return redirect('/proveedores/edit/'.$proveedor->id)->with('status', 'Item Creado Exitosamente!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $proveedor = Proveedor::find($id);
        $proveedor->delete();
        return redirect('/proveedores')->with('status', 'Item Eliminado!');
    }
    
    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        Proveedor::whereIn('id',$datos)->delete();
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        set_time_limit(300);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data->chunk(1000) as $chunk){
                    foreach ($chunk  as $value) {
//                        var_dump($value);exit;
                        if(empty($value->razonsocial)){
                            break;
                        }

                        $categoria = TipoIva::where('descripcion', $value->categoria)->select('id')->first();
                        $localidad = Localidad::where('descripcion', $value->localidad)->select('id')->first();

                        $item    = ['descripcion'    => $value->razonsocial,
                            'id_tipotitular'    => 3,
                            'cuit'	        => $value->cuit,
                            'id_tipoiva'	=> $categoria['id'],
                            'direccion'	        => $value->direccion,
                            'id_localidad'	=> $localidad['id'],
                            'telefono'	=> $value->telefono,
                            'web'	    => $value->web,
                            'email'	    => $value->email,
                            'activo'        => true];

                        Proveedor::create($item);
                        $items[]=$item;
                    }
                }
                if(!empty($items)){
//                    DB::table('items')->insert($items);
                    $msj =  count($items).' Proveedores Importados Correctamente!';
                    return redirect('/proveedores')->with('status', $msj);
                }else{
                    return redirect('/proveedores')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }

    }

    public function ctacte()
    {
        return view('titulares.proveedores.ctacte',[
            'titulo' => 'Proveedor',
            'urlkey' => 'proveedores',
            'icon' => 'fa-truck',
            'itemnav' => 'ctacte'
        ]);
    }
}
