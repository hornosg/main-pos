<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Moneda;
use App\Models\Cotizacion;
use App\Models\Item;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class MonedasController extends Controller
{
    //
    public function index()
    {
        $items = Moneda::all();
        return view('master.index', ['titulo' => 'Monedas','urlkey' => 'monedas','items' => $items]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
        ]);


        $moneda = $request->all();
        $moneda['descripcion'] = strtoupper($moneda['descripcion']);

        if (isset($moneda['activo'])){
            $moneda['activo']=1;
        }else{
            $moneda['activo']=0;
        }
        Moneda::create($moneda);

        if ($request->action=='save'){
            return redirect('/monedas')->with('status', 'Moneda Creado Exitosamente!');
        }else{
            return redirect('/monedas/create')->with('status', 'Moneda Creado Exitosamente!');
        }
    }

    public function edit($id)
    {
        $item = Moneda::find($id);
        return view('master.edit', ['titulo' => 'Monedas','urlkey' => 'monedas' ,'item' => $item]);

    }

    public function show($id)
    {
        $item = Moneda::find($id);
        return view('master.show', ['titulo' => 'Monedas','urlkey' => 'monedas' ,'item' => $item]);
    }

    public function update($id,Request $request)
    {
        $moneda = Moneda::find($id);
        $moneda->descripcion = strtoupper($request->descripcion);

        if (isset($request->activo)){
            $moneda->activo=1;
        }else{
            $moneda->activo=0;
        }

        $moneda->save();
        return redirect('/monedas')->with('status', 'Moneda Actualizado!');

    }

    public function delete($id)
    {
        $moneda = Moneda::find($id);
        $moneda->delete();
        return redirect('/monedas')->with('status', 'Moneda Eliminado!');
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $moneda = Moneda::find($item);
            $moneda->delete();
        }
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $items[] = ['descripcion' => $value->descripcion, 'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_monedas')->insert($items);
                    return redirect('/monedas')->with('status', 'Monedas Importados Correctamente!');
                }else{
                    return redirect('/monedas')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }

    }

    public function ajaxstorecotizacion(Request $request)
    {
        $item =  new Cotizacion();
        $item->id_moneda = 2; //Dolar por el momento!
        $item->id_sucursal = Auth::user()->id_sucursal;
        $item->cambio = floatval($request->cambio);
        $item->id_user = $sucursal = Auth::user()->id;
        $item->save();

        ini_set('max_execution_time', '300');
        $items = DB::select('select * from items where id_tipoitem=5 and id_moneda=2 and deleted_at is null');
        foreach ($items as $prod){
            $producto = Item::find($prod->id);
            $precioListaDolarizado = $producto->precioLista * $item->cambio;
            $precioListaIva = $precioListaDolarizado + (($precioListaDolarizado*$producto->iva)/100);
            if ($producto->bonifProv > 0){
                $producto->precioCosto=$precioListaIva - (($precioListaIva*$producto->bonifProv)/100);
            }else{
                $producto->precioCosto=$precioListaIva;
            }
            if ($producto->divisor>1){
                $producto->precioCosto = $producto->precioCosto / $producto->divisor;
            }
            if ($producto->ganancia > 0){
                $producto->precio=$producto->precioCosto + (($producto->precioCosto*$producto->ganancia)/100);
            }else{
                $producto->precio=$producto->precioCosto;
            }
            $producto->save();
        }

        $data['msg']="Creado Exitosamente!";
        $data['fecha']=date("d/m/Y H:i:s");
        $data['cambio']=number_format($item->cambio, 2, '.', '');
        return $data;
    }
}
