<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Origen;
use DB;
use Illuminate\Http\Request;
use Excel;

class OrigenesController extends Controller
{
    //
    public function index()
    {
        $items = Origen::all();
        return view('master.index', ['titulo' => 'Origenes de Fabricacion','urlkey' => 'origenes','items' => $items, 'itemnav' => 'otro']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
        ]);


        $origen = $request->all();
        $origen['descripcion'] = strtoupper($origen['descripcion']);

        if (isset($origen['activo'])){
            $origen['activo']=1;
        }else{
            $origen['activo']=0;
        }
        $data=Origen::create($origen);
        if ($origen['activo']==1){
            $data['activo']='SI';
        }else{
            $data['activo']='NO';
        }
        $data['msg']="Origen Creado Exitosamente!";
        return $data;
    }

    public function edit($id)
    {
        $item = Origen::find($id);
        return view('master.edit', ['titulo' => 'Origenes de Fabricacion','urlkey' => 'origenes' ,'item' => $item]);

    }

    public function show($id)
    {
        $item = Origen::find($id);
        return view('master.show', ['titulo' => 'Origenes de Fabricacion','urlkey' => 'origenes' ,'item' => $item]);
    }


    public function update($id,Request $request)
    {
        $origen = Origen::find($id);
        $origen->descripcion = strtoupper($request->descripcion);

        if (isset($request->activo)){
            $origen->activo=1;
        }else{
            $origen->activo=0;
        }

        $origen->save();
        $data['msg']="Marca Modificada.";
        return $data;

    }

    public function delete($id)
    {
        $origen = Origen::find($id);
        $origen->delete();
        $data['msg']="Origen de Fabricacion Eliminado!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $origen = Origen::find($item);
            $origen->delete();
        }
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $items[] = ['descripcion' => $value->descripcion, 'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_origen')->insert($items);
                    return redirect('/origenes')->with('status', 'Origenes Importados Correctamente!');
                }else{
                    return redirect('/origenes')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }
    }
}
