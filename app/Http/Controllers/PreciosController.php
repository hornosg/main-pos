<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\LogPrecio;
use App\Models\LogPrecioItem;
use App\Models\Item;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use Excel;

class PreciosController extends Controller
{
    public function home()
    {
        return view('precios.index',[
            'titulo' => 'Dashboard',
            'itemnav' => ''
        ]);
    }

    public function index()
    {
        $items = DB::table('log_precios as lp')
            ->leftJoin('titulares as t', 't.id', '=', 'lp.id_titular')
            ->leftJoin('conf_marcas as m', 'm.id', '=', 'lp.id_marca')
            ->leftJoin('conf_rubros as r', 'r.id', '=', 'lp.id_rubro')
            ->leftJoin('conf_subrubros as sr', 'sr.id', '=', 'lp.id_subrubro')
            ->leftJoin('conf_lineas as l', 'l.id', '=', 'lp.id_linea')
            ->leftJoin('conf_materiales as mt', 'mt.id', '=', 'lp.id_material')
            ->leftJoin('users as us', 'us.id', '=', 'lp.created_us')
            ->select('lp.id',
                    'lp.id_tipoactualizacion as id_tipo',
                    't.descripcion as titular',
                    'm.descripcion as marca',
                    'r.descripcion as rubro',
                    'sr.descripcion as subrubro',
                    'l.descripcion as linea',
                    'mt.descripcion as material',
                    'lp.signo',
                    'lp.archivo',
                     DB::raw('date_format(lp.created_at, "%d/%m/%y %H:%i") as creado'),
                    'us.name as usuario')
            ->get();

        return view('precios.actualizacion.index',[
            'titulo' => 'Actualizaciones',
            'urlkey' => 'precios/actualizacion',
            'items' => $items,
            'itemnav' => 'actu'
        ]);
    }

    public function create($tipo)
    {
        switch ($tipo) {
            case '1':
                $view='import';
                $tituloform = 'Importar Lista de Precios del Proveedor';
                $accion = 'Importacion';
                break;
            case '2':
                $view='form';
                $tituloform = 'Actualizacion Masiva de Precios';
                $accion = 'Actualizacion Masiva';
                break;
            case '3':
                $view='ingroup';
                $tituloform = 'Actualizacion Manual Comparativa';
                $accion = 'Actualizacion Manual';
                break;
        }
        return view('precios.actualizacion.'.$view,[
            'titulo' => 'Actualizaciones',
            'tituloform' => $tituloform,
            'urlkey' => 'precios/actualizacion',
            'edit'=> 1,
            'itemnav' => 'actu',
            'accion' => $accion
        ]);
    }

    public function store1(Request $request)
    {
        //dd($request->request);
        $this->validate($request, [
            'import_file' => 'required',
        ]);
        $almenosuno=false;
        $cotizacion = DB::select('select date_format(c.created_at, "%d/%m/%Y %H:%i:%s") as fecha, cambio
                                                        from cotizaciones as c
                                                       where id = ( SELECT MAX(id) FROM cotizaciones
                                                                     WHERE id_sucursal ='.Auth::user()->id_sucursal.')');
        set_time_limit(300);

        if(Input::hasFile('import_file')){
            DB::beginTransaction();
                $logprecio = new LogPrecio();
                $logprecio->id_tipoactualizacion = $request->id_tipoactualizacion;
                $logprecio->id_titular = $request->id_titular;
                $logprecio->archivo = Input::file('import_file')->getClientOriginalName();
                $logprecio->created_us = auth()->id();
                //dd($logprecio);
                $logprecio->save();

                $titular=$logprecio->id_titular;
                $path = Input::file('import_file')->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                if(!empty($data)){
                    foreach ($data as $value) {
                        $codproducto=strval($value->codproducto);
                        $items = DB::select('select i.id from items as i
                                              join items_proveedores as ip on i.id = ip.id_item
                                                                          and ip.actualizaprecio = 1
                                                                          and ip.id_titular = ?
                                                                          and ip.codproducto= ?
                                               where i.deleted_at is null',[$titular,$codproducto]);
                        if(!empty($items)){
                            foreach ($items as $item){
                                $producto = Item::find($item->id);
                                $logitem = New LogPrecioItem();
                                $logitem->id_log=$logprecio->id;
                                $logitem->id_item=$producto->id;

                                $logitem->oldprecioLista = $producto->precioLista;
                                $producto->precioLista = $value->preciolista;
                                $logitem->newprecioLista = $producto->precioLista;

                                if ($producto->id_moneda==2){
                                    $preciodolar = $producto->precioLista * $cotizacion[0]->cambio;
                                    if ($producto->bonifProv > 0 ){
                                        $producto->precioCosto = $preciodolar + (($preciodolar*$producto->iva)/100) - ( ( ($preciodolar + (($preciodolar*$producto->iva)/100)) * $producto->bonifProv) /100 );
                                    }else{
                                        $producto->precioCosto = $preciodolar + (($preciodolar*$producto->iva)/100);
                                    }
                                }else{
                                    if ($producto->bonifProv > 0 ){
                                        $producto->precioCosto = $producto->precioLista + (($producto->precioLista*$producto->iva)/100) - ((($producto->precioLista + (($producto->precioLista*$producto->iva)/100))*$producto->bonifProv)/100);
                                    }else{
                                        $producto->precioCosto = $producto->precioLista + (($producto->precioLista*$producto->iva)/100);
                                    }
                                }
                                if ($producto->divisor>1){
                                    $producto->precioCosto = $producto->precioCosto / $producto->divisor;
                                }
                                $producto->precio=$producto->precioCosto + (($producto->precioCosto * $producto->ganancia)/100);
                                $producto->save();
                                $logitem->save();
                                $almenosuno=true;
                            }
                        }
                    }
                    if ($almenosuno){
                        DB::commit();
                        $mensaje='Precios Importados Correctamente.';
                    }else{
                        DB::rollback();
                        $mensaje='No se encontraron Productos para actualizar.';
                    }
                }else{
                    DB::rollback();
                    $mensaje='No se importaron Precios.';
                }
            return redirect('/precios/actualizacion')->with('status', $mensaje);
        }
    }


    public function store2(Request $request)
    {
//        dd($request->request);
        $cotizacion = DB::select('select date_format(c.created_at, "%d/%m/%Y %H:%i:%s") as fecha, cambio
                                                        from cotizaciones as c
                                                       where id = ( SELECT MAX(id) FROM cotizaciones
                                                                     WHERE id_sucursal ='.Auth::user()->id_sucursal.')');
        DB::beginTransaction();
            $logprecio = new LogPrecio();
            $logprecio->id_tipoactualizacion = $request->id_tipoactualizacion;
            $logprecio->id_titular = $request->id_titular;
            $logprecio->id_marca =  $request->id_marca;
            $logprecio->id_rubro =  $request->id_rubro;
            $logprecio->id_subrubro =  $request->id_subrubro;
            $logprecio->id_linea =  $request->id_linea;
            $logprecio->id_material =  $request->id_material;
            $logprecio->porcentaje_precioLista = $request->porcentaje_preciolista;
            $logprecio->porcentaje_ganancia = $request->porcentaje_ganancia;
            $logprecio->signo = $request->signo;
            $logprecio->created_us = auth()->id();
            $logprecio->save();

            $sql = 'select i.id from items i';
            if (!empty($logprecio->id_titular)){$sql = $sql.' join items_proveedores ip on ip.id_item=i.id and ip.id_titular='.$logprecio->id_titular;}
            $sql = $sql.' where i.activo=1 and i.deleted_at is null';
            if (!empty($logprecio->id_marca)){$sql = $sql.' and i.id_marca='.$logprecio->id_marca;}
            if (!empty($logprecio->id_rubro)){$sql = $sql.' and i.id_rubro='.$logprecio->id_rubro;}
            if (!empty($logprecio->id_subrubro)){$sql = $sql.' and i.id_subrubro='.$logprecio->id_subrubro;}
            if (!empty($logprecio->id_linea)){$sql = $sql.' and i.id_linea='.$logprecio->id_linea;}
            if (!empty($logprecio->id_material)){$sql = $sql.' and i.id_material='.$logprecio->id_material;}

            $items = DB::select($sql);
            //dd($sql);
            foreach ($items as $item){
                $producto = Item::find($item->id);

                $logitem = New LogPrecioItem();
                $logitem->id_log=$logprecio->id;
                $logitem->id_item=$producto->id;
//                dd($producto);

                if (!empty($logprecio->porcentaje_precioLista)){

                    if ($producto->id_moneda==2){
                        $preciodolar = $producto->precioLista * $cotizacion[0]->cambio;

                        $logitem->oldprecioLista = $producto->precioLista;
                        $producto->precioLista = ($producto->precioLista*$logprecio->porcentaje_precioLista/100)*$logprecio->signo + $producto->precioLista;
                        $logitem->newprecioLista = $producto->precioLista;

                        if ($producto->bonifProv > 0 ){
                            $producto->precioCosto = $preciodolar + (($preciodolar*$producto->iva)/100) - ((($preciodolar + (($preciodolar*$producto->iva)/100))*$producto->bonifProv)/100);
                        }else{
                            $producto->precioCosto = $preciodolar + (($preciodolar*$producto->iva)/100);
                        }
                        if ($producto->divisor>1){
                            $producto->precioCosto = $producto->precioCosto / $producto->divisor;
                        }
                        $producto->precio=$producto->precioCosto + (($producto->precioCosto * $producto->ganancia)/100);
                    }else{
                        $logitem->oldprecioLista = $producto->precioLista;
                        $producto->precioLista = ($producto->precioLista*$logprecio->porcentaje_precioLista/100)*$logprecio->signo + $producto->precioLista;
                        $logitem->newprecioLista = $producto->precioLista;

                        if ($producto->bonifProv > 0 ){
                            $producto->precioCosto = $producto->precioLista + (($producto->precioLista*$producto->iva)/100) - ((($producto->precioLista + (($producto->precioLista*$producto->iva)/100))*$producto->bonifProv)/100);
                        }else{
                            $producto->precioCosto = $producto->precioLista + (($producto->precioLista*$producto->iva)/100);
                        }
                        if ($producto->divisor>1){
                            $producto->precioCosto = $producto->precioCosto / $producto->divisor;
                        }
                        $producto->precio=$producto->precioCosto + (($producto->precioCosto * $producto->ganancia)/100);
                    }
                }
                if (!empty($logprecio->porcentaje_ganancia)){
                    $logitem->oldganancia = $producto->ganancia;
                    $producto->ganancia = $logprecio->porcentaje_ganancia;
                    $logitem->newganancia = $producto->ganancia;
                    $producto->precio=$producto->precioCosto + (($producto->precioCosto * $producto->ganancia)/100);
                }
                $producto->save();
                $logitem->save();
            }
        DB::commit();
        return redirect('/precios/actualizacion')->with('status', 'Actualizacion Exitosa');
    }

    public function store3(Request $request)
    {
        //dd($request->request);
        DB::beginTransaction();
            $logprecio = new LogPrecio();
            $logprecio->id_tipoactualizacion = $request->id_tipoactualizacion;
            $logprecio->id_titular = $request->idtitular;
            $logprecio->id_marca =  $request->id_marca;
            $logprecio->id_rubro =  $request->id_rubro;
            $logprecio->id_subrubro =  $request->id_subrubro;
            $logprecio->id_linea =  $request->id_linea;
            $logprecio->id_material =  $request->id_material;
            $logprecio->created_us = auth()->id();
            //dd($logprecio);
            $logprecio->save();

            $items = json_decode($request->items);
            if(!empty($items)){
                foreach ($items as $item) {
                    if ($item->status==1){
                        $producto = Item::find($item->id);
                        $logitem = New LogPrecioItem();
                        $logitem->id_log=$logprecio->id;
                        $logitem->id_item=$producto->id;

                        $logitem->oldprecioLista = $producto->precioLista;
                        $producto->precioLista = $item->precioLista;
                        $logitem->newprecioLista = $producto->precioLista;
                        $logitem->oldbonifProv = $producto->bonifProv;
                        $producto->bonifProv = $item->bonifProv;
                        $producto->divisor = $item->divisor;
                        $logitem->newbonifProv = $producto->bonifProv;
                        $producto->precioCosto = $item->precioCosto;
                        $logitem->oldganancia = $producto->ganancia;
                        $producto->ganancia = $item->ganancia;
                        $logitem->newganancia = $producto->ganancia;
                        $producto->precio=$item->precio ;

                        $producto->save();
                        $logitem->save();
                        $almenosuno=true;

                        if (!empty($request->idtitular)){
                            $r = DB::table('items_proveedores')
                                ->where([['id_item','=',$producto->id],
                                    ['id_titular','=',$request->idtitular]])->get();
                            if($r->count()===0){
                                DB::table('items_proveedores')->insert(
                                    ['id_item' => $producto->id,
                                        'id_titular' => $request->idtitular,
                                        'codproducto' => $item->codproducto,
                                        'actualizaprecio' => 1 ]
                                );
                            }else{
                                DB::table('items_proveedores')
                                    ->where([['id_item','=',$producto->id],
                                             ['id_titular','=',$request->idtitular]])
                                    ->update(['codproducto' => $item->codproducto]);
                            }

                        }
                    }
                }
                if ($almenosuno){
                    DB::commit();
                    $mensaje='Actualizacion ejecutada Correctamente.';
                }else{
                    DB::rollback();
                    $mensaje='No se encontraron Productos para actualizar.';
                }
            }else{
                DB::rollback();
                $mensaje='No se efectuaron cambios.';
            }
        return redirect('/precios/actualizacion')->with('status', $mensaje);
    }

    public function detalle($id)
    {
        $detalle = DB::select("select lpi.*, i.descripcion, COALESCE(ip.codproducto,'') as codproducto, i.precioCosto, i.precio
                            from log_precios_items as lpi
                            join items i on lpi.id_item = i.id
                            left join items_proveedores ip on ip.id_item = i.id
                            where id_log=".$id);
        return $detalle;
    }

    public function getproductos(Request $request)
    {
        //dd($request->request);
        $joinprov =(!empty($request->idtitular) ? ' join items_proveedores ip on ip.id_item = i.id  and ip.id_titular= '.$request->idtitular : 'left join items_proveedores ip on ip.id_item = i.id');
        $wheremoneda  =(!empty($request->id_moneda) ? ' and i.id_moneda = '.$request->id_moneda : '');
        $wheremarca  =(!empty($request->id_marca) ? ' and i.id_marca = '.$request->id_marca : '');
        $whererubro  =(!empty($request->id_rubro) ? ' and i.id_rubro = '.$request->id_rubro : '');
        $wheresubrubro =(!empty($request->id_subrubro) ? ' and i.id_subrubro = '.$request->id_subrubro : '');
        $wherelinea =(!empty($request->id_linea) ? ' and i.id_linea = '.$request->id_linea : '');
        $wherematerial =(!empty($request->id_material) ? ' and i.id_material = '.$request->id_material : '');

        $query = 'select i.id,
                         COALESCE(ip.codproducto,"") as codproducto,
                         concat(\'[\',i.id,\'] \', i.descripcion) as producto,
                         m.descripcion as moneda,
                         i.precioLista,
                         i.bonifProv,
                         i.iva,
                         i.divisor,
                         i.precioCosto,
                         i.ganancia,
                         i.precio
                    from items i
                    join sys_monedas m on m.id=i.id_moneda ';
        $query = $query.$joinprov.' where 1=1 '.$wheremarca.$whererubro.$wheresubrubro.$wherelinea.$wherematerial.$wheremoneda;
        $items = DB::select($query);
        return $items;
    }
}
