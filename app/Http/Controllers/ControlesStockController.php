<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sector;
use App\Models\Ubicacion;
use App\Models\Posicion;
use App\Models\ControlStock;
use App\Models\ControlStockConteo;
use App\Models\MovimientoStock;
use App\Models\Stock;
use Dompdf\Dompdf;
use DB;
use Jenssegers\Agent\Agent;

class ControlesStockController extends Controller
{
    public function index()
    {
        $items = DB::table('inv_control_stock as cs')
            ->leftJoin('items as i', 'i.id', '=', 'cs.id_item')
            ->leftJoin('inv_sectores as so', 'so.id', '=', 'cs.id_sector')
            ->leftJoin('inv_ubicaciones as uo', 'uo.id', '=', 'cs.id_ubicacion')
            ->leftJoin('inv_posiciones as po', 'po.id', '=', 'cs.id_posicion')
            ->leftJoin('conf_marcas as ma', 'ma.id', '=','cs.id_marca')
            ->leftJoin('conf_rubros as ru', 'ru.id', '=','cs.id_rubro')
            ->leftJoin('conf_subrubros as sru', 'sru.id', '=','cs.id_subrubro')
            ->leftJoin('sys_estados as e', 'e.id', '=','cs.id_estado')
            ->whereNull('cs.deleted_at')
            ->select('cs.*',
                     DB::raw('date_format(cs.created_at, "%d/%m %H:%i") as creado'),
                     DB::raw("concat('[',i.id,'] ', i.descripcion, ' (', COALESCE(ma.descripcion,''), ')') as producto"),
                     'ma.descripcion as marca',
                     'ru.descripcion as rubro',
                     'sru.descripcion as subrubro',
                     'po.descripcion as posicion',
                     'so.alias as sector',
                     'uo.alias as ubicacion',
                     'e.descripcion as estado')
            ->get();
        return view('inventario.controles.index',[
            'titulo' => 'Controles de Stock',
            'items' => $items,
            'itemnav' => 'ctrolstk'
        ]);
    }

    public function create()
    {
        return view('inventario.controles.form',[
            'titulo' => 'Control de Stock',
            'tituloform' => 'Nuevo Control de Stock',
            'accion' => 'Nueva',
            'itemnav' => 'ctrolstk',
            'edit'=> 1,
        ]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $ctrolstk = new ControlStock();
        $ctrolstk->id_item =$request->id_item;
        $ctrolstk->id_marca =$request->id_marca;
        $ctrolstk->id_rubro =$request->id_rubro;
        $ctrolstk->id_subrubro =$request->id_subrubro;
        $ctrolstk->id_sector =$request->id_sector;
        $ctrolstk->id_ubicacion =$request->id_ubicacion;
        $ctrolstk->id_posicion =$request->id_posicion;
        $ctrolstk->id_estado = 5;
        $ctrolstk->save();

        if ( (!empty($request->id_item)) or
             (!empty($request->id_marca)) or
             (!empty($request->id_rubro)) or
             (!empty($request->id_subrubro)) ) {

            $joiniditem =(!empty($request->id_item) ? ' and i.id = '.$request->id_item : '');
            $joinmarca  =(!empty($request->id_marca) ? ' and i.id_marca = '.$request->id_marca : '');
            $joinrubro  =(!empty($request->id_rubro) ? ' and i.id_rubro = '.$request->id_rubro : '');
            $joinsubrubro =(!empty($request->id_subrubro) ? ' and i.id_subrubro = '.$request->id_subrubro : '');

            $query = 'select id_item,id_sector,id_ubicacion,id_posicion, sum(cantidad) as stock from inv_stock s join items i on i.id = s.id_item';
            $query = $query.$joiniditem.$joinmarca.$joinrubro.$joinsubrubro;

            $wheresector = (!empty($request->id_sector) ? ' and s.id_sector = '.$request->id_sector : '');
            $whereubicacion = (!empty($request->id_ubicacion) ? ' and s.id_ubicacion = '.$request->id_ubicacion : '');
            $whereposicion = (!empty($request->id_posicion) ? ' and s.id_posicion = '.$request->id_posicion : '');

            $query = $query.' where 1=1 '.$wheresector.$whereubicacion.$whereposicion;
            $query = $query.' group by 1,2,3,4 ';
            $items = DB::select($query);
        }else{
            $query = 'select id_item,id_sector,id_ubicacion,id_posicion, sum(cantidad) as stock  from inv_stock s where 1=1';
            if (!empty($request->id_posicion)){
                $query = $query.' and s.id_posicion = ? group by 1,2,3,4
                                union
                                select stk.id_item,
                                       stk.id_sector,
                                       stk.id_ubicacion,
                                       p.id as id_posicion,
                                       0 as stock
                                from inv_posiciones p
                                left join inv_stock stk on stk.id_posicion = p.id
                                where p.id = ?
                                  and stk.id is null';
                $items = DB::select($query,[$request->id_posicion,$request->id_posicion]);
            }else{
                if (!empty($request->id_ubicacion)){
                    $query = $query.' and s.id_ubicacion = ? group by 1,2,3,4
                        union
                            select stk.id_item,
                                   u.id_sector,
                                   u.id as id_ubicacion,
                                   p.id as id_posicion,
                                   0 as stock
                            from inv_posiciones p
                            join inv_ubicaciones u on u.id = p.id_ubicacion and p.id_ubicacion = ?
                            left join inv_stock stk on stk.id_posicion = p.id
                            where stk.id is null
                            union
                            select stk.id_item,
                                   u.id_sector,
                                   u.id as id_ubicacion,
                                   p.id as id_posicion,
                                   0 as stock
                            from inv_ubicaciones u
                            left join inv_posiciones p on p.id_ubicacion = u.id
                            left join inv_stock stk on stk.id_ubicacion = u.id
                            where u.id = ?
                              and stk.id is null
                            order by 1,4 desc';
                    $items = DB::select($query,[$request->id_ubicacion,$request->id_ubicacion,$request->id_ubicacion]);
                }else{
                    if (!empty($request->id_sector)){
                        $query = $query.' and s.id_sector = ? group by 1,2,3,4
                            union
                            select stk.id_item,
                                   s.id as id_sector,
                                   u.id as id_ubicacion,
                                   p.id as id_posicion,
                                   0 as stock
                            from inv_posiciones p
                            join inv_ubicaciones u on u.id = p.id_ubicacion
                            join inv_sectores s on s.id=u.id_sector and s.id = ?
                            left join inv_stock stk on stk.id_posicion = p.id
                            where stk.id is null
                            union
                            select stk.id_item,
                                   s.id as id_sector,
                                   u.id as id_ubicacion,
                                   p.id as id_posicion,
                                   0 as stock
                            from inv_ubicaciones u
                            join inv_sectores s on s.id=u.id_sector and s.id = ?
                            left join inv_posiciones p on p.id_ubicacion = u.id
                            left join inv_stock stk on stk.id_ubicacion = u.id
                            where stk.id is null
                            union
                            select stk.id_item,
                                   s.id as id_sector,
                                   u.id as id_ubicacion,
                                   p.id as id_posicion,
                                   0 as stock
                            from inv_sectores s
                            left join inv_ubicaciones u on u.id_sector = s.id
                            left join inv_posiciones p on p.id_ubicacion = u.id
                            left join inv_stock stk on stk.id_sector = s.id
                            where s.id = ?
                              and stk.id is null
                            order by 1,4 desc';
                        //dd($query);
                        $items = DB::select($query,[$request->id_sector,$request->id_sector,$request->id_sector,$request->id_sector]);
                    }
                }
            }
        }

        foreach ($items as $item){
            $ctrolstkct = new ControlStockConteo();
            $ctrolstkct->id_ctrolstk = $ctrolstk->id;
            $ctrolstkct->id_item = $item->id_item;
            $ctrolstkct->id_sector = $item->id_sector;
            $ctrolstkct->id_ubicacion = $item->id_ubicacion;
            $ctrolstkct->id_posicion = $item->id_posicion;
            $ctrolstkct->cantidad_fisica = null;
            $ctrolstkct->cantidad_stock = $item->stock;
            $ctrolstkct->save();
        }
        DB::commit();
        return redirect('/inventario/controles/index');
    }

    public function nuevocontrol($id){
        //Genera un Nuevo Control/Conteo unicamente para el registro de stock solicitado
        DB::beginTransaction();
        $Stock = Stock::find($id);
        $ctrolstk = new ControlStock();
        $ctrolstk->id_item =$Stock->id_item;
        $ctrolstk->id_sector =$Stock->id_sector;
        $ctrolstk->id_ubicacion =$Stock->id_ubicacion;
        $ctrolstk->id_posicion =$Stock->id_posicion;
        $ctrolstk->id_estado = 5;
        $ctrolstk->save();

        $ctrolstkct = new ControlStockConteo();
        $ctrolstkct->id_ctrolstk = $ctrolstk->id;
        $ctrolstkct->id_item = $ctrolstk->id_item;
        $ctrolstkct->id_sector = $ctrolstk->id_sector;
        $ctrolstkct->id_ubicacion = $ctrolstk->id_ubicacion;
        $ctrolstkct->id_posicion = $ctrolstk->id_posicion;
        $ctrolstkct->cantidad_fisica = null;
        $ctrolstkct->cantidad_stock = $Stock->cantidad_stock;
        $ctrolstkct->save();
        DB::commit();

        $data['msg']="Se genero el Control de Numero: ".$ctrolstk->id;
        return $data;
    }

    public function delete($id)
    {
        DB::table('inv_control_stock_conteo')->where('id_ctrolstk', '=', $id)->delete();
        $ctrol = ControlStock::find($id);
        $ctrol->delete();

        $data['msg']="Control de Stock Eliminado!";
        return $data;
    }

    public function control($id){
        $items = DB::select("select csc.id,
                                COALESCE(concat(i.descripcion,' - ', i.contNeto, ' ', COALESCE(um.descripcion,''),' - ', COALESCE(m.descripcion,'')),'') as producto,
                                s.alias as sector,
                                u.alias as ubicacion,
                                p.descripcion as posicion,
                                CASE WHEN cantidad_fisica > 0 THEN cantidad_fisica ELSE '?' END as fisico,
                                CASE WHEN cantidad_fisica > 0 THEN cantidad_stock ELSE 'X' END as stock,
                                CASE WHEN cantidad_fisica IS NULL THEN 'SIN INICIAR'
                                     WHEN cantidad_fisica <> cantidad_stock AND id_movimiento IS NULL THEN 'CON DIFERENCIAS'
                                     WHEN cantidad_fisica = cantidad_stock THEN 'OK'
                                     WHEN cantidad_fisica <> cantidad_stock AND id_movimiento IS NOT NULL THEN 'DIFERENCIA AJUSTADA'
                                END as status
                        from inv_control_stock_conteo csc
                        left join items i on csc.id_item = i.id
                        left join conf_unidades_medida um on i.id_unidadcontneto = um.id
                        left join conf_marcas m on i.id_marca = m.id
                        left join inv_sectores s on s.id = csc.id_sector
                        left join inv_ubicaciones u on u.id = csc.id_ubicacion
                        left join inv_posiciones p on p.id = csc.id_posicion
                        where cantidad_fisica is null and id_ctrolstk=".$id);
        $mobile = true;
        return view('inventario.controles.control',[
            'titulo' => 'Control de Stock '.$id,
            'items' => $items,
            'mobile' => $mobile,
            'idctrol' => $id,
            'itemnav' => 'ctrolstk'
        ]);
    }

    public function controladmin($id){
        $items = DB::select("select csc.id,
                                CASE WHEN cantidad_fisica > 0 THEN cantidad_fisica ELSE '?' END as fisico,
                                CASE WHEN cantidad_fisica > 0 THEN cantidad_stock ELSE 'X' END as stock,
                                CASE WHEN cantidad_fisica IS NULL THEN 'SIN INICIAR'
                                     WHEN cantidad_fisica <> cantidad_stock AND id_movimiento IS NULL THEN 'CON DIFERENCIAS'
                                     WHEN cantidad_fisica = cantidad_stock THEN 'OK'
                                     WHEN cantidad_fisica <> cantidad_stock AND id_movimiento IS NOT NULL THEN 'DIFERENCIA AJUSTADA'
                                END as status,
                                COALESCE(concat('[',i.id,'] ',i.descripcion,' - ', i.contNeto, ' ', COALESCE(um.descripcion,''),' - ', COALESCE(m.descripcion,'')),'') as producto,
                                s.alias as sector,
                                u.alias as ubicacion,
                                p.descripcion as posicion,
                                csc.cantidad_fisica,
                                csc.cantidad_stock,
                                csc.id_movimiento,
                                date_format(csc.created_at, '%d/%m %H:%i') as creado
                        from inv_control_stock_conteo csc
                        left join items i on csc.id_item = i.id
                        left join conf_unidades_medida um on i.id_unidadcontneto = um.id
                        left join conf_marcas m on i.id_marca = m.id
                        left join inv_sectores s on s.id = csc.id_sector
                        left join inv_ubicaciones u on u.id = csc.id_ubicacion
                        left join inv_posiciones p on p.id = csc.id_posicion
                        where id_ctrolstk=".$id);
        return view('inventario.controles.control',[
            'titulo' => 'Control de Stock '.$id,
            'items' => $items,
            'idctrol' => $id,
            'itemnav' => 'ctrolstk'
        ]);
    }

    public function detalle($id)
    {
        $detalle = DB::select("select csc.id, COALESCE(concat(i.descripcion,' - ', i.contNeto, ' ', COALESCE(um.descripcion,''),' - ', COALESCE(m.descripcion,'')),'') as producto,
                                      s.descripcion as sector,
                                      u.descripcion as ubicacion,
                                      p.descripcion as posicion,
                                      csc.cantidad_fisica,
                                      csc.cantidad_stock,
                                      csc.id_movimiento
                            from inv_control_stock_conteo csc
                            left join items i on csc.id_item = i.id
                            left join conf_unidades_medida um on i.id_unidadcontneto = um.id
                            left join conf_marcas m on i.id_marca = m.id
                            left join inv_sectores s on s.id = csc.id_sector
                            left join inv_ubicaciones u on u.id = csc.id_ubicacion
                            left join inv_posiciones p on p.id = csc.id_posicion
                            where id_ctrolstk=".$id);
        return $detalle;
    }

    public function conteocreate($idcontrol)
    {
        $ctrol = ControlStock::find($idcontrol);
        return view('inventario.controles.conteo',[
            'titulo' => 'Conteo',
            'tituloform' => 'Nuevo Conteo',
            'accion' => 'Nuevo',
            'itemnav' => 'ctrolstk',
            'edit'=> 1,
            'ctrol' =>$ctrol
        ]);
    }

    public function conteoedit($id)
    {
        $conteo = ControlStockConteo::find($id);
        $ctrol = ControlStock::where('id',$conteo->id_ctrolstk)->first();
        return view('inventario.controles.conteo',[
            'titulo' => 'Conteo',
            'tituloform' => 'Conteo',
            'accion' => 'Nuevo',
            'itemnav' => 'ctrolstk',
            'edit'=> 1,
            'conteo' =>$conteo,
            'ctrol' =>$ctrol
        ]);
    }

    public function conteostore($id=null,Request $request){
        if (empty($id)){
            $conteo = new ControlStockConteo();
        }else{
            $conteo = ControlStockConteo::find($id);
        }
        $conteo->id_ctrolstk = $request->id_ctrolstk;
        $conteo->id_item = ($request->id_item)?: $conteo->id_item;
        $conteo->id_sector = ($request->id_sector)?: $conteo->id_sector;
        $conteo->id_ubicacion = ($request->id_ubicacion)?: $conteo->id_ubicacion;
        $conteo->id_posicion = ($request->id_posicion)?: $conteo->id_posicion;
        $conteo->cantidad_fisica = $request->cantidad_fisica;
        $conteo->cantidad_stock = 0;
        $conteo->save();


        //Actualizo Estado del Control
        $control = ControlStock::find($conteo->id_ctrolstk);
        $diferencias = DB::select("select count(*) as cant from inv_control_stock_conteo where (cantidad_fisica is null or cantidad_fisica<>cantidad_stock) and id_ctrolstk=".$conteo->id_ctrolstk);
        if($diferencias[0]->cant==0){
            $control->id_estado = 9;    //Finalizado (Sin Diferencias)
        }else{
            if ($conteo->cantidad_fisica == $conteo->cantidad_stock){
                if($control->id_estado == 5){   //SIN INICIAR
                    $control->id_estado = 6;    //EN PROCESO
                }
            }else{
                $control->id_estado = 7;        //CON DIFERENCIAS
            }
        }
        $control->save();

        return redirect('/inventario/controles/'.$conteo->id_ctrolstk);
    }

    public function ajustar($id)
    {
        DB::beginTransaction();
        $conteo = ControlStockConteo::find($id);

        $movstk = new MovimientoStock();
        $movstk->id_motivo = 3;
        $movstk->id_item = $conteo->id_item;
        $movstk->id_sector_d = $conteo->id_sector;
        $movstk->id_ubicacion_d = $conteo->id_ubicacion;
        $movstk->id_posicion_d = $conteo->id_posicion;
        $movstk->cantidad = $conteo->cantidad_fisica - $conteo->cantidad_stock;
        $movstk->save();

        $stk = new Stock();
        $stk->id_item = $movstk->id_item;
        $stk->id_sector = $movstk->id_sector_d;
        $stk->id_ubicacion = $movstk->id_ubicacion_d;
        $stk->id_posicion = $movstk->id_posicion_d;
        $stk->cantidad = $movstk->cantidad;
        $stk->id_movstk = $movstk->id;
        $stk->save();

        $conteo->id_movimiento = $movstk->id;
        $conteo->save();

        //Valido Estado del Control
        $pendientes = DB::select("select count(*) as cant from inv_control_stock_conteo where cantidad_fisica is null and id_ctrolstk=".$conteo->id_ctrolstk);
        if($pendientes[0]->cant==0){
            $diferencias = DB::select("select count(*) as cant from inv_control_stock_conteo where cantidad_fisica is not null and cantidad_fisica<>cantidad_stock and id_movimiento is null and id_ctrolstk=".$conteo->id_ctrolstk);
            if($diferencias[0]->cant==0){
                $control = ControlStock::find($conteo->id_ctrolstk);
                $control->id_estado = 8;    //Resuelto (Diferencias Ajustadas)
                $control->save();
            }
        }

        DB::commit();

        $data['msg']="Ajuste Realizado!";
        return $data;
    }

    public function ajustarselected(Request $request)
    {
        DB::beginTransaction();
        $datos = $request->input('datos');
        foreach ($datos as $id){
            $conteo = ControlStockConteo::find($id);

            $movstk = new MovimientoStock();
            $movstk->id_motivo = 3;
            $movstk->id_item = $conteo->id_item;
            $movstk->id_sector_d = $conteo->id_sector;
            $movstk->id_ubicacion_d = $conteo->id_ubicacion;
            $movstk->id_posicion_d = $conteo->id_posicion;
            $movstk->cantidad = $conteo->cantidad_fisica - $conteo->cantidad_stock;
            $movstk->save();

            $stk = new Stock();
            $stk->id_item = $movstk->id_item;
            $stk->id_sector = $movstk->id_sector_d;
            $stk->id_ubicacion = $movstk->id_ubicacion_d;
            $stk->id_posicion = $movstk->id_posicion_d;
            $stk->cantidad = $movstk->cantidad;
            $stk->id_movstk = $movstk->id;
            $stk->save();

            $conteo->id_movimiento = $movstk->id;
            $conteo->save();

            //Valido Estado del Control
            $pendientes = DB::select("select count(*) as cant from inv_control_stock_conteo where cantidad_fisica is null and id_ctrolstk=".$conteo->id_ctrolstk);
            if($pendientes[0]->cant==0){
                $diferencias = DB::select("select count(*) as cant from inv_control_stock_conteo where cantidad_fisica is not null and cantidad_fisica<>cantidad_stock and id_movimiento is null and id_ctrolstk=".$conteo->id_ctrolstk);
                if($diferencias[0]->cant==0){
                    $control = ControlStock::find($conteo->id_ctrolstk);
                    $control->id_estado = 8;    //Resuelto (Diferencias Ajustadas)
                    $control->save();
                }
            }
        }
        DB::commit();
        return;
    }
}
