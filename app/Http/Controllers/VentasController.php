<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class VentasController extends Controller
{
    public function ventas($meses=null,$sucursal=null)
    {
        switch ($meses){
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                $date1 = 'DATE_ADD(DATE_ADD(LAST_DAY(DATE_ADD(NOW(), INTERVAL -'.$meses.' MONTH)),INTERVAL 1 DAY),INTERVAL - 1 MONTH)';
                $date2 = 'LAST_DAY(DATE_ADD(NOW(), INTERVAL -'.$meses.' MONTH))';
                break;
            default:
                $date1 = 'DATE_ADD(DATE_ADD(LAST_DAY(NOW()),INTERVAL 1 DAY),INTERVAL - 1 MONTH)';
                $date2 = 'LAST_DAY(NOW())';
        }

        if(!empty($sucursal)){
            $joinsucursal =  "join cajas ca on ca.id = c.id_caja and ca.id_sucursal = ".$sucursal;
        }else{
            $joinsucursal =  "";
        }

        $now = time();
        $temptable = 'zvtasdash'.$now;
        DB::select('CREATE TABLE IF NOT EXISTS '.$temptable.' ( INDEX(id_mediopago), INDEX(id_estado,pasaron) ) ENGINE=INNODB AS
                         select c.id,
                                    c.created_at,
                                    c.id_caja,
                                    tc.descripcion as tipo,
                                    c.numero,
                                    t.descripcion as cliente,
                                    t.id_tipocliente,
                                    tcl.descripcion as tipo_cliente,
                                    c.total,
                                    a.descripcion as ajuste,
                                    c.id_estado,
                                    e.descripcion as estado,
                                    CASE WHEN e.id = 2 THEN (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at))
                                         ELSE null
                                    END as pasaron,
                                    CASE WHEN e.id = 2 THEN IF((TO_DAYS(CURDATE()) - TO_DAYS(c.created_at))>15,	"VENCIDO",	null)
                                         ELSE null
                                    END as vencido,
                                    cmp.id_cpte,
                                    cmp.id_mediopago,
                                    mp.descripcion as mediopago,
                                    cmp.importe,
                                    cmp.cuotas,
                                    cmp.numero_transaccion as numerotrans,
                                    b.descripcion as banco,
                                    cmp.fecha_cobro,
                                    cmp.fecha_vto,
                                    UPPER(u.name) as usuario
                            from comprobantes c
                            join comprobantes_mp cmp on cmp.id_cpte = c.id
                            '.$joinsucursal.'
                            join sys_tipocomprobantes tc on tc.id= c.id_tipocpte
                            join titulares t on t.id = c.id_titular
                            left join sys_tipoclientes tcl on tcl.id = t.id_tipocliente
                            left join conf_ajustes_precio a on a.id = c.id_ajuste
                            join sys_estados e on e.id = c.id_estado
                            join sys_medios_pago mp on mp.id = cmp.id_mediopago
                            left join conf_bancos b on b.id = cmp.id_banco
                            join users u on u.id = c.created_us
                            where date(c.created_at) BETWEEN date('.$date1.') AND date('.$date2.')
                              and c.id_tipocpte in (5,6,7)
                              and c.deleted_at is null');

        DB::select('SET lc_time_names = "es_ES"');
        $titulo = DB::select('select UPPER(CONCAT(MONTHNAME('.$date2.'),\' \', EXTRACT(YEAR FROM '.$date2.'))) as titulo');

        $ingresos = DB::select('SELECT FORMAT(SUM(importe),2, "es_AR") as total,
                                       SUM(importe) as totalsf
                                  FROM '.$temptable.'
                                 WHERE id_mediopago != 2');

        if ($ingresos[0]->total>0){
            $efectivo = DB::select('SELECT FORMAT(SUM(importe),2, "es_AR") as total,
                                           ROUND((SUM(importe))*100/'.$ingresos[0]->totalsf.', 0) as porcentaje
                                      FROM '.$temptable.'
                                 WHERE id_mediopago = 1');

            $cheque = DB::select('SELECT FORMAT(SUM(importe),2, "es_AR") as total,
                                           ROUND((SUM(importe))*100/'.$ingresos[0]->totalsf.', 0) as porcentaje
                                      FROM '.$temptable.'
                                 WHERE id_mediopago = 5');

            $tarjetas = DB::select('SELECT FORMAT(SUM(importe),2, "es_AR") as total,
                                           ROUND((SUM(importe))*100/'.$ingresos[0]->totalsf.', 0) as porcentaje
                                      FROM '.$temptable.'
                                 WHERE id_mediopago in (3,4)');

            $otrosmp = DB::select('SELECT FORMAT(SUM(importe),2, "es_AR") as total,
                                          ROUND((SUM(importe))*100/'.$ingresos[0]->totalsf.', 0) as porcentaje
                                      FROM '.$temptable.'
                                 WHERE id_mediopago in (6,7)');
        }

        $ctacte = DB::select('SELECT FORMAT(SUM(importe),2, "es_AR") as total,
                                       SUM(importe) as totalsf
                                  FROM '.$temptable.'
                                 WHERE id_mediopago = 2');

        if ($ctacte[0]->total>0){
            $ctactesv = DB::select('SELECT FORMAT(SUM(importe),2, "es_AR") as total,
                                           ROUND((SUM(importe))*100/'.$ctacte[0]->totalsf.', 0) as porcentaje
                                      FROM '.$temptable.'
                                     WHERE id_mediopago = 2
                                       AND id_estado = 2
                                       AND pasaron < 16');

            $ctactevdo = DB::select('SELECT FORMAT(SUM(importe),2, "es_AR") as total,
                                           ROUND((SUM(importe))*100/'.$ctacte[0]->totalsf.', 0) as porcentaje
                                      FROM '.$temptable.'
                                     WHERE id_mediopago = 2
                                       AND id_estado = 2
                                       AND pasaron > 15');

            $ctactepgo = DB::select('SELECT FORMAT(SUM(importe),2, "es_AR") as total,
                                           ROUND((SUM(importe))*100/'.$ctacte[0]->totalsf.', 0) as porcentaje
                                      FROM '.$temptable.'
                                     WHERE id_mediopago = 2
                                       AND id_estado = 1');
        }

        $promedio = DB::select('SELECT FORMAT(AVG(total),2, "es_AR") AS promedio
                                  FROM '.$temptable.'
                                 WHERE id_tipocliente = 1');

        $totalvtas = DB::select('SELECT FORMAT(SUM(total),2, "es_AR") AS total,
                                        SUM(total) AS totalsf
                                  FROM '.$temptable.'
                                     WHERE NOT ( id_mediopago = 2 AND id_estado = 1)');


        $vtasxmp = DB::select('select
                                        DATE_FORMAT(c.created_at,"%Y-%m") as period,
                                        ROUND((SUM(IF(mp.id=1,	c.total,	null)))*100/SUM(c.total), 0) as efectivo,
                                        ROUND((SUM(IF(mp.id=2,	c.total,	null)))*100/SUM(c.total), 0) as ctacte,
                                        ROUND((SUM(IF(mp.id=3 OR mp.id=4,	c.total,	null)))*100/SUM(c.total), 0) as tarjeta,
                                        ROUND((SUM(IF(mp.id=5 OR mp.id=6 OR mp.id=7,	c.total,	null)))*100/SUM(c.total), 0) as otros
                                from comprobantes c
                                join comprobantes_mp cmp on cmp.id_cpte = c.id
                                join sys_medios_pago mp on cmp.id_mediopago = mp.id
                                where c.id_tipocpte in (5,6,7)
                                  AND date(c.created_at) BETWEEN DATE_ADD('.$date1.', INTERVAL -6 MONTH) AND '.$date2.'
                                  and c.deleted_at is null
                                group by 1
                                order by 1');
        //dd( $vtasxmp );
        $vtasxmparray = [];
        foreach($vtasxmp as $item){
            $item->efectivo = (int) $item->efectivo;
            $item->ctacte = (int) $item->ctacte;
            $item->tarjeta = (int) $item->tarjeta;
            $item->otros = (int) $item->otros;
            array_push($vtasxmparray,(collect($item)->toArray()));
        }

        $ctactevs = DB::select('select 	DATE_FORMAT(c.created_at,"%Y-%m") as mes,
                                        ROUND((SUM(IF(c.id_estado=1,	c.total,	null)))*100/SUM(c.total), 0) as pago,
                                        ROUND((SUM(IF(c.id_estado=2,	c.total,	null)))*100/SUM(c.total), 0) as pendiente
                                from comprobantes c
                                join comprobantes_mp cmp on cmp.id_cpte = c.id and cmp.id_mediopago=2
                                where c.id_tipocpte in (5,6,7)
                                  AND c.created_at BETWEEN DATE_ADD('.$date1.', INTERVAL -6 MONTH) AND '.$date2.'
                                  and c.deleted_at is null
                                group by 1
                                order by 1');
        $ctactearray = [];
        array_push($ctactearray,['Meses','Pago','Pendiente']);
        foreach($ctactevs as $item){
            $item->pago = (int) $item->pago;
            $item->pendiente = (int) $item->pendiente;
            array_push($ctactearray,[$item->mes,$item->pago,$item->pendiente]);
        }

        $estadosxtipocl = DB::select('SELECT estado,
                                           FORMAT(SUM(IF(id_tipocliente=1,	total,	null)),2, "es_AR") as eventual,
                                           FORMAT(SUM(IF(id_tipocliente=2,  total,	null)),2, "es_AR") as moyorista,
                                           FORMAT(SUM(IF(id_tipocliente=3,	total,	null)),2, "es_AR") as empresa,
                                           FORMAT(SUM(IF(id_tipocliente=4,	total,	null)),2, "es_AR") as profesional,
                                           FORMAT(SUM(total),2, "es_AR") as total
                                      FROM '.$temptable.'
                                     GROUP BY 1');
        $estadosarray = [];
        $tipoclsarray = [];
        $estadostipoclarray = [];
        if (!empty($totalvtas[0]->totalsf)){
            $estados = DB::select('SELECT estado AS label,
                                      ROUND((SUM(total))*100/'.$totalvtas[0]->totalsf.', 0) AS value
                                  FROM '.$temptable.'
                                 GROUP BY 1');
            foreach($estados as $item){
                $item->value = (int) $item->value;
                array_push($estadosarray,(collect($item)->toArray()));
            }

            $tipoclientes = DB::select('SELECT tipo_cliente AS label,
                                               ROUND((SUM(total))*100/'.$totalvtas[0]->totalsf.', 0) AS value
                                          FROM '.$temptable.'
                                         GROUP BY 1');
            foreach($tipoclientes as $item){
                $item->value = (int) $item->value;
                array_push($tipoclsarray,(collect($item)->toArray()));
            }

            $estadostipocl=DB::select('SELECT estado AS nodo,
                                               "ESTADOS" AS nodopadre,
                                               ROUND((SUM(total))*100/'.$totalvtas[0]->totalsf.', 0) AS valor
                                          FROM '.$temptable.'
                                         GROUP BY 1,2
                                        UNION
                                        SELECT CONCAT(tipo_cliente,\' (\',estado,\')\') AS nodo,
                                               estado AS nodopadre,
                                               ROUND((SUM(total))*100/'.$totalvtas[0]->totalsf.', 0) AS valor
                                          FROM '.$temptable.'
                                         GROUP BY 1,2
                                         ORDER BY 2');
            $estadostipoclarray[0] = array('nodo','nodopadre','valor');
            array_push($estadostipoclarray,array('ESTADOS',null,0));
            foreach($estadostipocl as $item){
                $item->valor = (int) $item->valor;
                array_push($estadostipoclarray,array_values(collect($item)->toArray()));
            }
        }

        DB::delete('DROP TABLE '.$temptable);

        return view('ventas.home',[
            'titulo' => $titulo[0]->titulo,
            'mes' => $meses,
            'idsucursal' => $sucursal,
            'ingresos'   => empty($ingresos[0]->total)?0:$ingresos[0]->total,
            'efectivo'  => empty($efectivo[0]->total)?0:$efectivo[0]->total,
            'porc_efectivo'  => empty($efectivo[0]->total)?0:$efectivo[0]->porcentaje,
            'cheques'  => empty($cheque[0]->total)?0:$cheque[0]->total,
            'porc_cheques'  => empty($cheque[0]->total)?0:$cheque[0]->porcentaje,
            'tarjetas'  => empty($tarjetas[0]->total)?0:$tarjetas[0]->total,
            'porc_tarjetas'  => empty($tarjetas[0]->total)?0:$tarjetas[0]->porcentaje,
            'otrosmp'   => empty($otrosmp[0]->total)?0:$otrosmp[0]->total,
            'porc_otrosmp'   => empty($otrosmp[0]->total)?0:$otrosmp[0]->porcentaje,
            'ctacte'   => empty($ctacte[0]->total)?0:$ctacte[0]->total,
            'ctactesv'  => empty($ctactesv[0]->total)?0:$ctactesv[0]->total,
            'porc_ctactesv'  => empty($ctactesv[0]->total)?0:$ctactesv[0]->porcentaje,
            'ctactevdo'  => empty($ctactevdo[0]->total)?0:$ctactevdo[0]->total,
            'porc_ctactevdo'  => empty($ctactevdo[0]->total)?0:$ctactevdo[0]->porcentaje,
            'ctactepgo'   => empty($ctactepgo[0]->total)?0:$ctactepgo[0]->total,
            'porc_ctactepgo'   => empty($ctactepgo[0]->total)?0:$ctactepgo[0]->porcentaje,
            'promedio' => empty($promedio[0]->promedio)?0:$promedio[0]->promedio,
            'totalvtas' => empty($totalvtas[0]->totalsf)?0:$totalvtas[0]->total,
            'diascalle' => '...',

            'estadosxtipocl' => $estadosxtipocl,
            'vtasxmp' => $vtasxmparray,
            'estados' => $estadosarray,
            'tipoclientes' => $tipoclsarray,
            'estadosxtipo' => $estadostipoclarray,
            'ctactevs' => json_encode($ctactearray, JSON_UNESCAPED_SLASHES )
        ]);
    }

    public function sabana()
    {
        return view('ventas.sabana',[
            'titulo' => 'Listado Sabana',
            'itemnav' => 'list'
        ]);
    }

    public function getsabana(Request $request)
    {
        $items = DB::select('select c.id,
                                    c.created_at,
                                    c.id_caja,
                                    s.id as sucursal,
                                    tc.descripcion as tipo,
                                    c.numero,
                                    t.descripcion as cliente,
                                    tcl.descripcion as tipo_cliente,
                                    FORMAT(c.total,2, "es_AR") as total,
                                    a.descripcion as ajuste,
                                    e.descripcion as estado,
                                    CASE WHEN e.id = 2 THEN (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at))
                                         ELSE null
                                    END as pasaron,
                                    CASE WHEN e.id = 2 THEN IF((TO_DAYS(CURDATE()) - TO_DAYS(c.created_at))>15,	"VENCIDO",	null)
                                         ELSE null
                                    END as vencido,
                                    cmp.id_cpte,
                                    cmp.id_mediopago,
                                    mp.descripcion as mediopago,
                                    FORMAT(cmp.importe,2, "es_AR") as importe,
                                    cmp.cuotas,
                                    cmp.numero_transaccion as numerotrans,
                                    b.descripcion as banco,
                                    cmp.fecha_cobro,
                                    cmp.fecha_vto,
                                    UPPER(u.name) as usuario
                            from comprobantes c
                            join comprobantes_mp cmp on cmp.id_cpte = c.id
                            left join cajas ca on ca.id = c.id_caja
                            left join sucursales s on s.id = ca.id_sucursal
                            join sys_tipocomprobantes tc on tc.id= c.id_tipocpte
                            join titulares t on t.id = c.id_titular
                            left join sys_tipoclientes tcl on tcl.id = t.id_tipocliente
                            left join conf_ajustes_precio a on a.id = c.id_ajuste
                            join sys_estados e on e.id = c.id_estado
                            join sys_medios_pago mp on mp.id = cmp.id_mediopago
                            left join conf_bancos b on b.id = cmp.id_banco
                            join users u on u.id = c.created_us
                            where date(c.created_at) BETWEEN "'.$request->desde.'" AND "'.$request->hasta.'"
                              and c.id_tipocpte in (5,6,7)
                              and c.deleted_at is null');

        return $items;
    }

    public function getdatadashboards($meses)
    {
        switch ($meses){
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                $date1 = 'DATE_ADD(DATE_ADD(LAST_DAY(DATE_ADD(NOW(), INTERVAL -'.$meses.' MONTH)),INTERVAL 1 DAY),INTERVAL - 3 MONTH)';
                $date2 = 'LAST_DAY(DATE_ADD(NOW(), INTERVAL -'.$meses.' MONTH))';
                break;
            default:
                $date1 = 'DATE_ADD(NOW(),INTERVAL - 3 MONTH)';
                $date2 = 'NOW()';
        }

        $sucursal = Auth::user()->id_sucursal;
        $total3m = DB::select('select SUM(total) as total
                                from comprobantes c
                                join comprobantes_mp cmp on cmp.id_cpte = c.id
                                left join cajas ca on ca.id = c.id_caja and ca.id_sucursal = '.$sucursal.'
                                where date(c.created_at) BETWEEN date('.$date1.') AND date('.$date2.')
                                  and c.id_tipocpte in (5,6,7)
                                  and c.deleted_at is null
                                  and NOT ( id_mediopago = 2 AND id_estado = 1)');
        $data['promedio3m']=$total3m[0]->total/3;


        //CTACTE
        ini_set('max_execution_time', '600');
        $now = time();

        //Busco los Pagos de Cada Cliente
        $temppagos = 'zpagos'.$now;
        DB::select('CREATE TABLE IF NOT EXISTS '.$temppagos.' ( INDEX(id_titular) ) ENGINE=INNODB AS
                    select id_titular, COALESCE(sum(total),0) as total
                      from comprobantes
                     where id_tipocpte in (7,13,14)
                       and id_titular in (SELECT distinct id_titular
                                           FROM comprobantes c
                                           JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                                          WHERE (
                                                  ( (c.id_tipocpte=5) AND (c.id_cpterel IS NULL) ) OR
                                                  ( (c.id_tipocpte=6) ) OR
                                                  ( (c.id_tipocpte=3) AND (c.id_cpterel IS NULL) ) OR
                                                  ( (c.id_tipocpte=3) AND ( (SELECT id_estado FROM comprobantes WHERE id_tipocpte=14 AND id=c.id_cpterel)=3 ) )
                                                 )
                                            AND c.deleted_at IS NULL
                                            AND c.id_titular != 2)
                       and date(created_at) <= date('.$date2.')
                    group by 1');

        $tempsaldocpte = 'zsaldos'.$now;
        DB::select('CREATE TABLE IF NOT EXISTS '.$tempsaldocpte.' ( INDEX(id) ) ENGINE=INNODB AS
                  select c.id, (select COALESCE(sum(c2.total),0)
                                  from comprobantes c2
                                  JOIN comprobantes_mp cmp2 ON cmp2.id_cpte = c2.id AND cmp2.id_mediopago = 2
                                 WHERE (
                                          ( (c2.id_tipocpte=5) AND (c2.id_cpterel IS NULL) ) OR
                                          ( (c2.id_tipocpte=6) ) OR
                                          ( (c2.id_tipocpte=3) AND (c2.id_cpterel IS NULL) ) OR
                                          ( (c2.id_tipocpte=3) AND ( (SELECT id_estado FROM comprobantes WHERE id_tipocpte=14 AND id=c2.id_cpterel)=3 ) )
                                        )
                                   AND c2.deleted_at IS NULL
                                   AND c2.id_titular != 2
                                   AND c2.id_titular=c.id_titular
                                   AND c2.id < c.id ) as total
                    from comprobantes c
                       JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                      WHERE (
                              ( (c.id_tipocpte=5) AND (c.id_cpterel IS NULL) ) OR
                              ( (c.id_tipocpte=6) ) OR
                              ( (c.id_tipocpte=3) AND (c.id_cpterel IS NULL) ) OR
                              ( (c.id_tipocpte=3) AND ( (SELECT id_estado FROM comprobantes WHERE id_tipocpte=14 AND id=c.id_cpterel)=3 ) )
                             )
                        AND c.deleted_at IS NULL
                        AND c.id_titular != 2
                        AND c.id_estado = 2
                        AND date(c.created_at) <= date('.$date2.')');

        $temptable = 'zctactevda'.$now;
        DB::select('CREATE TABLE IF NOT EXISTS '.$temptable.' ( INDEX(pasaron) ) ENGINE=INNODB AS
                     SELECT c.id AS id,
                            c.id_tipocpte AS id_tipocpte,
                            c.id_titular AS id_titular,
                            t.descripcion AS cliente,
                            CASE  WHEN (select total from '.$temppagos.' p where p.id_titular=c.id_titular) = 0 THEN c.total
                                  WHEN (select total from '.$temppagos.' p where p.id_titular=c.id_titular) >=  ((select total from '.$tempsaldocpte.' c2 where c2.id=c.id)+c.total) THEN 0
                                  WHEN (select total from '.$temppagos.' p where p.id_titular=c.id_titular) > (select total from '.$tempsaldocpte.' c2 where c2.id=c.id) THEN (((select total from '.$tempsaldocpte.' c2 where c2.id=c.id)+c.total) - (select total from '.$temppagos.' p where p.id_titular=c.id_titular))
                                  WHEN (select total from '.$temppagos.' p where p.id_titular=c.id_titular) <=  (select total from '.$tempsaldocpte.' c2 where c2.id=c.id) THEN c.total
                            END as total,
                            c.id_estado AS id_estado,
                            (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) AS pasaron
                       FROM comprobantes c
                       JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                       JOIN titulares t ON t.id = c.id_titular
                      WHERE (
                              ( (c.id_tipocpte=5) AND (c.id_cpterel IS NULL) ) OR
                              (c.id_tipocpte=6) OR
                              ( (c.id_tipocpte=3) AND (c.id_cpterel IS NULL) ) OR
                              ( (c.id_tipocpte=3) AND ( (SELECT id_estado FROM comprobantes WHERE id_tipocpte=14 AND id=c.id_cpterel)=3 ) )
                             )
                        AND c.deleted_at IS NULL
                        AND c.id_titular != 2
                        AND c.id_estado = 2
                        AND date(c.created_at) <= date('.$date2.')');

        $saldo=0;
        $saldo=DB::table($temptable.' as cc')->sum('total');
        $data['ctactetotal']=floatval($saldo);
        $data['diascalle']=floatval($saldo)*30/$data['promedio3m'];

        if ($saldo>0){
            $data['totalvencido']=floatval(DB::table($temptable.' as cc')
                ->where('cc.pasaron','>', 15)
                ->sum('cc.total'));

            $data['totalsvencer']=floatval(DB::table($temptable.' as cc')
                ->where('cc.pasaron','<', 16)
                ->sum('cc.total'));

            $data['saldov2']=floatval(DB::table($temptable.' as cc')
                ->whereBetween('cc.pasaron', [91,180])
                ->sum('cc.total'));

            $data['saldov3']=floatval(DB::table($temptable.' as cc')
                ->where('cc.pasaron','>', 180)
                ->sum('cc.total'));
        }else{
            $data['totalvencido']=0;$data['saldov1']=0;$data['saldov2']=0;$data['saldov3']=0;
        }

        DB::delete('DROP TABLE '.$temppagos);
        DB::delete('DROP TABLE '.$tempsaldocpte);
        DB::delete('DROP TABLE '.$temptable);

        return $data;
    }
}
