<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Material;
use DB;
use Illuminate\Http\Request;
use Excel;

class MaterialesController extends Controller
{
    //
    public function index()
    {
        $items = Material::all();
        return view('master.index', ['titulo' => 'Materiales','urlkey' => 'materiales','items' => $items, 'itemnav' => 'otro']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
        ]);


        $material = $request->all();
        $material['descripcion'] = strtoupper($material['descripcion']);

        if (isset($material['activo'])){
            $material['activo']=1;
        }else{
            $material['activo']=0;
        }
        $data=Material::create($material);
        if ($material['activo']==1){
            $data['activo']='SI';
        }else{
            $data['activo']='NO';
        }
        $data['msg']="Material Creado Exitosamente!";
        return $data;
    }

    public function edit($id)
    {
        $item = Material::find($id);
        return view('master.edit', ['titulo' => 'Materiales','urlkey' => 'materiales' ,'item' => $item]);

    }

    public function show($id)
    {
        $item = Material::find($id);
        return view('master.show', ['titulo' => 'Materiales','urlkey' => 'materiales' ,'item' => $item]);
    }


    public function update($id,Request $request)
    {
        $material = Material::find($id);
        $material->descripcion = strtoupper($request->descripcion);

        if (isset($request->activo)){
            $material->activo=1;
        }else{
            $material->activo=0;
        }

        $material->save();
        $data['msg']="Marca Modificada.";
        return $data;

    }

    public function delete($id)
    {
        $material = Material::find($id);
        $material->delete();
        $data['msg']="Material Eliminado!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $material = Material::find($item);
            $material->delete();
        }
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $items[] = ['descripcion' => $value->descripcion, 'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_materiales')->insert($items);
                    return redirect('/materiales')->with('status', 'Materiales Importados Correctamente!');
                }else{
                    return redirect('/materiales')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }
    }
}
