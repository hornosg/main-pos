<?php

namespace App\Http\Controllers;

use App\Models\Subrubro;
use Illuminate\Support\Facades\Input;
use App\Models\Item;
use App\Models\Marca;
use App\Models\Moneda;
use App\Models\Rubro;
use App\Models\UnidadMedida;
use App\Models\Material;
use App\Models\Linea;
use App\Models\MovimientoStock;
use App\Models\Stock;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class ProductosController extends Controller
{
    //
    public function index()
    {
        if (Auth::user()->id_rol==5){
            $productos = DB::select('select p.*,ma.descripcion as marca, ru.descripcion as rubro,  sr.descripcion as subrubro,mo.descripcion as moneda,
                                            UPPER(concat_ws(\' \', p.contNeto, um.descripcion)) as contenidoNeto,
                                            FORMAT((p.precioCosto + (p.precioCosto * gf.porcentaje/100)),2, "es_AR") as preciodist
                                        from items p
                                        left join conf_marcas ma on ma.id=p.id_marca
                                        left join conf_rubros ru on ru.id=p.id_rubro
                                        left join conf_subrubros sr on sr.id=p.id_subrubro
                                        left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                        left join sys_monedas mo on mo.id=p.id_moneda
                                        join conf_ganancia_fija gf on gf.id_tipocliente=2
                                        where id_tipoitem=5
                                          and deleted_at is null
                                          and distribuye = 1');
        }else{
            $productos = DB::select('select p.*,ma.descripcion as marca, ru.descripcion as rubro,  sr.descripcion as subrubro,mo.descripcion as moneda,
                                            UPPER(concat_ws(\' \', p.contNeto, um.descripcion)) as contenidoNeto,
                                            (SELECT GROUP_CONCAT(id_titular) FROM items_proveedores  WHERE id_item=p.id) as proveedores
                                        from items p
                                        left join conf_marcas ma on ma.id=p.id_marca
                                        left join conf_rubros ru on ru.id=p.id_rubro
                                        left join conf_subrubros sr on sr.id=p.id_subrubro
                                        left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                        left join sys_monedas mo on mo.id=p.id_moneda
                                        where id_tipoitem=5
                                          and deleted_at is null');
        }
        return view('items.productos.index', ['titulo' => 'Catalogo de Productos','urlkey' => 'productos','items' => $productos]);
    }

    public function create()
    {
        $productos = DB::select("select p.id, ma.descripcion as marca, p.contNeto as contneto, um.descripcion as unmedvta, p.descripcion as descripcion,
                                         concat_ws(' ',CASE WHEN p.contNeto is not null THEN p.contNeto END,CASE WHEN um.descripcion is not null THEN um.descripcion END) as contenidoNeto,
                                         p.precio
                                from items p
                                left join conf_marcas ma on ma.id=p.id_marca
                                left join conf_rubros ru on ru.id=p.id_rubro
                                left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                where id_tipoitem=5 or id_tipoitem=6");
        $marcas = Marca::all();
        $rubros = Rubro::all();
        $unidadesm = UnidadMedida::all();
        $materiales = Material::all();
        $lineas = Linea::all();
        $proveedores = DB::select("select * from titulares t where id_tipotitular=3 and activo=1");
        return view('items.productos.create', [ 'titulo' => 'Productos',
                                                'urlkey' => 'productos',
                                                'item' => 'Producto',
                                                'productos' => $productos,
                                                'unidadesm' => $unidadesm,
                                                'marcas' => $marcas,
                                                'rubros' => $rubros,
                                                'materiales' => $materiales,
                                                'lineas' => $lineas,
                                                'proveedores'=>$proveedores,
                                                'accion'=>'Crear']
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
            'contNeto'=> 'required',
            'id_unidadcontneto'=> 'required',
            'id_unidadventa'=> 'required',
            'id_moneda'=> 'required',
            'precioLista'=> 'required',
            'ganancia'=> 'required'
        ]);

        //dd($request->request);
        DB::beginTransaction();
            $producto = new Item;

            $request->descripcion=str_replace('"','',$request->descripcion);
            $request->descripcion=str_replace("'",'',$request->descripcion);

            $producto->descripcion = strtoupper($request->descripcion);
            $producto->id_tipoitem = $request->id_tipoitem;
            $producto->ean = $request->ean;
            $producto->id_unidadventa = $request->id_unidadventa;
            $producto->contNeto = $request->contNeto;
            $producto->id_unidadcontneto = $request->id_unidadcontneto;
            $producto->peso = $request->peso;
            $producto->largo = $request->largo;
            $producto->ancho = $request->ancho;
            $producto->alto = $request->alto;
            $producto->id_marca = $request->id_marca;

            if (!empty($request->id_rubro)){
                $producto->id_rubro = $request->id_rubro;
                $producto->id_subrubro = $request->id_subrubro;
            }else{
                if (!empty($request->id_subrubro)){
                    $producto->id_subrubro = $request->id_subrubro;
                    $producto->id_rubro = Subrubro::find($request->id_subrubro)->id_rubro;
                }
            }


            $producto->id_linea = $request->id_linea;
            $producto->id_material = $request->id_material;
            $producto->id_moneda = $request->id_moneda;
            $producto->puntoRepocicion = $request->puntoRepocicion;
            $producto->stockMinimo = $request->stockMinimo;
            $producto->precioLista = $request->precioLista;
            $producto->bonifProv = $request->bonifProv;
            $producto->divisor = $request->divisor;
            $producto->precioCosto = $request->precioCosto;
            $producto->ganancia = $request->ganancia;
            $producto->precio = $request->precio;
            $producto->especificaciones = $request->especificaciones;
            $producto->observaciones = $request->observaciones;

            if (isset($request->activo)){
                $producto->activo=1;
            }else{
                $producto->activo=0;
            }

            if (isset($request->distribuye)){
                $producto->distribuye=1;
            }else{
                $producto->distribuye=0;
            }

            $producto->save();

            if(isset($request->ups)){
                $upsarray=json_decode($request->ups);
                foreach ($upsarray as $up) {
                    $r = DB::table('items_up_sell')
                        ->where([['id_item','=',$producto->id],
                            ['id_itemup','=',$up->id]])->get();
                    if($r->count()===0)
                        DB::table('items_up_sell')->insert(
                            ['id_item' => $producto->id, 'id_itemup' => $up->id]
                        );
                }
            }
            if(isset($request->cross)){
                $crossarray=json_decode($request->cross);
                foreach ($crossarray as $cross) {
                    $r = DB::table('items_cross_sell')
                        ->where([['id_item','=',$producto->id],
                            ['id_itemcross','=',$cross->id]])->get();
                    if($r->count()===0)
                        DB::table('items_cross_sell')->insert(
                            ['id_item' => $producto->id, 'id_itemcross' => $cross->id]
                        );
                }
            }
            if(isset($request->provs)){
                $provsarray=json_decode($request->provs);
                foreach ($provsarray as $prov) {
                    $r = DB::table('items_proveedores')
                        ->where([['id_item','=',$producto->id],
                            ['id_titular','=',$prov->idproveedor]])->get();
                    if($r->count()===0){
                        if ($prov->actualizaprecio == 'SI'){
                            $prov->actualizaprecio=1;
                        }else{
                            $prov->actualizaprecio=0;
                        }
                        DB::table('items_proveedores')->insert(
                            ['id_item' => $producto->id,
                             'id_titular' => $prov->idproveedor,
                             'codproducto' => $prov->codproducto,
                             'actualizaprecio' => $prov->actualizaprecio ]
                        );
                    }
                }
            }
        DB::commit();

        if ($request->action=='save'){
            return redirect('/productos')->with('status', 'Item Creado Exitosamente!');
        }else{
            return redirect('/productos/create')->with('status', 'Item Creado Exitosamente!');
        }
    }

    public function edit($id)
    {
        $productos = DB::select("select p.*, ma.descripcion as marca, p.contNeto as contneto, um.descripcion as unmedvta, p.descripcion as descripcion,
                                         concat_ws(' ',CASE WHEN p.contNeto is not null THEN p.contNeto END,CASE WHEN um.descripcion is not null THEN um.descripcion END) as contenidoNeto
                                from items p
                                left join conf_marcas ma on ma.id=p.id_marca
                                left join conf_rubros ru on ru.id=p.id_rubro
                                left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                where p.id != ".$id." and (id_tipoitem=5 or id_tipoitem=6)");
        $producto = Item::find($id);

        $marcas = Marca::all();
        $rubros = Rubro::all();
        $unidadesm = UnidadMedida::all();
        $materiales = Material::all();
        $lineas = Linea::all();

        $ups =  DB::select("select p.*, ma.descripcion as marca, p.contNeto as contneto, um.descripcion as unmedvta, p.descripcion as descripcion,
                                         concat_ws(' ',CASE WHEN p.contNeto is not null THEN p.contNeto END,CASE WHEN um.descripcion is not null THEN um.descripcion END) as contenidoNeto,
                                         p.precio
                                from items p
                                left join conf_marcas ma on ma.id=p.id_marca
                                left join conf_rubros ru on ru.id=p.id_rubro
                                left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                join items_up_sell us on us.id_itemup=p.id and us.id_item= ".$id."
                                where id_tipoitem=5 or id_tipoitem=6");
        $cross =  DB::select("select p.*, ma.descripcion as marca, p.contNeto as contneto, um.descripcion as unmedvta, p.descripcion as descripcion,
                                         concat_ws(' ',CASE WHEN p.contNeto is not null THEN p.contNeto END,CASE WHEN um.descripcion is not null THEN um.descripcion END) as contenidoNeto,
                                         p.precio
                                from items p
                                left join conf_marcas ma on ma.id=p.id_marca
                                left join conf_rubros ru on ru.id=p.id_rubro
                                left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                join items_cross_sell cs on cs.id_itemcross=p.id and cs.id_item= ".$id."
                                where id_tipoitem=5 or id_tipoitem=6");
        $proveedores = DB::select("select * from titulares t where id_tipotitular=3 and activo=1");
        $provs =  DB::select("select ip.*, p.id as idproveedor, p.descripcion as proveedor
                                from items_proveedores ip
                                left join titulares p on id_tipotitular=3 and p.id = ip.id_titular
                                where ip.id_item= ".$id);
        return view('items.productos.show-edit',
                    ['titulo' => 'Productos',
                    'urlkey' => 'productos',
                    'item' => 'Producto',
                    'producto' => $producto,
                    'productos' => $productos,
                    'unidadesm' => $unidadesm,
                    'marcas' => $marcas,
                    'rubros' => $rubros,
                    'materiales' => $materiales,
                    'lineas' => $lineas,
                    'cross' => $cross,
                    'ups' => $ups,
                    'proveedores' => $proveedores,
                    'provs' => $provs,
                    'edit'=> 1,
                    'accion'=>'Editar']
        );

    }

    public function show($id)
    {
        $productos = DB::select("select p.*, ma.descripcion as marca, p.contNeto as contneto, um.descripcion as unmedvta, p.descripcion as descripcion,
                                         concat_ws(' ',CASE WHEN p.contNeto is not null THEN p.contNeto END,CASE WHEN um.descripcion is not null THEN um.descripcion END) as contenidoNeto,
                                         p.precio
                                from items p
                                left join conf_marcas ma on ma.id=p.id_marca
                                left join conf_rubros ru on ru.id=p.id_rubro
                                left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                where p.id != ".$id." and (id_tipoitem=5 or id_tipoitem=6)");
        $producto = Item::find($id);

        $marcas = Marca::all();
        $rubros = Rubro::all();
        $unidadesm = UnidadMedida::all();
        $materiales = Material::all();
        $lineas = Linea::all();
        $ups =  DB::select("select p.*, ma.descripcion as marca, p.contNeto as contneto, um.descripcion as unmedvta, p.descripcion as descripcion,
                                         concat_ws(' ',CASE WHEN p.contNeto is not null THEN p.contNeto END,CASE WHEN um.descripcion is not null THEN um.descripcion END) as contenidoNeto,
                                         p.precio
                                from items p
                                left join conf_marcas ma on ma.id=p.id_marca
                                left join conf_rubros ru on ru.id=p.id_rubro
                                left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                join items_up_sell us on us.id_itemup=p.id and us.id_item= ".$id."
                                where id_tipoitem=5 or id_tipoitem=6");
        $cross =  DB::select("select p.*, ma.descripcion as marca, p.contNeto as contneto, um.descripcion as unmedvta, p.descripcion as descripcion,
                                         concat_ws(' ',CASE WHEN p.contNeto is not null THEN p.contNeto END,CASE WHEN um.descripcion is not null THEN um.descripcion END) as contenidoNeto,
                                         p.precio
                                from items p
                                left join conf_marcas ma on ma.id=p.id_marca
                                left join conf_rubros ru on ru.id=p.id_rubro
                                left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                join items_cross_sell cs on cs.id_itemcross=p.id and cs.id_item= ".$id."
                                where id_tipoitem=5 or id_tipoitem=6");
        $proveedores = DB::select("select * from titulares t where id_tipotitular=3 and activo=1");
        $provs =  DB::select("select ip.*, p.id as idproveedor, p.descripcion as proveedor
                                from items_proveedores ip
                                left join titulares p on id_tipotitular=3 and p.id = ip.id_titular
                                where ip.id_item= ".$id);
        return view('items.productos.show-edit',
            ['titulo' => 'Productos',
                'urlkey' => 'productos',
                'item' => 'Producto',
                'producto' => $producto,
                'productos' => $productos,
                'unidadesm' => $unidadesm,
                'marcas' => $marcas,
                'rubros' => $rubros,
                'materiales' => $materiales,
                'lineas' => $lineas,
                'cross' => $cross,
                'ups' => $ups,
                'proveedores' => $proveedores,
                'provs' => $provs,
                'edit'=> 0,
                'accion'=>'Visualizar']
        );
    }

    public function update($id,Request $request)
    {
        //dd($request->request);
        DB::beginTransaction();
            $producto = Item::find($id);

            $request->descripcion=str_replace('"','',$request->descripcion);
            $request->descripcion=str_replace("'",'',$request->descripcion);

            $producto->descripcion = strtoupper($request->descripcion);
            $producto->id_tipoitem = $request->id_tipoitem;
            $producto->ean = $request->ean;
            $producto->id_unidadventa = $request->id_unidadventa;
            $producto->contNeto = $request->contNeto;
            $producto->id_unidadcontneto = $request->id_unidadcontneto;
            $producto->peso = $request->peso;
            $producto->largo = $request->largo;
            $producto->ancho = $request->ancho;
            $producto->alto = $request->alto;
            $producto->id_marca = $request->id_marca;

            if (!empty($request->id_rubro)){
                $producto->id_rubro = $request->id_rubro;
                $producto->id_subrubro = $request->id_subrubro;
            }else{
                if (!empty($request->id_subrubro)){
                    $producto->id_subrubro = $request->id_subrubro;
                    $producto->id_rubro = Subrubro::find($request->id_subrubro)->id_rubro;
                }
            }

            $producto->id_linea = $request->id_linea;
            $producto->id_material = $request->id_material;
            $producto->id_moneda = $request->id_moneda;
            $producto->puntoRepocicion = $request->puntoRepocicion;
            $producto->stockMinimo = $request->stockMinimo;
            $producto->precioLista = $request->precioLista;
            $producto->bonifProv = $request->bonifProv;
            $producto->divisor = $request->divisor;
            $producto->precioCosto = $request->precioCosto;
            $producto->ganancia = $request->ganancia;
            $producto->precio = $request->precio;
            $producto->especificaciones = $request->especificaciones;
            $producto->observaciones = $request->observaciones;

            //if (isset($request->esCombo)){
            //    $producto->esCombo=1;
            //}else{
            //    $producto->esCombo=0;
            //}

            if (isset($request->activo)){
                $producto->activo=1;
            }else{
                $producto->activo=0;
            }

            if (isset($request->distribuye)){
                $producto->distribuye=1;
            }else{
                $producto->distribuye=0;
            }

            $producto->save();

            if(isset($request->ups)){
                $upsarray=json_decode($request->ups);
                foreach ($upsarray as $up) {
                    $r = DB::table('items_up_sell')
                        ->where([['id_item','=',$producto->id],
                            ['id_itemup','=',$up->id]])->get();
                    if($r->count()===0)
                        DB::table('items_up_sell')->insert(
                            ['id_item' => $producto->id, 'id_itemup' => $up->id]
                        );
                }
            }
            if(isset($request->cross)){
                $crossarray=json_decode($request->cross);
                foreach ($crossarray as $cross) {
                    $r = DB::table('items_cross_sell')
                        ->where([['id_item','=',$producto->id],
                            ['id_itemcross','=',$cross->id]])->get();
                    if($r->count()===0)
                        DB::table('items_cross_sell')->insert(
                            ['id_item' => $producto->id, 'id_itemcross' => $cross->id]
                        );
                }
            }
            if(isset($request->provs)){
                $provsarray=json_decode($request->provs);
                foreach ($provsarray as $prov) {

                    $r = DB::table('items_proveedores')
                        ->where([['id_item','=',$producto->id],
                            ['id_titular','=',$prov->idproveedor]])->get();
                    if($r->count()===0){
                        if ($prov->actualizaprecio == 'SI'){
                            $prov->actualizaprecio=1;
                        }else{
                            $prov->actualizaprecio=0;
                        }
                        DB::table('items_proveedores')->insert(
                            ['id_item' => $producto->id,
                                'id_titular' => $prov->idproveedor,
                                'codproducto' => $prov->codproducto,
                                'actualizaprecio' => $prov->actualizaprecio ]
                        );
                    }
                }
            }
        DB::commit();

        return redirect('/productos')->with('status', 'Item Actualizado!');

    }

    public function delete($id)
    {
        $producto = Item::find($id);
        $producto->delete();
        return redirect('/productos')->with('status', 'Item Eliminado!');
    }

    public function removeup($idup)
    {
        $delete=DB::table('items_up_sell')->where('id_itemup', $idup)->delete();
        return $delete;
    }

    public function removecross($idcross)
    {
        $delete=DB::table('items_cross_sell')->where('id_itemcross', $idcross)->delete();
        return $delete;
    }

    public function removeprov($id)
    {
        $delete=DB::table('items_proveedores')->where('id', $id)->delete();
        return $delete;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $producto = Item::find($item);
            $producto->delete();
        }
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        set_time_limit(300);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data->chunk(1000) as $chunk){
                    foreach ($chunk  as $value) {
                        //var_dump($value);
                        if(empty($value->nombre)){
                            break;
                        }

                        $id_unidadventa = UnidadMedida::where('descripcion', $value->unidad_venta)->select('id')->first();
                        $id_unidadcontneto =UnidadMedida::where('descripcion', $value->unidad_cont_neto)->select('id')->first();
                        $id_marca = Marca::where('descripcion', $value->marca)->select('id')->first();
                        $id_rubro = Rubro::where('descripcion', $value->rubro)->select('id')->first();
                        $id_subrubro = Subrubro::where('descripcion', $value->subrubro)->select('id')->first();
                        $id_moneda = Moneda::where('descripcion', $value->moneda)->select('id')->first();
                        if( (empty($id_moneda)) and (!empty($value->moneda))){
                            $moneda = New Moneda();
                            $moneda->descripcion = $value->moneda;
                            $moneda->activo = 1;
                            $moneda->save();
                            $id_unidadventa = $moneda->id;
                        }
                        $id_linea = Linea::where('descripcion', $value->linea)->select('id')->first();
                        $id_material	= Material::where('descripcion', $value->material)->select('id')->first();


                        $item    = ['descripcion'   => $value->nombre,
                                    'id_tipoitem'    => 5,
                                    'ean'	        => $value->codigo_barras,
                                    'id_unidadventa'	=> $id_unidadventa['id'],
                                    'contNeto'	    => $value->cont_neto,
                                    'id_unidadcontneto'=>$id_unidadcontneto['id'],
                                    'peso'	        => $value->peso,
                                    'largo'	        => $value->largo,
                                    'ancho'	        => $value->ancho,
                                    'alto'	        => $value->alto,
                                    'id_marca'	    => $id_marca['id'],
                                    'id_rubro'	    => $id_rubro['id'],
                                    'id_subrubro'	=> $id_subrubro['id'],
                                    'id_linea'	    => $id_linea['id'],
                                    'id_material'	=> $id_material['id'],
                                    'puntoRepocicion'=>$value->punto_reposicion,
                                    'stockMinimo'	=> $value->stock_minimo,
                                    'id_moneda'	    => $id_moneda['id'],
                                    'precioLista'	=> $value->precio_lista,
                                    'bonifProv'	    => $value->bonificacion,
                                    'precioCosto'	=> $value->precio_costo,
                                    'ganancia'	    => $value->ganancia,
                                    'precio'	    => $value->precio_venta,
                                    'especificaciones'=>null,
                                    'observaciones'	=> null,
                                    'activo'        => true,
                                    'oldid'        => $value->oldid];

                        Item::create($item);
                        $items[]=$item;
                    }
                }
                if(!empty($items)){
//                    DB::table('items')->insert($items);
                    $msj =  count($items).' Productos Importados Correctamente!';
                    return redirect('/productos')->with('status', $msj);
                }else{
                    return redirect('/productos')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }

    }

    public function getpcosto($id){
        $producto = Item::find($id);
        return $producto->precioCosto;
    }

    public function getprecio($id,Request $request){
        //dd($request->request);
        $producto = Item::find($id);
        $precio = $producto->precio;

        if(!empty($request->idcliente)){
            //Verifico que el Tipo de Cliente no tenga definida una ganancia fija, si es así el precio es igual al precio de costo + la ganacia fija
            $titular = DB::table('titulares as t')->where('t.id',$request->idcliente)->get();
            if(!empty($titular[0]->id_tipocliente)){
                $gananciafija = DB::table('conf_ganancia_fija as gf')
                                    ->where('gf.id_tipocliente',$titular[0]->id_tipocliente)
                                    ->where('gf.activo',1)
                                    ->get();
                if(!empty($gananciafija[0]->porcentaje)){
                    $precio = $producto->precioCosto + ($producto->precioCosto * $gananciafija[0]->porcentaje/100);
                }
            }
        }

        if(!empty($request->idajuste)){
            //Aplico el ajusto seleccionado sobre el precio hasta el momento calculado
            $ajuste = DB::table('conf_ajustes_precio as ap')
                ->where('ap.id',$request->idajuste)
                ->where('ap.activo',1)
                ->get();
            if(!empty($ajuste[0]->porcentaje)){
                $precio = $precio + (($precio * $ajuste[0]->porcentaje/100) * $ajuste[0]->signo);
            }
        }

        return $precio;
    }

    public function getsugerencias($id){
        $productoscross = DB::select('SELECT i.id,
                                            concat("[R] ",i.descripcion," - ", i.contNeto, " ", um.descripcion, " (", m.descripcion, ") $ ", i.precio) as producto,
                                            "cross" as tipo
                                        FROM items as i
                                        JOIN items_cross_sell as cs ON cs.id_itemcross = i.id and cs.id_item=?
                                   LEFT JOIN conf_marcas as m ON m.id = i.id_marca
                                   LEFT JOIN conf_unidades_medida as um ON um.id = i.id_unidadcontneto
                                       WHERE precio is not null'
                                    , [$id]);
        $productosups = DB::select('SELECT i.id,
                                            concat("[A] ",i.descripcion," - ", i.contNeto, " ", um.descripcion, " (", m.descripcion, ") $ ", i.precio) as producto,
                                            "ups" as tipo
                                        FROM items as i
                                        JOIN items_up_sell as up ON up.id_itemup = i.id and up.id_item=?
                                   LEFT JOIN conf_marcas as m ON m.id = i.id_marca
                                   LEFT JOIN conf_unidades_medida as um ON um.id = i.id_unidadcontneto
                                       WHERE precio is not null'
                                    , [$id]);

        $productos = array_merge($productosups,$productoscross);
        return $productos;
    }

    public function ajaxstore(Request $request)
    {
//        dd($request->request);
        $this->validate($request, [
            'descripcion' => 'required|max:255',
            'contNeto'=> 'required',
            'id_unidadcontneto'=> 'required',
            'id_unidadventa'=> 'required',
            'id_moneda'=> 'required',
            'precioLista'=> 'required',
            'ganancia'=> 'required'
        ]);

        DB::beginTransaction();
        $producto = new Item;

        $request->descripcion=str_replace('"','',$request->descripcion);
        $request->descripcion=str_replace("'",'',$request->descripcion);


        $producto->descripcion = $request->descripcion;
        $producto->id_tipoitem = 5;
        $producto->ean = $request->ean;
        $producto->id_unidadventa = $request->id_unidadventa;
        $producto->contNeto = $request->contNeto;
        $producto->id_unidadcontneto = $request->id_unidadcontneto;
        $producto->id_marca = $request->id_marca;
        $producto->id_rubro = $request->id_rubro;
        $producto->id_linea = $request->id_linea;
        $producto->id_material = $request->id_material;
        $producto->id_moneda = $request->id_moneda;
        $producto->precioLista = $request->precioLista;
        $producto->bonifProv = $request->bonifProv;
        $producto->precioCosto = $request->precioCosto;
        $producto->ganancia = $request->ganancia;
        $producto->precio = $request->precio;
        $producto->activo=1;
        $producto->save();

        //Vinculo al Producto con el Proveedor
        if((!empty($request->id_titular)) and (!empty($request->codproducto))){
            DB::table('items_proveedores')->insert(
                ['id_item' => $producto->id,
                    'id_titular' => $request->id_titular,
                    'codproducto' => $request->codproducto,
                    'actualizaprecio' => 1 ]
            );
        }

        //Genero el Stock Inicial
        if ($request->cantidad > 0){
            $movstk = new MovimientoStock();
            $movstk->id_motivo =8;
            $movstk->id_item =$producto->id;
            $movstk->id_sector_d =$request->id_sector_d;
            $movstk->id_ubicacion_d =$request->id_ubicacion_d;
            $movstk->id_posicion_d =$request->id_posicion_d;
            $movstk->cantidad =$request->cantidad;
            $movstk->save();
            $stk_d = new Stock();
            $stk_d->id_item = $movstk->id_item;
            $stk_d->id_sector = $movstk->id_sector_d;
            $stk_d->id_ubicacion = $movstk->id_ubicacion_d;
            $stk_d->id_posicion = $movstk->id_posicion_d;
            $stk_d->cantidad = $movstk->cantidad;
            $stk_d->id_movstk = $movstk->id;
            $stk_d->save();
        }
        DB::commit();

        $unimed = UnidadMedida::find($producto->id_unidadcontneto);
        $unimeddes = (!empty($unimed))?$unimed->descripcion:'';

        $producto->contenidoNeto=$producto->contNeto." ".$unimeddes;
        if (!empty($producto->id_marca)){
            $marca=Marca::find($producto->id_marca);
            $producto->marca=$marca->descripcion;
        }
        $moneda=Moneda::find($producto->id_moneda);
        //dd($producto);
        $producto->moneda=$moneda->descripcion;

        $data['msg']="Producto Creado Exitosamente!";
        $data['item']=$producto;
        $data['id']=$producto->id;
        $data['text']="(".$producto->id.") ".$producto->descripcion." ".$producto->contNeto." ".$unimeddes." $ ".$producto->precio ;
        return $data;
    }

    public function eanslist()
    {
        $productos = DB::select('select p.*,ma.descripcion as marca, sr.descripcion as subrubro,
                                        concat_ws(\' \', p.contNeto, um.descripcion) as contenidoNeto,
                                        CASE WHEN p.ean IS NULL THEN "null" ELSE p.ean END as ean
                                    from items p
                                    left join conf_marcas ma on ma.id=p.id_marca
                                    left join conf_subrubros sr on sr.id=p.id_subrubro
                                    left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                    where id_tipoitem=5 and p.activo=1');
        return view('items.productos.eanlist', ['titulo' => 'Codigos de Barra','urlkey' => 'productos','items' => $productos]);
    }


    public function eansupdate(Request $request)
    {
        $producto = Item::find($request->iditem);
        $producto->ean = $request->ean;
        $producto->save();
        $producto['msg']='EAN Modificado Exitosamente';
        return $producto;

    }

    public function nuevamarca(Request $request)
    {
        $item = new Marca();
        $item->descripcion = strtoupper($request->marca);
        $item->activo=1;
        $item->save();
        $item['msg']='Nueva Marca creada Exitosamente';
        return $item;

    }

    public function vincular(Request $request)
    {
        $idrubro = $request->idrubro;
        $idsubrubro = $request->idsubrubro;
        $stkmin = $request->stkmin;
        $proveedor = $request->proveedor;
        $items=$request->ids;
        foreach($items as $item){
            $producto = Item::find($item);
            if (!empty($idrubro)){
                $producto->id_rubro = $idrubro;
                $producto->id_subrubro = $idsubrubro;
            }else{
                if (!empty($idsubrubro)){
                    $producto->id_subrubro = $idsubrubro;
                    $producto->id_rubro = Subrubro::find($idsubrubro)->id_rubro;
                }
            }

            if (!empty($stkmin)){
                $producto->stockMinimo = $stkmin;
            }
            $producto->save();

            if (!empty($proveedor)){
                $r = DB::table('items_proveedores')
                    ->where([['id_item','=',$producto->id],
                        ['id_titular','=',$proveedor]])->get();
                if($r->count()===0){
                    DB::table('items_proveedores')->insert(
                        ['id_item' => $producto->id,
                            'id_titular' => $proveedor,
                            'codproducto' => 'SINCODIGO',
                            'actualizaprecio' => 1 ]
                    );
                }
            }
        }

        $productos = DB::select('select p.*,ma.descripcion as marca, ru.descripcion as rubro,  sr.descripcion as subrubro,mo.descripcion as moneda,
                                        case when p.activo=1 then "SI" else "NO" end as activo,
                                        UPPER(concat_ws(\' \', p.contNeto, um.descripcion)) as contenidoNeto,
                                        (SELECT GROUP_CONCAT(id_titular) FROM items_proveedores  WHERE id_item=p.id) as proveedores
                                    from items p
                                    left join conf_marcas ma on ma.id=p.id_marca
                                    left join conf_rubros ru on ru.id=p.id_rubro
                                    left join conf_subrubros sr on sr.id=p.id_subrubro
                                    left join conf_unidades_medida um on um.id=p.id_unidadcontneto
                                    left join sys_monedas mo on mo.id=p.id_moneda
                                    where id_tipoitem=5
                                      and deleted_at is null');
        return $productos;
    }

    public function getmasvendidos(Request $request)
    {
        if (!empty($request->proveedor)){
            $joinprov = ' JOIN items_proveedores ip on ip.id_item = i.id AND ip.id_titular = '.$request->proveedor;
        }else{
            $joinprov = ' LEFT JOIN items_proveedores ip on ip.id_item = i.id';
        }
        if (!empty($request->marca)){
            $joinmarca = ' JOIN conf_marcas m on m.id=i.id_marca AND i.id_marca = '.$request->marca;
        }else{
            $joinmarca = ' LEFT JOIN conf_marcas m on m.id=i.id_marca';
        }
        $items = DB::select('SELECT ci.id_item, 
                                    i.descripcion as producto, 
                                    m.descripcion as marca,
                                    t.descripcion as proveedor,
                                    count(*) as cantidad_cptes
                               FROM comprobantes_items ci
                               JOIN items i on i.id = ci.id_item
                               '.$joinmarca.'
                               '.$joinprov.'
                               LEFT JOIN titulares t on t.id = ip.id_titular
                               WHERE DATE(ci.created_at) BETWEEN "'.$request->desde.'" AND "'.$request->hasta.'"
                              GROUP BY 1,2,3,4
                              ORDER BY 5 desc
                              LIMIT 100');
        $data['xcptes'] = $items;

        $items2 = DB::select('SELECT ci.id_item, 
                                     i.descripcion as producto, 
                                     m.descripcion as marca,
                                     t.descripcion as proveedor,
                                     SUM(cantidad) as cantidad_item
                               FROM comprobantes_items ci
                               JOIN items i on i.id = ci.id_item
                               '.$joinmarca.'                               
                               '.$joinprov.'
                               LEFT JOIN titulares t on t.id = ip.id_titular
                               WHERE DATE(ci.created_at) BETWEEN "'.$request->desde.'" AND "'.$request->hasta.'"
                              GROUP BY 1,2,3,4
                              ORDER BY 5 desc
                              LIMIT 100');
        $data['xcantidad'] = $items2;

        $items3 = DB::select('SELECT ci.id_item, 
                                     i.descripcion as producto, 
                                     m.descripcion as marca,
                                     t.descripcion as proveedor,
                                     sum(importe),
                                     FORMAT(sum(importe),2,"es_AR") as importe
                               FROM comprobantes_items ci
                               JOIN items i on i.id = ci.id_item
                               '.$joinmarca.'
                               '.$joinprov.'
                               LEFT JOIN titulares t on t.id = ip.id_titular
                               WHERE DATE(ci.created_at) BETWEEN "'.$request->desde.'" AND "'.$request->hasta.'"
                              GROUP BY 1,2,3,4
                              ORDER BY 5 desc
                              LIMIT 100');
        $data['ximporte'] = $items3;

        return $data;
    }

}
