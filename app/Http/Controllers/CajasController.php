<?php

namespace App\Http\Controllers;

use App\Models\Comprobante;
use App\Models\ComprobanteMp;
use App\Models\Caja;
use App\Models\Titular;
use App\User;
use App\Models\TipoComprobante;
use App\Models\Notificacion;
use App\Models\NotificacionEstado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Models\CajaArqueo;

class CajasController extends Controller
{
    public function index()
    {
        $sucursal = Auth::user()->id_sucursal;
        $ingresos = DB::table('comprobantes as c')
                    ->Join('comprobantes_mp as cmp', 'cmp.id_cpte', '=', 'c.id')
                    ->Join('sys_medios_pago as mp', 'mp.id', '=', 'cmp.id_mediopago')
                    ->Join('sys_tipocomprobantes as tc', 'tc.id', '=', 'c.id_tipocpte')
                    ->leftJoin('titulares as t', 't.id', '=', 'c.id_titular')
                    ->leftJoin('users as us', 'us.id', '=', 'c.created_us')
                    ->whereNull('c.deleted_at')
                    ->whereIn('cmp.id_mediopago', [1,5])
                    ->whereRaw('c.id_tipocpte in (5,6,7,8,9,15)')
                    ->whereRaw('c.id_caja = (SELECT MAX(id) FROM cajas WHERE id_sucursal = '.$sucursal.')')
                    ->select('c.id','c.id_caja','c.numero','tc.descripcion as tipocpte','c.id_titular','cmp.importe as total','mp.descripcion as mediopago', DB::raw('date_format(c.created_at, "%d/%m %H:%i") as creado'),'t.descripcion as titular', 'us.name as usuario', DB::raw('date_format(c.updated_at, "%d/%m/%Y %H:%i:%s") as  ultmodif'));
        $items = DB::table('comprobantes as c')
                    ->Join('comprobantes_mp as cmp', 'cmp.id_cpte', '=', 'c.id')
                    ->Join('sys_medios_pago as mp', 'mp.id', '=', 'cmp.id_mediopago')
                    ->Join('sys_tipocomprobantes as tc', 'tc.id', '=', 'c.id_tipocpte')
                    ->leftJoin('titulares as t', 't.id', '=', 'c.id_titular')
                    ->leftJoin('users as us', 'us.id', '=', 'c.created_us')
                    ->whereNull('c.deleted_at')
                    ->whereIn('cmp.id_mediopago', [1,5])
                    ->whereRaw('c.id_tipocpte in (10,11,12,13)')
                    ->whereRaw('c.id_caja = (SELECT MAX(id) FROM cajas WHERE id_sucursal = '.$sucursal.')')
                    ->select('c.id','c.id_caja','c.numero','tc.descripcion as tipocpte','c.id_titular',DB::raw('cmp.importe*-1 as total'),'mp.descripcion as mediopago', DB::raw('date_format(c.created_at, "%d/%m %H:%i") as creado'),'t.descripcion as titular', 'us.name as usuario', DB::raw('date_format(c.updated_at, "%d/%m/%Y %H:%i:%s") as  ultmodif'))
                    ->union($ingresos)
                    ->get();

        $arqueo = DB::table('cajas_arqueo as caa')
                    ->whereRaw('caa.id_caja = (SELECT MAX(id) FROM cajas WHERE id_sucursal = '.$sucursal.')')
                    ->select('caa.*', DB::raw('(valor*cantidad) as total'))
                    ->get();
        $totalarqueo = DB::table('cajas_arqueo as caa')
                        ->whereRaw('caa.id_caja = (SELECT MAX(id) FROM cajas WHERE id_sucursal = '.$sucursal.')')
                        ->select(DB::raw('SUM(valor*cantidad) as sumtotal'))
                        ->get();
        $totalcptes = DB::select('SELECT SUM(total) as sumtotal
                                    FROM (
                                        select SUM(cmp.importe*-1) as total
                                          from comprobantes as c
                                          join comprobantes_mp as cmp on cmp.id_cpte = c.id
                                          join users as us on us.id = c.created_us
                                         where c.deleted_at is null
                                           and cmp.id_mediopago in (1,5)
                                           and c.id_tipocpte in (10,11,12,13)
                                           and c.id_caja = (SELECT MAX(id) FROM cajas WHERE id_sucursal = '.$sucursal.')
                                        union
                                        select SUM(cmp.importe) as total
                                          from comprobantes as c
                                          join comprobantes_mp as cmp on cmp.id_cpte = c.id
                                          join users as us on us.id = c.created_us
                                         where c.deleted_at is null
                                           and cmp.id_mediopago in (1,5)
                                           and c.id_tipocpte in (5,6,7,8,9,15)
                                           and c.id_caja = (SELECT MAX(id) FROM cajas WHERE id_sucursal = '.$sucursal.')
                                       ) comprobantes_caja');
        return view('caja.index',[
            'titulo' => 'Efectivo & Cheques',
            'urlkey' => 'caja',
            'items' => $items,
            'arqueo' => $arqueo,
            'totalarqueo' => $totalarqueo[0]->sumtotal>0?$totalarqueo[0]->sumtotal:0,
            'totalcptes' => $totalcptes[0]->sumtotal>0?$totalcptes[0]->sumtotal:0,
            'itemnav' => 'cajaactual'
        ]);
    }

    public function indexall()
    {
        $sucursal = Auth::user()->id_sucursal;
        $items = DB::table('comprobantes as c')
            ->Join('comprobantes_mp as cmp', 'cmp.id_cpte', '=', 'c.id')
            ->Join('sys_medios_pago as mp', 'mp.id', '=', 'cmp.id_mediopago')
            ->Join('sys_tipocomprobantes as tc', 'tc.id', '=', 'c.id_tipocpte')
            ->leftJoin('titulares as t', 't.id', '=', 'c.id_titular')
            ->leftJoin('users as us', 'us.id', '=', 'c.created_us')
            ->whereNull('c.deleted_at')
            ->whereRaw('c.id_tipocpte in (5,6,7,8,9,15)')
            ->whereRaw('c.id_caja = (SELECT MAX(id) FROM cajas WHERE id_sucursal = '.$sucursal.')')
            ->select('c.id','c.id_caja','c.numero','tc.descripcion as tipocpte','c.id_titular','cmp.importe as total','mp.descripcion as mediopago', DB::raw('date_format(c.created_at, "%d/%m/%Y %H:%i:%s") as creado'),'t.descripcion as titular', 'us.name as usuario', DB::raw('date_format(c.updated_at, "%d/%m/%Y %H:%i:%s") as  ultmodif'))
            ->get();

        return view('caja.indexall',[
            'titulo' => 'Todos los Comprobantes',
            'urlkey' => 'caja/total',
            'items' => $items,
            'itemnav' => 'cajatotal'
        ]);
    }

    public function indexold()
    {
        $items = DB::table('cajas as c')
            ->leftJoin('sucursales as s', 's.id', '=', 'c.id_sucursal')
            ->whereRaw('c.created_at BETWEEN DATE_ADD(NOW(), INTERVAL -1 MONTH) AND NOW()')
            ->select('c.*',
                     's.descripcion as sucursal',
                     DB::raw('date_format(c.created_at, "%d/%m/%Y %H:%i:%s") as creado'),
                     DB::raw('(SELECT FORMAT(COALESCE(SUM(importe),0),2, "es_AR")
                                 FROM comprobantes as c2
                                 JOIN comprobantes_mp as c2mp on c2mp.id_cpte=c2.id and c2mp.id_mediopago = 1
                                WHERE c2.deleted_at is null and id_caja=c.id and id_tipocpte in (5,6,7)) as efectivo'),
                     DB::raw('(SELECT FORMAT(COALESCE(SUM(importe),0),2, "es_AR")
                                 FROM comprobantes as c2
                                 JOIN comprobantes_mp as c2mp on c2mp.id_cpte=c2.id and c2mp.id_mediopago = 5
                                WHERE c2.deleted_at is null and id_caja=c.id and id_tipocpte in (5,6,7)) as cheque'),
                     DB::raw('(SELECT FORMAT(COALESCE(SUM(importe),0),2, "es_AR")
                                 FROM comprobantes as c2
                                 JOIN comprobantes_mp as c2mp on c2mp.id_cpte=c2.id and c2mp.id_mediopago in (3,4)
                                WHERE c2.deleted_at is null and id_caja=c.id and id_tipocpte in (5,6,7)) as tarjetas'),
                     DB::raw('(SELECT FORMAT(COALESCE(SUM(importe),0),2, "es_AR")
                                 FROM comprobantes as c2
                                 JOIN comprobantes_mp as c2mp on c2mp.id_cpte=c2.id and c2mp.id_mediopago in (6,7)
                                WHERE c2.deleted_at is null and id_caja=c.id and id_tipocpte in (5,6,7)) as depotransf'),
                     DB::raw('(SELECT FORMAT(COALESCE(SUM(importe),0),2, "es_AR")
                                 FROM comprobantes as c2
                                 JOIN comprobantes_mp as c2mp on c2mp.id_cpte=c2.id and c2mp.id_mediopago in (1,3,4,5,6,7)
                                WHERE c2.deleted_at is null and id_caja=c.id and id_tipocpte in (5,6,7)) as total')
                     )
            ->get();

        return view('caja.indexold',[
            'titulo' => 'Cajas Anteriores',
            'urlkey' => 'caja/cajasanteriores',
            'items' => $items,
            'itemnav' => 'cajaant'
        ]);
    }

    public function detalle($id){
        $items = DB::table('comprobantes as c')
            ->Join('comprobantes_mp as cmp', 'cmp.id_cpte', '=', 'c.id')
            ->Join('sys_medios_pago as mp', 'mp.id', '=', 'cmp.id_mediopago')
            ->Join('sys_tipocomprobantes as tc', 'tc.id', '=', 'c.id_tipocpte')
            ->leftJoin('titulares as t', 't.id', '=', 'c.id_titular')
            ->leftJoin('users as us', 'us.id', '=', 'c.created_us')
            ->whereNull('c.deleted_at')
            ->whereRaw('c.id_tipocpte in (5,6,7,8,9,10,11,15)')
            ->whereRaw('c.id_caja = '.$id)
            ->select('c.id','c.id_caja','c.numero','tc.descripcion as tipocpte','c.id_titular',DB::raw('FORMAT(cmp.importe,2, "es_AR") as total'),'mp.descripcion as mediopago', DB::raw('date_format(c.created_at, "%d/%m/%Y %H:%i:%s") as creado'),'t.descripcion as titular', 'us.name as usuario', DB::raw('date_format(c.updated_at, "%d/%m/%Y %H:%i:%s") as  ultmodif'))
            ->get();
        return $items;
    }

    public function storeArqueo(Request $request)
    {
        $sucursal = Auth::user()->id_sucursal;
        $caja = DB::select('SELECT MAX(id) as id FROM cajas WHERE id_sucursal=?',[$sucursal]);
        $arqueo = New CajaArqueo();
        $arqueo->id_caja = $caja[0]->id;
        $arqueo->tipo_efectivo = $request->tipo_efectivo;
        $arqueo->valor = $request->valor;
        $arqueo->cantidad = $request->cantidad;
        $arqueo->save();

        if ($arqueo['tipo_efectivo']==1){
            $arqueo['tipo_efectivo']='BILLETES';
        }
        if ($arqueo['tipo_efectivo']==2){
            $arqueo['tipo_efectivo']='MONEDAS';
        }
        if ($arqueo['tipo_efectivo']==3){
            $arqueo['tipo_efectivo']='CHEQUES';
        }
        $arqueo['msg']="Efectivo Registrado Exitosamente!";
        return $arqueo;
    }

    public function deleteArqueo($id)
    {
        $arqueo = CajaArqueo::find($id);
        $arqueo->delete();
        $data['msg']="Item Eliminado!";
        return $data;
    }

    public function cerrar()
    {
        $sucursal = Auth::user()->id_sucursal;
        $totalarqueo = DB::table('cajas_arqueo as caa')
            ->whereRaw('caa.id_caja = (SELECT MAX(id) FROM cajas WHERE id_sucursal = '.$sucursal.')')
            ->select(DB::raw('SUM(valor*cantidad) as sumtotal'))
            ->get();

        DB::beginTransaction();
            $caja = New Caja();
            $caja->id_sucursal=$sucursal;
            $caja->save();

            $cpte = New Comprobante();
            $cpte->id_tipocpte = 8;
            $cpte->id_titular = 1;
            $cpte->id_estado = 3;
            $cpte->id_caja = $caja->id;
            $cpte->total=$totalarqueo[0]->sumtotal>0?$totalarqueo[0]->sumtotal:0;
            $cpte->created_us = auth()->id();
            $cpte->updated_us = auth()->id();
            $cpte->save();

            $cptemp = new ComprobanteMp();
            $cptemp->id_cpte = $cpte->id;
            $cptemp->id_mediopago = 1;
            $cptemp->created_us = auth()->id();
            $cptemp->updated_us = auth()->id();
            $cptemp->importe = $cpte->total;
            $cptemp->save();
        DB::commit();

        return redirect('/caja')->with('status', 'Nueva Caja:'.$cpte->id.' Creada Exitosamente!');
    }

    public function ajustar($signo,Request $request)
    {
        $sucursal = Auth::user()->id_sucursal;
        $caja = DB::select('SELECT MAX(id) as id FROM cajas WHERE id_sucursal = '.$sucursal);
        DB::beginTransaction();
            $cpte = New Comprobante();
            $cpte->id_tipocpte = ($signo>0)?9:10;

            $cpte->serie='X';
            $cpte->id_titular = 1;
            $cpte->id_estado = 3;
            $cpte->id_caja = $caja[0]->id;
            $cpte->total=$request->importe;
            $cpte->observaciones=$request->motivo;
            $cpte->created_us = auth()->id();
            $cpte->updated_us = auth()->id();
            $cpte->save();

            $cptemp = new ComprobanteMp();
            $cptemp->id_cpte = $cpte->id;
            $cptemp->id_mediopago = 1;
            $cptemp->created_us = auth()->id();
            $cptemp->updated_us = auth()->id();
            $cptemp->importe = $cpte->total;
            $cptemp->save();

            $notificacion = New Notificacion();
            $cpte = Comprobante::find($cpte->id);
            if ($cpte->id_tipocpte == 9){
                $notificacion->icon = 'fa fa-plus-square text-aqua';
            }else{
                $notificacion->icon = 'fa fa-minus-square text-red';
            }
            $tipocpte = TipoComprobante::find($cpte->id_tipocpte);
            $notificacion->descripcion = 'Se Crea el '.$tipocpte->descripcion.' Nro: '.$cpte->numero;
            $notificacion->motivo = $request->motivo;
            $notificacion->created_us = auth()->id();
            $notificacion->save();

            foreach(DB::table('users')->whereNull('deleted_at')->get() as $user){
                $notiEstado = New NotificacionEstado();
                $notiEstado->id_notificacion = $notificacion->id;
                $notiEstado->id_user = $user->id;
                $notiEstado->id_estado = 14;
                $notiEstado->save();
            }
        DB::commit();

        $cpte = Comprobante::find($cpte->id);
        if ($cpte->id_tipocpte==9){
            $cpte['tipocpte']='AJUSTE POSITIVO';
        }
        if ($cpte->id_tipocpte==10){
            $cpte['tipocpte']='AJUSTE NEGATIVO';
            $cpte->total=$cpte->total*-1;
        }
        $cpte['titular']=Titular::find(1)->descripcion;
        $cpte['usuario']=User::find($cpte->created_us)->name;
        $cpte['creado']=$cpte->created_at->format('d/m/Y  H:i:s');
        $cpte['mediopago']='EFECTIVO';
        $cpte['msg']='Ajuste: '.$cpte->id.' Creado Exitosamente!';
        return $cpte;
    }

    public function movefectivo($signo,Request $request)
    {
        $sucursal = Auth::user()->id_sucursal;
        $caja = DB::select('SELECT MAX(id) as id FROM cajas WHERE id_sucursal = '.$sucursal);

        DB::beginTransaction();
            $cpte = New Comprobante();
            $cpte->id_tipocpte = ($signo>0)?15:11;
            $cpte->serie='X';
            $cpte->id_titular = 1;
            $cpte->id_estado = 3;
            $cpte->id_caja = $caja[0]->id;
            $cpte->total=$request->importe;
            $cpte->observaciones=$request->motivo;
            $cpte->created_us = auth()->id();
            $cpte->updated_us = auth()->id();
            $cpte->save();

            $cptemp = new ComprobanteMp();
            $cptemp->id_cpte = $cpte->id;
            $cptemp->id_mediopago = 1;
            $cptemp->created_us = auth()->id();
            $cptemp->updated_us = auth()->id();
            $cptemp->importe = $cpte->total;
            $cptemp->save();
        DB::commit();

        $cpte = Comprobante::find($cpte->id);
        if ($cpte->id_tipocpte==15){
            $cpte['tipocpte']='INGRESO DE EFECTIVO';
        }
        if ($cpte->id_tipocpte==11){
            $cpte['tipocpte']='RETIRO DE EFECTIVO';
            $cpte->total=$cpte->total*-1;
        }
        $cpte['titular']=Titular::find(1)->descripcion;
        $cpte['usuario']=User::find($cpte->created_us)->name;
        $cpte['creado']=$cpte->created_at->format('d/m/Y  H:i:s');
        $cpte['mediopago']='EFECTIVO';
        $cpte['msg']=$cpte['tipocpte'].': '.$cpte->id.' Creado Exitosamente!';
        return $cpte;
    }

    public function sabana(){
        return view('caja.sabana',[
            'titulo' => 'Listado Sabana',
            'itemnav' => 'sabana'
        ]);
    }

    public function getsabana(Request $request)
    {
        $items = DB::table('comprobantes as c')
            ->Join('comprobantes_mp as cmp', 'cmp.id_cpte', '=', 'c.id')
            ->Join('sys_medios_pago as mp', 'mp.id', '=', 'cmp.id_mediopago')
            ->Join('sys_tipocomprobantes as tc', 'tc.id', '=', 'c.id_tipocpte')
            ->leftJoin('titulares as t', 't.id', '=', 'c.id_titular')
            ->leftJoin('users as us', 'us.id', '=', 'c.created_us')
            ->whereNull('c.deleted_at')
            ->whereRaw('c.id_tipocpte in (5,6,7,8,9,10,11,15)')
            ->whereRaw('date(c.created_at) BETWEEN "'.$request->desde.'" AND "'.$request->hasta.'" ')
            ->select('c.id','c.id_caja','c.numero','tc.descripcion as tipocpte','c.observaciones','c.id_titular',DB::raw('FORMAT(cmp.importe,2, "es_AR") as total'),'mp.descripcion as mediopago', DB::raw('date_format(c.created_at, "%d/%m/%Y %H:%i:%s") as creado'),'t.descripcion as titular', 'us.name as usuario', DB::raw('date_format(c.updated_at, "%d/%m/%Y %H:%i:%s") as  ultmodif'))
            ->get();
        return $items;
    }
}
