<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\NotificacionEstado;
use DB;

class NotificacionesController extends Controller
{
    public function index()
    {
        $items = DB::select('SELECT ne.id, n.icon, n.descripcion, n.motivo, e.descripcion as estado,
                                    date_format(n.created_at, "%d/%m/%Y %H:%i:%s") as creado
                                  FROM express.notificaciones as n
                                  join notificaciones_estados as ne on ne.id_notificacion=n.id
                                  join sys_estados as e on e.id=ne.id_estado
                                 where ne.id_user='.Auth::user()->id);

        return view('notificaciones.index',[
            'titulo' => 'Notificaciones',
            'urlkey' => 'notificaciones',
            'items' => $items,
            'itemnav' => '',
            'icon' => 'fa-info-circle'
        ]);
    }

    public function setleidos()
    {
        foreach(DB::table('notificaciones_estados')
                     ->where('id_estado','=',14)
                     ->where('id_user','=',Auth::user()->id)
                     ->get() as $item){
            $notiEstado = NotificacionEstado::find($item->id);
            $notiEstado->id_estado = 15;
            $notiEstado->save();
        }
        return;
    }

    public function marcar($id)
    {
        $notiEstado = NotificacionEstado::find($id);
        $notiEstado->id_estado = 15;
        $notiEstado->save();
        return redirect('/notificaciones');
    }

    public function desmarcar($id)
    {
        $notiEstado = NotificacionEstado::find($id);
        $notiEstado->id_estado = 14;
        $notiEstado->save();
        return redirect('/notificaciones');
    }
}
