<?php

namespace App\Http\Controllers;

use App\Models\Subrubro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;

class SubrubrosController extends Controller
{
    public function index()
    {
        $items = DB::select('select sr.*,r.descripcion as rubro
                                from conf_subrubros sr
                                left join conf_rubros r on sr.id_rubro=r.id');

        return view('master.subrubros.index',[
            'titulo' => 'Subrubros',
            'urlkey' => 'subrubros',
            'items' => $items,
            'itemnav' => 'subr'
        ]);
    }

    public function create()
    {
        return view('master.subrubros.form',[
            'titulo' => 'Subrubros',
            'tituloform' => 'Nuevo Subrubro',
            'urlkey' => 'subrubros',
            'edit'=> 1,
            'itemnav' => 'subr',
            'accion' => 'Crear'
        ]);
    }

    public function store(Request $request)
    {
//        dd($request->request);
        $this->validate($request, [
            'descripcion' => 'required'
        ]);

        if (empty($request->id)){
            $item = new Subrubro();
            $mensaje = 'Creado Exitosamente!';
        }else{
            $item = Subrubro::find($request->id);
            $mensaje = 'Editado Exitosamente!';
        }

        $item->descripcion =  strtoupper($request->descripcion);
        $item->id_rubro =  $request->id_rubro;
        $item->activo = (!empty($request->activo)) ? 1 : 0;
        $item->save();
        return redirect('/subrubros')->with('status', $mensaje);
    }

    public function show($id)
    {
        $item = Subrubro::find($id);
        return view('master.subrubros.form',[
            'titulo' => 'Subrubros',
            'tituloform' => 'Subrubro',
            'urlkey' => 'subrubros',
            'edit'=> 0,
            'itemnav' => 'subr',
            'item' => $item,
            'accion' => 'Visualizar'
        ]);
    }

    public function edit($id)
    {
        $item = Subrubro::find($id);
        return view('master.subrubros.form',[
            'titulo' => 'Subrubros',
            'tituloform' => 'Subrubro',
            'urlkey' => 'subrubros',
            'edit'=> 1,
            'itemnav' => 'subr',
            'item' => $item,
            'accion' => 'Editar'
        ]);
    }

    public function delete($id)
    {
        $item = Subrubro::find($id);
        $item->delete();
        $data['msg']="Subrubro Eliminado!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $item = Subrubro::find($item);
            $item->delete();
        }
        return;
    }

    public function import()
    {
        return view('master.import', ['titulo' => 'Subrubros','urlkey' => 'subrubros','itemnav' => 'subrubros']);
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get();
            if(!empty($data) && $data->count()){
//                dd($data);
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $items[] = ['descripcion' => strtoupper($value->descripcion), 'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_subrubros')->insert($items);
                    return redirect('/subrubros')->with('status', 'Subrubros Importados Correctamente!');
                }else{
                    return redirect('/subrubros')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comuníquese con el equipo de Soporte a Usuarios.');
                }
            }
        }

    }

    public function vincular(Request $request)
    {
        $idrubro = $request->idrubro;
        $items=$request->ids;
        foreach($items as $item){
            $subrubro = Subrubro::find($item);
            $subrubro->id_rubro = $idrubro;
            $subrubro->save();
        }
        $subrubros = DB::select('select sr.id,sr.descripcion,
                                        case when sr.activo=1 then "SI" else "NO"
                                        end as activo,
                                        r.descripcion as rubro
                                from conf_subrubros sr
                                left join conf_rubros r on sr.id_rubro=r.id');
        return $subrubros;
    }
}
