<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Marca;
use DB;
use Illuminate\Http\Request;
use Excel;

class MarcasController extends Controller
{
    //
    public function index()
    {
        $items = Marca::all();
        $instock = DB::select('SELECT 	ma.descripcion as label,
                                        sum(cantidad) as value
                                FROM inv_stock s
                                join items p on p.id=s.id_item
                                left join conf_marcas ma on ma.id=p.id_marca
                                where id_sector is not null
                                and p.id_tipoitem=5
                                group by 1
                                order by 2 desc
                                limit 10');
        $instockarray = [];
        foreach( $instock as $item){
            $item->value = (int) $item->value;
            array_push($instockarray,(collect($item)->toArray()));
        }

        return view('master.index', ['titulo' => 'Marcas',
                                     'urlkey' => 'marcas',
                                     'items' => $items,
                                     'instock' => $instockarray,
                                     'itemnav' => 'marcas']);
    }

    public function store(Request $request)
    {
//        dd($request->request);
        $this->validate($request, [
            'descripcion' => 'required|max:255',
        ]);
        $marca = $request->all();
        $marca['descripcion'] = strtoupper($marca['descripcion']);

        if (isset($marca['activo'])){
            $marca['activo']=1;
        }else{
            $marca['activo']=0;
        }

        $data=Marca::create($marca);
        if ($marca['activo']==1){
            $data['activo']='SI';
        }else{
            $data['activo']='NO';
        }
        $data['msg']="Marca Creada Exitosamente!";
        return $data;
    }

    public function edit($id)
    {
        $item = Marca::find($id);
        return view('master.edit', ['titulo' => 'Marcas','urlkey' => 'marcas' ,'item' => $item]);

    }

    public function show($id)
    {
        $item = Marca::find($id);
        return view('master.show', ['titulo' => 'Marcas','urlkey' => 'marcas' ,'item' => $item]);
    }


    public function update($id,Request $request)
    {
        $marca = Marca::find($id);
        $marca->descripcion = strtoupper($request->descripcion);

        if (isset($request->activo)){
            $marca->activo=1;
        }else{
            $marca->activo=0;
        }

        $marca->save();
        $data['msg']="Marca Modificada.";
        return $data;

    }

    public function delete($id)
    {
        $marca = Marca::find($id);
        $marca->delete();
        $data['msg']="Marca Eliminada!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $marca = Marca::find($item);
            $marca->delete();
        }
        return;
    }

    public function import()
    {
        return view('master.import', ['titulo' => 'Marcas','urlkey' => 'marcas', 'itemnav' => 'marcas']);
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $items[] = ['descripcion' => $value->descripcion, 'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_marcas')->insert($items);
                    return redirect('/marcas')->with('status', 'Marcas Importados Correctamente!');
                }else{
                    return redirect('/marcas')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }
    }
}
