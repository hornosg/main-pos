<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Item;
use App\Models\Marca;
use App\Models\Rubro;
use App\Models\UnidadMedida;
use App\Models\Material;
use App\Models\Linea;
use App\Models\CrossSell;
use App\Models\UpSell;
use DB;
use Illuminate\Http\Request;
use Excel;

class BienesController extends Controller
{
    //
    public function index()
    {
        $bienes = DB::select('select p.*,ma.descripcion as marca
                                    from items p
                                    left join conf_marcas ma on ma.id=p.id_marca
                                    where id_tipoitem=7');
        return view('items.bienes.index', ['titulo' => 'Catalogo de Bienes','urlkey' => 'bienes','items' => $bienes]);
    }

    public function create()
    {
        $marcas = Marca::all();
        return view('items.bienes.create', [ 'titulo' => 'Catalogo de Bienes',
                                                'urlkey' => 'bienes',
                                                'item' => 'Producto',
                                                'marcas' => $marcas]
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255'
        ]);

        //dd($request->request);
        $producto = new Item;
        $producto->descripcion = strtoupper($request->descripcion);
        $producto->id_tipoitem = $request->id_tipoitem;
        $producto->ean = $request->ean;
        $producto->id_unidadventa = $request->id_unidadventa;
        $producto->contNeto = $request->contNeto;
        $producto->id_unidadcontneto = $request->id_unidadcontneto;
        $producto->peso = $request->peso;
        $producto->largo = $request->largo;
        $producto->ancho = $request->ancho;
        $producto->alto = $request->alto;
        $producto->id_marca = $request->id_marca;
        $producto->id_rubro = $request->id_rubro;
        $producto->id_linea = $request->id_linea;
        $producto->id_material = $request->id_material;
        //$producto->esCombo = $request->esCombo;
        $producto->puntoRepocicion = $request->puntoRepocicion;
        $producto->stockMinimo = $request->stockMinimo;
        $producto->precioLista = $request->precioLista;
        $producto->bonifProv = $request->bonifProv;
        $producto->precioCosto = $request->precioCosto;
        $producto->ganancia = $request->ganancia;
        $producto->precio = $request->precio;
        $producto->especificaciones = $request->especificaciones;
        $producto->observaciones = $request->observaciones;

        if (isset($request->activo)){
            $producto->activo=1;
        }else{
            $producto->activo=0;
        }

        $producto->save();

        if ($request->action=='save'){
            return redirect('/bienes')->with('status', 'Item Creado Exitosamente!');
        }else{
            return redirect('/bienes/create')->with('status', 'Item Creado Exitosamente!');
        }
    }

    public function edit($id)
    {
        $producto = Item::find($id);
        $marcas = Marca::all();

        return view('items.bienes.show-edit',
                    ['titulo' => 'Catalogo de Bienes',
                    'urlkey' => 'bienes',
                    'producto' => $producto,
                    'marcas' => $marcas,
                    'edit'=> 1 ]
        );

    }

    public function show($id)
    {
        $producto = Item::find($id);

        $marcas = Marca::all();
        return view('items.bienes.show-edit',
            ['titulo' => 'Catalogo de Bienes',
                'urlkey' => 'bienes',
                'item' => 'Producto',
                'producto' => $producto,
                'marcas' => $marcas,
                'edit'=> 0 ]
        );
    }


    public function update($id,Request $request)
    {
        //dd($request->request);
        $producto = Item::find($id);
        $producto->descripcion = strtoupper($request->descripcion);
        $producto->id_tipoitem = $request->id_tipoitem;
        $producto->ean = $request->ean;
        $producto->id_unidadventa = $request->id_unidadventa;
        $producto->contNeto = $request->contNeto;
        $producto->id_unidadcontneto = $request->id_unidadcontneto;
        $producto->peso = $request->peso;
        $producto->largo = $request->largo;
        $producto->ancho = $request->ancho;
        $producto->alto = $request->alto;
        $producto->id_marca = $request->id_marca;
        $producto->id_rubro = $request->id_rubro;
        $producto->id_linea = $request->id_linea;
        $producto->id_material = $request->id_material;
        $producto->puntoRepocicion = $request->puntoRepocicion;
        $producto->stockMinimo = $request->stockMinimo;
        $producto->precioLista = $request->precioLista;
        $producto->bonifProv = $request->bonifProv;
        $producto->precioCosto = $request->precioCosto;
        $producto->ganancia = $request->ganancia;
        $producto->precio = $request->precio;
        $producto->especificaciones = $request->especificaciones;
        $producto->observaciones = $request->observaciones;

        if (isset($request->activo)){
            $producto->activo=1;
        }else{
            $producto->activo=0;
        }

        $producto->save();

        return redirect('/bienes')->with('status', 'Item Actualizado!');

    }

    public function delete($id)
    {
        $producto = Item::find($id);
        $producto->delete();
        return redirect('/bienes')->with('status', 'Item Eliminado!');
    }

    public function removeselected($idcodprod)
    {
        $delete=DB::table('items_proveedores')->where('codproducto', $idcodprod)->delete();
        return $delete;
    }
}
