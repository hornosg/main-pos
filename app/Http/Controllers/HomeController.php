<?php

namespace App\Http\Controllers;

use App\Models\Pais;
use App\Models\Localidad;
use App\Models\Provincia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $avgcptes = DB::select('select round(avg(cant),0) as promedio from ( select count(*) as cant, date(c.created_at) as dia
                                 from comprobantes c
                                where c.created_at BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()),INTERVAL 1 DAY),INTERVAL - 1 MONTH) AND LAST_DAY(NOW())
                                  and c.deleted_at is null
                                  and c.created_us = ?
                                  and c.id_tipocpte not in (8,9,10,11,12,14,15)
                             group by 2) as cantidadxdia',[Auth::user()->id]);

        $masde3xcpte = DB::select('select round(avg(cant),0) as cantidad from (select count(cpte) as cant, dia from (select count(*) as cant, c.id as cpte, date(c.created_at) as dia
                                     from comprobantes_items ci
                                     join comprobantes c on c.id=ci.id_cpte
                                    where c.created_us = ?
                                      and c.created_at BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()),INTERVAL 1 DAY),INTERVAL - 1 MONTH) AND LAST_DAY(NOW())
                                    group by 2,3
                                    having count(*) >= 3) masd3xcpte
                                    group by 2) masd3_promxdia',[Auth::user()->id]);

        DB::select('SET lc_time_names = \'es_ES\'');
//SELECT hour(c.created_at) as HORA,
//count(IF(DAYNAME( c.created_at )='domingo',	c.id,	null)) as DOMINGO,
//count(IF(DAYNAME( c.created_at )='lunes',	c.id,	null)) as LUNES,
//count(IF(DAYNAME( c.created_at )='martes',	c.id,	null)) as MARTES,
//count(IF(DAYNAME( c.created_at )='miercoles',	c.id,	null)) as MIERCOLES,
//count(IF(DAYNAME( c.created_at )='jueves',	c.id,	null)) as JUEVES,
//count(IF(DAYNAME( c.created_at )='viernes',	c.id,	null)) as VIERNES,
//count(IF(DAYNAME( c.created_at )='sabado',	c.id,	null)) as SABADO
//FROM comprobantes c
//join cajas ca on ca.id = c.id_caja and ca.id_sucursal = 1
//where date(c.created_at) >= DATE(NOW()) - INTERVAL 7 DAY
//GROUP BY hour(created_at);
        $cptesxhora = DB::select('select hora, ROUND(avg(cant),0) as promedio
                                    from (
                                            SELECT hour(c.created_at) as hora,
                                            DAYNAME( c.created_at ) as dia,
                                            count(*) as cant
                                            FROM comprobantes c
                                            join cajas ca on ca.id = c.id_caja and ca.id_sucursal = ?
                                            where date(c.created_at) >= DATE(NOW()) - INTERVAL 7 DAY
                                            GROUP BY 1,2 
                                        ) as cantxdiahora
                                    GROUP BY 1',[Auth::user()->id_sucursal]);

        $cptesxhoraarray = [];
        array_push($cptesxhoraarray,['Hs','Promedio']);
        foreach($cptesxhora as $item){
            array_push($cptesxhoraarray,[$item->hora,
                                                (int) $item->promedio]);
        }

        return view('home',[
            'avgcptes'=>$avgcptes[0]->promedio,
            'masde3xcpte'=>$masde3xcpte[0]->cantidad,
            'cptesxhora'=> json_encode($cptesxhoraarray, JSON_UNESCAPED_SLASHES ),
            'top'=>5

        ]);
    }

    public function confgeo()
    {
        $paises = Pais::all();
        $provincias = DB::select('select p.*,pa.descripcion as pais
                                    from conf_provincias p
                                    left join conf_paises pa on pa.id=p.id_pais');
        $localidades = DB::select('select l.*,
                                          CONCAT (pr.descripcion, \' (\',pa.descripcion,\')\') as provincia
                                    from conf_localidades l
                                    left join conf_provincias pr on pr.id=l.id_provincia
                                    left join conf_paises pa on pa.id=pr.id_pais');
        return view('configuracion/geografica',[
            'paises' => $paises,
            'provincias' => $provincias,
            'localidades' => $localidades
        ]);
    }

    public function masvendidos()
    {
        return view('items.productos.masvendidos',[
            'itemnav'=>'list',
            'titulo'=>'Mas Vendidos',
            'top'=>100
        ]);
    }
}
