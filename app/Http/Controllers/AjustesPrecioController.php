<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AjustePrecio;
use DB;

class AjustesPrecioController extends Controller
{
    public function index()
    {
        $items = DB::table('conf_ajustes_precio as ap')
            ->select('ap.*', DB::raw('date_format(ap.created_at, "%d/%m/%Y %H:%i:%s") as creado'))
            ->get();

        return view('precios.ajustes.index',[
            'titulo' => 'Ajustes de Precio',
            'urlkey' => 'precios/ajustes',
            'items' => $items,
            'itemnav' => 'aj'
        ]);
    }

    public function create()
    {
        return view('precios.ajustes.form',[
            'titulo' => 'Ajustes de Precio',
            'tituloform' => 'Nuevo',
            'urlkey' => 'precios/ajustes',
            'edit'=> 1,
            'itemnav' => 'aj',
            'accion' => 'Crear'
        ]);
    }

    public function store(Request $request)
    {
//        dd($request->request);
        $this->validate($request, [
            'porcentaje' => 'required'
        ]);

        if (empty($request->id)){
            $item = new AjustePrecio();
            $mensaje = 'Creado Exitosamente!';
        }else{
            $item = AjustePrecio::find($request->id);
            $mensaje = 'Editado Exitosamente!';
        }

        $item->descripcion =  strtoupper($request->descripcion);
        $item->nota =  $request->nota;
        $item->porcentaje = $request->porcentaje;
        $item->signo = $request->signo;
        $item->activo = (!empty($request->activo)) ? 1 : 0;
        $item->save();
        return redirect('/precios/ajustes')->with('status', $mensaje);
    }

    public function show($id)
    {
        $item = AjustePrecio::find($id);
        return view('precios.ajustes.form',[
            'titulo' => 'Ajustes de Precio',
            'urlkey' => 'precios/ajustes',
            'edit'=> 0,
            'itemnav' => 'aj',
            'item' => $item,
            'accion' => 'Visualizar'
        ]);
    }

    public function edit($id)
    {
        $item = AjustePrecio::find($id);
        return view('precios.ajustes.form',[
            'titulo' => 'Ajustes de Precio',
            'urlkey' => 'precios/ajustes',
            'edit'=> 1,
            'itemnav' => 'aj',
            'item' => $item,
            'accion' => 'Editar'
        ]);
    }
}
