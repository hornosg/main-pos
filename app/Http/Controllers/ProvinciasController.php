<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Provincia;
use App\Models\Pais;
use DB;
use Illuminate\Http\Request;
use Excel;

class ProvinciasController extends Controller
{
    //
    public function index()
    {
//        $provincias = Provincia::all();
        $provincias = DB::select('select p.*,pa.descripcion as pais
                                    from conf_provincias p
                                    left join conf_paises pa on pa.id=p.id_pais');
        return view('master.provincias.index', ['titulo' => 'Provincias','urlkey' => 'provincias','provincias' => $provincias]);
    }

    public function create()
    {
        $paises = Pais::all();
        return view('master.provincias.create', ['titulo' => 'Provincias','urlkey' => 'provincias','paises' => $paises]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255','id_pais'=> 'required'
        ]);


        $provincia = $request->all();
        $provincia['descripcion'] = strtoupper($provincia['descripcion']);

        if (isset($provincia['activo'])){
            $provincia['activo']=1;
        }else{
            $provincia['activo']=0;
        }
        $data=Provincia::create($provincia);
        $data['pais']=Pais::find($data['id_pais'])->descripcion;

        if ($provincia['activo']==1){
            $data['activo']='SI';
        }else{
            $data['activo']='NO';
        }
        $data['msg']="Provincia Creada Exitosamente!";
        return $data;
    }

    public function edit($id)
    {
        $provincia = Provincia::find($id);

        $provincia->descripcion_pais=Pais::find($provincia->id_pais)->descripcion;
        $paises = Pais::all();
        return view('master.provincias.edit', ['titulo' => 'Provincias','urlkey' => 'provincias','provincia' => $provincia,'paises' => $paises]);

    }

    public function show($id)
    {
        $provincia = Provincia::find($id);
        $provincia->descripcion_pais=Pais::find($provincia->id_pais)->descripcion;
        return view('master.provincias.show', ['titulo' => 'Provincias','urlkey' => 'provincias','provincia' => $provincia]);
    }


    public function update($id,Request $request)
    {
        $provincia = Provincia::find($id);

        $provincia->descripcion = strtoupper($request->descripcion);
        $provincia->id_pais = $request->id_pais;

        if (isset($request->activo)){
            $provincia->activo=1;
        }else{
            $provincia->activo=0;
        }

        $provincia->save();
        $data=$provincia;
        $data['pais']=Pais::find($data['id_pais'])->descripcion;
        $data['msg']="Provincia Modificada.";
        return $data;

    }

    public function delete($id)
    {
        $provincia = Provincia::find($id);
        $provincia->delete();
        $data['msg']="Provincia Eliminada!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $provincia = Provincia::find($item);
            $provincia->delete();
        }
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $idPais = Pais::where('descripcion', $value->pais)->select('id')->first();
                    $items[] = ['descripcion' => $value->descripcion,
                                'id_pais' => $idPais['id'],
                                'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_provincias')->insert($items);
                    return redirect('/provincias')->with('status', 'Provincias Importados Correctamente!');
                }else{
                    return redirect('/provincias')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }

    }
}
