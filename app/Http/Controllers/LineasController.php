<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Linea;
use DB;
use Illuminate\Http\Request;
use Excel;

class LineasController extends Controller
{
    //
    public function index()
    {
        $items = Linea::all();
        return view('master.index', ['titulo' => 'Lineas de Productos',
                                    'urlkey' => 'lineas',
                                    'items' => $items,
                                    'itemnav' => 'otro']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
        ]);


        $linea = $request->all();
        $linea['descripcion'] = strtoupper($linea['descripcion']);

        if (isset($linea['activo'])){
            $linea['activo']=1;
        }else{
            $linea['activo']=0;
        }
        $data=Linea::create($linea);
        if ($linea['activo']==1){
            $data['activo']='SI';
        }else{
            $data['activo']='NO';
        }
        $data['msg']="Linea Creada Exitosamente!";
        return $data;
    }

    public function edit($id)
    {
        $item = Linea::find($id);
        return view('master.edit', ['titulo' => 'Lineas','urlkey' => 'lineas' ,'item' => $item]);

    }

    public function show($id)
    {
        $item = Linea::find($id);
        return view('master.show', ['titulo' => 'Lineas','urlkey' => 'lineas' ,'item' => $item]);
    }


    public function update($id,Request $request)
    {
        $linea = Linea::find($id);
        $linea->descripcion = strtoupper($request->descripcion);

        if (isset($request->activo)){
            $linea->activo=1;
        }else{
            $linea->activo=0;
        }

        $linea->save();
        $data['msg']="Marca Modificada.";
        return $data;

    }

    public function delete($id)
    {
        $linea = Linea::find($id);
        $linea->delete();
        $data['msg']="Linea de Producto Eliminada!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $linea = Linea::find($item);
            $linea->delete();
        }
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $items[] = ['descripcion' => $value->descripcion, 'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_lineas')->insert($items);
                    return redirect('/lineas')->with('status', 'Lineas Importados Correctamente!');
                }else{
                    return redirect('/lineas')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }
    }
}
