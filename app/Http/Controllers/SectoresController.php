<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Sector;
use App\Models\Sucursal;
use DB;
use Illuminate\Http\Request;


class SectoresController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
            'id_sucursal' => 'required'
        ]);

        $sector = $request->all();
        $sector['descripcion'] = strtoupper($sector['descripcion']);

        if (isset($sector['activo'])){
            $sector['activo']=1;
        }else{
            $sector['activo']=0;
        }
        $data=Sector::create($sector);
        $data['sucursal']=Sucursal::find($data['id_sucursal'])->descripcion;

        if ($sector['activo']==1){
            $data['activo']='si';
        }else{
            $data['activo']='no';
        }
        $data['msg']="Sector Creado Exitosamente!";
        return $data;
    }

    public function update($id,Request $request)
    {
        if (($id==1) or ($id==2)){
            $data['msg']="No es Posible modificar Sector Recepcion";
            return $data;
        }

        $this->validate($request, [
            'descripcion' => 'required|max:255',
            'id_sucursal' => 'required'
        ]);

        $sector = Sector::find($id);

        $sector->descripcion = strtoupper($request->descripcion);
        $sector->id_sucursal = $request->id_sucursal;
        $sector->alias = $request->alias;

        if (isset($request->activo)){
            $sector->activo=1;
        }else{
            $sector->activo=0;
        }

        $sector->save();
        $data=$sector;
        $data['sucursal']=Sucursal::find($data['id_sucursal'])->descripcion;
        $data['msg']="Sector Modificado.";
        return $data;
    }

    public function delete($id)
    {
        if (($id==1) or ($id==2)){
            $data['msg']="No es Posible borrar el Sector Recepcion";
            return $data;
        }
        $sector = Sector::find($id);
        $sector->delete();
        $data['msg']="Sector Eliminado!";
        return $data;
    }
}
