<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\PasswordReset;
use App\User;
use DB;

class UsuariosController extends Controller
{
    public function index()
    {
        $items = DB::table('users as u')
            ->leftJoin('sys_roles as r', 'r.id', '=', 'u.id_rol')
            ->leftJoin('sucursales as s', 's.id', '=', 'u.id_sucursal')
            ->whereNull('u.deleted_at')
            ->select('u.*', DB::raw('date_format(u.created_at, "%d/%m/%Y %H:%i:%s") as creado'),
                DB::raw('date_format(u.updated_at, "%d/%m/%Y %H:%i:%s") as modificado'),
                'r.descripcion as rol','s.descripcion as sucursal')
            ->get();

        return view('usuarios.index',[
            'titulo' => 'Usuarios',
            'urlkey' => 'usuarios',
            'items' => $items,
            'itemnav' => 'users',
            'icon' => 'fa-user'
        ]);
    }

    public function create()
    {
        return view('usuarios.form',[
            'titulo' => 'Usuarios',
            'tituloform' => 'Nuevo',
            'urlkey' => 'usuarios',
            'edit'=> 1,
            'itemnav' => 'users',
            'accion' => 'Crear',
            'icon' => 'fa-user'
        ]);
    }

    public function store(Request $request)
    {
//        dd($request->request);
        if (empty($request->id)){
            $this->validate($request, [
                'name' => 'required|unique:users',
                'full_name' => 'required',
                'email' => 'required|unique:users',
                'id_rol' => 'required',
                'id_sucursal' => 'required'
            ]);

            $item = ['name'=>$request->name,
                     'full_name'=>strtoupper($request->full_name),
                     'email'=>$request->email,
                     'password' => Hash::make('mainpos'),
                     'id_sucursal'=>$request->id_sucursal,
                     'id_rol' =>$request->id_rol
                    ];
            User::create($item);
            $mensaje = 'Creado Exitosamente!';
        }else{
            $this->validate($request, [
                'name' => 'required',
                'full_name' => 'required',
                'email' => 'required',
                'id_rol' => 'required',
                'id_sucursal' => 'required'
            ]);

            $item = User::find($request->id);
            $item->name = $request->name;
            $item->full_name =  strtoupper($request->full_name);
            $item->email =  $request->email;
            $item->id_rol = $request->id_rol;
            $item->id_sucursal = $request->id_sucursal;
            $item->save();
            $mensaje = 'Editado Exitosamente!';
        }
        return redirect('/usuarios')->with('status', $mensaje);
    }

    public function show($id)
    {
        $item = User::find($id);
        return view('usuarios.form',[
            'titulo' => 'Usuarios',
            'urlkey' => 'usuarios',
            'edit'=> 0,
            'itemnav' => 'users',
            'item' => $item,
            'icon' => 'fa-user',
            'accion' => 'Visualizar'
        ]);
    }

    public function edit($id)
    {
        $item = User::find($id);
        return view('usuarios.form',[
            'titulo' => 'Usuarios',
            'urlkey' => 'usuarios',
            'edit'=> 1,
            'itemnav' => 'users',
            'item' => $item,
            'icon' => 'fa-user',
            'accion' => 'Editar'
        ]);
    }

    public function changePwd(Request $request)
    {
        //dd($request->request);
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);
        $user = Auth::user();
        $user->password = Hash::make($request->password);
        $user->setRememberToken(Str::random(60));
        $user->save();
        $data['msg']="Clave Modificada!";
        return $data;
    }

    public function resetPwd($id)
    {
        $user = User::find($id);
        $user->password = Hash::make('mainpos');
        $user->setRememberToken(Str::random(60));
        $user->save();
        $data['msg']="Clave Reseteada!";
        return $data;
    }

    public function performance()
    {
        $cptesxus = DB::select('select round(avg(cant),0) as promedio, created_us, u.name as usuario
                                  from ( select count(*) as cant, date(c.created_at) as dia, c.created_us
                                           from comprobantes c
                                          where c.created_at BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()),INTERVAL 1 DAY),INTERVAL - 1 MONTH) AND LAST_DAY(NOW())
                                            and c.deleted_at is null
                                         group by 2,3) as cantidadxdia
                                  join users as u on u.id = created_us and u.id !=1
                                 group by 2,3 order by 1 desc');

        return view('usuarios.performance',[
            'icon' => 'Resumen Mensual',
            'titulo' => 'Resumen Mensual',
            'urlkey' => 'usuarios',
            'itemnav' => 'list',
            'icon' => 'fa-user',
            'cptesxus'=>$cptesxus
        ]);
    }
}