<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\MedioPago;
use App\Models\Item;
use App\Models\ControlStock;
use App\Models\ControlStockConteo;
use DB;

class GetdataController extends Controller
{
    public function getmp($id){
        $item = MedioPago::find($id);
        return json_encode($item);
    }


    public function getmarcas($iditem=null){
        if (!empty($iditem)){
            $item = Item::find($iditem);
            $datos = DB::table('conf_marcas')->where('id',$item->id_marca)->select('id', 'descripcion')->get();
        }else{
            $datos = DB::select('select m.id, m.descripcion, sum(cantidad)
                                   from inv_stock s
                                   join items i on i.id = s.id_item
                                   join conf_marcas m on m.id = i.id_marca
                                  where id_sector is not null
                                   GROUP BY 1,2
                                   HAVING sum(cantidad) > 0');
        }
        return $datos;
    }

    public function getrubros($iditem=null){
        if (!empty($iditem)){
            $item = Item::find($iditem);
            $datos = DB::table('conf_rubros')->where('id',$item->id_rubro)->select('id', 'descripcion')->get();
        }else{
            $datos = DB::select('select r.id, r.descripcion, sum(cantidad)
                                   from inv_stock s
                                   join items i on i.id = s.id_item
                                   join conf_rubros r on r.id = i.id_rubro
                                  where id_sector is not null
                                   GROUP BY 1,2
                                   HAVING sum(cantidad) > 0');
        }
        return $datos;
    }

    public function getsubrubros($iditem=null){
        if (!empty($iditem)){
            $item = Item::find($iditem);
            $datos = DB::table('conf_subrubros')->where('id',$item->id_subrubro)->select('id', 'descripcion')->get();
        }else{
            $datos = DB::select('select sr.id, sr.descripcion, sum(cantidad)
                                   from inv_stock s
                                   join items i on i.id = s.id_item
                                   join conf_subrubros sr on sr.id = i.id_rubro
                                  where id_sector is not null
                                   GROUP BY 1,2
                                   HAVING sum(cantidad) > 0');
        }
        return $datos;
    }

    public function getorigen($iditem){
        $sucursal = Auth::user()->id_sucursal;
        $items = DB::select('SELECT (p.id + 1000000) as id, p.descripcion, sum(cantidad) as stock
                               FROM inv_stock s
                               JOIN inv_posiciones p on p.id = s.id_posicion
                               JOIN inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                              WHERE id_item = ?
                              GROUP BY 1,2
                             HAVING sum(cantidad) > 0',[$sucursal,$iditem]);
        return $items;
    }

    public function getsectores($iditem=null){
        $sucursal = Auth::user()->id_sucursal;
//        dd($sucursal);
        if (!empty($iditem)){
            $items = DB::table('inv_sectores')
                ->where('id_sucursal',$sucursal)
                ->whereRaw('id in (SELECT distinct id_sector FROM inv_stock where id_sector is not null and id_item = ?)',[$iditem])
                ->select('id', 'alias', 'descripcion')
                ->get();
//            dd($items);
            return $items;
        }else{
            $items = DB::table('inv_sectores')
                ->where('id_sucursal',$sucursal)
                ->select('id', 'alias', 'descripcion')->get();
            return $items;
        }
    }

    public function getubicaciones($iditem=null){
        $sucursal = Auth::user()->id_sucursal;
        if (!empty($iditem)){
            $items = DB::select('select distinct u.id,u.alias,u.descripcion
                                    from inv_ubicaciones u
                                    join inv_sectores s on s.id=u.id_sector and s.id_sucursal = ?
                                    join inv_stock ss on ss.id_ubicacion=u.id and ss.id_item = ?',[$sucursal,$iditem]);
            return $items;
        }else{
            $items = DB::table('inv_ubicaciones')
                ->whereRaw('id_sector in (SELECT distinct id from inv_sectores where id_sucursal = ?)',[$sucursal])
                ->select('id', 'descripcion', 'alias')->get();
            return $items;
        }
    }

    public function getposiciones($iditem=null){
        $sucursal = Auth::user()->id_sucursal;
        if (!empty($iditem)){
            $items = DB::table('inv_posiciones')
                ->whereRaw('id in (SELECT distinct id_posicion
                                     FROM inv_stock
                                    WHERE id_posicion is not null
                                      AND id_sector in ( select distinct id from inv_sectores where id_sucursal = ?)
                                      AND id_item = ?)',[$sucursal,$iditem])
                ->select('id', 'descripcion')
                ->get();
//            dd($items);
            return $items;
        }else{
            $items = DB::table('inv_posiciones')
                ->whereRaw('id_ubicacion in (SELECT distinct id from inv_ubicaciones where id_sector in ( select distinct id from inv_sectores where id_sucursal = ?))',[$sucursal])
                ->select('id', 'descripcion')->get();
//            dd($items);
            return $items;
        }
    }
    
    
    
    public function getrubrosmarca($idmarca=null){
        if (!empty($idmarca) and $idmarca!="null"){
            $datos = DB::select('select r.id, r.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_marca=?
                               join conf_rubros r on r.id = i.id_rubro
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$idmarca]);
        }else{
            $datos = DB::select('select r.id, r.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item
                               join conf_rubros r on r.id = i.id_rubro
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0');
        }
        return $datos;
    }
    
    public function getsubrubrosmarca($idmarca=null){
        if (!empty($idmarca)){
            $datos = DB::select('select sr.id, sr.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_marca=?
                               join conf_subrubros sr on sr.id = i.id_subrubro
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$idmarca]);
        }else{
            $datos = DB::select('select sr.id, sr.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item
                               join conf_subrubros sr on sr.id = i.id_subrubro
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0');
        }
        return $datos;
    }

    public function getsectoresmarca($idmarca=null){
        $sucursal = Auth::user()->id_sucursal;
        if (!empty($idmarca)){
            $datos = DB::select('select se.id, se.descripcion, se.alias, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_marca=?
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                              where id_sector is not null
                               GROUP BY 1,2,3
                               HAVING sum(cantidad) > 0',[$idmarca,$sucursal]);
        }else{
            $datos = DB::select('select se.id, se.descripcion, se.alias, sum(cantidad)
                               from inv_stock s
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                              where id_sector is not null
                               GROUP BY 1,2,3
                               HAVING sum(cantidad) > 0',[$sucursal]);
        }
        return $datos;
    }

    public function getubicacionesmarca($idmarca=null){
        $sucursal = Auth::user()->id_sucursal;
        if (!empty($idmarca)){
            $datos = DB::select('select u.id, u.descripcion, u.alias, sum(cantidad)
                           from inv_stock s
                           join items i on i.id = s.id_item and i.id_marca=?
                           join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                           join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                          where s.id_sector is not null
                           GROUP BY 1,2,3
                           HAVING sum(cantidad) > 0',[$idmarca,$sucursal]);
        }else{
            $datos = DB::select('select u.id, u.descripcion, u.alias, sum(cantidad)
                           from inv_stock s
                           join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                           join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                          where s.id_sector is not null
                           GROUP BY 1,2,3
                           HAVING sum(cantidad) > 0',[$sucursal]);
        }
        return $datos;
    }

    public function getposicionesmarca($idmarca=null){
        $sucursal = Auth::user()->id_sucursal;
        if (!empty($idmarca)){
            $datos = DB::select('select p.id, p.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_marca=?
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                               join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                               join inv_posiciones p on p.id = s.id_posicion and p.id_ubicacion = u.id
                              where s.id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$idmarca,$sucursal]);
        }else{
            $datos = DB::select('select p.id, p.descripcion, sum(cantidad)
                               from inv_stock s
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                               join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                               join inv_posiciones p on p.id = s.id_posicion and p.id_ubicacion = u.id
                              where s.id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$sucursal]);
        }
        return $datos;
    }



    public function getmarcasrubro($idrubro=null){
        if (!empty($idrubro) and $idrubro!="null"){
            $datos = DB::select('select m.id, m.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_rubro=?
                               join conf_marcas m on m.id = i.id_marca
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$idrubro]);
        }else{
            $datos = DB::select('select m.id, m.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item
                               join conf_marcas m on m.id = i.id_marca
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0');
        }
        return $datos;
    }

    public function getsubrubrosrubro($idrubro=null){
        if (!empty($idrubro) and $idrubro!="null"){
            $datos = DB::select('select sr.id, sr.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_rubro=?
                               join conf_subrubros sr on sr.id = i.id_subrubro
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$idrubro]);
        }else{
            $datos = DB::select('select sr.id, sr.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item
                               join conf_subrubros sr on sr.id = i.id_subrubro
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0');
        }
        return $datos;
    }

    public function getconfsubrubrosrubro($idrubro=null){
        if (!empty($idrubro) and $idrubro!="null"){
            $datos = DB::select('select sr.id, sr.descripcion
                                   from conf_subrubros sr
                                  where id_rubro = ?',[$idrubro]);
        }else{
            $datos = DB::select('select sr.id, sr.descripcion
                                   from conf_subrubros sr');
        }
        return $datos;
    }

    public function getsectoresrubro($idrubro=null){
        $sucursal = Auth::user()->id_sucursal;
        if (!empty($idrubro) and $idrubro!="null"){
            $datos = DB::select('select se.id, se.descripcion, se.alias, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_rubro=?
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                              where id_sector is not null
                               GROUP BY 1,2,3
                               HAVING sum(cantidad) > 0',[$idrubro,$sucursal]);
        }else{
            $datos = DB::select('select se.id, se.descripcion, se.alias, sum(cantidad)
                               from inv_stock s
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                              where id_sector is not null
                               GROUP BY 1,2,3
                               HAVING sum(cantidad) > 0',[$sucursal]);
        }
        return $datos;
    }

    public function getubicacionesrubro($idrubro=null){
        $sucursal = Auth::user()->id_sucursal;
        if (!empty($idrubro) and $idrubro!="null"){
            $datos = DB::select('select u.id, u.descripcion, u.alias, sum(cantidad)
                           from inv_stock s
                           join items i on i.id = s.id_item and i.id_rubro=?
                           join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                           join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                          where s.id_sector is not null
                           GROUP BY 1,2,3
                           HAVING sum(cantidad) > 0',[$idrubro,$sucursal]);
        }else{
            $datos = DB::select('select u.id,  u.descripcion, u.alias, sum(cantidad)
                           from inv_stock s
                           join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                           join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                          where s.id_sector is not null
                           GROUP BY 1,2,3
                           HAVING sum(cantidad) > 0',[$sucursal]);
        }
        return $datos;
    }

    public function getposicionesrubro($idrubro=null){
        $sucursal = Auth::user()->id_sucursal;
        if (!empty($idrubro) and $idrubro!="null"){
            $datos = DB::select('select p.id, p.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_rubro=?
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                               join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                               join inv_posiciones p on p.id = s.id_posicion and p.id_ubicacion = u.id
                              where s.id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$idrubro,$sucursal]);
        }else{
            $datos = DB::select('select p.id, p.descripcion, sum(cantidad)
                               from inv_stock s
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                               join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                               join inv_posiciones p on p.id = s.id_posicion and p.id_ubicacion = u.id
                              where s.id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$sucursal]);
        }
        return $datos;
    }


    public function getmarcassubrubro($idsubrubro=null){
        if (!empty($idsubrubro) and $idsubrubro!="null"){
            $datos = DB::select('select m.id, m.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_subrubro=?
                               join conf_marcas m on m.id = i.id_marca
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$idsubrubro]);
        }else{
            $datos = DB::select('select m.id, m.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item
                               join conf_marcas m on m.id = i.id_marca
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0');
        }
        return $datos;
    }

    public function getrubrossubrubro($idsubrubro=null){
        if (!empty($idsubrubro) and $idsubrubro!="null"){
            $datos = DB::select('select sr.id, sr.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_subrubro=?
                               join conf_rubros sr on sr.id = i.id_rubro
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$idsubrubro]);
        }else{
            $datos = DB::select('select sr.id, sr.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item
                               join conf_rubros sr on sr.id = i.id_rubro
                              where id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0');
        }
        return $datos;
    }

    public function getsectoressubrubro($idsubrubro=null){
        $sucursal = Auth::user()->id_sucursal;
        if (!empty($idsubrubro) and $idsubrubro!="null"){
            $datos = DB::select('select se.id, se.descripcion, se.alias, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_subrubro=?
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                              where id_sector is not null
                               GROUP BY 1,2,3
                               HAVING sum(cantidad) > 0',[$idsubrubro,$sucursal]);
        }else{
            $datos = DB::select('select se.id, se.descripcion, se.alias, sum(cantidad)
                               from inv_stock s
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                              where id_sector is not null
                               GROUP BY 1,2,3
                               HAVING sum(cantidad) > 0',[$sucursal]);
        }
        return $datos;
    }

    public function getubicacionessubrubro($idsubrubro=null){
        $sucursal = Auth::user()->id_sucursal;
        if (!empty($idsubrubro) and $idsubrubro!="null"){
            $datos = DB::select('select u.id, u.descripcion, u.alias, sum(cantidad)
                           from inv_stock s
                           join items i on i.id = s.id_item and i.id_subrubro=?
                           join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                           join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                          where s.id_sector is not null
                           GROUP BY 1,2,3
                           HAVING sum(cantidad) > 0',[$idsubrubro,$sucursal]);
        }else{
            $datos = DB::select('select u.id, u.descripcion, u.alias, sum(cantidad)
                           from inv_stock s
                           join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                           join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                          where s.id_sector is not null
                           GROUP BY 1,2,3
                           HAVING sum(cantidad) > 0',[$sucursal]);
        }
        return $datos;
    }

    public function getposicionessubrubro($idsubrubro=null){
        $sucursal = Auth::user()->id_sucursal;
        if (!empty($idsubrubro) and $idsubrubro!="null"){
            $datos = DB::select('select p.id, p.descripcion, sum(cantidad)
                               from inv_stock s
                               join items i on i.id = s.id_item and i.id_subrubro=?
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                               join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                               join inv_posiciones p on p.id = s.id_posicion and p.id_ubicacion = u.id
                              where s.id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$idsubrubro,$sucursal]);
        }else{
            $datos = DB::select('select p.id, p.descripcion, sum(cantidad)
                               from inv_stock s
                               join inv_sectores se on se.id = s.id_sector and se.id_sucursal=?
                               join inv_ubicaciones u on u.id = s.id_ubicacion and se.id = u.id_sector
                               join inv_posiciones p on p.id = s.id_posicion and p.id_ubicacion = u.id
                              where s.id_sector is not null
                               GROUP BY 1,2
                               HAVING sum(cantidad) > 0',[$sucursal]);
        }
        return $datos;
    }


    public function getubicacionessector($idsector=null){
        if (!empty($idsector) and $idsector!="null"){
            $datos = DB::select('select u.*
                           from inv_ubicaciones u
                           join inv_sectores se on se.id = u.id_sector and se.id=?',[$idsector]);
        }else{
            $sucursal = Auth::user()->id_sucursal;
            $datos = DB::select('select u.*
                           from inv_ubicaciones u
                           join inv_sectores se on se.id = u.id_sector and se.id_sucursal=?',[$sucursal]);
        }
        return $datos;
    }

    public function getposicionessector($idsector=null){
        if (!empty($idsector) and $idsector!="null"){
            $datos = DB::select('select p.id, p.descripcion
                               from inv_posiciones p
                               join inv_ubicaciones u on u.id = p.id_ubicacion and u.id_sector = ?',[$idsector]);
        }else{
            $sucursal = Auth::user()->id_sucursal;
            $datos = DB::select('select p.id, p.descripcion
                               from inv_posiciones p
                               join inv_ubicaciones u on u.id = p.id_ubicacion
                               join inv_sectores se on se.id = u.id_sector and se.id_sucursal=?',[$sucursal]);
        }
        return $datos;
    }

    public function getsectoresubicacion($idubicacion=null){
        if (!empty($idubicacion) and $idubicacion!="null"){
            $datos = DB::select('select se.*
                           from inv_sectores se
                           join inv_ubicaciones u on se.id = u.id_sector and u.id=?',[$idubicacion]);
        }else{
            $sucursal = Auth::user()->id_sucursal;
            $datos = DB::select('select se.*
                           from inv_sectores se
                          where se.id_sucursal=?',[$sucursal]);
        }
        return $datos;
    }

    public function getposicionesubicacion($idubicacion=null){
        if (!empty($idubicacion) and $idubicacion!="null"){
            $datos = DB::select('select p.id, p.descripcion
                               from inv_posiciones p
                               join inv_ubicaciones u on p.id_ubicacion = u.id and u.id=?',[$idubicacion]);
        }else{
            $sucursal = Auth::user()->id_sucursal;
            $datos = DB::select('select p.id, p.descripcion
                               from inv_posiciones p
                               join inv_ubicaciones u on p.id_ubicacion = u.id
                               join inv_sectores se on u.id_sector = se.id and se.id_sucursal=?',[$sucursal]);
        }
        return $datos;
    }

    public function getsectoresposicion($idposicion=null){
        if (!empty($idposicion) and $idposicion!="null"){
            $datos = DB::select('select se.*
                           from inv_sectores se
                           join inv_ubicaciones u on se.id = u.id_sector
                           join inv_posiciones p on p.id_ubicacion = u.id and p.id=?',[$idposicion]);
        }else{
            $sucursal = Auth::user()->id_sucursal;
            $datos = DB::select('select se.*
                           from inv_sectores se
                          where id_sucursal=?',[$sucursal]);
        }
        return $datos;
    }

    public function getubicacionesposicion($idposicion=null){
        if (!empty($idposicion) and $idposicion!="null"){
            $datos = DB::select('select u.*
                               from inv_ubicaciones u
                               join inv_sectores se on u.id_sector = se.id
                               join inv_posiciones p on p.id_ubicacion = u.id and p.id=?',[$idposicion]);
        }else{
            $sucursal = Auth::user()->id_sucursal;
            $datos = DB::select('select u.*
                               from inv_ubicaciones u
                               join inv_sectores se on u.id_sector = se.id and se.id_sucursal=?',[$sucursal]);
        }
        return $datos;
    }


    public function getoctitular($idtitular=null){

        if (empty($idtitular)){
            $datos = [];
        }else{
            $datos = DB::select('select c.id,
                                        CONCAT (date_format(c.created_at, "%d/%m/%Y"),\'  $\',c.total) as descripcion
                                   from comprobantes c
                              left join comprobantes rec on rec.id_tipocpte=2 and rec.id_cpterel=c.id
                                  where c.id_titular=?
                                    and c.id_tipocpte=1
                                    and c.deleted_at is null
                                    and rec.id is null',[$idtitular]);
        }
        return $datos;

    }

    public function getocdetalle($idoc){
        $datos = DB::select('select ci.id_item,ci.preciouni as precio,ci.cantidad,ci.importe as total, ip.codproducto,
                                    CONCAT (i.descripcion,\' \',COALESCE(i.contNeto,""),\' \',COALESCE(u.descripcion,""),\' (\',COALESCE(m.descripcion,""),\') \') as producto
                               from comprobantes_items ci
                               join items i on i.id = ci.id_item
                          left join conf_marcas m on m.id = i.id_marca
                          left join conf_unidades_medida u on u.id = i.id_unidadcontneto
                          left join items_proveedores ip on ip.id_item = i.id
                              where ci.id_cpte=?',[$idoc]);
        return $datos;
    }

    public function getfaltantes($idtitular){
        $datos = DB::select('select i.id  as id_item,i.precioCosto as precio, stockMinimo as cantidad, i.precioCosto*i.stockMinimo as total, ip.codproducto,
                                  CONCAT (i.descripcion,\' \',COALESCE(i.contNeto,""),\' \',COALESCE(u.descripcion,""),\' (\',COALESCE(m.descripcion,""),\') \') as producto,
                                  SUM(ci.cantidad) as totalvendidos
                              from items i
                              join items_proveedores ip on ip.id_item=i.id and ip.id_titular=?
                              left join inv_stock s on s.id_item=i.id
                              left join conf_marcas m on m.id = i.id_marca
                              left join conf_unidades_medida u on u.id = i.id_unidadcontneto
                              join comprobantes_items ci on ci.id_item = i.id
                             where i.id_tipoitem=5
                               and i.stockMinimo > 0
                               and i.activo=1
                               and s.id is null
                             GROUP BY 1,2,3,4,5,6
                            union
                            select i.id as id_item,i.precioCosto as precio, (i.stockMinimo-SUM(s.cantidad)) as cantidad, i.precioCosto*i.stockMinimo as total, ip.codproducto,
                                  CONCAT (i.descripcion,\' \',COALESCE(i.contNeto,""),\' \',COALESCE(u.descripcion,""),\' (\',COALESCE(m.descripcion,""),\') \') as producto,
                                  SUM(ci.cantidad) as totalvendidos
                              from items i
                              join items_proveedores ip on ip.id_item=i.id and ip.id_titular=?
                              join inv_stock s on s.id_item=i.id
                              left join conf_marcas m on m.id = i.id_marca
                              left join conf_unidades_medida u on u.id = i.id_unidadcontneto
                              join comprobantes_items ci on ci.id_item = i.id
                             where i.id_tipoitem=5
                               and i.stockMinimo > 0
                               and i.activo=1
                             group by 1,2,4,5,6
                             having cantidad>0
                             order by totalvendidos desc
                             limit 50',[$idtitular,$idtitular]);
        return $datos;
    }

    public function getitemsproveedor($idtitular){
        $datos = DB::select('select i.id,
                                    CONCAT (i.descripcion,\' \',COALESCE(i.contNeto,""),\' \',COALESCE(u.descripcion,""),\' (\',COALESCE(m.descripcion,""),\') [ean:\',COALESCE(i.ean,""),\' / CodProv:\',COALESCE(ip.codproducto,""),\']\') as producto
                               from items as i
                          left join conf_marcas as m on m.id = i.id_marca
                          left join conf_unidades_medida as u on u.id = i.id_unidadcontneto
                               join items_proveedores as ip on ip.id_item = i.id and ip.id_titular = ?
                              where id_tipoitem = 5
                                and i.activo = 1',[$idtitular]);
        return $datos;
    }

    public function getremitostitular($idtitular){
        $datos = DB::select('select id,id as rem
                               from comprobantes
                              where id_tipocpte = 5
                                and id_estado = 2
                                and id_titular=?',[$idtitular]);
        return $datos;
    }

    public function getremitosyfacturas(){
        $date1 = 'DATE_ADD(NOW(), INTERVAL -2 MONTH)';
        $date2 = 'LAST_DAY(NOW())';
        $datos = DB::select('select c.id,concat(tc.descripcion," ",COALESCE(c.numero,"")," $ ",c.total," ",t.descripcion) as text
                               from comprobantes c
                               join sys_tipocomprobantes tc on tc.id=c.id_tipocpte
                               join titulares t on t.id =c.id_titular
                              where id_tipocpte in (5,6)
                                and c.created_at BETWEEN '.$date1.' AND '.$date2.'
                                and c.deleted_at is null
                                and c.id_titular != 2
                                order by 1');
        return $datos;
    }

    public function getsaldotitular($idtitular,$id=null){
        $now = time();
        $temptable = 'SALDO'.$idtitular.$now;
//        if (!empty($id)){
            DB::select('CREATE temporary TABLE '.$temptable.'
                     SELECT SUM(c.total) AS saldo, (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) AS pasaron
                       FROM comprobantes c
                       JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                      WHERE c.id_tipocpte in (5,6,3)
                        AND ((ISNULL(c.id_cpterel)) OR (c.id_tipocpte IN (6 , 3) AND ISNULL(c.deleted_at)))
                        AND c.id_titular = ?
                        AND (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) > 16
                        GROUP BY 2
                      UNION
                     SELECT SUM((c.total * -(1))) AS saldo, 1 AS pasaron
                       FROM comprobantes c
                      WHERE c.id_tipocpte IN (7,13,14)
                        AND ISNULL(c.deleted_at)
                        AND c.id_titular = ?
                        AND c.id_estado = 3
                        GROUP BY 2',[$idtitular,$idtitular]);
//        }else{
//            DB::select('CREATE temporary TABLE '.$temptable.'
//                     SELECT SUM(c.total) AS saldo
//                       FROM comprobantes c
//                       JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
//                      WHERE c.id_tipocpte in (5,6,3)
//                        AND ((ISNULL(c.id_cpterel)) OR (c.id_tipocpte IN (6 , 3) AND ISNULL(c.deleted_at)))
//                        AND c.id_titular = ?
//                      UNION
//                     SELECT SUM((c.total * -(1))) AS saldo
//                       FROM comprobantes c
//                      WHERE c.id_tipocpte IN (7,13,14)
//                        AND ISNULL(c.deleted_at)
//                        AND c.id_estado = 3
//                        AND c.id_titular = ? ',[$idtitular,$idtitular]);
//        }
        $saldoq=DB::table($temptable.' as cc')->sum('saldo');
        $saldo = floatval($saldoq);
        if ($saldo < 0){
            $saldo = 0;
        }
        return $saldo;
    }

    public function getproductos(){
        $items = DB::table('items as i')->where('id_tipoitem', 5)
            ->where('i.activo', 1)
            ->whereNull('i.deleted_at')
            ->leftJoin('conf_marcas as m', 'm.id', '=', 'i.id_marca')
            ->leftJoin('conf_unidades_medida as u', 'u.id', '=', 'i.id_unidadcontneto')
            ->leftJoin('conf_subrubros as sr', 'sr.id', '=', 'i.id_subrubro')
            ->select('i.id',
                     DB::raw('CONCAT (\'(\',i.id,\') \',i.descripcion,\' \',COALESCE(i.contNeto,""),\' \',COALESCE(u.descripcion,""),\' $ \',i.precio,\' \',COALESCE(m.descripcion,""),\' [ean:\',COALESCE(i.ean,""),\']\') as producto'))
            ->get();
        return $items;
        /*DB::raw('CONCAT (\'(\',i.id,\') \',i.descripcion,\' \',COALESCE(i.contNeto,""),\' \',COALESCE(u.descripcion,""),\' $ \',i.precio,\' \',COALESCE(sr.descripcion,""),\' \',COALESCE(m.descripcion,""),\' [ean:\',COALESCE(i.ean,""),\']\') as producto'))*/
    }

    public function getproductos_v2(Request $request){
        $cond='WHERE 1=1';
        $search = explode( ' ',$request->term);
        foreach ($search as $item){
            $cond=$cond." AND data.producto like '%".$item."%'";
        }
        $items['results'] = DB::select("select data.id as id, data.producto as text
                              from (select i.id,
                                 CONCAT ('(',i.id,') ',
                                            i.descripcion,' ',
                                            COALESCE(i.contNeto,\"\"),' ',
                                            COALESCE(u.descripcion,\"\"),
                                            ' $ ',i.precio,' ',
                                            COALESCE(sr.descripcion,\"\"), ' ',
                                            COALESCE(m.descripcion,\"\"),
                                            ' [ean:',COALESCE(i.ean,\"\"),']') as producto
                            from items as i
                            left join conf_marcas as m on m.id = i.id_marca
                            left join conf_unidades_medida as u on u.id = i.id_unidadcontneto
                            left join conf_subrubros as sr on sr.id = i.id_subrubro
                            where id_tipoitem = 5
                              and i.activo = 1
                              and i.deleted_at is null) as data ".$cond);
        return json_encode($items);
        /*DB::raw('CONCAT (\'(\',i.id,\') \',i.descripcion,\' \',COALESCE(i.contNeto,""),\' \',COALESCE(u.descripcion,""),\' $ \',i.precio,\' \',COALESCE(sr.descripcion,""),\' \',COALESCE(m.descripcion,""),\' [ean:\',COALESCE(i.ean,""),\']\') as producto'))*/
    }

    public function getproductosrem(){
        $items = DB::select('select p.id, p.ean,p.precio,ma.descripcion as marca, ru.descripcion as rubro,  sr.descripcion as subrubro,
                                            UPPER(concat_ws(\' \',p.descripcion,\'-\', CASE WHEN (p.contNeto-FLOOR(p.contNeto)) > 0 THEN p.contNeto ELSE FLOOR(p.contNeto) END, um.descripcion)) as producto,
                                            (select count(*) 
                                               from comprobantes_items ci
                                               join ( select c.id from comprobantes c order by 1 desc limit 100) l50c on l50c.id=ci.id_cpte
                                              where ci.id_item=p.id 
                                              order by 1) as vendidos
                                        from items p
                                        left join conf_marcas ma on ma.id=p.id_marca
                                        left join conf_rubros ru on ru.id=p.id_rubro
                                        left join conf_subrubros sr on sr.id=p.id_subrubro
                                        left join conf_unidades_medida um on um.id=p.id_unidadcontneto  
                                        where id_tipoitem=5
                                          and p.activo = 1
                                          and deleted_at is null
                                        order by vendidos desc,1 desc limit 2000');
        return $items;
    }

    public function getproductosctrolstk($idctrol,$idconteo=null){

        if (!empty($idconteo)){
            $conteo = ControlStockConteo::find($idconteo);
            if (!empty($conteo->id_item)){
                $items = DB::table('items as i')->where('id_tipoitem', 5)
                    ->where('i.activo', 1)
                    ->where('i.id', $conteo->id_item)
                    ->leftJoin('conf_marcas as m', 'm.id', '=', 'i.id_marca')
                    ->leftJoin('conf_unidades_medida as u', 'u.id', '=', 'i.id_unidadcontneto')
                    ->leftJoin('conf_subrubros as sr', 'sr.id', '=', 'i.id_subrubro')
                    ->select('i.id',
                        DB::raw('CONCAT (\'(\',i.id,\') \',i.descripcion,\' \',COALESCE(i.contNeto,""),\' \',COALESCE(u.descripcion,""),\' $ \',i.precio,\' \',COALESCE(m.descripcion,""),\' [ean:\',COALESCE(i.ean,""),\']\') as producto'))
                    ->get();
                return $items;
            }
        }
        $ctrol = ControlStock::find($idctrol);
        if (!empty($ctrol->id_item)){
            $items = DB::table('items as i')->where('id_tipoitem', 5)
                ->where('i.activo', 1)
                ->where('i.id', $ctrol->id_item)
                ->leftJoin('conf_marcas as m', 'm.id', '=', 'i.id_marca')
                ->leftJoin('conf_unidades_medida as u', 'u.id', '=', 'i.id_unidadcontneto')
                ->leftJoin('conf_subrubros as sr', 'sr.id', '=', 'i.id_subrubro')
                ->select('i.id',
                    DB::raw('CONCAT (\'(\',i.id,\') \',i.descripcion,\' \',COALESCE(i.contNeto,""),\' \',COALESCE(u.descripcion,""),\' $ \',i.precio,\' \',COALESCE(m.descripcion,""),\' [ean:\',COALESCE(i.ean,""),\']\') as producto'))
                ->get();
            return $items;
        }else{
            $items = DB::table('items as i')->where('id_tipoitem', 5)
                ->where('i.activo', 1)
                ->leftJoin('conf_marcas as m', 'm.id', '=', 'i.id_marca')
                ->leftJoin('conf_unidades_medida as u', 'u.id', '=', 'i.id_unidadcontneto')
                ->leftJoin('conf_subrubros as sr', 'sr.id', '=', 'i.id_subrubro')
                ->select('i.id',
                    DB::raw('CONCAT (\'(\',i.id,\') \',i.descripcion,\' \',COALESCE(i.contNeto,""),\' \',COALESCE(u.descripcion,""),\' $ \',i.precio,\' \',COALESCE(m.descripcion,""),\' [ean:\',COALESCE(i.ean,""),\']\') as producto'))
                ->get();
            return $items;
        }


    }


    public function gettitulares($consumidorfinal=1){
        if($consumidorfinal==1){
            $items = DB::table('titulares')->whereIn('id_tipotitular', [2,3,4])->where('activo', 1)->get();
        }else{
            $items = DB::table('titulares')->whereIn('id_tipotitular', [2,3,4])->where('activo', 1)->where('id','<>', 2)->get();
        }
        return $items;
    }


    public function getvencidostotales(){
        ini_set('max_execution_time', '600');

        $totales['items'] = DB::select('SELECT cc.cliente, cc.id_titular,
                                               SUM(CASE WHEN (pasaron < 15) THEN saldocpte ELSE 0 END) AS sinvencer,
                                               SUM(CASE WHEN (pasaron between 15 and 90) THEN saldocpte ELSE 0 END) AS m1,
                                               SUM(CASE WHEN (pasaron between 91 and 180) THEN saldocpte ELSE 0 END) AS m2,
                                               SUM(CASE WHEN (pasaron > 180) THEN saldocpte ELSE 0 END) AS m3,  
                                               SUM(saldocpte) AS saldo
                                        FROM ctacte_view cc
                                        WHERE cc.id_estado = 2
                                        GROUP BY 1,2
                                        ORDER BY 7 DESC');

        $saldo=0;
        $saldo=DB::table('ctacte_view as cc')->sum('saldocpte');
        $totales['saldo'] = floatval($saldo);
        if ($saldo>0){
            $totales['saldosv']=floatval(DB::table('ctacte_view as cc')
                                            ->where('cc.pasaron','<', 15)
                                            ->sum('cc.saldocpte'));
            $totales['porcsv']= ($totales['saldosv']>0) ? round($totales['saldosv']*100/$saldo) : 0 ;

            $totales['saldov1']=floatval(DB::table('ctacte_view as cc')
                                    ->whereBetween('cc.pasaron', [15,90])
                                    ->sum('cc.saldocpte'));
            $totales['porcv1']= ($totales['saldov1']>0) ? round($totales['saldov1']*100/$saldo) : 0 ;

            $totales['saldov2']=floatval(DB::table('ctacte_view as cc')
                                    ->whereBetween('cc.pasaron', [91,180])
                                    ->sum('cc.saldocpte'));
            $totales['porcv2']= ($totales['saldov2']>0) ? round($totales['saldov2']*100/$saldo) : 0 ;

            $totales['saldov3']=floatval(DB::table('ctacte_view as cc')
                                    ->where('cc.pasaron','>', 180)
                                    ->sum('cc.saldocpte'));
            $totales['porcv3']= ($totales['saldov3']>0) ? round($totales['saldov3']*100/$saldo) : 0 ;
        }else{
            $totales['saldosv']=0;$totales['saldov1']=0;$totales['saldov2']=0;$totales['saldov3']=0;
            $totales['porcsv']=0;$totales['porcv1']=0;$totales['porcv2']=0;$totales['porcv3']=0;
        }
        return $totales;
    }
}

