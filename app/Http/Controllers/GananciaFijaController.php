<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GananciaFija;
use DB;

class GananciaFijaController extends Controller
{
    public function index()
    {
        $items = DB::table('conf_ganancia_fija as gf')
            ->leftJoin('sys_tipoclientes as tc', 'tc.id', '=', 'gf.id_tipocliente')
            ->select('gf.*', DB::raw('date_format(gf.created_at, "%d/%m/%Y %H:%i:%s") as creado'),
                     'tc.descripcion as tipocliente')
            ->get();

        return view('precios.gananciafija.index',[
            'titulo' => 'Ganancia Fija',
            'urlkey' => 'precios/gananciafija',
            'items' => $items,
            'itemnav' => 'ganf'
        ]);
    }

    public function create()
    {
        return view('precios.gananciafija.form',[
            'titulo' => 'Ganancia Fija',
            'tituloform' => 'Nuevo',
            'urlkey' => 'precios/gananciafija',
            'edit'=> 1,
            'itemnav' => 'ganf',
            'accion' => 'Crear'
        ]);
    }

    public function store(Request $request)
    {
//        dd($request->request);
        $this->validate($request, [
            'id_tipo' => 'required',
            'porcentaje' => 'required'
        ]);

        if (empty($request->id)){
            $item = new GananciaFija();
            $mensaje = 'Creado Exitosamente!';
        }else{
            $item = GananciaFija::find($request->id);
            $mensaje = 'Editado Exitosamente!';
        }

        $item->id_tipocliente =  $request->id_tipo;
        $item->porcentaje = $request->porcentaje;
        $item->activo = (!empty($request->activo)) ? 1 : 0;
        $item->save();
        return redirect('/precios/gananciafija')->with('status', $mensaje);
    }

    public function show($id)
    {
        $item = GananciaFija::find($id);
        return view('precios.gananciafija.form',[
            'titulo' => 'Ganancia Fija',
            'urlkey' => 'precios/gananciafija',
            'edit'=> 0,
            'itemnav' => 'ganf',
            'item' => $item,
            'accion' => 'Visualizar'
        ]);
    }

    public function edit($id)
    {
        $item = GananciaFija::find($id);
        return view('precios.gananciafija.form',[
            'titulo' => 'Ganancia Fija',
            'urlkey' => 'precios/gananciafija',
            'edit'=> 1,
            'itemnav' => 'ganf',
            'item' => $item,
            'accion' => 'Editar'
        ]);
    }
}
