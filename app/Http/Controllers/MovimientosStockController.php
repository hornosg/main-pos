<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\MotivoMStock;
use App\Models\Marca;
use Illuminate\Http\Request;
use App\Models\Sector;
use App\Models\Ubicacion;
use App\Models\Posicion;
use App\Models\MovimientoStock;
use App\Models\Stock;
use Dompdf\Dompdf;
use DB;

class MovimientosStockController extends Controller
{
    public function index()
    {
        $items = DB::table('inv_movimientos_stock as m')
            ->leftJoin('sys_motivos_mstock as mt', 'mt.id', '=', 'm.id_motivo')
            ->leftJoin('items as i', 'i.id', '=', 'm.id_item')
            ->leftJoin('inv_sectores as so', 'so.id', '=', 'm.id_sector_o')
            ->leftJoin('inv_ubicaciones as uo', 'uo.id', '=', 'm.id_ubicacion_o')
            ->leftJoin('inv_posiciones as po', 'po.id', '=', 'm.id_posicion_o')
            ->leftJoin('inv_sectores as sd', 'sd.id', '=', 'm.id_sector_d')
            ->leftJoin('inv_ubicaciones as ud', 'ud.id', '=', 'm.id_ubicacion_d')
            ->leftJoin('inv_posiciones as pd', 'pd.id', '=', 'm.id_posicion_d')
            ->leftJoin('conf_marcas as ma', 'ma.id', '=','i.id_marca')
            ->leftJoin('conf_rubros as ru', 'ru.id', '=','i.id_rubro')
            ->whereNull('m.deleted_at')
            ->select('m.*', 'mt.descripcion as motivo',
                     DB::raw('date_format(m.created_at, "%d/%m/%Y %H:%i:%s") as creado'),
                     DB::raw("concat('[',i.id,'] ', i.descripcion, ' (', COALESCE(ma.descripcion,''), ')') as producto"),
                     DB::raw("CASE WHEN pd.id IS NOT NULL THEN pd.descripcion WHEN ud.id IS NOT NULL THEN ud.alias ELSE sd.alias END as destino"),
                     DB::raw("CASE WHEN po.id IS NOT NULL THEN po.descripcion WHEN uo.id IS NOT NULL THEN uo.alias ELSE so.alias END as origen"))
            ->get();
//dd($items);
        return view('inventario.movimientos.index',[
            'titulo' => 'Movimientos de Stock',
            'items' => $items,
            'itemnav' => 'movstk'
        ]);
    }

    public function create()
    {
        return view('inventario.movimientos.form',[
            'titulo' => 'Movimiento de Stock',
            'tituloform' => 'Nuevo Movimiento de Stock',
            'accion' => 'Nueva',
            'itemnav' => 'movstk',
            'edit'=> 1,
        ]);
    }

    public function store(Request $request)
    {
        if (empty($request->id)){
            $movstk = new MovimientoStock();
        }else{
            $movstk = MovimientoStock::find($request->id);
            $movstk->updated_us = auth()->id();
        }
//dd($request->request);
        $movstk->id_motivo =$request->id_motivo;
        $movstk->id_item =$request->id_item;
        $movstk->id_sector_o =$request->id_sector_o;
        $movstk->id_ubicacion_o =$request->id_ubicacion_o;
        $movstk->id_posicion_o =$request->id_posicion_o;
        $movstk->id_sector_d =$request->id_sector_d;
        $movstk->id_ubicacion_d =$request->id_ubicacion_d;
        $movstk->id_posicion_d =$request->id_posicion_d;
        if ($movstk->id_motivo==5){
            $movstk->cantidad =$request->cantidad * -1;
        }else{
            $movstk->cantidad =$request->cantidad;
        }
//dd($movstk);
        $movstk->save();

        if (!empty($request->id)){
            Stock::where('id_movstk', $request->id)->delete();
        }

        if (($movstk->id_motivo!=4) and ($movstk->id_motivo!=5)) {
            $stk_o = new Stock();
            $stk_o->id_item = $movstk->id_item;
            $stk_o->id_sector = $movstk->id_sector_o;
            $stk_o->id_ubicacion = $movstk->id_ubicacion_o;
            $stk_o->id_posicion = $movstk->id_posicion_o;
            $stk_o->cantidad = $movstk->cantidad * -1;
            $stk_o->id_movstk = $movstk->id;
            $stk_o->save();
        }

        $stk_d = new Stock();
        $stk_d->id_item = $movstk->id_item;
        $stk_d->id_sector = $movstk->id_sector_d;
        $stk_d->id_ubicacion = $movstk->id_ubicacion_d;
        $stk_d->id_posicion = $movstk->id_posicion_d;
        $stk_d->cantidad = $movstk->cantidad;
        $stk_d->id_movstk = $movstk->id;
        $stk_d->save();

        //OJO CON ACTUALZIAR STOCK!.. BORRAR STOCK o VENTAS HUERFANOS
        //$this->ActualizarEstados($movstk->id_titular);

        return redirect('/inventario/movimientos/index');
    }

    public function ajaxstore(Request $request)
    {
//        dd($request->request);
        $this->validate($request, [
            'id_motivo' => 'required',
            'id_item'=> 'required',
            'cantidad'=> 'required',
            'id_sector_d'=> 'required'
        ]);

        $movstk = new MovimientoStock();
        $movstk->id_motivo =$request->id_motivo;
        $movstk->id_item =$request->id_item;
        $movstk->id_sector_o =$request->id_sector_o;
        $movstk->id_ubicacion_o =$request->id_ubicacion_o;
        $movstk->id_posicion_o =$request->id_posicion_o;
        $movstk->id_sector_d =$request->id_sector_d;
        $movstk->id_ubicacion_d =$request->id_ubicacion_d;
        $movstk->id_posicion_d =$request->id_posicion_d;
        if ($movstk->id_motivo==5){
            $movstk->cantidad =$request->cantidad * -1;
        }else{
            $movstk->cantidad =$request->cantidad;
        }
        $movstk->save();

        if (($movstk->id_motivo!=4) and ($movstk->id_motivo!=5)) {
            $stk_o = new Stock();
            $stk_o->id_item = $movstk->id_item;
            $stk_o->id_sector = $movstk->id_sector_o;
            $stk_o->id_ubicacion = $movstk->id_ubicacion_o;
            $stk_o->id_posicion = $movstk->id_posicion_o;
            $stk_o->cantidad = $movstk->cantidad * -1;
            $stk_o->id_movstk = $movstk->id;
            $stk_o->save();
        }

        $stk_d = new Stock();
        $stk_d->id_item = $movstk->id_item;
        $stk_d->id_sector = $movstk->id_sector_d;
        $stk_d->id_ubicacion = $movstk->id_ubicacion_d;
        $stk_d->id_posicion = $movstk->id_posicion_d;
        $stk_d->cantidad = $movstk->cantidad;
        $stk_d->id_movstk = $movstk->id;
        $stk_d->save();

        $motivo=MotivoMStock::find($movstk->id_motivo);
        $producto=Item::find($movstk->id_item);
        $marca=Marca::find($producto->id_marca);
        $movstk->motivo = $motivo->descripcion;
        $movstk->producto = "(".$producto->id.") ".$producto->descripcion." ".$producto->contNeto." ".$marca->descripcion;
        $movstk->creado = $movstk->created_at;

        if (!empty($movstk->id_posicion_o)){
            $posicion_o=Posicion::find($movstk->id_posicion_o);
            $movstk->origen=$posicion_o->descripcion;
        }elseif(!empty($movstk->id_ubicacion_o)){
            $ubicacion_o=Posicion::find($movstk->id_ubicacion_o);
            $movstk->origen=$ubicacion_o->alias;
        }elseif(!empty($movstk->id_sector_o)){
            $sector_o=Sector::find($movstk->id_sector_o);
            $movstk->origen=$sector_o->alias;
        }

        if (!empty($movstk->id_posicion_d)){
            $posicion_d=Posicion::find($movstk->id_posicion_d);
            $movstk->destino=$posicion_d->descripcion;
        }elseif(!empty($movstk->id_ubicacion_d)){
            $ubicacion_d=Posicion::find($movstk->id_ubicacion_d);
            $movstk->destino=$ubicacion_d->alias;
        }else{
            $sector_d=Sector::find($movstk->id_sector_d);
            $movstk->destino=$sector_d->alias;
        }

        $data['msg']="Producto Creado Exitosamente!";
        $data['item']=$movstk;
        return $data;
    }
}
