<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Titular;
use Carbon\Carbon;

class TitularesController extends Controller
{
    public function ajaxstore(Request $request)
    {
        $this->validate($request, [
            'descripciontit' => 'required',
            'direccion' => 'required',
            'telefono' =>'required'
        ]);

        $titular =  new Titular();
        $titular->descripcion = strtoupper($request->descripciontit);
        $titular->id_tipotitular =  2; //Cliente!
        $titular->id_tipocliente =  (!empty($request->id_tipocliente))?$request->id_tipocliente:null;
        $titular->id_tipoiva =  (!empty($request->id_tipoiva))?$request->id_tipoiva:null;
        $titular->direccion = $request->direccion;
        $titular->id_localidad = $request->id_localidad;
        $titular->telefono = $request->telefono;
        $titular->email = $request->email;
        $titular->activo = 1;
        $titular->fecha_cumple = Carbon::parse($request->fecha_cumple);
        $titular->save();

        $data['msg']="Creado Exitosamente!";
        $data['id']=$titular->id;
        $data['text']=$titular->descripcion;
        return $data;
    }
}
