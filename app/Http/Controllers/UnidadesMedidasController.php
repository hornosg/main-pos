<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\UnidadMedida;
use DB;
use Illuminate\Http\Request;
use Excel;


class UnidadesMedidasController extends Controller
{
    //
    public function index()
    {
        $items = UnidadMedida::all();
        return view('master.index', ['titulo' => 'Unidades de Medida',
                                     'urlkey' => 'unidadesMedidas',
                                     'items' => $items,
                                     'itemnav' => 'otro']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
        ]);


        $unidadMedida = $request->all();
        $unidadMedida['descripcion'] = strtoupper($unidadMedida['descripcion']);

        if (isset($unidadMedida['activo'])){
            $unidadMedida['activo']=1;
        }else{
            $unidadMedida['activo']=0;
        }
        $data=UnidadMedida::create($unidadMedida);
        if ($unidadMedida['activo']==1){
            $data['activo']='SI';
        }else{
            $data['activo']='NO';
        }
        $data['msg']="Unidad Creada Exitosamente!";
        return $data;
    }

    public function edit($id)
    {
        $item = UnidadMedida::find($id);
        return view('master.edit', ['titulo' => 'Unidades de Medida','urlkey' => 'unidadesMedidas' ,'item' => $item]);

    }

    public function show($id)
    {
        $item = UnidadMedida::find($id);
        return view('master.show', ['titulo' => 'Unidades de Medida','urlkey' => 'unidadesMedidas' ,'item' => $item]);
    }


    public function update($id,Request $request)
    {
        $unidadMedida = UnidadMedida::find($id);
        $unidadMedida->descripcion = strtoupper($request->descripcion);

        if (isset($request->activo)){
            $unidadMedida->activo=1;
        }else{
            $unidadMedida->activo=0;
        }

        $unidadMedida->save();
        $data['msg']="Marca Modificada.";
        return $data;

    }

    public function delete($id)
    {
        $unidadMedida = UnidadMedida::find($id);
        $unidadMedida->delete();
        $data['msg']="Unidad de Medida Eliminada!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $unidadMedida = UnidadMedida::find($item);
            $unidadMedida->delete();
        }
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $items[] = ['descripcion' => $value->descripcion, 'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_unidades_medida')->insert($items);
                    return redirect('/unidadesMedidas')->with('status', 'Unidades de Medida Importados Correctamente!');
                }else{
                    return redirect('/unidadesMedidas')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }

    }
}
