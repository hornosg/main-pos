<?php

namespace App\Http\Controllers;

use App\Models\Posicion;
use App\Models\Sucursal;
use App\Models\Ubicacion;
use App\Models\Sector;
use App\Models\Titular;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Models\Cliente;
use App\Models\Comprobante;
use App\Models\ComprobanteItems;
use App\Models\ComprobanteMp;
use App\Models\ComprobanteDI;
use App\Models\MovimientoStock;
use App\Models\Stock;
use App\Models\Caja;
use App\Models\TipoComprobante;
use App\Models\Notificacion;
use App\Models\NotificacionEstado;
use Dompdf\Dompdf;
use DB;
use Artisan;
use Log;

class ComprobantesController extends Controller
{
    protected $tipo                 = null;
    protected $idtipo               = null;
    protected $dir                  = "";
    protected $titulo               = "";
    protected $itemnav              = "";
    protected $tmovstk              = null;
    protected $sector               = null;
    protected $cpte                 = null;
    protected $actionLabel          = "";
    protected $titularDescripcion   = null;

    private function _initOc(){
        $this->idtipo   = 1;
        $this->dir      = "ordenesdecompra";
        $this->titulo   = 'Ordenes de Compra';
        $this->itemnav  = 'oc';
    }

    private function _initRec(){
        $this->idtipo   = 2;
        $this->dir      = "recepciones";
        $this->titulo   = 'Recepcion';
        $this->itemnav  = 'rec';
        $this->tmovstk  = 1;
        $this->sector   = 1;
    }

    private function _initNd(){
        $this->idtipo   = 3;
        $this->dir      = "notadedebito";
        $this->titulo   = 'Notas de Debito';
        $this->itemnav  = 'nd';
        $this->sector   = 2;
    }

    private function _initRem(){
        $this->idtipo   = 5;
        $this->dir      = "remitos";
        $this->titulo   = 'Remitos';
        $this->itemnav  = 'rem';
        $this->tmovstk  = 7;
    }

    private function _initFc(){
        $this->idtipo   = 6;
        $this->dir      = "facturas";
        $this->titulo   = 'Facturas';
        $this->itemnav  = 'fc';
        $this->tmovstk  = 7;
    }

    private function _initRbo(){
        $this->idtipo   = 7;
        $this->dir      = "recibos";
        $this->titulo   = 'Recibos';
        $this->itemnav  = 'rbo';
    }

    private function _initNc(){
        $this->idtipo   = 13;
        $this->dir      = "notadecredito";
        $this->titulo   = 'Notas de Crédito';
        $this->itemnav  = 'nc';
        $this->tmovstk  = 6;
    }

    private function _initComi(){
        $this->idtipo   = 14;
        $this->dir      = "comisiones";
        $this->titulo   = 'Comisiones';
        $this->itemnav  = 'comi';
    }
    private function _initPre(){
        $this->idtipo   = 16;
        $this->dir      = "presupuestos";
        $this->titulo   = 'Presupuestos';
        $this->itemnav  = 'pre';
    }
    private function _initPgo(){
        $this->idtipo   = 18;
        $this->dir      = "pagos";
        $this->titulo   = 'Pago a Provedores';
        $this->itemnav  = 'pgo';
    }

    private function _init(string $tipo){
        $this->tipo = $tipo;
        switch ($this->tipo) {
            case 'oc':
                $this->_initOc();
                break;
            case 'rec':
                $this->_initRec();
                break;
            case 'nd':
                $this->_initNd();
                break;
            case 'rem':
                $this->_initRem();
                break;
            case 'fc':
                $this->_initFc();
                break;
            case 'fcr':
                $this->_initFc();
                break;
            case 'rbo':
                $this->_initRbo();
                break;
            case 'nc':
                $this->_initNc();
                break;
            case 'comi':
                $this->_initComi();
                break;
            case 'pre':
                $this->_initPre();
                break;
            case 'pgo':
                $this->_initPgo();
                break;
        }
    }

    private function _initialize(string $tipo){
        $this->tipo = $tipo;
        $this->_init($this->tipo);
    }

    public function index($tipo)
    {
        $this->_initialize($tipo);

        if ($this->idtipo!=14){
            $items = DB::table('comprobantes as c')
                ->where('c.id_tipocpte', $this->idtipo)
                ->whereNull('c.deleted_at')
                ->whereRaw('c.created_at BETWEEN DATE_ADD(NOW(), INTERVAL -1 MONTH) AND NOW()')
                ->leftJoin('titulares as t', 't.id', '=', 'c.id_titular')
                ->leftJoin('users as us', 'us.id', '=', 'c.created_us')
                ->select('c.*',
                    DB::raw('date_format(c.created_at, "%d/%m/%Y %H:%i:%s") as creado'),
                    DB::raw('(TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) as pasaron'),
                    't.descripcion as titular',
                    'us.name as usuario',
                    DB::raw('date_format(c.updated_at, "%d/%m/%Y %H:%i:%s") as  ultmodif'))
                ->get();
        }else{
            $items = DB::table('comprobantes as c')
                ->where('c.id_tipocpte', $this->idtipo)
                ->whereNull('c.deleted_at')
                ->whereRaw('c.created_at BETWEEN DATE_ADD(NOW(), INTERVAL -3 MONTH) AND NOW()')
                ->leftJoin('titulares as t', 't.id', '=', 'c.id_titular')
                ->leftJoin('users as us', 'us.id', '=', 'c.created_us')
                ->leftJoin('comprobantes as crel', 'crel.id', '=', 'c.id_cpterel')
                ->leftJoin('titulares as trel', 'trel.id', '=', 'crel.id_titular')
                ->select('c.*',
                    DB::raw('date_format(c.created_at, "%d/%m/%Y %H:%i:%s") as creado'),
                    DB::raw('(TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) as pasaron'),
                    't.descripcion as titular',
                    'crel.id as idrel',
                    'crel.numero as numerorel',
                    'crel.total as totalrel',
                    'crel.id_estado as id_estadorel',
                    'trel.id as id_titularrel',
                    'trel.descripcion as titularrel',
                    'us.name as usuario',
                    DB::raw('date_format(c.updated_at, "%d/%m/%Y %H:%i:%s") as  ultmodif'))
                ->get();
        }

        return view('comprobantes.'.$this->dir.'.index',[
            'titulo' => $this->titulo,
            'items' => $items,
            'itemnav' => $this->itemnav
        ]);
    }

    public function create($tipo)
    {
        switch ($tipo) {
            case 'oc':
                $idtipo= 1;
                $dir = "ordenesdecompra";
                $titulo = 'Ordenes de Compra';
                $tituloform = 'Orden de Compra';
                $itemnav = 'oc';
                $vsrem = false;
                $accion = 'Nueva';
                break;
            case 'rec':
                $idtipo= 2;
                $dir = "recepciones";
                $titulo = 'Recepcion';
                $tituloform = 'Recepcion';
                $itemnav = 'rec';
                $vsrem = false;
                $accion = 'Nueva';
                break;
            case 'nd':
                $idtipo= 3;
                $dir = "notadedebito";
                $titulo = 'Notas de Debito';
                $tituloform = 'Nota de Debito';
                $itemnav = 'nd';
                $vsrem = false;
                $accion = 'Nuevo';
                break;
            case 'rem':
                $idtipo= 5;
                $dir = "remitos";
                $titulo = 'Remitos';
                $tituloform = 'Remito';
                $itemnav = 'rem';
                $vsrem = false;
                $accion = 'Nuevo';
                break;
            case 'fc':
                $idtipo= 6;
                $dir = "facturas";
                $titulo = 'Facturas';
                $tituloform = 'Factura';
                $itemnav = 'fc';
                $vsrem = false;
                $accion = 'Nueva';
                break;
            case 'fcr':
                $idtipo= 6;
                $dir = "facturas";
                $titulo = 'Facturas';
                $tituloform = 'Factura';
                $itemnav = 'fc';
                $vsrem = true;
                $accion = 'Nueva';
                break;
            case 'rbo':
                $idtipo= 7;
                $dir = "recibos";
                $titulo = 'Recibos';
                $tituloform = 'Recibo';
                $itemnav = 'rbo';
                $vsrem = false;
                $accion = 'Nuevo';
                break;
            case 'nc':
                $idtipo= 13;
                $dir = "notadecredito";
                $titulo = 'Notas de Crédito';
                $tituloform = 'Nota de Crédito';
                $itemnav = 'nc';
                $vsrem = false;
                $accion = 'Nuevo';
                break;
            case 'pre':
                $idtipo= 16;
                $dir = "presupuestos";
                $titulo = 'Presupuesto';
                $tituloform = 'Presupuesto';
                $itemnav = 'pre';
                $vsrem = false;
                $accion = 'Nuevo';
                break;
            case 'pgo':
                $idtipo= 18;
                $dir = "pagos";
                $titulo = 'Pago a Provedores';
                $tituloform = 'Pago a Provedores';
                $itemnav = 'pgo';
                $vsrem = false;
                $accion = 'Nuevo';
                break;
        }
        if ($tipo=='rem'){
            if (env('MP_POCOSPRODUCTOS')){
                $view = 'comprobantes.'.$dir.'.formMin';
            }else{
                $view = 'comprobantes.'.$dir.'.form';
            }
        }else{
            $view = 'comprobantes.'.$dir.'.form';
        }

        return view($view,[
            'titulo' => $titulo,
            'accion' => $accion,
            'tituloform' => $tituloform,
            'titulodetart' => 'Detalle de Productos',
            'itemnav' => $itemnav,
            'edit'=> 1,
            'vsrem'=> $vsrem
        ]);
    }

    private function _instancie($id=null){
        if (empty($id)){
            $this->cpte = new Comprobante();
            $this->cpte->created_us = auth()->id();
            $this->actionLabel='Creado';
        }else{
            $this->cpte = Comprobante::find($id);
            $this->cpte->updated_us = auth()->id();
            $this->actionLabel='Modifiado';
        }
    }

    public function store($tipo,Request $request)
    {
        $this->_initialize($tipo);
        DB::beginTransaction();
            /*---------------------------------------------------------*/
            /*Cabecera                                                 */
            /*---------------------------------------------------------*/

            //Log::info('Store Comprobante: '.$tipo.' '.$request->id);

            $this->_instancie($request->id);

            $this->cpte->id_tipocpte = $this->idtipo;
            $this->cpte->id_titular  = $request->id_titular;
            $this->cpte->id_ajuste   = (!empty($request->id_ajuste)) ? $request->id_ajuste : null;
            $this->cpte->id_caja     = DB::table('cajas')->where('id_sucursal', Auth::user()->id_sucursal)->max('id');
            $this->cpte->serie       = $this->_getSerie();

            $items  = json_decode($request->items);
            $mps    = json_decode($request->mps);
            $this->cpte->saldototal = 0;
            $this->cpte->saldocpte  = 0;
            $this->cpte->total = $this->_getTotal($items,$mps,$request->importe);

            $this->cpte->observaciones = $request->observaciones;

            $this->cpte->id_estado = null;

            if (!empty($request->id_cpterel)){
                $this->cpte->id_cpterel= $tipo=='fcr'?implode(",",$request->id_cpterel):$request->id_cpterel;
            }

            $this->_save();

            /*---------------------------------------------------------*/
            /*Detalles                                                 */
            /*---------------------------------------------------------*/

            if ($this->tipo=='fcr'){ // Si es Factura de Remitos
                $this->_vincularRemitosConFactura($request->id_cpterel);
            }

            if (!empty($request->id)){  // Estoy Modificando el Comprobante
                $this->_eliminarDetalleComprobante();
            }

            if (!empty($items)){ // Grabo el Detalle de items del comprobante
                $this->_grabarComprobantesItems($items, intval($request->origen));
            }

            if (!empty($mps)){ // Grabo el Detalle de mps del comprobante
                $this->_grabarComprobantesMps($mps);
            }else{
                if (isset($request->id_mediopago)) {
                    $mp = array('id_mediopago' => $request->id_mediopago,
                        'numero_transaccion' => $request->numero_transaccion,
                        'cuotas' => $request->cuotas,
                        'id_banco' => $request->id_banco,
                        'fecha_cobro' => $request->fecha_cobro,
                        'fecha_vto' => $request->fecha_vto,
                        'importe' => $request->importe);
                    $this->_grabarComprobantesMp($mp);
                }
            }

            $descints  = json_decode($request->descint);
            if (!empty($descints)){
                ComprobanteDI::where('id_cpte', $request->id)->delete();
                $this->_grabarComprobantesDescInt($descints);
            }

            if (!empty($request->id_referente)){
                $this->_grabarCpteRelacionado($request->id_cpterel,(int) $request->id_referente);
            }

/*            if ($this->cpte->id_estado==2){*/
                $this->_calcularSaldos();
                $this->_save();
/*          }*/
        DB::commit();

        //if (($idtipo==7) || ((count($mps)==1) && ($mps[0]->id_mediopago==2))){
        if (($this->idtipo==3) || ($this->idtipo==7) || ($this->idtipo==13)){
            $this->_RecalcularSaldos_Estados($this->cpte->id_titular);
        }

        if ($this->tipo!='rem'){
            return $this->_redirectToList();
        }else{
            if (env('MP_POCOSPRODUCTOS')){
                return $this->_ajaxResponse();
            }else{
                return  $this->_redirectToNewCpte();
            }
        }
    }

    private function _save(){
        $this->cpte->save();
    }

    private function _getSerie(){
        if($this->cpte->idtipo!=6){
            $serie='X';
        }else{
            $comercio=Titular::find(1);
            $this->titularDescripcion = $comercio->descripcion;
            if ($comercio->id_tipoiva!=1){
                $serie='C';
            }else{
                $titular = Titular::find($this->cpte->id_titular);
                $this->titularDescripcion = $titular->descripcion;
                if ($titular->id_tipoiva==1){
                    $serie='A';
                }else{
                    $serie='B';
                }
            }
        }
        return $serie;
    }

    /**
     * Calcula y devuelve el Total del Comprobante
     * @param null $items
     * @param null $mps
     * @param null $importe
     * @return int|null
     */
    private function _getTotal($items=null,$mps=null,$importe=null){
        $total = 0;
        if (!empty($mps)){
            foreach($mps as $mp ){
                $total = $total + $mp->importe;
            }
        }else{
            if (!empty($items)){
                foreach($items as $item){
                    $total = $total + $item->total;
                }
            }else{
                $total=$importe;
            }
        }
        return $total;
    }

    /**
     * Vincula remitos con factura.
     * @param array $idsrem
     */
    private function _vincularRemitosConFactura(array $idsrem){
        if (!empty($idsrem)) {
            foreach($idsrem as $idremito ){
                $rem = Comprobante::find($idremito);
                $rem->id_cpterel=$this->cpte->id;
                $rem->save();
            }
        }
    }

    /**
     * Elimina los Movimientos de Stock, los items, los mps y la ultima comision del cpte
     */
    private function _eliminarDetalleComprobante(){
        Stock::whereIn('id_movstk', MovimientoStock::whereIn('id_cpteitem', ComprobanteItems::where('id_cpte', $this->cpte->id)->select('id'))->select('id'))->delete();
        MovimientoStock::whereIn('id_cpteitem',ComprobanteItems::where('id_cpte', $this->cpte->id)->select('id'))->delete();
        ComprobanteItems::where('id_cpte', $this->cpte->id)->delete();
        ComprobanteMp::where('id_cpte', $this->cpte->id)->delete();
        $oldcomision = Comprobante::where('id_cpterel', $this->cpte->id)->where('id_tipocpte', 14)->get();
        if (!empty($oldcomision[0]->id)){
            ComprobanteMp::where('id_cpte', $oldcomision[0]->id)->delete();
            $oldcomision[0]->delete();
        }
    }

    private function _grabarComprobantesItems(array $items, int $idposicion=null){
        foreach($items as $item ){
            $cpteitem = new ComprobanteItems();
            $cpteitem->created_us = auth()->id();
            $cpteitem->updated_us = auth()->id();
            $cpteitem->id_cpte = $this->cpte->id;
            $cpteitem->id_item = $item->id_item;
            $cpteitem->preciouni = $item->precio;
            $cpteitem->cantidad = $item->cantidad;
            $cpteitem->importe = $item->total;
            $cpteitem->save();

            if ((!empty($this->tmovstk)) and (!empty($idposicion)) ){
                if (($this->tmovstk==1) || ($this->tmovstk==6) ){ //1-Recepcion //6-Devolucion
                    $this->_grabarMovStkSinOrigen($cpteitem,$idposicion);
                }
                if (($this->tmovstk==7) and (!empty($item->origen)) ){ //Venta
                    $this->_grabarMovStkSinDestino($cpteitem,$idposicion);
                }
            }
        }
    }

    private function _grabarMovStkSinOrigen(\App\Models\ComprobanteItems $cpteitem, int $idposicion){
        $movstk = new MovimientoStock();
        $movstk->id_motivo      = $this->tmovstk;
        $movstk->id_cpteitem    = $cpteitem->id;
        $movstk->id_item        = $cpteitem->id_item;
        $movstk->id_posicion_o  = null;
        $movstk->id_posicion_d  = $idposicion;
        $movstk->cantidad       = $cpteitem->cantidad;
        $movstk->save();
        $this->_grabarStock($movstk,$idposicion);
    }

    private function _grabarMovStkSinDestino(\App\Models\ComprobanteItems $cpteitem, int $idposicion){
        $movstk = new MovimientoStock();
        $movstk->id_motivo      = $this->tmovstk;
        $movstk->id_cpteitem    = $cpteitem->id;
        $movstk->id_item        = $cpteitem->id_item;
        $movstk->id_posicion_d  = null;
        $movstk->id_posicion_o  = $idposicion;
        $movstk->cantidad       = $cpteitem->cantidad *-1;
        $movstk->save();
        $this->_grabarStock($movstk,$idposicion);
    }

    private function _grabarStock(\App\Models\MovimientoStock $movstk, int $idposicion){
        $stk = new Stock();
        $stk->id_item       = $movstk->id_item;
        $stk->id_posicion  = $idposicion;
        $stk->cantidad      = $movstk->cantidad;
        $stk->id_movstk     = $movstk->id;
        $stk->save();
    }

    private function _grabarComprobantesMps($mps){
        ComprobanteMp::where('id_cpte', $this->cpte->id)->delete();
        foreach($mps as $mp ){
            $cptemp = new ComprobanteMp();
            $cptemp->created_us = auth()->id();
            $cptemp->updated_us = auth()->id();
            $cptemp->id_cpte = $this->cpte->id;
            $cptemp->id_mediopago = $mp->id_mediopago;
            if (!empty($mp->numero_transaccion)) {$cptemp->numero_transaccion = $mp->numero_transaccion;}
            if (!empty($mp->cuotas)) {$cptemp->cuotas = $mp->cuotas;}
            if (!empty($mp->id_banco)) {$cptemp->id_banco = $mp->id_banco;}
            if (!empty($mp->fecha_cobro)) {$cptemp->fecha_cobro = $mp->fecha_cobro;}
            if (!empty($mp->fecha_vto)) {
                $cptemp->fecha_vto = $mp->fecha_vto;
            }else{
                $cptemp->fecha_vto = $this->cpte->created_at;
            }
            $cptemp->importe = $mp->importe;
            $cptemp->save();
        }

        //Actualizo el Estado
        if (count($mps)==1){
            if ($this->idtipo==13){ // Nota de Credito
                $this->cpte->id_estado = 3;
            }else{ // Si es cuenta corriente es pendiente, sino esta pago.
                $this->cpte->id_estado = ($mps[0]->id_mediopago==2) ? 2 : 1;
            }
        }else{ // Mas de un medio de Pago, Significa que está Pago.
            $this->cpte->id_estado = 1;
        }
        $this->_save();
    }

    private function _grabarComprobantesMp($mp){
        ComprobanteMp::where('id_cpte', $this->cpte->id)->delete();
        $cptemp = new ComprobanteMp();
        $cptemp->created_us = auth()->id();
        $cptemp->updated_us = auth()->id();
        $cptemp->id_cpte = $this->cpte->id;
        $cptemp->id_mediopago = $mp['id_mediopago'];
        if (!empty($mp['numero_transaccion'])) {$cptemp->numero_transaccion = $mp['numero_transaccion'];}
        if (!empty($mp['cuotas'])) {$cptemp->cuotas = $mp['cuotas'];}
        if (!empty($mp['id_banco'])) {$cptemp->id_banco = $mp['id_banco'];}
        if (!empty($mp['fecha_cobro'])) {$cptemp->fecha_cobro = $mp['fecha_cobro'];}
        if (!empty($mp['fecha_vto'])) {$cptemp->fecha_vto = $mp['fecha_vto'];}
        $cptemp->importe = $mp['importe'];
        $cptemp->save();

        //Actualizo el Estado
        $this->cpte->id_estado = 3;
        $this->_save();
    }

    private function _grabarComprobantesDescInt($descints){
        foreach($descints as $descint ){
            $cptedi = new ComprobanteDI();
            $cptedi->created_us = auth()->id();
            $cptedi->updated_us = auth()->id();
            $cptedi->id_cpte = $this->cpte->id;
            $cptedi->id_descint = $descint->id_descint;
            $cptedi->importe = $descint->importe;
            $cptedi->save();
        }
    }

    private function _redirectToList(){
        return redirect('/cptes/'.$this->tipo)->with([
            'status'=>'Comprobante:'.$this->cpte->numero.' del Titular: '.$this->titularDescripcion.' por: $'.$this->cpte->total.' '.$this->actionLabel.' Exitosamente!',
            'lastid'=>$this->cpte->id
        ]);
    }

    private function _ajaxResponse(){
        $data['msg']='Comprobante:'.$this->cpte->numero.' del Titular: '.$this->titularDescripcion.' por: $'.$this->cpte->total.' '.$this->actionLabel.' Exitosamente!';
        $data['lastid']=$this->cpte->id;
        return $data;
    }

    private function _redirectToNewCpte(){
        return redirect('/cptes/create/'.$this->tipo)->with([
            'status'=>'Comprobante:'.$this->cpte->numero.' del Titular: '.$this->titularDescripcion.' por: $'.$this->cpte->total.' '.$this->actionLabel.' Exitosamente!',
            'lastid'=>$this->cpte->id
        ]);
    }

    private function _grabarCpteRelacionado(int $idcpterel=null, int $idreferente)
    {
        $cpterelacionado = New Comprobante();//Comision o Nota de Debito
        if (($this->cpte->id_tipocpte == 5) || ($this->cpte->id_tipocpte == 6)) { //Comision sobre remito o factura
            $cpterelacionado->id_tipocpte = 14;
            $cpterelacionado->id_cpterel = $this->cpte->id;
            $cpterelacionado->id_estado = $this->cpte->id_estado == 2 ? $this->cpte->id_estado : 3;
        } else if ($this->cpte->id_tipocpte == 13) { //Nota de Debito sobre Nota de Credito
            $cpterelacionado->id_tipocpte = 3;
            $cpterel = Comprobante::find($idcpterel);
            $comision = Comprobante::where('id_cpterel', $cpterel->id)->where('id_tipocpte', 14)->get();
            $cpterelacionado->id_cpterel = $comision[0]->id;
            $cpterelacionado->id_estado = $cpterel->id_estado;
        }
        $cpterelacionado->id_titular = $idreferente;
        $cpterelacionado->id_caja = $this->cpte->id_caja;
        $cpterelacionado->serie = 'X';
        $cpterelacionado->total = $this->cpte->total * 8 / 100;
        $cpterelacionado->created_us = auth()->id();
        $cpterelacionado->updated_us = auth()->id();
        $cpterelacionado->save();

        $cpterelacionadomp = new ComprobanteMp();
        $cpterelacionadomp->created_us = auth()->id();
        $cpterelacionadomp->updated_us = auth()->id();
        $cpterelacionadomp->id_cpte = $cpterelacionado->id;
        $cpterelacionadomp->id_mediopago = 2;
        $cpterelacionadomp->importe = $cpterelacionado->total;
        $cpterelacionadomp->save();
    }

    private function _calcularSaldos(){
        $stotal =  DB::select('select sum(cc.saldo) as saldototal  from ctacte_view cc where cc.id_titular = ? and cc.id <= ?',[$this->cpte->id_titular,$this->cpte->id]);
        $this->cpte->saldototal = $stotal[0]->saldototal;
        $pagos = DB::select('select sum(total) as total from comprobantes where id_titular = ? and id_tipocpte in (7,13,14) and deleted_at is null and id_estado=3',[$this->cpte->id_titular]);

        if ((!empty($pagos)) and ($pagos[0]->total > 0)){
            if ($pagos[0]->total >= $this->cpte->saldototal){
                $this->cpte->saldocpte = 0;
            }else{
                $this->cpte->saldocpte =  (($this->cpte->saldototal - $this->cpte->total) > $pagos[0]->total) ? $this->cpte->total : $this->cpte->saldototal-$pagos[0]->total;
            }
        }else{
            $this->cpte->saldocpte = $this->cpte->total;
        }
    }

    public function detalle($id)
    {
        $cpte = Comprobante::find($id);
        if (($cpte->id_tipocpte==5) ||($cpte->id_tipocpte==6) || ($cpte->id_tipocpte==13) ){
            $detalle = DB::select('select ci.*,i.descripcion, m.descripcion as marca,
                                      ci.preciouni as precio, ci.importe as total,
                                      concat(COALESCE(i.descripcion,""),\' - \',COALESCE(i.contNeto,""),\' \',COALESCE(um.descripcion,""),\' (\',COALESCE(m.descripcion,""),\')\') as producto
                            from comprobantes_items ci
                            join items i on ci.id_item = i.id
                            left join conf_marcas m on m.id = i.id_marca
                            left join conf_unidades_medida um on um.id=i.id_unidadcontneto
                            where id_cpte='.$id);
        }elseif($cpte->id_tipocpte==7){
            $detalle = DB::table('comprobantes_mp as cmp')->where('cmp.id_cpte',$id)
                ->join('sys_medios_pago as mp', 'mp.id', '=', 'cmp.id_mediopago')
                ->leftJoin('conf_bancos as b', 'b.id', '=', 'cmp.id_banco')
                ->select('cmp.id','cmp.id_mediopago', 'mp.descripcion', 'cmp.cuotas','cmp.numero_transaccion','cmp.id_banco','b.descripcion as banco','cmp.fecha_cobro','cmp.fecha_vto','cmp.importe')
                ->get();
        }else{
            $cpte->usuario=User::find($cpte->created_us)->name;
            $detalle=$cpte;
        }

        return $detalle;
    }

    public function titulares($id)
    {
        $cpte = Comprobante::find($id);
        $detalle['id_titular'] = $cpte->id_titular;
        $cpterel = Comprobante::where('id_cpterel','=',$id)->where('id_tipocpte','=',14)->first();
        if(!empty($cpterel)){
            $detalle['id_referente'] = $cpterel->id_titular;
        }
        return $detalle;
    }

    public function delete($id)
    {
        DB::beginTransaction();
            $cpte = Comprobante::find($id);
            $cpte->updated_us = auth()->id();
            Stock::whereIn('id_movstk', MovimientoStock::whereIn('id_cpteitem', ComprobanteItems::where('id_cpte', $id)->select('id'))->select('id'))->delete();
            MovimientoStock::whereIn('id_cpteitem',ComprobanteItems::where('id_cpte', $id)->select('id'))->delete();
            ComprobanteItems::where('id_cpte', $id)->delete();
            ComprobanteMp::where('id_cpte', $id)->delete();
            $titular=$cpte->id_titular;
            $cpte->delete();
            if($titular!=2){
                $this->_RecalcularSaldos_Estados($titular);
            }
            $tipocpte = TipoComprobante::find($cpte->id_tipocpte);
            $notificacion = New Notificacion();
            $notificacion->icon = 'fa fa-trash text-red';
            $notificacion->descripcion = 'Se Eliminó el '.$tipocpte->descripcion.' Nro: '.$cpte->numero;
            $notificacion->created_us = auth()->id();
            $notificacion->save();

            foreach(DB::table('users')->whereNull('deleted_at')->get() as $user){
                $notiEstado = New NotificacionEstado();
                $notiEstado->id_notificacion = $notificacion->id;
                $notiEstado->id_user = $user->id;
                $notiEstado->id_estado = 14;
                $notiEstado->save();
            }
        DB::commit();
        $data['msg']="Comprobante Eliminado!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach ($datos as $id){
            DB::beginTransaction();
                $cpte = Comprobante::find($id);
                $cpte->updated_us = auth()->id();
                Stock::whereIn('id_movstk', MovimientoStock::whereIn('id_cpteitem', ComprobanteItems::where('id_cpte', $id)->select('id'))->select('id'))->delete();
                MovimientoStock::whereIn('id_cpteitem',ComprobanteItems::where('id_cpte', $id)->select('id'))->delete();
                ComprobanteItems::where('id_cpte', $id)->delete();
                ComprobanteMp::where('id_cpte', $id)->delete();
                $titular=$cpte->id_titular;
                $cpte->delete();
                if($titular!=2){
                    $this->_RecalcularSaldos_Estados($titular);
                }
                $tipocpte = TipoComprobante::find($cpte->id_tipocpte);
                $notificacion = New Notificacion();
                $notificacion->icon = 'fa fa-trash text-red';
                $notificacion->descripcion = 'Se Eliminó el '.$tipocpte->descripcion.' Nro:'.$cpte->numero;
                $notificacion->created_us = auth()->id();
                $notificacion->save();

                foreach(DB::table('users')->whereNull('deleted_at')->get() as $user){
                    $notiEstado = New NotificacionEstado();
                    $notiEstado->id_notificacion = $notificacion->id;
                    $notiEstado->id_user = $user->id;
                    $notiEstado->id_estado = 14;
                    $notiEstado->save();
                }
            DB::commit();
        }
        return;
    }

    public function show($tipo,$id)
    {
        switch ($tipo) {
            case 'oc':
                $dir = "ordenesdecompra";
                $itemnav = "oc";
                $titulo = 'Ordenes de Compra';
                $tituloform = 'Orden de Compra';
                break;
            case 'rec':
                $dir = "recepciones";
                $itemnav = "rec";
                $titulo = 'Recepciones';
                $tituloform = 'Recepciones';
                break;
            case 'nd':
                $dir = "notadedebito";
                $itemnav = "nd";
                $titulo = 'Notas de Debito';
                $tituloform = 'Nota de Debito';
                break;
            case 'rem':
                $dir = "remitos";
                $itemnav = "rem";
                $titulo = 'Remitos';
                $tituloform = 'Remito';
                break;
            case 'fc':
                $dir = "facturas";
                $itemnav = "fc";
                $titulo = 'Facturas';
                $tituloform = 'Factura';
                break;
            case 'rbo':
                $dir = "recibos";
                $itemnav = "rbo";
                $titulo = 'Recibos';
                $tituloform = 'Recibos';
                break;
            case 'nc':
                $dir = "notadecredito";
                $itemnav = "cn";
                $titulo = 'Nota de Credito';
                $tituloform = 'Nota de Credito';
                break;
            case 'pre':
                $dir = "presupuestos";
                $itemnav = "pre";
                $titulo = 'Presupuestos';
                $tituloform = 'Presupuesto';
                break;
            case 'pgo':
                $dir = "pagos";
                $titulo = 'Pago a Provedores';
                $tituloform = 'Pago a Provedores';
                break;
        }
        $cpte = Comprobante::find($id);
        $comision = Comprobante::where('id_cpterel', $id)->where('id_tipocpte', 14)->get();
        if (!empty($comision[0])){
            $cpte->id_referente = $comision[0]->id_titular;
        }
        if (!empty($cpte->id_cpterel)){$vsrem=true;}else{$vsrem=false;}
        $prods = DB::table('comprobantes_items as ci')->where('ci.id_cpte',$id)
                      ->join('items as i', 'i.id', '=', 'ci.id_item')
                      ->leftJoin('conf_marcas as m', 'm.id', '=', 'i.id_marca')
                      ->leftJoin('conf_unidades_medida as um', 'um.id', '=', 'i.id_unidadcontneto')
                      ->leftJoin('items_proveedores as ip', 'ip.id_item', '=', 'i.id')
                      ->select('ci.id','ci.id_item', DB::raw('concat(i.descripcion," - ", i.contNeto, " ", COALESCE(um.descripcion,""), " (", m.descripcion, ")") as producto'), 'ci.cantidad', 'ci.preciouni as precio','ci.importe as total','ip.codproducto as codprov')
                      ->get();
        $descints = DB::table('comprobantes_descint as cdi')->where('cdi.id_cpte',$id)
            ->join('conf_descint as di', 'di.id', '=', 'cdi.id_descint')
            ->select('cdi.id','di.id_mediopago','cdi.id_descint', 'di.descripcion', 'cdi.importe')
            ->get();
        $mps = DB::table('comprobantes_mp as cmp')->where('cmp.id_cpte',$id)
            ->join('sys_medios_pago as mp', 'mp.id', '=', 'cmp.id_mediopago')
            ->leftJoin('conf_bancos as b', 'b.id', '=', 'cmp.id_banco')
            ->select('cmp.id','cmp.id_mediopago', 'mp.descripcion', 'cmp.cuotas','cmp.numero_transaccion','cmp.id_banco','b.descripcion as banco','cmp.fecha_cobro','cmp.fecha_vto','cmp.importe')
            ->get();
        //dd($mps);
        return view('comprobantes.'.$dir.'.form',[
            'titulo' => $titulo,
            'accion' => 'Visualizar',
            'tituloform' => $tituloform,
            'titulodetart' => 'Detalle de Productos',
            'itemnav' => $itemnav,
            'cpte' => $cpte,
            'prods' => $prods,
            'descints' => $descints,
            'mps' => $mps,
            'edit'=> 0,
            'vsrem'=> $vsrem
        ]);
    }

    public function edit($tipo,$id)
    {
        switch ($tipo) {
            case 'oc':
                $dir = "ordenesdecompra";
                $titulo = 'Ordenes de Compra';
                $tituloform = 'Orden de Compra';
                break;
            case 'rec':
                $dir = "recpciones";
                $titulo = 'Recepciones';
                $tituloform = 'Recepciones';
                break;
            case 'rem':
                $dir = "remitos";
                $titulo = 'Remitos';
                $tituloform = 'Remito';
                break;
            case 'fc':
                $dir = "facturas";
                $titulo = 'Facturas';
                $tituloform = 'Factura';
                break;
            case 'rbo':
                $dir = "recibos";
                $titulo = 'Recibos';
                $tituloform = 'Recibo';
                break;
            case 'nc':
                $dir = "notadecredito";
                $titulo = 'Nota de Credito';
                $tituloform = 'Nota de Credito';
                break;
            case 'pre':
                $dir = "presupuestos";
                $titulo = 'Presupuestos';
                $tituloform = 'Presupuesto';
                break;
            case 'pgo':
                $dir = "pagos";
                $titulo = 'Pago a Provedores';
                $tituloform = 'Pago a Provedores';
                break;
        }
        $cpte = Comprobante::find($id);
        $comision = Comprobante::where('id_cpterel', $id)->where('id_tipocpte', 14)->get();
        if (!empty($comision[0])){
            $cpte->id_referente = $comision[0]->id_titular;
        }
        if (!empty($cpte->id_cpterel)){$vsrem=true;}else{$vsrem=false;}
        $prods = DB::table('comprobantes_items as ci')->where('ci.id_cpte',$id)
            ->join('items as i', 'i.id', '=', 'ci.id_item')
            ->leftJoin('conf_marcas as m', 'm.id', '=', 'i.id_marca')
            ->leftJoin('conf_unidades_medida as um', 'um.id', '=', 'i.id_unidadcontneto')
            ->select('ci.id','ci.id_item', DB::raw('concat(i.descripcion," - ", i.contNeto, " ", COALESCE(um.descripcion,""), " (", m.descripcion, ")") as producto'), 'ci.cantidad', 'ci.preciouni as precio','ci.importe as total')
            ->get();
        $descints = DB::table('comprobantes_descint as cdi')->where('cdi.id_cpte',$id)
            ->join('conf_descint as di', 'di.id', '=', 'cdi.id_descint')
            ->select('cdi.id','di.id_mediopago','cdi.id_descint', 'di.descripcion', 'cdi.importe')
            ->get();
        $mps = DB::table('comprobantes_mp as cmp')->where('cmp.id_cpte',$id)
            ->join('sys_medios_pago as mp', 'mp.id', '=', 'cmp.id_mediopago')
            ->leftJoin('conf_bancos as b', 'b.id', '=', 'cmp.id_banco')
            ->select('cmp.id','cmp.id_mediopago', 'mp.descripcion','cmp.cuotas','cmp.numero_transaccion','cmp.id_banco','b.descripcion as banco','cmp.fecha_cobro','cmp.fecha_vto','cmp.importe')
            ->get();
//        dd($descints);
//        dd($mps);
        return view('comprobantes.'.$dir.'.form',[
            'titulo' => $titulo,
            'accion' => 'Visualizar',
            'tituloform' => $tituloform,
            'titulodetart' => 'Detalle de Productos',
            'itemnav' => 'oc',
            'cpte' => $cpte,
            'prods' => $prods,
            'descints' => $descints,
            'mps' => $mps,
            'edit'=> 1,
            'vsrem'=> $vsrem
        ]);
    }

    public function getdescint(Request $request){
        //dd($request->request);
        if(!empty($request->idcliente)){
            $cliente = Cliente::find($request->idcliente);
            if (!empty($request->cuotas)){
                $descint = DB::table('conf_descint')
                    ->where('activo', 1)
                    ->where('id_tipocliente', $cliente->id_tipocliente)
                    ->where('cuotas', $request->cuotas)
                    ->where('id_mediopago', $request->idmp)
                    ->get();
            }else{
                $descint = DB::table('conf_descint')
                    ->where('activo', 1)
                    ->where('id_tipocliente', $cliente->id_tipocliente)
                    ->whereNull('cuotas')
                    ->where('id_mediopago', $request->idmp)
                    ->get();
            }
            if (!empty($descint[0]->id_mediopago)){
                return json_encode($descint);
            }
        }
        return null;
    }

    public function printview($tipo,$id)
    {
        switch ($tipo) {
            case 'oc':
                $dir = "ordenesdecompra";
                break;
            case 'rec':
                $dir = "recpciones";
                break;
            case 'nd':
                $dir = "notadedebito";
                break;
            case 'rem':
                $dir = "remitos";
                break;
            case 'fc':
                $dir = "facturas";
                break;
            case 'rbo':
                $dir = "recibos";
                break;
            case 'pre':
                $dir = "presupuestos";
                break;
            case 'nc':
                $dir = "notadecredito";
                break;
            case 'pre':
                $dir = "presupuestos";
                break;
            case 'pgo':
                $dir = "pagos";
                break;
        }
        $cpte = Comprobante::find($id);
        $prods = DB::table('comprobantes_items as ci')->where('ci.id_cpte',$id)
            ->join('items as i', 'i.id', '=', 'ci.id_item')
            ->leftJoin('conf_unidades_medida as um', 'um.id', '=', 'i.id_unidadcontneto')
            ->select('ci.id','ci.id_item', DB::raw('CONCAT (i.descripcion,\' \',COALESCE(i.contNeto,""),\' \',COALESCE(um.descripcion,""),\' \') as producto'), 'ci.cantidad', 'ci.preciouni as precio','ci.importe as total')
            ->get();
        $titular = Titular::find($cpte->id_titular);
        $ref = DB::table('titulares as t')
                ->leftJoin('comprobantes as c', 'c.id_titular', '=', 't.id')
                ->where('c.id_cpterel',$id)
                ->where('c.id_tipocpte',14)
                ->get();
        if(!empty($ref[0])){$referente=$ref[0];}else{$referente='';}
        $totaldi = DB::table('comprobantes_descint')->where('id_cpte',$id)->sum('importe');
        $caja= Caja::find($cpte->id_caja);
        $sucursal = Sucursal::find($caja->id_sucursal);
        $saldo=DB::table('comprobantes as cc')
            ->where('cc.id_titular', $cpte->id_titular)
            ->whereNotNull('cc.deleted_at')
            ->sum('total');

        if (env('MP_ONEPAGEPRINT')){
            $printpage = '.print';
        }else{
            $printpage = '.print2in1';
        }

        return view('comprobantes.'.$dir.$printpage,[
            'cpte' => $cpte,
            'titular' => $titular,
            'ref' => $referente,
            'saldo' => $saldo,
            'prods' => $prods,
            'totaldi' => $totaldi,
            'sucursal' => $sucursal
//            'mps' => $mps,
//            'edit'=> 1
        ]);
    }

    public function printlayout()
    {
        $url = route('viewlayout');
        $dompdf = new DOMPDF();
        $dompdf->set_option('enable_remote', TRUE);
        $dompdf->loadHtmlFile($url);

        // Render the HTML as PDF
        $dompdf->render();
        //dd($dompdf);

        // Output the generated PDF to Browser
        $filename='layout2in1.pdf';
        $dompdf->stream($filename);
    }

    public function getprint($tipo,$id)
    {
        switch ($tipo) {
            case 'oc':
                $name = "oc";
                break;
            case 'rec':
                $name = "rec";
                break;
            case 'nd':
                $name = "notadedebito";
                break;
            case 'rem':
                $name = "remito";
                break;
            case 'fc':
                $name = "factura";
                break;
            case 'rbo':
                $name = "recibo";
                break;
            case 'pre':
                $name = "presupuesto";
                break;
            case 'nc':
                $name = "notadecredito";
                break;
            case 'pre':
                $name = "presupuestos";
                break;
            case 'pgo':
                $name = "pagos";
                break;
        }

        $url = route('printcpte',['tipo'=>$tipo,'id'=>$id]);
        //dd($url);

        $dompdf = new DOMPDF();
        $dompdf->set_option('enable_remote', TRUE);
        $dompdf->loadHtmlFile($url);

        // (Optional) Setup the paper size and orientation
//        if (true){
//            $dompdf->setPaper('A4', 'landscape');
//        }

        // Render the HTML as PDF
        $dompdf->render();
        //dd($dompdf);

        // Output the generated PDF to Browser
        $filename=$name.$id.'.pdf';
        $dompdf->stream($filename);
    }

    private function _RecalcularSaldos_Estados($id){
        Artisan::call('calcular:saldos', ['titular' => $id]);
    }


    public function ActualizarEstados($idtitular){
        $pagos = DB::select('select sum(total) as total from comprobantes where id_titular = ? and id_tipocpte in (7,13,14) and deleted_at is null and id_estado=3',[$idtitular]);
        if (empty($pagos)){
            $totalpagos = 0;
        }else{
            $totalpagos = $pagos[0]->total;
        }
        if ($totalpagos > 1 ){
            $maxcpte = DB::select('SELECT MAX(c.id) as id
                                     FROM comprobantes c
                                    WHERE c.id_titular = ?
                                      AND c.id_tipocpte IN (5,6,3)
                                      AND c.deleted_at is null
                                      AND (SELECT SUM(total)
                                             FROM comprobantes c2
                                             JOIN comprobantes_mp cmp2 ON cmp2.id_cpte = c2.id AND cmp2.id_mediopago = 2
                                            WHERE c2.id_titular=c.id_titular
                                              AND c2.id_tipocpte IN (5,6,3)
                                              AND c2.deleted_at is null
                                              AND c2.id <=c.id) <= ?',[$idtitular,floatval($totalpagos)]);
            if (!empty($maxcpte[0]->id)){
                DB::update('UPDATE comprobantes c
                              JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                               SET c.id_estado=1
                             WHERE c.id_titular = ?
                               AND c.id_tipocpte in (5,6,3)
                               AND c.id < ?',[$idtitular,$maxcpte[0]->id]);

                DB::update('UPDATE comprobantes c
                              JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                               SET c.id_estado=2
                             WHERE c.id_titular = ?
                               AND c.id_tipocpte in (5,6,3)
                               AND c.id > ?',[$idtitular,$maxcpte[0]->id]);

                $saldomax = DB::select ('SELECT SUM(total) as saldo
                                             FROM comprobantes c
                                             JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                                            WHERE c.id_titular= ?
                                              AND c.id_tipocpte IN (5,6,3)
                                              AND c.deleted_at is null
                                              AND c.id <=?',[$idtitular,$maxcpte[0]->id]);

                if ($saldomax[0]->saldo > floatval($totalpagos)){
                    DB::update('UPDATE comprobantes c
                                   SET id_estado=2
                                 WHERE id = '.$maxcpte[0]->id);
                }else{
                    DB::update('UPDATE comprobantes c
                                   SET id_estado=1
                                 WHERE id = '.$maxcpte[0]->id);
                }
            }else{
                DB::update('UPDATE comprobantes c
                                  JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                                   SET id_estado=2
                                 WHERE id_tipocpte IN (5,6,3)
                                   AND c.id_titular = '.$idtitular);
            }
        }else{
            DB::update('UPDATE comprobantes c
                              JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                               SET id_estado=2
                             WHERE id_tipocpte IN (5,6,3)
                               AND c.id_titular = '.$idtitular);
        }
        DB::update('UPDATE comprobantes as c1
                          JOIN comprobantes as c2 ON c2.id=c1.id_cpterel
                           SET c1.id_estado=3
                         WHERE c2.id_titular = ?
                           AND c2.id_estado=1
                           AND c1.id_tipocpte = 14
                           AND c1.deleted_at IS NULL',[$idtitular]);

        DB::update('UPDATE comprobantes as c1
                          JOIN comprobantes as c2 ON c2.id=c1.id_cpterel
                           SET c1.id_estado=2
                         WHERE c2.id_titular = ?
                           AND c2.id_estado=2
                           AND c1.id_tipocpte = 14
                           AND c1.deleted_at IS NULL',[$idtitular]);
        return;
    }


    public function gethistorico(Request $request)
    {
        if ($request->idtipo!=14){
            $items = DB::table('comprobantes as c')
                ->where('c.id_tipocpte', $request->idtipo)
                ->whereNull('c.deleted_at');

            if (!empty($request->idcliente)){
                $items = $items->where('c.id_titular', $request->idcliente);
            }

            if (!empty($request->desde)){
                $items = $items->whereRaw('date(c.created_at) BETWEEN "'.$request->desde.'" AND "'.$request->hasta.'"');
            }

            if (!empty($request->iditem)){
                $item = $request->iditem;
                $items = $items->join('comprobantes_items as ci', function ($join) use ($item){
                                $join->on('ci.id_cpte', '=', 'c.id')
                                    ->where('ci.id_item', '=', $item);
                        });
            }

            if (!empty($request->idmediopago)){
                $idmp = $request->idmediopago;
                $items = $items->join('comprobantes_mp as mp', function ($join) use ($idmp){
                    $join->on('mp.id_cpte', '=', 'c.id')
                        ->where('mp.id_mediopago', '=', $idmp);
                });
            }

            $items = $items->leftJoin('titulares as t', 't.id', '=', 'c.id_titular')
                ->leftJoin('users as us', 'us.id', '=', 'c.created_us')
                ->select('c.*',
                    DB::raw('date_format(c.created_at, "%d/%m/%Y %H:%i:%s") as creado'),
                    DB::raw('(TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) as pasaron'),
                    't.descripcion as titular',
                    'us.name as usuario',
                    DB::raw('date_format(c.updated_at, "%d/%m/%Y %H:%i:%s") as  ultmodif'))
                ->get();
        }
        return $items;
    }
}
