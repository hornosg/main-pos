<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Rubro;
use DB;
use Illuminate\Http\Request;
use Excel;

class RubrosController extends Controller
{
    //
    public function index()
    {
        $instock = DB::select('SELECT 	ru.descripcion as label,
                                        sum(cantidad) as value
                                FROM inv_stock s
                                join items p on p.id=s.id_item
                                left join conf_rubros ru on ru.id=p.id_rubro
                                where id_sector is not null
                                and p.id_tipoitem=5
                                group by 1
                                order by 2 desc
                                limit 10');
        $instockarray = [];
        foreach( $instock as $item){
            $item->value = (int) $item->value;
            array_push($instockarray,(collect($item)->toArray()));
        }

        $items = Rubro::all();
        return view('master.index',
            ['titulo' => 'Rubros',
             'urlkey' => 'rubros',
             'items' => $items,
             'instock' => $instockarray,
             'itemnav' => 'rubros']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
        ]);


        $rubro = $request->all();
        $rubro['descripcion'] = strtoupper($rubro['descripcion']);

        if (isset($rubro['activo'])){
            $rubro['activo']=1;
        }else{
            $rubro['activo']=0;
        }
        $data=Rubro::create($rubro);
        if ($rubro['activo']==1){
            $data['activo']='SI';
        }else{
            $data['activo']='NO';
        }
        $data['msg']="Rubro Creado Exitosamente!";
        return $data;
    }

    public function edit($id)
    {
        $item = Rubro::find($id);
        return view('master.edit', ['titulo' => 'Rubros','urlkey' => 'rubros' ,'item' => $item]);

    }

    public function show($id)
    {
        $item = Rubro::find($id);
        return view('master.show', ['titulo' => 'Rubros','urlkey' => 'rubros' ,'item' => $item]);
    }


    public function update($id,Request $request)
    {
        $rubro = Rubro::find($id);
        $rubro->descripcion = strtoupper($request->descripcion);

        if (isset($request->activo)){
            $rubro->activo=1;
        }else{
            $rubro->activo=0;
        }

        $rubro->save();
        $data['msg']="Marca Modificada.";
        return $data;

    }

    public function delete($id)
    {
        $rubro = Rubro::find($id);
        $rubro->delete();
        $data['msg']="Rubro Eliminado!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $rubro = Rubro::find($item);
            $rubro->delete();
        }
        return;
    }

    public function import()
    {
        return view('master.import', ['titulo' => 'Rubros','urlkey' => 'rubros','itemnav' => 'rubros']);
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $items[] = ['descripcion' => $value->descripcion, 'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_rubros')->insert($items);
                    return redirect('/rubros')->with('status', 'Rubros Importados Correctamente!');
                }else{
                    return redirect('/rubros')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }

    }
}
