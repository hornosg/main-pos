<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Pais;
use DB;
use Illuminate\Http\Request;
use Excel;

class PaisesController extends Controller
{
    //
    public function index()
    {
        $items = Pais::all();
        return view('master.index', ['titulo' => 'Paises','urlkey' => 'paises','items' => $items]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
        ]);


        $pais = $request->all();
        $pais['descripcion'] = strtoupper($pais['descripcion']);

        if (isset($pais['activo'])){
            $pais['activo']=1;
        }else{
            $pais['activo']=0;
        }

        $data=Pais::create($pais);

        if ($pais['activo']==1){
            $data['activo']='si';
        }else{
            $data['activo']='no';
        }
        $data['msg']="Pais Creado Exitosamente!";
        return $data;
    }

    public function edit($id)
    {
        $item = Pais::find($id);
        return view('master.edit', ['titulo' => 'Paises','urlkey' => 'paises' ,'item' => $item]);

    }

    public function show($id)
    {
        $item = Pais::find($id);
        return view('master.show', ['titulo' => 'Paises','urlkey' => 'paises' ,'item' => $item]);
    }


    public function update($id,Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
        ]);

        $pais = Pais::find($id);
        $pais->descripcion = strtoupper($request->descripcion);

        if (isset($request->activo)){
            $pais->activo=1;
        }else{
            $pais->activo=0;
        }

        $pais->save();
        $data['msg']="Pais Modificado.";
        return $data;
    }

    public function delete($id)
    {
        $pais = Pais::find($id);
        $pais->delete();
        $data['msg']="Pais Eliminado!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $pais = Pais::find($item);
            $pais->delete();
        }
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if(empty($value->descripcion)){
                        break;
                    }
                    $items[] = ['descripcion' => $value->descripcion, 'activo' => true];
                }
                if(!empty($items)){
                    DB::table('conf_paises')->insert($items);
                    return redirect('/paises')->with('status', 'Paises Importados Correctamente!');
                }else{
                    return redirect('/paises')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comun&iacute;quese con el equipo de Soporte a Usuarios.');
                }
            }
        }

    }
}
