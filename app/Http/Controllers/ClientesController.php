<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Localidad;
use App\Models\Notificacion;
use App\Models\NotificacionEstado;
use App\Models\TipoCliente;
use App\Models\TipoIva;
use App\Models\Titular;
use Artisan;
use Carbon\Carbon;
use DB;
use Dompdf\Dompdf;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ClientesController extends Controller
{

    public function __construct(){
        //$this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = DB::select('select titulares.*, sys_tipoclientes.descripcion as tipo, conf_localidades.descripcion as localidad
                                  from titulares
                                  left join sys_tipoclientes on sys_tipoclientes.id = titulares.id_tipocliente
                                  left join conf_localidades on conf_localidades.id = titulares.id_localidad
                                 where id_tipotitular=2
                                    ');

        return view('titulares.clientes.index',[
            'titulo' => 'Clientes',
            'urlkey' => 'clientes',
            'clientes' => $clientes,
            'itemnav' => 'clientes'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('titulares.clientes.create',[
            'titulo' => 'Clientes',
            'urlkey' => 'clientes',
            'itemnav' => 'clientes',
            'item' => 'Cliente',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->request);
        $this->validate($request, [
            'descripcion' => 'required'
        ]);

        $cliente =  new Cliente;
        $cliente->descripcion = strtoupper($request->descripcion);
        $cliente->id_tipotitular =  2;
        $cliente->id_tipocliente =  $request->tipo;
        $cliente->id_tipoiva =  $request->id_tipoiva;
        $cliente->cuit =  $request->cuit;
        $cliente->direccion = $request->direccion;
        $cliente->id_localidad = $request->id_localidad;
        $cliente->telefono = $request->telefono;
        $cliente->web = $request->web;
        $cliente->email = $request->email;
        $cliente->anotaciones = $request->anotaciones;
        $cliente->activo = $request->activo ? 1 : 0;
        $cliente->fecha_cumple = Carbon::parse($request->fecha_cumple);
        $cliente->save();
        if ($request->action=='save'){
            return redirect('/clientes')->with('status', 'Item Creado Exitosamente!');
        }else{
            return redirect('/clientes/create')->with('status', 'Item Creado Exitosamente!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = Cliente::find($id);
        return view('titulares.clientes.show-edit',[
            'titulo' => 'Clientes',
            'urlkey' => 'clientes',
            'itemnav' => 'clientes',
            'item' => 'Cliente',
            'cliente' => $cliente,
            'edit'=> 0 ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id);
        return view('titulares.clientes.show-edit',[
            'titulo' => 'Clientes',
            'urlkey' => 'clientes',
            'itemnav' => 'clientes',
            'item' => 'Cliente',
            'cliente' => $cliente,
            'edit'=> 1 ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        if ($id==2){
            $data['msg']="No es Posible modificar Consumidor Final";
            return $data;
        }

        $cliente = Cliente::find($id);
        $cliente->id_tipocliente =  $request->tipo;
        $cliente->descripcion = strtoupper($request->descripcion);
        $cliente->id_tipocliente =  $request->tipo;
        $cliente->id_tipoiva =  $request->id_tipoiva;
        $cliente->cuit =  $request->cuit;
        $cliente->direccion = $request->direccion;
        $cliente->id_localidad = $request->id_localidad;
        $cliente->telefono = $request->telefono;
        $cliente->web = $request->web;
        $cliente->email = $request->email;
        $cliente->anotaciones = $request->anotaciones;
        $cliente->activo = $request->activo ? 1 : 0;
        $cliente->fecha_cumple = Carbon::parse($request->fecha_cumple);
        $cliente->save();
        if ($request->action=='save'){
            return redirect('/clientes')->with('status', 'Item Creado Exitosamente!');
        }else{
            return redirect('/clientes/edit/'.$cliente->id)->with('status', 'Item Creado Exitosamente!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if ($id==2){
            return redirect('/clientes')->with('status', 'No es Posible eliminar Consumidor Final');
        }

        DB::beginTransaction();
            $cliente = Cliente::find($id);
            $cliente->delete();

            $notificacion = New Notificacion();
            $notificacion->icon = 'fa fa-user-times text-yellow';
            $notificacion->descripcion = 'Se Eliminó el Cliente: '.$cliente->descripcion;
            $notificacion->created_us = auth()->id();
            $notificacion->save();

            foreach(DB::table('users')->whereNull('deleted_at')->get() as $user){
                $notiEstado = New NotificacionEstado();
                $notiEstado->id_notificacion = $notificacion->id;
                $notiEstado->id_user = $user->id;
                $notiEstado->id_estado = 14;
                $notiEstado->save();
            }
        DB::commit();
        return redirect('/clientes')->with('status', 'Item Eliminado!');
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        Cliente::whereIn('id',$datos)->delete();
        return;
    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required',
        ]);

        set_time_limit(300);

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data->chunk(1000) as $chunk){
                    foreach ($chunk  as $value) {
//                        dd($value);
                        if(empty($value->nombre)){
                            break;
                        }

                        $tipocliente = TipoCliente::where('descripcion', $value->tipo)->select('id')->first();
                        $categoria = TipoIva::where('descripcion', $value->categoria)->select('id')->first();
                        $localidad = Localidad::where('descripcion', $value->localidad)->select('id')->first();

                        $item    = ['descripcion'    => $value->nombre,
                            'id_tipotitular'    => 2,
                            'id_tipocliente'	=> $tipocliente['id'],
                            'cuit'	        => $value->cuit,
                            'id_tipoiva'	=> $categoria['id'],
                            'direccion'	        => $value->direccion,
                            'id_localidad'	=> $localidad['id'],
                            'telefono'	=> $value->telefono,
                            'web'	    => $value->web,
                            'email'	    => $value->email,
                            'activo'        => true];

                        Cliente::create($item);
                        $items[]=$item;
                    }
                }
//                dd($items);
                if(!empty($items)){
//                    DB::table('items')->insert($items);
                    $msj =  count($items).' Clientes Importados Correctamente!';
                    return redirect('/clientes')->with('status', $msj);
                }else{
                    return redirect('/clientes')->with('status', 'No se importaron Datos. Revice el archivo y vuelva a intentarlo o comuníquese con el equipo de Soporte a Usuarios.');
                }
            }
        }

    }

//    public function ctacte($id=null)
//    {
//        $saldo=0;$saldosv=0;$saldov1=0;$saldov2=0;$saldov3=0;
//        $porcsv=0;$porcv1=0;$porcv2=0;$porcv3=0;
//        if (empty($id)){
//            $items=[];
//        }else{
//            $now = time();
//            $temptable = 'zctacte'.$now;
//            DB::select('CREATE TABLE IF NOT EXISTS '.$temptable.' ( INDEX(id_titular,id_tipocpte), INDEX(id_estado,pasaron) ) ENGINE=INNODB AS
//                         SELECT c.id AS id,
//                                c.id_tipocpte AS id_tipocpte,
//                                c.id_titular AS id_titular,
//                                c.numero AS numero,
//                                c.id_caja AS id_caja,
//                                c.id_facturaelectronica AS id_facturaelectronica,
//                                c.total AS total,
//                                c.observaciones AS observaciones,
//                                c.id_cpterel AS id_cpterel,
//                                c.id_estado AS id_estado,
//                                c.created_at AS created_at,
//                                c.updated_at AS updated_at,
//                                c.created_us AS created_us,
//                                c.updated_us AS updated_us,
//                                c.deleted_at AS deleted_at,
//                                DATE_FORMAT(c.created_at, "%d/%m/%Y %H:%i:%s") AS creado,
//                                tc.descripcion AS tipo,
//                                c.total AS saldo,
//                                t.descripcion AS cliente,
//                                us.name AS usuario,
//                                e.descripcion AS estado,
//                                (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) AS pasaron
//                           FROM comprobantes c
//                           JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
//                      LEFT JOIN titulares t ON t.id = c.id_titular
//                      LEFT JOIN users us ON us.id = c.created_us
//                      LEFT JOIN sys_tipocomprobantes tc ON tc.id = c.id_tipocpte
//                      LEFT JOIN sys_estados e ON e.id = c.id_estado
//                          WHERE (
//                                  ( (c.id_tipocpte=5) AND (c.id_cpterel IS NULL) ) OR
//                                  (c.id_tipocpte=6) OR
//                                  ( (c.id_tipocpte=3) AND (c.id_cpterel IS NULL) ) OR
//                                  ( (c.id_tipocpte=3) AND ( (SELECT id_estado FROM comprobantes WHERE id_tipocpte=14 AND id=c.id_cpterel)=3 ) )
//                                 )
//                            AND c.deleted_at IS NULL
//                            AND c.id_titular = ?
//                          UNION
//                         SELECT c.id AS id,
//                                c.id_tipocpte AS id_tipocpte,
//                                c.id_titular AS id_titular,
//                                c.numero AS numero,
//                                c.id_caja AS id_caja,
//                                c.id_facturaelectronica AS id_facturaelectronica,
//                                c.total AS total,
//                                c.observaciones AS observaciones,
//                                c.id_cpterel AS id_cpterel,
//                                c.id_estado AS id_estado,
//                                c.created_at AS created_at,
//                                c.updated_at AS updated_at,
//                                c.created_us AS created_us,
//                                c.updated_us AS updated_us,
//                                c.deleted_at AS deleted_at,
//                                DATE_FORMAT(c.created_at, "%d/%m/%Y %H:%i:%s") AS creado,
//                                tc.descripcion AS tipo,
//                                (c.total * -(1)) AS saldo,
//                                t.descripcion AS cliente,
//                                us.name AS usuario,
//                                e.descripcion AS estado,
//                                (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) AS pasaron
//                           FROM comprobantes c
//                      LEFT JOIN titulares t ON t.id = c.id_titular
//                      LEFT JOIN users us ON us.id = c.created_us
//                      LEFT JOIN sys_tipocomprobantes tc ON tc.id = c.id_tipocpte
//                      LEFT JOIN sys_estados e ON e.id = c.id_estado
//                          WHERE c.id_tipocpte IN (7,13,14)
//                            AND c.deleted_at IS NULL
//                            AND c.id_estado = 3
//                            AND c.id_titular = ?
//                      ORDER BY id ',[$id,$id]);
//
//            $pagosq= DB::select('select sum(total) as total from '.$temptable.' haber where haber.id_tipocpte in (7,13,14)');
//            $pagos = floatval($pagosq[0]->total);
//
//            $items = DB::table($temptable.' as cc')
//                ->where('cc.id_titular', $id)
//                ->select('cc.*',
//                    DB::raw('CASE WHEN cc.id_tipocpte in (5,6,3) THEN cc.total
//                                  ELSE 0
//                             END as debe'),
//                    DB::raw('CASE WHEN cc.id_tipocpte in (7,13,14) THEN cc.total
//                                  ELSE 0
//                             END as haber'),
//                    DB::raw('CASE WHEN cc.id_tipocpte in (7,13,14) THEN 0
//                                  WHEN ('.$pagos.') = 0 THEN cc.total
//                                  WHEN ('.$pagos.') >= (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) THEN 0
//                                  WHEN ('.$pagos.') >  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<cc.id) THEN (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) - ('.$pagos.')
//                                  WHEN ('.$pagos.') <=  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<cc.id) THEN cc.total
//                                  WHEN ('.$pagos.') <=  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) THEN (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) - ('.$pagos.')
//                             END as saldopend'),
//                    DB::raw('(select sum(saldo) from '.$temptable.' cc2 where cc2.id_titular=cc.id_titular and cc2.id <=cc.id) as saldototal'))
//                ->get();
//
//            $saldoq=DB::table($temptable.' as cc')->sum('saldo');
//            $saldo = floatval($saldoq);
//
//            if ($saldo>0){
//                $saldosv=DB::table($temptable.' as cc')
//                        ->where('cc.id_estado', 2)
//                        ->where('cc.pasaron','<', 15)
//                        ->sum(DB::raw('CASE WHEN ('.$pagos.') >  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<cc.id) THEN (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) - ('.$pagos.')
//                                            WHEN ('.$pagos.') >= (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) THEN 0
//                                            WHEN ('.$pagos.') <  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<cc.id) THEN cc.total
//                                            WHEN ('.$pagos.') <=  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) THEN (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) - ('.$pagos.')
//                                        END'));
//                $porcsv= ($saldosv>0) ? round($saldosv*100/$saldo) : 0 ;
//
//                $saldov1=DB::table($temptable.' as cc')
//                    ->where('cc.id_titular', $id)
//                    ->where('cc.id_estado', 2)
//                    ->whereBetween('cc.pasaron', [15,90])
//                    ->sum(DB::raw('CASE WHEN ('.$pagos.') >  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<cc.id) THEN (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) - ('.$pagos.')
//                                        WHEN ('.$pagos.') >= (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) THEN 0
//                                        WHEN ('.$pagos.') <  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<cc.id) THEN cc.total
//                                        WHEN ('.$pagos.') <=  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) THEN (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) - ('.$pagos.')
//                                    END'));
//                $porcv1= ($saldov1>0) ? round($saldov1*100/$saldo) : 0 ;
//
//                $saldov2=DB::table($temptable.' as cc')
//                    ->where('cc.id_titular', $id)
//                    ->where('cc.id_estado', 2)
//                    ->whereBetween('cc.pasaron', [91,180])
//                    ->sum(DB::raw('CASE WHEN ('.$pagos.') >  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<cc.id) THEN (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) - ('.$pagos.')
//                                        WHEN ('.$pagos.') >= (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) THEN 0
//                                        WHEN ('.$pagos.') <  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<cc.id) THEN cc.total
//                                        WHEN ('.$pagos.') <=  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) THEN (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) - ('.$pagos.')
//                                    END'));
//                $porcv2= ($saldov2>0) ? round($saldov2*100/$saldo) : 0 ;
//
//                $saldov3=DB::table($temptable.' as cc')
//                    ->where('cc.id_titular', $id)
//                    ->where('cc.id_estado', 2)
//                    ->where('cc.pasaron','>', 180)
//                    ->sum(DB::raw('CASE WHEN ('.$pagos.') >  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<cc.id) THEN (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) - ('.$pagos.')
//                                        WHEN ('.$pagos.') >= (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) THEN 0
//                                        WHEN ('.$pagos.') <  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<cc.id) THEN cc.total
//                                        WHEN ('.$pagos.') <=  (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) THEN (select sum(total) from '.$temptable.' debe where debe.id_tipocpte in (5,6,3) and debe.id<=cc.id) - ('.$pagos.')
//                                    END'));
//                $porcv3= ($saldov3>0) ? round($saldov3*100/$saldo) : 0 ;
//            }else{
//                $porcsv=0;$porcv1=0;$porcv2=0;$porcv3=0;
//            }
//            DB::delete('DROP TABLE '.$temptable);
//        }
//
//        return view('titulares.clientes.ctacte',[
//            'titulo' => 'Clientes',
//            'titulo2' => 'Cuenta Corriente',
//            'urlkey' => 'clientes',
//            'itemnav' => 'ctacte',
//            'idcliente' => $id,
//            'items' => $items,
//            'saldo'=> $saldo,
//            'saldosv'=> $saldosv,
//            'saldov1'=> $saldov1,
//            'saldov2'=> $saldov2,
//            'saldov3'=> $saldov3,
//            'porcsv'=> $porcsv,
//            'porcv1'=> $porcv1,
//            'porcv2'=> $porcv2,
//            'porcv3'=> $porcv3
//        ]);
//    }

    public function ctacte($id=null)
    {
        $saldo=0;$saldosv=0;$saldov1=0;$saldov2=0;$saldov3=0;
        $porcsv=0;$porcv1=0;$porcv2=0;$porcv3=0;
        if (empty($id)){
            $items=[];
        }else{
            //Artisan::call('calcular:saldos', ['titular' => $id]);
            $now = time();
            $temptable = 'zctacte'.$now;
            $this->createTempCtacte($temptable,$id);
            $items = DB::table($temptable.' as cc')
                ->where('id_titular', $id)
                ->select('id', 'creado', 'tipo', 'numero', 'id_estado', 'estado', 'pasaron', 'saldocpte as saldopend', 'saldototal',
                    DB::raw('CASE WHEN cc.id_tipocpte in (5,6,3) THEN cc.total ELSE 0 END as debe'),
                    DB::raw('CASE WHEN cc.id_tipocpte in (7,13,14) THEN cc.total ELSE 0 END as haber'))
                ->get();

            $saldoq=DB::table($temptable.' as cc')->where('id_titular', $id)->sum('saldocpte');
            $saldo = floatval($saldoq);

            if ($saldo>0){
                $saldosv=DB::table($temptable.' as cc')
                    ->where('id_titular', $id)
                    ->where('cc.id_estado', 2)
                    ->where('cc.pasaron','<', 15)
                    ->sum('saldocpte');
                $porcsv= ($saldosv>0) ? round($saldosv*100/$saldo) : 0 ;

                $saldov1=DB::table($temptable.' as cc')
                    ->where('cc.id_titular', $id)
                    ->where('cc.id_estado', 2)
                    ->whereBetween('cc.pasaron', [15,90])
                    ->sum('saldocpte');
                $porcv1= ($saldov1>0) ? round($saldov1*100/$saldo) : 0 ;

                $saldov2=DB::table($temptable.' as cc')
                    ->where('cc.id_titular', $id)
                    ->where('cc.id_estado', 2)
                    ->whereBetween('cc.pasaron', [91,180])
                    ->sum('saldocpte');
                $porcv2= ($saldov2>0) ? round($saldov2*100/$saldo) : 0 ;

                $saldov3=DB::table($temptable.' as cc')
                    ->where('cc.id_titular', $id)
                    ->where('cc.id_estado', 2)
                    ->where('cc.pasaron','>', 180)
                    ->sum('saldocpte');
                $porcv3= ($saldov3>0) ? round($saldov3*100/$saldo) : 0 ;

                $this->dropTempCtacte($temptable);
            }else{
                $porcsv=0;$porcv1=0;$porcv2=0;$porcv3=0;
            }
        }

        return view('titulares.clientes.ctacte',[
            'titulo' => 'Clientes',
            'titulo2' => 'Cuenta Corriente',
            'urlkey' => 'clientes',
            'itemnav' => 'ctacte',
            'idcliente' => $id,
            'items' => $items,
            'saldo'=> $saldo,
            'saldosv'=> $saldosv,
            'saldov1'=> $saldov1,
            'saldov2'=> $saldov2,
            'saldov3'=> $saldov3,
            'porcsv'=> $porcsv,
            'porcv1'=> $porcv1,
            'porcv2'=> $porcv2,
            'porcv3'=> $porcv3
        ]);
    }

    public function printview($id)
    {
        $titular = Titular::find($id);
        $items = DB::table('ctacte as cc')
            ->where('cc.id_titular', $id)
            ->select('cc.*',
                DB::raw('(select sum(saldo) from ctacte cc2 where cc2.id_titular=cc.id_titular and cc2.id <=cc.id) as saldototal'))
            ->orderBy('id', 'asc')
            ->get();
        $saldo=DB::table('ctacte as cc')
            ->where('cc.id_titular', $id)
            ->whereNotNull('cc.deleted_at')
            ->sum('saldo');
        if(empty($saldo)){
            $saldo=0;
        }

        return view('titulares.clientes.print',[
            'titular' => $titular,
            'items' => $items,
            'saldo' => $saldo
        ]);
    }

    public function getprint($id)
    {
        $url = route('printctacte',['id'=>$id]);
        //dd($url);

        $dompdf = new DOMPDF();
        $dompdf->set_option('enable_remote', TRUE);
        $dompdf->loadHtmlFile($url);

        // Render the HTML as PDF
        $dompdf->render();
        //dd($dompdf);

        // Output the generated PDF to Browser
        $filename='ctacte'.$id.'.pdf';
        $dompdf->stream($filename);
    }

    public function vencidos()
    {
        return view('titulares.clientes.ctactevda',[
            'titulo' => 'Clientes',
            'titulo2' => 'Comprobantes Vencidos',
            'urlkey' => 'clientes',
            'itemnav' => 'ctactevda'
        ]);
    }

    public function repararestados($id)
    {
        $exitCode = Artisan::call('calcular:saldos', ['titular' => $id]);
        return redirect('/clientes/cuentacorriente/'.$id);
    }

    public function actualizarprecios($id)
    {
        $exitCode = Artisan::call('actualizar:precios', ['titular' => $id]);
        return redirect('/clientes/cuentacorriente/'.$id);
    }

    private function createTempCtacte($tempTableName, $id)
    {
        DB::select('CREATE TABLE IF NOT EXISTS '.$tempTableName.' ( INDEX(id_titular,id_tipocpte), INDEX(id_estado,pasaron) ) ENGINE=INNODB AS
                         SELECT c.id AS id,
                                c.id_tipocpte AS id_tipocpte,
                                c.id_titular AS id_titular,
                                c.numero AS numero,
                                c.id_caja AS id_caja,
                                c.id_facturaelectronica AS id_facturaelectronica,
                                c.total AS total,
                                c.observaciones AS observaciones,
                                c.id_cpterel AS id_cpterel,
                                c.id_estado AS id_estado,
                                DATE_FORMAT(c.created_at, "%d/%m/%Y %H:%i:%s") AS creado,
                                tc.descripcion AS tipo,
                                c.total AS saldo,
                                saldocpte,
                                saldototal,
                                t.descripcion AS cliente,
                                us.name AS usuario,
                                e.descripcion AS estado,
                                (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) AS pasaron
                           FROM comprobantes c
                           JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                      LEFT JOIN titulares t ON t.id = c.id_titular
                      LEFT JOIN users us ON us.id = c.created_us
                      LEFT JOIN sys_tipocomprobantes tc ON tc.id = c.id_tipocpte
                      LEFT JOIN sys_estados e ON e.id = c.id_estado
                          WHERE (
                                  ( (c.id_tipocpte=5) AND (c.id_cpterel IS NULL) ) OR
                                  (c.id_tipocpte=6) OR
                                  ( (c.id_tipocpte=3) AND (c.id_cpterel IS NULL) ) OR
                                  ( (c.id_tipocpte=3) AND ( (SELECT id_estado FROM comprobantes WHERE id_tipocpte=14 AND id=c.id_cpterel)=3 ) )
                                 )
                            AND c.deleted_at IS NULL
                            AND c.id_titular = ?
                          UNION
                         SELECT c.id AS id,
                                c.id_tipocpte AS id_tipocpte,
                                c.id_titular AS id_titular,
                                c.numero AS numero,
                                c.id_caja AS id_caja,
                                c.id_facturaelectronica AS id_facturaelectronica,
                                c.total AS total,
                                c.observaciones AS observaciones,
                                c.id_cpterel AS id_cpterel,
                                c.id_estado AS id_estado,
                                DATE_FORMAT(c.created_at, "%d/%m/%Y %H:%i:%s") AS creado,
                                tc.descripcion AS tipo,
                                (c.total * -(1)) AS saldo,
                                saldocpte,
                                saldototal,
                                t.descripcion AS cliente,
                                us.name AS usuario,
                                e.descripcion AS estado,
                                (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) AS pasaron
                           FROM comprobantes c
                      LEFT JOIN titulares t ON t.id = c.id_titular
                      LEFT JOIN users us ON us.id = c.created_us
                      LEFT JOIN sys_tipocomprobantes tc ON tc.id = c.id_tipocpte
                      LEFT JOIN sys_estados e ON e.id = c.id_estado
                          WHERE c.id_tipocpte IN (7,13,14)
                            AND c.deleted_at IS NULL
                            AND c.id_estado = 3
                            AND c.id_titular = ?
                      ORDER BY id  ',[$id,$id]);
    }

    private function dropTempCtacte($tempTableName)
    {
        DB::select('DROP TABLE IF EXISTS '.$tempTableName);
    }

}
