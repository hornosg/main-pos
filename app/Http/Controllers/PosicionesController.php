<?php

namespace App\Http\Controllers;

use App\Models\Posicion;
use App\Models\Ubicacion;
use App\Models\Sector;
use DB;
use Illuminate\Http\Request;

class PosicionesController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
            'id_ubicacion' => 'required'
        ]);

        $posicion = $request->all();
        $posicion['descripcion'] = strtoupper($posicion['descripcion']);

        if (isset($posicion['activo'])){
            $posicion['activo']=1;
        }else{
            $posicion['activo']=0;
        }
        $data=Posicion::create($posicion);
        $data['ubicacion']=Ubicacion::find($data['id_ubicacion'])->descripcion.' ('.Sector::find(Ubicacion::find($data['id_ubicacion'])->id_sector)->descripcion.')';

        if ($posicion['activo']==1){
            $data['activo']='si';
        }else{
            $data['activo']='no';
        }
        $data['msg']="Posicion Creado Exitosamente!";
        return $data;
    }

    public function update($id,Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
            'id_ubicacion' => 'required'
        ]);

        $posicion = Posicion::find($id);

        $posicion->descripcion = strtoupper($request->descripcion);
        $posicion->id_ubicacion = $request->id_ubicacion;

        if (isset($request->activo)){
            $posicion->activo=1;
        }else{
            $posicion->activo=0;
        }

        $posicion->save();
        $data=$posicion;
        $data['ubicacion']=Ubicacion::find($data['id_ubicacion'])->descripcion.' ('.Sector::find(Ubicacion::find($data['id_ubicacion'])->id_sector)->descripcion.')';
        $data['msg']="Posicion Modificado.";
        return $data;
    }

    public function delete($id)
    {
        $posicion = Posicion::find($id);
        $posicion->delete();
        $data['msg']="Posicion Eliminado!";
        return $data;
    }

    public function removeselected(Request $request)
    {
        $datos = $request->input('datos');
        foreach($datos as $item){
            $posicion = Posicion::find($item);
            $posicion->delete();
        }
        return;
    }

    public function storeMasivo(Request $request)
    {
        $this->validate($request, [
            'prefijo' => 'required',
            'id_ubicacion2' => 'required',
            'columnas' => 'required',
            'filas' => 'required'
        ]);

        $datos=[];
        $columnas = explode( ',', $request->columnas);
        foreach ($columnas as $columna){
            for ($i = 1; $i <= $request->filas; $i++) {
                $posicion = new Posicion();
                $posicion->descripcion = $request->prefijo.$columna.$i;
                $posicion->id_ubicacion=$request->id_ubicacion2;
                $posicion->activo=1;
                $posicion->save();
                $posicion->ubicacion=Ubicacion::find($posicion['id_ubicacion'])->descripcion.' ('.Sector::find(Ubicacion::find($posicion['id_ubicacion'])->id_sector)->descripcion.')';
                array_push($datos,$posicion);
            }
        }
        $data['data']=$datos;
        $data['msg']="Posiciones Creadas Exitosamente!";
        return $data;
    }
}
