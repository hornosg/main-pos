<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Ubicacion;
use App\Models\Sector;
use DB;
use Illuminate\Http\Request;

class UbicacionesController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
            'id_sector' => 'required'
        ]);
        $ubicacion = $request->all();
        $ubicacion['descripcion'] = strtoupper($ubicacion['descripcion']);

        if (isset($ubicacion['activo'])){
            $ubicacion['activo']=1;
        }else{
            $ubicacion['activo']=0;
        }
        $data=Ubicacion::create($ubicacion);
        $data['sector']=Sector::find($data['id_sector'])->descripcion;

        if ($ubicacion['activo']==1){
            $data['activo']='si';
        }else{
            $data['activo']='no';
        }
        $data['msg']="Ubicacion Creado Exitosamente!";

        return $data;
    }

    public function update($id,Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:255',
            'id_sector' => 'required'
        ]);

        $ubicacion = Ubicacion::find($id);

        $ubicacion->descripcion = strtoupper($request->descripcion);
        $ubicacion->id_sector = $request->id_sector;
        $ubicacion->alias = $request->alias;

        if (isset($request->activo)){
            $ubicacion->activo=1;
        }else{
            $ubicacion->activo=0;
        }

        $ubicacion->save();
        $data=$ubicacion;
        $data['sector']=Sector::find($data['id_sector'])->alias;
        $data['msg']="Ubicacion Modificado.";
        return $data;
    }

    public function delete($id)
    {
        $ubicacion = Ubicacion::find($id);
        $ubicacion->delete();
        $data['msg']="Ubicacion Eliminado!";
        return $data;
    }
}
