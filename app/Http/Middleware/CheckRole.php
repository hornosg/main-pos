<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($request->user()->id_rol == $role) {
                    return $next($request);
                }
            }
        }else{
            if ($request->user()->id_rol == $roles) {
                return $next($request);
            }
        }

        if($request->ajax()) {
            abort(403, 'Acceso Denegado');
        }else{
            return redirect('/error/acceso-denegado');
        }

    }
}
