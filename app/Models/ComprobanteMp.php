<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComprobanteMp extends Model
{
    protected $table = 'comprobantes_mp';
}
