<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posicion extends Model
{
    protected $table = 'inv_posiciones';
    protected $fillable = ['descripcion', 'id_ubicacion', 'activo'];
}
