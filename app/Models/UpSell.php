<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UpSell extends Model
{
    protected $table = 'items_up_sell';
    protected $fillable = ['id_item', 'itemup'];
}
