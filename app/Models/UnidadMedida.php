<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnidadMedida extends Model
{
    protected $table = 'conf_unidades_medida';
    protected $fillable = ['descripcion', 'activo'];
}
