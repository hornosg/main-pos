<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoTitular extends Model
{
    protected $table = 'sys_tipotitulares';
    protected $fillable = ['descripcion', 'activo'];
}
