<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = 'conf_materiales';
    protected $fillable = ['descripcion', 'activo'];
}
