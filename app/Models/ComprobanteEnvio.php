<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComprobanteEnvio extends Model
{
    protected $table = 'comprobantes_envio';
}
