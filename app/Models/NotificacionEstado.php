<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificacionEstado extends Model
{
    protected $table = 'notificaciones_estados';
}
