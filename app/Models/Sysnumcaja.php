<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sysnumcaja extends Model
{
    protected $table = 'sys_numcaja';
    protected $fillable = ['id_sucursal','id_user'];
}
