<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoComprobante extends Model
{
    protected $table = 'sys_tipocomprobantes';
    protected $fillable = ['id_tipocpte','id_mediopago', 'id_titular','concepto','signo'];
}
