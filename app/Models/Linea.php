<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Linea extends Model
{
    protected $table = 'conf_lineas';
    protected $fillable = ['descripcion', 'activo'];
}
