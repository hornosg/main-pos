<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AjustePrecio extends Model
{
    protected $table = 'conf_ajustes_precio';
    protected $fillable = ['descripcion','nota','porcentaje','signo','activo'];
}
