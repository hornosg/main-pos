<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogPrecioItem extends Model
{
    protected $table = 'log_precios_items';
}
