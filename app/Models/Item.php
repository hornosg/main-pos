<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'items';
    protected $fillable = ['descripcion',
                          'id_tipoitem',
                          'ean',
                          'id_unidadventa',
                          'contNeto',
                          'id_unidadcontneto',
                          'peso',
                          'largo',
                          'ancho',
                          'alto',
                          'id_marca',
                          'id_rubro',
                          'id_linea',
                          'id_material',
//                          'esCombo',
                          'puntoRepocicion',
                          'stockMinimo',
                          'id_moneda',
                          'precioLista',
                          'bonifProv',
                          'precioCosto',
                          'ganancia',
                          'precio',
                          'especificaciones',
                          'observaciones',
                          'activo',
                          'distribuye'];
    public function scopeProducto($query){
      return $query->where('id_tipoitem',6);
    }

    public function setid_tipoitemAttribute($id_tipoitem)
    {
        $this->attributes['id_tipoitem'] = trim($id_tipoitem) !== '' ? $id_tipoitem : null;
    }

    public function setEanAttribute($ean)
    {
        $this->attributes['ean'] = trim($ean) !== '' ? $ean : null;
    }

    public function setid_unidadventaAttribute($id_unidadventa)
    {
        $this->attributes['id_unidadventa'] = trim($id_unidadventa) !== '' ? $id_unidadventa : null;
    }

    public function setContNetoAttribute($contNeto)
    {
        $this->attributes['contNeto'] = trim($contNeto) !== '' ? $contNeto : null;
    }

    public function setid_unidadcontnetoAttribute($id_unidadcontneto)
    {
        $this->attributes['id_unidadcontneto'] = trim($id_unidadcontneto) !== '' ? $id_unidadcontneto : null;
    }

    public function setPesoAttribute($peso)
    {
        $this->attributes['peso'] = trim($peso) !== '' ? $peso : null;
    }

    public function setLargoAttribute($largo)
    {
        $this->attributes['largo'] = trim($largo) !== '' ? $largo : null;
    }

    public function setAnchoAttribute($ancho)
    {
        $this->attributes['ancho'] = trim($ancho) !== '' ? $ancho : null;
    }

    public function setAltoAttribute($alto)
    {
        $this->attributes['alto'] = trim($alto) !== '' ? $alto : null;
    }

    public function setid_marcaAttribute($id_marca)
    {
        $this->attributes['id_marca'] = trim($id_marca) !== '' ? $id_marca : null;
    }

    public function setid_rubroAttribute($id_rubro)
    {
        $this->attributes['id_rubro'] = trim($id_rubro) !== '' ? $id_rubro : null;
    }

    public function setid_lineaAttribute($id_linea)
    {
        $this->attributes['id_linea'] = trim($id_linea) !== '' ? $id_linea : null;
    }

    public function setid_materialAttribute($id_material)
    {
        $this->attributes['id_material'] = trim($id_material) !== '' ? $id_material : null;
    }

    public function setPuntoRepocicionAttribute($puntoRepocicion)
    {
        $this->attributes['puntoRepocicion'] = trim($puntoRepocicion) !== '' ? $puntoRepocicion : null;
    }

    public function setStockMinimoAttribute($stockMinimo)
    {
        $this->attributes['stockMinimo'] = trim($stockMinimo) !== '' ? $stockMinimo : null;
    }

    public function setPrecioListaAttribute($precioLista)
    {
        $this->attributes['precioLista'] = trim($precioLista) !== '' ? $precioLista : null;
    }

    public function setBonifProvAttribute($bonifProv)
    {
        $this->attributes['bonifProv'] = trim($bonifProv) !== '' ? $bonifProv : null;
    }

    public function setPrecioCostoAttribute($precioCosto)
    {
        $this->attributes['precioCosto'] = trim($precioCosto) !== '' ? $precioCosto : null;
    }

    public function setGananciaAttribute($ganancia)
    {
        $this->attributes['ganancia'] = trim($ganancia) !== '' ? $ganancia : null;
    }

    public function setPrecioAttribute($precio)
    {
        $this->attributes['precio'] = trim($precio) !== '' ? $precio : null;
    }

    public function setEspecificacionesAttribute($especificaciones)
    {
        $this->attributes['especificaciones'] = trim($especificaciones) !== '' ? $especificaciones : null;
    }

    public function setObservacionesAttribute($observaciones)
    {
        $this->attributes['observaciones'] = trim($observaciones) !== '' ? $observaciones : null;
    }
}
