<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $table = 'inv_sectores';
    protected $fillable = ['descripcion', 'id_sucursal','alias','activo'];
}
