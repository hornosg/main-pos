<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MotivoMStock extends Model
{
    protected $table = 'sys_motivos_mstock';
}
