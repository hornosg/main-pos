<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoItem extends Model
{
    protected $table = 'sys_tipoitems';
    protected $fillable = ['descripcion', 'activo'];
}
