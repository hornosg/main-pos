<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rubro extends Model
{
    protected $table = 'conf_rubros';
    protected $fillable = ['descripcion', 'activo'];
}
