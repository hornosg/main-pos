<?php

namespace App\Models;

use App\Models\Titular;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
class Cliente extends Titular
{

	public function tipo(){
		return $this->hasOne('App\Models\TipoTitular','id','id_tipocliente');
	}

    protected static function boot(){
        parent::boot();
        static::addGlobalScope('cliente', function (Builder $builder) {
            $builder->where('id_tipotitular', '=', 2);
        });
    }
    public function setidTipoTitularAttribute($idTipoTitular = 2){
        $this->attributes['id_tipotitular'] = $idTipoTitular;
    }
}
