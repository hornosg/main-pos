<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recargo extends Model
{
    protected $table = 'conf_recargos';
    protected $fillable = ['descripcion','nota','porcentaje','activo'];
}
