<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Origen extends Model
{
    protected $table = 'conf_origen';
    protected $fillable = ['descripcion', 'activo'];
}
