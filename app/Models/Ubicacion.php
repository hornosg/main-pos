<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model
{
    protected $table = 'inv_ubicaciones';
    protected $fillable = ['descripcion', 'id_sector','alias','activo'];
}
