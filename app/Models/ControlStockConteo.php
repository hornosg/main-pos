<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ControlStockConteo extends Model
{
    protected $table = 'inv_control_stock_conteo';
}
