<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrossSell extends Model
{
    protected $table = 'items_cross_sell';
    protected $fillable = ['id_item', 'itemcross'];
}
