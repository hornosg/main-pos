<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    protected $table = 'conf_provincias';
    protected $fillable = ['descripcion','id_pais', 'activo'];
}
