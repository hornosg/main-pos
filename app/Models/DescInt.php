<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DescInt extends Model
{
    protected $table = 'conf_descint';
    protected $fillable = ['descripcion',
        'id_tipocliente',
        'id_mediopago',
        'importe_fijo',
        'porcentaje',
        'signo',
        'activo'];
}
