<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $table = 'conf_marcas';
    protected $fillable = ['descripcion', 'activo'];
}
