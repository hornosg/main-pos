<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ControlStock extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'inv_control_stock';
}
