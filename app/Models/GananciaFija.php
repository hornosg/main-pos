<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GananciaFija extends Model
{
    protected $table = 'conf_ganancia_fija';
    protected $fillable = ['id_tipocliente','importe_fijo','porcentaje','activo'];
}
