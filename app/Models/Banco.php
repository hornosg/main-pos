<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    protected $table = 'conf_bancos';
    protected $fillable = ['descripcion', 'activo'];
}
