<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MovimientoStock extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'inv_movimientos_stock';
}
