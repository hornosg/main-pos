<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Moneda extends Model
{
    protected $table = 'sys_monedas';
    protected $fillable = ['descripcion', 'activo'];
}
