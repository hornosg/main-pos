<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Titular extends Model{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
  protected $table = 'titulares';
  protected $fillable = [
    'descripcion',
    'id_tipotitular',
    'id_tipocliente',
    'plan',
    'logo',
    'id_tipoiva',
    'cuit',
    'direccion',
    'id_localidad',
    'telefono',
    'web',
    'email',
    'anotaciones',
    'activo'
  ];

  /*
  * para obtener los productos de un proveedor
  * $un_proveedor = App\Models\Titular::proveedor()->find(6); (o cualquier variante)
  * $un_proveedor->productos
  */

  /*
  * Relacion ManyToMany entre Item y proveedores intermediado por la tabla items_proveedores
  * donde id_titular y id_item son las claves foraneas de Item y Titular respectivamente
  */
  public function productos(){
    return $this->belongsToMany('App\Models\Item','items_proveedores','id_titular','id_item');
  }
  /*
  * para llamar a los proveedores usar
  * Titular::proveedor()->get();
  */
  public function scopeProveedor($query,$id = null){
    if($id)
      return $query->where('id_tipotitular','=',3)->where('id','=',$id);
    return $query->where('id_tipotitular',3);
  }

  public function scopeCliente($query,$id = null){
    if($id)
      return $query->where('id_tipotitular','=',2)->where('id','=',$id);
    return $query->where('id_tipotitular',2);
  }

  public function scopeEmpresa($query,$id =  null){
    if($id)
      return $query->where('id_tipotitular','=',1)->where('id','=',$id);
    return $query->where('id_tipotitular',1);
  }

    public function tipoiva()
    {
        return $this->belongsTo('App\Models\TipoIva','id_tipoiva','id');
    }
}
