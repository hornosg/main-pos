<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComprobanteDI extends Model
{
    protected $table = 'comprobantes_descint';

    public function descint()
    {
        return $this->belongsTo('App\Models\DescInt','id_descint','id');
    }
}
