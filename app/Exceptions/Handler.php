<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Mail\BugReport as BugReport;
use Illuminate\Support\Facades\Mail;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [

    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

//    /**  ORIGINAL DE LARAVEL */
//     * Render an exception into an HTTP response.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @param  \Exception  $exception
//     * @return \Illuminate\Http\Response
//     */
//    public function render($request, Exception $exception)
//    {
////        if ($exception->getStatusCode() = 404) {
////            return response()->view('vendor.adminlte.404', [], 404);
////        }
//
//        return parent::render($request, $exception);
//    }
////    /**  ORIGINAL DE LARAVEL */


    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        //dd($exception);
        //Mail::to('hornosg@gmail.com')->send(new BugReport($exception));

        if ($exception instanceof \Illuminate\Auth\AuthenticationException)  {
            $response = parent::render($request, $exception);
        }else{
            $response = response()->view('errors.custom',['exception' => $exception]);
        }
        return $response;
    }
}
