<?php

namespace App\Mail;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BugReport extends Mailable
{
    use Queueable, SerializesModels;

    public $exception;

    public function __construct(Exception $e)
    {
        $this->exception = $e;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('bugReport',['exception'=>$this->exception]);
    }
}
