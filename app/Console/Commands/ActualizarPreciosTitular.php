<?php

namespace App\Console\Commands;

use App\Models\Cliente;
use App\Models\ComprobanteItems;
use App\Models\Comprobante;
use App\Models\Item;
use Illuminate\Console\Command;
use DB;
use Log;

class ActualizarPreciosTitular extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'actualizar:precios {titular?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza los precios de los comprobantes impagos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info(date("Y-m-d H:i:s").' Comienza la actualizacion de precios');
        $titular=$this->argument('titular');
        if (empty($titular)){
            $titulares = Cliente::where('id', '!=' ,2)->get();
        }else{
            $titulares = Cliente::where('id',$titular)->get();
        }

        foreach ($titulares as $cliente){
            switch ($cliente->id_tipocliente) {
                case 1:
                    $diasplazo=15;
                    break;
                case 2:
                    $diasplazo=45;
                    break;
                case 3:
                    $diasplazo=30;
                    break;
                case 4:
                    $diasplazo=15;
                    break;
            }
            DB::beginTransaction();
                $this->info(date("Y-m-d H:i:s").' Comienza la actualizacion de precios para el Cliente:'. $cliente->id);
                $items = DB::select('select ci.*,c.id_ajuste
                                       from comprobantes as c
                                       join comprobantes_items as ci on ci.id_cpte = c.id
                                      where c.id_estado = 2
                                        and c.id_titular = ?
                                        and (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) > ?
                                        and c.deleted_at is null',[$cliente->id,$diasplazo]);
                foreach ($items as $item){
                    $cpteitem = ComprobanteItems::find($item->id);
                    $cpteitem->preciouni = $this->getprecio($cpteitem->id_item,$cliente->id,$item->id_ajuste);
                    $cpteitem->importe = $cpteitem->preciouni * $cpteitem->cantidad;
                    $cpteitem->save();
                }

                $cptes = DB::select('select c.*
                                       from comprobantes as c
                                      where c.id_estado = 2
                                        and c.id_titular = ?
                                        and (TO_DAYS(CURDATE()) - TO_DAYS(c.created_at)) > ?
                                        and c.deleted_at is null',[$cliente->id,$diasplazo]);
                foreach ($cptes as $cpte){
                    $comprobante = Comprobante::find($cpte->id);
                    $total = DB::select('SELECT SUM(importe) as total FROM comprobantes_items WHERE id_cpte=?',[$cpte->id]);
                    $comprobante->total = $total[0]->total;
                    $comprobante->save();
                }
            DB::commit();
        }
    }

    public function getprecio($id,$cliente,$ajuste=null){
        $producto = Item::find($id);
        $precio = $producto->precio;

        $titular = DB::table('titulares as t')->where('t.id',$cliente)->get();
        if(!empty($titular[0]->id_tipocliente)){
            $gananciafija = DB::table('conf_ganancia_fija as gf')
                ->where('gf.id_tipocliente',$titular[0]->id_tipocliente)
                ->where('gf.activo',1)
                ->get();
            if(!empty($gananciafija[0]->porcentaje)){
                $precio = $producto->precioCosto + ($producto->precioCosto * $gananciafija[0]->porcentaje/100);
            }
        }

        if(!empty($ajuste)){
            //Aplico el ajusto seleccionado sobre el precio hasta el momento calculado
            $ajusteprecio = DB::table('conf_ajustes_precio as ap')
                ->where('ap.id',$ajuste)
                ->where('ap.activo',1)
                ->get();
            if(!empty($ajusteprecio[0]->porcentaje)){
                $precio = $precio + (($precio * $ajusteprecio[0]->porcentaje/100) * $ajusteprecio[0]->signo);
            }
        }

        return $precio;
    }
}
