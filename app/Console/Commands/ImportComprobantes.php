<?php

namespace App\Console\Commands;

use App\Models\Comprobante;
use App\Models\ComprobanteMp;
use App\Models\Titular;
use Illuminate\Console\Command;
use DateTime;
use DB;

class ImportComprobantes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:comprobantes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importacion de Ventas y demas comprobantes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza la importacion de Comprobantes');
        DB::table('comprobantes_mp')->truncate();
        DB::table('comprobantes')->truncate();

        $file_n = storage_path('comprobantes.csv');
        $file = fopen($file_n, "r");
        while ( ($data = fgetcsv($file, 1000, ";")) !==FALSE )
        {
            $cpte = New Comprobante();
            $cpte->id_tipocpte = null;
            switch ($data[3]){
                case 1:
                    $cpte->id_tipocpte = 5;
                    $cpte->id_cpterel = null;
                    break;
                case 2:
                    $cpte->id_tipocpte = 7;
                    $cpte->id_cpterel = null;
                    break;
                case 5:
                    $cpte->id_tipocpte = 14;
                    $cpte->id_cpterel = $data[12]==''?null:$data[12];
                    break;
                case 6:
                    $cpte->id_tipocpte = 13;
                    $cpte->id_cpterel = $data[12]==''?null:$data[12];
                    break;
            }
            if(empty($cpte->id_tipocpte)){
                continue;
            }
            $cpte->numero = $data[4];
            if(empty($cpte->numero)){
                continue;
            }
            if ($data[5] == 234){
                $cpte->id_titular = $data[6]>0?$data[6]:2;
            }else{
                $cpte->id_titular = $data[5];
            }
            if(empty($cpte->id_titular)){
                $this->info($cpte->numero.' -> Sin Titular');
                continue;
            }else{
                if ($cpte->id_titular!=2){
                    $titular = Titular::where('oldid','=',$cpte->id_titular)->select('id')->first();
                    if(empty($titular)){
                        $this->info($cpte->numero.' -> Sin Titular oldid'.$cpte->id_titular);
                        continue;
                    }
                    $cpte->id_titular = $titular->id;
                }
            }
            $cpte->id_caja = $data[3];
            $cpte->total = $data[7];
            $cpte->created_us = 2;
            $cpte->updated_us = 2;

            $cpte->created_at = DateTime::createFromFormat('d/m/Y', $data[1]);
            $cpte->oldid = $data[0];

            if ($cpte->id_tipocpte==5){
                if ($data[8]==3){
                    $cpte->id_estado=2;
                }else{
                    $cpte->id_estado=1;
                }
            }else{
                $cpte->id_estado=3;
            }
            $cpte->save();

            if ($cpte->id_tipocpte==14 or $cpte->id_tipocpte==13){
                DB::update('update comprobantes as c1
                          join comprobantes as c2 on c2.oldid=c1.id_cpterel
                           set c1.id_cpterel=c2.id
                         where c1.id='.$cpte->id);
            }

            $cpteMp = New ComprobanteMp();
            $cpteMp->id_cpte = $cpte->id;
            switch ($data[8]){
                case 2:
                    $cpteMp->id_mediopago = 1;
                    break;
                case 3:
                    $cpteMp->id_mediopago = 2;
                    break;
                case 5:
                    $cpteMp->id_mediopago = 3;
                    break;
                case 6:
                    $cpteMp->id_mediopago = 5;
                    break;
                case 7:
                    $cpteMp->id_mediopago = 4;
                    break;
                case 9:
                    $cpteMp->id_mediopago = 2;
                    break;
            }
            $cpteMp->importe = $cpte->total;
            $cpteMp->created_us = $cpte->created_us;
            $cpteMp->save();
        }
        $this->info(date("Y-m-d H:i:s").' Fin Import Comprobantes');
        fclose($file);
    }
}
