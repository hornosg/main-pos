<?php

namespace App\Console\Commands;

use App\Models\Comprobante;
use Illuminate\Console\Command;
use DB;

class RepararEstadoNull extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reparar:estadosnull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera el estado a Cptes con Estado NULL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza el Proceso');
        $cptes = DB::select('select * from comprobantes where id_tipocpte IN (5,6) and id_estado is null');
        foreach ($cptes as $cpte){
            $this->info(date("Y-m-d H:i:s").' Reparando comprobante id:'.$cpte->id);
            $pagos = DB::select('select sum(total) as total from comprobantes where id_titular = ? and id_tipocpte in (7,13)',[$cpte->id_titular]);
            if (empty($pagos)){
                $totalpagos = 0;
            }else{
                $totalpagos = $pagos[0]->total;
            }

            if ($totalpagos > 1 ){
                $maxcpte = DB::select('SELECT MAX(id) as id FROM comprobantes c
                                                WHERE id_titular = ? AND id_tipocpte IN (5,6,3)
                                                  AND (SELECT SUM(total)
                                                         FROM comprobantes c2
                                                         JOIN comprobantes_mp cmp ON cmp.id_cpte = c2.id AND cmp.id_mediopago = 2
			                                            WHERE c2.id_titular=c.id_titular
                                                          AND c2.id_tipocpte IN (5,6,3)
                                                          AND c2.id <=c.id) < ?',[$cpte->id_titular,$totalpagos]);

                if ($cpte->id > $maxcpte[0]->id ){
                    DB::update('UPDATE comprobantes SET id_estado=2 WHERE id = ?',[$cpte->id]);
                }else{
                    DB::update('UPDATE comprobantes SET id_estado=1 WHERE id = ?',[$cpte->id]);
                }
            }
        }
    }
}
