<?php

namespace App\Console\Commands;

use App\Models\Cliente;
use Illuminate\Console\Command;
use DB;

class RepararEstadosTitular extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reparar:estados {titular?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza los estados del titular pasado por parametro';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $titular=$this->argument('titular');
        if (empty($titular)){
            $titulares = Cliente::where('id', '!=' ,2)->get();
        }else{
            $titulares = Cliente::where('id',$titular)->get();
        }

        foreach ($titulares as $cliente){
            $this->info(date("Y-m-d H:i:s").' Comienza la reparacion de estados para el titular:'. $cliente->id);
            $pagos = DB::select('select sum(total) as total
                                   from comprobantes
                                  where id_titular = ?
                                    and id_tipocpte in (7,13,14)
                                    and deleted_at is null
                                    and id_estado=3',[$cliente->id]);
            if (empty($pagos)){
                $totalpagos = 0;
            }else{
                $totalpagos = $pagos[0]->total;
            }

            if ($totalpagos > 1 ){
                $maxcpte = DB::select('SELECT MAX(c.id) as id
                                     FROM comprobantes c
                                    WHERE c.id_titular = ?
                                      AND id_tipocpte IN (5,6,3)
                                      AND c.deleted_at is null
                                      AND (SELECT SUM(total)
                                             FROM comprobantes c2
                                             JOIN comprobantes_mp cmp2 ON cmp2.id_cpte = c2.id AND cmp2.id_mediopago = 2
			                                WHERE c2.id_titular=c.id_titular
                                              AND c2.id_tipocpte IN (5,6,3)
                                              AND c2.deleted_at is null
                                              AND c2.id <=c.id) <= ?',[$cliente->id,floatval($totalpagos)]);
                DB::beginTransaction();
                if (!empty($maxcpte[0]->id)){
                    DB::update('UPDATE comprobantes c
                          JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                           SET id_estado=2
                         WHERE id_tipocpte IN (5,6,3)
                           AND c.id_titular = '.$cliente->id.'
                           AND c.id > '.$maxcpte[0]->id);
                    DB::update('UPDATE comprobantes c
                          JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                           SET id_estado=1
                         WHERE id_tipocpte IN (5,6,3)
                           AND c.id_titular = '.$cliente->id.'
                           AND c.id < '.$maxcpte[0]->id);

                    $saldomax = DB::select ('SELECT SUM(total) as saldo
                                             FROM comprobantes c
                                             JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                                            WHERE c.id_titular= ?
                                              AND c.id_tipocpte IN (5,6,3)
                                              AND c.deleted_at is null
                                              AND c.id <=?',[$cliente->id,$maxcpte[0]->id]);
                    if (floatval($saldomax[0]->saldo) > floatval($totalpagos)){
                        DB::update('UPDATE comprobantes c
                                   SET id_estado=2
                                 WHERE id = '.$maxcpte[0]->id);
                    }else{
                        DB::update('UPDATE comprobantes c
                                   SET id_estado=1
                                 WHERE id = '.$maxcpte[0]->id);
                    }
                }else{
                    DB::update('UPDATE comprobantes c
                              JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                               SET id_estado=2
                             WHERE id_tipocpte IN (5,6,3)
                               AND c.id_titular = '.$cliente->id);
                }
                DB::commit();
            }else{
                DB::update('UPDATE comprobantes c
                              JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                               SET id_estado=2
                             WHERE id_tipocpte IN (5,6,3)
                               AND c.id_titular = '.$cliente->id);
            }
        }
    }
}
