<?php

namespace App\Console\Commands;

use App\Models\Marca;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Console\Command;
use DB;

class ImportMarcas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:marcas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importacion de Marcas (Brands)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza la importacion de Marcas');
//        DB::table('conf_marcas')->truncate();

        $file_n = storage_path('marcas.csv');
        $file = fopen($file_n, "r");
        while ( ($data = fgetcsv($file, 1000, "|")) !==FALSE )
        {
            $marca = New Marca();
            $marca->descripcion = $data[1];
            $marca->activo = true;
            $marca->oldid = $data[0];
            $marca->save();
            $this->info(date("Y-m-d H:i:s").' Marca: '. $marca->oldid);
        }
        $this->info(date("Y-m-d H:i:s").' Fin Import Marcas');
        fclose($file);        
    }
}
