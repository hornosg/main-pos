<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\Titular;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Console\Command;
use DateTime;
use DB;

class ImportCodProdProv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:codprodprov';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importacion de Codigos de Productos de Proveedores';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza la importacion de Codigos de Productos de Proveedores');
//        DB::statement("SET foreign_key_checks = 0");
//        DB::table('items_cross_sell')->truncate();
//        DB::table('items_up_sell')->truncate();
        DB::table('items_proveedores')->truncate();
//        DB::table('items')->truncate();

        $file_n = storage_path('codartprov.csv');
        $file = fopen($file_n, "r");
        while ( ($data = fgetcsv($file, 1000, "|")) !==FALSE )
        {
            $this->info(date("Y-m-d H:i:s").' -> '.$data[0]);
            $item = Item::where('oldid','=',$data[0])->select('id')->first();
            if(empty($item)){
                continue;
            }
            $titular = Titular::where('oldid','=',$data[1])->where('id_tipotitular','=',3)->select('id')->first();
            if(empty($titular)){
                continue;
            }

            DB::table('items_proveedores')->insert(
                ['id_item' => $item->id,
                    'id_titular' => $titular->id,
                    'codproducto' => $data[2],
                    'actualizaprecio' => true ]
            );
        }
        $this->info(date("Y-m-d H:i:s").' Fin Import de Codigos de Productos de Proveedores');
        fclose($file);
    }
}
