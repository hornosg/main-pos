<?php

namespace App\Console\Commands;

use App\Models\Comprobante;
use App\Models\Item;
use App\Models\ComprobanteItems;
use Illuminate\Console\Command;
use DB;

class ImportCptesDetalle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:cptesdetalle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import detalle de Productos de Comprobantes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza la importacion de Comprobantes Detalle');
        DB::table('comprobantes_items')->truncate();

        $file_n = storage_path('cptesdetalle.csv');
        $file = fopen($file_n, "r");
        while ( ($data = fgetcsv($file, 1000, "|")) !==FALSE )
        {
            $cpte = Comprobante::where('oldid','=',$data[0])->select('id','created_us')->get();
            if(!empty($cpte[0])){
                $cpteitem = new ComprobanteItems();
                $cpteitem->id_cpte = $cpte[0]->id;
                $item = Item::where('oldid','=',$data[1])->select('id')->first();
                if(empty($item)){
                    continue;
                }
                $cpteitem->id_item = $item->id;
                $cpteitem->preciouni = $data[4];
                $cpteitem->cantidad = $data[2];
                $cpteitem->importe = $data[5];
                $cpteitem->created_us = $cpte[0]->created_us;
                $cpteitem->save();
            }
        }
        $this->info(date("Y-m-d H:i:s").' Fin Import Comprobantes Detalle');
        fclose($file);
    }
}
