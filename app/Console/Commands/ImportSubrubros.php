<?php

namespace App\Console\Commands;

use App\Models\Subrubro;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Console\Command;
use DB;

class ImportSubrubros extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:subrubros';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importacion de Subrubros';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza la importacion de Subrubros');
        DB::table('conf_subrubros')->truncate();

        $file_n = storage_path('subrubros.csv');
        $file = fopen($file_n, "r");
        while ( ($data = fgetcsv($file, 1000, "|")) !==FALSE )
        {
            $subrubro = New Subrubro();
            $subrubro->descripcion = $data[1];
            $subrubro->activo = true;
            $subrubro->oldid = $data[0];
            $subrubro->save();
            $this->info(date("Y-m-d H:i:s").' Subrubro: '. $subrubro->oldid);
        }
        $this->info(date("Y-m-d H:i:s").' Fin Import Subrubros');
        fclose($file);
        DB::unprepared(file_get_contents(storage_path('rubrotosubrubro.sql')));
    }
}
