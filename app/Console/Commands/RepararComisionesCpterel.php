<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;


class RepararComisionesCpterel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reparar:comisionesrel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Asigna cpterel = id del comprobotante con oldid = cpterel actual';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza la reparacion de referencias de id_cptel para comisiones...');
        DB::update('update comprobantes as c1
                      join comprobantes as c2 on c2.oldid=c1.id_cpterel
                       set c1.id_cpterel=c2.id
                     where c1.id_tipocpte=14');
        $this->info(date("Y-m-d H:i:s").' Fin del Proceso.');
    }
}
