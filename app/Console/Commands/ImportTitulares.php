<?php

namespace App\Console\Commands;

use App\Models\Localidad;
use App\Models\Titular;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use DB;

class ImportTitulares extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:titulares';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importacion de Clientes y Proveedores';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza la importacion de Clientes y Proveedores');
//        DB::statement("SET foreign_key_checks = 0");
//        DB::table('titulares')->truncate();

        $file_n = storage_path('clientes.csv');
        $file = fopen($file_n, "r");
        while ( ($data = fgetcsv($file, 1000, "|")) !==FALSE )
        {
            $titular = New Titular();
            $titular->descripcion = $data[1];
            $titular->id_tipotitular = 2;
                $titular->id_tipocliente = $data[10]>1?$data[10]:1 ;;
            switch ($data[7]){
                case 1:
                    $titular->id_tipoiva = 1;
                    break;
                case 2:
                    $titular->id_tipoiva = 6;
                    break;
                case 3:
                    $titular->id_tipoiva = 5;
                    break;
                case 4:
                    $titular->id_tipoiva = 4;
                    break;
            }
            $titular->cuit = $data[5];
            $titular->direccion = $data[3];
            $titular->id_localidad = $data[2]>1?$data[2]:null ;
            $titular->telefono = $data[4];
            $titular->email = $data[6];
            $titular->activo = true;
            $titular->oldid = $data[0];
            $titular->save();
            $this->info(date("Y-m-d H:i:s").' Cliente: '. $titular->oldid);
        }
        $this->info(date("Y-m-d H:i:s").' Fin Import Clientes');
        fclose($file);

//        $file2_n = storage_path('profesionales.csv');
//        $file2 = fopen($file2_n, "r");
//        while ( ($data = fgetcsv($file2, 1000, ";")) !==FALSE )
//        {
//            $titular = New Titular();
//            $titular->descripcion = $data[1];
//            $titular->id_tipotitular = 2;
//            $titular->id_tipocliente = 4;
//            switch ($data[7]){
//                case 1:
//                    $titular->id_tipoiva = 2;
//                    break;
//                case 2:
//                    $titular->id_tipoiva = 3;
//                    break;
//                case 3:
//                    $titular->id_tipoiva = 1;
//                    break;
//                case 4:
//                    $titular->id_tipoiva = 4;
//                    break;
//            }
//            $titular->cuit = $data[5];
//            $titular->direccion = $data[3];
//            $titular->id_localidad = $data[2]>1?$data[2]:null ;
//            $titular->telefono = $data[4];
//            $titular->email = $data[6];
//            $titular->activo = true;
//            $titular->oldid = $data[0];
//            $titular->save();
//            $this->info(date("Y-m-d H:i:s").' Profesional: '. $titular->oldid);
//        }
//        $this->info(date("Y-m-d H:i:s").' Fin Import Profesionales');
//        fclose($file2);

        $file3_n = storage_path('proveedores.csv');
        $file3 = fopen($file3_n, "r");
        while ( ($data = fgetcsv($file3, 1000, "|")) !==FALSE )
        {
            $titular = New Titular();
            $titular->descripcion = $data[1];
            $titular->id_tipotitular = 3;
            $titular->id_tipocliente = null;
            switch ($data[9]){
                case 1:
                    $titular->id_tipoiva = 2;
                    break;
                case 2:
                    $titular->id_tipoiva = 3;
                    break;
                case 3:
                    $titular->id_tipoiva = 1;
                    break;
                case 4:
                    $titular->id_tipoiva = 4;
                    break;
            }
            $titular->cuit = $data[6];
            $titular->direccion = $data[3];
            $titular->id_localidad = $data[2]>1?$data[2]:null ;
            $titular->telefono = $data[4];
            $titular->email = $data[7];
            $titular->activo = true;
            $titular->oldid = $data[0];
            $titular->save();
            $this->info(date("Y-m-d H:i:s").' Proveedor: '. $titular->oldid);
        }

        $this->info(date("Y-m-d H:i:s").' Fin Import Proveedores');
        fclose($file3);
    }
}
