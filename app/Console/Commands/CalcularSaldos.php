<?php

namespace App\Console\Commands;

use App\Models\Cliente;
use Illuminate\Console\Command;
use DB;
use PhpParser\Node\Expr\BinaryOp\Coalesce;

class CalcularSaldos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calcular:saldos {titular?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calcula los saldos previos y pariales de todos los comprobantes de ctacte';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $titular=$this->argument('titular');
        if (empty($titular)){
            $titulares = Cliente::where('id', '!=' ,2)->get();
        }else{
            $titulares = Cliente::where('id',$titular)->get();
        }

        foreach ($titulares as $cliente) {
            $this->info(date("Y-m-d H:i:s") . ' Comienza el calculo de saldos para el titular:' . $cliente->id);

            //Actualizo estado de Comisiones del Cliente
            foreach (DB::select('select * from comprobantes 
                                  where id_tipocpte=14 
                                    and deleted_at is null 
                                    and id_titular='.$cliente->id) as $comision){
                $estadordo = DB::select('select id_estado 
                                    from comprobantes c 
                                    where c.id = ?',[$comision->id_cpterel]);
                $estadocomision = ($estadordo[0]->id_estado!=2) ? 3 : 2;

                DB::update('UPDATE comprobantes c
                               SET id_estado=?
                            WHERE id=?',[$estadocomision,$comision->id]);
            }

            //Sumo los pagos
            $pagosq= DB::select('select sum(total) as total 
                                   from comprobantes c 
                                  where c.id_tipocpte in (7,13,14) 
                                    AND c.deleted_at IS NULL
                                    AND c.id_estado = 3
                                    AND c.id_titular not in (1,2)
                                    and c.id_titular = ?',[$cliente->id]);
            $pagos = floatval($pagosq[0]->total);
            $cptes = DB::select('select d.id as idcpte, id_titular, id_tipocpte, total, id_cpterel 
                                   from comprobantes d
                                   JOIN comprobantes_mp cmp ON cmp.id_cpte = d.id AND cmp.id_mediopago = 2 
                                   where (
                                              ( (d.id_tipocpte=5) AND (d.id_cpterel IS NULL) ) OR
                                              (d.id_tipocpte=6) OR
                                              ( (d.id_tipocpte=3) AND (d.id_cpterel IS NULL) ) OR
                                              ( (d.id_tipocpte=3) AND ( (SELECT id_estado FROM comprobantes WHERE id_tipocpte=14 AND id=d.id_cpterel)=3 ) )
                                          )
                                     AND d.deleted_at IS NULL
                                     AND d.id_titular not in (1,2)
                                     AND d.id_titular = ?                                    
                                   UNION 
                                   select h.id as idcpte, id_titular, id_tipocpte, total, id_cpterel  
                                   from comprobantes h 
                                   where h.id_tipocpte IN (7,13,14)
                                     AND h.deleted_at IS NULL
                                     AND h.id_estado = 3
                                     AND h.id_titular not in (1,2)                                          
                                     AND h.id_titular = ?                                    
                                   order by idcpte desc',[$cliente->id,$cliente->id]);
            foreach ($cptes as $cpte) {

                //Para no hacer el sum sobre la view
                //$stotal =  DB::select('select sum(cc.saldo) as saldototal  from ctacte_view cc where cc.id_titular = ? and cc.id <= ?',[$cpte->id_titular,$cpte->idcpte]);
                $stotal =  DB::select('select sum(total) as saldo 
                                   from comprobantes d
                                   JOIN comprobantes_mp cmp ON cmp.id_cpte = d.id AND cmp.id_mediopago = 2 
                                   where (
                                              ( (d.id_tipocpte=5) AND (d.id_cpterel IS NULL) ) OR
                                              (d.id_tipocpte=6) OR
                                              ( (d.id_tipocpte=3) AND (d.id_cpterel IS NULL) ) OR
                                              ( (d.id_tipocpte=3) AND ( (SELECT id_estado FROM comprobantes WHERE id_tipocpte=14 AND id=d.id_cpterel)=3 ) )
                                          )
                                     AND d.deleted_at IS NULL
                                     AND d.id_titular not in (1,2)
                                     AND d.id_titular = ?
                                     and d.id <= ?                                    
                                   UNION 
                                   select sum(total * (-1)) as saldo 
                                   from comprobantes h 
                                   where h.id_tipocpte IN (7,13,14)
                                     AND h.deleted_at IS NULL
                                     AND h.id_estado = 3
                                     AND h.id_titular not in (1,2)                                          
                                     AND h.id_titular = ?
                                     and h.id <= ?',[$cliente->id,$cpte->idcpte,$cliente->id,$cpte->idcpte]);
                $saldototal = (count($stotal)==1) ?  $stotal[0]->saldo : $stotal[0]->saldo+$stotal[1]->saldo;


                if (($cpte->id_tipocpte==5) || ($cpte->id_tipocpte==6) || ($cpte->id_tipocpte==3)){
                    if ((!empty($pagos)) and ($pagos>0)){

                        $sdebe =  DB::select('select SUM(total) as saldodebe
                                        from comprobantes c 
                                        JOIN comprobantes_mp cmp ON cmp.id_cpte = c.id AND cmp.id_mediopago = 2
                                        where (
                                                (
                                                  ( (c.id_tipocpte=5) AND (c.id_cpterel IS NULL) ) OR
                                                  (c.id_tipocpte=6) OR
                                                  ( (c.id_tipocpte=3) AND (c.id_cpterel IS NULL) ) OR
                                                  ( (c.id_tipocpte=3) AND ( (SELECT id_estado FROM comprobantes WHERE id_tipocpte=14 AND id=c.id_cpterel)=3 ) )
                                                )
                                                AND c.deleted_at IS NULL
                                                AND c.id_titular not in (1,2)                                   
                                              ) 
                                            and c.id_titular = ? 
                                            and c.id <= ?', [$cpte->id_titular,$cpte->idcpte]);
                        $saldodebe = $sdebe[0]->saldodebe;


                        if ($pagos>=$saldodebe){
                            $saldocpte = 0;
                        }else{
                            $saldocpte =  ($saldodebe-$cpte->total > $pagos) ? $cpte->total : $saldodebe-$pagos;
                        }

                    }else{
                        $saldocpte = $cpte->total;
                    }
                    $idestado = ($saldocpte>0) ? 2 : 1;
                }else{
                    $saldocpte = 0;
                    $idestado = 3;
                }
                if ($cpte->id_tipocpte == 14) {
                    $estadorel = DB::select('select id_estado 
                                        from comprobantes c 
                                        where c.id = ?',[$cpte->id_cpterel]);
                    $idestado = ($estadorel[0]->id_estado!=2) ? 3 : 2;
                }

                DB::update('UPDATE comprobantes c
                               SET saldototal=?, saldocpte=?, id_estado=?
                            WHERE id=?',[$saldototal,$saldocpte,$idestado,$cpte->idcpte]);
            }
            //DB::commit();
        }
    }
}
