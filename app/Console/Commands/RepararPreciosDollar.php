<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\Subrubro;
use App\Models\Marca;
use App\Models\Titular;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Console\Command;
use DateTime;
use DB;

class RepararPreciosDollar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reparar:preciosdolar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importacion de Productos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza la reparacion de Precios');
        $file_n = storage_path('productos.csv');
        $file = fopen($file_n, "r");
        while ( ($data = fgetcsv($file, 1000, "|")) !==FALSE )
        {
            $this->info(date("Y-m-d H:i:s").' -> '.$data[0]);
            $producto = Item::where('oldid',$data[0])->first();
            switch ($data[4]){
                case 1:
                    $producto->id_unidadcontneto = 15;
                    break;
                case 2:
                    $producto->id_unidadcontneto = 6;
                    break;
                case 3:
                    $producto->id_unidadcontneto = 4;
                    break;
                case 4:
                    $producto->id_unidadcontneto = 12;
                    break;
                case 5:
                    $producto->id_unidadcontneto = 13;
                    break;
                case 6:
                    $producto->id_unidadcontneto = 3;
                    break;
                case 7:
                    $producto->id_moneda = 2;
                    break;
                case 8:
                    $producto->id_unidadcontneto = 2;
                    break;
                case 9:
                    $producto->id_unidadcontneto = 1;
                    break;
                case 15:
                    $producto->id_unidadcontneto = 14;
                    break;
            }

            if ($producto->id_moneda == 2){
                $producto->precioLista = floatval($data[6])==0?1:floatval($data[6]);
                $preciodollar = $producto->precioLista * 37.5;
                $producto->iva = abs((((floatval($data[7])*$preciodollar)-$preciodollar)*100)/$preciodollar);
                $producto->bonifProv = abs((((floatval($data[8])*($preciodollar + (($preciodollar*$producto->iva)/100)))-($preciodollar+(($preciodollar*$producto->iva)/100)))*100)/($preciodollar + (($preciodollar*$producto->iva)/100)));
                $producto->precioCosto = $preciodollar + (($preciodollar*$producto->iva)/100) - ((($preciodollar + (($preciodollar*$producto->iva)/100))*$producto->bonifProv)/100);
                $producto->ganancia = abs((((floatval($data[9])*floatval($data[11])*$producto->precioCosto)-$producto->precioCosto)*100)/$producto->precioCosto);
                $producto->precio = $producto->precioCosto + (($producto->precioCosto*$producto->ganancia)/100);
            }
            $producto->save();
        }
        $this->info(date("Y-m-d H:i:s").' Fin Reparacion de Precios Productos');
        fclose($file);
    }
}
