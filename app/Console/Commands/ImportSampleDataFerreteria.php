<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\Subrubro;
use App\Models\Rubro;
use App\Models\Marca;
use App\Models\UnidadMedida;
use Illuminate\Console\Command;
use DB;

class ImportSampleDataFerreteria extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:sampledataferreteria';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importacion de Datos de Ejemplo de Ferretería';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza la importacion de Ferreteria');

        $file_n = storage_path('SampleDataFerreteria.csv');
        $file = fopen($file_n, "r");
        while ( ($data = fgetcsv($file, 1000, "|")) !==FALSE )
        {
            $this->info(date("Y-m-d H:i:s").' -> '.$data[3]);
            $producto = New Item();
            $producto->descripcion = $data[3];
            $producto->id_tipoitem = 5;
            $producto->contNeto = $data[4];

            //Unidad de Medida
            $unidadcontneto = UnidadMedida::where('descripcion','=',$data[5])->select('id')->first();
            if(!empty($unidadcontneto)){
                $producto->id_unidadcontneto = $unidadcontneto->id;
            }

            //Unidad de Medida de Venta
            $unidadventa = UnidadMedida::where('descripcion','=',$data[6])->select('id')->first();
            if(!empty($unidadventa)){
                $producto->id_unidadventa = $unidadventa->id;
            }
            $producto->id_moneda = 1;

            $producto->ean = null;
            $producto->peso = null;
            $producto->largo = null;
            $producto->ancho = null;
            $producto->alto = null;

            //Marca
            $marca = Marca::where('descripcion','=',$data[0])->select('id')->first();
            if(!empty($marca)){
                $producto->id_marca = $marca->id;
            }else{
                $marca = New Marca();
                $marca->descripcion = $data[0];
                $marca->activo = true;
                $marca->save();
                $producto->id_marca = $marca->id;
            }

            //Rubro
            $rubro = Rubro::where('descripcion','=',$data[1])->select('id')->first();
            if(!empty($rubro)){
                $producto->id_rubro = $rubro->id;
            }else{
                $rubro = New Rubro();
                $rubro->descripcion = $data[1];
                $rubro->activo = true;
                $rubro->save();
                $producto->id_rubro = $rubro->id;
            }

            //Subrubro
            $subrubro = Subrubro::where('descripcion','=',$data[2])->select('id')->first();
            if(!empty($subrubro)){
                $producto->id_subrubro = $subrubro->id;
            }else{
                $subrubro = New Subrubro();
                $subrubro->descripcion = $data[2];
                $subrubro->activo = true;
                $subrubro->save();
                $producto->id_subrubro = $subrubro->id;
            }

            $producto->id_linea = null;
            $producto->id_material = null;
            $producto->idOrigen = null;
            $producto->stockMinimo = null;

            $producto->id_moneda = 1;
            $producto->precioLista = 0.99;
            $producto->iva = 21;
            $producto->divisor = 1;
            $producto->bonifProv = 50;
            $producto->precioCosto = $producto->precioLista + (($producto->precioLista*$producto->iva)/100) - ((($producto->precioLista + (($producto->precioLista*$producto->iva)/100))*$producto->bonifProv)/100);
            $producto->ganancia = 50;
            $producto->precio = $producto->precioCosto + (($producto->precioCosto*$producto->ganancia)/100);

            $producto->distribuye = false;
            $producto->activo = true;
            $producto->oldid = null;
            $producto->save();
        }
        $this->info(date("Y-m-d H:i:s").' Fin Import Sample Data Ferreteria');
        fclose($file);
    }
}
