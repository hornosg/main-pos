<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\Subrubro;
use App\Models\Marca;
use App\Models\Titular;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Console\Command;
use DateTime;
use DB;

class ReCalcularPrecios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recalcular:precios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalcular precios';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza la reparacion de Precios');


        DB::update('UPDATE items AS i
                      JOIN log_precios_items lp ON lp.id_item = i.id
                       SET i.precioLista = lp.oldprecioLista
                     WHERE  date(lp.created_at) = date(current_date)');

        $productos = Item::where('id_tipoitem', '=' ,5)->get();
        foreach ($productos as $producto){
            $this->info(date("Y-m-d H:i:s").$producto->id);
            if ($producto->id_moneda == 2){
                $preciodollar = $producto->precioLista * 42;
                $producto->bonifProv = $producto->bonifProv/42;
                $producto->precioCosto = $preciodollar + (($preciodollar*$producto->iva)/100) - ((($preciodollar + (($preciodollar*$producto->iva)/100))*$producto->bonifProv)/100);
                if ($producto->divisor>1){
                    $producto->precioCosto = $producto->precioCosto / $producto->divisor;
                }
                $producto->precio = $producto->precioCosto + (($producto->precioCosto*$producto->ganancia)/100);
                $producto->save();
            }else{
                $producto->precioCosto = $producto->precioLista + (($producto->precioLista*$producto->iva)/100) - ((($producto->precioLista + (($producto->precioLista*$producto->iva)/100))*$producto->bonifProv)/100);
                if ($producto->divisor>1){
                    $producto->precioCosto = $producto->precioCosto / $producto->divisor;
                }
                $producto->precio = $producto->precioCosto + (($producto->precioCosto*$producto->ganancia)/100);
                $producto->save();
            }

        }
        $this->info(date("Y-m-d H:i:s").' Fin Reparacion de Precios Productos');
    }
}
