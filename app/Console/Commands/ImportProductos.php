<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\Subrubro;
use App\Models\Marca;
use App\Models\Titular;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Console\Command;
use DateTime;
use DB;

class ImportProductos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:productos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importacion de Productos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").' Comienza la importacion de Productos');
        DB::statement("SET foreign_key_checks = 0");
        DB::table('items_cross_sell')->truncate();
        DB::table('items_up_sell')->truncate();
        DB::table('items_proveedores')->truncate();
        DB::table('items')->truncate();

        $file_n = storage_path('productos.csv');
        $file = fopen($file_n, "r");
        while ( ($data = fgetcsv($file, 1000, "|")) !==FALSE )
        {
            $this->info(date("Y-m-d H:i:s").' -> '.$data[0]);
            $producto = New Item();
            $producto->descripcion = $data[2];
            $producto->id_tipoitem = 5;
            $producto->ean = $data[1];

            $producto->id_unidadventa = 1;
            $producto->contNeto = $data[5];
            $producto->id_moneda = 1;
            switch ($data[4]){
                case 1:
                    $producto->id_unidadcontneto = 15;
                    break;
                case 2:
                    $producto->id_unidadcontneto = 6;
                    break;
                case 3:
                    $producto->id_unidadcontneto = 4;
                    break;
                case 4:
                    $producto->id_unidadcontneto = 12;
                    break;
                case 5:
                    $producto->id_unidadcontneto = 13;
                    break;
                case 6:
                    $producto->id_unidadcontneto = 3;
                    break;
                case 7:
                    $producto->id_unidadcontneto = 7;
                    break;
                case 8:
                    $producto->id_unidadcontneto = 2;
                    break;
                case 9:
                    $producto->id_unidadcontneto = 1;
                    break;
                case 15:
                    $producto->id_unidadcontneto = 14;
                    break;
            }

            $producto->peso = null;
            $producto->largo = null;
            $producto->ancho = null;
            $producto->alto = null;

            $marca = Marca::where('oldid','=',$data[10])->select('id')->first();
            if(empty($marca)){
                $producto->id_marca = null;
            }else{
                $producto->id_marca = $marca->id;
            }

            $subrubro = Subrubro::where('oldid','=',$data[3])->first();
            if(empty($subrubro)){
                $producto->id_rubro = null;
                $producto->id_subrubro = null;
            }else{
                $producto->id_rubro = $subrubro->id_rubro;
                $producto->id_subrubro = $subrubro->id;
            }

            $producto->id_linea = null;
            $producto->id_material = null;
            $producto->idOrigen = null;

            $producto->stockMinimo = $data[13];

//            if ($producto->id_moneda == 2){
//                $producto->precioLista = floatval($data[6])==0?1:floatval($data[6]);
//                $preciodollar = $producto->precioLista * 40;
//                $producto->iva = $data[7];
//                if (floatval($data[8]) > 1){
//                    $producto->bonifProv = abs(((((floatval($data[8]/38))*($preciodollar + (($preciodollar*$producto->iva)/100)))-($preciodollar+(($preciodollar*$producto->iva)/100)))*100)/($preciodollar + (($preciodollar*$producto->iva)/100)));
//                }else{
//                    $producto->bonifProv = abs((((floatval($data[8])*($producto->precioLista + (($producto->precioLista*$producto->iva)/100)))-($producto->precioLista+(($producto->precioLista*$producto->iva)/100)))*100)/($producto->precioLista + (($producto->precioLista*$producto->iva)/100)));
//                }
//                $producto->precioCosto = $preciodollar + (($preciodollar*$producto->iva)/100) - ((($preciodollar + (($preciodollar*$producto->iva)/100))*$producto->bonifProv)/100);
//                $producto->ganancia = $data[9];
//                $producto->precio = $producto->precioCosto + (($producto->precioCosto*$producto->ganancia)/100);
//            }else{
                $producto->precioLista = floatval($data[6])==0?1:floatval($data[6]);
                $producto->iva = $data[7];
                $producto->divisor = $data[15];
                if (floatval($data[8]) > 0){
                    $producto->bonifProv = floatval($data[8])*100;
                    $producto->precioCosto = $producto->precioLista + (($producto->precioLista*$producto->iva)/100) - ((($producto->precioLista + (($producto->precioLista*$producto->iva)/100))*$producto->bonifProv)/100);
                }else{
                    $producto->bonifProv=0;
                    $producto->precioCosto = $producto->precioLista + (($producto->precioLista*$producto->iva)/100);
                }
                $producto->divisor = $data[15];
                if ($producto->divisor>1){
                    $producto->precioCosto = $producto->precioCosto / $producto->divisor;
                }
                $producto->ganancia = $data[9];
                $producto->precio = $producto->precioCosto + (($producto->precioCosto*$producto->ganancia)/100);
//            }


            $producto->distribuye = $data[12]=='t'?true:false;

            $producto->activo = true;
            $producto->oldid = $data[0];
            $producto->save();

//            $codigo = explode( '-', $data[1],2);
//            $codprov = ltrim($codigo[0], 'P');
//            $proveedor = Titular::where('oldid','=',intval($codprov))->where('id_tipotitular','=',3)->first();
//            if(!empty($proveedor)){
//                if(!empty($codigo[1])){
//                    $codprodprov = $codigo[1];
//                    DB::table('items_proveedores')->insert(
//                        ['id_item' => $producto->id, 'id_titular' => $proveedor->id, 'codproducto' => $codprodprov ]
//                    );
//                }
//            }
        }
        $this->info(date("Y-m-d H:i:s").' Fin Import Productos');
        fclose($file);
    }
}
