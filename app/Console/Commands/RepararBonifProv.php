<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\Subrubro;
use App\Models\Marca;
use App\Models\Titular;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Console\Command;
use DateTime;
use DB;

class RepararBonifProv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reparar:bonifprov';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importacion de Productos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date("Y-m-d H:i:s").'Comienza Reparacion de Precios Productos');
        $productos = Item::where('bonifProv', '>' ,100)->get();
        foreach ($productos as $producto){
            $this->info(date("Y-m-d H:i:s").$producto->id);
            $producto->id_moneda = 2;
            $preciodollar = $producto->precioLista * 38;
            $producto->bonifProv = $producto->bonifProv/38;
            $producto->precioCosto = $preciodollar + (($preciodollar*$producto->iva)/100) - ((($preciodollar + (($preciodollar*$producto->iva)/100))*$producto->bonifProv)/100);
            $producto->precio = $producto->precioCosto + (($producto->precioCosto*$producto->ganancia)/100);
            $producto->save();
        }
        $this->info(date("Y-m-d H:i:s").' Fin Reparacion de Precios Productos');
    }
}
