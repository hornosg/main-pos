@extends('adminlte::page')
@section('title', 'MainPOS')

@php
    if(empty($totalvtas)){
        $totalvtas=0;
        $efectivo=0;
        $ctacte=0;
        $tarjetas=0;
        $otrosmp=0;
        $porc_efectivo=0;
        $porc_ctacte=0;
        $porc_tarjetas=0;
        $porc_otrosmp=0;
    }
    $itemnav='';
@endphp


@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<style>
    .info-box{
        min-height: 67px !important;
        margin-bottom: 13px !important;
    }
    .info-box-icon{
        height:67px !important;
        line-height: 67px !important;
        width: 60px !important;
    }
    .info-box-text, .info-box-number{
        display: inline !important;
    }
    .info-box-number {
        font-size: 16px !important;
    }
    .info-box-content{
        padding: 5px 6px
    }
    .bootstrap-table {
        padding-bottom: 0 !important;
    }
    .pt0{
        padding-top: 0 !important;
    }
    .pt10{
        padding-top: 10px !important;
    }
    .pb10{
        padding-bottom: 10px !important;
    }
</style>
@stop

@section('content_header')
    @include('layouts.usersnavbar')
@stop

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="graph-container bg-white">
<!--                <div id="toolbar">-->
                    <div class="caption pb10">Cantidad de Comprobantes diarios en Promedio del Mes</div>
<!--                </div>-->
                <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                       data-toggle='table'
                       data-toolbar="#toolbar">
                    <thead class='thead-inverse'>
                    <th data-field='usuario' class='text-uppercase'>Usuario</th>
                    <th data-field='promedio' data-align='right' >CANTIDAD PROMEDIO</th>
                    </thead>
                </table>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
            <div class="graph-container bg-white">
                <div class="caption">Cantidad de Comprobantes diarios en Promedio Mes a Mes</div>
                <div id="columnchart_material" class="graph mt-20"></div>
<!--                <div id="columnchart_material" style="width: 800px; height: 500px;"></div>-->
            </div>
        </div>
    </div>
@stop

@section('js')
<!-- Bootstrap-Table -->
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<!-- Morris Chart JS-->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<!-- Google Charts -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script language="javascript" type="text/javascript">
    var datos = <?php echo json_encode($cptesxus); ?>;
    var $table = $('#table1');
    $table.bootstrapTable({data: datos });

    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Year', 'Sales', 'Expenses', 'Profit','Peter'],
            ['2014', 1000, 400, 200, 1212],
            ['2015', 1170, 460, 250, 1212],
            ['2016', 660, 1120, 300, 1212],
            ['2017', 1030, 540, 350, 1212],
            ['2018', 1030, 540, 350, 1212],
            ['2019', 1030, 540, 350, 1212]
        ]);
        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
        chart.draw(data, google.charts.Bar.convertOptions());
    }

</script>
@stop