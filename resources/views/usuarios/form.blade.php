@extends('adminlte::page')

@section('title', $titulo)

@php
    if ($edit==0){
        $editable='readonly';
        $visible='d-none';
        $disabled='disabled';
    }else{
        $editable=' ';
        $visible=' ';
        $disabled=' ';
    }
    if (!empty($item)){
        $checked = $item->activo == 1 ? 'checked':'';
    }else{
        $checked = 'checked';
    }
@endphp

@section('content_header')
    @include('layouts.usersnavbar')
@stop

@section('content')
<div id="content" class="container-fluid row">
    <form id="form1" role="form" method="POST" action="/{{$urlkey}}/store">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-primary">
                {{ csrf_field() }}
                <div class="box-body">
                    @if (!empty($item))
                        <input type = "hidden" name = "id" value = "{{$item->id}}">
                    @else
                        <input type = "hidden" name = "id">
                    @endif

                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-4 col-md-4">
                        <label for="name" class="control-label">Username</label>
                        @if (!empty($item))
                            <input id="name" type="text" class="form-control input-sm" name="name" value="{{ $item->name }}" {{$editable}}>
                        @else
                            <input id="name" type="text" class="form-control input-sm" name="name" value="" {{$editable}}>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('full_name') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-4 col-md-4">
                        <label for="full_name" class="control-label">Nombre y Apellido</label>
                        @if (!empty($item))
                        <input id="full_name" type="text" class="form-control input-sm text-uppercase" name="full_name" value="{{ $item->full_name }}" {{$editable}}>
                        @else
                        <input id="full_name" type="text" class="form-control input-sm text-uppercase" name="full_name" value="" {{$editable}}>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-4 col-md-4">
                        <label for="email" class="control-label">Email</label>
                        @if (!empty($item))
                        <input id="email" type="text" class="form-control input-sm" name="email" value="{{ $item->email }}" {{$editable}}>
                        @else
                        <input id="email" type="text" class="form-control input-sm" name="email" value="" {{$editable}}>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('id_rol') ? ' has-error' : '' }} col-md-4">
                        <label for="id_rol" class="control-label">Rol</label>
                        <select id="id_rol" name="id_rol" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @if (!empty($item))
                                @foreach(DB::table('sys_roles')->where('activo', 1)->get() as $rol)
                                    @if ( $item->id_rol == $rol->id )
                                        <option value="{{ $rol->id }}" selected>{{ $rol->descripcion }}</option>
                                    @else
                                        <option value="{{$rol->id}}">{{$rol->descripcion}}</option>
                                    @endif
                                @endforeach
                            @else
                                @foreach(DB::table('sys_roles')->where('activo', 1)->get() as $rol)
                                    <option value="{{$rol->id}}">{{$rol->descripcion}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group {{ $errors->has('id_sucursal') ? ' has-error' : '' }} col-md-4">
                        <label for="id_sucursal" class="control-label">Sucursal</label>
                        <select id="id_sucursal" name="id_sucursal" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @if (!empty($item))
                                @foreach(DB::table('sucursales')->where('activo', 1)->get() as $sucursal)
                                    @if ( $item->id_sucursal == $sucursal->id )
                                        <option value="{{ $sucursal->id }}" selected>{{ $sucursal->descripcion }}</option>
                                    @else
                                        <option value="{{$sucursal->id}}">{{$sucursal->descripcion}}</option>
                                    @endif
                                @endforeach
                            @else
                                @foreach(DB::table('sucursales')->where('activo', 1)->get() as $sucursal)
                                    <option value="{{$sucursal->id}}">{{$sucursal->descripcion}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="clearfix visible-lg-block visible-md-block"></div>
                    @if (!empty($item))
                        <div class="form-group col-sm-6 col-md-4 col-md-3">
                            <label for="created_at" class="control-label">Fecha Alta</label>
                            <input id="created_at" type="text" class="form-control input-sm text-uppercase" name="created_at" readonly value="{{ $item->created_at }}">
                        </div>

                        <div class="form-group col-sm-6 col-md-6 col-md-3">
                            <label for="updated_at" class="control-label">Fecha Ultima Edicion</label>
                            <input id="updated_at" type="text" class="form-control input-sm text-uppercase" name="updated_at" readonly value="{{ $item->updated_at }}">
                        </div>
                    @endif
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        @if ($edit==1)
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                            <button type="submit" name="action" value="save" class="btn btn-primary">
                                <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            </button>
                        @else
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type='text/javascript'>
    $(function () {
        $('[data-toggle="popover"]').popover();
    });
    $('#id_rol').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_sucursal').select2({
        placeholder: "",
        allowClear: true
    });
    $(window).on('load', function(event){
        document.getElementById("descripcion").focus();
    });
</script>
@include('utils.statusnotification')
@stop
