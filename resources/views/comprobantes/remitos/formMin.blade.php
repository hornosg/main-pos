@extends('adminlte::page')

@section('title', $titulo)

@php
if ($edit==0){
    $editable='readonly';
    $visible='d-none';
    $disabled='disabled';
    $accion = 'Visualizar';
}else{
    $editable=' ';
    $visible=' ';
    $disabled=' ';
    $accion = 'Editar';
}

if (!empty($prods)){
    $total = 0;
    $datos = json_encode($prods);
}else{
    $datos = "[]";
    $total = 0;
}

if (!empty($descints)){
    //dd($descints);
    $totaldi = 0;
    $datosdi = json_encode($descints);
    foreach ($descints as $descint){
        $totaldi = $totaldi + $descint->importe;
    }
}else{
    $datosdi = "[]";
    $totaldi = 0;
}

if (!empty($mps)){
    $totalmp = 0;
    $datosmp = json_encode($mps);
    foreach ($mps as $mp){
        $totalmp = $totalmp + $mp->importe;
    }
}else{
    $datosmp = "[]";
    $totalmp = 0;
}

if (empty($cpte)){
    $totalcpte=0;
    $referente=0;
}else{
    $totalcpte=$cpte->total;
    $referente=$cpte->id_referente;
}
@endphp

@section('content_header')
    @include('layouts.ventasnavbar')
@stop

@section('content')
<div id="content" class="container-fluid row">
    <div class="col-md-12" style="padding-left: 0 !important;padding-right: 0 !important;">
        <div class="col-md-6" style="padding-left: 0 !important;padding-right: 0 !important;">
            <div class="box box-primary">
                <div id="toolbar">
                    <a class='text-black' href='#' data-toggle='modal' data-target='#prodModal' title='Crear Nuevo Producto'><i class='fa fa-fw  fa-plus-circle'></i></a><span>Productos Disponibles</span>
                </div>
                <table id='table1p' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                       data-toggle='table'
                       data-toolbar="#toolbar"
                       data-search='true'
                       data-trim-on-search="false"
                       data-strict-search="false"
                       data-multiple-search='true'
                       data-search-align="right"
                       data-height="330"
                       data-pagination='false'>

                    <thead class='thead-inverse'>
                    <th data-field='ean' class='hidden'>Ean</th>
                    <th data-field='id' data-align='right' class='hidden text-uppercase'>Id</th>
                    <th data-field='marca' class='text-nowrap col-md-2 text-uppercase'>Marca</th>
                    <th data-field='producto' class='text-uppercase col-md-7'>Descripcion</th>
                    <th data-field='rubro'   class='hidden text-nowrap text-uppercase'>Rubro</th>
                    <th data-field='subrubro'   class='hidden text-nowrap text-uppercase'>subRubro</th>
                    {{--<th data-field='contenidoNeto' data-align='right' class='text-uppercase'>C.Neto</th>--}}
                    <th data-field='precio'  data-align='right' class='text-nowrap' data-formatter="moneyFormatter">PRECIO</th>
                    <th data-align='right' class='form-inline p0' data-formatter='FunctionsFormatter2'></th>
                    </thead>
                </table>
            </div>
        </div>
        <form id="form1" class="form-inline" role="form" method="POST" action="/cptes/store/rem" enctype="multipart/form-data">
        <div class="col-md-6" style="padding-right: 0 !important;">
            <div class="col-md-12" style="padding-left: 0 !important;padding-right: 0 !important;">
                <div class="box box-primary" style="margin-bottom: 0 !important;">
                    {{--<div class="box-header with-border">--}}
                        {{--<h3 class="box-title">--}}
                            {{--<strong>{{$tituloform}} N°</strong>--}}
                            {{--@if (!empty($cpte))--}}
                                {{--{{$cpte->numero}}--}}
                            {{--@endif--}}
                        {{--</h3>--}}
                    {{--</div>--}}
                    <div class="box-body">
                        {{ csrf_field() }}

                        @if (!empty($cpte))
                            <input id="id" type = "hidden" name = "id" value = "{{$cpte->id}}">
                        @else
                            <input type = "hidden" name = "id">
                        @endif

                        <div class="form-group col-md-4">
                            <label for="id_titular" class="control-label">Cliente</label>
                            <select id="id_titular" name="id_titular" class="form-control text-uppercase" {{$disabled}}>
                                <option value=""></option>
                                @if (!empty($cpte))
                                    @foreach(DB::table('titulares')->where('id_tipotitular', 2)->where('activo', 1)->get() as $tipotitular)
                                        @if ( $cpte->id_titular == $tipotitular->id )
                                            <option value="{{ $tipotitular->id }}" selected>{{ $tipotitular->descripcion }}</option>
                                        @else
                                            <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                        @endif
                                    @endforeach
                                @else
                                    @foreach(DB::table('titulares')->where('id_tipotitular', 2)->where('activo', 1)->get() as $tipotitular)
                                        @if (env('MP_CLIENTE')==$tipotitular->id)
                                            <option value="{{$tipotitular->id}}" selected>{{$tipotitular->descripcion}}</option>
                                        @else
                                            <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        {{--@if (!empty(env('MP_COMISION')))--}}
                            {{--<div class="form-group col-md-12">--}}
                                {{--<label for="id_referente" class="control-label">Profesional / Referente</label><div id="loading2" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>--}}
                                {{--<select id="id_referente" name="id_referente" class="form-control text-uppercase" {{$disabled}}>--}}
                                    {{--<option value=""></option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                        @if ($edit==1)
                            <div class="form-inline col-md-8">
                                <label for="recargo" class="control-label col-md-12">Aj Precios</label>
                                <div class="form-check form-check-inline col-md-4">
                                    <input class="form-check-input" type="radio" name="id_ajuste" id="0" value="">
                                    <label class="form-check-label" for="ajuste0">Ninguno</label>
                                </div>
                                @foreach(DB::table('conf_ajustes_precio')->where('activo', 1)->get() as $ajuste)
                                    @if (!empty($cpte))
                                        @if ( $cpte->id_ajuste == $ajuste->id )
                                            <div class="form-check form-check-inline col-md-2">
                                                <input class="form-check-input" type="radio" name="id_ajuste" id="ajuste{{$ajuste->id}}" value="{{$ajuste->id}}" checked>
                                                <label class="form-check-label" for="ajuste{{$ajuste->id}}">{{$ajuste->descripcion}}
                                                    <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="{{$ajuste->nota}} ({{$ajuste->porcentaje*$ajuste->signo}}%)">
                                                        <i class="fa fa-info-circle"></i>
                                                    </a>
                                                </label>
                                            </div>
                                        @else
                                            <div class="form-check form-check-inline col-md-2">
                                                <input class="form-check-input" type="radio" name="id_ajuste" id="ajuste{{$ajuste->id}}" value="{{$ajuste->id}}">
                                                <label class="form-check-label" for="ajuste{{$ajuste->id}}">{{$ajuste->descripcion}}
                                                    <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="{{$ajuste->nota}} ({{$ajuste->porcentaje*$ajuste->signo}}%)">
                                                        <i class="fa fa-info-circle"></i>
                                                    </a>
                                                </label>
                                            </div>
                                        @endif
                                    @else
                                        <div class="form-check form-check-inline col-md-4">
                                            <input class="form-check-input" type="radio" name="id_ajuste" id="ajuste{{$ajuste->id}}" value="{{$ajuste->id}}">
                                            <label class="form-check-label" for="ajuste{{$ajuste->id}}">{{$ajuste->descripcion}}
                                                <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="{{$ajuste->nota}} ({{$ajuste->porcentaje*$ajuste->signo}}%)">
                                                    <i class="fa fa-info-circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif

                        @if (!empty($cpte))
                            <fieldset class="form-group col-md-3">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Alta]</label>
                                @php $uscr=DB::table('users')->where('id', $cpte->created_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{  $uscr->name }} {{ date_format($cpte->created_at, 'd/m/Y H:i:s') }}" readonly>
                            </fieldset>
                            <fieldset class="form-group col-md-3">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Modificacion]</label>
                                @php $usup=DB::table('users')->where('id', $cpte->updated_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{ $usup->name }} {{ date_format($cpte->updated_at, 'd/m/Y H:i:s') }}" readonly>
                            </fieldset>
                            @if ($edit==1)
                                <fieldset class="form-group col-md-6">
                                    <label for="saldoctecte" class="control-label">SALDO VENCIDO</label>
                                    <input id="saldoctecte" type="text" class="form-control" name="saldoctecte" value="0" readonly>
                                </fieldset>
                            @endif
                        @else
                            <div class="col-md-6">
                                <div class="mt-10 text-bold info-box bg-gray hidden">
                                    <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">SALDO VENCIDO</span>
                                        <span class="info-box-number saldo"></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="padding-left: 0 !important;padding-right: 0 !important;">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>{{$titulodetart}}</strong></h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                       data-toggle='table'
                                       data-total-field='total'
                                       data-unique-id='id_item'>
                                    <thead class='thead-inverse'>
                                    <th data-formatter="runningFormatter"></th>
                                    <th data-field='id' data-align='right' class='text-uppercase hidden'></th>
                                    <th data-field='id_item' data-align='right' class='hidden text-uppercase'>IdItem</th>
                                    <th data-field='producto' class='text-nowrap text-uppercase col-md-6'>Descripcion</th>
                                    @if ($edit==1)
                                        <th data-field='origen' class='text-uppercase col-md-1'>Orig. Stk</th>
                                    @endif
                                    <th data-field='precio' data-align='right' class='col-md-2' data-formatter="moneyFormatter">PRECIO</th>
                                    <th data-field='cantidad' data-align='right' class='text-uppercase col-md-1'>Cant</th>
                                    <th data-field='total'  data-align='right' class='col-md-2' data-formatter="moneyFormatter">TOTAL</th>
                                    @if ($edit==1)
                                        <th data-align='center' data-formatter='FunctionsFormatter'></th>
                                    @endif
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        @if ($edit==1)
                                            <td></td>
                                        @endif
                                        <th>$ <span data-align='right' class='cltotal pull-right'></span></th>
                                        @if ($edit==1)
                                            <td></td>
                                        @endif
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer pb-10 ">
                        <div class="pull-right pr-10">
                        <!--                        @if ($edit==1)-->
                            <!--                        @else-->
                            <!--                        @endif-->
                            <a id="pagosvarios" class='btn btn-warning d-none' href='#'  onclick="OtroMP()" title='Shift + W'>Agregar Medio de Pago</i></a>
                            <a id="pagoctacte" class="btn btn-warning d-none" href="#" onclick="UnSoloPago(2)" title='Shift + Z'><i class="fa fa-btn fa-sort-amount-desc text-white"></i> Registrar en Cta Cte</a>
                            <a id="pagoefectivo" class="btn btn-success d-none" href="#" onclick="UnSoloPago(1)" title='Shift + E'><i class="fa fa-btn fa-thumbs-o-up text-white"></i> Total en Efectivo</a>
                        </div>
                    </div>
                </div>
                @if ($edit==1)
                    <div id="sugerencias" class="box box-primary collapsed-box d-none">
                        <div class="box-header with-border">
                            <h3 class="box-title"><strong>Sugerencias</strong></h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <table id='table2' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                           data-toggle='table'
                                           data-total-field='total'
                                           data-unique-id='id'>
                                        <thead class='thead-inverse'>
                                        <th data-field='id' data-align='right' class='text-uppercase col-md-1'>Id</th>
                                        <th data-field='producto' class='text-uppercase col-md-10'>Descripcion</th>
                                        <th data-align='center' data-formatter='FunctionsFormatter2'></th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-9" style="padding-left: 0 !important;padding-right: 0 !important;">
        <div id="mediospago" class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><strong>Medios de Pago</strong></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <h5 class="box-title">
                            <strong>Descuentos <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="DESCUENTOS UNICAMENTE APLICABLES A CLIENTES SIN SALDO EN CTA.CTE.">
                                                    <i class="fa fa-info-circle"></i>
                                               </a> / intereses </strong>
                        </h5>
                        <table id='tableDI' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-total-field='total'
                               data-unique-id='id_mediopago'>
                            <thead class='thead-inverse'>
                            <th data-field='id' data-align='right' class='text-uppercase hidden'>Id</th>
                            <th data-field='id_descint' data-align='right' class='hidden'>Id</th>
                            <th data-field='id_mediopago' class='text-uppercase hidden'></th>
                            <th data-field='descripcion' class='text-uppercase col-md-8'>Descripcion</th>
                            <th data-field='importe'  data-align='right' class='col-md-4' data-formatter="moneyFormatter">TOTAL</th>
                            </thead>
                            <tfoot>
                            <tr>
                                <td></td>
                                <th>$ <span data-align='right' class='cltotaldi pull-right'></span></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-8">
                        <h5 class="box-title"><strong>Detalle de Pagos</strong></h5>
                        <table id='tableMp' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-total-field='total'
                               data-unique-id='id_mediopago'>
                            <thead class='thead-inverse'>
                            <th data-field='id' data-align='right' class='text-uppercase hidden'>Id</th>
                            <th data-field='id_mediopago' class='text-uppercase hidden'></th>
                            <th data-field='descripcion' class='text-uppercase col-md-3'>Medio de Pago</th>
                            <th data-field='cuotas' data-align='right' class='text-uppercase col-md-1'>Cuotas</th>
                            <th data-field='numero_transaccion' data-align='right' class='text-uppercase col-md-1'>Numero</th>
                            <th data-field='id_banco' class='hidden'></th>
                            <th data-field='banco' class='text-uppercase col-md-3'>Banco</th>
                            <th data-field='fecha_cobro' class='text-uppercase col-md-1'>F.Cobro</th>
                            <th data-field='importe' data-align='right' class='col-md-3' data-formatter="moneyFormatter">IMPORTE</th>
                            @if ($edit==1)
                            <th data-align='center' data-formatter='FunctionsFormatter3'></th>
                            @endif
                            </thead>
                            <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>$ <span data-align='right' class='cltotalmp pull-right'></span></td>
                                @if ($edit==1)
                                <td></td>
                                @endif
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-3">
        <div id="resumen" class="box box-primary pull-right">
            <div class="box-header with-border">
                <h3 class="box-title"><strong>Resumen</strong></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body bg-info">
                <dl class="dl-horizontal pull-right" style="margin-bottom: 8px;">
                    <dt>Subt. de Productos: $</dt>
                    <dd><span class='stprod pull-right'>0</span></dd>
                    <dt>Subtotal Desc/Int.: $</dt>
                    <dd><span class='stdescint pull-right'>0</span></dd>
                    <dt>Total: $</dt>
                    <dd><strong><span class='totalcpte pull-right'>0</span></strong></dd>
                </dl>
            </div>
            <!-- /.box-body -->
            <div class="box-footer ptb-10">
                <div class="pull-right pr-5">
                    @if ($edit==1)
                    <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                    <button id="btnsubmit" type="submit" name="action" value="save" class="btn bg-green" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                        <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                    </button>
                    @else
                    <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
@include('utils.newprodmodal')

<!-- Form MP -->
<div class="modal fade" id="modalMP" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modalmaster" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="box-title"><strong>Agregar Medio de Pago</strong></h4>
            </div>
            <div class="modal-body box box-primary">
                <form id="formMp" name="formMp" class="" role="form"">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="id_mediopago" class="control-label">Medio de Pago</label>
                                <select id="id_mediopago" name="id_mediopago" class="form-control text-uppercase" {{$disabled}}>
                                <option value=""></option>
                                @foreach(DB::table('sys_medios_pago')->where('activo', 1)->whereNotIn('id', [2,6,7])->get() as $mp)
                                <option value="{{$mp->id}}">{{$mp->descripcion}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="saldo" class="control-label">Saldo</label>
                                <input id="saldo" type="text" class="form-control input-sm text-uppercase" name="saldo" value="" {{$editable}}>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="cuotas" class="control-label">Cuotas</label>
                                <input id="cuotas" type="number" class="form-control input-sm text-uppercase" name="cuotas" value="" {{$editable}}>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="numero_transaccion" class="control-label">Numero</label>
                                <input id="numero_transaccion" type="text" class="form-control input-sm text-uppercase" name="numero_transaccion" value="" {{$editable}}>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="id_banco" class="control-label">Banco</label>
                                <select id="id_banco" name="id_banco" class="form-control text-uppercase" disabled>
                                    <option value=""></option>
                                    @foreach(DB::table('conf_bancos')->where('activo', 1)->get() as $banco)
                                        <option value="{{$banco->id}}">{{$banco->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="fecha_cobro" class="control-label">F.Cobro</label>
                                <input id="fecha_cobro" type="date" class="form-control input-sm text-uppercase" name="fecha_cobro" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="importe" class="control-label">Importe</label>
                                <input id="importe" type="number" class="form-control input-sm text-uppercase" name="importe" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer pb-10 no-bt">
                        <div class="pull-right">
                            <a id="agregarmp" class="btn bg-green minw90 d-none" href="#" onclick="AgregarMP()"><i class="fa fa-btn fa-plus text-white"></i> Agregar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<style>
    #addprodform > div.form-group.col-md-7{
        padding-left: 0 !important;
    }
    #addprodform > div.form-group.col-md-2, #addprodform > div.form-group.col-md-1{
        padding-right: 0 !important;
    }
    .info-box-number{
        font-size: 27px;;
    }
    #loading{
        display: inline !important;
    }
    div.pull-left:nth-child(2) > input:nth-child(1){
        width: 100% !important;
    }
    .fixed-table-toolbar .search {
        width: 60% !important;
    }
    .p0{
        padding: 0 !important;
    }
</style>
@stop

@section('js')
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/vendor/select2/js/select2.full.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    $(document.body).addClass('sidebar-collapse');
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
    var listener = new window.keypress.Listener();
    var saldotitular=0;
    var $referente = Number(<?php echo $referente; ?>);

    var datos = <?php echo $datos; ?>;
    $('#table1p').bootstrapTable();
    $('#table1').bootstrapTable();
    var total = <?php echo $total; ?>;
    document.querySelector('.cltotal').innerHTML = total.toFixed(2);
    document.querySelector('.stprod').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

    var datosdi = <?php echo $datosdi; ?>;
    $('#tableDI').bootstrapTable({data: datosdi});
    var totaldi = <?php echo $totaldi; ?>;
    document.querySelector('.cltotaldi').innerHTML = totaldi.toFixed(2);
    document.querySelector('.stdescint').innerHTML = totaldi.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

    var datosmp = <?php echo $datosmp; ?>;
    $('#tableMp').bootstrapTable({data: datosmp});
    var totalmp = <?php echo $totalmp; ?>;
    document.querySelector('.cltotalmp').innerHTML = totalmp.toFixed(2);

    var totalcpte =  <?php echo $totalcpte; ?>;
    document.querySelector('.totalcpte').innerHTML = totalcpte.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

    $("#id_titular").select2({ placeholder: "Buscar/Seleccionar Cliente", allowClear: true });
    $("#id_referente").select2({ placeholder: "Buscar/Seleccionar Referente", allowClear: true });
    $("#id_mediopago").select2({placeholder: "Seleccionar M.Pago",allowClear: true});
    $("#id_banco").select2({placeholder: "Seleccionar Banco",allowClear: true});
    $("#id_item").select2({
        placeholder: "Buscar/Seleccionar Producto",
        allowClear: true,
        minimumInputLength: 4,
        ajax: {
            url: '/datos/getproductos_v2',
            dataType: 'json'
//            processResults: function (data) {
//                // Tranforms the top-level key of the response object from 'items' to 'results'
//                return {
//                    results: data.items
//                };
//            }
        }
    });
    $("#origen").select2();

    listener.simple_combo("shift c", function() {
        $('#id_titular').select2('open');
    });
    listener.simple_combo("shift a", function() {
        $('#id_item').select2('open');
    });
    listener.simple_combo("shift q", function() {
        $('#cantidad').focus();
    });
    listener.simple_combo("shift e", function() {
        UnSoloPago(1);
    });
    listener.simple_combo("shift z", function() {
        UnSoloPago(2);
    });
    listener.simple_combo("shift w", function() {
        OtroMP();
    });
    listener.simple_combo("shift enter", function() {
        AgregarItem();
    });
    listener.simple_combo("shift g", function() {
        $('#form1').submit();
    });

    function moneyFormatter(value, row, index) {
        return  "$ "+value;
    };

    function FunctionsFormatter(value, row, index) {
        return  "<a class='text-danger' href='#' onclick='BorrarItem("+row.id_item+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>";
    };

    function FunctionsFormatter2(value, row, index) {
        return  "<div class=\"form-group\">\n" +
            "      <input id=\"cant"+row.id+"\"  class=\"form-control text-uppercase\" name=\"cantidad\"  value=\"1\" style=\"width: 45px;height: 24px;padding: 5px 5px;font-size: 12px;text-align: right;\">\n" +
            "   </div>\n" +
            "<a class='text-success' href='#' onclick='BuscaOrigenStock("+row.id+",\""+row.producto+"\","+row.precio+")' title='Agregar Producto'><i class='fa fa-fw  fa-plus-circle'></i></a>";
    };

    function FormatterSelectOrigenStk(value, row, index) {
        return  "<div class=\"form-group\">\n" +
                "<select id=\"origen"+row.id_item+"\" name=\"origen\" class=\"form-control text-uppercase\" disabled>\n" +
                "<option value=''></option>\n" +
                "</select>\n" +
                "</div>\n";
    };

    function FunctionsFormatter3(value, row, index) {
        return  "<a class='text-danger' href='#' onclick='BorrarItemMp("+row.id_mediopago+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>";
    };

    $("#id_titular").change(function(){
        var validator = $( "#form1" ).validate();
        validator.element( "#id_titular" );
        ActualizarPrecios();

        var $items = $('#table1').bootstrapTable('getData');
        if (($items.length)>0){
            $('#pagosvarios').removeClass("d-none");

            var combocliente = document.getElementById("id_titular");
            var idcliente = combocliente.options[combocliente.selectedIndex].value;
            if (idcliente != 2){
                $('#pagoctacte').removeClass("d-none");
            }else{
                $('#pagoctacte').addClass("d-none");
            }
            $('#pagoefectivo').removeClass("d-none");
            HabilitarAgregarProds();
        }
        $('#tableMp').bootstrapTable('removeAll');
        $('#tableDI').bootstrapTable('removeAll');

        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        if (idcliente==2){
            $('.info-box').addClass('hidden');
            saldotitular = 0;
        }else{
            $.ajax('/data/getsaldo_tit/'+idcliente, {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(saldo) {
                    $('.info-box').removeClass('hidden');
                    saldotitular = saldo;
                    if (saldotitular > 0) {
                        document.querySelector('.saldo').innerHTML = saldotitular.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                    }else{
                        document.querySelector('.saldo').innerHTML = saldotitular.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                    }
                }
            });
        }
    });    

    $('input[name="id_ajuste"]').click(function() {
        ActualizarPrecios();

        var $items = $('#table1').bootstrapTable('getData');
        if (($items.length)>0){
            $('#pagosvarios').removeClass("d-none");
            var combocliente = document.getElementById("id_titular");
            var idcliente = combocliente.options[combocliente.selectedIndex].value;
            if (idcliente != 2){
                $('#pagoctacte').removeClass("d-none");
            }else{
                $('#pagoctacte').addClass("d-none");
            }
            $('#pagoefectivo').removeClass("d-none");
            HabilitarAgregarProds();
        }
        $('#tableMp').bootstrapTable('removeAll');
        $('#tableDI').bootstrapTable('removeAll');
    });

    $("#id_item").change(function() {
        var comboitem = document.getElementById("id_item");
        var iditem = comboitem.options[comboitem.selectedIndex].value;
        if (iditem>0){
            $.ajax('/datos/getorigen/'+iditem, {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#origen').empty();
                    if (data.length==1){
                        $("#origen").append(
                            '<option value="' + data[0].id + '">' + data[0].descripcion + ' (' + data[0].stock + ') </option>'
                        );
                        $("#origen").prop("disabled", true);
                        $("#origen").val(data[0].id).trigger('change');
                    }else{
                        if (data.length==0){
                            $("#origen").append(
                                '<option value="0">SIN STOCK</option>'
                            );
                            $("#origen").prop("disabled", true);
                            $("#origen").val(0).trigger('change');
                        }else{
                            $.each(data, function(index) {
                                $("#origen").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + ' (' + data[index].stock + ') </option>'
                                );
                            });
                            $("#origen").prop("disabled", false);
                            $("#origen").val('').trigger('change');
                        }
                    }
                }
            });
        }
    });
    
    function DeshabilitarAgregarProds(){
        $('#sugerencias').addClass("d-none");
        $('#addprodform').addClass("d-none");
    }

    function HabilitarAgregarProds(){
        $('#sugerencias').removeClass("d-none");
        $('#addprodform').removeClass("d-none");
    }

    function EliminarDescMp(){
        $('#tableMp').bootstrapTable('removeAll');
        $('#tableDI').bootstrapTable('removeAll');
        totaldescint = 0;
        document.querySelector('.stdescint').innerHTML = totaldescint.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
        document.querySelector('.cltotaldi').innerHTML = totaldescint.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
        document.querySelector('.cltotalmp').innerHTML = totaldescint.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
    }

    function ActualizarPrecios(){
        var total = 0;
        document.querySelector('.cltotaldi').innerHTML = total.toFixed(2);
        document.querySelector('.stdescint').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
        document.querySelector('.cltotalmp').innerHTML = total.toFixed(2);
        $('#tableMp').bootstrapTable('removeAll');
        $('#tableDI').bootstrapTable('removeAll');

        $('#table1').bootstrapTable('showLoading');
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        var idajuste =  $('input[name="id_ajuste"]:checked').val();
        $data = $('#table1').bootstrapTable('getData');
        $data.forEach(function(element) {
            $.ajax('/productos/precio/'+element.id_item, {
                type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data : { idcliente : idcliente, idajuste: idajuste },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                success: function(precio) {
                    element.precio = Number(precio).toFixed(2);
                    element.total = Number(element.cantidad * precio).toFixed(2);
                    total = total + Number(element.total);
                    $('#table1').bootstrapTable('updateByUniqueId', element);;

                    document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
                    document.querySelector('.stprod').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                    document.querySelector('.totalcpte').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                }
            });
        });
        $('#table1').bootstrapTable('hideLoading');
    }

    function AgregarSugerido(id,producto){
        var idproducto = id;
        var cantidad = "1";

        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;

        var idajuste =  $('input[name="id_ajuste"]:checked').val();

        var yaexiste = false;
        total = 0;

        $data = $('#table1').bootstrapTable('getData');
        $data.forEach(function(element) {
            if(element.id_item == idproducto){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'Atencion! Ya ingreso ese Producto.',
                    message: '¿Sumamos las cantidades?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {
                            element.cantidad = Number(element.cantidad) + Number(cantidad);
                            element.total = Number(element.cantidad * element.precio).toFixed(2);
                            $('#table1').bootstrapTable('updateByUniqueId', element);
                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }, true],
                        ['<button>CANCELAR</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }]
                    ],
                    onClosing: function(instance, toast, closedBy){
                        console.info('Closing | closedBy: ' + closedBy);
                    },
                    onClosed: function(instance, toast, closedBy){
                        console.info('Closed | closedBy: ' + closedBy);
                    }
                });
                yaexiste=true;
            }
            total = total + Number(element.total);
            document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
            document.querySelector('.stprod').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
            document.querySelector('.totalcpte').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
        });

        if (!yaexiste){
            $('#table1').bootstrapTable('showLoading');
            $.ajax('/productos/precio/'+idproducto, {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data : { idcliente : idcliente, idajuste: idajuste },
                success: function(precio) {
                    var newreg = '';
                    newreg+= '"id_item":"'+ idproducto + '",';
                    newreg+= '"producto":"'+ producto+'",';
                    newreg+= '"cantidad":"'+ cantidad+'",';
                    newreg+= '"precio":"'+ precio+'",';
                    newreg+= '"total":"'+ Number(cantidad*precio).toFixed(2)+'",';
                    newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
                    var newregarray=JSON.parse('{'+newreg+'}');
                    $('#table1').bootstrapTable('append', newregarray);
                    $('#table1').bootstrapTable('hideLoading');
                    total = total + cantidad*precio;
                    document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
                    document.querySelector('.stprod').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                    document.querySelector('.totalcpte').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

                }
            });
            $.ajax('/productos/sugerencias/'+idproducto, {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(productos) {
                    $('#table2').bootstrapTable('append', productos);
                    $('#table2').bootstrapTable('removeByUniqueId', idproducto);
                }
            });
        }

        scroll_to_anchor('content');
    }

    function BuscaOrigenStock(id,producto,precio){
        var origen = '';
        $.ajax('/datos/getorigen/'+id, {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                //$(cbo_origen).empty();
                if (data.length==1){
                    origen =  data[0].id;
                    AgregarProducto(id,producto,precio,origen);
                }else{
                    if (data.length==0){
                        origen = 0;
                        AgregarProducto(id,producto,precio,origen);
                    }else{
                        var combostk = '<select><option value="">Seleccione</option>';
                        $.each(data, function(index) {
                            combostk = combostk + '<option value='+data[index].id+'>'+data[index].descripcion+'</option>';
                        });
                        combostk = combostk + '</select>';
                        iziToast.question({
                            icon:'fa fa-btn fa-cubes',
                            rtl: false,
                            layout: 1,
                            drag: false,
                            timeout: false,
                            close: true,
                            overlay: true,
                            displayMode: 'once',
                            id: 'question',
                            progressBar: true,
                            title: 'Existe más de un origen de Stock.',
                            message: '',
                            position: 'center',
                            inputs: [
                                [combostk, 'change', function (instance, toast, select, e) {
                                    origen = select.options[select.selectedIndex].value;
                                    console.info('origen: ' + origen);
                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                    AgregarProducto(id,producto,precio,origen);
                                }]
                            ],
                            onClosing: function(instance, toast, closedBy){
                                $("#addean").button('reset');
                                $("#addean").dequeue();
                                $('.overlay').addClass('d-none');
                                console.info('Closing | closedBy: ' + closedBy);
                            },
                            onClosed: function(instance, toast, closedBy){
                                $("#addean").button('reset');
                                $("#addean").dequeue();
                                $('.overlay').addClass('d-none');
                                console.info('Closed | closedBy: ' + closedBy);
                            }
                        });
                    }
                }
            }
        });
    }

    function AgregarProducto(id,producto,precio,origen) {
        var idproducto = id;
        var idcantidad = '#cant'+id;
        var cantidad = $(idcantidad).val();
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        var idajuste =  $('input[name="id_ajuste"]:checked').val();
        var yaexiste = false;
        total = 0;

        $data = $('#table1').bootstrapTable('getData');
        $data.forEach(function(element) {
            if(element.id_item == idproducto){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'Atencion! Ya ingreso ese Producto.',
                    message: '¿Sumamos las cantidades?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {
                            element.cantidad = Number(element.cantidad) + Number(cantidad);
                            element.total = Number(element.cantidad * element.precio).toFixed(2);
                            $('#table1').bootstrapTable('updateByUniqueId', element);
                            total = total + Number(element.total);
                            document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
                            document.querySelector('.stprod').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                            document.querySelector('.totalcpte').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }, true],
                        ['<button>CANCELAR</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }]
                    ],
                    onClosing: function(instance, toast, closedBy){
                        console.info('Closing | closedBy: ' + closedBy);
                    },
                    onClosed: function(instance, toast, closedBy){
                        console.info('Closed | closedBy: ' + closedBy);
                    }
                });
                yaexiste=true;
            }else{
                total = total + Number(element.total);
            }
        });

        if (!yaexiste){
            $('#table1').bootstrapTable('showLoading');
                    @if (env('MP_UNICOPRECIO'))
            var newreg = '';
            newreg+= '"id_item":"'+ idproducto + '",';
            newreg+= '"producto":"'+ producto+'",';
            newreg+= '"origen":"'+ origen +'",';
            newreg+= '"cantidad":"'+ cantidad+'",';
            newreg+= '"precio":"'+ precio+'",';
            newreg+= '"total":"'+ Number(cantidad*precio).toFixed(2)+'",';
            newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
            var newregarray=JSON.parse('{'+newreg+'}');
            $('#table1').bootstrapTable('append', newregarray);
            $('#table1').bootstrapTable('hideLoading');
            total = total + cantidad*precio;
            document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
            document.querySelector('.stprod').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
            document.querySelector('.totalcpte').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

            $('#pagosvarios').removeClass("d-none");
            var combocliente = document.getElementById("id_titular");
            var idcliente = combocliente.options[combocliente.selectedIndex].value;
            if (idcliente != 2){
                $('#pagoctacte').removeClass("d-none");
            }else{
                $('#pagoctacte').addClass("d-none");
            }
            $('#pagoefectivo').removeClass("d-none");
            @else
            $.ajax('/productos/precio/'+idproducto, {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data : { idcliente : idcliente, idajuste: idajuste },
                success: function(precio) {
                    var newreg = '';
                    newreg+= '"id_item":"'+ idproducto + '",';
                    newreg+= '"producto":"'+ producto+'",';
                    newreg+= '"cantidad":"'+ cantidad+'",';
                    newreg+= '"precio":"'+ precio+'",';
                    newreg+= '"total":"'+ Number(cantidad*precio).toFixed(2)+'",';
                    newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
                    var newregarray=JSON.parse('{'+newreg+'}');
                    $('#table1').bootstrapTable('append', newregarray);
                    $('#table1').bootstrapTable('hideLoading');
                    total = total + cantidad*precio;
                    document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
                    document.querySelector('.stprod').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                    document.querySelector('.totalcpte').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

                    $('#pagosvarios').removeClass("d-none");
                    var combocliente = document.getElementById("id_titular");
                    var idcliente = combocliente.options[combocliente.selectedIndex].value;
                    if (idcliente != 2){
                        $('#pagoctacte').removeClass("d-none");
                    }else{
                        $('#pagoctacte').addClass("d-none");
                    }
                    $('#pagoefectivo').removeClass("d-none");
                }
            });
            @endif
            // $.ajax('/productos/sugerencias/'+idproducto, {
            //     type: 'GET',
            //     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            //     success: function(productos) {
            //         $('#table2').bootstrapTable('append', productos);
            //         $('#table2').bootstrapTable('removeByUniqueId', idproducto);
            //     }
            // });
        }

        EliminarDescMp();
        scroll_to_anchor('content');


    }

    function AgregarItem(){
        $('#addprodbtn').button('loading');
        var idproducto = $("#id_item").val();
        var origen = $("#origen").val();
        var cantidad = $("#cantidad").val();

        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;

        var idajuste =  $('input[name="id_ajuste"]:checked').val();

        var yaexiste = false;
        total = 0;

        if (!idproducto || !cantidad) {
            iziToast.warning({
                title: 'Atencion!',
                message: 'Debe ingresar Producto y cantidad.'
            });
        } else {
            $data = $('#table1').bootstrapTable('getData');
            $data.forEach(function(element) {
                if ((element.id_item === idproducto) && (element.origen === origen)){
                    iziToast.question({
                        timeout: 20000,
                        close: false,
                        overlay: true,
                        displayMode: 'once',
                        id: 'question',
                        zindex: 999,
                        title: 'Atencion! Ya ingreso ese Producto.',
                        message: '¿Sumamos las cantidades?',
                        position: 'center',
                        buttons: [
                            ['<button><b>SI</b></button>', function (instance, toast) {
                                element.cantidad = Number(element.cantidad) + Number(cantidad);
                                element.total = Number(element.cantidad * element.precio).toFixed(2);
                                $('#table1').bootstrapTable('updateByUniqueId', element);
                                total = total + Number(element.total);
                                document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
                                document.querySelector('.stprod').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                                document.querySelector('.totalcpte').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                                $("#addprodbtn").button('reset');
                                $("#addprodbtn").dequeue();
                                $('.overlay').addClass('d-none');
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                            }, true],
                            ['<button>CANCELAR</button>', function (instance, toast) {
                                $("#addprodbtn").button('reset');
                                $("#addprodbtn").dequeue();
                                $('.overlay').addClass('d-none');
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                            }]
                        ],
                        onClosing: function(instance, toast, closedBy){
                            $("#addprodbtn").button('reset');
                            $("#addprodbtn").dequeue();
                            $('.overlay').addClass('d-none');
                            console.info('Closing | closedBy: ' + closedBy);
                        },
                        onClosed: function(instance, toast, closedBy){
                            $("#addprodbtn").button('reset');
                            $("#addprodbtn").dequeue();
                            $('.overlay').addClass('d-none');
                            console.info('Closed | closedBy: ' + closedBy);
                        }
                    });
                    yaexiste=true;
                }else{
                    total = total + Number(element.total);
                }
            });

            if (!yaexiste){
                $('#table1').bootstrapTable('showLoading');
                $.ajax('/productos/precio/'+idproducto, {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data : { idcliente : idcliente, idajuste: idajuste },
                    success: function(precio) {
                        var newreg = '';
                        var combo = document.getElementById("id_item");
                        var idprod = combo.options[combo.selectedIndex].value;
                        newreg+= '"id_item":"'+ idprod + '",';
                        var producto = combo.options[combo.selectedIndex].text;
                        var re = /\$.+?]/g;
                        var producto = producto.replace(re,'');
                        newreg+= '"producto":"'+ producto+'",';
                        newreg+= '"origen":"'+ origen+'",';
                        newreg+= '"cantidad":"'+ cantidad+'",';
                        newreg+= '"precio":"'+ precio +'",';
                        importe = cantidad*precio;
                        newreg+= '"total":"'+ importe.toFixed(2) +'",';
                        newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
                        var newregarray=JSON.parse('{'+newreg+'}');
                        $('#table1').bootstrapTable('append', newregarray);
                        $('#table1').bootstrapTable('hideLoading');
                        total = total + cantidad*precio;
                        document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
                        document.querySelector('.stprod').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                        document.querySelector('.totalcpte').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

                        $('#pagosvarios').removeClass("d-none");
                        var combocliente = document.getElementById("id_titular");
                        var idcliente = combocliente.options[combocliente.selectedIndex].value;
                        if (idcliente != 2){
                            $('#pagoctacte').removeClass("d-none");
                        }else{
                            $('#pagoctacte').addClass("d-none");
                        }
                        $('#pagoefectivo').removeClass("d-none");

                        $("#addprodbtn").button('reset');
                        $("#addprodbtn").dequeue();
                        $('.overlay').addClass('d-none');
                    }
                });
                $.ajax('/productos/sugerencias/'+idproducto, {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(productos) {
                        $('#table2').bootstrapTable('append', productos);
                        $('#table2').bootstrapTable('removeByUniqueId', idproducto);
                        HabilitarAgregarProds();
                    }
                });
            }

            EliminarDescMp();
        }
        $('#cantidad').val(1);
        scroll_to_anchor('content');
    }

    function BorrarItem(id){
        row = $('#table1').bootstrapTable('getRowByUniqueId', id);
        total = total - Number(row.total);
        document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
        document.querySelector('.stprod').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
        $('#table1').bootstrapTable('removeByUniqueId', id);

        EliminarDescMp();
        document.querySelector('.totalcpte').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
        $('#pagosvarios').removeClass("d-none");
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        if (idcliente != 2){
            $('#pagoctacte').removeClass("d-none");
        }else{
            $('#pagoctacte').addClass("d-none");
        }
        $('#pagoefectivo').removeClass("d-none");
        HabilitarAgregarProds();

        scroll_to_anchor('content');
    }

    function BorrarItemMp(idmediopago){
        $('#tableMp').bootstrapTable('removeByUniqueId', idmediopago);
        $('#tableDI').bootstrapTable('removeByUniqueId', idmediopago);

        var $mps = $('#tableMp').bootstrapTable('getData');
        if (($mps.length)==0){
            HabilitarAgregarProds();

            totaldescint = 0;
            document.querySelector('.stdescint').innerHTML = totaldescint.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
            document.querySelector('.cltotaldi').innerHTML = totaldescint.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
            document.querySelector('.cltotalmp').innerHTML = totaldescint.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
            document.querySelector('.totalcpte').innerHTML = document.querySelector('.stprod').innerHTML;

            scroll_to_anchor('content');
        }else{
            /* Recalculo Total Mp */
            var mps = $('#tableMp').bootstrapTable('getData');
            totalmp = 0;
            mps.forEach(function(element) {
                totalmp = totalmp + Number(element.importe);
            });
            document.querySelector('.cltotalmp').innerHTML = totalmp.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

            /* Recalculo Total DescInt */
            var dis = $('#tableDI').bootstrapTable('getData');
            totaldi = 0;
            dis.forEach(function(element) {
                totaldi = totaldi + Number(element.importe);
            });
            document.querySelector('.cltotaldi').innerHTML = totaldi.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
            document.querySelector('.stdescint').innerHTML = totaldi.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

            /* Recalculo Total Cpte */
            subtotalprodcutos =  document.querySelector('.stprod').innerHTML;
            totalcpte = Number(replaceAll(subtotalprodcutos,',','')) + totaldi;
            document.querySelector('.totalcpte').innerHTML = totalcpte.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
        }
        $('#pagosvarios').removeClass("d-none");
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        if (idcliente != 2){
            $('#pagoctacte').removeClass("d-none");
        }else{
            $('#pagoctacte').addClass("d-none");
        }
        $('#pagoefectivo').removeClass("d-none");
    }

    function UnSoloPago(idmp){
        $('#form1').validate();
        if ($('#form1').valid()){
            total = document.querySelector('.cltotal').innerHTML;
            if (Number(total)>0){
                $('#pagosvarios').addClass("d-none");
                $('#pagoctacte').addClass("d-none");
                $('#pagoefectivo').addClass("d-none");
                DeshabilitarAgregarProds();
                $('#tableMp').bootstrapTable('showLoading');
                $('#tableDI').bootstrapTable('showLoading');
                $('#mediospago').removeClass("d-none");

                var combocliente = document.getElementById("id_titular");
                var idcliente = combocliente.options[combocliente.selectedIndex].value;
                var totaldescint = 0;

                $.ajax('/cptes/desint', {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data : { idcliente : idcliente, idmp: idmp },
                    success: function(data) {
                        if (jQuery.isEmptyObject(data)){
                            $('#tableDI').bootstrapTable('hideLoading');
                        }else{
                            element = JSON.parse(data);
                            var newdi = '';
                            newdi+= '"id_descint":"'+ element[0].id + '",';
                            newdi+= '"id_mediopago":"'+ element[0].id_mediopago + '",';
                            newdi+= '"descripcion":"'+ element[0].descripcion + '",';
                            totaldescint = ( Number(total) * Number(element[0].porcentaje) / 100 ) * Number(element[0].signo);
                            newdi+= '"importe":"'+ totaldescint.toFixed(2) +'"';
                            if ((saldotitular<1) ||  (element[0].signo>1)){
                                var newdescint=JSON.parse('{'+newdi+'}');
                                $('#tableDI').bootstrapTable('append', newdescint);
                            }else{
                                totaldescint=0;
                            }
                            $('#tableDI').bootstrapTable('hideLoading');


                            if (totaldescint !== null){
                                document.querySelector('.cltotaldi').innerHTML = totaldescint.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                                document.querySelector('.stdescint').innerHTML = totaldescint.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                            }
                        }

                        var newreg = '';
                        newreg+= '"id":"",';
                        newreg+= '"id_mediopago":'+idmp+',';
                        if (idmp == 1){
                            newreg+= '"descripcion":"EFECTIVO",';
                        }else{
                            newreg+= '"descripcion":"CTA CTE",';
                        }
                        importe = Number(total) + totaldescint;
                        newreg+= '"importe":"'+importe.toFixed(2)+'",';
                        newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
                        var newregarray=JSON.parse('{'+newreg+'}');
                        $('#tableMp').bootstrapTable('append', newregarray);
                        $('#tableMp').bootstrapTable('hideLoading');

                        document.querySelector('.cltotalmp').innerHTML = importe.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                        document.querySelector('.totalcpte').innerHTML = importe.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                    }
                });
            }
        }
        scroll_to_anchor('content');
    }

    function OtroMP(){
        $('#form1').validate();
        if ($('#form1').valid()){
            $("#formMp")[0].reset();
            $('#id_mediopago').val('').trigger('change');
            $('#agregarmp').addClass('d-none');
            $('#modalMP').modal();
        }
    }

    $("#id_mediopago").change(function() {
        var combomp = document.getElementById("id_mediopago");
        var idmp = combomp.options[combomp.selectedIndex].value;
        if (idmp>0){
            $.ajax('/mediospago/getdata/'+idmp, {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    if (jQuery.isEmptyObject(data)){
                        $('#cuotas').prop('readonly', false);
                        $('#numero_transaccion').prop('readonly', false);
                    }else{
                        element = JSON.parse(data);
                        element.cuotas == 0 ? $('#cuotas').prop('readonly', true) : $('#cuotas').prop('readonly', false);
                        element.identificador == 0 ? $('#numero_transaccion').prop('readonly', true) : $('#numero_transaccion').prop('readonly', false);
                        element.fecha_cobro == 0 ? $('#fecha_cobro').prop('readonly', true) : $('#fecha_cobro').prop('readonly', false);
                        element.id_banco == 0 ? $('#id_banco').prop('disabled', true) : $('#id_banco').prop('disabled', false);
                        $('#saldo').focus();
                    }
                }
            });
        }

        /* Calculo el saldo a Pagar */
        var mps = $('#tableMp').bootstrapTable('getData');
        totalmp = 0;
        mps.forEach(function(element) {
            totalmp = totalmp + Number(element.importe);
        });
        subtotalprodcutos =  document.querySelector('.stprod').innerHTML;
        saldo = Number(replaceAll(subtotalprodcutos,',','')) - totalmp;
        $('#saldo').val(saldo);
    });

    $("#saldo").focusout(function() {
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        var combomp = document.getElementById("id_mediopago");
        var idmp = combomp.options[combomp.selectedIndex].value;
        var totaldescint = 0;

        $.ajax('/cptes/desint', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : { idcliente : idcliente, idmp: idmp },
            success: function(data) {
                if (jQuery.isEmptyObject(data)){
                    $('#importe').val($('#saldo').val());
                }else{
                    element = JSON.parse(data);

                    var prods = $('#table1').bootstrapTable('getData');
                    stprod = 0;
                    prods.forEach(function(item) {
                        stprod = stprod + Number(item.total);
                    });

                    /* Solo Aplico Descuentos cuando el medio de pago es uno solo */
                    if ((Number(element[0].signo) > 0 ) || ( (Number(element[0].signo) < 0 ) && (Number($('#saldo').val()) == stprod.toFixed(2)) )){
                        var descint = (Number($('#saldo').val()) * Number(element[0].porcentaje) / 100 ) * Number(element[0].signo);
                        $('#importe').val( Number(descint) + Number($('#saldo').val()));
                    }else{
                        saldo = Number($('#saldo').val());
                        $('#importe').val(saldo.toFixed(2));
                    }
                }
            }
        });

        if($('#cuotas').prop('readonly')){
            $('#agregarmp').removeClass('d-none');
        }
    });

    $("#cuotas").focusout(function() {
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        var combomp = document.getElementById("id_mediopago");
        var idmp = combomp.options[combomp.selectedIndex].value;
        var cuotas = $('#cuotas').val();
        var totaldescint = 0;

        $.ajax('/cptes/desint', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : { idcliente : idcliente, idmp: idmp, cuotas: cuotas },
            success: function(data) {
                if (jQuery.isEmptyObject(data)){
                    $('#importe').val($('#saldo').val());
                    $('#agregarmp').removeClass('d-none');
                }else{
                    element = JSON.parse(data);

                    var prods = $('#table1').bootstrapTable('getData');
                    stprod = 0;
                    prods.forEach(function(item) {
                        stprod = stprod + Number(item.importe);
                    });

                    /* Solo Aplico Descuentos cuando el medio de pago es uno solo */
                    if ((Number(element[0].signo) > 0 ) || ( (Number(element[0].signo) > 0 ) && (Number($('#saldo').val()) == stprod.toFixed(2)) )){
                        var descint = (Number($('#saldo').val()) * Number(element[0].porcentaje) / 100 ) * Number(element[0].signo);
                        var importe = Number(descint) + Number($('#saldo').val());
                        $('#importe').val( importe.toFixed(2));
                    }else{
                        saldo = Number($('#saldo').val());
                        $('#importe').val(saldo.toFixed(2));
                    }
                    $('#agregarmp').removeClass('d-none');
                }
            }
        });
    });

    function AgregarMP(){
        $('#ppnro').popover('hide');
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;

        var totaldescint = Number(document.querySelector('.cltotaldi').innerHTML);

        var combomp = document.getElementById("id_mediopago");
        var idmp = combomp.options[combomp.selectedIndex].value;
        var descmp = combomp.options[combomp.selectedIndex].text;
        var saldo = Number($('#saldo').val());
        var cuotas = Number($('#cuotas').val());
        var numtrans = Number($('#numero_transaccion').val());
        var combobanco = document.getElementById("id_banco");
        var idbanco = combobanco.options[combobanco.selectedIndex].value;
        var banco= combobanco.options[combobanco.selectedIndex].text;
        var fecha_cobro = $('#fecha_cobro').val();
        var importe = Number($('#importe').val());
        $('#modalMP').modal('hide');

        total = document.querySelector('.cltotal').innerHTML;
        if (Number(total)>0){
            $('#pagoctacte').addClass("d-none");
            $('#pagoefectivo').addClass("d-none");
            DeshabilitarAgregarProds();
            $('#tableMp').bootstrapTable('showLoading');
            $('#tableDI').bootstrapTable('showLoading');
            $('#mediospago').removeClass("d-none");

            $.ajax('/cptes/desint', {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data : { idcliente : idcliente, idmp: idmp, cuotas: cuotas },
                success: function(data) {
                    if (jQuery.isEmptyObject(data)){
                        $('#tableDI').bootstrapTable('hideLoading');
                    }else{
                        element = JSON.parse(data);

                        var prods = $('#table1').bootstrapTable('getData');
                        stprod = 0;
                        prods.forEach(function(item) {
                            stprod = stprod + Number(item.total);
                        });

                        /* Solo Aplico Descuentos cuando el medio de pago es uno solo */
                        if ((Number(element[0].signo) > 0 ) || ( (Number(element[0].signo) < 0 ) && (Number($('#saldo').val()) == stprod.toFixed(2)) )){
                            var newdi = '';
                            newdi+= '"id_descint":"'+ element[0].id + '",';
                            newdi+= '"id_mediopago":"'+ element[0].id_mediopago + '",';
                            newdi+= '"descripcion":"'+ element[0].descripcion + '",';
                            var descint = (saldo * Number(element[0].porcentaje) / 100 ) * Number(element[0].signo);
                            newdi+= '"importe":"'+ descint.toFixed(2) +'"';
                            var newdescint=JSON.parse('{'+newdi+'}');

                            $('#tableDI').bootstrapTable('append', newdescint);
                        }
                        $('#tableDI').bootstrapTable('hideLoading');
                    }

                    /* Calculo el Total del Comprobante considerando los Descuentos e Intereses */
                    var dis = $('#tableDI').bootstrapTable('getData');
                    totaldi = 0;
                    dis.forEach(function(element) {
                        totaldi = totaldi + Number(element.importe);
                    });
                    document.querySelector('.cltotaldi').innerHTML = totaldi.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                    document.querySelector('.stdescint').innerHTML = totaldi.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

                    subtotalprodcutos =  document.querySelector('.stprod').innerHTML;
                    totalcpte = Number(replaceAll(subtotalprodcutos,',','')) + totaldi;
                    document.querySelector('.totalcpte').innerHTML = totalcpte.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');


                    var newreg = '';
                    newreg+= '"id":"",';
                    newreg+= '"id_mediopago":'+idmp+',';
                    newreg+= '"descripcion":"'+descmp+'",';
                    newreg+= '"cuotas":"'+cuotas+'",';
                    newreg+= '"numero_transaccion":"'+numtrans+'",';
                    newreg+= '"id_banco":"'+idbanco+'",';
                    newreg+= '"banco":"'+banco+'",';
                    newreg+= '"fecha_cobro":"'+fecha_cobro+'",';
                    newreg+= '"importe":"'+importe.toFixed(2)+'",';
                    newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
                    var newregarray=JSON.parse('{'+newreg+'}');
                    $('#tableMp').bootstrapTable('append', newregarray);
                    $('#tableMp').bootstrapTable('hideLoading');

                    var mps = $('#tableMp').bootstrapTable('getData');
                    totalmp = 0;
                    mps.forEach(function(element) {
                        totalmp = totalmp + Number(element.importe);
                    });
                    document.querySelector('.cltotalmp').innerHTML = totalmp.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                }
            });
            $('#agregarmp').addClass('d-none');
        }
    }

    $('#form1').validate({
        rules: {
            id_titular: {required: true}
        },
        messages: {
            id_titular: {required: 'Ingrese un Cliente'}
        }
    });

    $('#form1').submit(function (event) {
        $('#btnsubmit').button('loading');
        var $detalle = $('#table1').bootstrapTable('getData');
        if (($detalle.length)==0){
            iziToast.warning({
                title: 'Atencion!',
                message: 'Debe ingresar al menos un Producto.'
            });
            $('#btnsubmit').button('reset');
            event.preventDefault();
        }

        /* Recalculo Total Mp */
        var mps = $('#tableMp').bootstrapTable('getData');
        totalmp = 0;
        mps.forEach(function(element) {
            totalmp = totalmp + Number(element.importe);
        });
        /* Recalculo Total DescInt */
        var dis = $('#tableDI').bootstrapTable('getData');
        totaldi = 0;
        dis.forEach(function(element) {
            totaldi = totaldi + Number(element.importe);
        });
        /* Recalculo Total Productos */
        var prods = $('#table1').bootstrapTable('getData');
        totalprods = 0;
        prods.forEach(function(element) {
            totalprods = totalprods + Number(element.total);
        });
        /* Valido el Total Cpte Vs Total Medio de Pago */
        totalcpte = totalprods + totaldi;
        if (totalcpte.toFixed(2)!=totalmp.toFixed(2)){
            iziToast.warning({
                title: 'Atencion!',
                message: 'El Total del Comprobante no Coincide con el Total de Medios de Pago.'
            });
            $('#btnsubmit').button('reset');
            event.preventDefault();
        }

        var params = [
            {
                name: "items",
                value: JSON.stringify($('#table1').bootstrapTable('getData'))
            },
            {
                name: "mps",
                value: JSON.stringify($('#tableMp').bootstrapTable('getData'))
            },
            {
                name: "descint",
                value: JSON.stringify($('#tableDI').bootstrapTable('getData'))
            }
        ];
        $(this).append($.map(params, function (param) {
            return   $('<input>', {
                type: 'hidden',
                name: param.name,
                value: param.value
            })
        }))

        var formData = $('#form1').serialize();
        $.ajax('/cptes/store/rem', {
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:formData,
            success: function(data) {
                iziToast.success(
                    {
                        timeout: 4000,
                        position:'topRight',
                        title: 'Listo!',
                        message: data.msg,
                        buttons: [
                            ['<button class="text-bold"><i class="fa fa-btn fa-print"></i> Imprimir</button>', function (instance, toast, button, e, inputs) {
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                window.location.href = '/cptes/print/rem/'+data.lastid;
                            }]
                        ]
                    });
                EliminarDescMp();
                $('#table1').bootstrapTable('removeAll');
                total = 0;
                document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
                document.querySelector('.stprod').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                document.querySelector('.totalcpte').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                $('#form1').trigger("reset");
                @if (!empty(env('MP_CLIENTE')))
                    var cliente = <?php echo env('MP_CLIENTE'); ?>;
                    $('#id_titular').val(cliente).trigger('change');
                @else
                    $('#id_titular').val(null).trigger('change');
                @endif
            }
        });
        event.preventDefault();
        $('#btnsubmit').button('reset');
    });


    function replaceAll( text, busca, reemplaza ){
        while (text.toString().indexOf(busca) != -1)
            text = text.toString().replace(busca,reemplaza);
        return text;
    }

    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }

    $(window).on('load', function(event){
        @if (session('status'))
            iziToast.success(
                {
                timeout: 4000,
                position:'topRight',
                title: 'Listo!',
                message: '{{session('status')}}',
                buttons: [
                    ['<button class="text-bold"><i class="fa fa-btn fa-print"></i> Imprimir</button>', function (instance, toast, button, e, inputs) {
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                        window.location.href = '/cptes/print/rem/'+{{session('lastid')}};
                    }]
                ]
            });
        @endif

        @if (!empty($cpte))
            var combocliente = document.getElementById("id_titular");
            var idcliente = combocliente.options[combocliente.selectedIndex].value;
            var idcpte = $('#id').val();
            if (idcliente==2){
                saldotitular = 0;
            }else{
                $.ajax('/data/getsaldo_tit/'+idcliente+'/'+idcpte, {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(saldo) {
                        saldotitular = saldo;
                        $('#saldoctecte').val(saldotitular.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,'));
                    }
                });
            }
        @endif

       $('#table1p').bootstrapTable('showLoading');
       $.ajax('/datos/getproductosrem', {
           type: 'GET',
           headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
           success: function(data) {
               $('#table1p').bootstrapTable('append', data);
               $('#table1p').bootstrapTable('hideLoading');
           }
       });

        {{--@if (!empty(env('MP_COMISION')))--}}
            {{--$("#loading2").button('loading');--}}
            {{--$("#loading2").removeClass('hidden');--}}
            {{--$.ajax('/datos/gettitulares/0', {--}}
                {{--type: 'GET',--}}
                {{--headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
                {{--success: function(data) {--}}
                    {{--$.each(data, function(index) {--}}
                        {{--if ($referente===data[index].id){--}}
                            {{--$("#id_referente").append(--}}
                                {{--'<option value="' + data[index].id + '" selected>' + data[index].descripcion + '</option>'--}}
                            {{--);--}}
                        {{--}else{--}}
                            {{--$("#id_referente").append(--}}
                                {{--'<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'--}}
                            {{--);--}}
                        {{--}--}}
                    {{--});--}}
                    {{--var $disabled = '{{$disabled}}';--}}
                    {{--if ($disabled.length==0){--}}
                        {{--$("#id_referente").prop("disabled", false);--}}
                    {{--}--}}
                    {{--$("#loading2").button('reset');--}}
                    {{--$("#loading2").addClass('hidden');--}}
                {{--}--}}
            {{--});--}}
        {{--@endif--}}
    });
    function runningFormatter(value, row, index) {
        return index+1;
    }
</script>
@include('utils.newprodmodaljs')
@stop