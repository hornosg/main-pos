@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.ventasnavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <a href="/cptes/create/nd" class="btn btn-primary"  title="Nuevo Remito">
                                    <i class="fa fa-btn fa-plus text-white"></i>
                                </a>
                                <button id="btn-eliminar" class="btn btn-danger"  title="Eliminar Seleccion">
                                    <i class="fa fa-btn fa-trash text-white"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-detail-view='true'
                               data-unique-id='id'>

                            <thead class='thead-inverse'>
                                <th data-checkbox='true'></th>
                                <th data-field='id' data-align='right' class='hidden text-uppercase'>Id</th>
                                <th data-field='pasaron' data-align='right' class='hidden'>pasaron</th>
                                <th data-field='creado' data-align='right'  class='text-uppercase'>Creado</th>
                                <th data-field='numero'>NUMERO</th>
                                <th data-field='id_titular' class='hidden'></th>
                                <th data-field='titular'  data-formatter="clienteFormatter" class='text-uppercase  text-bold'>Cliente</th>
                                <th data-field='total' data-align='right' data-formatter="moneyFormatter" class='text-uppercase text-bold'>Importe</th>
                                <th data-field='usuario' data-align='left' class='text-uppercase'>Usuario</th>
                                <th data-field='id_estado' data-align='left' data-formatter='FunctionsStatusFormatter'>ESTADO</th>
                                <th data-formatter='FunctionsFormatter'></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer ptb-10">
                <div class="pull-right pr-5">
                    <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script language='JavaScript' type='text/javascript'>
    var datos = <?php echo json_encode($items); ?>;
    $(document.body).addClass('sidebar-collapse');
    var $table = $('#table1');
    $table.bootstrapTable('showLoading');
    $table.bootstrapTable({
        data: datos,
        exportDataType:'all',
        exportOptions:{fileName: 'items'}
    });
    $table.bootstrapTable('hideLoading');

    $table.on('expand-row.bs.table', function (e, index, row, $detail) {
        $idtabla2="tabla"+row.id;
        html='<div class="box"><table id='+$idtabla2+' class="table table-sm table-bordered table-hover table-striped table-condensed table-responsive" ' +
            'data-search="true" data-strict-search="false" data-multiple-search="true" data-pagination="true">' +
            '<thead class="thead-inverse-gray"><tr>' +
            '<th data-field="id" data-align="right">Codigo</th>' +
            '<th data-field="descripcion">Descripcion</th>' +
            '<th data-field="marca">Marca</th>' +
            '<th data-field="preciouni" data-align="right">Precio Unitario</th>' +
            '<th data-field="cantidad" data-align="right">Cantidad</th>' +
            '</tr></thead>' +
            '</table></divS>';
        $detail.html(html);
        $('#'+$idtabla2).bootstrapTable();
        $('#'+$idtabla2).bootstrapTable('showLoading');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'/cptes/detalle/'+row.id,
            data: row.id,
            success: function(datosdetalle) {
                $('#'+$idtabla2).bootstrapTable('append', datosdetalle);
                $('#'+$idtabla2).bootstrapTable('hideLoading');
            }
        });
    });

    function clienteFormatter(value, row, index) {
        return  "<a href='/clientes/cuentacorriente/"+row.id_titular+"'>"+value+"</a>";
    };

    function moneyFormatter(value, row, index) {
        return  "$ "+value;
    };

    function FunctionsFormatter(value, row, index) {
        var links = '';
        if (row.pasaron<16){
            links ="<a class='btn-outline-danger' href='#' onclick='Borrar("+row.id+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>";
        }
        links = links + "<a class='btn-outline-primary' href='/cptes/show/nd/"+row.id+"'><i class='fa fa-fw fa-search'></i></a>" +
                        "<a class='btn-outline-primary' href='/cptes/print/nd/"+row.id+"'><i class='fa fa-fw fa-print'></i></a>";

        return links;
    }

    function FunctionsStatusFormatter(value, row, index) {
        if(row.id_estado===1){
            return '<span class="badge bg-green">PAGO</span>';
        }
        if(row.id_estado===2){
            return '<span class="badge bg-red">PENDIENTE</span>';
        }
        if(row.id_estado===3){
            return '<span class="badge bg-primary">REGISTRADO</span>';
        }
    };

</script>
@include('utils.borrarcpte')
@include('utils.borrarcptes')
@include('utils.statusnotification')
@stop
