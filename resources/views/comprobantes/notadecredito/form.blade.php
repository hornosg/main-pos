@extends('adminlte::page')

@section('title', $titulo)

@php
if ($edit==0){
    $editable='readonly';
    $visible='d-none';
    $disabled='disabled';
    $accion = 'Visualizar';
}else{
    $editable=' ';
    $visible=' ';
    $disabled=' ';
    $accion = 'Editar';
}

if (!empty($prods)){
    $total = 0;
    $datos = json_encode($prods);
    foreach ($prods as $prod){
        $total = $total + $prod->total;
    }
}else{
    $datos = "[]";
    $total = 0;
}

if (!empty($mps)){
    $totalmp = 0;
    $datosmp = json_encode($mps);
    foreach ($mps as $mp){
        $totalmp = $totalmp + $mp->importe;
    }
}else{
    $datosmp = "[]";
    $totalmp = 0;
}

if (empty($cpte)){
    $totalcpte=0;
    $referente=0;
}else{
    $totalcpte=$cpte->total;
    $referente=$cpte->id_referente;
}
@endphp

@section('content_header')
    @include('layouts.ventasnavbar')
@stop

@section('content')
<div id="content" class="container-fluid row">
    <form id="form1" class="" role="form" method="POST" action="/cptes/store/nc" enctype="multipart/form-data">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>{{$tituloform}}</strong></h3>
                </div>
                <div class="box-body">
                    {{ csrf_field() }}

                    @if (!empty($cpte))
                        <input type = "hidden" name = "id" value = "{{$cpte->id}}">
                    @else
                        <input type = "hidden" name = "id">
                    @endif

                    <div class="form-group col-md-12">
                        <label for="id_cpterel" class="control-label">Facturas & Remitos</label><div id="loading" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>
                        <select id="id_cpterel" name="id_cpterel" class="form-control text-uppercase">
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="id_titular" class="control-label">Cliente</label>
                        <select id="id_titular" name="id_titular" class="form-control text-uppercase" {{$disabled}}>
                            @if (!empty($cpte))
                                <option value=""></option>
                                @foreach(DB::table('titulares')->where('id_tipotitular', 2)->where('activo', 1)->get() as $tipotitular)
                                    @if ( $cpte->id_titular == $tipotitular->id )
                                        <option value="{{ $tipotitular->id }}" selected>{{ $tipotitular->descripcion }}</option>
                                    @else
                                        <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                    @endif
                                @endforeach
                            @else
                                <option value="" selected></option>
                                @foreach(DB::table('titulares')->where('id_tipotitular', 2)->where('activo', 1)->get() as $tipotitular)
                                    <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="id_referente" class="control-label">Profesional / Referente</label><div id="loading2" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>
                        <select id="id_referente" name="id_referente" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="id_sector" class="control-label">Sector</label>
                        <select id="id_sector" name="id_sector" class="form-control text-uppercase" disabled>
                            <option value="2">SALON DE VENTAS</option>
                        </select>
                    </div>
                    @if (!empty($cpte))
                        <fieldset class="form-group col-md-12">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Alta]</label>
                                @php $uscr=DB::table('users')->where('id', $cpte->created_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{  $uscr->name }} {{ date_format($cpte->created_at, 'd/m/Y H:i:s') }}" readonly>
                        </fieldset>
                        @if (!empty($cpte->updated_us))
                        <fieldset class="form-group col-md-12">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Modificacion]</label>
                                @php $usup=DB::table('users')->where('id', $cpte->updated_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{ $usup->name }} {{ date_format($cpte->updated_at, 'd/m/Y H:i:s') }}" readonly>
                        </fieldset>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>{{$titulodetart}}</strong></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="field-group col-md-12">
                            <div id="addprodform">
                                @if ($edit==1)
                                    <div class="form-group col-md-10">
                                        <label for="id_item" class="control-label">Producto</label><div id="loading" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>
                                        <a class='text-primary pull-right' href='#' data-toggle='modal' data-target='#prodModal' title='Crear Nuevo Producto'>Crear Nuevo</i></a>
                                        <select id="id_item" name="id_item" class="form-control text-uppercase" {{$disabled}}>
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <label for="cantidad" class="control-label">Cantidad</label>
                                        <input id="cantidad"  class="form-control input-sm text-uppercase" name="cantidad" value="1" {{$editable}}>
                                    </div>
                                    <div class="col-md-1 pt-21">
                                        <button id="addprodbtn" type="button" class='btn btn-primary' onclick='AgregarItem()' title="Agregar Producto"><i class="fa fa-btn fa-plus text-white"></i></button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                   data-toggle='table'
                                   data-total-field='total'
                                   data-unique-id='id_item'>
                                <thead class='thead-inverse'>
                                    <th data-field='id' data-align='right' class='text-uppercase hidden'></th>
                                    <th data-field='id_item' data-align='right' class='hidden text-uppercase'>IdItem</th>
                                    <th data-field='producto' class='text-uppercase col-md-7'>Descripcion</th>
                                    <th data-field='cantidad' data-align='right' class='text-uppercase col-md-1' data-editable="true">Cant</th>
                                    <th data-field='precio' data-align='right' class='col-md-2' data-formatter="moneyFormatter">PRECIO UNI.</th>
                                    <th data-field='total'  data-align='right' class='col-md-2' data-formatter="moneyFormatter">TOTAL</th>
                                    @if ($edit==1)
                                        <th data-align='center' data-formatter='FunctionsFormatter'></th>
                                    @endif
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <th>$ <span data-align='right' class='cltotal pull-right'></span></th>
                                        @if ($edit==1)
                                            <td></td>
                                        @endif
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    @if ($edit==1)
                        <div class="pull-right pr-10">
                            <a id="pagoctacte" class="btn btn-warning" href="#" onclick="UnSoloPago(2)" title='Shift + Z'><i class="fa fa-btn fa-sort-amount-desc text-white"></i> Registar en Cta Cte</a>
                            <a id="pagoefectivo" class="btn btn-success d-none" href="#" onclick="UnSoloPago(1)" title='Shift + E'><i class="fa fa-btn fa-thumbs-o-up text-white"></i> Total en Efectivo</a>
                        </div>
                    @endif
                </div>
            </div>
            <div id="mediospago" class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>Medios de Pago</strong></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <table id='tableMp' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                   data-toggle='table'
                                   data-total-field='total'
                                   data-unique-id='id_mediopago'>
                                <thead class='thead-inverse'>
                                <th data-field='id' data-align='right' class='text-uppercase hidden'>Id</th>
                                <th data-field='id_mediopago' class='text-uppercase hidden'></th>
                                <th data-field='descripcion' class='text-uppercase col-md-6'>Medio de Pago</th>
                                <th data-field='importe' data-align='right' class='col-md-4' data-formatter="moneyFormatter">IMPORTE</th>
                                @if ($edit==1)
                                <th data-align='center' data-formatter='FunctionsFormatter3'></th>
                                @endif
                                </thead>
                                <tfoot>
                                <tr>
                                    <td></td>
                                    <td>$ <span data-align='right' class='cltotalmp pull-right'></span></td>
                                    @if ($edit==1)
                                    <td></td>
                                    @endif
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-md-6 align-bottom">
                            <div class="pull-right pr-5 align-bottom">
                                @if ($edit==1)
                                <a class="btn btn-primary" href="/cptes/nc"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                                <button type="submit" name="action" value="save" class="btn btn-primary">
                                    <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                                </button>
                                @else
                                <a class="btn btn-primary" href="/cptes/nc"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </form>
</div>
@include('utils.newprodmodal')
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<style>
    #addprodform > div.form-group.col-md-7{
        padding-left: 0 !important;
    }
    #addprodform > div.form-group.col-md-2, #addprodform > div.form-group.col-md-1{
        padding-right: 0 !important;
    }
    .info-box-number{
        font-size: 27px;;
    }
    #loading{
        display: inline !important;
    }
</style>
@stop

@section('js')
<script src="/js/bootstrap-editable.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/js/bootstrap-table-editable.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    $(document.body).addClass('sidebar-collapse');
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
    var listener = new window.keypress.Listener();
    var saldotitular=0;
    var $referente = Number(<?php echo $referente; ?>);

    var datos = <?php echo $datos; ?>;
    $('#table1').bootstrapTable({data: datos});
    var total = <?php echo $total; ?>;
    document.querySelector('.cltotal').innerHTML = total.toFixed(2);

    var datosmp = <?php echo $datosmp; ?>;
    $('#tableMp').bootstrapTable({data: datosmp});
    var totalmp = <?php echo $totalmp; ?>;
    document.querySelector('.cltotalmp').innerHTML = totalmp.toFixed(2);

    $("#id_cpterel").select2({ placeholder: "Seleccionar Factura o Remito",allowClear: true});
    $("#id_titular").select2({ placeholder: "Buscar/Seleccionar Proveedor", allowClear: true });
    $("#id_referente").select2({ placeholder: "Buscar/Seleccionar Referente", allowClear: true });
    $("#id_sector").select2({disabled: true});
    $("#id_mediopago").select2({placeholder: "Seleccionar M.Pago",allowClear: true});
    $("#id_item").select2({placeholder: "Buscar/Seleccionar Producto",allowClear: true,minimumInputLength: 4});

    listener.simple_combo("shift c", function() {
        $('#id_titular').select2('open');
    });
    listener.simple_combo("shift a", function() {
        $('#id_item').select2('open');
    });
    listener.simple_combo("shift q", function() {
        $('#cantidad').focus();
    });
    listener.simple_combo("shift e", function() {
        UnSoloPago(1);
    });
    listener.simple_combo("shift z", function() {
        UnSoloPago(2);
    });
    listener.simple_combo("shift w", function() {
        OtroMP();
    });
    listener.simple_combo("shift enter", function() {
        AgregarItem();
    });
    listener.simple_combo("shift g", function() {
        $('#form1').submit();
    });

    function moneyFormatter(value, row, index) {
        return  "$ "+value;
    };

    function FunctionsFormatter(value, row, index) {
        return  "<a class='text-danger' href='#' onclick='BorrarItem("+row.id_item+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>";
    };

    function FunctionsFormatter3(value, row, index) {
        return  "<a class='text-danger' href='#' onclick='BorrarItemMp("+row.id_mediopago+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>";
    };

    $("#id_cpterel").change(function(){
        var combocpterel = document.getElementById("id_cpterel");
        var idrel = combocpterel.options[combocpterel.selectedIndex].value;
        if (idrel>0){
            $('body').addClass('waiting');
            $('#table1').bootstrapTable('removeAll');
            $('#tableMp').bootstrapTable('removeAll');

            $('#table1').bootstrapTable('showLoading');
            $.ajax({
                type: "GET",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                async: false,
                url:'/cptes/detalle/'+idrel,
                data: idrel,
                success: function(detallerem) {
                    detallerem.forEach(function(datosdetalle) {
                        yaexiste=false;
                        $data = $('#table1').bootstrapTable('getData');
                        $data.forEach(function(element) {
                            if(element.id_item == datosdetalle.id_item){
                                element.cantidad = Number(element.cantidad) + Number(datosdetalle.cantidad);
                                element.cantidad = element.cantidad.toFixed(2);
                                element.total = Number(element.cantidad * element.precio).toFixed(2);
                                $('#table1').bootstrapTable('updateByUniqueId', element);
                                yaexiste=true;
                            }
                        });
                        if(!yaexiste){
                            $('#table1').bootstrapTable('append', datosdetalle);
                        }
                    });
                    $('#table1').bootstrapTable('hideLoading');
                }
            });

            $.ajax({
                type: "GET",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                async: false,
                url:'/cptes/gettitulares/'+idrel,
                success: function(data) {
                    $("#id_titular").val(data.id_titular);
                    $("#id_titular").trigger('change');
                    $("#id_titular").prop("disabled", true);
                    $("#id_referente").val(data.id_referente);
                    $("#id_referente").trigger('change');
                    $("#id_referente").prop("disabled", true);
                }
            });

            /* Recalculo Total Prods */
            var prods = $('#table1').bootstrapTable('getData');
            total = 0;
            prods.forEach(function(element) {
                total = total + Number(element.total);
            });
            document.querySelector('.cltotal').innerHTML =  total.toFixed(2);

            $('#addprodform').addClass("d-none");
            $('body').removeClass('waiting');
        }else{
            $('#addprodform').removeClass("d-none");
            $("#id_titular").prop("disabled", false);
            $("#id_referente").prop("disabled", false);
        }
    });

    $('#table1').on('editable-save.bs.table', function (e, field, row, oldValue) {
        if(Number(row.cantidad) > 0 ){
            row.total = Number(row.cantidad)  * Number(row.precio);
            row.total = Number(row.total).toFixed(2);
        }
        $('#table1').bootstrapTable('updateByUniqueId', {id: row.id, row: row});
        $('#table1').bootstrapTable('resetView');
    });

    $("#id_titular").change(function(){
        ActualizarPrecios();
        var $items = $('#table1').bootstrapTable('getData');
        if (($items.length)>0){
            HabilitarAgregarProds();
            $('#pagoefectivo').removeClass("d-none");
        }
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        if (idcliente != 2){
            $('#pagoctacte').removeClass("d-none");
        }else{
            $('#pagoctacte').addClass("d-none");
        }
    });
    
    function DeshabilitarAgregarProds(){
        $('#addprodform').addClass("d-none");
    }

    function HabilitarAgregarProds(){
        $('#addprodform').removeClass("d-none");
    }

    function ActualizarPrecios(){
        var total = 0;
        document.querySelector('.cltotalmp').innerHTML = total.toFixed(2);
        $('#tableMp').bootstrapTable('removeAll');

        $('#table1').bootstrapTable('showLoading');
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        $data = $('#table1').bootstrapTable('getData');
        $data.forEach(function(element) {
            $.ajax('/productos/precio/'+element.id_item, {
                type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data : { idcliente : idcliente, idajuste: null },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                success: function(precio) {
                    element.precio = Number(precio).toFixed(2);
                    element.total = Number(element.cantidad * precio).toFixed(2);
                    total = total + Number(element.total);
                    $('#table1').bootstrapTable('updateByUniqueId', element);;

                    document.querySelector('.cltotal').innerHTML =  total;
                    document.querySelector('.stprod').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                }
            });
        });
        $('#table1').bootstrapTable('hideLoading');
    }

    function AgregarItem(){
        var idproducto = $("#id_item").val();
        var origen = $("#origen").val();
        var cantidad = $("#cantidad").val();

        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;

        var idajuste =  $('input[name="id_ajuste"]:checked').val();

        var yaexiste = false;
        total = 0;

        if (!idproducto || !cantidad) {
            iziToast.warning({
                title: 'Atencion!',
                message: 'Debe ingresar Producto y cantidad.'
            });
        } else {
            $data = $('#table1').bootstrapTable('getData');
            $data.forEach(function(element) {
                if ((element.id_item === idproducto) && (element.origen === origen)){
                    iziToast.question({
                        timeout: 20000,
                        close: false,
                        overlay: true,
                        displayMode: 'once',
                        id: 'question',
                        zindex: 999,
                        title: 'Atencion! Ya ingreso ese Producto.',
                        message: '¿Sumamos las cantidades?',
                        position: 'center',
                        buttons: [
                            ['<button><b>SI</b></button>', function (instance, toast) {
                                element.cantidad = Number(element.cantidad) + Number(cantidad);
                                element.total = Number(element.cantidad * element.precio).toFixed(2);
                                $('#table1').bootstrapTable('updateByUniqueId', element);
                                total = total + Number(element.total);
                                document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                            }, true],
                            ['<button>CANCELAR</button>', function (instance, toast) {

                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                            }]
                        ],
                        onClosing: function(instance, toast, closedBy){
                            console.info('Closing | closedBy: ' + closedBy);
                        },
                        onClosed: function(instance, toast, closedBy){
                            console.info('Closed | closedBy: ' + closedBy);
                        }
                    });
                    yaexiste=true;
                }else{
                    total = total + Number(element.total);
                }
            });

            if (!yaexiste){
                $('#table1').bootstrapTable('showLoading');
                $.ajax('/productos/precio/'+idproducto, {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data : { idcliente : idcliente, idajuste: idajuste },
                    success: function(precio) {
                        var newreg = '';
                        var combo = document.getElementById("id_item");
                        var idprod = combo.options[combo.selectedIndex].value;
                        newreg+= '"id_item":"'+ idprod + '",';
                        var producto = combo.options[combo.selectedIndex].text;
                        var re = /\[.+?]/g;
                        var producto = producto.replace(re,'');
                        newreg+= '"producto":"'+ producto+'",';
                        newreg+= '"origen":"'+ origen+'",';
                        newreg+= '"cantidad":"'+ cantidad+'",';
                        newreg+= '"precio":"'+ precio +'",';
                        importe = cantidad*precio;
                        newreg+= '"total":"'+ importe.toFixed(2) +'",';
                        newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
                        var newregarray=JSON.parse('{'+newreg+'}');
                        $('#table1').bootstrapTable('append', newregarray);
                        $('#table1').bootstrapTable('hideLoading');
                        total = total + cantidad*precio;
                        document.querySelector('.cltotal').innerHTML =  total.toFixed(2);

                        $('#pagosvarios').removeClass("d-none");
                        var combocliente = document.getElementById("id_titular");
                        var idcliente = combocliente.options[combocliente.selectedIndex].value;
                        if (idcliente != 2){
                            $('#pagoctacte').removeClass("d-none");
                        }else{
                            $('#pagoctacte').addClass("d-none");
                        }
                        $('#pagoefectivo').removeClass("d-none");
                    }
                });
            }
        }

        scroll_to_anchor('content');
    }

    function BorrarItem(id){
        row = $('#table1').bootstrapTable('getRowByUniqueId', id);
        total = total - Number(row.total);
        document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
        $('#table1').bootstrapTable('removeByUniqueId', id);

        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        if (idcliente != 2){
            $('#pagoctacte').removeClass("d-none");
        }else{
            $('#pagoctacte').addClass("d-none");
        }
        $('#pagoefectivo').removeClass("d-none");
        HabilitarAgregarProds();

        scroll_to_anchor('content');
    }

    function BorrarItemMp(idmediopago){
        $('#tableMp').bootstrapTable('removeByUniqueId', idmediopago);
        var mps = $('#tableMp').bootstrapTable('getData');
        totalmp = 0;
        mps.forEach(function(element) {
            totalmp = totalmp + Number(element.importe);
        });
        document.querySelector('.cltotalmp').innerHTML = totalmp.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
        if (totalmp==0){
            HabilitarAgregarProds();
        }
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        if (idcliente != 2){
            $('#pagoctacte').removeClass("d-none");
        }else{
            $('#pagoctacte').addClass("d-none");
        }
        total = document.querySelector('.cltotal').innerHTML;
        if (Number(total)>0){
            $('#pagoefectivo').removeClass("d-none");
        }
    }

    function UnSoloPago(idmp){
        $('#form1').validate();
        if ($('#form1').valid()){
            total = document.querySelector('.cltotal').innerHTML;
            if (Number(total)>0){
                $('#pagoctacte').addClass("d-none");
                $('#pagoefectivo').addClass("d-none");
                DeshabilitarAgregarProds();
                $('#tableMp').bootstrapTable('showLoading');
                $('#mediospago').removeClass("d-none");

                var combocliente = document.getElementById("id_titular");
                var idcliente = combocliente.options[combocliente.selectedIndex].value;
                var totaldescint = 0;

                var newreg = '';
                newreg+= '"id":"",';
                newreg+= '"id_mediopago":'+idmp+',';
                if (idmp == 1){
                    newreg+= '"descripcion":"EFECTIVO",';
                }else{
                    newreg+= '"descripcion":"CTA CTE",';
                }
                importe = Number(total);
                newreg+= '"importe":"'+importe.toFixed(2)+'",';
                newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
                var newregarray=JSON.parse('{'+newreg+'}');
                $('#tableMp').bootstrapTable('append', newregarray);
                $('#tableMp').bootstrapTable('hideLoading');

                document.querySelector('.cltotalmp').innerHTML = importe.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
            }else{
                iziToast.question({
                    icon:'fa fa-btn fa-sliders',
                    rtl: false,
                    layout: 1,
                    drag: false,
                    timeout: false,
                    close: true,
                    overlay: true,
                    displayMode: 1,
                    id: 'question',
                    progressBar: true,
                    title: 'Ingreso el monto',
                    message: '',
                    position: 'center',
                    inputs: [
                        ['<input placeholder="Importe" class="col-md-6">']
                    ],
                    buttons: [
                        ['<button><i class="fa fa-btn fa-floppy-o"></i> Agregar</button>', function (instance, toast, button, e, inputs) {
                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            total =inputs[0].value;
                            $('#pagoctacte').addClass("d-none");
                            $('#pagoefectivo').addClass("d-none");
                            DeshabilitarAgregarProds();
                            $('#tableMp').bootstrapTable('showLoading');
                            var newreg = '';
                            newreg+= '"id":"",';
                            newreg+= '"id_mediopago":'+idmp+',';
                            if (idmp == 1){
                                newreg+= '"descripcion":"EFECTIVO",';
                            }else{
                                newreg+= '"descripcion":"CTA CTE",';
                            }
                            importe = Number(total);
                            newreg+= '"importe":"'+importe.toFixed(2)+'",';
                            newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
                            var newregarray=JSON.parse('{'+newreg+'}');
                            $('#tableMp').bootstrapTable('append', newregarray);
                            $('#tableMp').bootstrapTable('hideLoading');
                            document.querySelector('.cltotalmp').innerHTML = importe.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

                        }, false]
                    ],
                    onClosing: function(instance, toast, closedBy){
                        console.info('Closing | closedBy: ' + closedBy);
                    },
                    onClosed: function(instance, toast, closedBy){
                        console.info('Closed | closedBy: ' + closedBy);
                    }
                });
            }
        }
        scroll_to_anchor('content');
    }

    $('#form1').validate({
        rules: {
            id_titular: {required: true}
        },
        messages: {
            id_titular: {required: 'Ingrese un Cliente'}
        }
    });

    $('#form1').submit(function (event) {
        /* Recalculo Total Mp */
        var mps = $('#tableMp').bootstrapTable('getData');
        totalmp = 0;
        mps.forEach(function(element) {
            totalmp = totalmp + Number(element.importe);
        });
        /* Recalculo Total Productos */
        var prods = $('#table1').bootstrapTable('getData');
        totalprods = 0;
        prods.forEach(function(element) {
            totalprods = totalprods + Number(element.total);
        });
        /* Valido el Total Cpte Vs Total Medio de Pago */
        totalcpte = totalprods;
        if (totalcpte>0){
            if (totalcpte=!totalmp){
                iziToast.warning({
                    title: 'Atencion!',
                    message: 'El Total del Comprobante no Coincide con el Total de Medios de Pago.'
                });
                event.preventDefault();
            }
        }

        $("#id_titular").prop("disabled", false);
        $("#id_referente").prop("disabled", false);

        var params = [
            {
                name: "items",
                value: JSON.stringify($('#table1').bootstrapTable('getData'))
            },
            {
                name: "mps",
                value: JSON.stringify($('#tableMp').bootstrapTable('getData'))
            }
        ];
        $(this).append($.map(params, function (param) {
            return   $('<input>', {
                type: 'hidden',
                name: param.name,
                value: param.value
            })
        }))
    });


    function replaceAll( text, busca, reemplaza ){
        while (text.toString().indexOf(busca) != -1)
            text = text.toString().replace(busca,reemplaza);
        return text;
    }

    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }

    $(window).on('load', function(event){
        @if (session('status'))
            iziToast.success(
                {
                timeout: 4000,
                position:'topRight',
                title: 'Listo!',
                message: '{{session('status')}}',
                buttons: [
                    ['<button class="text-bold"><i class="fa fa-btn fa-print"></i> Imprimir</button>', function (instance, toast, button, e, inputs) {
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                        window.location.href = '/cptes/print/nc/'+{{session('lastid')}};
                    }]
                ]
            });
        @endif

        $("#loading").button('loading');
        $("#loading").removeClass('hidden');
        $.ajax('/datos/getproductos', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $.each(data, function(index) {
                    $("#id_item").append(
                        '<option value="' + data[index].id + '">' + data[index].producto + '</option>'
                    );
                });
                $("#id_item").prop("disabled", false);
                $("#loading").button('reset');
                $("#loading").addClass('hidden');
            }
        });
        $("#loading2").button('loading');
        $("#loading2").removeClass('hidden');
        $.ajax('/datos/gettitulares', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $.each(data, function(index) {
                    if ($referente===data[index].id){
                        $("#id_referente").append(
                            '<option value="' + data[index].id + '" selected>' + data[index].descripcion + '</option>'
                        );
                    }else{
                        $("#id_referente").append(
                            '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                        );
                    }
                });
                var $disabled = '{{$disabled}}';
                if ($disabled.length==0){
                    $("#id_referente").prop("disabled", false);
                }
                $("#loading2").button('reset');
                $("#loading2").addClass('hidden');
            }
        });
        $('#id_titular').val(null);
    });

    $( document ).ready(function() {
        $('#id_titular').val(null);

        $("#loading").button('loading');
        $("#loading").removeClass('hidden');
        $.ajax('/data/getremfact', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_cpterel').empty();
                $("#id_cpterel").append(
                    '<option value=""></option>'
                );
                $.each(data, function(index) {
                    $("#id_cpterel").append(
                        '<option value="' + data[index].id + '">' + data[index].text + '</option>'
                    );
                });
                $("#id_cpterel").prop("disabled", false);
                $("#loading").button('reset');
                $("#loading").addClass('hidden');
            }
        });
    });
</script>
@include('utils.newprodmodaljs')
@stop
