<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>MainPOS</title>
    <style>
        body{
            font-size: 11px;
        }
        table{
            width: 100%;
            border-collapse: collapse;
        }
        td{
            font-size: 9px;
            border: 1px solid;
            vertical-align: top;
        }
    </style>
</head>
<body>
    <table>
        <tr>
            <td style="height:6%; width: 50%; text-align: center;">
                <img src="/img/Logos/logo.png" style="width: 150px;height: auto;padding: 1%;text-align: center; background-color: #fff">
                <p>{{$sucursal->descripcion}} -  {{$sucursal->direccion}} - {{$sucursal->localidad->descripcion}} - Tel:{{$sucursal->telefono}}</p>
            </td>
            <td style="height:6%">
                @php
                $date = new DateTime($cpte->created_at);
                $fecha =  $date->format('d-m-Y');
                @endphp
                <p style="font-weight:bold;padding-top:0px;padding-left: 5%;">RECIBO [X]<span style="float: right;padding-right: 5%;">- Documento no valido como factura -</span></p><br/>
                <p style="font-size:x-large;font-weight:bold;padding-left: 5%;">N°:{{$cpte->numero}}<span style="font-size:medium;padding-left: 25%">Fecha: {{$fecha}}</span></p>
            </td>
        </tr>
        <tr>
            <td style="height:4%;padding-left: 10px;" colspan="2">
                @if (empty($ref->descripcion))
                    <p style="padding-left: 5%">Cliente: {{$titular->descripcion}}<span style="margin-left:240px;"></span></p>
                @else
                    <p style="padding-left: 5%">Cliente: {{$titular->descripcion}}<span style="margin-left:200px;">Profesional: {{$ref->descripcion}}</span></p>
                @endif
                <p></p>
                <p style="padding-left: 5%">Direccion: {{$titular->direccion}} <span style="margin-left:280px;">Telefono: {{$titular->telefono}}</span></p>
            </td>
        </tr>
        <tr>
            <td style="height:230px;" colspan="2">
                <p></p>
                <p>Recibí la suma de: $ {{$cpte->total}}.- En concepto de pago a cuenta .-</p>
            </td>
        </tr>
        <tr>
            <td style="height:70px;padding-left: 10px;">
                <p></p>
                <p></p>
                <p class="pull-right">             Firma              </p>
            </td>
            <td style="height:70px;padding-left: 10px;">
                <p></p>
                <p></p>
                <p class="pull-right">             Aclaracion              </p>
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <table>
        <tr>
            <td style="height:6%; width: 50%; text-align: center;">
                <img src="/img/Logos/logo.png" style="width: 150px;height: auto;padding: 1%;text-align: center; background-color: #fff">
                <p>{{$sucursal->descripcion}} -  {{$sucursal->direccion}} - {{$sucursal->localidad->descripcion}} - Tel:{{$sucursal->telefono}}</p>
            </td>
            <td style="height:6%">
                @php
                $date = new DateTime($cpte->created_at);
                $fecha =  $date->format('d-m-Y');
                @endphp
                <p style="font-weight:bold;padding-top:0px;padding-left: 5%;">NOTA DE DEBITO [X]<span style="float: right;padding-right: 5%;">- Documento no valido como factura -</span></p><br/>
                <p style="font-size:x-large;font-weight:bold;padding-left: 5%;">N°:{{$cpte->numero}}<span style="font-size:medium;padding-left: 25%">Fecha: {{$fecha}}</span></p>
            </td>
        </tr>
        <tr>
            <td style="height:4%;padding-left: 10px;" colspan="2">
                @if (empty($ref->descripcion))
                <p style="padding-left: 5%">Cliente: {{$titular->descripcion}}<span style="margin-left:240px;"></span></p>
                @else
                <p style="padding-left: 5%">Cliente: {{$titular->descripcion}}<span style="margin-left:200px;">Profesional: {{$ref->descripcion}}</span></p>
                @endif
                <p></p>
                <p style="padding-left: 5%">Direccion: {{$titular->direccion}} <span style="margin-left:280px;">Telefono: {{$titular->telefono}}</span></p>
            </td>
        </tr>
        <tr>
            <td style="height:230px;" colspan="2">
                <p></p>
                <p>Recibí la suma de: $ {{$cpte->total}}.- En concepto de pago a cuenta .-</p>
            </td>
        </tr>
        <tr>
            <td style="height:70px;padding-left: 10px;">
                <p></p>
                <p></p>
                <p class="pull-right">             Firma              </p>
            </td>
            <td style="height:70px;padding-left: 10px;">
                <p></p>
                <p></p>
                <p class="pull-right">             Aclaracion              </p>
            </td>
        </tr>
    </table>
</body>
</html>