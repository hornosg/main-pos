@extends('adminlte::page')

@section('title', $titulo)

@php
if ($edit==0){
    $editable='readonly';
    $visible='d-none';
    $disabled='disabled';
    $accion = 'Visualizar';
}else{
    $editable=' ';
    $visible=' ';
    $disabled=' ';
    $accion = 'Editar';
}

if (!empty($prods)){
    $total = 0;
    $datos = json_encode($prods);
    foreach ($prods as $prod){
        $total = $total + $prod->total;
    }
}else{
    $datos = "[]";
    $total = 0;
}

if (!empty($descints)){
    //dd($descints);
    $totaldi = 0;
    $datosdi = json_encode($descints);
    foreach ($descints as $descint){
        $totaldi = $totaldi + $descint->importe;
    }
}else{
    $datosdi = "[]";
    $totaldi = 0;
}

if (!empty($mps)){
    //dd($mps);
    $totalmp = 0;
    $datosmp = json_encode($mps);
    foreach ($mps as $mp){
        $totalmp = $totalmp + $mp->importe;
    }
    $idmediopago=$mps[0]->id_mediopago;
    $idbanco=$mps[0]->id_banco;
    $importe=$mps[0]->importe;
    $cuotas=$mps[0]->cuotas;
    $numero=$mps[0]->numero_transaccion;
    $fechavto=$mps[0]->fecha_vto;
    $fechacbro=$mps[0]->fecha_cobro;
}else{
    $datosmp = "[]";
    $totalmp = 0;
    $idmediopago=null;
    $idbanco=null;
    $importe=null;
    $cuotas=null;
    $numero=null;
    $fechavto=null;
    $fechacbro=null;
}

if (empty($cpte)){
    $totalcpte=0;
}else{
    $totalcpte=$cpte->total;
}
@endphp

@section('content_header')
    @include('layouts.ventasnavbar')
@stop

@section('content')
<div id="content" class="container-fluid row">
    <form id="form1" class="" role="form" method="POST" action="/cptes/store/rbo" enctype="multipart/form-data">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <strong>{{$tituloform}} N°</strong>
                        @if (!empty($cpte))
                        {{$cpte->numero}}
                        @endif
                    </h3>
                </div>
                <div class="box-body">
                    {{ csrf_field() }}

                    @if (!empty($cpte))
                        <input type = "hidden" name = "id" value = "{{$cpte->id}}">
                    @else
                        <input type = "hidden" name = "id">
                    @endif

                    <div class="form-group col-md-12">
                        <label for="id_titular" class="control-label">Cliente</label>
                        <select id="id_titular" name="id_titular" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @if (!empty($cpte))
                                @foreach(DB::table('titulares')->where('id_tipotitular', 2)->where('activo', 1)->get() as $tipotitular)
                                    @if ( $cpte->id_titular == $tipotitular->id )
                                        <option value="{{ $tipotitular->id }}" selected>{{ $tipotitular->descripcion }}</option>
                                    @else
                                        <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                    @endif
                                @endforeach
                            @else
                                @foreach(DB::table('titulares')->where('id_tipotitular', 2)->where('activo', 1)->get() as $tipotitular)
                                    <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    @if (!empty($cpte))
                        <fieldset class="form-group col-md-12">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Alta]</label>
                                @php $uscr=DB::table('users')->where('id', $cpte->created_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{  $uscr->name }} {{ date_format($cpte->created_at, 'd/m/Y H:i:s') }}" readonly>
                        </fieldset>
                        @if (!empty($cpte->updated_us))
                        <fieldset class="form-group col-md-12">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Modificacion]</label>
                                @php $usup=DB::table('users')->where('id', $cpte->updated_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{ $usup->name }} {{ date_format($cpte->updated_at, 'd/m/Y H:i:s') }}" readonly>
                        </fieldset>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>Medio de Pago</strong></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="field-group col-md-12">
                            @if ($edit==1)
                                <div id="formMp" class="row">
                                    <div class="form-group col-md-4">
                                        <label for="id_mediopago" class="control-label">Medio de Pago</label>
                                        <select id="id_mediopago" name="id_mediopago" class="form-control text-uppercase" {{$disabled}}>
                                            <option value=""></option>
                                            @foreach(DB::table('sys_medios_pago')->where('activo', 1)->whereNotIn('id', [2])->get() as $mp)
                                                @if (!empty($idmediopago))
                                                    @if ( $mp->id == $idmediopago )
                                                        <option value="{{$mp->id}}" selected>{{$mp->descripcion}}</option>
                                                    @else
                                                        <option value="{{$mp->id}}">{{$mp->descripcion}}</option>
                                                    @endif
                                                @else
                                                        <option value="{{$mp->id}}">{{$mp->descripcion}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="importe" class="control-label">Importe</label>
                                        <input id="importe" type="text" class="form-control input-sm text-uppercase" name="importe" value="{{$importe}}" {{$editable}}>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="cuotas" class="control-label">Cuotas</label>
                                        <input id="cuotas" type="number" class="form-control input-sm text-uppercase" name="cuotas" value="{{$cuotas}}" {{$editable}}>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="numero_transaccion" class="control-label">Numero</label>
                                        <input id="numero_transaccion" type="text" class="form-control input-sm text-uppercase" name="numero_transaccion" value="{{$numero}}" {{$editable}}>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="id_banco" class="control-label">Banco</label>
                                        <select id="id_banco" name="id_banco" class="form-control text-uppercase" {{$disabled}}>
                                            <option value=""></option>
                                            @foreach(DB::table('conf_bancos')->where('activo', 1)->get() as $banco)
                                                @if (!empty($idbanco))
                                                    @if ( $banco->id == $idmediopago )
                                                        <option value="{{$banco->id}}" selected>{{$banco->descripcion}}</option>
                                                    @else
                                                        <option value="{{$banco->id}}">{{$banco->descripcion}}</option>
                                                    @endif
                                                @else
                                                    <option value="{{$banco->id}}">{{$banco->descripcion}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="fecha_vto" class="control-label">F.Recibido</label>
                                        <input id="fecha_vto" type="date" class="form-control input-sm text-uppercase" name="fecha_vto" value="{{$fechavto}}" {{$editable}}>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="fecha_cobro" class="control-label">F.Cobro</label>
                                        <input id="fecha_cobro" type="date" class="form-control input-sm text-uppercase" name="fecha_cobro" value="{{$fechacbro}}" {{$editable}}>
                                    </div>
                                    <div class="pull-right pt-19 mr-15">
                                        <a class="btn btn-primary" href="/cptes/rbo"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                                        <button type="submit" name="action" value="save" class="btn btn-primary">
                                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                                        </button>
                                    </div>
                                </div>
                            @else
                                <table id='tableMp' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                       data-toggle='table'
                                       data-total-field='total'
                                       data-unique-id='id_mediopago'>
                                    <thead class='thead-inverse'>
                                    <th data-field='id' data-align='right' class='text-uppercase hidden'>Id</th>
                                    <th data-field='id_mediopago' class='text-uppercase hidden'></th>
                                    <th data-field='descripcion' class='text-uppercase col-md-2'>Medio de Pago</th>
                                    <th data-field='numero_transaccion' data-align='right' class='text-uppercase'>Numero</th>
                                    <th data-field='banco' data-align='right' class='text-uppercase'>Banco</th>
                                    <th data-field='cuotas' data-align='right' class='text-uppercase'>Cuotas</th>
                                    <th data-field='fecha_vto' data-align='right' class='text-uppercase'>F.Rcbo</th>
                                    <th data-field='fecha_cobro' data-align='right' class='text-uppercase'>F.Cbro</th>
                                    <th data-field='importe' data-align='right' class='col-md-3' data-formatter="moneyFormatter">IMPORTE</th>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <th>$ <span data-align='right' class='cltotalmp pull-right'></span></th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <div class="pull-right">
                                    <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    $(document.body).addClass('sidebar-collapse');
    $(function () {
        $('[data-toggle="popover"]').popover();
    });
    var listener = new window.keypress.Listener();

    var datosmp = <?php echo $datosmp; ?>;
    $('#tableMp').bootstrapTable({data: datosmp});
    var totalmp = <?php echo $totalmp; ?>;
    if (!<?php echo $edit; ?>){
        document.querySelector('.cltotalmp').innerHTML = totalmp.toFixed(2);
    }


    $("#id_titular").select2({ placeholder: "Buscar/Seleccionar Proveedor", allowClear: true });
    $("#id_mediopago").select2({
        placeholder: "Seleccionar M.Pago",
        allowClear: true
    });
    $("#id_banco").select2({
        placeholder: "Seleccionar Banco",
        allowClear: true
    });

    listener.simple_combo("shift c", function() {
        $('#id_titular').select2('open');
    });
    listener.simple_combo("shift b", function() {
        $('#id_banco').select2('open');
    });

    function moneyFormatter(value, row, index) {
        return  "$ "+value;
    };

    $("#id_mediopago").change(function() {
        var combomp = document.getElementById("id_mediopago");
        var idmp = combomp.options[combomp.selectedIndex].value;
        if (idmp>0){
            $.ajax('/mediospago/getdata/'+idmp, {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    if (jQuery.isEmptyObject(data)){
                        $('#cuotas').prop('readonly', false);
                        $('#numero_transaccion').prop('readonly', false);
                        $('#fecha_cobro').prop('readonly', false);
                        $('#fecha_vto').prop('readonly', false);
                        $('#id_banco').prop('disable', false);
                    }else{
                        element = JSON.parse(data);
                        element.cuotas == 0 ? $('#cuotas').prop('readonly', true) : $('#cuotas').prop('readonly', false);
                        element.identificador == 0 ? $('#numero_transaccion').prop('readonly', true) : $('#numero_transaccion').prop('readonly', false);
                        element.fecha_cobro == 0 ? $('#fecha_cobro').prop('readonly', true) : $('#fecha_cobro').prop('readonly', false);
                        element.fecha_vto == 0 ? $('#fecha_vto').prop('readonly', true) : $('#fecha_vto').prop('readonly', false);
                        element.id_banco == 0 ? $('#id_banco').prop('disabled', true) : $('#id_banco').prop('disabled', false);
                        $('#importe').focus();
                    }
                }
            });
        }
    });

    $('#form1').validate({
        rules: {
            id_titular: {required: true},
            id_mediopago: {required: true},
            importe: {required: true}
        },
        messages: {
            id_titular: {required: 'Ingrese el Cliente'},
            id_mediopago: {required: 'Ingrese el Medio de Pago'},
            importe: {required: 'Ingrese el Importe'}
        }
    });


    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }
</script>
@include('utils.newprodmodaljs')
@include('utils.statusnotification')
@stop
