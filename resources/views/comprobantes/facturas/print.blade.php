<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>MainPOS</title>
    <style>
        body{
            font-size: 12px;
        }
        table{
            width: 100%;
            border-collapse: collapse;
        }
        td{
            border: 1px solid #000000;
            vertical-align: top;
        }
    </style>
</head>
<body>
    <table>
        <tr>
            <td style="height:84px; width: 50%; text-align: center;padding: 10px;">
<!--                <img src="/img/Logos/main_pos.png" style="width: 250px;height: auto;padding: 1%;text-align: center; background-color: #000000">-->
<!--                <p>Calsasasale 1234, Lsasauján, Buenos Aies - Tel:0111540470815 - hornosg@gmail.com - www.mainpos.com.ar</p>-->
            </td>
            <td style="height:84px;">
                @php
                    $date = new DateTime($cpte->created_at);
                    $fecha =  $date->format('d-m-Y');
                @endphp
                <h2 style="top:0px; margin: 5px;">NUMERO:<span style="margin: 10px;">{{$cpte->numero}}</span></h2>
                <h3 style="float: right;padding-right: 10px;">FECHA: {{$fecha}}</h3>
            </td>
        </tr>
        <tr>
            <td style="height:70px;padding-left: 10px;" colspan="2">
                <p>Cliente: {{$titular->descripcion}}</p>
                <p>Domicilio: {{$titular->direccion}}</p>
                <p>Iva: @if (!empty($titular->tipoiva)) {{$titular->tipoiva->descripcion}}@endif <span style="margin-left:100px;">CUIT:{{$titular->cuit}}</span></p>
            </td>
        </tr>
        <tr>
            <td style="height:300px;" colspan="2">
                <table>
                    <tr>
                        <td style="font-weight: bold;border: 1px solid #000000;">CANTIDAD</td>
                        <td style="font-weight: bold;border: 1px solid #000000;">DESCRIPCION</td>
                        <td style="font-weight: bold;border: 1px solid #000000;">PRECIO</td>
                        <td style="font-weight: bold;border: 1px solid #000000;">IMPORTE</td>
                    </tr>
                    @php
                        $subtotal = 0;
                    @endphp
                    @foreach ($prods as $prod)
                        <tr>
                            <td style="text-align: right;padding-right: 10px;border: 1px solid #000000;">{{$prod->cantidad}}</td>
                            <td style=";padding-left: 10px;border: 1px solid #000000;">{{$prod->producto}}</td>
                            <td style="text-align: right;padding-right: 10px;border: 1px solid #000000;">{{$prod->precio}}</td>
                            <td style="text-align: right;padding-right: 10px;border: 1px solid #000000;">{{$prod->total}}</td>
                        </tr>
                        @php
                            $subtotal = $subtotal + $prod->total
                        @endphp
                    @endforeach
                    <tr>
                        <td style="border: none;"></td>
                        <td style="border: none;"></td>
                        @if ($cpte->id_estado==2)
                            <td style="text-align: right;font-weight: bold;border: 1px solid #000000;">TOTAL</td>
                        @else
                            <td style="text-align: right;font-weight: bold;border: 1px solid #000000;">SUBTOTAL:</td>
                        @endif
                        <td style="text-align: right;padding-right: 10px;font-weight: bold;border: 1px solid #000000;">$ {{$subtotal}}</td>
                    </tr>
                    @if ($cpte->id_estado!=2)
                        <tr>
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td style="text-align: right;font-weight: bold;border: 1px solid #000000;">DESC./INTERESES</td>
                            <td style="text-align: right;padding-right: 10px;font-weight: bold;border: 1px solid #000000;">$ {{$totaldi}}</td>
                        </tr>
                        <tr>
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td style="text-align: right;font-weight: bold;border: 1px solid #000000;">TOTAL</td>
                            <td style="text-align: right;padding-right: 10px;font-weight: bold;border: 1px solid #000000;">$ {{$cpte->total}}</td>
                        </tr>
                    @endif
                </table>
            </td>
        </tr>
    </table>
    <img src="/img/Logos/main_pos.png" style="width: auto;height: 15px;background-color:#000000; padding: 5px; margin-top: 5px;">
    <span style="margin: 5px:">www.mainpos.com.ar</span>
</body>
</html>