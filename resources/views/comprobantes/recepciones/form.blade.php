@extends('adminlte::page')

@section('title', $titulo)

@php
if ($edit==0){
    $editable='readonly';
    $visible='d-none';
    $disabled='disabled';
    $accion = 'Visualizar';
}else{
    $editable=' ';
    $visible=' ';
    $disabled=' ';
    $accion = 'Editar';
}

if (!empty($prods)){
    $total = 0;
    $datos = json_encode($prods);
    foreach ($prods as $prod){
        $total = $total + $prod->total;
    }
}else{
    $datos = "[]";
    $total = 0;
}
@endphp

@section('content_header')
    @include('layouts.comprasnavbar')
@stop

@section('content')
<div class="container-fluid row">
    <form id="form1" class="" role="form" method="POST" action="/cptes/store/rec" enctype="multipart/form-data">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>{{$tituloform}}</strong></h3>
                </div>
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="form-group col-md-12 ">
                        <label for="id_titular" class="control-label">Proveedor</label>
                        <select id="id_titular" name="id_titular" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @if (!empty($cpte))
                                @foreach(DB::table('titulares')->where('id_tipotitular', 3)->where('activo', 1)->get() as $tipotitular)
                                    @if ( $cpte->id_titular == $tipotitular->id )
                                        <option value="{{ $tipotitular->id }}" selected>{{ $tipotitular->descripcion }}</option>
                                    @else
                                        <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                    @endif
                                @endforeach
                            @else
                                @foreach(DB::table('titulares')->where('id_tipotitular', 3)->where('activo', 1)->get() as $tipotitular)
                                    <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="id_cpterel" class="control-label">Orden de Compra</label>
                        <select id="id_cpterel" name="id_cpterel" class="form-control text-uppercase" disabled>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="id_sector" class="control-label">Sector</label>
                        <select id="id_sector" name="id_sector" class="form-control text-uppercase" disabled>
                            <option value="1">RECEPCION</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="id_ubicacion" class="control-label">Ubicacion</label>
                        <select id="id_ubicacion" name="id_ubicacion" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::table('inv_ubicaciones')
                                    ->where('inv_ubicaciones.activo', 1)
                                    ->where('inv_ubicaciones.id_sector', 1)
                                    ->select('inv_ubicaciones.*')
                                    ->get() as $ubicacion)
                                <option value="{{$ubicacion->id}}">{{$ubicacion->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="id_posicion" class="control-label">Posicion</label>
                        <select id="id_posicion" name="id_posicion" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="observaciones" class="control-label">Comentarios</label>
                        <textarea id="observaciones" name="observaciones" class="form-control" rows="3" {{$editable}}> {{ $cpte->observaciones or '' }} </textarea>
                    </div>
                    @if (!empty($cpte))
                        <fieldset class="form-group col-md-12">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Alta]</label>
                                @php $uscr=DB::table('users')->where('id', $cpte->created_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{  $uscr->name }} {{ date_format($cpte->created_at, 'd/m/Y H:i:s') }}" readonly>
                        </fieldset>
                        @if (!empty($cpte->updated_us))
                        <fieldset class="form-group col-md-12">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Modificacion]</label>
                                @php $usup=DB::table('users')->where('id', $cpte->updated_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{ $usup->name }} {{ date_format($cpte->updated_at, 'd/m/Y H:i:s') }}" readonly>
                        </fieldset>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>{{$titulodetart}}</strong></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="field-group col-md-12">
                            <div class="row">
                                @if (!empty($cpte))
                                    <input type = "hidden" name = "id" value = "{{$cpte->id}}">
                                @else
                                    <input type = "hidden" name = "id">
                                @endif

                                @if ($edit==1)
                                    <div class="form-group {{ $errors->has('id_item') ? ' has-error' : '' }} col-md-8 ">
                                        <label for="id_item" class="control-label">Articulo</label>
                                        @if ($edit==1)
                                            <a class='text-primary pull-right' href='#' data-toggle='modal' data-target='#prodModal' title='Crear Nuevo Producto'>Crear Nuevo</i></a>
                                        @endif
                                        <select id="id_item" name="id_item" class="form-control text-uppercase" {{$disabled}}>
                                            <option value=""></option>
                                            @foreach(DB::table('items')->where('id_tipoitem', 5)
                                                            ->where('items.activo', 1)
                                                            ->leftJoin('conf_marcas', 'conf_marcas.id', '=', 'items.id_marca')
                                                            ->leftJoin('conf_unidades_medida', 'conf_unidades_medida.id', '=', 'items.id_unidadcontneto')
                                                            ->leftJoin('items_proveedores', 'items_proveedores.id_item', '=', 'items.id')
                                                            ->select('items.*', 'conf_marcas.descripcion as marca', 'conf_unidades_medida.descripcion as unimed', 'items_proveedores.codproducto as codprov')
                                                            ->get() as $item)
                                        <option value="{{$item->id}}">[{{$item->id}}] {{$item->descripcion}} - {{$item->contNeto}} {{$item->unimed}} ({{$item->marca}}) [ean:{{$item->ean}} / CodProv:{{$item->codprov}}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group{{ $errors->has('cantidad') ? ' has-error' : '' }} col-md-1">
                                        <label for="cantidad" class="control-label">Cantidad</label>
                                        <input id="cantidad" type="number" class="form-control input-sm text-uppercase" name="cantidad" value="1" {{$editable}}>
                                    </div>
                                    <div class="form-group{{ $errors->has('precio') ? ' has-error' : '' }} col-md-2">
                                        <label for="precio" class="control-label">Precio</label>
                                        <input id="precio" type="number" step="any" class="form-control input-sm" min="0" name="precio" {{$editable}}>
                                    </div>
                                    <div class="col-md-1 pt-21">
                                        <button id="btnitems" type="button" class='btn btn-primary' onclick='AgregarItem()' title="Agregar Producto" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>"><i class="fa fa-btn fa-arrow-down text-white"></i></button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                   data-toggle='table'
                                   data-total-field='total'
                                   data-unique-id='id_item'>
                                <thead class='thead-inverse'>
                                    <th data-field='id_item' data-align='right' class='hidden'>Id</th>
                                    <th data-field='codprov' data-align='right' class='text-uppercase col-md-1'>CODPROV</th>
                                    <th data-field='producto' class='text-uppercase col-md-6'>Descripcion</th>
                                    <th data-field='cantidad' data-align='right' class='col-md-1' data-editable="true">Cant</th>
                                    <th data-field='precio' data-align='right' class='col-md-2'  data-editable="true">PRECIO</th>
                                    <th data-field='total'  data-align='right' class='col-md-2'  data-editable="true">TOTAL</th>
                                    @if ($edit==1)
                                        <th data-align='center' data-formatter='FunctionsFormatter'></th>
                                    @endif
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <th><span data-align='right' class='cltotal pull-right'></span></th>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 ">
                    <div class="pull-right pr-10">
                        @if ($edit==1)
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                            <button type="submit" name="action" value="save" class="btn btn-primary">
                                <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            </button>
                        @else
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@include('utils.newprodmodal')
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-editable.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/js/bootstrap-editable.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/js/bootstrap-table-editable.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    $(document.body).addClass('sidebar-collapse');
    var listener = new window.keypress.Listener();

    var datos = <?php echo $datos; ?>;
    $('#table1').bootstrapTable({data: datos});
    var total = <?php echo $total; ?>;
    document.querySelector('.cltotal').innerHTML = '$ '+total;

    $("#id_titular").select2({ placeholder: "Buscar/Seleccionar Proveedor", allowClear: true });
    $("#id_cpterel").select2({disabled: true});
    $("#id_sector").select2({disabled: true});
    $("#id_ubicacion").select2({});
    $("#id_posicion").select2({});
    $("#id_item").select2({
        placeholder: "Buscar/Seleccionar Articulo",
        allowClear: true,
        minimumInputLength: 3
    });

    listener.simple_combo("shift p", function() {
        $('#id_titular').select2('open');
    });
    listener.simple_combo("shift a", function() {
        $('#id_item').select2('open');
    });
    listener.simple_combo("shift c", function() {
        $('#cantidad').focus();
    });
    listener.simple_combo("shift enter", function() {
        AgregarItem();
    });

    $("#id_titular").change(function() {
        $("#id_cpterel").prop("disabled", true);
        var combotit = document.getElementById("id_titular");
        var idtitular = combotit.options[combotit.selectedIndex].value;
        $.ajax('/datos/getoc_titular/'+idtitular, {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $.each(data, function(index, value) {
                    $("#id_cpterel").append(
                        '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                    );
                });
                $("#id_cpterel").val('').trigger('change');
                $("#id_cpterel").prop("disabled", false);
            },
            error: function(data){
                $("#id_cpterel").prop("disabled", false);
            }
        });
    });

    $("#id_cpterel").change(function() {
        var combocpterel = document.getElementById("id_cpterel");
        var idoc = combocpterel.options[combocpterel.selectedIndex].value;
        if (idoc > 1) {
            $("#btnitems").button('loading');
            $.ajax('/datos/getoc_detalle/'+idoc, {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $("#id_titular").attr('disabled', true);
                    $("#id_cpterel").prop("disabled", true);
                    $('#table1').bootstrapTable('removeAll');
                    $('#table1').bootstrapTable('showLoading');
                    $.each(data, function(index, value) {
                        var newreg = '';
                        newreg+= '"id_item":"'+ data[index].id_item + '",';
                        newreg+= '"codprov":"'+ data[index].codproducto +'",';
                        newreg+= '"producto":"'+ data[index].producto +'",';
                        newreg+= '"cantidad":"'+ data[index].cantidad +'",';
                        newreg+= '"precio":"'+ data[index].precio+'",';
                        newreg+= '"total":"'+ data[index].total+'",';
                        newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
                        var newregarray=JSON.parse('{'+newreg+'}');
                        $('#table1').bootstrapTable('append', newregarray);
                    });
                    $("#btnitems").button('reset');
                    $("#btnitems").dequeue();
                    total = 0;
                    $data = $('#table1').bootstrapTable('getData');
                    $data.forEach(function(element){
                        total = total + Number(element.total);
                    });
                    document.querySelector('.cltotal').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                    $('#table1').bootstrapTable('hideLoading');
                }
            });
        }
    });

    $("#id_ubicacion").change(function() {
        $("#id_posicion").prop("disabled", true);
        var comboubi = document.getElementById("id_ubicacion");
        var idubi = comboubi.options[comboubi.selectedIndex].value;
        $.ajax('/datos/getposiciones_ubicacion/'+idubi, {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_posicion').empty();
                $.each(data, function(index, value) {
                    $("#id_posicion").append(
                        '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                    );
                });
                $("#id_posicion").val('').trigger('change');
                $("#id_posicion").prop("disabled", false);
            },
            error: function(data){
                $("#id_posicion").prop("disabled", false);
            }
        });
    });

    function AgregarItem(){
        var id_item = $("#id_item").val();
        var cantidad = $("#cantidad").val();
        var precio = $("#precio").val();
        var existe = false;
        total = 0;

        if (!id_item || !cantidad) {
            iziToast.warning({
                title: 'Atencion!',
                message: 'Debe ingresar articulo y cantidad.'
            });
        } else {
            $data = $('#table1').bootstrapTable('getData');
            $data.forEach(function(element) {
                if(element.id_item === id_item){
                    iziToast.question({
                        timeout: 20000,
                        close: false,
                        overlay: true,
                        displayMode: 'once',
                        id: 'question',
                        zindex: 999,
                        title: 'Atencion! Ya ingreso ese Producto.',
                        message: '¿Sumamos las cantidades?',
                        position: 'center',
                        buttons: [
                            ['<button><b>SI</b></button>', function (instance, toast) {
                                element.cantidad = Number(element.cantidad) + Number(cantidad);
                                element.precio = Number(precio).toFixed(2);
                                element.total = (Number(element.cantidad) * Number(precio)).toFixed(2);
                                $('#table1').bootstrapTable('updateByUniqueId', element);
                                total = total + Number(element.total);
                                document.querySelector('.cltotal').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                            }, true],
                            ['<button>CANCELAR</button>', function (instance, toast) {

                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                            }],
                        ],
                        onClosing: function(instance, toast, closedBy){
                            console.info('Closing | closedBy: ' + closedBy);
                        },
                        onClosed: function(instance, toast, closedBy){
                            console.info('Closed | closedBy: ' + closedBy);
                        }
                    });
                    existe=true;
                }else{
                    total = total + Number(element.total);
                }
            });

            if (!existe){
                $('#table1').bootstrapTable('showLoading');
                var newreg = '';
                newreg+= '"id_item":"'+ id_item + '",';
                var combo = document.getElementById("id_item");
                var producto = combo.options[combo.selectedIndex].text;
                var a = producto.indexOf("CodProv:");
                var b = producto.lastIndexOf("]");
                var codprov = producto.substring(a+8,b);
                var re = /\[.+?]/g;
                var producto = producto.replace(re,'');
                newreg+= '"codprov":"'+ codprov+'",';
                newreg+= '"producto":"'+ producto+'",';
                newreg+= '"cantidad":"'+ cantidad+'",';
                newreg+= '"precio":"'+ precio+'",';
                importe = cantidad*precio;
                newreg+= '"total":"'+ importe.toFixed(2) +'",';
                newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
                var newregarray=JSON.parse('{'+newreg+'}');
                $('#table1').bootstrapTable('append', newregarray);
                $('#table1').bootstrapTable('hideLoading');
                total = total + cantidad*precio;
                document.querySelector('.cltotal').innerHTML =  total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
            }
        }
    }

    function moneyFormatter(value, row, index) {
        return  "$ "+value;
    };

    function FunctionsFormatter(value, row, index) {
        return  "<a class='text-danger' href='#' onclick='BorrarItem("+row.id_item+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>";
    };

    function BorrarItem(id){
        row = $('#table1').bootstrapTable('getRowByUniqueId', id);
        total = total - row.total;
        document.querySelector('.cltotal').innerHTML = '$ '+total;
        $('#table1').bootstrapTable('removeByUniqueId', id);
    }

    $('#form1').submit(function (event) {
        var $detalle = $('#table1').bootstrapTable('getData');
        if (($detalle.length)==0){
            iziToast.warning({
                title: 'Atencion!',
                message: 'Debe ingresar al menos un articulo.'
            });
            event.preventDefault();
        }

        var params = [
            {
                name: "items",
                value: JSON.stringify($('#table1').bootstrapTable('getData'))
            }
        ];
        $("#id_titular").attr('disabled', false);
        $("#id_cpterel").prop("disabled", false);
        $(this).append($.map(params, function (param) {
            return   $('<input>', {
                type: 'hidden',
                name: param.name,
                value: param.value
            })
        }))
    });
</script>
@include('utils.newprodmodaljs')
@include('utils.statusnotification')
@stop
