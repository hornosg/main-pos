<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>MainPOS</title>
    <style>
        body{
            font-size: 11px;
        }
        table{
            width: 100%;
            border-collapse: collapse;
        }
        td{
            font-size: 9px;
            border: 1px solid;
            vertical-align: top;
        }
    </style>
</head>
<body>
    <table>
        <tr>
            <td style="height:6%; width: 50%; text-align: center;">
                <img src="/img/Logos/logo.png" style="width: 150px;height: auto;padding: 1%;text-align: center; background-color: #fff">
                <p>{{$sucursal->descripcion}} -  {{$sucursal->direccion}} - {{$sucursal->localidad->descripcion}} - Tel:{{$sucursal->telefono}}</p>
            </td>
            <td style="height:6%">
                @php
                $date = new DateTime($cpte->created_at);
                $fecha =  $date->format('d-m-Y');
                @endphp
                <p style="font-weight:bold;padding-top:0px;padding-left: 5%;">PRESUPUESTO [X]<span style="float: right;padding-right: 5%;">- Documento no valido como factura -</span></p><br/>
                <p style="font-size:x-large;font-weight:bold;padding-left: 5%;">N°:{{$cpte->numero}}<span style="font-size:medium;padding-left: 25%">Fecha: {{$fecha}}</span></p>
            </td>
        </tr>
        <tr>
            <td style="height:4%;padding-left: 10px;" colspan="2">
                @if (empty($ref->descripcion))
                    <p style="padding-left: 5%">Cliente: {{$titular->descripcion}}<span style="margin-left:240px;"></span></p>
                @else
                    <p style="padding-left: 5%">Cliente: {{$titular->descripcion}}<span style="margin-left:200px;">Profesional: {{$ref->descripcion}}</span></p>
                @endif
                <p></p>
                <p style="padding-left: 5%">Direccion: {{$titular->direccion}} <span style="margin-left:280px;">Telefono: {{$titular->telefono}}</span></p>
            </td>
        </tr>
        <tr>
            <td style="height:287px;" colspan="2">
                <table>
                    <tr>
                        <td style="text-align: right;padding-right: 10px;width:6%">CANT</td>
                        <td style="padding-left: 10px;width:44%">PRODUCTO</td>
                        <td style="text-align: right;padding-right: 20px;width:20%">PRECIO U.</td>
                        <td style="text-align: right;padding-right: 20px;width:20%">IMPORTE</td>
                    </tr>
                    @php
                        $subtotal = 0;
                    @endphp
                    @foreach ($prods as $prod)
                        <tr>
                            <td style="text-align: right;padding-right: 10px;width:6%">{{$prod->cantidad}}</td>
                            <td style="padding-left: 10px;width:44%">({{$prod->id_item}}) {{$prod->producto}}</td>
                            <td style="text-align: right;padding-right: 20px;width:20%">{{$prod->precio}}</td>
                            <td style="text-align: right;padding-right: 20px;width:20%">{{$prod->total}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td style="border: none;"></td>
                        <td style="border: none;"></td>
                        <td style="text-align: right;font-weight: bold;">TOTAL</td>
                        <td style="text-align: right;padding-right: 10px;font-weight: bold;">$ {{$cpte->total}}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2"><span>LOS PRESENTES VALORES <bold>VENCEN EN 15 DIAS</bold></span></td>
        </tr>
    </table>
    <br/>
    <table>
        <tr>
            <td style="height:6%; width: 50%; text-align: center;">
                <img src="/img/Logos/logo.png" style="width: 150px;height: auto;padding: 1%;text-align: center; background-color: #fff">
                <p>{{$sucursal->descripcion}} -  {{$sucursal->direccion}} - {{$sucursal->localidad->descripcion}} - Tel:{{$sucursal->telefono}}</p>
            </td>
            <td style="height:6%">
                @php
                $date = new DateTime($cpte->created_at);
                $fecha =  $date->format('d-m-Y');
                @endphp
                <p style="font-weight:bold;padding-top:0px;padding-left: 5%;">PRESUPUESTO [X]<span style="float: right;padding-right: 5%;">- Documento no valido como factura -</span></p><br/>
                <p style="font-size:x-large;font-weight:bold;padding-left: 5%;">N°:{{$cpte->numero}}<span style="font-size:medium;padding-left: 25%">Fecha: {{$fecha}}</span></p>
            </td>
        </tr>
        <tr>
            <td style="height:4%;padding-left: 10px;" colspan="2">
                @if (empty($ref->descripcion))
                <p style="padding-left: 5%">Cliente: {{$titular->descripcion}}<span style="margin-left:240px;"></span></p>
                @else
                <p style="padding-left: 5%">Cliente: {{$titular->descripcion}}<span style="margin-left:200px;">Profesional: {{$ref->descripcion}}</span></p>
                @endif
                <p></p>
                <p style="padding-left: 5%">Direccion: {{$titular->direccion}} <span style="margin-left:280px;">Telefono: {{$titular->telefono}}</span></p>
            </td>
        </tr>
        <tr>
            <td style="height:287px;" colspan="2">
                <table>
                    <tr>
                        <td style="text-align: right;padding-right: 10px;width:6%">CANT</td>
                        <td style="padding-left: 10px;width:44%">PRODUCTO</td>
                        <td style="text-align: right;padding-right: 20px;width:20%">PRECIO U.</td>
                        <td style="text-align: right;padding-right: 20px;width:20%">IMPORTE</td>
                    </tr>
                    @php
                    $subtotal = 0;
                    @endphp
                    @foreach ($prods as $prod)
                    <tr>
                        <td style="text-align: right;padding-right: 10px;width:6%">{{$prod->cantidad}}</td>
                        <td style="padding-left: 10px;width:44%">({{$prod->id_item}}) {{$prod->producto}}</td>
                        <td style="text-align: right;padding-right: 20px;width:20%">{{$prod->precio}}</td>
                        <td style="text-align: right;padding-right: 20px;width:20%">{{$prod->total}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td style="border: none;"></td>
                        <td style="border: none;"></td>
                        <td style="text-align: right;font-weight: bold;">TOTAL</td>
                        <td style="text-align: right;padding-right: 10px;font-weight: bold;">$ {{$cpte->total}}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2"><span>LOS PRESENTES VALORES <bold>VENCEN EN 15 DIAS</bold></span></td>
        </tr>
    </table>
</body>
</html>