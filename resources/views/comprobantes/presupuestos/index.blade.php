@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.presupuestosnavbar')
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-default collapsed-box">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <span style="margin-right: 10px;">Busqueda Avanzada</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: none;">
                <div class="form-group col-md-2">
                    <label for="id_titular" class="control-label">Cliente</label><div id="loading2" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>
                    <select id="id_titular" name="id_titular" class="form-control text-uppercase">
                    <option value=""></option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="id_item" class="control-label">Producto</label><div id="loading" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>
                    <select id="id_item" name="id_item" class="form-control text-uppercase">
                        <option value=""></option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label>Desde/Hasta Fecha:</label>

                    <div class="input-group">
                        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                </div>
<!--                <div class="clearfix visible-lg-block visible-md-block"></div>-->
                <div class="pull-right mt-20">
                    <button id="buscar" type="button" class='btn btn-primary' onclick='BuscarItems()' data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>"> <i class="fa fa-btn fa-search text-white"></i> Buscar</button>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-xs-12">
        <div class="box box-primary">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <a href="/cptes/create/pre" class="btn btn-primary"  title="Nuevo Presupuesto">
                                    <i class="fa fa-btn fa-plus text-white"></i>
                                </a>
                                <button id="btn-eliminar" class="btn btn-danger"  title="Eliminar Seleccion">
                                    <i class="fa fa-btn fa-trash text-white"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-detail-view='true'
                               data-unique-id='id'>

                            <thead class='thead-inverse'>
                                <th data-checkbox='true'></th>
                                <th data-field='id' data-align='right' class='hidden text-uppercase'>Id</th>
                                <th data-field='pasaron' data-align='right' class='hidden'>pasaron</th>
                                <th data-field='creado' data-align='right'  class='text-uppercase'>Creado</th>
                                <th data-field='numero'>NUMERO</th>
                                <th data-field='id_titular' class='hidden'></th>
                                <th data-field='titular'  data-formatter="clienteFormatter" class='text-uppercase  text-bold'>Cliente</th>
                                <th data-field='total' data-align='right' data-formatter="moneyFormatter" class='text-uppercase text-bold'>Importe</th>
                                <th data-field='usuario' data-align='left' class='text-uppercase'>Usuario</th>
                                <th data-field='id_estado' data-align='left' data-formatter='FunctionsStatusFormatter'>ESTADO</th>
                                <th data-formatter='FunctionsFormatter'></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer ptb-10">
                <div class="pull-right pr-5">
                    <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<!--<link href="/css/daterangepicker.css" rel="stylesheet">-->
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<style>
    #loading,#loading2{
        display: inline !important;
    }
    .pt-20{
        padding-top: 20px;
    }
</style>
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="/vendor/select2/js/select2.full.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    $("#id_titular").select2({ placeholder: "Buscar/Seleccionar Cliente", allowClear: true });
    $("#id_item").select2({
        placeholder: "Buscar/Seleccionar Producto",
        allowClear: true,
        minimumInputLength: 6
    });

    $(function() {
        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);
    });

    var datos = <?php echo json_encode($items); ?>;
    $(document.body).addClass('sidebar-collapse');
    var $table = $('#table1');
    $table.bootstrapTable('showLoading');
    $table.bootstrapTable({
        data: datos,
        exportDataType:'all',
        exportOptions:{fileName: 'items'}
    });
    $table.bootstrapTable('hideLoading');

    $table.on('expand-row.bs.table', function (e, index, row, $detail) {
        $idtabla2="tabla"+row.id;
        html='<div class="box"><table id='+$idtabla2+' class="table table-sm table-bordered table-hover table-striped table-condensed table-responsive" ' +
            'data-search="true" data-strict-search="false" data-multiple-search="true" data-pagination="true">' +
            '<thead class="thead-inverse-gray"><tr>' +
            '<th data-field="id_item" data-align="right">Codigo</th>' +
            '<th data-field="descripcion">Descripcion</th>' +
            '<th data-field="marca">Marca</th>' +
            '<th data-field="preciouni" data-align="right">Precio Unitario</th>' +
            '<th data-field="cantidad" data-align="right">Cantidad</th>' +
            '</tr></thead>' +
            '</table></divS>';
        $detail.html(html);
        $('#'+$idtabla2).bootstrapTable();
        $('#'+$idtabla2).bootstrapTable('showLoading');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'/cptes/detalle/'+row.id,
            data: row.id,
            success: function(datosdetalle) {
                $('#'+$idtabla2).bootstrapTable('append', datosdetalle);
                $('#'+$idtabla2).bootstrapTable('hideLoading');
            }
        });
    });

    function clienteFormatter(value, row, index) {
        if (row.id_titular===2){
            return  "<a href='/clientes/cuentacorriente/"+row.id_titular+"'>"+value+"</a>";
        }else{
            return  value;
        }

    };

    function moneyFormatter(value, row, index) {
        return  "$ "+value;
    };

    function FunctionsFormatter(value, row, index) {
        var links = '';
        if (row.pasaron<16){
            if (row.id_estado===2){
                links ="<a class='btn-outline-danger' href='#' onclick='Borrar("+row.id+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
                       "<a class='btn-outline-warning' href='/cptes/edit/pre/"+row.id+"'><i class='fa fa-fw fa-pencil'></i></a>";
            }
        }
        links = links + "<a class='btn-outline-primary' href='/cptes/show/pre/"+row.id+"'><i class='fa fa-fw fa-search'></i></a>" +
                        "<a class='btn-outline-primary' href='/cptes/print/pre/"+row.id+"'><i class='fa fa-fw fa-print'></i></a>";

        return links;
    }

    function FunctionsStatusFormatter(value, row, index) {
        if(row.id_estado===2){
            return '<span class="badge bg-red">PENDIENTE</span>';
        }
    };

    $("#buscar").click(function(){
        $(this).button('loading');
        $table.bootstrapTable('showLoading');
    });
    function BuscarItems(){
        var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        var comboitem = document.getElementById("id_item");
        var iditem = comboitem.options[comboitem.selectedIndex].value;
        var combomp = document.getElementById("id_mediopago");
        var idmediopago = combomp.options[combomp.selectedIndex].value;
        $.ajax('/cptes/gethistorico', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : { idtipo: 16, idcliente : idcliente, iditem: iditem, desde:startDate, hasta:endDate},
            success: function(data) {
                if (data.length>0){
                    $table.bootstrapTable('removeAll');
                    $.each(data, function(index, value) {
                        var newreg = '';
                        newreg+= '"id":"'+ data[index].id + '",';
                        newreg+= '"pasaron":"'+ data[index].pasaron +'",';
                        newreg+= '"creado":"'+ data[index].creado +'",';
                        newreg+= '"numero":"'+ data[index].numero +'",';
                        newreg+= '"id_titular":"'+ data[index].id_titular +'",';
                        newreg+= '"titular":"'+ data[index].titular +'",';
                        newreg+= '"total":"'+ data[index].total +'",';
                        newreg+= '"usuario":"'+ data[index].usuario +'",';
                        newreg+= '"id_estado":'+ data[index].id_estado;
                        var newregarray=JSON.parse('{'+newreg+'}');
                        $table.bootstrapTable('append', newregarray);
                        $table.bootstrapTable('hideLoading');
                        $("#buscar").button('reset');
                        $("#buscar").dequeue();
                        $("#buscar").prop("disabled", false);
                    });
                }else{
                    $table.bootstrapTable('removeAll');
                    $("#buscar").button('reset');
                    $("#buscar").dequeue();
                    $("#buscar").prop("disabled", false);
                    $('#table1').bootstrapTable('hideLoading');
                }
            },
            error:function(data) {
                console.log(data);
                iziToast.error({
                    timeout: 2000,
                    position:'center',
                    title: 'Error:',
                    message: data.responseJSON.message
                });
                $("#buscar").button('reset');
                $("#buscar").dequeue();
                $("#buscar").prop("disabled", false);
                $('#table1').bootstrapTable('hideLoading');
            }
        });
    };

    $(window).on('load', function(event){
        @if (session('status'))
            iziToast.success(
            {
                timeout: 4000,
                position:'topRight',
                title: 'Listo!',
                message: '{{session('status')}}',
                buttons: [
                    ['<button class="text-bold"><i class="fa fa-btn fa-print"></i> Imprimir</button>', function (instance, toast, button, e, inputs) {
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                        window.location.href = '/cptes/print/pre/'+{{session('lastid')}};
                    }]
                ]
            });
        @endif

        $("#loading").button('loading');
        $("#loading").removeClass('hidden');
        $.ajax('/datos/getproductos', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $.each(data, function(index) {
                    $("#id_item").append(
                        '<option value="' + data[index].id + '">' + data[index].producto + '</option>'
                    );
                });
                $("#id_item").prop("disabled", false);
                $("#loading").button('reset');
                $("#loading").addClass('hidden');
            }
        });
        $("#loading2").button('loading');
        $("#loading2").removeClass('hidden');
        $.ajax('/datos/gettitulares/0', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $.each(data, function(index) {
                    $("#id_titular").append(
                        '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                    );
                });
                $("#loading2").button('reset');
                $("#loading2").addClass('hidden');
            }
        });
    });
</script>
{{--@include('utils.borrarcpte')--}}
@include('utils.borrarcptes')
{{--@include('utils.statusnotification')--}}
@stop
