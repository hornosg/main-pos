@extends('adminlte::page')

@section('title', $titulo)

@php
if ($edit==0){
    $editable='readonly';
    $visible='d-none';
    $disabled='disabled';
    $accion = 'Visualizar';
}else{
    $editable=' ';
    $visible=' ';
    $disabled=' ';
    $accion = 'Editar';
}

if (!empty($prods)){
    $total = 0;
    $datos = json_encode($prods);
    foreach ($prods as $prod){
        $total = $total + $prod->total;
    }
}else{
    $datos = "[]";
    $total = 0;
}

if (!empty($descints)){
    //dd($descints);
    $totaldi = 0;
    $datosdi = json_encode($descints);
    foreach ($descints as $descint){
        $totaldi = $totaldi + $descint->importe;
    }
}else{
    $datosdi = "[]";
    $totaldi = 0;
}

if (!empty($mps)){
    $totalmp = 0;
    $datosmp = json_encode($mps);
    foreach ($mps as $mp){
        $totalmp = $totalmp + $mp->importe;
    }
}else{
    $datosmp = "[]";
    $totalmp = 0;
}

if (empty($cpte)){
    $totalcpte=0;
    $referente=0;
}else{
    $totalcpte=$cpte->total;
    $referente=$cpte->id_referente;
}
@endphp

@section('content_header')
    @include('layouts.presupuestosnavbar')
@stop

@section('content')
<div id="content" class="container-fluid row">
    <form id="form1" class="" role="form" method="POST" action="/cptes/store/pre" enctype="multipart/form-data">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <strong>{{$tituloform}} N°</strong>
                        @if (!empty($cpte))
                            {{$cpte->numero}}
                        @endif
                    </h3>
                </div>
                <div class="box-body">
                    {{ csrf_field() }}

                    @if (!empty($cpte))
                        <input id="id" type = "hidden" name = "id" value = "{{$cpte->id}}">
                    @else
                        <input type = "hidden" name = "id">
                    @endif

                    <div class="form-group col-md-12">
                        <label for="id_titular" class="control-label">Cliente</label>
                        <select id="id_titular" name="id_titular" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @if (!empty($cpte))
                                @foreach(DB::table('titulares')->where('id_tipotitular', 2)->where('activo', 1)->whereNull('deleted_at')->get() as $tipotitular)
                                    @if ( $cpte->id_titular == $tipotitular->id )
                                        <option value="{{ $tipotitular->id }}" selected>{{ $tipotitular->descripcion }}</option>
                                    @else
                                        <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                    @endif
                                @endforeach
                            @else
                                @foreach(DB::table('titulares')->where('id_tipotitular', 2)->where('activo', 1)->get() as $tipotitular)
                                    <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="id_referente" class="control-label">Profesional / Referente</label><div id="loading2" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>
                        <select id="id_referente" name="id_referente" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                        </select>
                    </div>
                    @if ($edit==1)
                        <div class="form-inline col-md-12">
                            <label for="recargo" class="control-label col-md-12">Aj Precios</label>
                                <div class="form-check form-check-inline col-md-4">
                                    <input class="form-check-input" type="radio" name="id_ajuste" id="0" value="">
                                    <label class="form-check-label" for="ajuste0">Ninguno</label>
                                </div>
                            @foreach(DB::table('conf_ajustes_precio')->where('activo', 1)->get() as $ajuste)
                                <div class="form-check form-check-inline col-md-4">
                                    <input class="form-check-input" type="radio" name="id_ajuste" id="ajuste{{$ajuste->id}}" value="{{$ajuste->id}}">
                                    <label class="form-check-label" for="ajuste{{$ajuste->id}}">{{$ajuste->descripcion}}
                                        <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="{{$ajuste->nota}} ({{$ajuste->porcentaje*$ajuste->signo}}%)">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    @if (!empty($cpte))
                        <fieldset class="form-group col-md-12">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Alta]</label>
                                @php $uscr=DB::table('users')->where('id', $cpte->created_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{  $uscr->name }} {{ date_format($cpte->created_at, 'd/m/Y H:i:s') }}" readonly>
                        </fieldset>
                        @if (!empty($cpte->updated_us))
                        <fieldset class="form-group col-md-12">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Modificacion]</label>
                                @php $usup=DB::table('users')->where('id', $cpte->updated_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{ $usup->name }} {{ date_format($cpte->updated_at, 'd/m/Y H:i:s') }}" readonly>
                        </fieldset>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>{{$titulodetart}}</strong></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="field-group col-md-12">
                            <div id="addprodform">
                                @if ($edit==1)
                                    <div class="form-group col-md-7">
                                        <label for="id_item" class="control-label">Producto</label><div id="loading" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>
                                        <a class='text-primary pull-right' href='#' data-toggle='modal' data-target='#prodModal' title='Crear Nuevo Producto'>Crear Nuevo</i></a>
                                        <select id="id_item" name="id_item" class="form-control text-uppercase" {{$disabled}}>
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <label for="cantidad" class="control-label">Cantidad</label>
                                        <input id="cantidad"  class="form-control input-sm text-uppercase" name="cantidad"  value="1" {{$editable}}>
                                    </div>
                                    <div class="col-md-1 pt-21">
                                        <button id="addprodbtn" type="button" class='btn btn-primary' onclick='AgregarItem()' title="Agregar Producto" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                                            <i class="fa fa-btn fa-plus text-white"></i>
                                        </button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                   data-toggle='table'
                                   data-total-field='total'
                                   data-unique-id='id_item'>
                                <thead class='thead-inverse'>
                                    <th data-formatter="runningFormatter"></th>
                                    <th data-field='id' data-align='right' class='text-uppercase hidden'></th>
                                    <th data-field='id_item' data-align='right' class='hidden text-uppercase'>IdItem</th>
                                    <th data-field='producto' class='text-uppercase col-md-6'>Descripcion</th>
                                    <th data-field='cantidad' data-align='right' class='text-uppercase col-md-1'>Cant</th>
                                    <th data-field='precio' data-align='right' class='col-md-2' data-formatter="moneyFormatter">PRECIO UNI.</th>
                                    <th data-field='total'  data-align='right' class='col-md-2' data-formatter="moneyFormatter">TOTAL</th>
                                    @if ($edit==1)
                                        <th data-align='center' data-formatter='FunctionsFormatter'></th>
                                    @endif
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        @if ($edit==1)
                                        <td></td>
                                        @endif
                                        <th>$ <span data-align='right' class='cltotal pull-right'></span></th>
                                        @if ($edit==1)
                                            <td></td>
                                        @endif
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 ">
                    <div class="pull-right pr-10">
                        @if ($edit==1)
                        <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                        <button id="btnsubmit" type="submit" name="action" value="save" class="btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                        </button>
                        @else
                        <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@include('utils.newprodmodal')
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<style>
    #addprodform > div.form-group.col-md-7{
        padding-left: 0 !important;
    }
    #addprodform > div.form-group.col-md-2, #addprodform > div.form-group.col-md-1{
        padding-right: 0 !important;
    }
    #loading{
        display: inline !important;
    }
</style>
@stop

@section('js')
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/vendor/select2/js/select2.full.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    $(document.body).addClass('sidebar-collapse');
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
    var listener = new window.keypress.Listener();
    var $referente = Number(<?php echo $referente; ?>);

    var datos = <?php echo $datos; ?>;
    $('#table1').bootstrapTable({data: datos});
    var total = <?php echo $total; ?>;
    document.querySelector('.cltotal').innerHTML = total.toFixed(2);

    $("#id_titular").select2({ placeholder: "Buscar/Seleccionar Cliente", allowClear: true });
    $("#id_referente").select2({ placeholder: "Buscar/Seleccionar Referente", allowClear: true });
    $("#id_item").select2({
        placeholder: "Buscar/Seleccionar Producto",
        allowClear: true,
        minimumInputLength: 4,
        ajax: {
            url: '/datos/getproductos_v2',
            dataType: 'json'
        }
    });

    listener.simple_combo("shift c", function() {
        $('#id_titular').select2('open');
    });
    listener.simple_combo("shift a", function() {
        $('#id_item').select2('open');
    });
    listener.simple_combo("shift q", function() {
        $('#cantidad').focus();
    });
    listener.simple_combo("shift enter", function() {
        AgregarItem();
    });
    listener.simple_combo("shift g", function() {
        $('#form1').submit();
    });

    function moneyFormatter(value, row, index) {
        return  "$ "+value;
    };

    function FunctionsFormatter(value, row, index) {
        return  "<a class='text-danger' href='#' onclick='BorrarItem("+row.id_item+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>";
    };

    $("#id_titular").change(function(){
        ActualizarPrecios();

        var $items = $('#table1').bootstrapTable('getData');
        if (($items.length)>0){
            $('#pagosvarios').removeClass("d-none");

            var combocliente = document.getElementById("id_titular");
            var idcliente = combocliente.options[combocliente.selectedIndex].value;
            if (idcliente != 2){
                $('#pagoctacte').removeClass("d-none");
            }else{
                $('#pagoctacte').addClass("d-none");
            }
            $('#pagoefectivo').removeClass("d-none");
            HabilitarAgregarProds();
        }
    });    

    $('input[name="id_ajuste"]').click(function() {
        ActualizarPrecios();
        HabilitarAgregarProds();
    });
    
    function DeshabilitarAgregarProds(){
        $('#sugerencias').addClass("d-none");
        $('#addprodform').addClass("d-none");
    }

    function HabilitarAgregarProds(){
        $('#sugerencias').removeClass("d-none");
        $('#addprodform').removeClass("d-none");
    }

    function ActualizarPrecios(){
        var total = 0;

        $('#table1').bootstrapTable('showLoading');
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        var idajuste =  $('input[name="id_ajuste"]:checked').val();
        $data = $('#table1').bootstrapTable('getData');
        $data.forEach(function(element) {
            $.ajax('/productos/precio/'+element.id_item, {
                type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data : { idcliente : idcliente, idajuste: idajuste },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                success: function(precio) {
                    element.precio = Number(precio).toFixed(2);
                    element.total = Number(element.cantidad * precio).toFixed(2);
                    total = total + Number(element.total);
                    $('#table1').bootstrapTable('updateByUniqueId', element);;
                    document.querySelector('.cltotal').innerHTML =  total;
                }
            });
        });
        $('#table1').bootstrapTable('hideLoading');
    }

    function AgregarItem(){
        $('#addprodbtn').button('loading');
        var idproducto = $("#id_item").val();
        var origen = $("#origen").val();
        var cantidad = $("#cantidad").val();

        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;

        var idajuste =  $('input[name="id_ajuste"]:checked').val();

        var yaexiste = false;
        total = 0;

        if (!idproducto || !cantidad) {
            iziToast.warning({
                title: 'Atencion!',
                message: 'Debe ingresar Producto y cantidad.'
            });
        } else {
            $data = $('#table1').bootstrapTable('getData');
            $data.forEach(function(element) {
                if (element.id_item === idproducto){
                    iziToast.question({
                        timeout: 20000,
                        close: false,
                        overlay: true,
                        displayMode: 'once',
                        id: 'question',
                        zindex: 999,
                        title: 'Atencion! Ya ingreso ese Producto.',
                        message: '¿Sumamos las cantidades?',
                        position: 'center',
                        buttons: [
                            ['<button><b>SI</b></button>', function (instance, toast) {
                                element.cantidad = Number(element.cantidad) + Number(cantidad);
                                element.total = Number(element.cantidad * element.precio).toFixed(2);
                                $('#table1').bootstrapTable('updateByUniqueId', element);
                                total = total + Number(element.total);
                                document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
                                $("#addprodbtn").button('reset');
                                $("#addprodbtn").dequeue();
                                $('.overlay').addClass('d-none');
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                            }, true],
                            ['<button>CANCELAR</button>', function (instance, toast) {
                                $("#addprodbtn").button('reset');
                                $("#addprodbtn").dequeue();
                                $('.overlay').addClass('d-none');
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                            }]
                        ],
                        onClosing: function(instance, toast, closedBy){
                            $("#addprodbtn").button('reset');
                            $("#addprodbtn").dequeue();
                            $('.overlay').addClass('d-none');
                            console.info('Closing | closedBy: ' + closedBy);
                        },
                        onClosed: function(instance, toast, closedBy){
                            $("#addprodbtn").button('reset');
                            $("#addprodbtn").dequeue();
                            $('.overlay').addClass('d-none');
                            console.info('Closed | closedBy: ' + closedBy);
                        }
                    });
                    yaexiste=true;
                }else{
                    total = total + Number(element.total);
                }
            });

            if (!yaexiste){
                $('#table1').bootstrapTable('showLoading');
                $.ajax('/productos/precio/'+idproducto, {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data : { idcliente : idcliente, idajuste: idajuste },
                    success: function(precio) {
                        var newreg = '';
                        var combo = document.getElementById("id_item");
                        var idprod = combo.options[combo.selectedIndex].value;
                        newreg+= '"id_item":"'+ idprod + '",';
                        var producto = combo.options[combo.selectedIndex].text;
                        var re = /\$.+?]/g;
                        var producto = producto.replace(re,'');
                        newreg+= '"producto":"'+ producto+'",';
                        newreg+= '"origen":"'+ origen+'",';
                        newreg+= '"cantidad":"'+ cantidad+'",';
                        newreg+= '"precio":"'+ precio +'",';
                        importe = cantidad*precio;
                        newreg+= '"total":"'+ importe.toFixed(2) +'",';
                        newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
                        var newregarray=JSON.parse('{'+newreg+'}');
                        $('#table1').bootstrapTable('append', newregarray);
                        $('#table1').bootstrapTable('hideLoading');
                        total = total + cantidad*precio;
                        document.querySelector('.cltotal').innerHTML =  total.toFixed(2);

                        $("#addprodbtn").button('reset');
                        $("#addprodbtn").dequeue();
                        $('.overlay').addClass('d-none');
                    }
                });
                $.ajax('/productos/sugerencias/'+idproducto, {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(productos) {
                        $('#table2').bootstrapTable('append', productos);
                        $('#table2').bootstrapTable('removeByUniqueId', idproducto);
                        HabilitarAgregarProds();
                    }
                });
            }

            EliminarDescMp();
        }
        $('#cantidad').val(1);
        scroll_to_anchor('content');
    }

    function BorrarItem(id){
        row = $('#table1').bootstrapTable('getRowByUniqueId', id);
        total = total - Number(row.total);
        document.querySelector('.cltotal').innerHTML =  total.toFixed(2);
        $('#table1').bootstrapTable('removeByUniqueId', id);
        scroll_to_anchor('content');
    }

    $('#form1').validate({
        rules: {
            id_titular: {required: true}
        },
        messages: {
            id_titular: {required: 'Ingrese un Cliente'}
        }
    });

    $('#form1').submit(function (event) {
        var $detalle = $('#table1').bootstrapTable('getData');
        if (($detalle.length)==0){
            iziToast.warning({
                title: 'Atencion!',
                message: 'Debe ingresar al menos un Producto.'
            });
            $("#btnsubmit").button('reset');
            $("#btnsubmit").dequeue();
            $('.overlay').addClass('d-none');
            event.preventDefault();
        }else{
            var combocliente = document.getElementById("id_titular");
            var idcliente = combocliente.options[combocliente.selectedIndex].value;
            if (idcliente>0){
                $('#btnsubmit').button('loading');
            }
        }

        /* Recalculo Total Productos */
        var prods = $('#table1').bootstrapTable('getData');
        totalprods = 0;
        prods.forEach(function(element) {
            totalprods = totalprods + Number(element.total);
        });

        var params = [
            {
                name: "items",
                value: JSON.stringify($('#table1').bootstrapTable('getData'))
            }
        ];
        $(this).append($.map(params, function (param) {
            return   $('<input>', {
                type: 'hidden',
                name: param.name,
                value: param.value
            })
        }))
    });


    function replaceAll( text, busca, reemplaza ){
        while (text.toString().indexOf(busca) != -1)
            text = text.toString().replace(busca,reemplaza);
        return text;
    }

    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }

    $(window).on('load', function(event){
        {{--@if (session('status'))--}}
            {{--iziToast.success(--}}
                {{--{--}}
                {{--timeout: 4000,--}}
                {{--position:'topRight',--}}
                {{--title: 'Listo!',--}}
                {{--message: '{{session('status')}}',--}}
                {{--buttons: [--}}
                    {{--['<button class="text-bold"><i class="fa fa-btn fa-print"></i> Imprimir</button>', function (instance, toast, button, e, inputs) {--}}
                        {{--instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');--}}
                        {{--window.location.href = '/cptes/print/pre/'+{{session('lastid')}};--}}
                    {{--}]--}}
                {{--]--}}
            {{--});--}}
        {{--@endif--}}

        $("#loading2").button('loading');
        $("#loading2").removeClass('hidden');
        $.ajax('/datos/gettitulares/0', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $.each(data, function(index) {
                    if ($referente===data[index].id){
                        $("#id_referente").append(
                            '<option value="' + data[index].id + '" selected>' + data[index].descripcion + '</option>'
                        );
                    }else{
                        $("#id_referente").append(
                            '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                        );
                    }
                });
                var $disabled = '{{$disabled}}';
                if ($disabled.length==0){
                    $("#id_referente").prop("disabled", false);
                }
                $("#loading2").button('reset');
                $("#loading2").addClass('hidden');
            }
        });
    });
    function runningFormatter(value, row, index) {
        return index+1;
    }
</script>
@include('utils.newprodmodaljs')
@stop
