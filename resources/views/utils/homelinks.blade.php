<div class="hidden-xs link-box bg-white"><a href="/caja"><span class="info-box-icon bg-white text-maroon"><i class="fa fa-inbox"></i></span><h4 class="text-center text-maroon text-bold">Caja</h4></a></div>
@if ( (Auth::user()->id_rol==1) or (Auth::user()->id_rol==2) )
<div class="hidden-xs link-box bg-gray-light"><a href="#"><span class="info-box-icon bg-gray-light text-gray"><i class="fa fa-calculator"></i></span><h4 class="text-center text-gray text-bold">Gastos</h4></a></div>
@endif
@if (Auth::user()->id_rol==1)
    @if (env('MP_CHEQUES'))
    <div class="hidden-xs link-box bg-green-active"><a href="/cheques"><span class="info-box-icon bg-green-active text-white"><i class="fa fa-money"></i></span><h4 class="text-center text-white text-bold">Cheques</h4></a></div>
    @endif
@endif
<div class="hidden-xs link-box bg-orange-active"><a href="/ventas"><span class="info-box-icon bg-orange-active"><i class="fa fa-trophy"></i></span><h4 class="text-center text-white text-bold">Ventas</h4></a></div>

<div class="hidden-xs link-box bg-purple-active"><a href="/productos"><span class="info-box-icon bg-purple-active"><i class="material-icons md-48 fs-xlarge">ballot</i></span><h4 class="text-center text-white text-bold">Productos</h4></a></div>
@if ( (Auth::user()->id_rol==1) or (Auth::user()->id_rol==2) )
<div class="hidden-xs link-box bg-orange-active"><a href="/precios"><span class="info-box-icon bg-orange-active"><i class="fa fa-tags"></i></span><h4 class="text-center text-white text-bold">Precios</h4></a></div>
@endif

<div class="link-box bg-black text-white"><a href="/productos/eans"><span class="info-box-icon bg-black text-white"><i class="fa fa-barcode"></i></span><h4 class="text-center text-red text-bold">Cod.Barras</h4></a></div>

@if (env('APP_PLAN')>1)
    @if (Auth::user()->id_rol==1)
    <div class="hidden-xs link-box bg-teal"><a href="/inventario"><span class="info-box-icon bg-teal"><i class="fa fa-puzzle-piece"></i></span><h4 class="text-center text-white text-bold">Inventario</h4></a></div>
    @endif
    <div class="link-box bg-orange-active"><a href="/inventario/movimientos/index"><span class="info-box-icon bg-orange-active"><i class="fa fa-exchange"></i></span><h4 class="text-center text-white text-bold">Mov. Stock</h4></a></div>
    <div class="link-box bg-red-active"><a href="/inventario/controles/index"><span class="info-box-icon bg-red-active"><i class="glyphicon glyphicon-check"></i></span><h4 class="text-center text-white text-bold">Ctrol. Stock</h4></a></div>
    <div class="hidden-xs link-box bg-teal"><a href="/inventario/stock/index"><span class="info-box-icon bg-teal"><i class="fa fa-cubes"></i></span><h4 class="text-center text-white text-bold">Stock</h4></a></div>
@endif
<div class="hidden-xs link-box bg-purple-active"><a href="/clientes"><span class="info-box-icon bg-purple-active"><i class="fa fa-fw fa-users"></i></span><h4 class="text-center text-white text-bold">Clientes</h4></a></div>
<div class="hidden-xs link-box bg-red-active"><a href="/clientes/cuentacorriente"><span class="info-box-icon bg-red-active"><i class="fa fa-pencil-square-o"></i></span><h4 class="text-center text-white text-bold">Cta Cte</h4></a></div>
<div class="hidden-xs link-box bg-red-active"><a href="/clientes/vencidos"><span class="info-box-icon bg-red-active"><i class="fa fa-warning"></i></span><h4 class="text-center text-white text-bold">Vencidos</h4></a></div>

@if ( (Auth::user()->id_rol==1) or (Auth::user()->id_rol==2) )
<div class="hidden-xs link-box bg-orange-active"><a href="/compras"><span class="info-box-icon bg-orange-active"><i class="fa fa-shopping-cart"></i></span><h4 class="text-center text-white text-bold">Compras</h4></a></div>
<div class="hidden-xs link-box bg-purple-active"><a href="/proveedores"><span class="info-box-icon bg-purple-active"><i class="fa fa-truck"></i></span><h4 class="text-center text-white text-bold">Proveedores</h4></a></div>
<!--<div class="hidden-xs link-box bg-gray"><a href="#"><span class="info-box-icon bg-gray"><i class="fa fa-bullhorn"></i></span><h4 class="text-center text-black text-bold">Ofertas</h4></a></div>-->
@endif
@if (env('APP_PLAN')>2)
    @if (Auth::user()->id_rol==1)
        <div class="hidden-xs link-box bg-gray-light"><a href="#"><span class="info-box-icon  bg-gray-light text-gray"><i class="fa fa-line-chart"></i></span><h4 class="text-center text-gray text-bold">Tendencias</h4></a></div>
        <div class="hidden-xs link-box bg-gray-light"><a href="/usuarios/resumenmensual"><span class="info-box-icon bg-gray-light text-gray"><i class="fa fa-user"></i></span><h4 class="text-center text-gray text-bold">Usuarios</h4></a></div>
    @endif
@endif