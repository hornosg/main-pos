<script language='JavaScript' type='text/javascript'>
    function Borrar(idrow){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $.ajax({
                        type: "GET",
                        url:'/cptes/delete/'+idrow,
                        data: idrow,
                        success: function(data) {
                            $('#table1').bootstrapTable('removeByUniqueId', idrow);
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        },
                        error:function(data) {
                            console.log(data);
                            iziToast.error({
                                timeout: 2000,
                                position:'center',
                                title: 'Error:',
                                message: data.responseJSON.message
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }
</script>