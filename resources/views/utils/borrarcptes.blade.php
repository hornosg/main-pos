<script language='JavaScript' type='text/javascript'>
    $(function () {
        $('#btn-eliminar').click(function (e) {
            e.preventDefault();
            var choice;
            iziToast.question({
                close: true,
                overlay: true,
                displayMode: 'once',
                id: 'question',
                zindex: 999,
                title: 'Atencion!',
                message: '¿Está seguro de Eliminar todos los items seleccionados?',
                position: 'center',
                buttons: [
                    ['<button><b>SI</b></button>', function (instance, toast) {
                        var ids = $.map($('#table1').bootstrapTable('getAllSelections'), function (row) {
                            return row.id;
                        });
                        $.ajax('/cptes/removeselected', {
                            type: 'POST',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: {datos:ids},
                            success: function(data){
                                $('#table1').bootstrapTable('remove', {
                                    field: 'id',
                                    values: ids
                                });
                                iziToast.success({
                                    timeout: 2000,
                                    position:'topCenter',
                                    title: 'Listo!',
                                    message: 'Items, Eliminados.'
                                });
                            },
                            error:function(data) {
                                console.log(data);
                                iziToast.error({
                                    timeout: 2000,
                                    position:'center',
                                    title: 'Error:',
                                    message: data.responseJSON.message
                                });
                            }
                        });
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    }, true],
                    ['<button>NO</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    }]
                ]
            });
        });
    });
</script>