<div class="box box-primary">
    <div class="box-header">
        <h5 class="text-bold">Top 5 - Productos Mas Vendidos (Ultimos 60 días) <a class="text-bold pull-right" href="/masvendidos"><span class="badge bg-navy-active" style="float: right;text-align: right;">VER TOP 100</span></a></h5>
    </div>
    <div class="box-body no-padding">
        <ul class="nav nav-tabs bg-white">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">En Mayor cantidad de Comprobantes</a></li>
            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">En Volumen</a></li>
            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">En Importe</a></li>
        </ul>
        <div class="tab-content bg-white">
            <div class="tab-pane active" id="tab_1">
                <table class="table table-condensed">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Id</th>
                        <th>Descripcion</th>
                        <th>Marca</th>
                        <th style="width: 50px">Comprobantes</th>
                    </tr>
                    @php
                        $row=0;
                        $date1 = 'DATE_ADD(DATE_ADD(NOW(),INTERVAL 1 DAY),INTERVAL - 2 MONTH)';
                        $date2 = 'NOW()';
                    @endphp
                    @foreach(DB::select('SELECT id_item, i.descripcion as producto, m.descripcion as marca,count(*) as cantidad_cptes
                                           FROM comprobantes_items ci
                                           JOIN items i on i.id = ci.id_item
                                           LEFT JOIN conf_marcas m on m.id=i.id_marca
                                           where date(ci.created_at) BETWEEN date('.$date1.') AND date('.$date2.')
                                          GROUP BY 1,2,3
                                          ORDER BY 4 desc
                                          LIMIT '.$top) as $item)
                        @php
                            $row=$row+1;
                        @endphp
                        <tr>
                            <td>{{$row}}</td>
                            <td>{{$item->id_item}}</td>
                            <td>{{$item->producto}}</td>
                            <td>{{$item->marca}}</td>
                            <td><span class="badge bg-green-active" style="float: right;text-align: right;">{{$item->cantidad_cptes}}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2">
                <table class="table table-condensed">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Id</th>
                        <th>Descripcion</th>
                        <th>Marca</th>
                        <th style="width: 50px">Cantidad</th>
                    </tr>
                    @php
                        $row=0;
                    @endphp
                    @foreach(DB::select('SELECT id_item,
                                                i.descripcion as producto,
                                                m.descripcion as marca,
                                                SUM(cantidad) as cantidad_item
                                           FROM comprobantes_items ci
                                           JOIN items i on i.id = ci.id_item
                                           LEFT JOIN conf_marcas m on m.id=i.id_marca
                                           where date(ci.created_at) BETWEEN date('.$date1.') AND date('.$date2.')
                                          GROUP BY 1,2,3
                                          ORDER BY 4 desc
                                          LIMIT '.$top) as $item)
                        @php
                            $row=$row+1;
                        @endphp
                        <tr>
                            <td>{{$row}}</td>
                            <td>{{$item->id_item}}</td>
                            <td>{{$item->producto}}</td>
                            <td>{{$item->marca}}</td>
                            <td><span class="badge bg-green-active" style="float: right;text-align: right;">{{$item->cantidad_item}}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_3">
                <table class="table table-condensed">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Id</th>
                        <th>Descripcion</th>
                        <th>Marca</th>
                        <th style="width: 50px">Importe</th>
                    </tr>
                    @php
                        $row=0;
                    @endphp
                    @foreach(DB::select('SELECT id_item,
                                                i.descripcion as producto,
                                                m.descripcion as marca,
                                                sum(importe),
                                                FORMAT(sum(importe),2,"es_AR") as importe
                                           FROM comprobantes_items ci
                                           JOIN items i on i.id = ci.id_item
                                           LEFT JOIN conf_marcas m on m.id=i.id_marca
                                           where date(ci.created_at) BETWEEN date('.$date1.') AND date('.$date2.')
                                          GROUP BY 1,2,3
                                          ORDER BY 4 desc
                                          LIMIT '.$top) as $item)
                        @php
                            $row=$row+1;
                        @endphp
                        <tr>
                            <td>{{$row}}</td>
                            <td>{{$item->id_item}}</td>
                            <td>{{$item->producto}}</td>
                            <td>{{$item->marca}}</td>
                            <td><span class="badge bg-green-active" style="float: right;text-align: right;">$ {{$item->importe}}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.tab-pane -->
        </div>
    </div>
    <!-- /.tab-content -->
</div>