<!-- Form Modal -->
<div class="modal fade" id="prodModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><strong><i class="fa fa-certificate"></i> Nuevo Producto</strong></h4>
            </div>
            <div class="modal-body box">
                <form id="form2" name="form2" class="" role="form"">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('ean') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-4 col-md-3">
                        <label for="ean" class="control-label">Codigo de Barras</label>
                        <input id="ean" type="text" class="form-control input-sm text-uppercase" name="ean" value="{{ old('ean') }}">
                    </div>
                    <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-8 col-md-6">
                        <label for="descripcion" class="control-label">Nombre (*)</label>
                        <input id="descripcion" type="text" class="form-control input-sm text-uppercase" name="descripcion" value="{{ old('descripcion') }}" autofocus>
                    </div>
                    <div class="form-group{{ $errors->has('id_marca') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">
                        <label for="id_marca" class="control-label" for="marcas">Marca</label>
                        <select id="id_marca" name="id_marca"  type="button" class="form-control">
                            <option value=""></option>
                            @foreach(DB::table('conf_marcas')->where('activo', 1)->get() as $marca)
                                <option value="{{$marca->id}}">{{$marca->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
<!--                    <div class="form-group{{ $errors->has('id_rubro') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">-->
<!--                        <label for="id_rubro" class="control-label" for="rubroes">Rubro</label>-->
<!--                        <select id="id_rubro" name="id_rubro"  type="button" class="form-control">-->
<!--                            <option value=""></option>-->
<!--                            @foreach(DB::table('conf_rubros')->where('activo', 1)->get() as $rubro)-->
<!--                                <option value="{{$rubro->id}}">{{$rubro->descripcion}}</option>-->
<!--                            @endforeach-->
<!--                        </select>-->
<!--                    </div>-->
<!--                    <div class="form-group{{ $errors->has('id_linea') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">-->
<!--                        <label for="id_linea" class="control-label" for="lineaes">Linea</label>-->
<!--                        <select id="id_linea" name="id_linea"  type="button" class="form-control">-->
<!--                            <option value=""></option>-->
<!--                            @foreach(DB::table('conf_lineas')->where('activo', 1)->get() as $linea)-->
<!--                                <option value="{{$linea->id}}">{{$linea->descripcion}}</option>-->
<!--                            @endforeach-->
<!--                        </select>-->
<!--                    </div>-->
<!--                    <div class="form-group{{ $errors->has('id_material') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">-->
<!--                        <label for="id_material" class="control-label" for="materiales">Material</label>-->
<!--                        <select id="id_material" name="id_material"  type="button" class="form-control">-->
<!--                            <option value=""></option>-->
<!--                            @foreach(DB::table('conf_materiales')->where('activo', 1)->get() as $material)-->
<!--                                <option value="{{$material->id}}">{{$material->descripcion}}</option>-->
<!--                            @endforeach-->
<!--                        </select>-->
<!--                    </div>-->
                    <div class="form-group{{ $errors->has('contNeto') ? ' has-error' : '' }}  col-xs-12 col-sm-12 col-md-4 col-md-2">
                        <label for="contNeto" class="control-label">Cont. Neto  (*)</label>
                        <input id="contNeto" type="number" class="form-control input-sm text-uppercase" name="contNeto" value="{{ old('contNeto') }}">
                    </div>
                    <div class="form-group{{ $errors->has('id_unidadcontneto') ? ' has-error' : '' }}  col-xs-12 col-sm-12 col-md-3 col-md-3">
                        <label for="id_unidadcontneto" class="control-label" for="unidadesm">UM. Cont. Neto  (*)</label>
                        <select id="id_unidadcontneto" name="id_unidadcontneto"  type="button" class="form-control">
                            <option value=""></option>
                            @foreach(DB::table('conf_unidades_medida')->where('activo', 1)->get() as $unidad)
                                <option value="{{$unidad->id}}">{{$unidad->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group {{ $errors->has('id_unidadventa') ? ' has-error' : '' }}  col-xs-12 col-sm-12 col-md-3 col-md-3">
                        <label for="id_unidadventa" class="control-label" for="unidadesm">UM.Venta  (*)</label>
                        <select id="id_unidadventa" name="id_unidadventa"  type="button" class="form-control">
                            <option value=""></option>
                            @foreach(DB::table('conf_unidades_medida')->where('activo', 1)->get() as $unidad)
                                @if ( $unidad->descripcion == "U" )
                                    <option value="{{$unidad->id}}" selected>{{$unidad->descripcion}}</option>
                                @else
                                    <option value="{{$unidad->id}}">{{$unidad->descripcion}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group{{ $errors->has('id_moneda') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2 col-md-offset-right-2">
                        <label for="id_moneda" class="control-label" for="subrubros">Moneda  (*)</label>
                        <select id="id_moneda" name="id_moneda"  type="button" class="form-control">
                            <option value=""></option>
                            @foreach(DB::table('sys_monedas')->where('activo', 1)->get() as $moneda)
                                <option value="{{$moneda->id}}">{{$moneda->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group{{ $errors->has('precioLista') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                        <label for="precioLista" class="control-label">P. Lista  (*)</label>
                        <input id="precioLista" type="number" step="any" class="form-control input-sm text-uppercase" min="0" name="precioLista" value="{{ old('precioLista') }}">
                    </div>

                    <div class="form-group{{ $errors->has('iva') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                        <label for="iva" class="control-label">Iva</label>
                        <input id="iva"  class="form-control input-sm text-uppercase" name="iva" value="21">
                    </div>

                    <div class="form-group{{ $errors->has('bonifProv') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                        <label for="bonifProv" class="control-label">Bonificacion</label>
                        <input id="bonifProv" type="number" step="any" class="form-control input-sm text-uppercase" name="bonifProv" min="1" max="100" value="{{ old('bonifProv') }}">
                    </div>

                    <div class="form-group{{ $errors->has('precioCosto') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                        <label for="precioCosto" class="control-label">P. Costo</label>
                        <input id="precioCosto" type="number" step="any" class="form-control input-sm text-uppercase" name="precioCosto" readonly value="{{ old('precioCosto') }}">
                    </div>

                    <div class="form-group{{ $errors->has('ganancia') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                        <label for="ganancia" class="control-label">Ganancia  (*)</label>
                        <input id="ganancia" type="number" step="any" class="form-control input-sm text-uppercase" name="ganancia" min="1" value="{{ old('ganancia') }}">
                    </div>

                    <div class="form-group{{ $errors->has('precio') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                        <label for="precio" class="control-label">P. Venta</label>
                        <input id="precio" type="number" step="any" class="form-control input-sm text-uppercase" name="precio" readonly value="{{ old('precio') }}">
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <button id="guardar" type="submit" name="guardar" class="btn bg-green minw90">
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>