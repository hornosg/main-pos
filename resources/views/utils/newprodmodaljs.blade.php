<script language='JavaScript' type='text/javascript'>
    $("#id_marca").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_rubro").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_material").select2({
        tags:true,
        placeholder: "",
        allowClear: true
    });
    $("#idModelo").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_linea").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_unidadcontneto").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_unidadventa").select2({placeholder: "",allowClear: true});
    $("#id_moneda").select2({placeholder: "", allowClear: true});

    $("#precioLista").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (idmoneda === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }

        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl+((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((pl*parseFloat($("#iva").val()))/100);
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#bonifProv").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (idmoneda === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }

        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl +((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((pl*parseFloat($("#iva").val()))/100);
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#iva").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (idmoneda === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }

        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl +((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((pl*parseFloat($("#iva").val()))/100);
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#ganancia").change(function(){
        pc = $("#precioCosto").val();
        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#id_moneda").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (idmoneda === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }
        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl +((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((parseFloat($("#precioLista").val())*parseFloat($("#iva").val()))/100);
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $('#form2').on('submit', function(e) {
        $('.overlay').removeClass('d-none');
        e.preventDefault();
        $.ajax({
            type: "POST",
            url:'/ajax/productos/store',
            data: $(this).serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                $('#prodModal').modal('hide');
                document.getElementById('form2').reset();
                $('.overlay').addClass('d-none');

                var newOption = new Option(data.text, data.id, false, false);
                $('#id_item').append(newOption).trigger('change');
                $('.overlay').addClass('d-none');
            },
            error:function(data) {
                console.log(data);
                iziToast.error({
                    timeout: 3000,
                    position:'center',
                    title: 'Error:',
                    message: 'Hubo un error, Los campos marcados con un (*) son obligatorios'
                });
                $('.overlay').addClass('d-none');
            }
        });
    })
</script>