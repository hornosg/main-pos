<div class="row">
    <div class="col-md-12">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{{$avgcptes}}</h3>

                <p>Comprobantes diarios</p>
                <sup style="font-size: 10px">EN PROMEDIO, MES ACTUAL</sup>
            </div>
            <div class="icon">
                <i class="fa fa-files-o"></i>
            </div>
            <!--                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
        </div>
    </div>
    <!-- ./col -->
    <div class="col-md-12">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{$masde3xcpte}}</h3>

                <p>Ventas multiples diarias</p>
                <sup style="font-size: 10px">EN PROMEDIO, MES ACTUAL</sup>
            </div>
            <div class="icon">
                <i class="fa fa-list-ol"></i>
            </div>
            <!--                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
        </div>
    </div>
    <!-- ./col -->
    <div class="col-md-12">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>44</h3>

                <p>Gestiones de Stock</p>
                <sup style="font-size: 10px">MES ACTUAL</sup>
            </div>
            <div class="icon">
                <i class="fa fa-th-large"></i>
            </div>
            <!--                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
        </div>
    </div>
    <!-- ./col -->
    <div class="col-md-12">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>65</h3>

                <p>Gestion de Cobranzas</p>
                <sup style="font-size: 10px">MES ACTUAL</sup>
            </div>
            <div class="icon">
                <i class="fa fa-phone-square"></i>
            </div>
            <!--                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
        </div>
    </div>
</div>