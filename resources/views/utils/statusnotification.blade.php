<script language='JavaScript' type='text/javascript'>
    $(window).on('load', function(event){
        @if (session('status'))
            iziToast.success({
                timeout: 2000,
                position:'topRight',
                title: 'Listo!',
                message: '{{session('status')}}'
            });
        @endif
    });
</script>