@extends('adminlte::page')
@section('title', 'MainPOS')

@section('content_header')
@include('layouts.inventarionavbar')
@stop

@section('content')
    <div class="row">
    <!-- Left col -->
        <div class="col-md-4 col-md-6 col-sm-12 prl-5">
            <div class="box box-primary">
                <div id="toolbar1">
                    <div class="form-inline" role="form">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formModalSector" title="Nuevo Sector">
                            <i class="fa fa-btn fa-plus text-white"></i>
                        </button>
                        <span class="text-bold">SECTORES</span>
                    </div>
                </div>
                <table id='tsectores' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                       data-toolbar="#toolbar1"
                       data-search='true'
                       data-strict-search="false"
                       data-multiple-search='true'
                       data-pagination='true'
                       data-page-size=15
                       data-sort-name='id'
                       data-sort-order='asc'>
                    <thead class='thead-inverse'>
                        <th data-checkbox='true' class='hidden'></th>
                        <th data-field='id' class='hidden'>Id</th>
                        <th data-field='descripcion'   class='text-uppercase'>Descripcion</th>
                        <th data-field='id_sucursal'   class='hidden'>SUC</th>
                        <th data-field='sucursal'   class='text-uppercase'>SUCURSAL</th>
                        <th data-field='alias'   class='text-uppercase'>ALIAS</th>
                        <th data-field='sucursal'   class='hidden'>Sucursal</th>
<!--                            <th data-field='activo' data-align='center' class='text-uppercase'>Activo</th>-->
                        <th data-align='center' data-formatter='FunctionsSectorFormatter' class="col-md-2"></th>
                    </thead>
                </table>
            </div>
        </div>
        <div class="col-md-4 col-md-6 col-sm-12 prl-5">
            <div class="box box-primary">
                <div id="toolbar2">
                    <div class="form-inline" role="form">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formModalUbic" title="Nueva Ubicacion">
                            <i class="fa fa-btn fa-plus text-white"></i>
                        </button>
                        <span class="text-bold text">UBICACIONES</span>
                    </div>
                </div>
                <table id='tubicaciones' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                       data-toolbar="#toolbar2"
                       data-search='true'
                       data-strict-search="false"
                       data-multiple-search='true'
                       data-pagination='true'
                       data-page-size=15
                       data-sort-name='id'
                       data-sort-order='asc'>
                    <thead class='thead-inverse'>
                        <th data-checkbox='true' class='hidden'></th>
                        <th data-field='id' class='hidden'>Id</th>
                        <th data-field='descripcion'   class='text-uppercase'>Descripcion</th>
                        <th data-field='id_sector'   class='hidden'></th>
                        <th data-field='sector'  class='text-uppercase'>SECTOR</th>
                        <th data-field='alias'   class='text-uppercase'>ALIAS</th>
<!--                            <th data-field='activo' data-align='center' class='text-uppercase'>Activo</th>-->
                        <th data-align='right' class='text-uppercase' data-formatter='FunctionsUbicFormatter'></th>
                    </thead>
                </table>
                <!-- /.box-footer -->
            </div>
        </div>
        <div class="col-md-4 col-md-12 col-sm-12 prl-5">
            <div class="box box-primary">
                <div id="toolbar3">
                    <div class="form-inline" role="form">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formModalPos" title="Nueva Posicion">
                            <i class="fa fa-btn fa-plus text-white"></i>
                        </button>
                        <button id="btn-eliminar" class="btn btn-danger btn-sm hidden-sm hidden-xs" title="Eliminar Items Seleccionados">
                            <i class="fa fa-btn fa-trash text-white"></i>
                        </button>
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formModalPosMas" title="Nuevas Posiciones">
                            <i class="fa fa-btn fa-plus text-white"></i>
                            <i class="fa fa-btn fa-table text-white"></i>
                        </button>
                        <span class="text-bold">POSICIONES</span>
                    </div>
                </div>
                <table id='tposiciones' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                       data-toolbar="#toolbar3"
                       data-search='true'
                       data-strict-search="false"
                       data-multiple-search='true'
                       data-pagination='true'
                       data-page-size=15
                       data-sort-name='id'
                       data-sort-order='asc'
                       >
                    <thead class='thead-inverse'>
                        <th data-checkbox='true' class='hidden-sm hidden-xs'></th>
                        <th data-field='id' class='hidden'>Id</th>
                        <th data-field='descripcion'   class='text-uppercase'>Descripcion</th>
                        <th data-field='ubicacion'   class='text-uppercase'>Ubicacion</th>
<!--                            <th data-field='activo' data-align='center' class='text-uppercase'>Activo</th>-->
                        <th data-align='right' class='text-uppercase' data-formatter='FunctionsPosFormatter'></th>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>



<!-- Form Modal -->
<div class="modal fade" id="formModalSector" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modalmaster" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nuevo Sector</h4>
            </div>
            <div class="modal-body box">
                <form id="formSector" name="formSector" class="form-horizontal" role="form"">
                <div class="box-body">
                    {{ csrf_field() }}
                    <input id="id" type="hidden" name="id">
                    <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                        <label for="descripcion" class="col-md-3 col-md-3 col-sm-3 control-label">Descripcion</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="descripcion" type="text" class="form-control input-sm text-uppercase col-10" name="descripcion">
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('id_sucursal') ? ' has-error' : '' }}">
                        <label for="sucursal" class="col-md-3 col-md-3 col-sm-3 control-label" for="sucursales">Sucursal</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <select id="id_sucursal" name="id_sucursal"  type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <option value=""></option>
                                @foreach(DB::table('sucursales')->where('activo', 1)->get() as $sucursal)
                                    <option value="{{$sucursal->id}}">{{$sucursal->descripcion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('alias') ? ' has-error' : '' }}">
                        <label for="alias" class="col-md-3 col-md-3 col-sm-3 control-label">Alias</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="alias" type="text" class="form-control input-sm text-uppercase col-10" name="alias">
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }}">
                        <label for="activo" class="col-md-3 col-md-3 col-sm-3 control-label">Activo</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <button id="guardar" type="submit" name="guardar" class="btn btn-primary minw90">
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                        <button id="modificar" type="button" name="modificar" class="btn btn-primary minw90 d-none" onclick='ModificarSector()'>
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay ">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Form Modal -->
<div class="modal fade" id="formModalUbic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modalmaster" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva Ubicacion</h4>
            </div>
            <div class="modal-body box">
                <form id="formUbic" name="formUbic" class="form-horizontal" role="form"">
                <div class="box-body">
                    {{ csrf_field() }}
                    <input id="idubic" type="hidden" name="id">
                    <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                        <label for="descripcion" class="col-md-3 col-md-3 col-sm-3 control-label">Descripcion</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="descripcionubic" type="text" class="form-control input-sm text-uppercase col-10" name="descripcion">
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('id_sector') ? ' has-error' : '' }}">
                        <label for="id_sector" class="col-md-3 col-md-3 col-sm-3 control-label" for="sectores">Sector</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <select id="id_sector" name="id_sector"  type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <option value=""></option>
                                @foreach($sectores as $sector)
                                    <option value="{{$sector->id}}">{{$sector->descripcion2}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('alias') ? ' has-error' : '' }}">
                        <label for="alias" class="col-md-3 col-md-3 col-sm-3 control-label">Alias</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="alias2" type="text" class="form-control input-sm text-uppercase col-10" name="alias">
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }}">
                        <label for="activo" class="col-md-3 col-md-3 col-sm-3 control-label">Activo</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <button id="guardarprov" type="submit" name="guardar" class="btn btn-primary minw90">
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                        <button id="modificarprov" type="button" name="modificar" class="btn btn-primary minw90 d-none" onclick='ModificarUbic()'>
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay ">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Form Modal -->
<div class="modal fade" id="formModalPos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modalmaster" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva Posicion</h4>
            </div>
            <div class="modal-body box">
                <form id="formPos" name="formPos" class="form-horizontal" role="form"">
                <div class="box-body">
                    {{ csrf_field() }}
                    <input id="idpos" type="hidden" name="id">
                    <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                        <label for="descripcion" class="col-md-3 col-md-3 col-sm-3 control-label">Descripcion</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="descripcionpos" type="text" class="form-control input-sm text-uppercase col-10" name="descripcion">
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('id_ubicacion') ? ' has-error' : '' }}">
                        <label for="id_ubicacion" class="col-md-3 col-md-3 col-sm-3 control-label" for="ubicaciones">Ubicacion</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <select id="id_ubicacion" name="id_ubicacion"  type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <option value=""></option>
                                @foreach($ubicaciones as $ubicacion)
                                    <option value="{{$ubicacion->id}}">{{$ubicacion->descripcion2}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }}">
                        <label for="activoloc" class="col-md-3 col-md-3 col-sm-3 control-label">Activo</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <button id="guardarloc" type="submit" name="guardar" class="btn btn-primary minw90">
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                        <button id="modificarloc" type="button" name="modificar" class="btn btn-primary minw90 d-none" onclick='ModificarPos()'>
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay ">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="formModalPosMas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modalmaster" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nuevas Posiciones</h4>
            </div>
            <div class="modal-body box">
                <form id="formPosMas" name="formPosMas" class="form-horizontal" role="form"">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('prefijo') ? ' has-error' : '' }}">
                        <label for="prefijo" class="col-md-3 col-md-3 col-sm-3 control-label">Prefijo</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="prefijo" type="text" class="form-control input-sm text-uppercase col-10" name="prefijo" placeholder="Ejemplo: REC-E1-">
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('columnas') ? ' has-error' : '' }}">
                        <label for="columnas" class="col-md-3 col-md-3 col-sm-3 control-label">Lista de Columnas</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="columnas" type="text" class="form-control input-sm text-uppercase col-10" name="columnas" placeholder="Ejemplo: A,B,C,...">
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('filas') ? ' has-error' : '' }}">
                        <label for="filas" class="col-md-3 col-md-3 col-sm-3 control-label">Cantidad Filas</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="filas" type="number" class="form-control input-sm" name="filas">
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('id_ubicacion2') ? ' has-error' : '' }}">
                        <label for="id_ubicacion2" class="col-md-3 col-md-3 col-sm-3 control-label" for="ubicaciones">Ubicacion</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <select id="id_ubicacion2" name="id_ubicacion2"  type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <option value=""></option>
                                @foreach($ubicaciones as $ubicacion)
                                    <option value="{{$ubicacion->id}}">{{$ubicacion->descripcion2}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <button id="guardarlocmas" type="submit" name="guardar" class="btn btn-primary minw90">
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

<?php
foreach ($sectores as $item) {
    $item->activo = $item->activo == 1 ? 'si':'no';
}
foreach ($ubicaciones as $item) {
    $item->activo = $item->activo == 1 ? 'si':'no';
}
foreach ($posiciones as $item) {
    $item->activo = $item->activo == 1 ? 'si':'no';
}
?>

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<!-- Bootstrap-Table -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
<script language="javascript" type="text/javascript">
    item = {};
    $(function () {
        var data1 =  <?php echo json_encode($sectores); ?>;
        $('#tsectores').bootstrapTable({data: data1});
        var data2 =  <?php echo json_encode($ubicaciones); ?>;
        $('#tubicaciones').bootstrapTable({data: data2});
        var data3 =  <?php echo json_encode($posiciones); ?>;
        $('#tposiciones').bootstrapTable({data: data3});
    });

    function FunctionsSectorFormatter(value, row, index) {
        if ((row.id!=1) && (row.id!=2)) {
            return  "<a class='btn-outline-danger hidden-xs' href='#' onclick='BorrarSector("+index+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
                    "<a class='btn-outline-warning' href='#' onclick='EditarSector("+index+")' data-toggle='modal' data-target='#formModalSector' title='Modificar'><i class='fa fa-fw fa-pencil'></i></a>";
        }
    };
    function FunctionsUbicFormatter(value, row, index) {
        return  "<a class='btn-outline-danger hidden-xs' href='#' onclick='BorrarUbic("+index+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
            "<a class='btn-outline-warning' href='#' onclick='EditarUbic("+index+")' data-toggle='modal' data-target='#formModalUbic' title='Modificar'><i class='fa fa-fw fa-pencil'></i></a>";
    };
    function FunctionsPosFormatter(value, row, index) {
        return  "<a class='btn-outline-danger hidden-xs' href='#' onclick='BorrarPos("+index+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
            "<a class='btn-outline-warning' href='#' onclick='EditarPos("+index+")' data-toggle='modal' data-target='#formModalPos' title='Modificar'><i class='fa fa-fw fa-pencil'></i></a>";
    };

    
    //ABM SECTOR---
    
    function BorrarSector(index){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $('#tsectores').bootstrapTable('check', index);
                    var ids = $.map($('#tsectores').bootstrapTable('getSelections'), function (row) {
                        return row.id;
                    });
                    $.ajax({
                        type: "GET",
                        url:'/sectores/delete/'+ids,
                        data: $('#formSector').serialize(),
                        success: function(data) {
                            $('#tsectores').bootstrapTable('remove', {
                                field: 'id',
                                values: ids
                            });
                            $('#tsectores').bootstrapTable('uncheckAll');
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    $('#formSector').on('submit', function(e) {
        $('.overlay').removeClass('d-none');
        e.preventDefault();
        $.ajax({
            type: "POST",
            url:'/sectores/store',
            data: $('#formSector').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                $('#formModalSector').modal('hide');
                $('#tsectores').bootstrapTable('append', data);
                document.getElementById('formSector').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            },
            error: function(data){
                var errors = data.responseJSON;
                if (errors.errors.descripcion){
                    $('#descripcion').parents(".form-group").addClass('has-error');
                }
                if (errors.errors.id_sucursal){
                    $('#id_sucursal').parents(".form-group").addClass('has-error');
                }
                $('.overlay').addClass('d-none');
            }
        });
    })

    function EditarSector(index){
        $('#tsectores').bootstrapTable('check', index);
        $row = $('#tsectores').bootstrapTable('getSelections');
        item.index = index;
        item.id = $row[0]['id'];
        item.descripcion = $row[0]['descripcion'];
        item.id_sucursal = $row[0]['id_sucursal'];
        item.sucursal = $row[0]['sucursal'];
        item.alias = $row[0]['alias'];
        item.activo = $row[0]['activo'];
        $('#tsectores').bootstrapTable('uncheckAll');
        $('#descripcion').attr('readonly', false);
        $('#activo').attr('disabled', false);
    }

    $('#formModalSector').on('shown.bs.modal', function (e) {
        if (item.index >= 0 ){
            $('#descripcion').val(item.descripcion);
            $('#alias').val(item.alias);
            $("#id_sucursal option[value="+item.id_sucursal+"]").attr('selected', 'selected');
            if (item.activo=='no'){
                //alert(item.activo);
                $('.toggle').addClass('btn-default off');
            }
            $( '.modal-title' ).text( 'Modificar Sector' );
            if (item.disable){
                $('#modificar').addClass('d-none');
                $('#guardar').addClass('d-none');
            }else{
                $('#modificar').removeClass('d-none');
                $('#guardar').addClass('d-none');
            }

        }else{
            $('#descripcion').attr('readonly', false);
            $('#alias').attr('readonly', false);
            $('#activo').attr('disabled', false);
            $('.toggle').removeClass('btn-default off');
            $('.toggle').addClass('btn-primary');
            $('#guardar').removeClass('d-none');
            $('#modificar').addClass('d-none');
        }
    })

    $('#formModalSector').on('hide.bs.modal', function (e) {
        document.getElementById('formSector').reset();
        item.index=undefined;
        $('.off').removeClass('off');
        item.disable=false;
        $('#descripcion').parents(".form-group").removeClass('has-error');
        $('#id_sucursal').parents(".form-group").removeClass('has-error');
        $('#alias').parents(".form-group").removeClass('has-error');
    });

    function ModificarSector(){
        $('.overlay').removeClass('d-none');
        $.ajax('/sectores/update/'+item.id, {
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: $('#formSector').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });

                activo = $('.off').val() == '' ? 'no':'si';
                $('#tsectores').bootstrapTable('updateRow', {
                    index: item.index,
                    row: {
                        descripcion: $('#descripcion').val(),
                        id_sucursal: data.id_sucursal,
                        sucursal: data.sucursal,
                        alias: data.alias,
                        activo:activo
                    }
                });
                $('#formModalSector').modal('toggle');
                document.getElementById('formSector').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            },
            error: function(data){
                var errors = data.responseJSON;
                if (errors.errors.descripcion){
                    $('#descripcion').parents(".form-group").addClass('has-error');
                }
                if (errors.errors.id_sucursal){
                    $('#id_sucursal').parents(".form-group").addClass('has-error');
                }
                $('.overlay').addClass('d-none');
            }
        });

    }

    //ABM UBICACION---

    function BorrarUbic(index){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $('#tubicaciones').bootstrapTable('check', index);
                    var ids = $.map($('#tubicaciones').bootstrapTable('getSelections'), function (row) {
                        return row.id;
                    });
                    $.ajax({
                        type: "GET",
                        url:'/ubicaciones/delete/'+ids,
                        data: $('#formUbic').serialize(),
                        success: function(data) {
                            $('#tubicaciones').bootstrapTable('remove', {
                                field: 'id',
                                values: ids
                            });
                            $('#tubicaciones').bootstrapTable('uncheckAll');
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    $('#formUbic').on('submit', function(e) {
        $('.overlay').removeClass('d-none');
        e.preventDefault();
        $.ajax({
            type: "POST",
            url:'/ubicaciones/store',
            data: $('#formUbic').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                $('#formModalUbic').modal('hide');
                $('#tubicaciones').bootstrapTable('append', data);
                document.getElementById('formUbic').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            },
            error: function(data){
                var errors = data.responseJSON;
                if (errors.errors.descripcion){
                    $('#descripcion').parents(".form-group").addClass('has-error');
                }
                if (errors.errors.id_sector){
                    $('#id_sector').parents(".form-group").addClass('has-error');
                }
                $('.overlay').addClass('d-none');
            }
        });
    })

    function EditarUbic(index){
        $('#tubicaciones').bootstrapTable('check', index);
        $row = $('#tubicaciones').bootstrapTable('getSelections');
        item.index = index;
        item.id = $row[0]['id'];
        item.descripcion = $row[0]['descripcion'];
        item.alias = $row[0]['alias'];
        item.id_sector = $row[0]['id_sector'];
        item.sector = $row[0]['sector'];
        item.activo = $row[0]['activo'];
        $('#tubicaciones').bootstrapTable('uncheckAll');
        $('#descripcion').attr('readonly', false);
        $('#activo').attr('disabled', false);
    }

    $('#formModalUbic').on('shown.bs.modal', function (e) {
        if (item.index >= 0 ){
            $('#descripcionubic').val(item.descripcion);
            $('#alias2').val(item.alias);
            $("#id_sector option[value="+item.id_sector+"]").attr('selected', 'selected');
            if (item.activo=='no'){
                $('.toggle').addClass('btn-default off');
            }
            $( '.modal-title' ).text( 'Modificar Ubicacion' );
            if (item.disable){
                $('#modificarprov').addClass('d-none');
                $('#guardarprov').addClass('d-none');
            }else{
                $('#modificarprov').removeClass('d-none');
                $('#guardarprov').addClass('d-none');
            }
        }else{
            $('#descripcion').attr('readonly', false);
            $('#alias').attr('readonly', false);
            $('#activo').attr('disabled', false);
            $('.toggle').removeClass('btn-default off');
            $('.toggle').addClass('btn-primary');
            $('#guardarprov').removeClass('d-none');
            $('#modificarprov').addClass('d-none');
        }
    });

    $('#formModalUbic').on('hide.bs.modal', function (e) {
        document.getElementById('formUbic').reset();
        item.index=undefined;
        $('.off').removeClass('off');
        item.disable=false;
        $('#descripcion').parents(".form-group").removeClass('has-error');
        $('#id_sector').parents(".form-group").removeClass('has-error');
        $('#alias2').parents(".form-group").removeClass('has-error');
    });

    function ModificarUbic(){
        $('.overlay').removeClass('d-none');
        $.ajax('/ubicaciones/update/'+item.id, {
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: $('#formUbic').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });

                activo = $('.off').val() == '' ? 'no':'si';
                $('#tubicaciones').bootstrapTable('updateRow', {
                    index: item.index,
                    row: {
                        descripcion: data.descripcion,
                        id_sector: data.id_sector,
                        sector: data.sector,
                        alias: data.alias,
                        activo:data.activo
                    }
                });
                $('#formModalUbic').modal('toggle');
                document.getElementById('formUbic').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            },
            error: function(data){
                var errors = data.responseJSON;
                if (errors.errors.descripcion){
                    $('#descripcion').parents(".form-group").addClass('has-error');
                }
                if (errors.errors.id_sector){
                    $('#id_sector').parents(".form-group").addClass('has-error');
                }
                $('.overlay').addClass('d-none');
            }
        });
    }


    //ABM POSICION---

    function BorrarPos(index){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $('#tposiciones').bootstrapTable('check', index);
                    var ids = $.map($('#tposiciones').bootstrapTable('getSelections'), function (row) {
                        return row.id;
                    });
                    $.ajax({
                        type: "GET",
                        url:'/posiciones/delete/'+ids,
                        data: $('#formPos').serialize(),
                        success: function(data) {
                            $('#tposiciones').bootstrapTable('remove', {
                                field: 'id',
                                values: ids
                            });
                            $('#tposiciones').bootstrapTable('uncheckAll');
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    $('#formPos').on('submit', function(e) {
        $('.overlay').removeClass('d-none');
        e.preventDefault();
        $.ajax({
            type: "POST",
            url:'/posiciones/store',
            data: $('#formPos').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                $('#formModalPos').modal('hide');
                $('#tposiciones').bootstrapTable('append', data);
                document.getElementById('formPos').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            },
            error: function(data){
                var errors = data.responseJSON;
                if (errors.errors.descripcion){
                    $('#descripcion').parents(".form-group").addClass('has-error');
                }
                if (errors.errors.id_ubicacion){
                    $('#id_ubicacion').parents(".form-group").addClass('has-error');
                }
                $('.overlay').addClass('d-none');
            }
        });
    })

    function EditarPos(index){
        $('#tposiciones').bootstrapTable('check', index);
        $row = $('#tposiciones').bootstrapTable('getSelections');
        item.index = index;
        item.id = $row[0]['id'];
        item.descripcion = $row[0]['descripcion'];
        item.id_ubicacion = $row[0]['id_ubicacion'];
        item.ubicacion = $row[0]['sector'];
        item.activo = $row[0]['activo'];
        $('#tposiciones').bootstrapTable('uncheckAll');
        $('#descripcion').attr('readonly', false);
        $('#activo').attr('disabled', false);
    }

    $('#formModalPos').on('shown.bs.modal', function (e) {
        if (item.index >= 0 ){
            $('#descripcionpos').val(item.descripcion);
            $("#id_ubicacion option[value="+item.id_ubicacion+"]").attr('selected', 'selected');
            if (item.activo=='no'){
                $('.toggle').addClass('btn-default off');
            }
            $( '.modal-title' ).text( 'Modificar Posicion' );
            if (item.disable){
                $('#modificarloc').addClass('d-none');
                $('#guardarloc').addClass('d-none');
            }else{
                $('#modificarloc').removeClass('d-none');
                $('#guardarloc').addClass('d-none');
            }
        }else{
            $('#descripcionpos').attr('readonly', false);
            $('#id_ubicacion').attr('disabled', false);
            $('#activoloc').attr('disabled', false);
            $('.toggle').removeClass('btn-default off');
            $('.toggle').addClass('btn-primary');
            $('#guardarlocmas').removeClass('d-none');
        }
    })

    $('#formModalPos').on('hide.bs.modal', function (e) {
        document.getElementById('formPos').reset();
        item.index=undefined;
        $('.off').removeClass('off');
        item.disable=false;
        $('#descripcion').parents(".form-group").removeClass('has-error');
        $('#id_ubicacion').parents(".form-group").removeClass('has-error');
    })

    function ModificarPos(){
        $('.overlay').removeClass('d-none');
        $.ajax('/posiciones/update/'+item.id, {
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: $('#formPos').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });

                activo = $('.off').val() == '' ? 'no':'si';
                $('#tposiciones').bootstrapTable('updateRow', {
                    index: item.index,
                    row: {
                        descripcion: data.descripcion,
                        id_ubicacion: data.id_ubicacion,
                        ubicacion: data.ubicacion,
                        activo:data.activo
                    }
                });
                $('#formModalPos').modal('toggle');
                document.getElementById('formPos').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            },
            error: function(data){
                var errors = data.responseJSON;
                if (errors.errors.descripcion){
                    $('#descripcion').parents(".form-group").addClass('has-error');
                }
                if (errors.errors.id_ubicacion){
                    $('#id_ubicacion').parents(".form-group").addClass('has-error');
                }
                $('.overlay').addClass('d-none');
            }
        });
    }

    $(function () {
        $('#btn-eliminar').click(function (e) {
            e.preventDefault();
            var choice;
            iziToast.question({
                close: true,
                overlay: true,
                displayMode: 'once',
                id: 'question',
                zindex: 999,
                title: 'Atencion!',
                message: '¿Está seguro de Eliminar todos los items seleccionados?',
                position: 'center',
                buttons: [
                    ['<button><b>SI</b></button>', function (instance, toast) {
                        var ids = $.map($('#tposiciones').bootstrapTable('getAllSelections'), function (row) {
                            return row.id;
                        });
                        $('#tposiciones').bootstrapTable('remove', {
                            field: 'id',
                            values: ids
                        });

                        $.ajax('/posiciones/removeselected', {
                            type: 'POST',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: {datos:ids}
                        });

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        iziToast.success({
                            timeout: 2000,
                            position:'topCenter',
                            title: 'Listo!',
                            message: 'Items, Eliminados.'
                        });
                    }, true],
                    ['<button>NO</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    }]
                ]
            });
        });
    });

    $('#formPosMas').on('submit', function(e) {
        $('.overlay').removeClass('d-none');
        e.preventDefault();
        $.ajax({
            type: "POST",
            url:'/posiciones/store/masivo',
            data: $('#formPosMas').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                $('#formModalPosMas').modal('hide');
                $('#tposiciones').bootstrapTable('append', data.data);
                document.getElementById('formPosMas').reset();
                $('.overlay').addClass('d-none');
            },
            error: function(data){
                var errors = data.responseJSON;
                if (errors.errors.prefijo){
                    $('#prefijo').parents(".form-group").addClass('has-error');
                }
                if (errors.errors.columnas){
                    $('#columnas').parents(".form-group").addClass('has-error');
                }
                if (errors.errors.filas){
                    $('#filas').parents(".form-group").addClass('has-error');
                }
                $('.overlay').addClass('d-none');
            }
        });
    })
</script>
@stop