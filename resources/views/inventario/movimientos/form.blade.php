@extends('adminlte::page')

@section('title', $titulo)

@php
if ($edit==0){
    $editable='readonly';
    $visible='d-none';
    $disabled='disabled';
    $accion = 'Visualizar';
}else{
    $editable=' ';
    $visible=' ';
    $disabled=' ';
    $accion = 'Editar';
}
@endphp

@section('content_header')
    @include('layouts.inventarionavbar')
@stop

@section('content')
<div id="content" class="container-fluid row">
    <form id="form1" class="" role="form" method="POST" action="/inventario/movimientos/store" enctype="multipart/form-data">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>{{$tituloform}}</strong></h3>
                </div>
                <div class="box-body">
                    {{ csrf_field() }}

                    @if (!empty($movstk))
                        <input type = "hidden" name = "id" value = "{{$movstk->id}}">
                    @else
                        <input type = "hidden" name = "id">
                    @endif
                    <div class="form-group col-md-4">
                        <label for="id_motivo" class="control-label">Motivo</label>
                        <select id="id_motivo" name="id_motivo" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::table('sys_motivos_mstock')
                                    ->where('sys_motivos_mstock.activo', 1)
                                    ->where('sys_motivos_mstock.interno', 0)
                                    ->select('sys_motivos_mstock.*')
                                    ->get() as $motivo)
                                <option value="{{$motivo->id}}">{{$motivo->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-8 col-md-offset-right-6">
                        <label for="id_item" class="control-label">Producto</label><div id="loading" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>
                        <a class='text-primary pull-right' href='#' data-toggle='modal' data-target='#prodModal' title='Crear Nuevo Producto'>Crear Nuevo</i></a>
                        <select id="id_item" name="id_item" class="form-control text-uppercase" {{$disabled}} required>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="clearfix visible-lg-block visible-md-block"></div>

                    <div class="form-group col-md-4">
                        <label for="id_sector_o" class="control-label">Sector Origen</label>
                        <select id="id_sector_o" name="id_sector_o" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_ubicacion_o" class="control-label">Ubicacion Origen</label>
                        <select id="id_ubicacion_o" name="id_ubicacion_o" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>

                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_posicion_o" class="control-label">Posicion Origen</label>
                        <select id="id_posicion_o" name="id_posicion_o" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_sector_d" class="control-label">Sector Destino</label>
                        <select id="id_sector_d" name="id_sector_d" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::table('inv_sectores')
                                    ->where('inv_sectores.activo', 1)
                                    ->select('inv_sectores.*')
                                    ->get() as $sector)
                                <option value="{{$sector->id}}">{{$sector->alias}} ( {{$sector->descripcion}} )</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_ubicacion_d" class="control-label">Ubicacion Destino</label>
                        <select id="id_ubicacion_d" name="id_ubicacion_d" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_posicion_d" class="control-label">Posicion Destino</label>
                        <select id="id_posicion_d" name="id_posicion_d" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="form-group{{ $errors->has('cantidad') ? ' has-error' : '' }} col-xs-12 col-sm-6 col-md-4 col-md-3 col-md-offset-0">
                        <label for="cantidad" class="control-label">Cantidad</label>
                        <input id="cantidad" type="number" step="any" class="form-control text-uppercase w100 input-sm" name="cantidad" value="{{ old('cantidad') }}">
                    </div>

                    @if (!empty($movstk))
                        <fieldset class="form-group col-md-4">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Alta]</label>
                                @php $uscr=DB::table('users')->where('id', $movstk->created_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{  $uscr->name }} {{ date_format($movstk->created_at, 'd/m/Y H:i:s') }}" readonly>
                        </fieldset>
                        <fieldset class="form-group col-md-4">
                                <label for="created_at" class="control-label">Usuario Fecha/Hora [Modificacion]</label>
                                @php $usup=DB::table('users')->where('id', $movstk->updated_us)->select('name')->first(); @endphp
                                <input type="text" class="form-control" name="name" value="{{ $usup->name }} {{ date_format($movstk->updated_at, 'd/m/Y H:i:s') }}" readonly>
                        </fieldset>
                    @endif
                </div>
                <div class="box-footer ptb-10">
                    <div class="pull-right pr-5">
                        @if ($edit==1)
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                            <button type="submit" name="action" value="save" class="btn btn-primary">
                                <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            </button>
                        @else
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@include('utils.newprodmodal')
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<style>
    #loading{
        display: inline !important;
    }
</style>
@stop

@section('js')
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    $(document.body).addClass('sidebar-collapse');
    $(function () {
        $('[data-toggle="popover"]').popover()
    })

    $("#id_motivo").select2();
    $("#id_item").select2({placeholder: "Buscar/Seleccionar Producto",allowClear: true, minimumInputLength: 3, disabled: true});
    $("#id_sector_o").select2({disabled: true});
    $("#id_ubicacion_o").select2({disabled: true});
    $("#id_posicion_o").select2({disabled: true});
    $("#id_sector_d").select2({});
    $("#id_ubicacion_d").select2({disabled: true});
    $("#id_posicion_d").select2({disabled: true});

    $("#id_motivo").change(function() {
        var combomot = document.getElementById("id_motivo");
        var idmot = combomot.options[combomot.selectedIndex].value;
        if ((idmot==4) || (idmot==5)){
            $("#id_sector_o").val('').trigger('change')
            $("#id_sector_o").prop("disabled", true);
            $("#id_ubicacion_o").val('').trigger('change')
            $("#id_ubicacion_o").prop("disabled", true);
            $("#id_posicion_o").val('').trigger('change')
            $("#id_posicion_o").prop("disabled", true);
        }
        $("#id_item").prop("disabled", false);
    });

    $("#id_item").change(function() {
        var comboitem = document.getElementById("id_item");
        var iditem = comboitem.options[comboitem.selectedIndex].value;
        if (iditem>0){
            var combomot = document.getElementById("id_motivo");
            var idmot = combomot.options[combomot.selectedIndex].value;
            if ((idmot!=4) && (idmot!=5)){
                $("#id_sector").prop("disabled", true);
                $.ajax('/datos/getsectores/'+iditem, {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_sector_o').empty();
                        if (data.length==1){
                            $("#id_sector_o").append(
                                '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                            );
                            $("#id_sector_o").prop("disabled", true);
                            $("#id_sector_o").val(data[0].id).trigger('change');
                        }else{
                            $.each(data, function(index) {
                                $("#id_sector_o").append(
                                    '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                                );
                            });
                            $("#id_sector_o").prop("disabled", false);
                            $("#id_sector_o").val('').trigger('change');
                        }
                    }
                });
                $("#id_ubicacion_o").prop("disabled", true);
                $.ajax('/datos/getubicaciones/'+iditem, {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_ubicacion_o').empty();
                        if (data.length==1){
                            $("#id_ubicacion_o").append(
                                '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                            );
                            $("#id_ubicacion_o").prop("disabled", true);
                            $("#id_ubicacion_o").val(data[0].id).trigger('change');
                        }else{
                            $.each(data, function(index) {
                                $("#id_ubicacion_o").append(
                                    '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                                );
                            });
                            $("#id_ubicacion_o").prop("disabled", false);
                            $("#id_ubicacion_o").val('').trigger('change');
                        }
                    }
                });
                $("#id_posicion_o").prop("disabled", true);
                $.ajax('/datos/getposiciones/'+iditem, {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_posicion_o').empty();
                        if (data.length==1){
                            $("#id_posicion_o").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_posicion_o").prop("disabled", true);
                            $("#id_posicion_o").val(data[0].id).trigger('change');
                        }else{
                            $.each(data, function(index) {
                                $("#id_posicion_o").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_posicion_o").prop("disabled", false);
                            $("#id_posicion_o").val('').trigger('change');
                        }
                    }
                });
            }
        }else{
            $('#id_sector_o').empty();
            $("#id_sector_o").prop("disabled", true);
            $("#id_sector_o").val('').trigger('change');
            $('#id_posicion_o').empty();
            $("#id_posicion_o").prop("disabled", true);
            $("#id_posicion_o").val('').trigger('change');
            $('#id_ubicacion_o').empty();
            $("#id_ubicacion_o").prop("disabled", true);
            $("#id_ubicacion_o").val('').trigger('change');
        }
    });

    $("#id_sector_d").change(function(){
        if ($(this).val() > 0){
            $.ajax('/datos/getubicaciones_sector/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_ubicacion_d').empty();
                    if (data.length==1){
                        $("#id_ubicacion_d").append(
                            '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                        );
                        $("#id_ubicacion_d").prop("disabled", true);
                        $("#id_ubicacion_d").val(data[0].id).trigger('change');
                    }else{
                        $("#id_ubicacion_d").append(
                            '<option value=""></option>'
                        );
                        $.each(data, function(index) {
                            $("#id_ubicacion_d").append(
                                '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                            );
                        });
                        $("#id_ubicacion_d").prop("disabled", false);
                    }
                }
            });
        }
    });

    $("#id_ubicacion_d").change(function(){
        if ($(this).val() > 0){
            $.ajax('/datos/getposiciones_ubicacion/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_posicion_d').empty();
                    if (data.length==1){
                        $("#id_posicion_d").append(
                            '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                        );
                        $("#id_posicion_d").prop("disabled", true);
                        $("#id_posicion_d").val(data[0].id).trigger('change');
                    }else{
                        $("#id_posicion_d").append(
                            '<option value=""></option>'
                        );
                        $.each(data, function(index) {
                            $("#id_posicion_d").append(
                                '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                            );
                        });
                        $("#id_posicion_d").prop("disabled", false);
                    }
                }
            });
        }
    });

    $('#form1').validate({
        rules: {
            id_item: {required: true},
            cantidad: {required: true}
        },
        messages: {
            id_item: {required: 'Ingrese un Producto'},
            cantidad: {required: 'Ingrese la Cantidad'}
        }
    });

    $('#form1').submit(function (event) {
        $("#id_item").prop("disabled", false);
        $("#id_sector_o").prop("disabled", false);
        $("#id_ubicacion_o").prop("disabled", false);
        $("#id_posicion_o").prop("disabled", false);
        $("#id_sector_d").prop("disabled", false);
        $("#id_ubicacion_d").prop("disabled", false);
        $("#id_posicion_d").prop("disabled", false);
    });

    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }

    $(window).on('load', function(event){
        $("#loading").button('loading');
        $("#loading").removeClass('hidden');
        $.ajax('/datos/getproductos', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $.each(data, function(index) {
                    $("#id_item").append(
                        '<option value="' + data[index].id + '">' + data[index].producto + '</option>'
                    );
                });
                $("#id_item").prop("disabled", false);
                $("#loading").button('reset');
                $("#loading").addClass('hidden');
            }
        });
    });
</script>
@include('utils.newprodmodaljs')
@include('utils.statusnotification')
@stop