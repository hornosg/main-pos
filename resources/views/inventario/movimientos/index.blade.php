@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.inventarionavbar')
    <div id="content_header" class="box box-primary collapsed-box">
        <div class="box-header with-border" style="height: 45px;">
            <div class="box-tools pull-right">
                <span style="margin-right: 10px;">Nuevo Movimiento/Ajuste de Stock</span>
                <button type="button" class="btn btn-primary" data-widget="collapse" onclick="scroll_to_anchor('content_header')"><i class="fa fa-plus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
            <form id="form1" class="form-inline" role="form">
                {{ csrf_field() }}
                <div class="form-group" style="width: 190px;">
                    <label class="control-label">Motivo</label>
                    <select id="id_motivo" name="id_motivo" class="form-control text-uppercase">
                        <option value=""></option>
                        @foreach(DB::table('sys_motivos_mstock')
                        ->where('sys_motivos_mstock.activo', 1)
                        ->where('sys_motivos_mstock.interno', 0)
                        ->select('sys_motivos_mstock.*')
                        ->get() as $motivo)
                            <option value="{{$motivo->id}}">{{$motivo->descripcion}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" style="width:600px;">
                    <label class="control-label">Producto</label>
                    <a class='text-primary pull-right' href='#' data-toggle='modal' data-target='#prodModal' title='Crear Nuevo Producto'>Crear Nuevo</i></a>
                    <select id="id_item" name="id_item" class="form-control text-uppercase">
                    <option value=""></option>
                    </select>
                </div>
                <div class="form-group" style="width:100px;">
                    <label class="control-label">Cantidad</label>
                    <input id="cantidad"  class="form-control input-sm text-bold" style="width:90px" name="cantidad" placeholder="">
                </div>

                <div class="clearfix visible-lg-block visible-md-block"></div>

                <div class="form-group"  style="width: 205px;">
                    <label class="control-label">Sector Origen</label>
                    <select id="id_sector_o" name="id_sector_o" class="form-control text-uppercase" >
                    <option value=""></option>
                    </select>
                </div>

                <div class="form-group"  style="width: 205px;">
                    <label class="control-label">Ubicacion Origen</label>
                    <select id="id_ubicacion_o" name="id_ubicacion_o" class="form-control text-uppercase" >
                    <option value=""></option>

                    </select>
                </div>

                <div class="form-group"  style="width: 205px;">
                    <label class="control-label">Posicion Origen</label>
                    <select id="id_posicion_o" name="id_posicion_o" class="form-control text-uppercase" >
                    <option value=""></option>
                    </select>
                </div>

                <div class="form-group"  style="width: 205px;">
                    <label class="control-label">Sector Destino</label>
                    <select id="id_sector_d" name="id_sector_d" class="form-control text-uppercase" >
                    <option value=""></option>
                    @foreach(DB::table('inv_sectores')
                    ->where('inv_sectores.activo', 1)
                    ->select('inv_sectores.*')
                    ->get() as $sector)
                    <option value="{{$sector->id}}">{{$sector->alias}} ( {{$sector->descripcion}} )</option>
                    @endforeach
                    </select>
                </div>

                <div class="form-group"  style="width: 205px;">
                    <label class="control-label">Ubicacion Destino</label>
                    <select id="id_ubicacion_d" name="id_ubicacion_d" class="form-control text-uppercase" >
                    <option value=""></option>
                    </select>
                </div>

                <div class="form-group"  style="width: 205px;">
                    <label class="control-label">Posicion Destino</label>
                    <select id="id_posicion_d" name="id_posicion_d" class="form-control text-uppercase" >
                    <option value=""></option>
                    </select>
                </div>

                <div class="form-group pull-right" style="padding-top: 16px;padding-left: 10px;">
                    <button id="guardar" type="button" class='btn bg-green' onclick='GuardarItem()' data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>" title="Guardar"> <i class="fa fa-btn fa-save text-white"></i> Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
<!--                            <div class="form-inline" role="form">-->
<!--                                <a href="/inventario/movimientos/create" class="btn btn-primary"  title="Nuevo Movimientos de Stock">-->
<!--                                    <i class="fa fa-btn fa-plus text-white"></i>-->
<!--                                </a>-->
<!--                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-unique-id='id'>

                            <thead class='thead-inverse'>
                            <th data-checkbox='true' class="hidden"></th>
                            <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                            <th data-field='creado' class='text-uppercase'>Creado</th>
                            <th data-field='motivo' class='text-uppercase'>Motivo</th>
                            <th data-field='producto' class='text-uppercase'>Producto</th>
                            <th data-field='origen'  class='text-uppercase  text-bold'>Origen</th>
                            <th data-field='destino' class='text-uppercase  text-bold'>Destino</th>
                            <th data-field='cantidad' data-align='right' class='text-uppercase text-bold'>Cantidad</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer ptb-10">
                <div class="pull-right pr-5">
                    <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>
@include('utils.newprodmodal')
@stop


@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<style>
    .form-inline .form-group {
        padding-right: 10px;
    }
</style>
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<!--<script src="/js/bootstrap-table-filter.js"></script>-->
<!--<script src="/js/bootstrap-table-filter-control.js"></script>-->
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    var datos = <?php echo json_encode($items); ?>;
    $(document.body).addClass('sidebar-collapse');
    var $table = $('#table1');
    $table.bootstrapTable({
        data: datos,
        exportDataType:'all',
        exportOptions:{fileName: 'items'}
    });
    $("#id_motivo").select2({allowClear: true});
    $("#id_item").select2({
        placeholder: "Buscar/Seleccionar Producto",
        allowClear: true,
        minimumInputLength: 4,
        ajax: {
            url: '/datos/getproductos_v2',
            dataType: 'json'
        }
    });
    $("#id_sector_o").select2({allowClear: true,disabled: true});
    $("#id_ubicacion_o").select2({allowClear: true,disabled: true});
    $("#id_posicion_o").select2({allowClear: true,disabled: true});
    $("#id_sector_d").select2({allowClear: true});
    $("#id_ubicacion_d").select2({allowClear: true,disabled: true});
    $("#id_posicion_d").select2({allowClear: true,disabled: true});

    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }

    $("#id_motivo").change(function() {
        var combomot = document.getElementById("id_motivo");
        var idmot = combomot.options[combomot.selectedIndex].value;
        if ((idmot==4) || (idmot==5)){
            $("#id_sector_o").val('').trigger('change')
            $("#id_sector_o").prop("disabled", true);
            $("#id_ubicacion_o").val('').trigger('change')
            $("#id_ubicacion_o").prop("disabled", true);
            $("#id_posicion_o").val('').trigger('change')
            $("#id_posicion_o").prop("disabled", true);
        }
        $("#id_item").prop("disabled", false);
    });

    $("#id_item").change(function() {
        var comboitem = document.getElementById("id_item");
        var iditem = comboitem.options[comboitem.selectedIndex].value;
        if (iditem>0){
            var combomot = document.getElementById("id_motivo");
            var idmot = combomot.options[combomot.selectedIndex].value;
            if ((idmot!=4) && (idmot!=5)){
                $("#id_sector").prop("disabled", true);
                $.ajax('/datos/getsectores/'+iditem, {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_sector_o').empty();
                        if (data.length==1){
                            $("#id_sector_o").append(
                                '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                            );
                            $("#id_sector_o").prop("disabled", true);
                            $("#id_sector_o").val(data[0].id).trigger('change');
                        }else{
                            $.each(data, function(index) {
                                $("#id_sector_o").append(
                                    '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                                );
                            });
                            $("#id_sector_o").prop("disabled", false);
                            $("#id_sector_o").val('').trigger('change');
                        }
                    }
                });

            }
        }else{
            $('#id_sector_o').empty();
            $("#id_sector_o").prop("disabled", true);
            $("#id_sector_o").val('').trigger('change');
            $('#id_posicion_o').empty();
            $("#id_posicion_o").prop("disabled", true);
            $("#id_posicion_o").val('').trigger('change');
            $('#id_ubicacion_o').empty();
            $("#id_ubicacion_o").prop("disabled", true);
            $("#id_ubicacion_o").val('').trigger('change');
        }
    });

    $("#id_sector_o").change(function(){
        if ($(this).val() > 0){
            $.ajax('/datos/getubicaciones_sector/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_ubicacion_o').empty();
                    $('#id_posicion_o').empty();
                    if (data.length==1){
                        $("#id_ubicacion_o").append(
                            '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                        );
                        $("#id_ubicacion_o").prop("disabled", true);
                        $("#id_ubicacion_o").val(data[0].id).trigger('change');
                    }else{
                        $("#id_ubicacion_o").append(
                            '<option value=""></option>'
                        );
                        $.each(data, function(index) {
                            $("#id_ubicacion_o").append(
                                '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                            );
                        });
                        $("#id_ubicacion_o").prop("disabled", false);
                    }
                }
            });
        }
    });

    $("#id_ubicacion_o").change(function(){
        if ($(this).val() > 0){
            $.ajax('/datos/getposiciones_ubicacion/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_posicion_o').empty();
                    if (data.length==1){
                        $("#id_posicion_o").append(
                            '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                        );
                        $("#id_posicion_o").prop("disabled", true);
                        $("#id_posicion_o").val(data[0].id).trigger('change');
                    }else{
                        $("#id_posicion_o").append(
                            '<option value=""></option>'
                        );
                        $.each(data, function(index) {
                            $("#id_posicion_o").append(
                                '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                            );
                        });
                        $("#id_posicion_o").prop("disabled", false);
                    }
                }
            });
        }
    });
    
    $("#id_sector_d").change(function(){
        if ($(this).val() > 0){
            $.ajax('/datos/getubicaciones_sector/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_ubicacion_d').empty();
                    $('#id_posicion_d').empty();
                    if (data.length==1){
                        $("#id_ubicacion_d").append(
                            '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                        );
                        $("#id_ubicacion_d").prop("disabled", true);
                        $("#id_ubicacion_d").val(data[0].id).trigger('change');
                    }else{
                        $("#id_ubicacion_d").append(
                            '<option value=""></option>'
                        );
                        $.each(data, function(index) {
                            $("#id_ubicacion_d").append(
                                '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                            );
                        });
                        $("#id_ubicacion_d").prop("disabled", false);
                    }
                }
            });
        }
    });

    $("#id_ubicacion_d").change(function(){
        if ($(this).val() > 0){
            $.ajax('/datos/getposiciones_ubicacion/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_posicion_d').empty();
                    if (data.length==1){
                        $("#id_posicion_d").append(
                            '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                        );
                        $("#id_posicion_d").prop("disabled", true);
                        $("#id_posicion_d").val(data[0].id).trigger('change');
                    }else{
                        $("#id_posicion_d").append(
                            '<option value=""></option>'
                        );
                        $.each(data, function(index) {
                            $("#id_posicion_d").append(
                                '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                            );
                        });
                        $("#id_posicion_d").prop("disabled", false);
                    }
                }
            });
        }
    });

    function GuardarItem(){
        $("#id_sector_o").prop("disabled", false);
        $("#id_ubicacion_o").prop("disabled", false);
        $("#id_posicion_o").prop("disabled", false);
        $("#id_sector_d").prop("disabled", false);
        $("#id_ubicacion_d").prop("disabled", false);
        $("#id_posicion_d").prop("disabled", false);

        $("#guardar").button('loading');
        $.ajax({
            type: "POST",
            url:'/ajax/movimientos/store',
            data: $('#form1').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                $('#table1').bootstrapTable('append', data.item);
                $("#codproducto").val('');
                $("#descripcion").val('');
                $("#ean").val('');
                //document.getElementById('form1').reset();

                $("#guardar").button('reset');
                $("#guardar").dequeue();
                $("#guardar").prop("disabled", false);
            },
            error:function(data) {
                console.log(data);
                iziToast.error({
                    timeout: 3000,
                    position:'center',
                    title: 'Error:',
                    message: 'Hubo un error, Los campos marcados con un (*) son obligatorios'
                });
                $("#guardar").button('reset');
                $("#guardar").dequeue();
                $("#guardar").prop("disabled", false);
            }
        });
    }

    function Borrar(idrow){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $.ajax({
                        type: "GET",
                        url:'/inventario/movimientos/delete/'+idrow,
                        data: idrow,
                        success: function(data) {
                            $('#table1').bootstrapTable('removeByUniqueId', idrow);
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    $(function () {
        $('#btn-eliminar').click(function (e) {
            e.preventDefault();
            var choice;
            iziToast.question({
                close: true,
                overlay: true,
                displayMode: 'once',
                id: 'question',
                zindex: 999,
                title: 'Atencion!',
                message: '¿Está seguro de Eliminar todos los items seleccionados?',
                position: 'center',
                buttons: [
                    ['<button><b>SI</b></button>', function (instance, toast) {
                        var ids = $.map($('#table1').bootstrapTable('getAllSelections'), function (row) {
                            return row.id;
                        });
                        $('#table1').bootstrapTable('remove', {
                            field: 'id',
                            values: ids
                        });

                        $.ajax('/inventario/movimientos/removeselected', {
                            type: 'POST',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: {datos:ids}
                        });

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        iziToast.success({
                            timeout: 2000,
                            position:'topCenter',
                            title: 'Listo!',
                            message: 'Items, Eliminados.'
                        });
                    }, true],
                    ['<button>NO</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    }]
                ]
            });
        });
    });
</script>
@include('utils.statusnotification')
@include('utils.newprodmodaljs')
@stop
