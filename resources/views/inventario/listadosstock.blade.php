@extends('adminlte::page')
@section('title', 'Listado Stock')

@section('content_header')
@include('layouts.inventarionavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div id="detallestock" class="box box-primary">
            <div id="box-header">
                <div id="toolbar">
                    <h4 class="box-title">Detalle de productos en Stock</h4>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-trim-on-search="false"
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-detail-view='true'
                               data-unique-id='id'>
                            <thead class='thead-inverse'>
                            <th data-checkbox='true' class="hidden"></th>
                            <th data-field='id' class='hidden'>Id</th>
                            <th data-field='descripcion'   class='text-nowrap text-uppercase'>Descripcion</th>
                            <th data-field='contenidoNeto'  class='text-nowrap text-uppercase'>C. Neto</th>
                            <th data-field='marca'   class='text-nowrap text-uppercase'>Marca</th>
                            <th data-field='rubro'   class='text-nowrap text-uppercase'>Rubro</th>
                            <th data-field='subrubro'   class='text-nowrap text-uppercase'>subRubro</th>
                            <th data-field='costo'  data-align='right' class='text-uppercase'>P. Costo</th>
                            <th data-field='precio'  data-align='right' class='text-uppercase'>P. Venta</th>
                            <th data-field='stockMinimo'  data-align='right' class='text-uppercase'>Stock Min</th>
                            <th data-field='cantidad'  data-align='right' class='text-uppercase'>En Stock</th>
                            <th data-field='status' class='text-nowrap text-uppercase'>Estado</th>
<!--                            <th data-field='totalcosto'  data-align='right' class='text-uppercase'>Total P.Costo</th>-->
<!--                            <th data-field='total'  data-align='right' class='text-uppercase'>Total P. Venta</th>-->
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer ptb-10">
                <div class="pull-right pr-5">
                    <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
//foreach ($items as $item) {
//    $item->totalcosto = '$ '.round($item->costo*$item->cantidad,2);
//    $item->total = '$ '.round($item->precio*$item->cantidad,2);
//}
?>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<!-- Bootstrap-Table -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    var datos = <?php echo json_encode($items); ?>;
    $('#table1').bootstrapTable({data: datos,exportDataType:'all',exportOptions:{fileName: 'productos'}});
    $('#table1').on('expand-row.bs.table', function (e, index, row, $detail) {
        $idtabla2="tabla"+row.id;
        html='<div class="pt-10 prl-5"><div class=""><table id='+$idtabla2+' class="table table-sm table-bordered table-hover table-striped table-condensed table-responsive" ' +
            'data-pagination="true">' +
            '<thead class="thead-inverse-gray"><tr>' +
            '<th data-field="sucursal">Sucursal</th>' +
            '<th data-field="id_sector" class="hidden"></th>' +
            '<th data-field="sector">Sector</th>' +
            '<th data-field="ubicacion">Ubicacion</th>' +
            '<th data-field="id_ubicacion" class="hidden"></th>' +
            '<th data-field="posicion">Posicion</th>' +
            '<th data-field="id_posicion" class="hidden"></th>' +
            '<th data-field="cantidad" data-align="right">Stock</th>' +
            '<th data-align="center" data-formatter="FunctionsFormatter"></th>' +
            '</tr></thead>' +
            '</table></div></div>';
        $detail.html(html);
        $('#'+$idtabla2).bootstrapTable();
        $('#'+$idtabla2).bootstrapTable('showLoading');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'/inventario/stock/detalle/'+row.id,
            data: row.id,
            success: function(datosdetalle) {
                $('#'+$idtabla2).bootstrapTable('append', datosdetalle);
                $('#'+$idtabla2).bootstrapTable('hideLoading');
            }
        });
    });

    function FunctionsFormatter(value, row, index) {
        if (row.id_posicion > 0) {
            return "<a class='btn-outline-success' href='#' onclick='NuevoConteo("+row.id_sector+","+row.id_ubicacion+","+row.id_posicion+")' title='Nuevo Control de Stock'><i class='fa fa-fw fa-gears'></i></a>" ;
        }else{
            if (row.id_ubicacion > 0) {
                return "<a class='btn-outline-success' href='#' onclick='NuevoConteo("+row.id_sector+","+row.id_ubicacion+",null)' title='Nuevo Control de Stock'><i class='fa fa-fw fa-gears'></i></a>" ;
            }else{
                return "<a class='btn-outline-success' href='#' onclick='NuevoConteo("+row.id_sector+",null,null)' title='Nuevo Control de Stock'><i class='fa fa-fw fa-gears'></i></a>" ;
            }
        }
    };

    function NuevoConteo(idrow){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Desea generar un nuevo control de Stock para este registro?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $.ajax({
                        type: "GET",
                        url:'/inventario/stock/generarcontrol/'+idrow,
                        success: function(data) {
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }
</script>
@stop