@extends('adminlte::page')

@section('title', $titulo)

@php
if ($edit==0){
    $editable='readonly';
    $visible='d-none';
    $disabled='disabled';
    $accion = 'Visualizar';
}else{
    $editable=' ';
    $visible=' ';
    $disabled=' ';
    $accion = 'Editar';
}
@endphp

@section('content_header')
    @include('layouts.inventarionavbar')
@stop

@section('content')
<div id="content" class="container-fluid row">
    <form id="form1" class="" role="form" method="POST" action="/inventario/controles/store" enctype="multipart/form-data">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>{{$tituloform}}</strong></h3>
                </div>
                <div class="box-body">
                    {{ csrf_field() }}

                    <div class="form-group col-md-9 col-md-offset-right-6">
                        <label for="id_item" class="control-label">Producto</label>
                        <select id="id_item" name="id_item" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                                @foreach(DB::table('items')->where('id_tipoitem', 5)
                                        ->where('items.activo', 1)
                                        ->whereRaw('0 < (SELECT sum(cantidad) as stock  FROM inv_stock s
                                                          WHERE s.id_sector is not null AND s.id_item=items.id)')
                                        ->leftJoin('conf_marcas', 'conf_marcas.id', '=', 'items.id_marca')
                                        ->leftJoin('conf_unidades_medida', 'conf_unidades_medida.id', '=', 'items.id_unidadcontneto')
                                        ->select('items.*', 'conf_marcas.descripcion as marca', 'conf_unidades_medida.descripcion as unimed')
                                        ->get() as $item)
                                    <option value="{{$item->id}}">[{{$item->id}}] {{$item->descripcion}} - {{$item->contNeto}} {{$item->unimed}} ({{$item->marca}}) [ean:{{$item->ean}}]</option>
                                @endforeach
                        </select>
                    </div>
                    <div class="clearfix visible-lg-block visible-md-block"></div>

                    <div class="form-group col-md-4">
                        <label for="id_marca" class="control-label">Marca</label>
                        <select id="id_marca" name="id_marca" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::select('select m.id, m.descripcion, sum(cantidad)
                                                   from inv_stock s
                                                   join items i on i.id = s.id_item
                                                   join conf_marcas m on m.id = i.id_marca
                                                  where id_sector is not null
                                                   GROUP BY 1,2
                                                   HAVING sum(cantidad) > 0') as $marca)
                                <option value="{{$marca->id}}">{{$marca->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_rubro" class="control-label">Rubro</label>
                        <select id="id_rubro" name="id_rubro" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::select('select r.id, r.descripcion, sum(cantidad)
                                                   from inv_stock s
                                                   join items i on i.id = s.id_item
                                                   join conf_rubros r on r.id = i.id_rubro
                                                  where id_sector is not null
                                                  GROUP BY 1,2
                                                  HAVING sum(cantidad) > 0') as $rubros)
                                <option value="{{$rubros->id}}">{{$rubros->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_subrubro" class="control-label">Subrubro</label>
                        <select id="id_subrubro" name="id_subrubro" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::select('select sr.id, sr.descripcion, sum(cantidad)
                                                   from inv_stock s
                                                   join items i on i.id = s.id_item
                                                   join conf_subrubros sr on sr.id = i.id_subrubro
                                                  where id_sector is not null
                                                  GROUP BY 1,2
                                                  HAVING sum(cantidad) > 0') as $subrubro)
                                <option value="{{$subrubro->id}}">{{$subrubro->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_sector" class="control-label">Sector</label>
                        <select id="id_sector" name="id_sector" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::table('inv_sectores')
                                    ->where('inv_sectores.activo', 1)
                                    ->select('inv_sectores.*')
                                    ->get() as $sector)
                                <option value="{{$sector->id}}">{{$sector->alias}} ({{$sector->descripcion}})</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_ubicacion" class="control-label">Ubicacion</label>
                        <select id="id_ubicacion" name="id_ubicacion" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::table('inv_ubicaciones')
                                    ->where('inv_ubicaciones.activo', 1)
                                    ->select('inv_ubicaciones.*')
                                    ->get() as $ubicacion)
                                <option value="{{$ubicacion->id}}">{{$ubicacion->alias}} ({{$ubicacion->descripcion}})</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_posicion" class="control-label">Posicion</label>
                        <select id="id_posicion" name="id_posicion" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::table('inv_posiciones')
                                    ->where('inv_posiciones.activo', 1)
                                    ->select('inv_posiciones.*')
                                    ->get() as $posicion)
                                <option value="{{$posicion->id}}">{{$posicion->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="box-footer ptb-10">
                    <div class="pull-right pr-5">
                        @if ($edit==1)
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                            <button type="submit" name="action" value="save" class="btn btn-primary">
                                <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            </button>
                        @else
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@include('utils.newprodmodal')
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    $(document.body).addClass('sidebar-collapse');
    $(function () {
        $('[data-toggle="popover"]').popover()
    })

    $("#id_item").select2({placeholder: "Buscar/Seleccionar Producto",allowClear: true, minimumInputLength: 3});
    $("#id_marca").select2();
    $("#id_rubro").select2();
    $("#id_subrubro").select2();
    $("#id_sector").select2();
    $("#id_ubicacion").select2();
    $("#id_posicion").select2();


    $("#id_item").change(function() {
        var comboitem = document.getElementById("id_item");
        var iditem = comboitem.options[comboitem.selectedIndex].value;
        $("#id_marca").prop("disabled", true);
        $.ajax('/datos/getmarcas/'+iditem, {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_marca').empty();
                if (data.length==1){
                    $("#id_marca").append(
                        '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                    );
                    $("#id_marca").prop("disabled", true);
                    $("#id_marca").val(data[0].id).trigger('change');
                }else{
                    $.each(data, function(index) {
                        $("#id_marca").append(
                            '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                        );
                    });
                    $("#id_marca").prop("disabled", false);
                    $("#id_marca").val('').trigger('change');
                }
            }
        });
        $("#id_rubro").prop("disabled", true);
        $.ajax('/datos/getrubros/'+iditem, {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_rubro').empty();
                if (data.length==1){
                    $("#id_rubro").append(
                        '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                    );
                    $("#id_rubro").prop("disabled", true);
                    $("#id_rubro").val(data[0].id).trigger('change');
                }else{
                    $.each(data, function(index) {
                        $("#id_rubro").append(
                            '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                        );
                    });
                    $("#id_rubro").prop("disabled", false);
                    $("#id_rubro").val('').trigger('change');
                }
            }
        });
        $("#id_subrubro").prop("disabled", true);
        $.ajax('/datos/getsubrubros/'+iditem, {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_subrubro').empty();
                if (data.length==1){
                    $("#id_subrubro").append(
                        '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                    );
                    $("#id_subrubro").prop("disabled", true);
                    $("#id_subrubro").val(data[0].id).trigger('change');
                }else{
                    $.each(data, function(index) {
                        $("#id_subrubro").append(
                            '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                        );
                    });
                    $("#id_subrubro").prop("disabled", false);
                    $("#id_subrubro").val('').trigger('change');
                }
            }
        });
        $("#id_sector").prop("disabled", true);
        $.ajax('/datos/getsectores/'+iditem, {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_sector').empty();
                if (data.length==1){
                    $("#id_sector").append(
                        '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                    );
                    $("#id_sector").prop("disabled", true);
                    $("#id_sector").val(data[0].id).trigger('change');
                }else{
                    $.each(data, function(index) {
                        $("#id_sector").append(
                            '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                        );
                    });
                    $("#id_sector").prop("disabled", false);
                    $("#id_sector").val('').trigger('change');
                }
            }
        });
        $("#id_ubicacion").prop("disabled", true);
        $.ajax('/datos/getubicaciones/'+iditem, {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_ubicacion').empty();
                if (data.length==1){
                    $("#id_ubicacion").append(
                        '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                    );
                    $("#id_ubicacion").prop("disabled", true);
                    $("#id_ubicacion").val(data[0].id).trigger('change');
                }else{
                    $.each(data, function(index) {
                        $("#id_ubicacion").append(
                            '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                        );
                    });
                    $("#id_ubicacion").prop("disabled", false);
                    $("#id_ubicacion").val('').trigger('change');
                }
            }
        });
        $("#id_posicion").prop("disabled", true);
        $.ajax('/datos/getposiciones/'+iditem, {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_posicion').empty();
                if (data.length==1){
                    $("#id_posicion").append(
                        '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                    );
                    $("#id_posicion").prop("disabled", true);
                    $("#id_posicion").val(data[0].id).trigger('change');
                }else{
                    $.each(data, function(index) {
                        $("#id_posicion").append(
                            '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                        );
                    });
                    $("#id_posicion").prop("disabled", false);
                    $("#id_posicion").val('').trigger('change');
                }
            }
        });
    });

    $("#id_marca").change(function(){
        if (!$("#id_marca").prop("disabled")){
            if ($(this).val()>0){
                $("#id_item").prop("disabled", true);
            }else{
                if  (($("#id_rubro").val() == "") || ($("#id_rubro").val() == null) && ($("#id_subrubro").val() == "") || ($("#id_subrubro").val() == null)) {
                    $("#id_item").prop("disabled", false);
                }
            }
            if  (($("#id_rubro").val() == "") || ($("#id_rubro").val() == null) || ( ($("#id_rubro").val() != "") && ($("#id_rubro").prop("disabled")))){
                $("#id_rubro").prop("disabled", true);
                $.ajax('/datos/getrubros_marca/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_rubro').empty();
                        if (data.length==1){
                            $("#id_rubro").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_rubro").prop("disabled", true);
                            $("#id_rubro").val(data[0].id).trigger('change');
                        }else{
                            $("#id_rubro").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_rubro").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_rubro").prop("disabled", false);
                        }
                    }
                });
            }
            if  (($("#id_subrubro").val() == "") || ($("#id_subrubro").val() == null) || ( ($("#id_subrubro").val() != "") && ($("#id_subrubro").prop("disabled")))){
                $("#id_subrubro").prop("disabled", true);
                $.ajax('/datos/getsubrubros_marca/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_subrubro').empty();
                        if (data.length==1){
                            $("#id_subrubro").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_subrubro").prop("disabled", true);
                            $("#id_subrubro").val(data[0].id).trigger('change');
                        }else{
                            $("#id_subrubro").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_subrubro").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_subrubro").prop("disabled", false);
                        }
                    }
                });
            }
            $("#id_sector").prop("disabled", true);
            $.ajax('/datos/getsectores_marca/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_sector').empty();
                    if (data.length==1){
                        $("#id_sector").append(
                            '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                        );
                        $("#id_sector").prop("disabled", true);
                        $("#id_sector").val(data[0].id).trigger('change');
                    }else{
                        $.each(data, function(index) {
                            $("#id_sector").append(
                                '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                            );
                        });
                        $("#id_sector").prop("disabled", false);
                        $("#id_sector").val('').trigger('change');
                    }
                }
            });
            $("#id_ubicacion").prop("disabled", true);
            $.ajax('/datos/getubicaciones_marca/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_ubicacion').empty();
                    if (data.length==1){
                        $("#id_ubicacion").append(
                            '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                        );
                        $("#id_ubicacion").prop("disabled", true);
                        $("#id_ubicacion").val(data[0].id).trigger('change');
                    }else{
                        $.each(data, function(index) {
                            $("#id_ubicacion").append(
                                '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                            );
                        });
                        $("#id_ubicacion").prop("disabled", false);
                        $("#id_ubicacion").val('').trigger('change');
                    }
                }
            });
            $("#id_posicion").prop("disabled", true);
            $.ajax('/datos/getposiciones_marca/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_posicion').empty();
                    if (data.length==1){
                        $("#id_posicion").append(
                            '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                        );
                        $("#id_posicion").prop("disabled", true);
                        $("#id_posicion").val(data[0].id).trigger('change');
                    }else{
                        $.each(data, function(index) {
                            $("#id_posicion").append(
                                '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                            );
                        });
                        $("#id_posicion").prop("disabled", false);
                        $("#id_posicion").val('').trigger('change');
                    }
                }
            });
        }
    });

    $("#id_rubro").change(function(){
        if (!$("#id_rubro").prop("disabled")){
            if ($(this).val()>0){
                $("#id_item").prop("disabled", true);
            }else{
                if  (($("#id_marca").val() == "") || ($("#id_marca").val() == null) && ($("#id_subrubro").val() == "") || ($("#id_subrubro").val() == null)) {
                    $("#id_item").prop("disabled", false);
                }
            }
            if  (($("#id_marca").val() == "") || ($("#id_marca").val() == null) || ( ($("#id_marca").val() != "") && ($("#id_marca").prop("disabled")))){
                $("#id_marca").prop("disabled", true);
                $.ajax('/datos/getmarcas_rubro/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_marca').empty();
                        if (data.length==1){
                            $("#id_marca").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_marca").prop("disabled", true);
                            $("#id_marca").val(data[0].id).trigger('change');
                        }else{
                            $("#id_marca").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_marca").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_marca").prop("disabled", false);
                        }
                    }
                });
            }
            if  (($("#id_subrubro").val() == "") || ($("#id_subrubro").val() == null) || ( ($("#id_subrubro").val() != "") && ($("#id_subrubro").prop("disabled")))){
                $("#id_subrubro").prop("disabled", true);
                $.ajax('/datos/getsubrubros_rubro/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_subrubro').empty();
                        if (data.length==1){
                            $("#id_subrubro").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_subrubro").prop("disabled", true);
                            $("#id_subrubro").val(data[0].id).trigger('change');
                        }else{
                            $("#id_subrubro").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_subrubro").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_subrubro").prop("disabled", false);
                        }
                    }
                });
            }
            $("#id_sector").prop("disabled", true);
            $.ajax('/datos/getsectores_rubro/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_sector').empty();
                    if (data.length==1){
                        $("#id_sector").append(
                            '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                        );
                        $("#id_sector").prop("disabled", true);
                        $("#id_sector").val(data[0].id).trigger('change');
                    }else{
                        $.each(data, function(index) {
                            $("#id_sector").append(
                                '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                            );
                        });
                        $("#id_sector").prop("disabled", false);
                        $("#id_sector").val('').trigger('change');
                    }
                }
            });
            $("#id_ubicacion").prop("disabled", true);
            $.ajax('/datos/getubicaciones_rubro/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_ubicacion').empty();
                    if (data.length==1){
                        $("#id_ubicacion").append(
                            '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                        );
                        $("#id_ubicacion").prop("disabled", true);
                        $("#id_ubicacion").val(data[0].id).trigger('change');
                    }else{
                        $.each(data, function(index) {
                            $("#id_ubicacion").append(
                                '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                            );
                        });
                        $("#id_ubicacion").prop("disabled", false);
                        $("#id_ubicacion").val('').trigger('change');
                    }
                }
            });
            $("#id_posicion").prop("disabled", true);
            $.ajax('/datos/getposiciones_rubro/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_posicion').empty();
                    if (data.length==1){
                        $("#id_posicion").append(
                            '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                        );
                        $("#id_posicion").prop("disabled", true);
                        $("#id_posicion").val(data[0].id).trigger('change');
                    }else{
                        $.each(data, function(index) {
                            $("#id_posicion").append(
                                '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                            );
                        });
                        $("#id_posicion").prop("disabled", false);
                        $("#id_posicion").val('').trigger('change');
                    }
                }
            });
        }
    });

    $("#id_subrubro").change(function(){
        if (!$("#id_subrubro").prop("disabled")){
            if ($(this).val()>0){
                $("#id_item").prop("disabled", true);
            }else{
                if  (($("#id_marca").val() == "") || ($("#id_marca").val() == null) && ($("#id_rubro").val() == "") || ($("#id_rubro").val() == null)) {
                    $("#id_item").prop("disabled", false);
                }
            }
            if  (($("#id_marca").val() == "") || ($("#id_marca").val() == null) || ( ($("#id_marca").val() != "") && ($("#id_marca").prop("disabled")))){
                $("#id_rubro").prop("disabled", true);
                $.ajax('/datos/getmarcas_subrubro/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_marca').empty();
                        if (data.length==1){
                            $("#id_marca").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_marca").prop("disabled", true);
                            $("#id_marca").val(data[0].id).trigger('change');
                        }else{
                            $("#id_marca").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_marca").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_marca").prop("disabled", false);
                        }
                    }
                });
            }
            if  (($("#id_rubro").val() == "") || ($("#id_rubro").val() == null) || ( ($("#id_rubro").val() != "") && ($("#id_rubro").prop("disabled")))){
                $("#id_subrubro").prop("disabled", true);
                $.ajax('/datos/getrubros_subrubro/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_rubro').empty();
                        if (data.length==1){
                            $("#id_rubro").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_rubro").prop("disabled", true);
                            $("#id_rubro").val(data[0].id).trigger('change');
                        }else{
                            $("#id_rubro").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_rubro").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_rubro").prop("disabled", false);
                        }
                    }
                });
            }
            $("#id_sector").prop("disabled", true);
            $.ajax('/datos/getsectores_subrubro/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_sector').empty();
                    if (data.length==1){
                        $("#id_sector").append(
                            '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                        );
                        $("#id_sector").prop("disabled", true);
                        $("#id_sector").val(data[0].id).trigger('change');
                    }else{
                        $.each(data, function(index) {
                            $("#id_sector").append(
                                '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                            );
                        });
                        $("#id_sector").prop("disabled", false);
                        $("#id_sector").val('').trigger('change');
                    }
                }
            });
            $("#id_ubicacion").prop("disabled", true);
            $.ajax('/datos/getubicaciones_subrubro/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_ubicacion').empty();
                    if (data.length==1){
                        $("#id_ubicacion").append(
                            '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                        );
                        $("#id_ubicacion").prop("disabled", true);
                        $("#id_ubicacion").val(data[0].id).trigger('change');
                    }else{
                        $.each(data, function(index) {
                            $("#id_ubicacion").append(
                                '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                            );
                        });
                        $("#id_ubicacion").prop("disabled", false);
                        $("#id_ubicacion").val('').trigger('change');
                    }
                }
            });
            $("#id_posicion").prop("disabled", true);
            $.ajax('/datos/getposiciones_subrubro/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_posicion').empty();
                    if (data.length==1){
                        $("#id_posicion").append(
                            '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                        );
                        $("#id_posicion").prop("disabled", true);
                        $("#id_posicion").val(data[0].id).trigger('change');
                    }else{
                        $.each(data, function(index) {
                            $("#id_posicion").append(
                                '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                            );
                        });
                        $("#id_posicion").prop("disabled", false);
                        $("#id_posicion").val('').trigger('change');
                    }
                }
            });
        }
    });


    $("#id_sector").change(function(){
        if (!$("#id_sector").prop("disabled")){
            if  ((($("#id_item").val() == "") || ($("#id_item").val() == null)) &&
                (($("#id_marca").val() == "") || ($("#id_marca").val() == null)) &&
                (($("#id_rubro").val() == "") || ($("#id_rubro").val() == null)) &&
                (($("#id_subrubro").val() == "") || ($("#id_subrubro").val() == null))){
                $("#id_item").prop("disabled", true);
                $("#id_marca").prop("disabled", true);
                $("#id_rubro").prop("disabled", true);
                $("#id_subrubro").prop("disabled", true);
            }

            if  (($("#id_ubicacion").val() == "") || ($("#id_ubicacion").val() == null) || ( ($("#id_ubicacion").val() != "") && ($("#id_ubicacion").prop("disabled")))){
                $("#id_ubicacion").prop("disabled", true);
                $.ajax('/datos/getubicaciones_sector/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_ubicacion').empty();
                        if (data.length==1){
                            $("#id_ubicacion").append(
                                '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                            );
                            $("#id_ubicacion").prop("disabled", true);
                            $("#id_ubicacion").val(data[0].id).trigger('change');
                        }else{
                            $("#id_ubicacion").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_ubicacion").append(
                                    '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                                );
                            });
                            $("#id_ubicacion").prop("disabled", false);
                        }
                    }
                });
            }
            if  (($("#id_posicion").val() == "") || ($("#id_posicion").val() == null) || ( ($("#id_posicion").val() != "") && ($("#id_posicion").prop("disabled")))){
                $("#id_posicion").prop("disabled", true);
                $.ajax('/datos/getposiciones_sector/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_posicion').empty();
                        if (data.length==1){
                            $("#id_posicion").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_posicion").prop("disabled", true);
                            $("#id_posicion").val(data[0].id).trigger('change');
                        }else{
                            $("#id_posicion").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_posicion").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_posicion").prop("disabled", false);
                        }
                    }
                });
            }
        }
    });

    $("#id_ubicacion").change(function(){
        if (!$("#id_ubicacion").prop("disabled")){
            if  ((($("#id_item").val() == "") || ($("#id_item").val() == null)) &&
                (($("#id_marca").val() == "") || ($("#id_marca").val() == null)) &&
                (($("#id_rubro").val() == "") || ($("#id_rubro").val() == null)) &&
                (($("#id_subrubro").val() == "") || ($("#id_subrubro").val() == null))){
                $("#id_item").prop("disabled", true);
                $("#id_marca").prop("disabled", true);
                $("#id_rubro").prop("disabled", true);
                $("#id_subrubro").prop("disabled", true);
            }

            if  (($("#id_sector").val() == "") || ($("#id_sector").val() == null) || ( ($("#id_sector").val() != "") && ($("#id_sector").prop("disabled")))){
                $("#id_sector").prop("disabled", true);
                $.ajax('/datos/getsectores_ubicacion/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_sector').empty();
                        $("#id_sector").append(
                            '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                        );
                        $("#id_sector").prop("disabled", true);
                        $("#id_sector").val(data[0].id).trigger('change');
                    }
                });
            }else{
                if (($(this).val()>0) && ($("#id_sector").val() != "") && (!$("#id_sector").prop("disabled"))){
                    $("#id_sector").prop("disabled", true);
                }else{
                    $("#id_sector").prop("disabled", false);
                }
            }
            if ($(this).val() > 0){
                $("#id_posicion").prop("disabled", true);
                $.ajax('/datos/getposiciones_ubicacion/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_posicion').empty();
                        if (data.length==1){
                            $("#id_posicion").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_posicion").prop("disabled", true);
                            $("#id_posicion").val(data[0].id).trigger('change');
                        }else{
                            $("#id_posicion").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_posicion").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_posicion").prop("disabled", false);
                        }
                    }
                });
            }else{
                $.ajax('/datos/getposiciones_sector/'+$("#id_sector").val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_posicion').empty();
                        if (data.length==1){
                            $("#id_posicion").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_posicion").prop("disabled", true);
                            $("#id_posicion").val(data[0].id).trigger('change');
                        }else{
                            $("#id_posicion").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_posicion").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_posicion").prop("disabled", false);
                        }
                    }
                });
            }
        }
    });

    $("#id_posicion").change(function(){
        if (!$("#id_posicion").prop("disabled")){
            if  ((($("#id_item").val() == "") || ($("#id_item").val() == null)) &&
                (($("#id_marca").val() == "") || ($("#id_marca").val() == null)) &&
                (($("#id_rubro").val() == "") || ($("#id_rubro").val() == null)) &&
                (($("#id_subrubro").val() == "") || ($("#id_subrubro").val() == null))){
                $("#id_item").prop("disabled", true);
                $("#id_marca").prop("disabled", true);
                $("#id_rubro").prop("disabled", true);
                $("#id_subrubro").prop("disabled", true);
            }

            if  (($("#id_ubicacion").val() == "") || ($("#id_ubicacion").val() == null) || ( ($("#id_ubicacion").val() != "") && ($("#id_ubicacion").prop("disabled")))){
                $("#id_ubicacion").prop("disabled", true);
                $.ajax('/datos/getubicaciones_posicion/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_ubicacion').empty();
                        if (data.length==1){
                            $("#id_ubicacion").append(
                                '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                            );
                            $("#id_ubicacion").prop("disabled", true);
                            $("#id_ubicacion").val(data[0].id).trigger('change');
                        }else{
                            $("#id_ubicacion").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_ubicacion").append(
                                    '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                                );
                            });
                            $("#id_ubicacion").prop("disabled", false);
                        }
                    }
                });
            }else{
                $("#id_sector").prop("disabled", true);
                $.ajax('/datos/getsectores_ubicacion/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_sector').empty();
                        if (data.length==1){
                            $("#id_sector").append(
                                '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                            );
                            $("#id_sector").prop("disabled", true);
                            $("#id_sector").val(data[0].id).trigger('change');
                        }else{
                            $("#id_sector").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_sector").append(
                                    '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                                );
                            });
                            $("#id_sector").prop("disabled", false);
                        }
                    }
                });
            }
            if  (($("#id_sector").val() == "") || ($("#id_sector").val() == null) || ( ($("#id_sector").val() != "") && ($("#id_sector").prop("disabled")))){
                if (($("#id_posicion").val() > 0)) {
                    $("#id_sector").prop("disabled", true);
                    $.ajax('/datos/getsectores_posicion/'+$(this).val(), {
                        type: 'GET',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function(data) {
                            $('#id_sector').empty();
                            if (data.length==1){
                                $("#id_sector").append(
                                    '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                                );
                                $("#id_sector").prop("disabled", true);
                                $("#id_sector").val(data[0].id).trigger('change');
                            }else{
                                $("#id_sector").append(
                                    '<option value=""></option>'
                                );
                                $.each(data, function(index) {
                                    $("#id_sector").append(
                                        '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                                    );
                                });
                                $("#id_sector").prop("disabled", false);
                            }
                        }
                    });
                }else{
                    if  ( ($("#id_ubicacion").val() != "") && (!$("#id_ubicacion").prop("disabled"))){
                        $("#id_sector").prop("disabled", true);
                        $.ajax('/datos/getsectores_ubicacion/'+$("#id_ubicacion").val(), {
                            type: 'GET',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            success: function(data) {
                                $('#id_sector').empty();
                                if (data.length==1){
                                    $("#id_sector").append(
                                        '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                                    );
                                    $("#id_sector").prop("disabled", true);
                                    $("#id_sector").val(data[0].id).trigger('change');
                                }else{
                                    $("#id_sector").append(
                                        '<option value=""></option>'
                                    );
                                    $.each(data, function(index) {
                                        $("#id_sector").append(
                                            '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                                        );
                                    });
                                    $("#id_sector").prop("disabled", false);
                                }
                            }
                        });
                    }
                }
            }
        }
    });

    $('#form1').submit(function (event) {
        if ((($("#id_item").val() == "") || ($("#id_item").val() == null)) &&
            (($("#id_marca").val() == "") || ($("#id_marca").val() == null)) &&
            (($("#id_rubro").val() == "") || ($("#id_rubro").val() == null)) &&
            (($("#id_subrubro").val() == "") || ($("#id_subrubro").val() == null)) &&
            (($("#id_sector").val() == "") || ($("#id_sector").val() == null)) &&
            (($("#id_ubicacion").val() == "") || ($("#id_ubicacion").val() == null)) &&
            (($("#id_posicion").val() == "") || ($("#id_posicion").val() == null)) ){
            iziToast.warning({
                title: 'Atencion!',
                message: 'No ha seleccionado ningun criterio de búsqueda. Debe seleccionar al menos un campo.',
                close: false,
                displayMode: 'once',
                position:'center'
            });
            event.preventDefault();
        }
        $("#id_item").prop("disabled", false);
        $("#id_marca").prop("disabled", false);
        $("#id_rubro").prop("disabled", false);
        $("#id_subrubro").prop("disabled", false);
        $("#id_sector").prop("disabled", false);
        $("#id_ubicacion").prop("disabled", false);
        $("#id_posicion").prop("disabled", false);
    });

    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }
</script>
@include('utils.newprodmodaljs')
@include('utils.statusnotification')
@stop