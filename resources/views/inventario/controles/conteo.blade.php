@extends('adminlte::page')

@section('title', $titulo)

@php
if ($edit==0){
    $editable='readonly';
    $visible='d-none';
    $accion = 'Visualizar';
}else{
    $editable=' ';
    $visible=' ';
    $accion = 'Editar';
}

if (!empty($conteo->id)){
    $disabledsec = 'disabled';
    $disabledubi = 'disabled';
    $disabledpos = 'disabled';
}else{
    $disabledsec = ( !empty($ctrol->id_sector) )? 'disabled':'';
    $disabledubi = ( !empty($ctrol->id_ubicacion) )? 'disabled':'';
    $disabledpos = ( !empty($ctrol->id_posicion) )? 'disabled':'';
}
@endphp


@section('content')
<div id="content" class="container-fluid row">
@if (!empty($conteo))
    <form id="form1" class="" role="form" method="POST" action="/inventario/controles/conteo/store/{{$conteo->id}}" enctype="multipart/form-data">
@else
    <form id="form1" class="" role="form" method="POST" action="/inventario/controles/conteo/store/" enctype="multipart/form-data">
@endif
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>{{$tituloform}}</strong></h3>
                </div>
                <div class="box-body">
                    {{ csrf_field() }}

                    @if (!empty($conteo))
                        <input type = "hidden" name = "id" value = "{{$conteo->id}}">
                        <input type = "hidden" name = "id_ctrolstk" value = "{{$conteo->id_ctrolstk}}">
                    @else
                        <input type = "hidden" name = "id">
                        <input type = "hidden" name = "id_ctrolstk" value = "{{$ctrol->id}}">
                    @endif

                    @php
                        if (!empty($conteo->id_item)){
                            $disableditem = 'disabled';
                        }else{
                            $disableditem = ( !empty($ctrol->id_item) )? 'disabled':'';
                        }
                    @endphp
                    <div class="form-group col-md-9 col-md-offset-right-6">
                        <label for="id_item" class="control-label">Producto</label><div id="loading" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>
                        <select id="id_item" name="id_item" class="form-control text-uppercase" {{$disableditem}}>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="clearfix visible-lg-block visible-md-block"></div>

                    <div class="form-group col-md-4">
                        <label for="id_sector" class="control-label">Sector</label>
                        <select id="id_sector" name="id_sector" class="form-control text-uppercase" {{$disabledsec}}>
                            @if (!empty($conteo->id_sector))
                                <!-- El conteo es epecificamente sobre ese sector -->
                                @php
                                    $sector =   DB::table('inv_sectores')
                                                    ->where('inv_sectores.id', $conteo->id_sector)
                                                    ->select('inv_sectores.*')
                                                ->first();
                                @endphp
                                <option value="{{$sector->id}}" selected>{{$sector->descripcion}}</option>
                            @else
                                @if ((empty($conteo->id)) and (!empty($ctrol->id_sector)))
                                    <!-- Va a crear un conteo nuevo basandose en los param del control-->
                                    @php
                                        $sector =   DB::table('inv_sectores')
                                                        ->where('inv_sectores.id', $ctrol->id_sector)
                                                        ->select('inv_sectores.*')
                                                    ->first();
                                    @endphp
                                    <option value="{{$sector->id}}" selected>{{$sector->descripcion}}</option>
                                @else
                                    <!-- El conteo no epecifica sector -->
                                    <option value=""></option>
                                    @foreach(DB::table('inv_sectores')
                                            ->where('inv_sectores.activo', 1)
                                            ->select('inv_sectores.*')
                                            ->get() as $sector)
                                        <option value="{{$sector->id}}">{{$sector->descripcion}}</option>
                                    @endforeach
                                @endif
                            @endif
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_ubicacion" class="control-label">Ubicacion</label>
                        <select id="id_ubicacion" name="id_ubicacion" class="form-control text-uppercase" {{$disabledubi}}>
                            @if (!empty($conteo->id_ubicacion))
                                <!-- El conteo es epecificamente sobre esa ubicacion -->
                                @php
                                    $ubicacion = DB::table('inv_ubicaciones')
                                                    ->where('inv_ubicaciones.id', $conteo->id_ubicacion)
                                                    ->select('inv_ubicaciones.*')
                                                ->first();
                                @endphp
                                <option value="{{$ubicacion->id}}" selected>{{$ubicacion->descripcion}}</option>
                            @else
                                @if ((empty($conteo->id)) and (!empty($ctrol->id_ubicacion)))
                                    <!-- Va a crear un conteo nuevo basandose en los param del control-->
                                    @php
                                        $ubicacion = DB::table('inv_ubicaciones')
                                                        ->where('inv_ubicaciones.id', $ctrol->id_ubicacion)
                                                        ->select('inv_ubicaciones.*')
                                                    ->first();
                                    @endphp
                                    <option value="{{$ubicacion->id}}" selected>{{$ubicacion->descripcion}}</option>
                                @else
                                    <!-- El conteo no epecifica ubicacion -->
                                    <option value=""></option>
                                    @if (!empty($ctrol->id_sector)))
                                        @foreach(DB::table('inv_ubicaciones')
                                                ->where('inv_ubicaciones.activo', 1)
                                                ->join('inv_sectores', function ($join) use($ctrol){
                                                    $join->on('inv_sectores.id', '=', 'inv_ubicaciones.id_sector')
                                                    ->where('inv_sectores.id', '=', $ctrol->id_sector);
                                                })
                                                ->select('inv_ubicaciones.*')
                                            ->get() as $ubicacion)
                                            <option value="{{$ubicacion->id}}">{{$ubicacion->descripcion}}</option>
                                        @endforeach
                                    @else
                                        @foreach(DB::table('inv_ubicaciones')
                                                ->where('inv_ubicaciones.activo', 1)
                                                ->select('inv_ubicaciones.*')
                                                ->get() as $ubicacion)
                                            <option value="{{$ubicacion->id}}">{{$ubicacion->descripcion}}</option>
                                        @endforeach
                                    @endif
                                @endif
                            @endif
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="id_posicion" class="control-label">Posicion</label>
                        <select id="id_posicion" name="id_posicion" class="form-control text-uppercase" {{$disabledpos}}>
                            @if (!empty($conteo->id_posicion))
                                <!-- El conteo es epecificamente sobre esa posicion -->
                                @php
                                    $posicion = DB::table('inv_posiciones')
                                                    ->where('inv_posiciones.id', $conteo->id_posicion)
                                                    ->select('inv_posiciones.*')
                                                ->first();
                                @endphp
                                <option value="{{$posicion->id}}" selected>{{$posicion->descripcion}}</option>
                            @else
                                @if ((empty($conteo->id)) and (!empty($ctrol->id_posicion)))
                                    <!-- Va a crear un conteo nuevo basandose en los param del control-->
                                    @php
                                        $posicion = DB::table('inv_posiciones')
                                                        ->where('inv_posiciones.id', $ctrol->id_posicion)
                                                        ->select('inv_posiciones.*')
                                                    ->first();
                                    @endphp
                                    <option value="{{$posicion->id}}" selected>{{$posicion->descripcion}}</option>
                                @else
                                    <!-- El conteo no epecifica posicion -->
                                    <option value=""></option>
                                    @if (!empty($ctrol->id_ubicacion)))
                                        @foreach(DB::table('inv_posiciones')
                                                ->where('inv_posiciones.activo', 1)
                                                ->Join('inv_ubicaciones', function ($join) use($ctrol){
                                                    $join->on('inv_ubicaciones.id', '=', 'inv_posiciones.id_ubicacion')
                                                    ->where('inv_ubicaciones.id', '=', $ctrol->id_ubicacion);
                                                })
                                                ->select('inv_posiciones.*')
                                                ->get() as $posicion)
                                            <option value="{{$posicion->id}}">{{$posicion->descripcion}}</option>
                                        @endforeach
                                    @elseif (!empty($ctrol->id_sector)))
                                        @foreach(DB::table('inv_posiciones')
                                                    ->where('inv_posiciones.activo', 1)
                                                    ->Join('inv_ubicaciones','inv_ubicaciones.id', '=', 'inv_posiciones.id_ubicacion')
                                                    ->join('inv_sectores', function ($join) use($ctrol){
                                                        $join->on('inv_sectores.id', '=', 'inv_ubicaciones.id_sector')
                                                        ->where('inv_sectores.id', '=', $ctrol->id_sector);
                                                    })
                                                    ->select('inv_posiciones.*')
                                                    ->get() as $posicion)
                                                <option value="{{$posicion->id}}">{{$posicion->descripcion}}</option>
                                        @endforeach
                                    @else
                                        @foreach(DB::table('inv_posiciones')
                                                    ->where('inv_posiciones.activo', 1)
                                                    ->select('inv_posiciones.*')
                                                ->get() as $posicion)
                                            <option value="{{$posicion->id}}">{{$posicion->descripcion}}</option>
                                        @endforeach
                                    @endif
                                @endif
                            @endif
                        </select>
                    </div>


                    <div class="form-group col-md-3">
                        <label for="cantidad_fisica" class="control-label">Cantidad</label>
                        <input id="cantidad_fisica" type="number" step="any" class="form-control input-sm text-uppercase" name="cantidad_fisica">
                    </div>
                </div>
                <div class="box-footer ptb-10">
                    <div class="pull-right pr-5">
                        @if ($edit==1)
                            <a class="btn btn-primary" href="/inventario/controles/{{$ctrol->id}}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                            <button type="submit" name="action" value="save" class="btn btn-primary">
                                <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            </button>
                        @else
                            <a class="btn btn-primary" href="/inventario/controles/{{$ctrol->id}}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@include('utils.newprodmodal')
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<style>
    #loading{
        display: inline !important;
    }
</style>
@stop

@section('js')
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    $(document.body).addClass('sidebar-collapse');
    $(function () {
        $('[data-toggle="popover"]').popover()
    });

    $("#id_item").select2({placeholder: "Buscar/Seleccionar Producto",allowClear: true, minimumInputLength: 3});
    $("#id_sector").select2();
    $("#id_ubicacion").select2();
    $("#id_posicion").select2();

    $("#id_ubicacion").change(function(){
        if (($("#id_ubicacion").val() != "") && ($("#id_ubicacion").val() != null)) {
            if (!$("#id_ubicacion").prop("disabled")){
                $.ajax('/datos/getposiciones_ubicacion/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_posicion').empty();
                        if (data.length==1){
                            $("#id_posicion").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_posicion").prop("disabled", true);
                            $("#id_posicion").val(data[0].id).trigger('change');
                        }else{
                            $("#id_posicion").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_posicion").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_posicion").prop("disabled", false);
                        }
                    }
                });
            }
        }
    });

    $("#id_posicion").change(function(){
        if (($("#id_posicion").val() != "") && ($("#id_posicion").val() != null)) {
            if (!$("#id_posicion").prop("disabled")){
                $.ajax('/datos/getubicaciones_posicion/'+$(this).val(), {
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data) {
                        $('#id_ubicacion').empty();
                        if (data.length==1){
                            $("#id_ubicacion").append(
                                '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                            );
                            $("#id_ubicacion").prop("disabled", true);
                            $("#id_ubicacion").val(data[0].id).trigger('change');
                        }else{
                            $("#id_ubicacion").append(
                                '<option value=""></option>'
                            );
                            $.each(data, function(index) {
                                $("#id_ubicacion").append(
                                    '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                                );
                            });
                            $("#id_ubicacion").prop("disabled", false);
                        }
                    }
                });
            }
        }
    });

    $('#form1').submit(function (event) {
        if ((($("#id_item").val() == "") || ($("#id_item").val() == null)) ||
            (($("#id_sector").val() == "") || ($("#id_sector").val() == null)) ||
            (($("#cantidad_fisica").val() == "") || ($("#cantidad_fisica").val() == null)) ){
            iziToast.warning({
                title: 'Atencion!',
                message: 'Producto, Sector y Cantidad son requeridos.',
                close: false,
                displayMode: 'once',
                position:'center'
            });
            event.preventDefault();
        }else{
            $("#id_item").prop("disabled", false);
            $("#id_sector").prop("disabled", false);
            $("#id_ubicacion").prop("disabled", false);
            $("#id_posicion").prop("disabled", false);
            $("#cantidad_fisica").prop("disabled", false);
        }
    });

    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }


    $(window).on('load', function(event){
        $("#loading").button('loading');
        $("#loading").removeClass('hidden');
        idcontrol =  {!! $ctrol->id !!};
        @if (!empty($conteo->id))
            idconteo={!! $conteo->id !!};
        @else
            idconteo=null;
        @endif
        $.ajax('/datos/ctrolstk/getproductos/'+idcontrol+'/'+idconteo, {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                if (data.length==1){
                    $("#id_item").append(
                        '<option value="' + data[0].id + '">' + data[0].producto + '</option>'
                    );
                    $("#id_item").prop("disabled", true);
                    $("#id_item").val(data[0].id).trigger('change');
                }else{
                    $.each(data, function(index) {
                        $("#id_item").append(
                            '<option value="' + data[index].id + '">' + data[index].producto + '</option>'
                        );
                    });
                    $("#id_item").prop("disabled", false);
                    $("#id_item").val('').trigger('change');
                }
                $("#loading").button('reset');
                $("#loading").addClass('hidden');
            }
        });
    });
</script>
@include('utils.newprodmodaljs')
@include('utils.statusnotification')
@stop