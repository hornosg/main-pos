@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
@include('layouts.inventarionavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <a href="/inventario/controles/create" class="btn btn-primary"  title="Nuevo Controles de Stock">
                                    <i class="fa fa-btn fa-plus text-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row bodycontainer scrollable">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-detail-view='true'
                               data-unique-id='id'>
                            <thead class='thead-inverse'>
                            <th data-checkbox='true' class="hidden"></th>
                            <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                            <th data-field='creado' class='text-uppercase'>Creado</th>
                            <th data-field='producto' class='hidden-xs text-uppercase'>Producto</th>
                            <th data-field='marca'  class='hidden-xs text-uppercase  text-bold'>Marca</th>
                            <th data-field='rubro' class='hidden-xs text-uppercase  text-bold'>Rubro</th>
                            <th data-field='subrubro'  class='hidden-xs text-uppercase  text-bold'>Subsubro</th>
                            <th data-field='sector' class='hidden-xs text-uppercase  text-bold'>Sector</th>
                            <th data-field='ubicacion'  class='hidden-xs text-uppercase  text-bold'>Ubicacion</th>
                            <th data-field='posicion' class='hidden-xs text-uppercase  text-bold'>Posicion</th>
                            <th data-field='estado' class='text-uppercase text-bold' data-formatter='FunctionsStatusFormatter'>Estado</th>
                            <th data-align='center' class="col-md-2" data-formatter='FunctionsFormatter'></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer ptb-10">
                <div class="pull-right pr-5">
                    <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<!--<script src="/js/bootstrap-table-filter.js"></script>-->
<!--<script src="/js/bootstrap-table-filter-control.js"></script>-->
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script language='JavaScript' type='text/javascript'>
    var datos = <?php echo json_encode($items); ?>;
    $(document.body).addClass('sidebar-collapse');
    var $table = $('#table1');
    $table.bootstrapTable({
        data: datos,
        exportDataType:'all',
        exportOptions:{fileName: 'items'}
    });

    function FunctionsStatusFormatter(value, row, index) {
        if(row.estado=='SIN INICIAR'){
            return '<span class="badge bg-yellow">S/INI</span>';
        }
        if(row.estado=='EN PROCESO'){
            return '<span class="badge bg-aqua">E/PRO</span>';
        }
        if(row.estado=='CON DIFERENCIAS'){
            return '<span class="badge bg-red">C/DIF</span>';
        }
        if(row.estado=='FINALIZADO'){
            return '<span class="badge bg-green-active">FIN</span>';
        }
        if(row.estado=='RESUELTO'){
            return '<span class="badge bg-green-active">RTO</span>';
        }
    };

    function FunctionsFormatter(value, row, index) {
        if ((row.estado=='RESUELTO') || (row.estado=='FINALIZADO')){
            return;
        }else{
            return "<a class='btn-outline-danger' href='#' onclick='Borrar("+row.id+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
                "<a class='btn-outline-success' href='/inventario/controles/admin/"+row.id+"'><i class='fa fa-fw fa-check-square'></i></a>" +
                "<a class='btn-outline-primary' href='/inventario/controles/"+row.id+"'><i class='fa fa-fw fa-search'></i></a>" ;
        }
    };

    function Borrar(idrow){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $.ajax({
                        type: "GET",
                        url:'/inventario/controles/delete/'+idrow,
                        data: idrow,
                        success: function(data) {
                            $('#table1').bootstrapTable('removeByUniqueId', idrow);
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    $table.on('expand-row.bs.table', function (e, index, row, $detail) {
        $idtabla2="tabla"+row.id;
        html='<div class="box"><table id='+$idtabla2+' class="table table-sm table-bordered table-hover table-striped table-condensed table-responsive" ' +
            'data-search="true" data-strict-search="false" data-multiple-search="true" data-pagination="true">' +
            '<thead class="thead-inverse-gray"><tr>' +
            '<th data-field="id" class="hidden"></th>' +
            '<th data-field="producto">PRODUCTO</th>' +
            '<th data-field="sector">SECTOR</th>' +
            '<th data-field="ubicacion">UBICACION</th>' +
            '<th data-field="posicion">POSICION</th>' +
            '</tr></thead>' +
            '</table></div>';
        $detail.html(html);
        $('#'+$idtabla2).bootstrapTable();
        $('#'+$idtabla2).bootstrapTable('showLoading');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'/inventario/controles/detalle/'+row.id,
            data: row.id,
            success: function(datosdetalle) {
                $('#'+$idtabla2).bootstrapTable('append', datosdetalle);
                $('#'+$idtabla2).bootstrapTable('hideLoading');
            }
        });
    });
</script>
@include('utils.statusnotification')
@stop
