@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
@include('layouts.inventarionavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header with-border">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <a href="/inventario/controles/{{$idctrol}}/conteo/create" class="btn btn-primary"  title="Agregar Conteo">
                                    <i class="fa fa-btn fa-plus text-white"></i>
                                </a>
                                <button id="btn-ajustar" class="btn btn-danger"  title="Ajustar Seleccionados">
                                    AJ
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="hidden-lg hidden-md">{{$titulo}}</h3>
            </div>
            <div id="box-body">
                <div class="col-xs-12">
                    <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                           data-toggle='table'
                           data-toolbar="#toolbar"
                           data-search='true'
                           data-strict-search="false"
                           data-multiple-search='true'
                           data-pagination='true'
                           data-page-size='20'
                           data-sort-name='id'
                           data-sort-order='desc'
                           data-unique-id='id'>
                        <thead class='thead-inverse'>
                        <th data-checkbox='true'></th>
                        <th data-field='id' data-align='right' class='hidden'>Id</th>
                        <th name="creado" data-field='creado' class='text-uppercase text-bold hidden-sm hidden-xs'>Creado</th>
                        <th data-field='producto' class='text-uppercase'>Producto</th>
                        <th data-field='sector' class='text-uppercase text-bold'>Sector</th>
                        <th data-field='ubicacion'  class='text-uppercase text-bold'>Ubicacion</th>
                        <th data-field='posicion' class='text-uppercase text-bold'>Posicion</th>
                        <th data-field='fisico' data-align='right' class='text-uppercase text-bold hidden-sm hidden-xs'>Stk Fisico</th>
                        <th data-field='stock' data-align='right' class='text-uppercase text-bold hidden-sm hidden-xs'>Stk Sistema</th>
                        <th data-field='status' class='text-uppercase text-bold hidden-sm hidden-xs' data-formatter='FunctionsStatusFormatter'>Estado</th>
                        <th data-align='center' data-formatter='FunctionsFormatter'></th>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="box-footer ptb-10">
                <div class="pull-right pr-5">
                    <a class="btn btn-primary" href="/inventario/controles/index"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<style>
    @media only screen and (max-width: 600px) {
        #table1 > tbody > tr > td > div > div:nth-child(1),
        #table1 > tbody > tr > td > div > div:nth-child(2),
        #table1 > tbody > tr > td > div > div:nth-child(7),
        #table1 > tbody > tr > td > div > div:nth-child(8),
        #table1 > tbody > tr > td > div > div:nth-child(9){
            display: none;
        }
    }
</style>
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<!--<script src="/js/bootstrap-table-filter.js"></script>-->
<!--<script src="/js/bootstrap-table-filter-control.js"></script>-->
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script language='JavaScript' type='text/javascript'>
    var datos = <?php echo json_encode($items); ?>;
    $(document.body).addClass('sidebar-collapse');
    var $table = $('#table1');
    $table.bootstrapTable({
        data: datos,
        exportDataType:'all',
        exportOptions:{fileName: 'items'}
    });

    function FunctionsStatusFormatter(value, row, index) {
        if(row.status=='SIN INICIAR'){
            return '<span class="badge bg-yellow">SIN INICIAR</span>';
        }
        if(row.status=='CON DIFERENCIAS'){
            return '<span class="badge bg-red">CON DIFERENCIAS</span>';
        }
        if(row.status=='DIFERENCIA AJUSTADA'){
            return '<span class="badge bg-green-active">DIF. AJUSTADA</span>';
        }
        if(row.status=='OK'){
            return '<span class="badge bg-green-active">OK</span>';
        }
    };

    function FunctionsFormatter(value, row, index) {
        if(row.status=='SIN INICIAR'){
                if (row.producto != ''){
                    return "<a class='btn btn-block btn-social btn-bitbucket' href='/inventario/controles/conteo/"+row.id+"'><i class='fa fa-fw fa-sliders'></i>Contar</a>" ;
                }else{
                    return "<a class='btn btn-block btn-social btn-bitbucket' href='/inventario/controles/conteo/"+row.id+"'><i class='fa fa-fw fa-barcode'></i>Scannear y Contar</a>" ;
                }
        }
        if(row.status=='CON DIFERENCIAS'){
            return "<a class='btn-outline-primary text-bold' href='#' onclick='Ajuste("+row.id+","+index+")'>AJ</a>" ;
        }

        if((row.status=='OK') || (row.status=='DIF. AJUSTADA')){
            return "<p><i class='fa fa-lg fa-thumbs-o-up'></i></p>" ;
        }
    };

    function Ajuste(idrow,index){
        iziToast.info({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'info',
            zindex: 999,
            title: 'Ajustar Diferencia!',
            message: '',
            position: 'center',
            buttons: [
                ['<button><b>OK</b></button>', function (instance, toast) {
                    $.ajax({
                        type: "GET",
                        url:'/inventario/controles/ajustar/'+idrow,
                        success: function(data) {
                            $('#table1').bootstrapTable('updateCell', {index:index,field:'status',value:'DIFERENCIA AJUSTADA'});
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true]
            ]
        });
    }

    $('#btn-ajustar').click(function (e) {
        e.preventDefault();
        var choice;
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: '¿Está seguro de Ajustar todos los items seleccionados?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    var ids = $.map($('#table1').bootstrapTable('getAllSelections'), function (row) {
                        return row.id;
                    });
                    $.ajax('/inventario/controles/ajustarselected', {
                        type: 'POST',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {datos:ids},
                        success: function(data){
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: 'Items, Ajustados. Refrescar para ver el cambio'
                            });
                        },
                        error:function(data) {
                            console.log(data);
                            iziToast.error({
                                timeout: 2000,
                                position:'center',
                                title: 'Error:',
                                message: data.responseJSON.message
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }]
            ]
        });
    });

    $(window).on('load', function(event){
        var windowSize = $(window).width();
        if(windowSize < 450){
            $('#table1').bootstrapTable('toggleView');
            $('#table1').bootstrapTable('hideColumn', 'created_at');
            $('#table1').bootstrapTable('hideColumn', 'fisico');
            $('#table1').bootstrapTable('hideColumn', 'stock');
            $('#table1').bootstrapTable('hideColumn', 'status');

        }
    });
</script>
@include('utils.statusnotification')
@stop
