<script language='JavaScript' type='text/javascript'>
    $.fn.select2.defaults.set('matcher', function(params, data) {
        if ($.trim(params.term) === '') {
            return data;
        }

        if (typeof data.text === 'undefined') {
            return null;
        }

        var words = params.term.toUpperCase().split(" ");

        for (var i = 0; i < words.length; i++) {
            if (data.text.toUpperCase().indexOf(words[i]) < 0) {
                return null;
            }
        }

        return data;
    });
</script>