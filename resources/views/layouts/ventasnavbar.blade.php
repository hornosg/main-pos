<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/ventas">Ventas & Cobranzas</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ $itemnav == 'rem' ? 'active text-bold' : ''}}"><a href="/cptes/rem">Remitos<span class="sr-only">(current)</span></a></li>
                {{--<li class="{{ $itemnav == 'fc' ? 'active text-bold' : ''}}"><a href="/cptes/fc">Facturas<span class="sr-only">(current)</span></a></li>--}}
                <li class="{{ $itemnav == 'rbo' ? 'active text-bold' : ''}}"><a href="/cptes/rbo">Recibos</a></li>
                <li class="{{ $itemnav == 'nc' ? 'active text-bold' : ''}}"><a href="/cptes/nc">N.Credito</a></li>
                <li class="{{ $itemnav == 'nd' ? 'active text-bold' : ''}}"><a href="/cptes/nd">N.Debito</a></li>
                @if (Auth::user()->id_rol==1)
                    <li class="{{ $itemnav == 'list' ? 'active text-bold' : ''}}"><a href="/ventas/sabana">Listado Sabana</a></li>
                @endif
                <ol class="breadcrumb hidden-sm">
                    @if (empty($itemnav))
                        <form class="form-inline">
                            <div class="form-group pull-right" style="width: 150px;margin-top: -5px;margin-right: 5px;">
                                <select id="id_sucursal" name="id_sucursal" class="form-control text-uppercase">
                                <option value=""></option>
                                @foreach(DB::table('sucursales')->where('activo', 1)->get() as $sucursal)
                                    @if ($idsucursal==$sucursal->id)
                                        <option value="{{$sucursal->id}}" selected>{{$sucursal->descripcion}}</option>
                                    @else
                                        <option value="{{$sucursal->id}}">{{$sucursal->descripcion}}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group pull-right" style="width: 150px;margin-top: -5px;">
                                <select id="meses" name="meses" class="form-control text-uppercase">
                                    @if (!empty($mes))
                                        <option value="0" selected>MES ACTUAL</option>
                                    @else
                                        <option value="0">MES ACTUAL</option>
                                    @endif
                                    @if ($mes==1)
                                        <option value="1" selected>1 MES ATRAS</option>
                                    @else
                                        <option value="1">1 MES ATRAS</option>
                                    @endif
                                    @if ($mes==2)
                                        <option value="2" selected>2 MESES ATRAS</option>
                                    @else
                                        <option value="2">2 MESES ATRAS</option>
                                    @endif
                                    @if ($mes==3)
                                        <option value="3" selected>3 MESES ATRAS</option>
                                    @else
                                        <option value="3">3 MESES ATRAS</option>
                                    @endif
                                    @if ($mes==4)
                                        <option value="4" selected>4 MESES ATRAS</option>
                                    @else
                                        <option value="4">4 MESES ATRAS</option>
                                    @endif
                                    @if ($mes==5)
                                        <option value="5" selected>5 MESES ATRAS</option>
                                    @else
                                        <option value="5">5 MESES ATRAS</option>
                                    @endif
                                    @if ($mes==6)
                                        <option value="6" selected>6 MESES ATRAS</option>
                                    @else
                                        <option value="6">6 MESES ATRAS</option>
                                    @endif
                                    @if ($mes==7)
                                        <option value="7" selected>7 MESES ATRAS</option>
                                    @else
                                        <option value="7">7 MESES ATRAS</option>
                                    @endif
                                    @if ($mes==8)
                                        <option value="8" selected>8 MESES ATRAS</option>
                                    @else
                                        <option value="8">8 MESES ATRAS</option>
                                    @endif
                                    @if ($mes==9)
                                        <option value="9" selected>9 MESES ATRAS</option>
                                    @else
                                        <option value="9">9 MESES ATRAS</option>
                                    @endif
                                    @if ($mes==10)
                                        <option value="10" selected>10 MESES ATRAS</option>
                                    @else
                                        <option value="10">10 MESES ATRAS</option>
                                    @endif
                                    @if ($mes==11)
                                        <option value="11" selected>11 MESES ATRAS</option>
                                    @else
                                        <option value="11">11 MESES ATRAS</option>
                                    @endif
                                    @if ($mes==12)
                                        <option value="12" selected>12 MESES ATRAS</option>
                                    @else
                                        <option value="12">12 MESES ATRAS</option>
                                    @endif
                                </select>
                            </div>
                        </form>
                    @else
                        <li><a href="/ventas"><i class="fa fa-dashboard"></i> Ventas & Cobranzas </a></li>
                        <li class="active">{{$titulo}}</li>
                        @if (!empty($accion))
                            <li class="active">{{$accion}}</li>
                        @endif
                    @endif
                </ol>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>