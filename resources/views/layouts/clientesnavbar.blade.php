<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Clientes</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ $itemnav == 'clientes' ? 'active text-bold' : ''}}"><a href="/clientes">Clientes <span class="sr-only">(current)</span></a></li>
                <li class="{{ $itemnav == 'ctacte' ? 'active text-bold' : ''}}"><a href="/clientes/cuentacorriente">Cuenta Corriente <span class="sr-only">(current)</span></a></li>
                <li class="{{ $itemnav == 'ctactevda' ? 'active text-bold' : ''}}"><a href="/clientes/vencidos">CtaCte Vencida</a></li>
                @if (env('MP_COMISION'))
                <li class="{{ $itemnav == 'comi' ? 'active text-bold' : ''}}"><a href="/cptes/comi">Comisiones</a></li>
                @endif
                <li class="{{ $itemnav == 'ranking' ? 'active text-bold' : ''}}"><a href="#">Ranking</a></li>
                <ol class="breadcrumb hidden-sm">
                    <li><a href="#"><i class="fa fa-list-ul"></i> Clientes </a></li>
                    <li class="active">{{$titulo}}</li>
                    @if (!empty($accion))
                        <li class="active">{{$accion}}</li>
                    @endif
                </ol>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>