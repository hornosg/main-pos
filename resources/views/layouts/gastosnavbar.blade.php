<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/gastos">Gastos</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ $itemnav == 'gas' ? 'active text-bold' : ''}}"><a href="/comprobantes">Comprobantes<span class="sr-only">(current)</span></a></li>
                <li class="{{ $itemnav == 'serv' ? 'active text-bold' : ''}}"><a href="/servicios">Servicios</a></li>
                <li class="{{ $itemnav == 'insu' ? 'active text-bold' : ''}}"><a href="/insumos">Insumos<span class="sr-only">(current)</span></a></li>
                <li class="{{ $itemnav == 'bien' ? 'active text-bold' : ''}}"><a href="/bienes">Bienes</a></li>
                <li class="{{ $itemnav == 'inve' ? 'active text-bold' : ''}}"><a href="/inversiones">Inversiones</a></li>
                <ol class="breadcrumb hidden-sm">
                    <li><a href="#"><i class="fa fa-shopping-cart"></i> Gastos </a></li>
                    <li class="active">{{$titulo}}</li>
                    @if (!empty($accion))
                        <li class="active">{{$accion}}</li>
                    @endif
                </ol>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>