<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @if (!empty($titulo2))
            <a class="navbar-brand" href="#">{{$titulo}} - {{$titulo2}}</a>
            @else
            <a class="navbar-brand" href="#">{{$titulo}}</a>
            @endif
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ $itemnav == 'users' ? 'active text-bold' : ''}}"><a href="/usuarios">Usuarios <span class="sr-only">(current)</span></a></li>
                <li class="{{ $itemnav == 'list' ? 'active text-bold' : ''}}"><a href="/usuarios/resumenmensual">Actividad Mensual<span class="sr-only">(current)</span></a></li>
                <ol class="breadcrumb hidden-sm">
                    <li><a href="#"><i class="fa {{$icon}}"></i> Usuarios </a></li>
                    <li class="active">{{$titulo}}</li>
                    @if (!empty($accion))
                        <li class="active">{{$accion}}</li>
                    @endif
                </ol>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>