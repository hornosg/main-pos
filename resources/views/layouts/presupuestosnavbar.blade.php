<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="/presupuestos">Oportunidades de Ventas</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ $itemnav == 'pre' ? 'active text-bold' : ''}}"><a href="/cptes/pre">Presupuestos<span class="sr-only">(current)</span></a></li>
                <ol class="breadcrumb hidden-sm">
                    <li><a href="/presupuestos"><i class="fa fa-magnet"></i> Oportunidades de Ventas </a></li>
                    <li class="active">{{$titulo}}</li>
                    @if (!empty($accion))
                    <li class="active">{{$accion}}</li>
                    @endif
                </ol>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>