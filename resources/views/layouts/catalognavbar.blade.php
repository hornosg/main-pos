<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @if (!empty($titulo2))
            <a class="navbar-brand" href="#">{{$titulo}} - {{$titulo2}}</a>
            @else
            <a class="navbar-brand" href="#">{{$titulo}}</a>
            @endif
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ $itemnav == 'productos' ? 'active text-bold' : ''}}"><a href="/productos">Productos <span class="sr-only">(current)</span></a></li>
                @if (Auth::user()->id_rol<>5)
                <li class="{{ $itemnav == 'marcas' ? 'active text-bold' : ''}}"><a href="/marcas">Marcas</a></li>
                <li class="{{ $itemnav == 'rubros' ? 'active text-bold' : ''}}"><a href="/rubros">Rubros</a></li>
                <li class="{{ $itemnav == 'subr' ? 'active text-bold' : ''}}"><a href="/subrubros">Subrubros</a></li>
                <li class="{{ $itemnav == 'otro' ? 'active dropdown' : 'dropdown'}}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Mas características<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/lineas">Lineas de productos</a></li>
                        <li><a href="/materiales">Materiales</a></li>
                        <li><a href="/origenes">Origenes de Fabricacion</a></li>
                        <li><a href="/productos/eans">Codigos de Barra</a></li>
                    </ul>
                </li>
                <li class="{{ $itemnav == 'list' ? 'active dropdown' : 'dropdown'}}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Listados<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/masvendidos">Productos mas Vendidos</a></li>
                    </ul>
                </li>
                @endif
                <ol class="breadcrumb hidden-sm">
                    <li><a href="#"><i class="fa fa-list-ul"></i> Catalogo Productos</a></li>
                    <li class="active">{{$titulo}}</li>
                    @if (!empty($accion))
                        <li class="active">{{$accion}}</li>
                    @endif
                </ol>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>