<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Inventario</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ $itemnav == 'confdep' ? 'active text-bold' : ''}}"><a href="/inventario/configuracion/deposito">Configuracion<span class="sr-only">(current)</span></a></li>
                <li class="{{ $itemnav == 'movstk' ? 'active text-bold' : ''}}"><a href="/inventario/movimientos/index">Movimientos<span class="sr-only">(current)</span></a></li>
                <li class="{{ $itemnav == 'ctrolstk' ? 'active text-bold' : ''}}"><a href="/inventario/controles/index">Controles</a></li>
                <li class="{{ $itemnav == 'liststk' ? 'active text-bold' : ''}}"><a href="/inventario/stock/index">Stock</a></li>
                <li class="{{ $itemnav == 'resumen' ? 'active text-bold' : ''}}"><a href="/inventario">Resumen</a></li>
                <ol class="breadcrumb hidden-sm">
                    <li><a href="/inventario"><i class="fa fa-cubes"></i> Inventario </a></li>
                    @if (!empty($titulo))
                        <li class="active">{{$titulo}}</li>
                    @endif
                    @if (!empty($accion))
                        <li class="active">{{$accion}}</li>
                    @endif
                </ol>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>