<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/caja">Caja</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ $itemnav == 'cajaactual' ? 'active text-bold' : ''}}"><a href="/caja">Efectivo & Cheques<span class="sr-only">(current)</span></a></li>
                <li class="{{ $itemnav == 'cajatotal' ? 'active text-bold' : ''}}"><a href="/caja/total">Todos los Comprobantes<span class="sr-only">(current)</span></a></li>
                @if (Auth::user()->id_rol==1)
                    <li class="{{ $itemnav == 'cajaant' ? 'active text-bold' : ''}}"><a href="/caja/cajasanteriores">Cajas Anteriores<span class="sr-only">(current)</span></a></li>
                    @if (env('MP_CHEQUES'))
                    <li class="{{ $itemnav == 'cheques' ? 'active text-bold' : ''}}"><a href="/cheques">Cartera de Cheques<span class="sr-only">(current)</span></a></li>
                    @endif
                    <li class="{{ $itemnav == 'sabana' ? 'active text-bold' : ''}}"><a href="/caja/sabana">Listado Sabana<span class="sr-only">(current)</span></a></li>
                @endif
                <ol class="breadcrumb hidden-sm">
                    <li><a href="#"><i class="fa fa-inbox"></i> Caja </a></li>
                    <li class="active">{{$titulo}}</li>
                </ol>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>