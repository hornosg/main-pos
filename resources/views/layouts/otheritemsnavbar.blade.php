<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @if (!empty($titulo2))
                <a class="navbar-brand" href="#">{{$titulo}} - {{$titulo2}}</a>
            @else
                <a class="navbar-brand" href="#">{{$titulo}}</a>
            @endif
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <ol class="breadcrumb">
                    <li class="active text-capitalize"><i class="fa fa-list-ul"></i>  {{$urlkey}}</li>
                    @if (!empty($titulo2))
                        <li class="active">{{$titulo2}}</li>
                    @endif
                </ol>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>