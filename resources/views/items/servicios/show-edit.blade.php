@extends('adminlte::page')

@section('title', $titulo)

@php
if ($edit==0){
    $editable='readonly';
    $visible='d-none';
    $disabled='disabled';
    $accion = 'Visualizar';
    $titulo2 = 'Visualizar';
}else{
    $editable=' ';
    $visible=' ';
    $disabled=' ';
    $accion = 'Editar';
    $titulo2 = 'Modificar';
}
$checked = $producto->activo == 1 ? 'checked':'';
@endphp

@section('content_header')
    @include('layouts.gastosnavbar',['titulo2' => $titulo2,'itemnav'=>'serv'])
@stop



@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <form id="form1" role="form" method="POST" action="/servicios/update/{{$producto->id}}">
                {{ csrf_field() }}
                <div class="box-body">

                    <input id="id_tipoitem" type="hidden" name="id_tipoitem" value="2">

                    <div class="row">
                            <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-4 col-md-4">
                                <label for="descripcion" class="control-label">Nombre</label>
                                <input id="descripcion" type="text" class="form-control input-sm text-uppercase" name="descripcion" value="{{ $producto->descripcion }}" {{$editable}}>
                            </div>
                            <div class="form-group{{ $errors->has('precioCosto') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                <label for="precioCosto" class="control-label">Precio Costo</label>
                                <input id="precioCosto" type="number" step="any" class="form-control input-sm text-uppercase" name="precioCosto" readonly value="{{ $producto->precioCosto }}" {{$editable}}>
                            </div>
                            <div class="form-group{{ $errors->has('activo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-1 col-md-1 pull-left">
                                <label for="activo" class="control-label">Activo</label>
                                <input id="activo" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked {{$disabled}} >
                            </div>
                            <div class="form-group{{ $errors->has('observaciones') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-10 col-md-10">
                                <label for="observaciones" class="control-label">Observaciones</label>
                                <input id="observaciones" type="text" class="form-control input-sm" name="observaciones" value="{{ $producto->observaciones }}" {{$editable}}>
                            </div>

                            <div class="form-group col-sm-6 col-md-4 col-md-3">
                                <label for="created_at" class="control-label">Fecha Alta</label>
                                <input id="created_at" type="text" class="form-control input-sm text-uppercase" name="created_at" readonly value="{{ $producto->created_at }}">
                            </div>

                            <div class="form-group col-sm-6 col-md-6 col-md-3">
                                <label for="updated_at" class="control-label">Fecha Ultima Edicion</label>
                                <input id="updated_at" type="text" class="form-control input-sm text-uppercase" name="updated_at" readonly value="{{ $producto->updated_at }}">
                            </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <a class="btn btn-default minw90" href="/{{$urlkey}}"><i class="fa fa-btn fa-arrow-left text-danger"></i> Salir</a>
                        @if ($edit==1)
                            <button type="submit" name="action" value="save" class="btn btn-default minw90">
                                <i class="fa fa-btn fa-floppy-o text-primary"></i> Guardar</a>
                            </button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">

<style>
    .pagination-info{
        display: none !important;
    }
</style>
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script type='text/javascript'>
    $('#activo').bootstrapToggle();

    $(window).on('load', function(event){
        document.getElementById("descripcion").focus();
        //-----------------------------------------------------------------------------------------------------
    });
</script>
@include('utils.statusnotification')
@stop
