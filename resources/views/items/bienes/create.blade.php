@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.gastosnavbar',['titulo2'=>'Nuevo','itemnav'=>'bien'])
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <form id="form1" role="form" method="POST" action="/{{$urlkey}}/store">
                {{ csrf_field() }}
                    <div class="box-body">
                        <input id="id_tipoitem" type="hidden" name="id_tipoitem" value="7">

                        <div class="row">
                            <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-8 col-md-7">
                                <label for="descripcion" class="control-label">Nombre</label>
                                <input id="descripcion" type="text" class="form-control input-sm text-uppercase" name="descripcion" value="{{ old('descripcion') }}" autofocus>
                            </div>
                            <div class="form-group{{ $errors->has('id_marca') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">
                                <label for="id_marca" class="control-label" for="marcas">Marca</label>
                                <select id="id_marca" name="id_marca"  type="button" class="form-control">
                                    <option value=""></option>
                                    @foreach($marcas as $marca)
                                        @if ( old('id_marca') == $marca->id )
                                            <option value="{{ $marca->id }}" selected>{{ $marca->descripcion }}</option>
                                        @else
                                            <option value="{{ $marca->id }}">{{ $marca->descripcion }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="clearfix">

                            </div>
                            <div class="form-group{{ $errors->has('precioLista') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                <label for="precioLista" class="control-label">Precio Lista</label>
                                <input id="precioLista" type="number" step="any" class="form-control input-sm text-uppercase" min="0" name="precioLista" value="{{ old('precioLista') }}">
                            </div>

                            <div class="form-group{{ $errors->has('bonifProv') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                <label for="bonifProv" class="control-label">Bonificacion</label>
                                <input id="bonifProv" type="number" step="any" class="form-control input-sm text-uppercase" name="bonifProv" min="1" max="100" value="{{ old('bonifProv') }}">
                            </div>

                            <div class="form-group{{ $errors->has('precioCosto') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                <label for="precioCosto" class="control-label">Precio Costo</label>
                                <input id="precioCosto" type="number" step="any" class="form-control input-sm text-uppercase" name="precioCosto" readonly value="{{ old('precioCosto') }}">
                            </div>
                            <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-1 col-md-1 pull-left">
                                <label for="activo" class="control-label">Activo</label>
                                <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>
                            </div>
                            <div class="form-group{{ $errors->has('observaciones') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-10 col-md-10">
                                <label for="observaciones" class="control-label">Observaciones</label>
                                <input id="observaciones" type="text" class="form-control input-sm" name="observaciones" value="{{ old('observaciones') }}">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer pb-10 no-bt">
                        <div class="pull-right">
                            <a class="btn btn-default" href="/{{$urlkey}}"><i class="fa fa-btn fa-arrow-left text-danger"></i> Cancel</a>
                            <button type="submit" name="action" value="savecontinue" class="btn btn-default">
                                <i class="fa fa-btn fa-floppy-o text-primary"></i> Guardar & <i class="fa fa-btn fa-repeat text-primary"></i> Continuar</a>
                            </button>
                            <button type="submit" name="action" value="save" class="btn btn-default">
                                <i class="fa fa-btn fa-floppy-o text-primary"></i> Guardar</a>
                            </button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script type='text/javascript'>
    $("#id_marca").select2({
        placeholder: "",
        allowClear: true
    });
    $('#activo').bootstrapToggle();

    $(document).ready(function() {
        var preciocosto = $('input[name=precioCosto]#precioCosto');
        var preciolista = $('input[name=precioLista]#precioLista');
        var bonificacion = $('input[name=bonifProv]#bonifProv');
        var pc = 0, pv = 0 ;
        preciolista.on('change',function (e) {
        if(bonificacion.val() > 0 )
          pc = $(this).val() - (($(this).val() * bonificacion.val())/100);
        else
          pc = $(this).val();
        preciocosto.val(pc);
        });
        bonificacion.on('change',function (e) {
        if($(this).val() > 0 )
          pc = preciolista.val() - ((preciolista.val() * $(this).val())/100);
        else
          pc = preciolista.val() ;
        preciocosto.val(pc);
        });

//        $(document.body).addClass('sidebar-collapse');
    });
</script>
@stop
