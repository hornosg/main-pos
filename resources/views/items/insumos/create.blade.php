@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.gastosnavbar',['titulo2'=>'Nuevo','itemnav'=>'insu'])
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <form id="form1" role="form" method="POST" action="/{{$urlkey}}/store">
                {{ csrf_field() }}
                <div class="box-body">
                    <input id="id_tipoitem" type="hidden" name="id_tipoitem" value="3"><!--value="7"-->

                    <div class="nav-tabs-custom">
                        <div class="nav nav-tabs">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tabInfoBasica">Información Básica</a></li>
                                <li class="hidden-sm hidden-xs"><a data-toggle="tab" href="#tabProveedores">Proveedores</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div id="tabInfoBasica" class="tab-pane active">
                                <div class="row">
                                    <div class="form-group {{ $errors->has('ean') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">
                                        <label for="ean" class="control-label">Codigo de Barras</label>
                                        <input id="ean" type="text" class="form-control input-sm text-uppercase" name="ean" value="{{ old('ean') }}">
                                    </div>
                                    <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-6">
                                        <label for="descripcion" class="control-label">Nombre</label>
                                        <input id="descripcion" type="text" class="form-control input-sm text-uppercase" name="descripcion" value="{{ old('descripcion') }}" autofocus>
                                    </div>
                                    <div class="form-group{{ $errors->has('id_marca') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">
                                        <label for="id_marca" class="control-label" for="marcas">Marca</label>
                                        <select id="id_marca" name="id_marca"  type="button" class="form-control">
                                            <option value=""></option>
                                            @foreach($marcas as $marca)
                                                @if ( old('id_marca') == $marca->id )
                                                    <option value="{{ $marca->id }}" selected>{{ $marca->descripcion }}</option>
                                                @else
                                                    <option value="{{ $marca->id }}">{{ $marca->descripcion }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="clearfix">
                                    <div class="form-group{{ $errors->has('contNeto') ? ' has-error' : '' }}  col-xs-12 col-sm-12 col-md-4 col-md-3">
                                        <label for="contNeto" class="control-label">Cont. Neto</label>
                                        <input id="contNeto" type="number" class="form-control input-sm text-uppercase" name="contNeto" value="{{ old('contNeto') }}">
                                    </div>
                                    <div class="form-group{{ $errors->has('id_unidadcontneto') ? ' has-error' : '' }}  col-xs-12 col-sm-12 col-md-6 col-md-3">
                                        <label for="id_unidadcontneto" class="control-label" for="unidadesm">Unidad M. Cont. Neto</label>
                                        <select id="id_unidadcontneto" name="id_unidadcontneto"  type="button" class="form-control">
                                            <option value=""></option>
                                            @foreach($unidadesm as $unidadContNeto)
                                            @if ( old('id_unidadcontneto') == $unidadContNeto->id )
                                            <option value="{{ $unidadContNeto->id }}" selected>{{ $unidadContNeto->descripcion }}</option>
                                            @else
                                            <option value="{{ $unidadContNeto->id }}">{{ $unidadContNeto->descripcion }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group {{ $errors->has('id_unidadventa') ? ' has-error' : '' }}  col-xs-12 col-sm-12 col-md-6 col-md-3">
                                        <label for="id_unidadventa" class="control-label" for="unidadesm">Unidad M.Venta</label>
                                        <select id="id_unidadventa" name="id_unidadventa"  type="button" class="form-control">
                                            <option value="1">U</option>
                                            @foreach($unidadesm as $unidadVenta)
                                            @if ( old('id_unidadventa') == $unidadVenta->id )
                                            <option value="{{ $unidadVenta->id }}" selected>{{ $unidadVenta->descripcion }}</option>
                                            @else
                                            <option value="{{ $unidadVenta->id }}">{{ $unidadVenta->descripcion }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group{{ $errors->has('stockMinimo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">
                                        <label for="stockMinimo" class="control-label">Stock Minimo</label>
                                        <input id="stockMinimo" type="number" step="any" class="form-control input-sm text-uppercase" name="stockMinimo">
                                    </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('precioLista') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                        <label for="precioLista" class="control-label">Precio Lista</label>
                                        <input id="precioLista" type="number" step="any" class="form-control input-sm text-uppercase" min="0" name="precioLista" value="{{ old('precioLista') }}">
                                    </div>

                                    <div class="form-group{{ $errors->has('bonifProv') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                        <label for="bonifProv" class="control-label">Bonificacion</label>
                                        <input id="bonifProv" type="number" step="any" class="form-control input-sm text-uppercase" name="bonifProv" min="1" max="100" value="{{ old('bonifProv') }}">
                                    </div>

                                    <div class="form-group{{ $errors->has('precioCosto') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                        <label for="precioCosto" class="control-label">Precio Costo</label>
                                        <input id="precioCosto" type="number" step="any" class="form-control input-sm text-uppercase" name="precioCosto" readonly value="{{ old('precioCosto') }}">
                                    </div>

                                    <div class="form-group{{ $errors->has('ganancia') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                        <label for="ganancia" class="control-label">Ganancia</label>
                                        <input id="ganancia" type="number" step="any" class="form-control input-sm text-uppercase" name="ganancia" min="1" value="{{ old('ganancia') }}">
                                    </div>

                                    <div class="form-group{{ $errors->has('precio') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                        <label for="precio" class="control-label">Precio Venta</label>
                                        <input id="precio" type="number" step="any" class="form-control input-sm text-uppercase" name="precio" readonly value="{{ old('precio') }}">
                                    </div>
                                    <div class="form-group{{ $errors->has('observaciones') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-10 col-md-10">
                                        <label for="observaciones" class="control-label">Link - Pagina del Fabriante</label>
                                        <input id="observaciones" type="text" class="form-control input-sm" name="observaciones" value="{{ old('observaciones') }}" placeholder="por ejemplo: http://gammaherramientas.com.ar/hidrolavadoras/hidrolavadora-150-elite">
                                    </div>
                                    <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-1 col-md-1 pull-left">
                                        <label for="activo" class="control-label">Activo</label>
                                        <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>
                                    </div>
                                </div>
                            </div>
                            <div id="tabProveedores" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>Asociacion del producto con el codigo de producto del/los proveedor/res. Facilita la actualizacion de precios y la generacion de OC</p>
                                        <div class="row">
                                            <div class="field-group col-xs-12 col-sm-12 col-md-10 col-md-10">
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <input id="codproducto" type="text" class="form-control input-sm text-uppercase" name="codproducto" placeholder="Codigo Producto">
                                                    </div>
                                                    <div class="form-group col-md-5">
<!--                                                        <label for="id_proveedor" class="control-label" for="proveedores">Proveedores</label>-->
                                                        <select id="id_proveedor" name="id_proveedor"  type="button" class="select2-container" placeholder="Proveedor" style="width:100%">
                                                            <option value=""></option>
                                                            @foreach($proveedores as $proveedor)
                                                            <option value="{{ $proveedor->id }}">{{ $proveedor->descripcion }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <button type="button" class='btn btn-default btn-xs' onclick='AgregarProv()'><i class="fa fa-btn fa-plus text-success"></i> Agregar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="field-group col-xs-12 col-sm-12 col-md-12 col-md-12">
                                                <table id='itemsprovs' class='table table-sm table-hover table-striped table-condensed'
                                                    data-unique-id="codproducto">
                                                    <thead class='thead-inverse'>
                                                        <th data-field='codproducto' name="codproducto" id="codproducto" class='text-uppercase'>Cod. Prodcuto</th>
                                                        <th data-field='idproveedor' name="idproveedor" id="idproveedor" class='text-uppercase hidden'>Cod. Proveedor</th>
                                                        <th data-field='proveedor' name="proveedor" id="proveedor" class='text-uppercase'>Proveedor</th>
                                                        <th data-field='eventos' data-align='right' data-formatter='FunctionsFormatterProvs'></th>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <a class="btn btn-default" href="/{{$urlkey}}"><i class="fa fa-btn fa-arrow-left text-danger"></i> Cancel</a>
                        <button type="submit" name="action" value="savecontinue" class="btn btn-default">
                            <i class="fa fa-btn fa-floppy-o text-primary"></i> Guardar & <i class="fa fa-btn fa-repeat text-primary"></i> Continuar</a>
                        </button>
                        <button type="submit" name="action" value="save" class="btn btn-default">
                            <i class="fa fa-btn fa-floppy-o text-primary"></i> Guardar</a>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script type='text/javascript'>
    $("#id_marca").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_rubro").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_unidadcontneto").select2({
        placeholder: "Unidad de Medida"
    });
    $("#id_unidadventa").select2({
        placeholder: "Unidad de Medida"
    });
    $("#id_proveedor").select2({
        placeholder: "Proveedor"
    });
    $('#activo').bootstrapToggle();
    $('#itemsprovs').bootstrapTable();

    function FunctionsFormatterProvs(value, row, index) {
        return "<a href='#' onclick='EliminarProv("+row.codproducto+")'><i class=\"glyphicon glyphicon-trash text-danger text-right\"></i></a>";
    };

    $(document).ready(function() {
        var preciocosto = $('input[name=precioCosto]#precioCosto');
        var precioventa = $('input[name=precio]#precio');
        var preciolista = $('input[name=precioLista]#precioLista');
        var bonificacion = $('input[name=bonifProv]#bonifProv');
        var ganancia = $('input[name=ganancia]#ganancia');
        var pc = 0, pv = 0 ;
        preciolista.on('change',function (e) {
        if(bonificacion.val() > 0 )
          pc = $(this).val() - (($(this).val() * bonificacion.val())/100);
        else
          pc = $(this).val();
        preciocosto.val(pc);
        });
        bonificacion.on('change',function (e) {
        if($(this).val() > 0 )
          pc = preciolista.val() - ((preciolista.val() * $(this).val())/100);
        else
          pc = preciolista.val() ;
        preciocosto.val(pc);
        });
        preciocosto.on('change',function (e) {
        pc = $(this).val();
        pv = parseFloat(pc) + ((pc * ganancia.val())/100);
        precioventa.val(pv)
        });
        ganancia.on('change',function (e) {
        pc = preciocosto.val();
        pv = parseFloat(pc) + ((pc * $(this).val())/100);
        precioventa.val(pv)
        });

//        $(document.body).addClass('sidebar-collapse');
    });

    
    function AgregarProv(){
        var codproducto = $("#codproducto").val();
        var idproveedor = $("#id_proveedor").val();

        if (!codproducto || !idproveedor) {
            bootbox.alert({
                message: "Debe ingresar el Codigo de Producto y el Proveedor",
                size: 'small'
            });
        } else {
            var newreg = '';
            newreg+= '"codproducto":"'+ codproducto + '",';
            var combo = document.getElementById("id_proveedor");
            var idproveedor = combo.options[combo.selectedIndex].value;
            newreg+= '"idproveedor":"'+ idproveedor + '",';
            var proveedor = combo.options[combo.selectedIndex].text;
            newreg+= '"proveedor":"'+ proveedor+'",';
            newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
            var newregarray=JSON.parse('{'+newreg+'}');
            $('#itemsprovs').bootstrapTable('append', newregarray);
        }
    }

    $('#form1').submit(function () {
        var params = [
            {
                name: "provs",
                value: JSON.stringify($('#itemsprovs').bootstrapTable('getData'))
            }

        ];
        $(this).append($.map(params, function (param) {
            return   $('<input>', {
                type: 'hidden',
                name: param.name,
                value: param.value
            })
        }))
    });
</script>
@stop
