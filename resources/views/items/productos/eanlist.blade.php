@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.catalognavbar', ['itemnav' => 'otro'])
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar"></div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-trim-on-search="false"
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-pagination-first-text='Primera'
                               data-pagination-pre-text='Ant.'
                               data-pagination-next-text='Sig.'
                               data-pagination-last-text='Ultima'
                               data-sort-name='id'
                               data-sort-order='desc'>

                            <thead class='thead-inverse'>
                                <th data-field='ean' class='hidden'>Ean</th>
                                <th data-field='id' data-align='right' class='hidden-sm hidden-xs text-uppercase'>Id</th>
                                <th data-field='descripcion'   class='text-uppercase'>Descripcion</th>
                                <th data-field='marca'   class='text-uppercase'>Marca</th>
                                <th data-field='subrubro'   class='hidden-sm hidden-xs text-uppercase'>subRubro</th>
                                <th data-field='contenidoNeto'  class='text-uppercase'>Cont. Neto</th>
                                <th data-field='precio'  class='hidden-sm hidden-xs text-uppercase'>Precio</th>
                                <th data-field='ean'  class='text-uppercase'>Codigo de Barras</th>
                                <th data-align='right' class='text-uppercase' data-formatter='FunctionsFormatter'>CARGAR</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
foreach ($items as $item) {
    $item->precio = '$ '.$item->precio;
    $item->activo = $item->activo == 1 ? 'si':'no';
}
?>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<!--<script src="/js/bootstrap-table-filter.js"></script>-->
<!--<script src="/js/bootstrap-table-filter-control.js"></script>-->
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script language='JavaScript' type='text/javascript'>
    var datos = <?php echo json_encode($items); ?>;
    $('#table1').bootstrapTable('showLoading');
    $('#table1').bootstrapTable({data: datos,exportDataType:'all',exportOptions:{fileName: 'productos'}});
    $('#table1').bootstrapTable('hideLoading');
    $(document.body).addClass('sidebar-collapse');

    function FunctionsFormatter(value, row, index) {
        return "<a id='addean' class='btn-outline-primary' data-loading-text=\"<i class='fa fa-circle-o-notch fa-spin'></i> ...\" href='#' onclick='CargarEAN("+index+","+row.id+")' title='Cargar EAN'><i class='fa fa-fw fa-barcode'></i></a>";

    };

    function CargarEAN(index,idrow){
        $("#addean").button('loading');
        iziToast.question({
            icon:'fa fa-btn fa-barcode',
            rtl: false,
            layout: 1,
            drag: false,
            timeout: false,
            close: true,
            overlay: true,
            displayMode: 1,
            id: 'question',
            progressBar: true,
            title: 'Ingresar Codigo de Barras',
            message: '',
            position: 'center',
            inputs: [
                ['<input type="text" class="col-md-8">'],
            ],
            buttons: [
                ['<button><i class="fa fa-btn fa-floppy-o"></i> Guardar</button>', function (instance, toast, button, e, inputs) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    $.ajax({
                        type: "GET",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url:'/productos/eans/update',
                        data : { iditem : idrow, ean: inputs[0].value },
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data) {
                            iziToast.success({
                                timeout: 2000,
                                position:'topRight',
                                title: 'Listo!',
                                message: data.msg
                            });
                            $('#table1').bootstrapTable('updateCell', {
                                index: index,
                                field: 'ean',
                                value: data.ean
                            });

                            $("#addean").button('reset');
                            $("#addean").dequeue();
                            $('.overlay').addClass('d-none');
                        }
                    });
                }, false]
            ],
            onClosing: function(instance, toast, closedBy){
                $("#addean").button('reset');
                $("#addean").dequeue();
                $('.overlay').addClass('d-none');
                console.info('Closing | closedBy: ' + closedBy);
            },
            onClosed: function(instance, toast, closedBy){
                $("#addean").button('reset');
                $("#addean").dequeue();
                $('.overlay').addClass('d-none');
                console.info('Closed | closedBy: ' + closedBy);
            }
        });
    }
</script>
@include('utils.statusnotification')
@stop
