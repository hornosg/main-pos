@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.catalognavbar')
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div id="box-header">
                    <div class="row">
                        <div class="col-xs-12">
                                <h5 class="text-bold">Top 100 - Productos Mas Vendidos</h5>
                                <div class="form-inline" role="form">
                                    <div class="form-group">
                                        <label for="reportrange" class="control-label">Desde/Hasta Fecha:</label>
                                        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span></span> <i class="fa fa-caret-down"></i>
                                        </div>
                                    </div>
                                    <div class="form-group" style="width: 190px;">
                                        <label for="id_titular" class="control-label">Proveedor</label>
                                        <select id="id_titular" name="id_titular"  type="button" class="form-control">
                                            <option value=""></option>
                                            @foreach(DB::table('titulares')->where('id_tipotitular', 3)->whereNull('deleted_at')->where('activo', 1)->get() as $titulares)
                                                <option value="{{$titulares->id}}">{{$titulares->descripcion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" style="width: 180px;">
                                        <label for="id_marca" class="control-label">Marca</label>
                                        <select id="id_marca" name="id_marca"  type="button" class="form-control">
                                            <option value=""></option>
                                            @foreach(DB::table('conf_marcas')->where('activo', 1)->get() as $marca)
                                                <option value="{{$marca->id}}">{{$marca->descripcion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="pull-right ml-30 pt-10">
                                        <button id="buscar" type="button" class='btn btn-primary' onclick='BuscarItems()'> <i class="fa fa-btn fa-search text-white"></i> Buscar</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div id="box-body" style="padding-top: 10px !important;">
                    <ul class="nav nav-tabs bg-white">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">En Mayor cantidad de Comprobantes</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">En Volumen</a></li>
                        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">En Importe</a></li>
                    </ul>
                    <div class="tab-content bg-white">
                        <div class="tab-pane active" id="tab_1">
                            <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                   data-toggle='table'
                                   data-toolbar="#toolbar1"
                                   data-trim-on-search="false"
                                   data-search='true'
                                   data-strict-search="false"
                                   data-multiple-search='true'
                                   data-show-export='true'>
                                <thead class='thead-inverse'>
                                <th data-field='rownumber' data-align='right' class='text-uppercase' data-formatter="rownumber">#</th>
                                <th data-field='id_item' data-align='right' class='text-uppercase'>Id</th>
                                <th data-field='producto' class='text-uppercase'>Descripcion</th>
                                <th data-field='marca' class='text-uppercase'>Marca</th>
                                <th data-field='proveedor' class='text-uppercase'>Proveedor</th>
                                <th data-field='cantidad_cptes' data-align='right'>COMPROBANTES</th>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <table id='table2' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                   data-toggle='table'
                                   data-toolbar="#toolbar2"
                                   data-trim-on-search="false"
                                   data-search='true'
                                   data-strict-search="false"
                                   data-multiple-search='true'
                                   data-show-export='true'>
                                <thead class='thead-inverse'>
                                <th data-field='rownumber' data-align='right' class='text-uppercase' data-formatter="rownumber">#</th>
                                <th data-field='id_item' data-align='right' class='text-uppercase'>Id</th>
                                <th data-field='producto' class='text-uppercase'>Descripcion</th>
                                <th data-field='marca' class='text-uppercase'>Marca</th>
                                <th data-field='proveedor' class='text-uppercase'>Proveedor</th>
                                <th data-field='cantidad_item' data-align='right'>CANTIDAD</th>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <table id='table3' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                   data-toggle='table'
                                   data-toolbar="#toolbar3"
                                   data-trim-on-search="false"
                                   data-search='true'
                                   data-strict-search="false"
                                   data-multiple-search='true'
                                   data-show-export='true'>
                                <thead class='thead-inverse'>
                                <th data-field='rownumber' data-align='right' class='text-uppercase' data-formatter="rownumber">#</th>
                                <th data-field='id_item' data-align='right' class='text-uppercase'>Id</th>
                                <th data-field='producto' class='text-uppercase'>Descripcion</th>
                                <th data-field='marca' class='text-uppercase'>Marca</th>
                                <th data-field='proveedor' class='text-uppercase'>Proveedor</th>
                                <th data-field='importe' data-formatter="moneyFormatter" data-align='right'>IMPORTE</th>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <!--<link rel="stylesheet" href="/css/admin_custom.css">-->
    <link href="/css/mainpos.css" rel="stylesheet">
    <link href="/css/bootstrap-table.css" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
    <!--<link href="/css/daterangepicker.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <style>
        #loading,#loading2{
            display: inline !important;
        }
        .pt-20{
            padding-top: 20px;
        }
    </style>
@stop

@section('js')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="/js/bootstrap-table.js"></script>
    <script src="/js/bootstrap-table-multiple-search.js"></script>
    <script src="/js/bootstrap-table-export.js"></script>
    <script src="/js/tableExport.js"></script>
    <script src="/js/bootstrap-table-es-AR.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="/vendor/select2/js/select2.full.min.js"></script>
    @include('layouts.s2multiplesearch')
    <script language='JavaScript' type='text/javascript'>
        $('#id_titular').select2({
            placeholder: "",
            allowClear: true
        });
        $('#id_marca').select2({
            placeholder: "",
            allowClear: true
        });
        function rownumber(value, row, index) {
            return  index+1;
        };
        function moneyFormatter(value, row, index) {
            return  "$ "+value;
        };
        $(function() {
            var start = moment().subtract(59, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Hoy': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Ultimos 7 dias': [moment().subtract(6, 'days'), moment()],
                    'Ultimos 30 dias': [moment().subtract(29, 'days'), moment()],
                    'Ultimos 60 dias': [moment().subtract(59, 'days'), moment()],
                    'Mes actual': [moment().startOf('month'), moment().endOf('month')],
                    'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);
        });

        var $table = $('#table1');
        $table.bootstrapTable({
            exportDataType:'all',
            exportOptions:{fileName: 'sabana'}
        });
        var $table2 = $('#table2');
        $table2.bootstrapTable({
            exportDataType:'all',
            exportOptions:{fileName: 'sabana'}
        });
        var $table3 = $('#table3');
        $table3.bootstrapTable({
            exportDataType:'all',
            exportOptions:{fileName: 'sabana'}
        });

        function moneyFormatter(value, row, index) {
            return  "$ "+value;
        };

        function BuscarItems(){
            $('#table1').bootstrapTable('showLoading');
            $('#table2').bootstrapTable('showLoading');
            $('#table3').bootstrapTable('showLoading');
            $('.overlay').removeClass('d-none');
            var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
            var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
            var comboproveedor = document.getElementById("id_titular");
            var idproveedor = comboproveedor.options[comboproveedor.selectedIndex].value;
            var combomatca = document.getElementById("id_marca");
            var idmarca = combomatca.options[combomatca.selectedIndex].value;
            $.ajax('/productos/getmasvendidos', {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data : { desde:startDate, hasta:endDate, proveedor:idproveedor, marca:idmarca},
                success: function(data) {
                    $table.bootstrapTable('removeAll');
                    $table2.bootstrapTable('removeAll');
                    $table3.bootstrapTable('removeAll');
                    $('#table1').bootstrapTable('append', data.xcptes);
                    $('#table2').bootstrapTable('append', data.xcantidad);
                    $('#table3').bootstrapTable('append', data.ximporte);
                    $('#table1').bootstrapTable('hideLoading');
                    $('#table2').bootstrapTable('hideLoading');
                    $('#table3').bootstrapTable('hideLoading');
                    $('.overlay').addClass('d-none');
                },
                error:function(data) {
                    console.log(data);
                    iziToast.error({
                        timeout: 2000,
                        position:'center',
                        title: 'Error:',
                        message: data.responseJSON.message
                    });
                    $('#table1').bootstrapTable('hideLoading');
                    $('#table2').bootstrapTable('hideLoading');
                    $('#table3').bootstrapTable('hideLoading');
                    $('.overlay').addClass('d-none');
                }
            });
        };

        $(document).ready(function(event){
            $('#table1').bootstrapTable('showLoading');
            $('#table2').bootstrapTable('showLoading');
            $('#table3').bootstrapTable('showLoading');
            var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
            var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
            $.ajax('/productos/getmasvendidos', {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data : { desde:startDate, hasta:endDate},
                success: function(data) {
                    $table.bootstrapTable('removeAll');
                    $table2.bootstrapTable('removeAll');
                    $table3.bootstrapTable('removeAll');
                    $('#table1').bootstrapTable('append', data.xcptes);
                    $('#table2').bootstrapTable('append', data.xcantidad);
                    $('#table3').bootstrapTable('append', data.ximporte);
                    $('#table1').bootstrapTable('hideLoading');
                    $('#table2').bootstrapTable('hideLoading');
                    $('#table3').bootstrapTable('hideLoading');
                    $('.overlay').addClass('d-none');
                },
                error:function(data) {
                    console.log(data);
                    iziToast.error({
                        timeout: 2000,
                        position:'center',
                        title: 'Error:',
                        message: data.responseJSON.message
                    });
                    $('#table1').bootstrapTable('hideLoading');
                    $('#table2').bootstrapTable('hideLoading');
                    $('#table3').bootstrapTable('hideLoading');
                    $('.overlay').addClass('d-none');
                }
            });
        });
    </script>
    @include('utils.statusnotification')
@stop
