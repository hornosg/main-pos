@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.catalognavbar', ['itemnav' => 'productos'])
    @if (Auth::user()->id_rol<>5)
    <div id="content_header" class="box box-primary collapsed-box mb-0">
        <div class="box-header with-border" style="height: 45px;">
            <div class="box-tools pull-right">
                <span style="margin-right: 10px;">Nuevo Producto</span>
                <button type="button" class="btn btn-primary" data-widget="collapse" onclick="scroll_to_anchor('content_header')"><i class="fa fa-plus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body form-collapsable" style="display: none;">
            <form id="form1" class="form-inline" role="form">
                {{ csrf_field() }}
                <div class="form-group" style="width: 190px;">
                    <label for="id_titular" class="control-label">Proveedor</label>
                    <select id="id_titular" name="id_titular"  type="button" class="form-control">
                        <option value=""></option>
                        @foreach(DB::table('titulares')->where('id_tipotitular', 3)->whereNull('deleted_at')->where('activo', 1)->get() as $titulares)
                        <option value="{{$titulares->id}}">{{$titulares->descripcion}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" style="width:110px;">
                    <label for="codproducto" class="control-label">Cod.Art.Prov</label>
                    <input id="codproducto"  class="form-control input-sm text-bold" style="width:100px" name="codproducto" placeholder="">
                </div>
                <div class="form-group" style="width:320px;">
                    <label for="descripcion" class="control-label">Nombre / Descripcion (*)</label>
                    <input id="descripcion"  class="form-control input-sm text-bold" style="width:320px" name="descripcion" placeholder="">
                </div>
                <div class="form-group" style="width: 180px;">
                    <label for="id_marca" class="control-label">Marca</label>
                    <a class='text-primary pull-right' href='#' onclick='CargarMarca()' title='Crear Nueva Marca'>Crear Marca</i></a>
                    <select id="id_marca" name="id_marca"  type="button" class="form-control">
                        <option value=""></option>
                        @foreach(DB::table('conf_marcas')->where('activo', 1)->get() as $marca)
                        <option value="{{$marca->id}}">{{$marca->descripcion}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" style="width:65px;">
                    <label for="contNeto" class="control-label">C.Neto</label>
                    <input id="contNeto"  class="form-control input-sm text-bold" style="width:55px" name="contNeto" placeholder="">
                </div>
                <div class="form-group" style="width:115px;">
                    <label for="id_unidadcontneto" class="control-label">U.Med C.Neto (*)</label>
                    <select id="id_unidadcontneto" name="id_unidadcontneto"  type="button" class="form-control">
                        <option value=""></option>
                        @foreach(DB::table('conf_unidades_medida')->where('activo', 1)->get() as $unidadContNeto)
                        <option value="{{ $unidadContNeto->id }}">{{ $unidadContNeto->descripcion }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" style="width:105px;">
                    <label for="id_unidadventa" class="control-label">U.Med Venta (*)</label>
                    <select id="id_unidadventa" name="id_unidadventa"  type="button" class="form-control">
                        @foreach(DB::table('conf_unidades_medida')->where('activo', 1)->get() as $unidadVenta)
                            @if ( $unidadVenta->id == 1)
                                <option value="{{ $unidadVenta->id }}" selected>{{ $unidadVenta->descripcion }}</option>
                            @else
                                <option value="{{ $unidadVenta->id }}">{{ $unidadVenta->descripcion }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group" style="width:130px;">
                    <label class="control-label">Codigo de Barras</label>
                    <input id="ean"  class="form-control input-sm text-bold" style="width:120px" name="ean" placeholder="">
                </div>
                <div class="form-group" style="width: 160px;">
                    <label for="id_moneda" class="control-label">Moneda (*)</label>
                    <select id="id_moneda" name="id_moneda"  type="button" class="form-control">
                        <option value=""></option>
                        @foreach(DB::table('sys_monedas')->where('activo', 1)->get() as $moneda)
                            @if ( $moneda->id == 1)
                                <option value="{{ $moneda->id }}" selected>{{ $moneda->descripcion }}</option>
                            @else
                                <option value="{{ $moneda->id }}">{{ $moneda->descripcion }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group" style="width:130px;">
                    <label class="control-label">Precio Lista (*)</label>
                    <input id="precioLista"  class="form-control input-sm text-bold" style="width:120px" name="precioLista" placeholder="">
                </div>
                <div class="form-group" style="width:70px;">
                    <label class="control-label">Iva</label>
                    <input id="iva"  class="form-control input-sm text-bold" style="width:60px" name="iva" value="21">
                </div>
                <div class="form-group" style="width:100px;">
                    <label class="control-label">Bonificacion</label>
                    <input id="bonifProv"  class="form-control input-sm text-bold" style="width:90px" name="bonifProv" placeholder="">
                </div>
                <div class="form-group" style="width:70px;">
                    <label class="control-label">Divisor</label>
                    <input id="divisor"  class="form-control input-sm text-bold" style="width:60px" name="divisor" value="1">
                </div>
                <div class="form-group" style="width:130px;">
                    <label class="control-label">Precio Costo</label>
                    <input id="precioCosto"  class="form-control input-sm" style="width:120px" name="precioCosto" readonly>
                </div>
                <div class="form-group" style="width:90px;">
                    <label class="control-label">Ganancia (*)</label>
                    <input id="ganancia"  class="form-control input-sm text-bold" style="width:80px" name="ganancia" placeholder="">
                </div>
                <div class="form-group" style="width:130px;">
                    <label class="control-label">Precio Venta</label>
                    <input id="precio"  class="form-control input-sm" style="width:120px" name="precio" readonly>
                </div>
                @if (env('APP_PLAN')>2)
                    </br>
                    <span style="width: 400px"> Informacion de Inventario (Opcional):</span>
                    <div class="form-group"  style="width: 205px;">
                        <label class="control-label">Sector</label>
                        <select id="id_sector_d" name="id_sector_d" class="form-control text-uppercase" >
                            <option value=""></option>
                            @foreach(DB::table('inv_sectores')
                            ->where('inv_sectores.activo', 1)
                            ->select('inv_sectores.*')
                            ->get() as $sector)
                                <option value="{{$sector->id}}">{{$sector->alias}} ( {{$sector->descripcion}} )</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group"  style="width: 205px;">
                        <label class="control-label">Ubicacion</label>
                        <select id="id_ubicacion_d" name="id_ubicacion_d" class="form-control text-uppercase" >
                            <option value=""></option>
                        </select>
                    </div>

                    <div class="form-group"  style="width: 205px;">
                        <label class="control-label">Posicion</label>
                        <select id="id_posicion_d" name="id_posicion_d" class="form-control text-uppercase" >
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group" style="width:100px;">
                        <label class="control-label">Stock Inicial</label>
                        <input id="cantidad"  class="form-control input-sm text-bold" style="width:90px" name="cantidad" placeholder="">
                    </div>
                @endif
                <div class="form-group" style="padding-top: 16px;padding-left: 10px;">
                    <button id="guardar" type="button" class='btn bg-green' onclick='GuardarItem()' data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>" title="Guardar"> <i class="fa fa-btn fa-save text-white"></i> Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    @endif
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            @if (Auth::user()->id_rol<>5)
                                <div class="form-inline" role="form">
                                    <div class="form-group" style="padding-right: 10px;">
                                        <a href="/{{$urlkey}}/create" class="btn btn-primary" title="Nuevo Producto">
                                            <i class="fa fa-btn fa-plus text-white"></i>
                                        </a>
                                        <button id="btn-eliminar" class="btn btn-danger  hidden-xs" title="Eliminar Items Seleccionados">
                                            <i class="fa fa-btn fa-trash text-white"></i>
                                        </button>
                                    </div>
                                    <span>Asignar: </span>
                                    <div class="form-group" style="width: 180px;">
                                        <select id="id_rubro" name="id_rubro"  type="button" class="form-control">
                                            <option value=""></option>
                                            @foreach(DB::table('conf_rubros')->where('activo', 1)->get() as $rubro)
                                            <option value="{{$rubro->id}}">{{$rubro->descripcion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" style="width: 200px;">
                                        <select id="id_subrubro" name="id_subrubro"  type="button" class="form-control">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="form-group" style="width:75px;">
                                        <input id="stockMinimo"  class="form-control input-sm text-bold" style="width:65px" name="stockMinimo" placeholder="STK MIN">
                                    </div>
                                    <div class="form-group" style="width: 180px;">
                                        <select id="id_proveedor" name="id_proveedor"  type="button" class="form-control">
                                            <option value=""></option>
                                            @foreach(DB::table('titulares')->where('id_tipotitular', 3)->whereNull('deleted_at')->where('activo', 1)->get() as $proveedor)
                                                <option value="{{$proveedor->id}}">{{$proveedor->descripcion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button id="vincular" type="button" class='btn btn-primary' onclick='VincularItems()' data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>" title="Asignar Rubro / Subrubro / Stock Minimo a Items Seleccionados"> <i class="fa fa-btn fa-check text-white"></i></button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-trim-on-search="false"
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-pagination-first-text='Primera'
                               data-pagination-pre-text='Ant.'
                               data-pagination-next-text='Sig.'
                               data-pagination-last-text='Ultima'
                               data-sort-name='id'
                               data-sort-order='desc'>

                            <thead class='thead-inverse'>
                                @if (Auth::user()->id_rol<>5)
                                    <th data-checkbox='true'></th>
                                @endif
                                <th data-field='ean' class='hidden'>Ean</th>
                                <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                                <th data-field='descripcion'   class='text-nowrap  text-uppercase text-bold'>Descripcion</th>
                                <th data-field='marca'   class='text-nowrap  text-uppercase'>Marca</th>
                                <th data-field='proveedores'   class='text-nowrap  text-uppercase'>Prov.</th>
                                <th data-field='rubro'   class='text-nowrap text-uppercase'>Rubro</th>
                                <th data-field='subrubro'   class='text-nowrap text-uppercase'>subRubro</th>
                                <th data-field='contenidoNeto'  class='text-uppercase'>Cont. Neto</th>
                                @if (Auth::user()->id_rol<>5)
                                    <th data-field='precio'  data-align='right' class='text-nowrap text-bold'>PRECIO</th>
                                @else
                                    <th data-field='preciodist'  data-align='right' class='text-nowrap'>PRECIO</th>
                                @endif
                                <th data-field='moneda'  class='text-uppercase'>Moneda</th>
                                <th data-field='stockMinimo'  data-align='right' class='text-uppercase'>Stk Min</th>
                                <th data-field='activo' data-align='center' class='text-uppercase'>Activo</th>
                                @if (Auth::user()->id_rol<>5)
                                    <th data-align='right' class='text-uppercase' data-formatter='FunctionsFormatter'>FUNCTIONS</th>
                                @endif
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
foreach ($items as $item) {
    $item->precio = '$ '.$item->precio;
    if (!empty($item->preciodist)){
        $item->preciodist = '$ '.$item->preciodist;
    }
    $item->activo = $item->activo == 1 ? 'si':'no';
}
?>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<style>
    .form-inline .form-group {
        padding-right: 10px;
        margin-bottom: 5px;
    }
    .fixed-table-toolbar .search {
        width: 30% !important;
    }
</style>
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<!--<script src="/js/bootstrap-table-filter.js"></script>-->
<!--<script src="/js/bootstrap-table-filter-control.js"></script>-->
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
<script language='JavaScript' type='text/javascript'>
    $('#id_titular').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_marca').select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_unidadcontneto").select2({placeholder: "",allowClear: true});
    $("#id_unidadventa").select2({placeholder: "",allowClear: true});
    $("#id_moneda").select2({placeholder: "",allowClear: true});
    $("#id_sector_d").select2({placeholder: "",allowClear: true});
    $("#id_ubicacion_d").select2({placeholder: "",allowClear: true,disabled: true});
    $("#id_posicion_d").select2({placeholder: "",allowClear: true,disabled: true});


    $('#id_rubro').select2({
        placeholder: "RUBRO",
        allowClear: true
    });
    $('#id_subrubro').select2({
        placeholder: "SUBRUBRO",
        allowClear: true
    });
    $('#id_proveedor').select2({
        placeholder: "PROVEEDOR",
        allowClear: true
    });

    var datos = <?php echo json_encode($items); ?>;
    $('#table1').bootstrapTable('showLoading');
    $('#table1').bootstrapTable({data: datos,exportDataType:'all',exportOptions:{fileName: 'productos'}});
    $('#table1').bootstrapTable('hideLoading');
    $(document.body).addClass('sidebar-collapse');

    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }

    $("#precioLista").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (Number(idmoneda)=== 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }

        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl+((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((pl*parseFloat($("#iva").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#bonifProv").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (Number(idmoneda) === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }

        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl +((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((pl*parseFloat($("#iva").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#iva").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (Number(idmoneda) === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }

        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl +((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((pl*parseFloat($("#iva").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#divisor").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (Number(idmoneda) === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }

        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl +((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((pl*parseFloat($("#iva").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#ganancia").change(function(){
        pc = $("#precioCosto").val();
        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#id_moneda").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (Number(idmoneda) === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }
        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl +((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((parseFloat($("#precioLista").val())*parseFloat($("#iva").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#id_sector_d").change(function(){
        if ($(this).val() > 0){
            $.ajax('/datos/getubicaciones_sector/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_ubicacion_d').empty();
                    $('#id_posicion_d').empty();
                    if (data.length==1){
                        $("#id_ubicacion_d").append(
                            '<option value="' + data[0].id + '">' + data[0].alias + ' (' + data[0].descripcion + ')' + '</option>'
                        );
                        $("#id_ubicacion_d").prop("disabled", true);
                        $("#id_ubicacion_d").val(data[0].id).trigger('change');
                    }else{
                        $("#id_ubicacion_d").append(
                            '<option value=""></option>'
                        );
                        $.each(data, function(index) {
                            $("#id_ubicacion_d").append(
                                '<option value="' + data[index].id + '">' + data[index].alias + ' (' + data[index].descripcion + ')' + '</option>'
                            );
                        });
                        $("#id_ubicacion_d").prop("disabled", false);
                    }
                }
            });
        }
    });

    $("#id_ubicacion_d").change(function(){
        if ($(this).val() > 0){
            $.ajax('/datos/getposiciones_ubicacion/'+$(this).val(), {
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    $('#id_posicion_d').empty();
                    if (data.length==1){
                        $("#id_posicion_d").append(
                            '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                        );
                        $("#id_posicion_d").prop("disabled", true);
                        $("#id_posicion_d").val(data[0].id).trigger('change');
                    }else{
                        $("#id_posicion_d").append(
                            '<option value=""></option>'
                        );
                        $.each(data, function(index) {
                            $("#id_posicion_d").append(
                                '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                            );
                        });
                        $("#id_posicion_d").prop("disabled", false);
                    }
                }
            });
        }
    });

    function GuardarItem(){
        var sector = $("#id_sector_d").val();
        var stkini = Number($("#cantidad").val());
        $("#guardar").button('loading');
        if (( ($("#id_sector_d").val() == "") || ($("#id_sector_d").val() == null) ) && (stkini > 0) ){
            iziToast.warning({
                timeout: 3000,
                position:'center',
                title: 'Atencion!',
                message: 'Si desea definir un Stock Inicial debe seleccionar el Sector, la ubicacion y la posicion.'
            });
            $('#guardar').button('reset');
            event.preventDefault();
        }else{
            $.ajax({
                type: "POST",
                url:'/ajax/productos/store',
                data: $('#form1').serialize(),
                success: function(data) {
                    iziToast.success({
                        timeout: 2000,
                        position:'topRight',
                        title: 'Listo!',
                        message: data.msg
                    });
                    $('#table1').bootstrapTable('append', data.item);
                    $("#codproducto").val('');
                    $("#descripcion").val('');
                    $("#ean").val('');
                    $("#cantidad").val('');
                    $("#precioLista").val('');
                    //document.getElementById('form1').reset();

                    $("#guardar").button('reset');
                    $("#guardar").dequeue();
                    $("#guardar").prop("disabled", false);
                },
                error:function(data) {
                    console.log(data);
                    iziToast.error({
                        timeout: 3000,
                        position:'center',
                        title: 'Error:',
                        message: 'Hubo un error, Los campos marcados con un (*) son obligatorios'
                    });
                    $("#guardar").button('reset');
                    $("#guardar").dequeue();
                    $("#guardar").prop("disabled", false);
                }
            });
        }
    }

    function FunctionsFormatter(value, row, index) {
        return "<a class='btn-outline-danger hidden-xs' href='/{{$urlkey}}/delete/"+row.id+"'><i class='fa fa-fw fa-times-circle'></i></a>" +
               "<a class='btn-outline-warning' href='/{{$urlkey}}/edit/"+row.id+"'><i class='fa fa-fw fa-pencil'></i></a>" +
               "<a class='btn-outline-primary' href='/{{$urlkey}}/show/"+row.id+"'><i class='fa fa-fw fa-search'></i></a>";
    };

    $(function () {
        $('#btn-eliminar').click(function (e) {
            e.preventDefault();
            var choice;
            iziToast.question({
                close: true,
                overlay: true,
                displayMode: 'once',
                id: 'question',
                zindex: 999,
                title: 'Atencion!',
                message: '¿Está seguro de Eliminar todos los items seleccionados?',
                position: 'center',
                buttons: [
                    ['<button><b>SI</b></button>', function (instance, toast) {
                        var ids = $.map($('#table1').bootstrapTable('getAllSelections'), function (row) {
                            return row.id;
                        });
                        $.ajax('/{{$urlkey}}/removeselected', {
                            type: 'POST',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: {datos:ids},
                            success: function(data){
                                $('#table1').bootstrapTable('remove', {
                                    field: 'id',
                                    values: ids
                                });
                                iziToast.success({
                                    timeout: 2000,
                                    position:'topCenter',
                                    title: 'Listo!',
                                    message: 'Items, Eliminados.'
                                });
                            },
                            error:function(data) {
                                console.log(data);
                                iziToast.error({
                                    timeout: 2000,
                                    position:'center',
                                    title: 'Error:',
                                    message: data.responseJSON.message
                                });
                            }
                        });
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    }, true],
                    ['<button>NO</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    }]
                ]
            });
        });
    });

    $("#id_rubro").change(function(){
        $("#id_subrubro").prop("disabled", true);
        $.ajax('/data/getsubrubros_rubro/'+$(this).val(), {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_subrubro').empty();
                if (data.length==1){
                    $("#id_subrubro").append(
                        '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                    );
                    $("#id_subrubro").prop("disabled", true);
                    $("#id_subrubro").val(data[0].id).trigger('change');
                }else{
                    $("#id_subrubro").append(
                        '<option value=""></option>'
                    );
                    $.each(data, function(index) {
                        $("#id_subrubro").append(
                            '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                        );
                    });
                    $("#id_subrubro").prop("disabled", false);
                }
            }
        });
    });

    function VincularItems(){
        $("#vincular").button('loading');
        var comborubro = document.getElementById("id_rubro");
        var idrubro = comborubro.options[comborubro.selectedIndex].value;
        var combosubrubro = document.getElementById("id_subrubro");
        var idsubrubro = combosubrubro.options[combosubrubro.selectedIndex].value;
        var stkmin = $('#stockMinimo').val();
        var comboproveedor = document.getElementById("id_proveedor");
        var idproveedor = comboproveedor.options[comboproveedor.selectedIndex].value;
        var ids = $.map($('#table1').bootstrapTable('getAllSelections'), function (row) {
            return row.id;
        });
        $.ajax('/productos/vincular', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: { idrubro : idrubro, idsubrubro : idsubrubro, stkmin : stkmin, proveedor: idproveedor, ids: ids },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                $('#table1').bootstrapTable('removeAll');
                $('#table1').bootstrapTable('showLoading');
                $('#table1').bootstrapTable('append', data);
                $("#vincular").button('reset');
                $("#vincular").dequeue();
                $("#vincular").prop("disabled", false);
                $('#table1').bootstrapTable('hideLoading');
            },
            error:function(data) {
                console.log(data);
                iziToast.error({
                    timeout: 2000,
                    position:'center',
                    title: 'Error:',
                    message: data.responseJSON.message
                });
                $("#vincular").button('reset');
                $("#vincular").dequeue();
                $("#vincular").prop("disabled", false);
                $('#table1').bootstrapTable('hideLoading');
            }
        });
    };

    function CargarMarca(index,idrow){
        $("#addean").button('loading');
        iziToast.question({
            icon:'fa fa-btn fa-registered',
            rtl: false,
            layout: 1,
            drag: false,
            timeout: false,
            close: true,
            overlay: true,
            displayMode: 1,
            id: 'question',
            progressBar: true,
            title: 'Ingresar Marca',
            message: '',
            position: 'center',
            inputs: [
                ['<input type="text" class="col-md-8">'],
            ],
            buttons: [
                ['<button><i class="fa fa-btn fa-floppy-o"></i> Guardar</button>', function (instance, toast, button, e, inputs) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    $.ajax({
                        type: "GET",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url:'/productos/nuevamarca',
                        data : { marca: inputs[0].value },
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data) {
                            iziToast.success({
                                timeout: 2000,
                                position:'topRight',
                                title: 'Listo!',
                                message: data.msg
                            });
                            $("#id_marca").append(
                                '<option value="' + data.id + '">' + data.descripcion  + '</option>'
                            );
                        }
                    });
                }, false]
            ],
            onClosing: function(instance, toast, closedBy){
                $("#addean").button('reset');
                $("#addean").dequeue();
                $('.overlay').addClass('d-none');
                console.info('Closing | closedBy: ' + closedBy);
            },
            onClosed: function(instance, toast, closedBy){
                $("#addean").button('reset');
                $("#addean").dequeue();
                $('.overlay').addClass('d-none');
                console.info('Closed | closedBy: ' + closedBy);
            }
        });
    }
</script>
@include('utils.statusnotification')
@stop
