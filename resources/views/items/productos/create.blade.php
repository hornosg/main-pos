@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.catalognavbar', ['itemnav' => 'productos'])
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <form id="form1" role="form" method="POST" action="/{{$urlkey}}/store">
                {{ csrf_field() }}
                <div class="box-body">
                    <input id="id_tipoitem" type="hidden" name="id_tipoitem" value="5"><!--value="7"-->

                    <div class="nav-tabs-custom">
                        <div class="nav nav-tabs">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tabInfoBasica">Información Básica</a></li>
                                <li class="hidden-sm hidden-xs"><a data-toggle="tab" href="#tabVentaAsistida">Relacionados</a></li>
                                <li class="hidden-sm hidden-xs"><a data-toggle="tab" href="#tabProveedores">Proveedores</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div id="tabInfoBasica" class="tab-pane active">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group {{ $errors->has('ean') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-4 col-md-4">
                                            <label for="ean" class="control-label">Codigo de Barras</label>
                                            <input id="ean" type="text" class="form-control input-sm text-uppercase" name="ean" value="{{ old('ean') }}">
                                        </div>
                                        <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-8 col-md-8">
                                            <label for="descripcion" class="control-label">Nombre</label>
                                            <input id="descripcion" type="text" class="form-control input-sm text-uppercase" name="descripcion" value="{{ old('descripcion') }}" autofocus>
                                        </div>
                                        <div class="form-group{{ $errors->has('id_marca') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">
                                            <label for="id_marca" class="control-label" for="marcas">Marca</label>
                                            <select id="id_marca" name="id_marca"  type="button" class="form-control">
                                                <option value=""></option>
                                                @foreach($marcas as $marca)
                                                    @if ( old('id_marca') == $marca->id )
                                                        <option value="{{ $marca->id }}" selected>{{ $marca->descripcion }}</option>
                                                    @else
                                                        <option value="{{ $marca->id }}">{{ $marca->descripcion }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group{{ $errors->has('id_rubro') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">
                                            <label for="id_rubro" class="control-label" for="rubroes">Rubro</label>
                                            <select id="id_rubro" name="id_rubro"  type="button" class="form-control">
                                                <option value=""></option>
                                                @foreach($rubros as $rubro)
                                                    @if ( old('id_rubro') == $rubro->id )
                                                        <option value="{{ $rubro->id }}" selected>{{ $rubro->descripcion }}</option>
                                                    @else
                                                        <option value="{{ $rubro->id }}">{{ $rubro->descripcion }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group{{ $errors->has('id_subrubro') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">
                                            <label for="id_subrubro" class="control-label" for="subrubros">Subrubro</label>
                                            <select id="id_subrubro" name="id_subrubro"  type="button" class="form-control" disabled>
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="form-group{{ $errors->has('id_linea') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-3">
                                            <label for="id_linea" class="control-label" for="lineaes">Linea</label>
                                            <select id="id_linea" name="id_linea"  type="button" class="form-control">
                                                <option value=""></option>
                                                @foreach($lineas as $linea)
                                                    @if ( old('id_linea') == $linea->id )
                                                        <option value="{{ $linea->id }}" selected>{{ $linea->descripcion }}</option>
                                                    @else
                                                        <option value="{{ $linea->id }}">{{ $linea->descripcion }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

<!--                                        <div class="clearfix visible-lg-block visible-md-block"></div>-->

                                        <div class="form-group{{ $errors->has('id_material') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-2 col-md-2">
                                            <label for="id_material" class="control-label" for="materiales">Material</label>
                                            <select id="id_material" name="id_material"  type="button" class="form-control">
                                                <option value=""></option>
                                                @foreach($materiales as $material)
                                                    @if ( old('id_material') == $material->id )
                                                        <option value="{{ $material->id }}" selected>{{ $material->descripcion }}</option>
                                                    @else
                                                        <option value="{{ $material->id }}">{{ $material->descripcion }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group{{ $errors->has('contNeto') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-3">
                                            <label for="contNeto" class="control-label">Cont. Neto</label>
                                            <input id="contNeto" type="number" class="form-control input-sm text-uppercase" name="contNeto" value="{{ old('contNeto') }}">
                                        </div>
                                        <div class="form-group{{ $errors->has('id_unidadcontneto') ? ' has-error' : '' }}  col-xs-12 col-sm-12 col-md-2 col-md-2">
                                            <label for="id_unidadcontneto" class="control-label" for="unidadesm">Unidad M. Cont. Neto</label>
                                            <select id="id_unidadcontneto" name="id_unidadcontneto"  type="button" class="form-control">
                                                <option value=""></option>
                                                @foreach($unidadesm as $unidadContNeto)
                                                    @if ( old('id_unidadcontneto') == $unidadContNeto->id )
                                                        <option value="{{ $unidadContNeto->id }}" selected>{{ $unidadContNeto->descripcion }}</option>
                                                    @else
                                                        <option value="{{ $unidadContNeto->id }}">{{ $unidadContNeto->descripcion }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group {{ $errors->has('id_unidadventa') ? ' has-error' : '' }}  col-xs-12 col-sm-12 col-md-2 col-md-2">
                                            <label for="id_unidadventa" class="control-label" for="unidadesm">Unidad M.Venta</label>
                                            <select id="id_unidadventa" name="id_unidadventa"  type="button" class="form-control">
                                                <option value="1">U</option>
                                                @foreach($unidadesm as $unidadVenta)
                                                    @if ( old('id_unidadventa') == $unidadVenta->id )
                                                        <option value="{{ $unidadVenta->id }}" selected>{{ $unidadVenta->descripcion }}</option>
                                                    @else
                                                        <option value="{{ $unidadVenta->id }}">{{ $unidadVenta->descripcion }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group{{ $errors->has('stockMinimo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-2 col-md-2">
                                            <label for="stockMinimo" class="control-label">Stock Minimo</label>
                                            <input id="stockMinimo" type="number" step="any" class="form-control input-sm text-uppercase" name="stockMinimo">
                                        </div>

<!--                                        <div class="clearfix visible-lg-block visible-md-block"></div>-->
<!---->
<!--                                        <div class="form-group{{ $errors->has('peso') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2 col-md-offset-0">-->
<!--                                            <label for="peso" class="control-label">Peso (kg)</label>-->
<!--                                            <input id="peso" type="number" step="any" class="form-control text-uppercase w100 input-sm" name="peso" value="{{ old('peso') }}">-->
<!--                                        </div>-->
<!--                                        <div class="form-group{{ $errors->has('largo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">-->
<!--                                            <label for="largo" class="control-label">Largo</label>-->
<!--                                            <input id="largo" type="number" step="any" class="form-control text-uppercase w100 input-sm" name="largo" value="{{ old('largo') }}">-->
<!--                                        </div>-->
<!--                                        <div class="form-group{{ $errors->has('ancho') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">-->
<!--                                            <label for="ancho" class="control-label">Ancho</label>-->
<!--                                            <input id="ancho" type="number" step="any" class="form-control text-uppercase w100 input-sm" name="ancho" value="{{ old('ancho') }}">-->
<!--                                        </div>-->
<!--                                        <div class="form-group{{ $errors->has('alto') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">-->
<!--                                            <label for="alto" class="control-label">Alto</label>-->
<!--                                            <input id="alto" type="number" step="any" class="form-control text-uppercase w100 input-sm" name="alto" value="{{ old('alto') }}">-->
<!--                                        </div>-->

                                        <div class="form-group{{ $errors->has('id_moneda') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                            <label for="id_moneda" class="control-label" for="subrubros">Moneda</label>
                                            <select id="id_moneda" name="id_moneda"  type="button" class="form-control">
                                                <option value=""></option>
                                                @foreach(DB::table('sys_monedas')->where('activo', 1)->get() as $moneda)
                                                    @if ( old('id_moneda') == $moneda->id )
                                                        <option value="{{ $moneda->id }}" selected>{{ $moneda->descripcion }}</option>
                                                    @else
                                                        <option value="{{$moneda->id}}">{{$moneda->descripcion}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

<!--                                        <div class="clearfix visible-lg-block visible-md-block"></div>-->

                                        <div class="form-group{{ $errors->has('precioLista') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2 col-md-offset-0">
                                            <label for="precioLista" class="control-label">Precio Lista</label>
                                            <input id="precioLista" class="form-control input-sm text-uppercase" name="precioLista" value="{{ old('precioLista') }}">
                                        </div>

                                        <div class="form-group{{ $errors->has('iva') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-1">
                                            <label for="iva" class="control-label">Iva</label>
                                            <input id="iva"  class="form-control input-sm text-uppercase" name="iva" value="21">
                                        </div>

                                        <div class="form-group{{ $errors->has('bonifProv') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-1">
                                            <label for="bonifProv" class="control-label">Bonificacion</label>
                                            <input id="bonifProv" class="form-control input-sm text-uppercase" name="bonifProv"  value="{{ old('bonifProv') }}">
                                        </div>

                                        <div class="form-group{{ $errors->has('divisor') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-1">
                                            <label for="divisor" class="control-label">Divisor</label>
                                            <input id="divisor"  class="form-control input-sm text-uppercase" name="divisor" value="1">
                                        </div>

                                        <div class="form-group{{ $errors->has('precioCosto') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                            <label for="precioCosto" class="control-label">Precio Costo</label>
                                            <input id="precioCosto"  class="form-control input-sm text-uppercase" name="precioCosto" readonly value="{{ old('precioCosto') }}">
                                        </div>

                                        <div class="form-group{{ $errors->has('ganancia') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-1">
                                            <label for="ganancia" class="control-label">Ganancia</label>
                                            <input id="ganancia" class="form-control input-sm text-uppercase" name="ganancia"  value="{{ old('ganancia') }}">
                                        </div>


                                        <div class="form-group{{ $errors->has('precio') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-2">
                                            <label for="precio" class="control-label">Precio Venta</label>
                                            <input id="precio"  class="form-control input-sm text-uppercase" name="precio" readonly value="{{ old('precio') }}">
                                        </div>
                                        <div class="form-group{{ $errors->has('observaciones') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-10 col-md-10">
                                            <label for="observaciones" class="control-label">Link - Pagina del Fabriante</label>
                                            <input id="observaciones" type="text" class="form-control input-sm" name="observaciones" value="{{ old('observaciones') }}" placeholder="por ejemplo: http://gammaherramientas.com.ar/hidrolavadoras/hidrolavadora-150-elite">
                                        </div>
                                        <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-1 col-md-1 pull-left">
                                            <label for="activo" class="control-label">Activo</label>
                                            <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>
                                        </div>
                                        <div class="form-group {{ $errors->has('distribuye') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-1 col-md-1 pull-left">
                                            <label for="distribuye" class="control-label">Distribuye</label>
                                            <input id="distribuye" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="distribuye" checked>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tabVentaAsistida" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Asociacion del producto con otros, con el fin de mostrarlos en la parte de ventas como alternativa o complementarios al que se quiere comprar.
                                        </br>Si un cliente busca el producto Y se le puede ofrecer el producto X de mayor ganancia, generando una venta más rentable.
                                        </br>Si busca el producto Y se le puede/n ofrecer tambien el/los producto/s Z incitando al cliente a comprarlo tambien.</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>
                                            <strom>Productos Alternativos</strom>
                                            <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modal-art1">Buscar</button>
                                        </h4>
                                        <div class="row">
                                            <div class="field-group col-xs-12 col-sm-12 col-md-12 col-md-12">
                                                <table id='itemsups' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                                    data-unique-id="id">
                                                    <thead class='thead-inverse'>
                                                        <th data-field='id' name="id" id="id" class='text-uppercase'>ID</th>
                                                        <th data-field='marca' name="marca" id="marca" class='text-uppercase'>Marca</th>
                                                        <th data-field='descripcion' name="descripcion" id="descripcion" class='text-uppercase'>Descripcion</th>
                                                        <th data-field='contenidoNeto' name="contenidoNeto" id="contenidoNeto" class='text-uppercase'>C.Neto</th>
                                                        <th data-field='precio' name="precio" id="precio" class='text-uppercase'>Precio</th>
                                                        <th data-field='eventos' data-align='right' data-formatter='FunctionsFormatterUps'></th>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>
                                            <strom>Productos Complementarios</strom>
                                            <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modal-art2">Buscar</button>
                                        </h4>
                                        <div class="row">
                                            <div class="field-group col-xs-12 col-sm-12 col-md-12 col-md-12">
                                                <table id='itemscross' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                                    data-unique-id="id">
                                                    <thead class='thead-inverse'>
                                                        <th data-field='id' name="id" id="id" class='text-uppercase'>ID</th>
                                                        <th data-field='marca' name="marca" id="marca" class='text-uppercase'>Marca</th>
                                                        <th data-field='descripcion' name="descripcion" id="descripcion" class='text-uppercase'>Descripcion</th>
                                                        <th data-field='contenidoNeto' name="contenidoNeto" id="contenidoNeto" class='text-uppercase'>C.Neto</th>
                                                        <th data-field='precio' name="precio" id="precio" class='text-uppercase'>Precio</th>
                                                        <th data-field='eventos' data-align='right' data-formatter='FunctionsFormatterCross'></th>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tabProveedores" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>Asociacion del producto con el codigo de producto del/los proveedore/res. Facilita la actualizacion de precios y la generacion de OC</p>
                                            <div class="field-group">
                                                <div class="form-group col-md-3">
                                                    <label for="codproducto" class="control-label">Cod.Producto</label>
                                                    <input id="codproducto" type="text" class="form-control input-sm text-uppercase" name="codproducto" placeholder="Codigo Producto">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="id_proveedor" class="control-label" for="proveedores">Proveedor</label>
                                                    <select id="id_proveedor" name="id_proveedor"  type="button" class="select2-container" placeholder="Proveedor" style="width:100%">
                                                        <option value=""></option>
                                                        @foreach($proveedores as $proveedor)
                                                        <option value="{{ $proveedor->id }}">{{ $proveedor->descripcion }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label for="actualizaprecio" class="control-label">Actualiza Precio</label>
                                                    <input id="actualizaprecio" type="checkbox" data-toggle="toggle" data-on="SI" data-off="NO" name="actualizaprecio" checked>
                                                </div>
                                                <div class="form-group col-md-2 mt-20">
                                                    <button type="button" class='btn btn-default btn-xs' onclick='AgregarProv()'><i class="fa fa-btn fa-plus text-success"></i> Agregar</button>
                                                </div>
                                            </div>
                                        <div class="row">
                                            <div class="field-group col-xs-12 col-sm-12 col-md-12 col-md-12">
                                                <table id='itemsprovs' class='table table-sm table-hover table-striped table-condensed'
                                                    data-unique-id="idproveedor">
                                                    <thead class='thead-inverse'>
                                                        <th data-field='codproducto' name="codproducto" id="codproducto" class='text-uppercase'>Cod. Prodcuto</th>
                                                        <th data-field='idproveedor' name="idproveedor" id="idproveedor" class='text-uppercase hidden'>Cod. Proveedor</th>
                                                        <th data-field='proveedor' name="proveedor" id="proveedor" class='text-uppercase'>Proveedor</th>
                                                        <th data-field='actualizaprecio' name="actualizaprecio" data-align='center' class='text-uppercase col-md-2'>Actualiza Precio</th>
                                                        <th data-field='eventos' data-align='right' data-formatter='FunctionsFormatterProvs'></th>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="/{{$urlkey}}"><i class="fa fa-btn fa-arrow-left"></i> Cancel</a>
                        <button type="submit" name="action" value="savecontinue" class="btn btn-primary">
                            <i class="fa fa-btn fa-floppy-o"></i> Guardar & <i class="fa fa-btn fa-repeat"></i> Continuar</a>
                        </button>
                        <button type="submit" name="action" value="save" class="btn btn-primary">
                            <i class="fa fa-btn fa-floppy-o"></i> Guardar</a>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-art1">
    <div class="modal-dialog modalart">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Agregar Producto Alternativo</h4>
            </div>
            <div class="modal-body box">
                <table id='tmodalups' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                       data-toggle='table'
                       data-toolbar="#toolbar"
                       data-search='true'
                       data-strict-search="false"
                       data-multiple-search='true'
                       data-pagination='true'
                       data-page-size='10'
                       data-pagination-first-text='Primera'
                       data-pagination-pre-text='Ant.'
                       data-pagination-next-text='Sig.'
                       data-pagination-last-text='Ultima'
                       data-sort-name='id'
                       data-unique-id='id'
                       data-sort-order='desc'>

                    <thead class='thead-inverse'>
                    <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                    <th data-field='descripcion'   class='text-uppercase'>Descripcion</th>
                    <th data-field='marca'   class='text-uppercase'>Marca</th>
                    <th data-field='rubro'   class='text-uppercase'>Rubro</th>
                    <th data-field='contenidoNeto'  class='text-uppercase'>Cont. Neto</th>
                    <th data-field='precio'  class='text-uppercase'>Precio</th>
                    <th data-align='right' class='text-uppercase' data-formatter='FunctionsFormatterModalUp'></th>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-art2">
    <div class="modal-dialog modalart">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Agregar Producto Complementario</h4>
            </div>
            <div class="modal-body box">
                <table id='tmodalcross' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                       data-toggle='table'
                       data-toolbar="#toolbar"
                       data-search='true'
                       data-strict-search="false"
                       data-multiple-search='true'
                       data-pagination='true'
                       data-page-size='10'
                       data-pagination-first-text='Primera'
                       data-pagination-pre-text='Ant.'
                       data-pagination-next-text='Sig.'
                       data-pagination-last-text='Ultima'
                       data-sort-name='id'
                       data-unique-id='id'
                       data-sort-order='desc'>

                    <thead class='thead-inverse'>
                    <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                    <th data-field='descripcion'   class='text-uppercase'>Descripcion</th>
                    <th data-field='marca'   class='text-uppercase'>Marca</th>
                    <th data-field='rubro'   class='text-uppercase'>Rubro</th>
                    <th data-field='contenidoNeto'  class='text-uppercase'>Cont. Neto</th>
                    <th data-field='precio'  class='text-uppercase'>Precio</th>
                    <th data-align='right' class='text-uppercase' data-formatter='FunctionsFormatterModalCross'></th>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<?php
foreach ($productos as $producto) {
    $producto->precio = '$ '.$producto->precio;
}
?>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script type='text/javascript'>
    $("#id_marca").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_rubro").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_subrubro").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_material").select2({
        placeholder: "",
        allowClear: true
    });
    $("#idModelo").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_linea").select2({
        placeholder: "",
        allowClear: true
    });

    $("#id_unidadcontneto").select2({
        placeholder: "",
        allowClear: true
    });
    $("#id_unidadventa").select2({placeholder: "",allowClear: true});
    $("#id_moneda").select2({placeholder: "", allowClear: true});
    $("#id_proveedor").select2({placeholder: "PROVEEDOR", allowClear: true});

    $("#id_rubro").change(function(){
        $("#id_subrubro").prop("disabled", true);
        $.ajax('/data/getsubrubros_rubro/'+$(this).val(), {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_subrubro').empty();
                if (data.length==1){
                    $("#id_subrubro").append(
                        '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                    );
                    $("#id_subrubro").prop("disabled", true);
                    $("#id_subrubro").val(data[0].id).trigger('change');
                }else{
                    $("#id_subrubro").append(
                        '<option value=""></option>'
                    );
                    $.each(data, function(index) {
                        $("#id_subrubro").append(
                            '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                        );
                    });
                    $("#id_subrubro").prop("disabled", false);
                }
            }
        });
    });

    $('#itemsups').bootstrapTable();
    $('#itemscross').bootstrapTable();
    $('#itemsprovs').bootstrapTable();

    function FunctionsFormatterUps(value, row, index) {
        return "<a href='#' onclick='EliminarUp("+row.id+")'><i class=\"glyphicon glyphicon-trash text-danger text-right\"></i></a>";
    };
    function FunctionsFormatterCross(value, row, index) {
        return "<a href='#' onclick='EliminarCross("+row.id+")'><i class=\"glyphicon glyphicon-trash text-danger text-right\"></i></a>";
    };
    function FunctionsFormatterProvs(value, row, index) {
        return "<a href='#' onclick='EliminarProv("+row.idproveedor+")'><i class=\"glyphicon glyphicon-trash text-danger text-right\"></i></a>";
    };

    var datos = <?php echo json_encode($productos); ?>;
    $('#tmodalups').bootstrapTable({data: datos});
    $('#tmodalcross').bootstrapTable({data: datos});

    function FunctionsFormatterModalUp(value, row, index) {
        return "<button class='btn btn-xs' onclick='AgregarUp("+row.id+")'>Agregar</button>";
    };
    function FunctionsFormatterModalCross(value, row, index) {
        return "<button class='btn btn-xs' onclick='AgregarCross("+row.id+")'>Agregar</button>";
    };


    $("#precioLista").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (Number(idmoneda)=== 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }

        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl+((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((pl*parseFloat($("#iva").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#bonifProv").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (Number(idmoneda) === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }

        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl +((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((pl*parseFloat($("#iva").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#iva").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (Number(idmoneda) === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }

        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl +((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((pl*parseFloat($("#iva").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#divisor").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (Number(idmoneda) === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }

        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl +((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((pl*parseFloat($("#iva").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#ganancia").change(function(){
        pc = $("#precioCosto").val();
        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    $("#id_moneda").change(function(){
        var combomoneda = document.getElementById("id_moneda");
        var idmoneda = combomoneda.options[combomoneda.selectedIndex].value;
        if (Number(idmoneda) === 2) {
            pl = parseFloat($("#precioLista").val()) * dolar;
        }else{
            pl = parseFloat($("#precioLista").val());
        }
        if(parseFloat($("#bonifProv").val()) > 0 ){
            pc = pl + ((pl*parseFloat($("#iva").val()))/100) - (( (pl +((pl*parseFloat($("#iva").val()))/100)) * parseFloat($("#bonifProv").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }else{
            pc = pl + ((parseFloat($("#precioLista").val())*parseFloat($("#iva").val()))/100);
            if (parseFloat($("#divisor").val())>1){
                pc = pc/parseFloat($("#divisor").val());
            }
            $("#precioCosto").val(pc.toFixed(2));
        }

        if(parseFloat($("#ganancia").val()) > 0 ){
            pv = parseFloat($("#precioCosto").val()) + ((parseFloat($("#precioCosto").val())*parseFloat($("#ganancia").val()))/100);
            $("#precio").val(pv.toFixed(2));
        }else{
            pv = pc;
            $("#precio").val(pv.toFixed(2));
        }
    });

    function AgregarUp(id){
        var data = $('#tmodalups').bootstrapTable('getRowByUniqueId', id);

        var newup = '';
        newup+= '"id":"'+ data['id'] + '",';
        newup+= '"marca":"'+ data['marca']+'",';
        newup+= '"descripcion":"'+ data['descripcion']+'",';
        newup+= '"contenidoNeto":"'+ data['contenidoNeto']+'",';
        newup+= '"precio":"'+ data['precio']+'",';
        newup+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
        var newuparray=JSON.parse('{'+newup+'}');
        $('#itemsups').bootstrapTable('append', newuparray);
        data.length = 0;
        $('#modal-art1').modal('hide');
    };

    function AgregarCross(id){
        var data = $('#tmodalups').bootstrapTable('getRowByUniqueId', id);

        var newcross = '';
        newcross+= '"id":"'+ data['id'] + '",';
        newcross+= '"marca":"'+ data['marca']+'",';
        newcross+= '"descripcion":"'+ data['descripcion']+'",';
        newcross+= '"contenidoNeto":"'+ data['contenidoNeto']+'",';
        newcross+= '"precio":"'+ data['precio']+'",';
        newcross+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
        var newcrossarray=JSON.parse('{'+newcross+'}');
        $('#itemscross').bootstrapTable('append', newcrossarray);
        data.length = 0;
        $('#modal-art2').modal('hide');
    };
    
    function AgregarProv(){
        var codproducto = $("#codproducto").val();
        var idproveedor = $("#id_proveedor").val();
        var actualizaprecio = $('#actualizaprecio').is(':checked') ? 'SI':'NO';

        if (!codproducto || !idproveedor) {
            bootbox.alert({
                message: "Debe ingresar el Codigo de Producto y el Proveedor",
                size: 'small'
            });
        } else {
            var newreg = '';
            newreg+= '"codproducto":"'+ codproducto + '",';
            var combo = document.getElementById("id_proveedor");
            var idproveedor = combo.options[combo.selectedIndex].value;
            newreg+= '"idproveedor":"'+ idproveedor + '",';
            var proveedor = combo.options[combo.selectedIndex].text;
            newreg+= '"proveedor":"'+ proveedor+'",';
            newreg+= '"actualizaprecio":"'+ actualizaprecio + '",';
            newreg+= '"eventos":"<a href=\'javascript:void(0);\' id=\'hapus\'><i class=\'glyphicon glyphicon-trash text-danger\'></i></a>"';
            var newregarray=JSON.parse('{'+newreg+'}');
            $('#itemsprovs').bootstrapTable('append', newregarray);
        }
    }

    function EliminarUp(id){
        $('#itemsups').bootstrapTable('removeByUniqueId', id);
    }

    function EliminarCross(id){
        $('#itemscross').bootstrapTable('removeByUniqueId', id);
    }

    function EliminarProv(id){
        $('#itemsprovs').bootstrapTable('removeByUniqueId', id);
    }

    $('#form1').submit(function () {
        var params = [
            {
                name: "ups",
                value: JSON.stringify($('#itemsups').bootstrapTable('getData'))
            },
            {
                name: "cross",
                value: JSON.stringify($('#itemscross').bootstrapTable('getData'))
            },
            {
                name: "provs",
                value: JSON.stringify($('#itemsprovs').bootstrapTable('getData'))
            }

        ];
        $(this).append($.map(params, function (param) {
            return   $('<input>', {
                type: 'hidden',
                name: param.name,
                value: param.value
            })
        }))
    });
</script>
@stop
