@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    <style>
        .main-sidebar{
            display: none;
        }
        html body.skin-black-light.sidebar-mini.sidebar-collapse div.wrapper div.content-wrapper{
            margin-left: 0px !important;
            min-height: 555px !important;
        }
        html body.skin-black-light.sidebar-mini.sidebar-collapse div.wrapper footer.main-footer{
            margin-left: 0px !important;
        }
    </style>
    @stack('css')
    @yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper overlay-wrapper">
        <div class="overlay d-none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- Main Header -->
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
                            {!! config('adminlte.logo', 'Main<b>POS</b>') !!}
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
            @else
            <!-- Logo -->
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo ">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">{!! config('adminlte.logo_mini', 'M<b>POS</b>') !!}</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">{!! config('adminlte.logo', 'Main<b>POS</b>') !!}</span>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top hidden-xs" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="hidden-lg hidden-md hidden-sm sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                </a>
            @endif
                <!-- Navbar Right Menu -->
                <div class="navbar-custom">
                    @if ((URL::current() != URL::to('/')) and (URL::current() != URL::to('/home')))
                        <ul class="nav navbar-nav">
                            @if (Auth::user()->id_rol<>5)
                                <li class="hidden-sm mt-5"><a href="/productos" title="Prodcutos"><i class="material-icons md-48 fs-xlarge">ballot</i></a></li>
                                @if (env('APP_PLAN')>1)
                                    <li class="hidden-sm mt-5"><a href="/inventario/stock/index" title="Stock"><i class="material-icons md-48 fs-xlarge">widgets</i></a></li>
                                @endif
                                <li class="hidden-sm mt-5"><a href="/precios" title="Precios"><i class="material-icons md-48 fs-xlarge">local_offer</i></a></li>
                                <li class="hidden-sm mt-5"><a href="/clientes" title="Clientes"><i class="fa fa-users fs-xlarge"></i></a></li>
                                @if (env('MP_PRESUPUESTOS'))
                                <li class="hidden-sm mt-5"><a href="/presupuestos" title="Oportunidades de Venta"><i class="fa fa-magnet fs-xlarge"></i></a></li>
                                @endif
                                <li class="hidden-sm mt-5"><a href="/ventas" title="Ventas & Cobranzas"><i class="material-icons md-48 fs-xlarge">monetization_on</i></a></li>
                                <li class="hidden-sm mt-5"><a href="/caja" title="Caja"><i class="fa fa-inbox fs-xlarge"></i></a></li>
                                @if (env('MP_CHEQUES'))
                                <li class="hidden-sm mt-5"><a href="/cheques" title="Cheques"><i class="fa fa-money fs-xlarge"></i></a></li>
                                @endif
                                <li class="hidden-sm mt-5"><a href="/clientes/cuentacorriente" title="Cuenta Corriente"><i class="fa fa-pencil-square-o fs-xlarge"></i></a></li>
                                @if (env('MP_VENCIDOS'))
                                <li class="hidden-sm mt-5"><a href="/clientes/vencidos" title="Vencidos"><i class="fa fa-warning fs-xlarge"></i></a></li>
                                @endif
                                <li class="hidden-sm mt-5"><a href="/proveedores" title="Proveedores"><i class="fa fa-truck fs-xlarge"></i></a></li>
                                <li class="hidden-sm mt-5"><a href="/compras" title="Compras & Pagos"><i class="fa fa-credit-card fs-xlarge"></i></a></li>
                                <li class="hidden-sm mt-5"><a href="/gastos" title="Gastos"><i class="fa fa-shopping-cart fs-xlarge"></i></a></li>
                            @endif
                        </ul>
                    @endif
                    <ul class="nav navbar-nav navbar-right" style="background-color: #A93226;">
                        @if (Auth::user()->id_rol<>5)
                        <li class="hidden-xs dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-plus-square"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li>
                                            <a href="/cptes/create/rem">
                                                <i class="fa fa-sticky-note text-primary"></i> Nuevo Remito
                                            </a>
                                        </li>

                                        <li>
                                            <a href="/cptes/create/rbo">
                                                <i class="fa fa-check-square-o text-red"></i> Nuevo Recibo
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/cptes/create/nc">
                                                <i class="fa fa-sign-in text-yellow"></i> Nueva Nota de Credito
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/cptes/create/pre">
                                                <i class="fa fa-magnet text-primary"></i> Nuevo Presupuesto
                                            </a>
                                        </li>

<!--                                        <li>-->
<!--                                            <a href="/cptes/create/fc">-->
<!--                                                <i class="fa fa-sticky-note text-green"></i> Factura-->
<!--                                            </a>-->
<!--                                        </li>-->
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="hidden-xs dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-list-ul"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li>
                                            <a href="/cptes/rem">
                                                <i class="fa fa-sticky-note text-primary"></i> Listado de Remitos
                                            </a>
                                        </li>
                                        @if (Auth::user()->id_rol<>5)
                                        <li>
                                            <a href="/cptes/rbo">
                                                <i class="fa fa-check-square-o text-red"></i> Listado de Recibos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/cptes/nc">
                                                <i class="fa fa-sign-in text-yellow"></i> Listado de N.Credito
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/cptes/pre">
                                                <i class="fa fa-magnet text-primary"></i> Listado de Presupuestos
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="hidden-xs dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa fa-user-plus"></i>
                            </a>
                            <ul class="dropdown-menu bg-black newtit">
                                <div class="box bg-black">
                                <!-- User image -->
                                <form id="newtit" name="newtit" role="form">
                                    {{ csrf_field() }}
                                    <!-- Menu Body -->
                                    <div class="box-body bg-black text-white">
                                        <div id="tipoclientes" class="form-group {{ $errors->has('id_tipocliente') ? ' has-error' : '' }} col-md-6">
                                            <label for="id_tipocliente" class="control-label">Tipo Cliente</label>
                                            <select id="id_tipocliente" name="id_tipocliente" class="form-control text-uppercase">
                                                <option value=""></option>
                                                @foreach(DB::table('sys_tipoclientes')->get() as $tipocliente)
                                                <option value="{{$tipocliente->id}}">{{$tipocliente->descripcion}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="clearfix visible-lg-block visible-md-block"></div>

                                        <div class="form-group text-white {{ $errors->has('descripciontit') ? 'has-error' : '' }} col-md-6">
                                            <label for="descripciontit" class="control-label">Nombre (*)</label>
                                            <input id="descripciontit" type="text" class="form-control input-sm text-uppercase" name="descripciontit" value="{{ old('descripciontit') }}" autofocus>
                                        </div>
                                        <div class="form-group text-white {{ $errors->has('direccion') ? 'has-error' : '' }} col-md-6">
                                            <label for="direccion" class="control-label">Direccion (*)</label>
                                            <input id="direccion" type="text" class="form-control input-sm text-uppercase" name="direccion" value="{{ old('direccion') }}">
                                        </div>

                                        <div class="clearfix visible-lg-block visible-md-block"></div>

                                        <div class="form-group text-white {{ $errors->has('id_localidad') ? ' has-error' : '' }} col-md-6">
                                            <label for="id_localidad" class="control-label">Localidad</label>
                                            <select id="id_localidad" name="id_localidad" class="form-control text-uppercase">
                                                <option value=""></option>
                                                @foreach(DB::table('conf_localidades')->get() as $localidad)
                                                    <option value="{{$localidad->id}}">{{$localidad->descripcion}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group text-white {{ $errors->has('telefono') ? 'has-error' : '' }} col-md-6">
                                            <label for="telefono" class="control-label">Telefono (*)</label>
                                            <input id="telefono" type="text" class="form-control input-sm text-uppercase" name="telefono" value="{{ old('telefono') }}" autofocus>
                                        </div>

                                        <div class="clearfix visible-lg-block visible-md-block"></div>

                                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} col-md-6">
                                            <label for="email" class="control-label">Email</label>
                                            <input id="email" type="text" class="form-control input-sm text-uppercase" name="email" value="{{ old('email') }}" autofocus>
                                        </div>
                                        <div class="form-group text-white {{ $errors->has('fecha_cumple') ? 'has-error' : '' }} col-md-6">
                                            <label for="fecha_cumple" class="control-label">F.Nacimiento / Cumpleaños</label>
                                            <input id="fecha_cumple" type="date" class="form-control input-sm" name="fecha_cumple" value="{{ old('fecha_cumple') }}" autofocus>
                                        </div>
                                        <div class="form-group pull-right pr-10">
                                            <button type="submit" name="action" value="save" class="btn btn-primary primary">
                                                <i class="fa fa-btn fa-floppy-o text-white text-bold"></i> Guardar</a>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </ul>
                        </li>
                        @endif
                        @php
                            $cotizacion = DB::select('select date_format(c.created_at, "%d/%m/%Y %H:%i:%s") as fecha, cambio
                                                        from cotizaciones as c
                                                       where id = ( SELECT MAX(id) FROM cotizaciones
                                                                     WHERE id_sucursal ='.Auth::user()->id_sucursal.')');
                            if(!empty($cotizacion)){
                                $fecha=$cotizacion[0]->fecha;
                                $cambio=$cotizacion[0]->cambio;
                            }else{
                                $fecha='';
                                $cambio=1;
                            }
                        @endphp
                        @if ( (Auth::user()->id_rol==1) || (Auth::user()->id_rol==2))
                            <li class="hidden-xs dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><strong>USD</strong></a>
                                <ul class="dropdown-menu bg-black">
                                    <div class="box bg-black">
                                        <!-- User image -->
                                        <form id="dolarform" role="form">
                                            {{ csrf_field() }}
                                            <div class="box-body bg-black">
                                                <h5 id="cotfecha" class="text-bold text-center">{{$fecha}}</h5>
                                                <h1 id="cotcambio" class="text-bold text-danger text-center pb-10">$ {{$cambio}}</h1>
                                                <div class="input-group input-group-sm col-md-12 p-10">
                                                    <input id="cambio" name="cambio" class="form-control">
                                                    <span class="input-group-btn">
                                                        <button type="submit" class="btn btn-primary btn-flat">
                                                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </ul>
                            </li>
                        @endif
                        <li class="hidden-xs dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><strong><i class="fa fa-magic"></i></strong></a>
                            <ul class="dropdown-menu bg-black"  style="width: 400px !important;">
                                <div class="box bg-black">
                                    <!-- User image -->
                                    <form id="bonifform" role="form">
                                        <div class="box-body bg-black">
                                            <h5 class="text-bold text-center">Descuentos en Cascada</h5>
                                            <div class="form-group col-md-3">
                                                <label class="control-label">% Desc 1</label>
                                                <input id="desc1" name="descuento1" class="form-control">
                                            </div>
                                            <div class="form-group col-md-1">
                                                <label class="control-label">+</label>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="control-label">% Desc 2</label>
                                                <input id="desc2" name="descuento2" class="form-control">
                                            </div>
                                            <div class="form-group col-md-1">
                                                <label class="control-label">+</label>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="control-label">% Desc 3</label>
                                                <input id="desc3" name="descuento3" class="form-control">
                                            </div>
                                            <h5 class="text-bold text-center">Descuento Real Equivalente</h5>
                                            <h1 id="descreal" class="text-bold text-danger text-center pb-10">0</h1>
                                        </div>
                                    </form>
                                </div>
                            </ul>
                        </li>
                        @if (Auth::user()->id_rol<>5)
                        <li class="dropdown notifications-menu noti-icon">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-bell"></i>
                                @php
                                    $cant = DB::select('select count(*) as cant
                                                         from notificaciones as n
                                                         join notificaciones_estados as ne on ne.id_notificacion = n.id
                                                        where n.deleted_at is null
                                                          and ne.id_estado=14
                                                          and ne.id_user = '.Auth::user()->id);
                                @endphp
                                @if ($cant[0]->cant > 0 )
                                    <span class="label label-danger noleidos">{{$cant[0]->cant}}</span>
                                @endif
                            </a>
                            <ul class="dropdown-menu notifs">
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        @foreach(DB::table('notificaciones as n')
                                                    ->join('notificaciones_estados as ne', 'n.id', '=', 'ne.id_notificacion')
                                                    ->whereNull('n.deleted_at')
                                                    ->where('ne.id_estado','=',14)
                                                    ->where('ne.id_user','=',Auth::user()->id)
                                                    ->orderByRaw('1 DESC')->get() as $notificacion)
                                            <li class="noleidos">
                                                <a href="#">
                                                    <i class="{{$notificacion->icon}}"></i> {{$notificacion->descripcion}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="footer">
                                    @if ($cant[0]->cant > 0 )
                                        <a id="sinleer" class="noleidos" href="#">Macar todos como Leídos</a>
                                    @else
                                        <a href="#">No hay nuevas Notificacciones</a>
                                    @endif
                                    <a href="/notificaciones" target="_blank">Ver Listado Completo</a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        <li class="hidden-xs dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="hidden-xs">{{Auth::user()->name}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <h4 class="text-black">{{Auth::user()->full_name}}</h4>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <button id="chgclv" type="button" name="chgclv" class="btn btn-default btn-flat"><i class="fa fa-fw fa-refresh"></i> Cambiar Clave</button>
                                        @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                            <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                                <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                            </a>
                                        @else
                                            <a href="#" class="btn btn-default btn-flat" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                            </a>
                                            <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                                @if(config('adminlte.logout_method'))
                                                {{ method_field(config('adminlte.logout_method')) }}
                                                @endif
                                                {{ csrf_field() }}
                                            </form>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </li>
                        @if (env('APP_PLAN')>2)
                            @if (Auth::user()->id_rol==1)
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                            @endif
                        @endif
                        <li>

                        </li>
                    </ul>
                </div>
                @if(config('adminlte.layout') == 'top-nav')
                </div>
                @endif
            </nav>
        </header>


<!--        @if(config('adminlte.layout') != 'top-nav')-->
        <!-- Left side column. contains the logo and sidebar -->
<!--        <aside class="main-sidebar hidden-xs">-->
            <!-- sidebar: style can be found in sidebar.less -->
<!--            <section class="sidebar">-->
                <!-- Sidebar Menu -->
<!--                <ul class="sidebar-menu" data-widget="tree">-->
<!--                    @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')-->
<!--                </ul>-->
                <!-- /.sidebar-menu -->
<!--            </section>-->
            <!-- /.sidebar -->
<!--        </aside>-->
<!--        @endif-->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            {{--<div style="padding: 5px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">--}}
                {{--<a style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Test Notifitacions!</a>--}}
            {{--</div>--}}

            @if(config('adminlte.layout') == 'top-nav')
            <div class="container">
            @endif

            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content_header')
            </section>

            <!-- Main content -->
            <section class="content">

                @yield('content')

            </section>
            <!-- /.content -->
            @if(config('adminlte.layout') == 'top-nav')
            </div>
            <!-- /.container -->
            @endif
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
<!--                <b>Version</b> 1.0.0-->
            </div>
            <img src="/img/blank.gif" class="flag flag-ar" alt="Argentina" /><strong> Copyright &copy; 2018 <a href="https://mainpos.com.ar">MainPOS</a>.</strong> Todos los derechos reservados.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                <li class="active"><a href="#control-sidebar-settings-admin" data-toggle="tab" aria-expanded="false"><i class="fa fa-gears"></i></a></li>
                <li class=""><a href="#control-sidebar-operations-admin" data-toggle="tab" aria-expanded="true"><i class="fa fa-wrench"></i></a></li>
                <!--
                <li class=""><a href="#control-sidebar-list-admin" data-toggle="tab" aria-expanded="false"><i class="fa fa-line-chart"></i></a></li>
                -->
            </ul>
            <div class="tab-content">
                <!-- Home tab content -->
                <div class="tab-pane active" id="control-sidebar-settings-admin">
                    <h3 class="control-sidebar-heading">Configuración Básica</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-home bg-blue"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Datos del Comercio</h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/usuarios">
                                <i class="menu-icon fa fa-user-secret bg-blue"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Gestion de Usuarios</h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/configuracion/geografica">
                                <i class="menu-icon fa fa-globe bg-blue"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Contexto Geografico</h4>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->
                </div>
                <div class="tab-pane" id="control-sidebar-operations-admin">
                    <h3 class="control-sidebar-heading">Configuracion Avanzada</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            @if (env('APP_PLAN')==4)
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-user bg-red"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Sucursales</h4>
                                    </div>
                                </a>
                            @endif
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-dollar bg-red"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Cotizacion Dolar</h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-trash bg-red"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Registros Eliminados</h4>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--
                <div class="tab-pane" id="control-sidebar-list-admin">
                    <h3 class="control-sidebar-heading">Analisis de Ventas</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-user-secret bg-purple"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Segun Clientes</h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-barcode bg-purple"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Segun Productos</h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-hourglass-start bg-purple"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Gestion de Tiempo</h4>
                                </div>
                            </a>
                        </li>
                    </ul>

                    <h3 class="control-sidebar-heading">Situacion Comercial / Financiera</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-balance-scale bg-red-active"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Ingresos vs Egresos</h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-thumbs-o-down bg-red-active"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Cta Cte Vencida</h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-money bg-red-active"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Cheques en Cartera</h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-dollar bg-red-active"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Flujo de Efectivo</h4>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                -->
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
    <script type="application/javascript">
        var dolar = Number($('#cotcambio').text().replace('$ ', ''));
        var descreal = 0;

        $('#newtit').on('submit', function(e) {
            $('.overlay').removeClass('d-none');
            e.preventDefault();
            $.ajax({
                type: "POST",
                url:'/ajax/titulares/store',
                data: $(this).serialize(),
                success: function(data) {
                    iziToast.success({
                        timeout: 2000,
                        position:'topRight',
                        title: 'Listo!',
                        message: data.msg
                    });
                    document.getElementById('newtit').reset();
                    $('.overlay').addClass('d-none');

                    var newOption = new Option(data.text, data.id, false, false);
                    if ( $("#id_cliente").length > 0 ) {
                        $('#id_cliente').append(newOption).trigger('change');
                    }
                    if ( $("#id_titular").length > 0 ) {
                        $('#id_titular').append(newOption).trigger('change');
                    }
                    $('.overlay').addClass('d-none');
                },
                error:function(data) {
                    console.log(data);
                    iziToast.error({
                        timeout: 3000,
                        position:'center',
                        title: 'Error:',
                        message: 'Hubo un error, Los campos marcados con un (*) son obligatorios'
                    });
                    $('.overlay').addClass('d-none');
                }
            });
        });

        $('#dolarform').on('submit', function(e) {
            $('.overlay').removeClass('d-none');
            e.preventDefault();
            $.ajax({
                type: "POST",
                url:'/ajax/nuevacotizacion/store',
                data: $(this).serialize(),
                success: function(data) {
                    iziToast.success({
                        timeout: 2000,
                        position:'topRight',
                        title: 'Listo!',
                        message: data.msg
                    });
                    $('#cotfecha').text(data.fecha);
                    $('#cotcambio').text('$ ' + data.cambio);
                    dolar = Number(data.cambio);
                    document.getElementById('dolarform').reset();
                    $('.overlay').addClass('d-none');
                },
                error:function(data) {
                    console.log(data);
                    iziToast.error({
                        timeout: 3000,
                        position:'center',
                        title: 'Error:',
                        message: data.responseJSON.message
                    });
                    $('.overlay').addClass('d-none');
                }
            });
        });

        $("#desc1").change(function(){
            var desc1 = Number($('#desc1').val())/100;
            var desc2 = Number($('#desc2').val())/100;
            var desc3 = Number($('#desc3').val())/100;

            descreal = (1 - ((1-desc1)*(1-desc2)*(1-desc3))) * 100;
            $('#descreal').text(descreal.toFixed(2) + ' %');
        })

        $("#desc2").change(function(){
            var desc1 = Number($('#desc1').val())/100;
            var desc2 = Number($('#desc2').val())/100;
            var desc3 = Number($('#desc3').val())/100;

            descreal = (1 - ((1-desc1)*(1-desc2)*(1-desc3))) * 100;
            $('#descreal').text(descreal.toFixed(2) + ' %');
        })

        $("#desc3").change(function(){
            var desc1 = Number($('#desc1').val())/100;
            var desc2 = Number($('#desc2').val())/100;
            var desc3 = Number($('#desc3').val())/100;

            descreal = (1 - ((1-desc1)*(1-desc2)*(1-desc3))) * 100;
            $('#descreal').text(descreal.toFixed(2) + ' %');
        })


        $("#chgclv").click(function(){
            iziToast.question({
                icon:'fa fa-btn fa-unlock-alt',
                color:'#dd4b39',
                rtl: false,
                layout: 1,
                drag: false,
                timeout: false,
                close: true,
                overlay: true,
                displayMode: 1,
                id: 'question',
                progressBar: true,
                title: 'Cambio de Clave',
                message: '',
                position: 'center',
                inputs: [
                    ['<input type="password" name="password" placeholder="Clave" class="col-md-4">'],
                    ['<input type="password" name="password_confirmation" placeholder="Confirme Clave" class="col-md-4">']
                ],
                buttons: [
                    ['<button><i class="fa fa-btn fa-floppy-o"></i> Guardar</button>', function (instance, toast, button, e, inputs) {
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                        $.ajax({
                            type: "GET",
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            url:'/usuarios/cambiodeclave',
                            data : { password: inputs[0].value, password_confirmation: inputs[1].value },
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function(data) {
                                iziToast.success({
                                    timeout: 2000,
                                    position:'topRight',
                                    title: 'Listo!',
                                    message: data.msg
                                });
                            },
                            error:function(data) {
                                console.log(data);
                                iziToast.error({
                                    timeout: 2000,
                                    position:'center',
                                    title: 'Error:',
                                    message: data.responseJSON.errors.password[0]
                                });
                            }
                        });
                    }, false]
                ],
                onClosing: function(instance, toast, closedBy){
                    console.info('Closing | closedBy: ' + closedBy);
                },
                onClosed: function(instance, toast, closedBy){
                    console.info('Closed | closedBy: ' + closedBy);
                }
            });
        });

        $("#sinleer").click(function(){
            $.ajax({
                type: "GET",
                url:'/ajax/notificaciones/leidos',
                success: function(){
                    $('.noleidos').addClass('d-none');
                },
                error:function(data) {
                    console.log(data);
                    iziToast.error({
                        timeout: 2000,
                        position:'center',
                        title: 'Error:',
                        message: data.responseJSON.message
                    });
                }
            });
        });
    </script>
@stop
