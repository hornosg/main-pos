@extends('adminlte::page')
@section('title', 'MainPOS')

@section('content_header')
@include('layouts.inventarionavbar')
@stop

@section('content')
<div class="container" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-4">
        <div class="small-box bg-orange-active">
            <div class="inner">
                <h3>{{$cantidad}}</h3>
                <p>Unidades en Stock</p>
            </div>
            <div class="icon">
                <i class="fa fa-cubes"></i>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="small-box bg-orange-active">
            <div class="inner">
                <h3>$ <span class="tcosto"></span></h3>
                <p>Segun Precio de Costo</p>
            </div>
            <div class="icon">
                <i class="fa fa-dollar"></i>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="small-box bg-orange-active">
            <div class="inner">
                <h3>$ <span class="tpventa"></span></h3>
                <p>Segun Precio de Venta</p>
            </div>
            <div class="icon">
                <i class="fa fa-dollar"></i>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Distribucion de Stock (Cantidad x Costo)</h3>
            </div>
            <div id="box-body pb-10">
                <div class="row">
                    <div id="chart_rubros"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop


@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<!-- Bootstrap-Table -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    var totalcosto = <?php echo $costo; ?>;
    document.querySelector('.tcosto').innerHTML = totalcosto.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
    var totalpventa = <?php echo $pventa; ?>;
    document.querySelector('.tpventa').innerHTML = totalpventa.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
    var $datarubros;

    google.charts.load('current', {'packages':['treemap']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        $datarubros = google.visualization.arrayToDataTable(<?php echo json_encode($rubros); ?>);
        tree = new google.visualization.TreeMap(document.getElementById('chart_rubros'));
        tree.draw($datarubros, {
            minColor: '#001F3F',
            midColor: '#00c0ef',
            maxColor: '#605ca8',
            headerHeight: 30,
            fontColor: 'white',
            height: 320,
            maxDepth:1,
            generateTooltip: showFullTooltip
        });
    };

    function showFullTooltip(row, size, value) {
        return '<div style="background:#fd9; padding:10px; border-style:solid">' +
            '<span style="font-family:Courier"><b>'+
            'Unidades pór Precio de Costo: $ '+$datarubros.getValue(row, 2)+
            '</b></span> </div>';
    }

    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }
</script>

@stop