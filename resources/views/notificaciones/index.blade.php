@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.notificacionesnavbar')
@stop

@section('content')
<div class="container">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <button id="btn-marcar" class="btn btn-success"  title="Marcar Seleccionados">
                                    <i class="fa fa-btn fa-check text-white"></i>
                                </button>
                                <button id="btn-desmarcar" class="btn btn-danger"  title="Desmarcar Seleccionados">
                                    <i class="fa fa-btn fa-close text-white"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-unique-id='id'>

                            <thead class='thead-inverse'>
                                <th data-checkbox='true'></th>
                                <th data-field='id' data-align='right' class='hidden'>Id</th>
                                <th data-field='creado' data-align='right'  class='text-uppercase'>Creado</th>
                                <th data-field='descripcion' data-formatter='DescripcionFormatter' class=''>Descripcion</th>
                                <th data-field='motivo' class='text-uppercase'>Motivo</th>
                                <th data-field='estado' class='text-uppercase'>Estado</th>
                                <th data-formatter='FunctionsFormatter'>LEIDO</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer ptb-10">
                <div class="pull-right pr-5">
                    <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script language='JavaScript' type='text/javascript'>
    $('.noti-icon').addClass('d-none');
    var datos = <?php echo json_encode($items); ?>;
    $(document.body).addClass('sidebar-collapse');
    var $table = $('#table1');
    $table.bootstrapTable('showLoading');
    $table.bootstrapTable({
        data: datos,
        exportDataType:'all',
        exportOptions:{fileName: 'items'}
    });
    $table.bootstrapTable('hideLoading');

    function FunctionsFormatter(value, row, index) {
        console.log(row);
        if(row.estado=='REVISADO'){
            return "<a class='btn-outline-danger' href='/notificaciones/desmarcar/" + row.id + "'><i class='fa fa-fw fa-close'></i></a>";
        }else{
            return "<a class='btn-outline-success' href='/notificaciones/marcar/"+row.id+"'><i class='fa fa-fw fa-check'></i></a>";
        }

    };

    function DescripcionFormatter(value, row, index){
        return "<i class='"+row.icon+"'></i> "+row.descripcion;

    };
</script>
@include('utils.borrarcpte')
@include('utils.borrarcptes')
@include('utils.statusnotification')
@stop
