@extends('adminlte::master')

@section('title', 'Error Registrado')


<div class="container">
    <h1 class="text-bold"><i class="fa fa-support"></i> Hemos detectado un Error!</h1>
    <p>{{$exception->getMessage()}}</p>
    {{--<p>{{$exception->getTraceAsString()}}</p>--}}
    <h2 >Ya estamos tomando el tema, nos comunicaremos en la brevedad.</h2>
    <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
</div>


