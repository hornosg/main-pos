@extends('adminlte::page')

@section('title', 'Sin Permiso!')

@section('content')
    <div class="container">
        <h1 class="text-bold"><i class="fa fa-user-secret"></i> Lo siento! <small class="text-bold">Acceso Denegado.</small></h1>
    </div>
@stop


@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
@stop