@extends('adminlte::page')
@section('title', 'MainPOS')

@section('content_header')
    <h1>
        Contexto Geográfico
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-gears"></i> Configuracion</a></li>
        <li class="active">Contexto Geografico</li>
    </ol>
@stop

@section('content')
    <div class="row">
    <!-- Left col -->
        <div class="col-md-6 prl-5">
            <div class="col-md-12 prl-5">
                <div class="box box-info">
                    <div id="toolbar1">
                        <div class="form-inline" role="form">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formModalPais" title="Nuevo Pais">
                                <i class="fa fa-btn fa-plus text-white"></i>
                            </button>
                            <span class="text-bold">PAISES</span>
                        </div>
                    </div>
                    <table id='tpaises' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                           data-toolbar="#toolbar1"
                           data-search='true'
                           data-strict-search="false"
                           data-multiple-search='true'
                           data-pagination='true'
                           data-page-size='3'
                           data-sort-name='id'
                           data-sort-order='asc'
                           data-show-export='true'>
                        <thead class='thead-inverse'>
                            <th data-checkbox='true' class='hidden'></th>
                            <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                            <th data-field='descripcion'   class='text-uppercase'>Descripcion</th>
<!--                            <th data-field='activo' data-align='center' class='text-uppercase'>Activo</th>-->
                            <th data-align='center' data-formatter='FunctionsPaisFormatter'></th>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="col-md-12 prl-5">
                <div class="box box-info">
                    <div id="toolbar2">
                        <div class="form-inline" role="form">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formModalProv" title="Nueva Provincia">
                                <i class="fa fa-btn fa-plus text-white"></i>
                            </button>
                            <span class="text-bold text">PROVINCIAS</span>
                        </div>
                    </div>
                    <table id='tprovincias' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                           data-toolbar="#toolbar2"
                           data-search='true'
                           data-strict-search="false"
                           data-multiple-search='true'
                           data-pagination='true'
                           data-page-size='3'
                           data-sort-name='id'
                           data-sort-order='asc'
                           data-show-export='true'>
                        <thead class='thead-inverse'>
                            <th data-checkbox='true' class='hidden'></th>
                            <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                            <th data-field='descripcion'   class='text-uppercase'>Descripcion</th>
                            <th data-field='id_pais'   class='hidden'></th>
                            <th data-field='pais'   class='text-uppercase'>Pais</th>
<!--                            <th data-field='activo' data-align='center' class='text-uppercase'>Activo</th>-->
                            <th data-align='right' class='text-uppercase' data-formatter='FunctionsProvFormatter'></th>
                        </thead>
                    </table>
                    <!-- /.box-footer -->
                </div>
            </div>
        </div>

        <div class="col-md-6 prl-5">
            <div class="col-md-12">
                <div class="box box-info">
                    <div id="toolbar3">
                        <div class="form-inline" role="form">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formModalLoc" title="Nueva Localidad">
                                <i class="fa fa-btn fa-plus text-white"></i>
                            </button>
                            <a href="/localidades/import" class="btn btn-info btn-sm hidden-xs" title="Importar Datos">
                                <i class="fa fa-btn fa-upload text-white"></i>
                            </a>
                            <span class="text-bold">LOCALIDADES</span>
                        </div>
                    </div>
                    <table id='tlocalidades' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                           data-toolbar="#toolbar3"
                           data-search='true'
                           data-strict-search="false"
                           data-multiple-search='true'
                           data-pagination='true'
                           data-page-size=15
                           data-sort-name='id'
                           data-sort-order='asc'
                           data-show-export='true'>
                        <thead class='thead-inverse'>
                            <th data-checkbox='true' class='hidden'></th>
                            <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                            <th data-field='descripcion'   class='text-uppercase'>Descripcion</th>
                            <th data-field='provincia'   class='text-uppercase'>Provincia</th>
<!--                            <th data-field='activo' data-align='center' class='text-uppercase'>Activo</th>-->
                            <th data-align='right' class='text-uppercase' data-formatter='FunctionsLocFormatter'></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Form Modal -->
<div class="modal fade" id="formModalPais" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modalmaster" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nuevo Pais</h4>
            </div>
            <div class="modal-body box">
                <form id="formPais" name="formPais" class="form-horizontal" role="form"">
                <div class="box-body">
                    {{ csrf_field() }}
                    <input id="id" type="hidden" name="id">
                    <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                        <label for="descripcion" class="col-md-3 col-md-3 col-sm-3 control-label">Descripcion</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="descripcion" type="text" class="form-control input-sm text-uppercase col-10" name="descripcion">
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }}">
                        <label for="activo" class="col-md-3 col-md-3 col-sm-3 control-label">Activo</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <button id="guardar" type="submit" name="guardar" class="btn btn-primary minw90">
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                        <button id="modificar" type="button" name="modificar" class="btn btn-primary minw90 d-none" onclick='ModificarPais()'>
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay ">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Form Modal -->
<div class="modal fade" id="formModalProv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modalmaster" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva Provincia</h4>
            </div>
            <div class="modal-body box">
                <form id="formProv" name="formProv" class="form-horizontal" role="form"">
                <div class="box-body">
                    {{ csrf_field() }}
                    <input id="idprov" type="hidden" name="id">
                    <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                        <label for="descripcion" class="col-md-3 col-md-3 col-sm-3 control-label">Descripcion</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="descripcionprov" type="text" class="form-control input-sm text-uppercase col-10" name="descripcion">
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('id_pais') ? ' has-error' : '' }}">
                        <label for="id_pais" class="col-md-3 col-md-3 col-sm-3 control-label" for="paises">Pais</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <select id="id_pais" name="id_pais"  type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <option value=""></option>
                                @foreach($paises as $pais)
                                    <option value="{{$pais->id}}">{{$pais->descripcion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }}">
                        <label for="activo" class="col-md-3 col-md-3 col-sm-3 control-label">Activo</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <button id="guardarprov" type="submit" name="guardar" class="btn btn-primary minw90">
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                        <button id="modificarprov" type="button" name="modificar" class="btn btn-primary minw90 d-none" onclick='ModificarProv()'>
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay ">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Form Modal -->
<div class="modal fade" id="formModalLoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modalmaster" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva Localidad</h4>
            </div>
            <div class="modal-body box">
                <form id="formLoc" name="formLoc" class="form-horizontal" role="form"">
                <div class="box-body">
                    {{ csrf_field() }}
                    <input id="id" type="hidden" name="id">
                    <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                        <label for="descripcion" class="col-md-3 col-md-3 col-sm-3 control-label">Descripcion</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="descripcionloc" type="text" class="form-control input-sm text-uppercase col-10" name="descripcion">
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('id_provincia') ? ' has-error' : '' }}">
                        <label for="id_provincia" class="col-md-3 col-md-3 col-sm-3 control-label" for="provincias">Provincia</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <select id="id_provincia" name="id_provincia"  type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <option value=""></option>
                                @foreach($provincias as $provincia)
                                    <option value="{{$provincia->id}}">{{$provincia->descripcion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }}">
                        <label for="activoloc" class="col-md-3 col-md-3 col-sm-3 control-label">Activo</label>
                        <div class="col-md-7 col-md-7 col-sm-7">
                            <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <button id="guardarloc" type="submit" name="guardar" class="btn btn-primary minw90">
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                        <button id="modificarloc" type="button" name="modificar" class="btn btn-primary minw90 d-none" onclick='ModificarLoc()'>
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            <div class="d-none text-danger overlay ">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

<?php
foreach ($paises as $item) {
    $item->activo = $item->activo == 1 ? 'si':'no';
}
foreach ($provincias as $item) {
    $item->activo = $item->activo == 1 ? 'si':'no';
}
foreach ($localidades as $item) {
    $item->activo = $item->activo == 1 ? 'si':'no';
}
?>

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<!-- Bootstrap-Table -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script language="javascript" type="text/javascript">
    item = {};
    $(function () {
        var data1 =  <?php echo json_encode($paises); ?>;
        $('#tpaises').bootstrapTable({data: data1});
        var data2 =  <?php echo json_encode($provincias); ?>;
        $('#tprovincias').bootstrapTable({data: data2});
        var data3 =  <?php echo json_encode($localidades); ?>;
        $('#tlocalidades').bootstrapTable({data: data3});
    });

    function FunctionsPaisFormatter(value, row, index) {
        return  "<a class='btn-outline-danger hidden-xs' href='#' onclick='BorrarPais("+index+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
                "<a class='btn-outline-warning' href='#' onclick='EditarPais("+index+")' data-toggle='modal' data-target='#formModalPais' title='Modificar'><i class='fa fa-fw fa-pencil'></i></a>";
    };
    function FunctionsProvFormatter(value, row, index) {
        return  "<a class='btn-outline-danger hidden-xs' href='#' onclick='BorrarProv("+index+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
            "<a class='btn-outline-warning' href='#' onclick='EditarProv("+index+")' data-toggle='modal' data-target='#formModalProv' title='Modificar'><i class='fa fa-fw fa-pencil'></i></a>";
    };
    function FunctionsLocFormatter(value, row, index) {
        return  "<a class='btn-outline-danger hidden-xs' href='#' onclick='BorrarLoc("+index+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
            "<a class='btn-outline-warning' href='#' onclick='EditarLoc("+index+")' data-toggle='modal' data-target='#formModalLoc' title='Modificar'><i class='fa fa-fw fa-pencil'></i></a>";
    };
    
    function BorrarPais(index){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $('#tpaises').bootstrapTable('check', index);
                    var ids = $.map($('#tpaises').bootstrapTable('getSelections'), function (row) {
                        return row.id;
                    });
                    $.ajax({
                        type: "GET",
                        url:'/paises/delete/'+ids,
                        data: $('#formPais').serialize(),
                        success: function(data) {
                            $('#tpaises').bootstrapTable('remove', {
                                field: 'id',
                                values: ids
                            });
                            $('#tpaises').bootstrapTable('uncheckAll');
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    $('#formPais').on('submit', function(e) {
        $('.overlay').removeClass('d-none');
        e.preventDefault();
        $.ajax({
            type: "POST",
            url:'/paises/store',
            data: $('#formPais').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                $('#formModalPais').modal('hide');
                $('#tpaises').bootstrapTable('append', data);
                document.getElementById('formPais').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            },
            error: function(data){
                var errors = data.responseJSON;
                if (errors.errors.descripcion){
                    $('#descripcion').parents(".form-group").addClass('has-error');
                }
                $('.overlay').addClass('d-none');
            }
        });
    })

    function EditarPais(index){
        $('#tpaises').bootstrapTable('check', index);
        $row = $('#tpaises').bootstrapTable('getSelections');
        item.index = index;
        item.id = $row[0]['id'];
        item.descripcion = $row[0]['descripcion'];
        item.activo = $row[0]['activo'];
        $('#tpaises').bootstrapTable('uncheckAll');
        $('#descripcion').attr('readonly', false);
        $('#activo').attr('disabled', false);
    }

    $('#formModalPais').on('shown.bs.modal', function (e) {
        if (item.index >= 0 ){
            $('#descripcion').val(item.descripcion);
            if (item.activo=='no'){
                //alert(item.activo);
                $('.toggle').addClass('btn-default off');
            }
            $( '.modal-title' ).text( 'Modificar Pais' );
            if (item.disable){
                $('#modificar').addClass('d-none');
                $('#guardar').addClass('d-none');
            }else{
                $('#modificar').removeClass('d-none');
                $('#guardar').addClass('d-none');
            }

        }else{
            $('#descripcion').attr('readonly', false);
            $('#activo').attr('disabled', false);
            $('.toggle').removeClass('btn-default off');
            $('.toggle').addClass('btn-primary');
            $('#guardar').removeClass('d-none');
            $('#modificar').addClass('d-none');
        }
    })

    $('#formModalPais').on('hide.bs.modal', function (e) {
        document.getElementById('formPais').reset();
        item.index=undefined;
        $('.off').removeClass('off');
        item.disable=false;
        $('#descripcion').parents(".form-group").removeClass('has-error');
    })

    function ModificarPais(){
        $('.overlay').removeClass('d-none');
        $.ajax('/paises/update/'+item.id, {
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: $('#formPais').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });

                activo = $('.off').val() == '' ? 'no':'si';
                $('#tpaises').bootstrapTable('updateRow', {
                    index: item.index,
                    row: {
                        descripcion: $('#descripcion').val(),
                        activo:activo
                    }
                });
                $('#formModalPais').modal('toggle');
                document.getElementById('formPais').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            },
            error: function(data){
                var errors = data.responseJSON;
                if (errors.errors.descripcion){
                    $('#descripcion').parents(".form-group").addClass('has-error');
                }
                $('.overlay').addClass('d-none');
            }
        });

    }

    function BorrarProv(index){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $('#tprovincias').bootstrapTable('check', index);
                    var ids = $.map($('#tprovincias').bootstrapTable('getSelections'), function (row) {
                        return row.id;
                    });
                    $.ajax({
                        type: "GET",
                        url:'/provincias/delete/'+ids,
                        data: $('#formProv').serialize(),
                        success: function(data) {
                            $('#tprovincias').bootstrapTable('remove', {
                                field: 'id',
                                values: ids
                            });
                            $('#tprovincias').bootstrapTable('uncheckAll');
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    $('#formProv').on('submit', function(e) {
        $('.overlay').removeClass('d-none');
        e.preventDefault();
        $.ajax({
            type: "POST",
            url:'/provincias/store',
            data: $('#formProv').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                $('#formModalProv').modal('hide');
                $('#tprovincias').bootstrapTable('append', data);
                document.getElementById('formProv').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            }
        });
    })

    function EditarProv(index){
        $('#tprovincias').bootstrapTable('check', index);
        $row = $('#tprovincias').bootstrapTable('getSelections');
        item.index = index;
        item.id = $row[0]['id'];
        item.descripcion = $row[0]['descripcion'];
        item.id_pais = $row[0]['id_pais'];
        item.pais = $row[0]['pais'];
        item.activo = $row[0]['activo'];
        $('#tprovincias').bootstrapTable('uncheckAll');
        $('#descripcion').attr('readonly', false);
        $('#activo').attr('disabled', false);
    }

    $('#formModalProv').on('shown.bs.modal', function (e) {
        if (item.index >= 0 ){
            $('#descripcionprov').val(item.descripcion);
            $("#id_pais option[value="+item.id_pais+"]").attr('selected', 'selected');
            if (item.activo=='no'){
                $('.toggle').addClass('btn-default off');
            }
            $( '.modal-title' ).text( 'Modificar Provincia' );
            if (item.disable){
                $('#modificarprov').addClass('d-none');
                $('#guardarprov').addClass('d-none');
            }else{
                $('#modificarprov').removeClass('d-none');
                $('#guardarprov').addClass('d-none');
            }
        }else{
            $('#descripcion').attr('readonly', false);
            $('#activo').attr('disabled', false);
            $('.toggle').removeClass('btn-default off');
            $('.toggle').addClass('btn-primary');
            $('#guardarprov').removeClass('d-none');
            $('#modificarprov').addClass('d-none');
        }
    })

    $('#formModalProv').on('hide.bs.modal', function (e) {
        document.getElementById('formProv').reset();
        item.index=undefined;
        $('.off').removeClass('off');
        item.disable=false;
    })

    function ModificarProv(){
        $('.overlay').removeClass('d-none');
        $.ajax('/provincias/update/'+item.id, {
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: $('#formProv').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });

                activo = $('.off').val() == '' ? 'no':'si';
                $('#tprovincias').bootstrapTable('updateRow', {
                    index: item.index,
                    row: {
                        descripcion: data.descripcion,
                        pais: data.pais,
                        activo:data.activo
                    }
                });
                $('#formModalProv').modal('toggle');
                document.getElementById('formProv').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            }
        });
    }

    function BorrarLoc(index){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $('#tlocalidades').bootstrapTable('check', index);
                    var ids = $.map($('#tlocalidades').bootstrapTable('getSelections'), function (row) {
                        return row.id;
                    });
                    $.ajax({
                        type: "GET",
                        url:'/localidades/delete/'+ids,
                        data: $('#formLoc').serialize(),
                        success: function(data) {
                            $('#tlocalidades').bootstrapTable('remove', {
                                field: 'id',
                                values: ids
                            });
                            $('#tlocalidades').bootstrapTable('uncheckAll');
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    $('#formLoc').on('submit', function(e) {
        $('.overlay').removeClass('d-none');
        e.preventDefault();
        $.ajax({
            type: "POST",
            url:'/localidades/store',
            data: $('#formLoc').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                $('#formModalLoc').modal('hide');
                $('#tlocalidades').bootstrapTable('append', data);
                document.getElementById('formLoc').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            }
        });
    })

    function EditarLoc(index){
        $('#tlocalidades').bootstrapTable('check', index);
        $row = $('#tlocalidades').bootstrapTable('getSelections');
        item.index = index;
        item.id = $row[0]['id'];
        item.descripcion = $row[0]['descripcion'];
        item.id_provincia = $row[0]['id_provincia'];
        item.provincia = $row[0]['pais'];
        item.activo = $row[0]['activo'];
        $('#tlocalidades').bootstrapTable('uncheckAll');
        $('#descripcion').attr('readonly', false);
        $('#activo').attr('disabled', false);
    }

    $('#formModalLoc').on('shown.bs.modal', function (e) {
        if (item.index >= 0 ){
            $('#descripcionloc').val(item.descripcion);
            $("#id_provincia option[value="+item.id_provincia+"]").attr('selected', 'selected');
            if (item.activo=='no'){
                $('.toggle').addClass('btn-default off');
            }
            $( '.modal-title' ).text( 'Modificar Localidad' );
            if (item.disable){
                $('#modificarloc').addClass('d-none');
                $('#guardarloc').addClass('d-none');
            }else{
                $('#modificarloc').removeClass('d-none');
                $('#guardarloc').addClass('d-none');
            }
        }else{
            $('#descripcionloc').attr('readonly', false);
            $('#id_provincia').attr('disabled', false);
            $('#activoloc').attr('disabled', false);
            $('.toggle').removeClass('btn-default off');
            $('.toggle').addClass('btn-primary');
            $('#guardarloc').removeClass('d-none');
            $('#modificarloc').addClass('d-none');
        }
    })

    $('#formModalLoc').on('hide.bs.modal', function (e) {
        document.getElementById('formLoc').reset();
        item.index=undefined;
        $('.off').removeClass('off');
        item.disable=false;
    })

    function ModificarLoc(){
        $('.overlay').removeClass('d-none');
        $.ajax('/localidades/update/'+item.id, {
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: $('#formLoc').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });

                activo = $('.off').val() == '' ? 'no':'si';
                $('#tlocalidades').bootstrapTable('updateRow', {
                    index: item.index,
                    row: {
                        descripcion: data.descripcion,
                        id_provincia: data.id_provincia,
                        provincia: data.provincia,
                        activo:data.activo
                    }
                });
                $('#formModalLoc').modal('toggle');
                document.getElementById('formLoc').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            }
        });
    }
</script>
@stop