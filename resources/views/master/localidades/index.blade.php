@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
<h1>{{$titulo}}</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-sliders"></i> Configuracion</a></li>
    <li class="active">Geografica</li>
    <li class="active">{{$titulo}}</li>
</ol>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <a href="/{{$urlkey}}/create" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus text-primary"></i> Nuevo
                                </a>
                                <button id="btn-eliminar" class="btn btn-default">
                                    <i class="fa fa-btn fa-trash text-danger"></i> Eliminar Seleccion
                                </button>
                                <a href="/{{$urlkey}}/import" class="btn btn-default">
                                    <i class="fa fa-btn fa-upload text-success"></i> Importar Datos
                                </a>
                            </div>
                        </div>
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-pagination='true'
                               data-page-size=15
                               data-pagination-first-text='Primera'
                               data-pagination-pre-text='Ant.'
                               data-pagination-next-text='Sig.'
                               data-pagination-last-text='Ultima'
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-show-columns='true'
                               data-show-export='true'
                               data-filter-control='true'
                               data-filter-show-clear='true'>

                            <thead class='thead-inverse'>
                                <th data-checkbox='true'></th>
                                <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                                <th data-field='descripcion'   class='text-uppercase'>Descripcion</th>
                                <th data-field='provincia'   class='text-uppercase'>Provincia</th>
                                <th data-field='activo' data-align='center' class='text-uppercase'>Activo</th>
                                <th data-align='right' class='text-uppercase' data-formatter='FunctionsFormatter'>FUNCTIONS</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
foreach ($localidades as $item) {
    $item->activo = $item->activo == 1 ? 'si':'no';
}
?>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-filter.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script language='JavaScript' type='text/javascript'>
    var datos = <?php echo json_encode($localidades); ?>;
    console.log(datos);
    $('#table1').bootstrapTable({data: datos});

    function FunctionsFormatter(value, row, index) {
        return "<a class='text-lowercase' href='/{{$urlkey}}/delete/"+row.id+"'><i class='glyphicon glyphicon-trash text-danger'></i></a>  | <a class='text-lowercase' href='/{{$urlkey}}/edit/"+row.id+"'><i class='glyphicon glyphicon-pencil text-primary'></i></a>  | <a class='text-lowercase' href='/{{$urlkey}}/show/"+row.id+"'><i class='glyphicon glyphicon-search text-info'></i></a>";
    };

    $(window).on('load', function(event){
        var windowSize = $(window).width(); // Could've done $(this).width()
        if(windowSize < 350){
            //Add your javascript for larger screens here
            $('#table1').bootstrapTable('toggleView');
        }
    });

    $(window).on('resize', function(event){
        var windowSize = $(window).width(); // Could've done $(this).width()
        if(windowSize < 350){
            //Add your javascript for larger screens here
            $('#table1').bootstrapTable('toggleView');
        }
    });

    $(function () {
        $('#btn-eliminar').click(function (e) {
            e.preventDefault();
            var choice;
            bootbox.confirm("Se van a ELIMINAR todos los registros seleccionados", function(result){
                console.log('This was logged in the callback: ' + result);
                if (result) {
                    var ids = $.map($('#table1').bootstrapTable('getAllSelections'), function (row) {
                        return row.id;
                    });
                    $('#table1').bootstrapTable('remove', {
                        field: 'id',
                        values: ids
                    });

                    $.ajax('/{{$urlkey}}/removeselected', {
                        type: 'POST',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {datos:ids}
                    });

                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="icon fa fa-check text-success"></i> Registros Eliminados</p>',
                        closeButton: false
                    });
                    dialog.init(function(){
                        setTimeout(function(){
                            dialog.modal('hide');
                        }, 2000);
                    });
                }
            });
        });
    });
</script>
@include('utils.statusnotification')
@stop
