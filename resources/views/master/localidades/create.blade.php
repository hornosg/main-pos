@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
<h1>Nuevo</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-sliders"></i> Configuracion</a></li>
    <li class="active">Geografica</li>
    <li class="active">{{$titulo}}</li>
    <li class="active">Nuevo</li>
</ol>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <form class="form-horizontal" role="form" method="POST" action="/{{$urlkey}}/store">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label for="descripcion" class="col-md-4 control-label">Descripcion</label>

                        <div class="col-md-6">
                            <input id="descripcion" type="text" class="form-control text-uppercase" name="descripcion" value="{{ old('descripcion') }}" autofocus>

                            @if ($errors->has('descripcion'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('descripcion') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="id_provincia" class="col-md-4 control-label" for="provincias">Provincia</label>
                        <div class="col-md-6">
                            <select id="id_provincia" name="id_provincia"  type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <option value=""></option>
                                @foreach($provincias as $provincia)
                                <option value="{{$provincia->id}}">{{$provincia->descripcion}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('id_provincia'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_provincia') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('activo') ? ' has-error' : '' }}">
                        <label for="activo" class="col-md-4 control-label">Activo</label>

                        <div class="col-md-6">
                            <input id="activo" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>

                            @if ($errors->has('activo'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('activo') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a class="btn btn-default" href="/{{$urlkey}}"><i class="fa fa-btn fa-arrow-left text-danger"></i> Cancel</a>
                        <button type="submit" name="action" value="savecontinue" class="btn btn-default">
                            <i class="fa fa-btn fa-floppy-o"></i> Guardar & <i class="fa fa-btn fa-repeat"></i> Continuar</a>
                        </button>
                        <button type="submit" name="action" value="save" class="btn btn-default">
                            <i class="fa fa-btn fa-floppy-o text-primary"></i> Guardar</a>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script language='JavaScript' type='text/javascript'>
    $(window).on('load', function(event){
        document.getElementById("descripcion").focus();
        //-----------------------------------------------------------------------------------------------------
    });

    $(function() {
        $('#activo').bootstrapToggle();
    })
</script>
@include('utils.statusnotification')
@stop
