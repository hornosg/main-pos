@extends('adminlte::page')

@section('title', $titulo)

@php
    if (empty($instock)){
        $instock='';
    }
@endphp

@section('content_header')
    @include('layouts.catalognavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12 col-md-8">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <button id="addnew" type="button" class="btn btn-primary" data-toggle="modal" data-target="#formModal" title="Nuevo">
                                    <i class="fa fa-btn fa-plus text-white"></i>
                                </button>
                                <button id="btn-eliminar" class="btn btn-danger hidden-xs" title="Eliminar Items Seleccionados">
                                    <i class="fa fa-btn fa-trash text-white"></i>
                                </button>
                                <a href="/{{$urlkey}}/import" class="btn btn-info hidden-xs" title="Importar Datos">
                                    <i class="fa fa-btn fa-upload text-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive dataTable'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-pagination='true'
                               data-page-size='10'
                               data-pagination-first-text='Primera'
                               data-pagination-pre-text='Ant.'
                               data-pagination-next-text='Sig.'
                               data-pagination-last-text='Ultima'
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-show-export='true'
                               data-filter-control='true'
                               data-filter-show-clear='true'>

                            <thead class='thead-inverse'>
                                <th data-checkbox='true' class='col-md-1'></th>
                                <th data-field='id' data-align='right' class='text-uppercase col-md-1'>Id</th>
                                <th data-field='descripcion' data-editable="true" class='text-uppercase col-md-5'>Descripcion</th>
                                <th data-field='activo'      data-editable="true" data-align='center' class='text-uppercase col-md-2'>Activo</th>
                                <th data-align='center'      data-formatter='FunctionsFormatter'></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
    if (!empty($instock)){
        echo '<div class="col-xs-12 col-md-4">
                <div class="box box-default">
                    <div class="box-header">
                        <h3 class="box-title">Top 10 en Stock</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div id="chart_stock"></div>
                        </div>
                    </div>
                </div>
            </div>';
    }
    @endphp
</div>

<!-- Form Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modalmaster" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body box">
                <form id="form1" name="form1" class="form-horizontal" role="form">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <input id="id" type="hidden" name="id">
                        <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                            <label for="descripcion" class="col-md-3 col-md-3 col-sm-3 control-label">Descripcion</label>
                            <div class="col-md-7 col-md-7 col-sm-7">
                                <input id="descripcion" type="text" class="form-control input-sm text-uppercase col-10" name="descripcion">
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }}">
                            <label for="activo" class="col-md-3 col-md-3 col-sm-3 control-label">Activo</label>
                            <div class="col-md-7 col-md-7 col-sm-7">
                                <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer pb-10 no-bt">
                        <div class="pull-right">
                            <button id="guardar" type="submit" name="guardar" class="btn btn-primary minw90">
                                <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                                <div class="d-none text-danger overlay">
                                    <i class="fa fa-refresh fa-spin"></i>
                                </div>
                            </button>
                            <button id="modificar" type="button" name="modificar" class="btn btn-primary minw90 d-none" onclick='Modificar()'>
                                <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                                <div class="d-none text-danger overlay ">
                                    <i class="fa fa-refresh fa-spin"></i>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
foreach ($items as $item) {
    $item->activo = $item->activo == 1 ? 'si':'no';
}
?>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/css/bootstrap-editable.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-filter.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<!-- Morris Chart JS-->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script language='JavaScript' type='text/javascript'>
    var datos = <?php echo json_encode($items); ?>,
        item = {};
    $(document.body).addClass('sidebar-collapse');
    $('#table1').bootstrapTable({data: datos});

    function FunctionsFormatter(value, row, index) {
        return  "<a class='btn-outline-danger hidden-xs' href='#' onclick='Borrar("+index+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
                "<a class='btn-outline-warning' href='#' onclick='Editar("+index+")' data-toggle='modal' data-target='#formModal' title='Modificar'><i class='fa fa-fw fa-pencil'></i></a>" +
                "<a class='btn-outline-primary' href='#' onclick='Visualizar("+index+")' data-toggle='modal' data-target='#formModal' title='Visualizar'><i class='fa fa-fw fa-search'></i></a>";
    };

    var datos2 = <?php echo json_encode($instock); ?>;
    if (datos2.length > 1 ) {
        new Morris.Donut({
            element: 'chart_stock',
            data: datos2
        });
    }

    function Borrar(index){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $('#table1').bootstrapTable('check', index);
                    var ids = $.map($('#table1').bootstrapTable('getSelections'), function (row) {
                        return row.id;
                    });
                    $.ajax({
                        type: "GET",
                        url:'/{{$urlkey}}/delete/'+ids,
                        data: $(this).serialize(),
                        success: function(data) {
                            $('#table1').bootstrapTable('remove', {
                                field: 'id',
                                values: ids
                            });
                            $('#table1').bootstrapTable('uncheckAll');
                            iziToast.success({
                                timeout: 2000,
                                position:'topRight',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    $(document).on('click', '#addnew', function() {
        $('.modal-title').text('Cargar {{$titulo}}');
    });

    $('#form1').on('submit', function(e) {
        $('.overlay').removeClass('d-none');
        e.preventDefault();
        $.ajax({
            type: "POST",
            url:'/{{$urlkey}}/store',
            data: $(this).serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                $('#formModal').modal('hide');
                $('#table1').bootstrapTable('append', data);
                document.getElementById('form1').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            }
        });
    });

    function Editar(index){
        $('#table1').bootstrapTable('check', index);
        $row = $('#table1').bootstrapTable('getSelections');
        item.index = index;
        item.id = $row[0]['id'];
        item.descripcion = $row[0]['descripcion'];
        item.activo = $row[0]['activo'];
        $('#table1').bootstrapTable('uncheckAll');
        $('#descripcion').attr('readonly', false);
        $('#activo').attr('disabled', false);
    }


    function Visualizar(index){
        $('#table1').bootstrapTable('check', index);
        $row = $('#table1').bootstrapTable('getSelections');
        item.index = index;
        item.id = $row[0]['id'];
        item.descripcion = $row[0]['descripcion'];
        item.activo = $row[0]['activo'];
        item.disable = true;
        $('#table1').bootstrapTable('uncheckAll');
        $('#descripcion').attr('readonly', true);
        $('#activo').attr('disabled', true);
        $('#modificar').addClass('d-none');
        $('#guardar').addClass('d-none');
    }

    function Modificar(){
        $('.overlay').removeClass('d-none');
        $.ajax('/{{$urlkey}}/update/'+item.id, {
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: $('#form1').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });

                activo = $('.off').val() == '' ? 'no':'si';
                $('#table1').bootstrapTable('updateRow', {
                    index: item.index,
                    row: {
                        descripcion: $('#descripcion').val(),
                        activo:activo
                    }
                });
                $('#formModal').modal('toggle');
                document.getElementById('form1').reset();
                item.index=undefined;
                $('.overlay').addClass('d-none');
            }
        });

    }

    $('#formModal').on('shown.bs.modal', function (e) {
        if (item.index >= 0 ){
            $('#descripcion').val(item.descripcion);
            if (item.activo=='no'){
                $('.toggle').addClass('btn-default off');
            }
            $( '.modal-title' ).text( 'Modificar {{$titulo}}' );
            if (item.disable){
                $('#modificar').addClass('d-none');
                $('#guardar').addClass('d-none');
            }else{
                $('#modificar').removeClass('d-none');
                $('#guardar').addClass('d-none');
            }

        }else{
            $('#descripcion').attr('readonly', false);
            $('#activo').attr('disabled', false);
            $('.toggle').removeClass('btn-default off');
            $('.toggle').addClass('btn-primary');
            $('#guardar').removeClass('d-none');
            $('#modificar').addClass('d-none');
        }
    });

    $('#formModal').on('hide.bs.modal', function (e) {
        document.getElementById('form1').reset();
        item.index=undefined;
        $('.off').removeClass('off');
        item.disable=false;
    });


    $(function () {
        $('#btn-eliminar').click(function (e) {
            e.preventDefault();
            var choice;
            iziToast.question({
                close: true,
                overlay: true,
                displayMode: 'once',
                id: 'question',
                zindex: 999,
                title: 'Atencion!',
                message: '¿Está seguro de Eliminar todos los items seleccionados?',
                position: 'center',
                buttons: [
                    ['<button><b>SI</b></button>', function (instance, toast) {
                        var ids = $.map($('#table1').bootstrapTable('getAllSelections'), function (row) {
                            return row.id;
                        });
                        $('#table1').bootstrapTable('remove', {
                            field: 'id',
                            values: ids
                        });

                        $.ajax('/{{$urlkey}}/removeselected', {
                            type: 'POST',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: {datos:ids}
                        });

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        iziToast.success({
                            timeout: 2000,
                            position:'topRight',
                            title: 'Listo!',
                            message: 'Items, Eliminados.'
                        });
                    }, true],
                    ['<button>NO</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    }]
                ]
            });
        });
    });
</script>
@include('utils.statusnotification')
@stop
