@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
<h1>Visualizar</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-sliders"></i> Configuracion</a></li>
    <li class="active">Geografica</li>
    <li class="active">{{$titulo}}</li>
    <li class="active">Visualizar</li>
</ol>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <form class="form-horizontal" role="form" method="" action="">
                    {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label for="descripcion" class="col-md-4 control-label">Descripcion</label>
                        <div class="col-md-6">
                            <input id="descripcion" type="text" class="form-control text-uppercase" name="name" value="{{ $provincia->descripcion }}" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="id_pais" class="col-md-4 control-label" for="provincias">Pais</label>
                        <div class="col-md-6">
                            <input id="pais" type="text" class="form-control text-uppercase" name="name" value="{{$provincia->descripcion_pais}}" readonly>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('enable') ? ' has-error' : '' }}">
                        <label for="activo" class="col-md-4 control-label">Activo</label>

                        <div class="col-md-6">
                            @if ($provincia->activo == 1)
                            <input id="activo" type="checkbox" Data-toggle="toggle" data-on="true" data-off="false" name="activo" checked disabled>
                            @else
                            <input id="activo" type="checkbox" Data-toggle="toggle" data-on="true" data-off="false" name="activo" disabled>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a class="btn btn-default" href="/provincias"><i class="fa fa-btn fa-arrow-left text-danger"></i> Volver</a>
                        <a class="btn btn-default" href="/provincias/edit/{{$provincia->id}}"><i class="fa fa-btn fa-edit text-primary"></i> Modificar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script language='JavaScript' type='text/javascript'>
    $(function() {
        $('#activo').bootstrapToggle();
    })
</script>
@include('utils.statusnotification')
@stop


