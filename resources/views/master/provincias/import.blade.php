@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
<h1>Importar</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-sliders"></i> Configuracion</a></li>
    <li class="active">Geografica</li>
    <li class="active">{{$titulo}}</li>
    <li class="active">Importar</li>
</ol>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12 col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="thumbnail">
                        <h3><span class="label label-default">1</span> Descargar Planilla</h3>
                        <p>Haga click sobre el siguiente link <a href="/files/ImportLocalidades.xlsx" download>&nbsp;[ <i class="fa fa-btn fa-download"></i> Descargar ]</a> para bajarse la planilla con el formato necesario para importar productos. </p>
                    </div>
                </div>
            </div>
        </div><!--/.col-xs-6.col-md-4-->
        <div class="col-xs-6 col-md-6">
            <div class="box">
                <div class="box-body">
                    <div class="thumbnail">
                        <img id="importarxls" src="/img/importprov.png" alt="...">
                        <h3><span class="label label-default">2</span> Completar Planilla</h3>
                        <p>Complete la planilla sin modificar ningun nombre de la fila 1 del documento.</p>
                    </div>
                </div>
            </div>
        </div><!--/.col-xs-6.col-md-4-->
        <div class="col-xs-6 col-md-6">
            <div class="box">
                <div class="box-body">
                    <div class="thumbnail">
<!--                        <img src="/img/ImportarDatos3.GIF" alt="...">-->
                        <h3><span class="label label-default">3</span> Go! Importar Datos</h3>
                        <p>Luego de seleccionar el archivo, y que se vea el nombre del mismo en el formulario precionar el bot&oacute;n <strong><i class="fa fa-btn fa-upload"></i> Importar</strong>. </p>
                    </div>
                </div>
            </div>
        </div><!--/.col-xs-6.col-md-4-->
        <div class="row">
            <div class="col-xs-6 col-md-6">
                <div class="box">
                    <div class="thumbnail">
                        <div id="box-header">
                            <h3>Importar {{$titulo}}</h3>
                        </div>
                        <form class="form-horizontal" role="form" method="POST" action="/{{$urlkey}}/importExcel" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input type="file" name="import_file" class="file" />

                                        @if ($errors->has('import_file'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('import_file') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="pull-right">
                                    <a class="btn btn-default" href="/{{$urlkey}}"><i class="fa fa-btn fa-arrow-left text-danger"></i> Cancel</a>
                                    <button class="btn btn-default">
                                        <i class="fa fa-btn fa-upload text-success"></i> Importar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link href="/css/mainpos.css" rel="stylesheet">-->
@stop

@section('js')
<!--<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>-->
@stop