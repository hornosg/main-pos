@extends('adminlte::page')

@section('title', $titulo)

@php
    if ($edit==0){
        $editable='readonly';
        $visible='d-none';
        $disabled='disabled';
    }else{
        $editable=' ';
        $visible=' ';
        $disabled=' ';
    }
    if (!empty($item)){
        $checked = $item->activo == 1 ? 'checked':'';
    }else{
        $checked = 'checked';
    }
@endphp

@section('content_header')
    @include('layouts.preciosnavbar')
@stop

@section('content')
<div id="content" class="container-fluid row">
    <form id="form1" role="form" method="POST" action="/{{$urlkey}}/store">
        <div class="row col-md-8 col-md-offset-2">
            <div class="box box-primary">
                {{ csrf_field() }}
                <div class="box-header">
                    <h3 class="box-title">
                        <strong>{{$tituloform}}</strong>
                    </h3>
                </div>
                <div class="box-body">
                    @if (!empty($item))
                        <input type = "hidden" name = "id" value = "{{$item->id}}">
                    @else
                        <input type = "hidden" name = "id">
                    @endif
                    <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-2 col-md-6">
                        <label for="descripcion" class="control-label">Descripcion</label>
                        @if (!empty($item))
                            <input id="descripcion" type="text" class="form-control input-sm text-uppercase" name="descripcion" value="{{ $item->descripcion }}" {{$editable}}>
                        @else
                            <input id="descripcion" type="text" class="form-control input-sm text-uppercase" name="descripcion" value="" {{$editable}}>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <label for="id_rubro" class="control-label" for="rubros">Rubro</label>
                        <select id="id_rubro" name="id_rubro"  type="button" class="form-control" {{$disabled}}>
                            <option value=""></option>
                            @if (!empty($item))
                                @foreach(DB::table('conf_rubros')->where('activo', 1)->get() as $rubro)
                                    @if ( $rubro->id == $item->id_rubro )
                                        <option value="{{ $rubro->id }}" selected>{{ $rubro->descripcion }}</option>
                                    @else
                                        <option value="{{$rubro->id}}">{{$rubro->descripcion}}</option>
                                    @endif
                                @endforeach
                            @else
                                @foreach(DB::table('conf_rubros')->where('activo', 1)->get() as $rubro)
                                    <option value="{{$rubro->id}}">{{$rubro->descripcion}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-1 col-md-1 pull-left">
                        <label for="activo" class="control-label">Activo</label>
                        <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" {{$checked}} {{$disabled}}>
                    </div>
                    <div class="clearfix visible-lg-block visible-md-block"></div>
                    @if (!empty($item))
                        <div class="form-group col-sm-6 col-md-4 col-md-3">
                            <label for="created_at" class="control-label">Fecha Alta</label>
                            <input id="created_at" type="text" class="form-control input-sm text-uppercase" name="created_at" readonly value="{{ $item->created_at }}">
                        </div>

                        <div class="form-group col-sm-6 col-md-6 col-md-3">
                            <label for="updated_at" class="control-label">Fecha Ultima Edicion</label>
                            <input id="updated_at" type="text" class="form-control input-sm text-uppercase" name="updated_at" readonly value="{{ $item->updated_at }}">
                        </div>
                    @endif
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        @if ($edit==1)
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                            <button type="submit" name="action" value="save" class="btn btn-primary">
                                <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            </button>
                        @else
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type='text/javascript'>
    $(function () {
        $('[data-toggle="popover"]').popover();
    });
    $('#id_rubro').select2({
        placeholder: "",
        allowClear: true
    });
</script>
@include('utils.statusnotification')
@stop
