@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
@include('layouts.catalognavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <div class="form-group">
                                    <a href="/{{$urlkey}}/create" class="btn btn-primary"  title="Nuevo">
                                        <i class="fa fa-btn fa-plus text-white"></i>
                                    </a>
                                    <button id="btn-eliminar" class="btn btn-danger hidden-xs" title="Eliminar Items Seleccionados">
                                        <i class="fa fa-btn fa-trash text-white"></i>
                                    </button>
                                    <a href="/{{$urlkey}}/import" class="btn btn-info hidden-xs" title="Importar Datos">
                                        <i class="fa fa-btn fa-upload text-white"></i>
                                    </a>
                                </div>
                                <div class="form-group" style="width: 200px;">
                                    <select id="id_rubro" name="id_rubro"  type="button" class="form-control">
                                        <option value=""></option>
                                        @foreach(DB::table('conf_rubros')->where('activo', 1)->get() as $rubro)
                                        <option value="{{$rubro->id}}">{{$rubro->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button id="vincular" type="button" class='btn btn-primary' onclick='VincularItems()' data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>"> <i class="fa fa-btn fa-check text-white"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-pagination-first-text='Primera'
                               data-pagination-pre-text='Ant.'
                               data-pagination-next-text='Sig.'
                               data-pagination-last-text='Ultima'
                               data-sort-name='id'
                               data-sort-order='desc'>

                            <thead class='thead-inverse'>
                            <th data-checkbox='true' class='col-md-1'></th>
                            <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                            <th data-field='descripcion'   class='text-nowrap text-uppercase'>DESCRIPCION</th>
                            <th data-field='rubro'   class='text-nowrap text-uppercase'>RUBRO</th>
                            <th data-field='activo' data-align='center' class='text-uppercase'>ACTIVO</th>
                            <th data-align='center' data-formatter='FunctionsFormatter'></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@php
foreach ($items as $item) {
    $item->activo = $item->activo == 1 ? 'si':'no';
}
@endphp
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
<script language='JavaScript' type='text/javascript'>
    $('#id_rubro').select2({
        placeholder: "Vincular Seleccionados con Rubro",
        allowClear: true
    });
    var datos = <?php echo json_encode($items); ?>;
    $('#table1').bootstrapTable({data: datos,exportDataType:'all',exportOptions:{fileName: 'subrubros'}});

    function FunctionsFormatter(value, row, index) {
        return  "<a class='btn-outline-danger' href='#' onclick='Borrar("+index+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
                "<a class='btn-outline-warning' href='/{{$urlkey}}/edit/"+row.id+"'><i class='fa fa-fw fa-pencil'></i></a>" +
                "<a class='btn-outline-primary' href='/{{$urlkey}}/show/"+row.id+"'><i class='fa fa-fw fa-search'></i></a>";
    };

    function Borrar(index){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $('#table1').bootstrapTable('check', index);
                    var ids = $.map($('#table1').bootstrapTable('getSelections'), function (row) {
                        return row.id;
                    });
                    $.ajax({
                        type: "GET",
                        url:'/{{$urlkey}}/delete/'+ids,
                        data: $(this).serialize(),
                        success: function(data) {
                            $('#table1').bootstrapTable('remove', {
                                field: 'id',
                                values: ids
                            });
                            $('#table1').bootstrapTable('uncheckAll');
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    $(function () {
        $('#btn-eliminar').click(function (e) {
            e.preventDefault();
            var choice;
            iziToast.question({
                close: true,
                overlay: true,
                displayMode: 'once',
                id: 'question',
                zindex: 999,
                title: 'Atencion!',
                message: '¿Está seguro de Eliminar todos los items seleccionados?',
                position: 'center',
                buttons: [
                    ['<button><b>SI</b></button>', function (instance, toast) {
                        var ids = $.map($('#table1').bootstrapTable('getAllSelections'), function (row) {
                            return row.id;
                        });
                        $('#table1').bootstrapTable('remove', {
                            field: 'id',
                            values: ids
                        });

                        $.ajax('/{{$urlkey}}/removeselected', {
                            type: 'POST',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: {datos:ids}
                        });

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        iziToast.success({
                            timeout: 2000,
                            position:'topCenter',
                            title: 'Listo!',
                            message: 'Items, Eliminados.'
                        });
                    }, true],
                    ['<button>NO</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    }]
                ]
            });
        });
    });

    function VincularItems(){
        $("#vincular").button('loading');
        var comborubro = document.getElementById("id_rubro");
        var idrubro = comborubro.options[comborubro.selectedIndex].value;
        var ids = $.map($('#table1').bootstrapTable('getAllSelections'), function (row) {
            return row.id;
        });
        $.ajax('/subrubros/vincular', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: { idrubro : idrubro, ids: ids },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                $('#table1').bootstrapTable('removeAll');
                $('#table1').bootstrapTable('showLoading');
                $('#table1').bootstrapTable('append', data);
                $("#vincular").button('reset');
                $("#vincular").dequeue();
                $("#vincular").prop("disabled", false);
                $('#table1').bootstrapTable('hideLoading');
            },
            error:function(data) {
                console.log(data);
                iziToast.error({
                    timeout: 2000,
                    position:'center',
                    title: 'Error:',
                    message: data.responseJSON.message
                });
                $("#vincular").button('reset');
                $("#vincular").dequeue();
                $("#vincular").prop("disabled", false);
                $('#table1').bootstrapTable('hideLoading');
            }
        });
    };
</script>
@include('utils.statusnotification')
@stop

