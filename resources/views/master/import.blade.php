@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.catalognavbar')
@stop

@section('content')
<div class="container-fluid row">
    <div class="col-md-12">
        <div class="box col-md-8">
            <form class="form-horizontal" role="form" method="POST" action="/{{$urlkey}}/importExcel" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body mt-20">
                    <div class="form-group">
                        <div class="col-md-6">
                            <input type="file" name="import_file" class="file" />

                            @if ($errors->has('import_file'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('import_file') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-default" href="/{{$urlkey}}"><i class="fa fa-btn fa-arrow-left text-danger"></i> Cancel</a>
                        <button class="btn btn-default">
                            <i class="fa fa-btn fa-upload text-success"></i> Importar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <div class="thumbnail">
                    <img src="/img/ImportarDatos1.GIF" alt="...">
                    <h3><span class="label label-default">1</span> Crear Archivo</h3>
                    <p>El archivo a importar debe tener el t&iacute;tulo <strong>descripcion</strong> en la celda A1, y acontinuacion (a partir de la celda A2) todos los items a importar. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="thumbnail">
                        <h3><span class="label label-default">2</span> Buscar Archivo</h3>
                        <p>Haciendo click sobre el bot&oacute;n <strong>seleccionar archivo</strong>, se abre una ventana para buscar y seleccionar el archivo a importar. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="thumbnail">
                        <h3><span class="label label-default">3</span> Go! Importar Datos</h3>
                        <p>Luego de seleccionar el archivo, y que su nombre reemplace el texto "Ningún archivo seleccionado" en el formulario, precione el bot&oacute;n <strong><i class="fa fa-btn fa-upload"></i> Importar</strong>. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
@stop

@section('js')
<!--<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>-->
@stop
