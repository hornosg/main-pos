@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.cajasnavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <div class="form-group">
                                    <label>Desde/Hasta Fecha:</label>

                                    <div class="input-group">
                                        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span></span> <i class="fa fa-caret-down"></i>
                                        </div>
                                    </div>

                                    <div class="pull-right mt-5 ml-30">
                                        <button id="buscar" type="button" class='btn btn-primary' onclick='BuscarItems()'> <i class="fa fa-btn fa-search text-white"></i> Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'>
                            <thead class='thead-inverse'>
                                <th data-field="id_caja" data-align="right" class="text-uppercase">Caja</th>
                                <th data-field="creado" data-align="right"  class="text-uppercase">Creado</th>
                                <th data-field="tipocpte"  class="text-uppercase text-bold">TIPO</th>
                                <th data-field="numero">NUMERO</th>
                                <th data-field="titular"  class="text-uppercase  text-bold">TITULAR</th>
                                <th data-field="mediopago"  class="text-uppercase  text-bold">M.PAGO</th>
                                <th data-field="total" data-align="right"  class="text-uppercase text-bold">IMPORTE</th>
                                <th data-field="usuario" data-align="left" class="text-uppercase">Usuario</th>
                                <th data-field="observaciones" data-align="left" class="text-uppercase">Motivo</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<script src="/js/bootstrap-table-multiple-search.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<!--<link href="/css/daterangepicker.css" rel="stylesheet">-->
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<style>
    #loading,#loading2{
        display: inline !important;
    }
    .pt-20{
        padding-top: 20px;
    }
</style>
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="/vendor/select2/js/select2.full.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    $(function() {
        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Ultimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Ultimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Mes actual': [moment().startOf('month'), moment().endOf('month')],
                'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);
    });

    var $table = $('#table1');
    $table.bootstrapTable({
        exportDataType:'all',
        exportOptions:{fileName: 'sabana'}
    });

    function moneyFormatter(value, row, index) {
        return  "$ "+value;
    };

    function BuscarItems(){
        $('#table1').bootstrapTable('showLoading');
        $('.overlay').removeClass('d-none');
        var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
        $.ajax('/caja/getsabana', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : { desde:startDate, hasta:endDate},
            success: function(data) {
                $table.bootstrapTable('removeAll');
                $('#table1').bootstrapTable('append', data);
                $('#table1').bootstrapTable('hideLoading');
                $('.overlay').addClass('d-none');
            },
            error:function(data) {
                console.log(data);
                iziToast.error({
                    timeout: 2000,
                    position:'center',
                    title: 'Error:',
                    message: data.responseJSON.message
                });
                $('#table1').bootstrapTable('hideLoading');
                $('.overlay').addClass('d-none');
            }
        });
    };
</script>
@include('utils.borrarcpte')
@include('utils.borrarcptes')
@include('utils.statusnotification')
@stop
