@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
@include('layouts.cajasnavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div id="box-header">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="pr-10 pt-10 col-md-6 col-sm-6 col-xs-12">
                            <div class="mt-10 text-bold bg-color ">
                                <span class="info-icon"><i class="fa fa-dollar"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">TOTAL CAJA</span>
                                    <span class="info-box-number aqtotal"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </div>
                        <div id="toolbar2">
                            <div class="form-inline" role="form">
                                <button id="addnew" type="button" class="btn btn-primary" data-toggle="modal" data-target="#formModal" title="Agregar Efectivo">
                                    <i class="fa fa-btn fa-plus text-white"></i>
                                </button>
                                <span class="text-bold ml-10">ARQUEO DE CAJA</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="pr-10 pt-10 col-md-4 col-sm-6 col-xs-12">
                            <div class="mt-10 text-bold bg-color ">
                                <span class="info-icon"><i class="fa fa-dollar"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">TOTAL COMPROBANTES</span>
                                    <span class="info-box-number cptestotal"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </div>
                        <div class="pt-10 pull-right">
                            <div class="mt-10">
                                <button id="ajpos" type="button" name="ajpos" class="btn btn-info text-bold hidden" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> ..." title="Ajuste Positivo"><i class='fa fa-sign-in'></i> Ajuste (+)</button>
                                <button id="ajneg" type="button" name="ajneg" class="ml-10 btn btn-danger text-bold hidden" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> ..." title="Ajuste Negativo"><i class='fa fa-sign-out'></i> Ajuste (-)</button>
                            </div>
                            <div class="mt-10">
                                <a id="cerrar" class="btn btn-primary text-bold hidden" href="/{{$urlkey}}/cerrar" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> ...">Cerrar Caja</a>
                            </div>
                        </div>
                        <div id="toolbar">
                            <button id="ingreso" type="button" class="btn btn-primary" title="Ingreso de Efectivo" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> ...">
                                <i class="fa fa-btn fa-plus text-white"></i>
                            </button>
                            <button id="retiro" type="button" class="btn btn-warning" title="Retiro de Efectivo" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> ...">
                                <i class="fa fa-btn fa-plus text-white"></i>
                            </button>
                            <span class="text-bold ml-10">COMPROBANTES</span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <table id='table2' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar2"
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-unique-id='id'>
                            <thead class='thead-inverse'>
                                <th data-field='id' data-align='right' class='hidden text-uppercase'>Id</th>
                                <th data-field='id_caja' data-align='right' class='hidden text-uppercase'>Caja</th>
                                <th data-field='tipo_efectivo' data-align='right'  class='text-uppercase'>Tipo</th>
                                <th data-field='valor' data-align='right' data-formatter="moneyFormatter" class='text-uppercase text-bold'>VALOR</th>
                                <th data-field='cantidad' data-align='right' class='text-uppercase text-bold'>CANTIDAD</th>
                                <th data-field='total' data-align='right' data-formatter="moneyFormatter" class='text-uppercase text-bold'>TOTAL</th>
                                <th data-align='center' data-formatter='FunctionsFormatterArqueo'></th>
                            </thead>
                        </table>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search='false'
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-detail-view='true'
                               data-unique-id='id'>

                            <thead class='thead-inverse'>
                            <th data-field='id' data-align='right' class='hidden text-uppercase'>Id</th>
                            <th data-field='id_caja' data-align='right'  class='text-uppercase'>Caja</th>
                            <th data-field='creado' data-align='right'  class='text-uppercase'>Creado</th>
                            <th data-field='tipocpte'  class='text-uppercase  text-bold'>TIPO</th>
                            <th data-field='numero'>NUMERO</th>
                            <th data-field='id_titular' class='hidden'></th>
                            <th data-field='titular'  class='text-uppercase  text-bold'>TITULAR</th>
                            <th data-field='mediopago'  class='text-uppercase  text-bold'>M.PAGO</th>
                            <th data-field='total' data-align='right' data-formatter="moneyFormatter" class='text-uppercase text-bold'>IMPORTE</th>
                            <th data-field='usuario' data-align='left' class='text-uppercase'>Usuario</th>
                            <th data-formatter='FunctionsFormatter'></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Form Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modalmaster" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Arqueo de Caja: Agregar Efectivo</h4>
            </div>
            <div class="modal-body box box-primary">
                <form id="form1" name="form1" role="form">
                    <div class="box-body">
                        <input id="id" type="hidden" name="id">
                        <div class="form-group col-md-3">
                            <label for="tipo_efectivo" class="control-label">Tipo de Efectivo</label>
                            <select id="tipo_efectivo" name="tipo_efectivo" class="form-control text-uppercase">
                                <option value=""></option>
                                <option value="1">BILLETES</option>
                                <option value="2">MONEDAS</option>
                                <option value="3">CHEQUES</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-3 col-md-3">
                            <label for="valor" class="control-label">Valor</label>
                            <input id="valor" type="number" step="0.10" class="form-control input-sm" name="valor" value="">
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-3 col-md-3">
                            <label for="cantidad" class="control-label">Cantidad</label>
                            <input id="cantidad" type="number" class="form-control input-sm" name="cantidad" value="1">
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-3 col-md-3">
                            <label for="total" class="control-label">Total</label>
                            <input id="total" type="number" class="form-control input-sm" name="total" value="" readonly>
                        </div>
                    </div>
                    <div class="box-footer pb-10 no-bt">
                        <div class="pull-right">
                            <button id="guardar" type="submit" name="guardar" class="btn btn-primary minw90">
                                <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                                <div class="d-none text-danger overlay">
                                    <i class="fa fa-refresh fa-spin"></i>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@php
foreach ($arqueo as $item) {
    if ($item->tipo_efectivo == 1){
        $item->tipo_efectivo ='BILLETES';
    }
    if ($item->tipo_efectivo == 2){
        $item->tipo_efectivo ='MONEDAS';
    }
    if ($item->tipo_efectivo == 3){
        $item->tipo_efectivo ='CHEQUES';
    }
}
@endphp
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<style>
    .info-icon {
        float: left;
        height: 55px;
        width: 55px;
        text-align: center;
        font-size: 40px;
        line-height: 55px;
        background: rgba(0,0,0,0.2);
    }
</style>
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
<script language='JavaScript' type='text/javascript'>
    var motivoaj='';
    var importeaj=0;
    var datos = <?php echo json_encode($items); ?>;
    var $table = $('#table1');
    $table.bootstrapTable({data: datos,exportDataType:'all',exportOptions:{fileName: 'comprobantes_caja'}});
    var datos2 = <?php echo json_encode($arqueo); ?>;
    $('#table2').bootstrapTable({data: datos2});
    $("#tipo_efectivo").select2({ placeholder: "Seleccionar Tipo", allowClear: true});
    var totalarqueo = <?php echo $totalarqueo; ?>;
    document.querySelector('.aqtotal').innerHTML = totalarqueo.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
    var cptestotal = <?php echo $totalcptes; ?>;
    document.querySelector('.cptestotal').innerHTML = cptestotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
    if (totalarqueo == cptestotal){
        $('.bg-color').addClass('bg-green');
        $('#cerrar').removeClass('hidden');
        $('#ajpos').addClass('hidden');
        $('#ajneg').addClass('hidden');
    }else{
        $('.bg-color').addClass('bg-gray');
        $('#cerrar').addClass('hidden');
        $('#ajpos').removeClass('hidden');
        $('#ajneg').removeClass('hidden');
    }

    $("#valor").change(function(){
        valor = $("#valor").val();
        cantidad = $("#cantidad").val();
        $("#total").val(valor*cantidad);
    });
    $("#cantidad").change(function(){
        valor = $("#valor").val();
        cantidad = $("#cantidad").val();
        $("#total").val(valor*cantidad);
    });

    $('#form1').on('submit', function(e) {
        $('.overlay').removeClass('d-none');
        e.preventDefault();
        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'/{{$urlkey}}/store/arqueo',
            data: $('#form1').serialize(),
            success: function(data) {
                iziToast.success({
                    timeout: 2000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                data.total = data.valor*data.cantidad;
                data.total = data.total.toFixed(2);
                $('#formModal').modal('hide');
                $('#table2').bootstrapTable('append', data);
                /* Recalculo Arqueo */
                var aqs = $('#table2').bootstrapTable('getData');
                totalarqueo = 0;
                aqs.forEach(function(element) {
                    totalarqueo = totalarqueo + Number(element.total);
                });
                document.querySelector('.aqtotal').innerHTML = totalarqueo.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

                if (totalarqueo == cptestotal){
                    $('.bg-color').removeClass('bg-gray');
                    $('.bg-color').addClass('bg-green');
                    $('#cerrar').removeClass('hidden');
                    $('#ajpos').addClass('hidden');
                    $('#ajneg').addClass('hidden');
                }else{
                    $('.bg-color').removeClass('bg-green');
                    $('.bg-color').addClass('bg-gray');
                    $('#cerrar').addClass('hidden');
                    $('#ajpos').removeClass('hidden');
                    $('#ajneg').removeClass('hidden');
                }

                document.getElementById('form1').reset();
                $('.overlay').addClass('d-none');
            },
            error: function(data){
                var errors = data.responseJSON;
                if (errors.errors.descripcion){
                    $('#descripcion').parents(".form-group").addClass('has-error');
                }
                if (errors.errors.id_sucursal){
                    $('#id_sucursal').parents(".form-group").addClass('has-error');
                }
                $('.overlay').addClass('d-none');
            }
        });
    });

    function BorrarArqueo(id){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $.ajax({
                        type: "GET",
                        url:'/{{$urlkey}}/delete/arqueo/'+id,
                        success: function(data) {
                            $('#table2').bootstrapTable('removeByUniqueId', id);
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                            /* Recalculo Arqueo */
                            var aqs = $('#table2').bootstrapTable('getData');
                            totalarqueo = 0;
                            aqs.forEach(function(element) {
                                totalarqueo = totalarqueo + Number(element.total);
                            });
                            document.querySelector('.aqtotal').innerHTML = totalarqueo.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

                            if (totalarqueo == cptestotal){
                                $('.bg-color').removeClass('bg-gray');
                                $('.bg-color').addClass('bg-green');
                                $('#cerrar').removeClass('hidden');
                                $('#ajpos').addClass('hidden');
                                $('#ajneg').addClass('hidden');
                            }else{
                                $('.bg-color').removeClass('bg-green');
                                $('.bg-color').addClass('bg-gray');
                                $('#cerrar').addClass('hidden');
                                $('#ajpos').removeClass('hidden');
                                $('#ajneg').removeClass('hidden');
                            }
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }

    $('#formModal').on('shown.bs.modal', function (e) {
        $('#tipo_efectivo').val('').trigger('change');
    });

    $("#ajpos").click(function(){
        $(this).button('loading');

        iziToast.question({
            icon:'fa fa-btn fa-sliders',
            rtl: false,
            layout: 1,
            drag: false,
            timeout: false,
            close: true,
            overlay: true,
            displayMode: 1,
            id: 'question',
            progressBar: true,
            title: 'Nuevo Ajuste (+)',
            message: '',
            position: 'center',
            inputs: [
                ['<input id="motivopos" type="text" placeholder="Motivo" class="col-md-7">'],
                ['<input id="importepos" type="number" placeholder="Importe" class="col-md-3">']
            ],
            buttons: [
                ['<button><i class="fa fa-btn fa-floppy-o"></i> Guardar</button>', function (instance, toast, button, e, inputs) {
                    /*console.info(button);
                    console.info(e);
                    alert(inputs[0].value);*/
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    $.ajax({
                        type: "GET",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url:'/{{$urlkey}}/store/aj/1',
                        data : { motivo : inputs[0].value, importe: inputs[1].value },
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data) {
                            iziToast.success({
                                timeout: 2000,
                                position:'topRight',
                                title: 'Listo!',
                                message: data.msg
                            });
                            $('#table1').bootstrapTable('append', data);
                            CalcularTotalCptes()

                            $("#ajpos").button('reset');
                            $("#ajpos").dequeue();
                            $('.overlay').addClass('d-none');
                        }
                    });
                }, false]
            ],
            onClosing: function(instance, toast, closedBy){
                $("#ajpos").button('reset');
                $("#ajpos").dequeue();
                $('.overlay').addClass('d-none');
                console.info('Closing | closedBy: ' + closedBy);
            },
            onClosed: function(instance, toast, closedBy){
                $("#ajpos").button('reset');
                $("#ajpos").dequeue();
                $('.overlay').addClass('d-none');
                console.info('Closed | closedBy: ' + closedBy);
            }
        });
    });

    $("#ajneg").click(function(){
        $(this).button('loading');

        iziToast.question({
            icon:'fa fa-btn fa-sliders',
            rtl: false,
            layout: 1,
            drag: false,
            timeout: false,
            close: true,
            overlay: true,
            displayMode: 1,
            id: 'question',
            progressBar: true,
            title: 'Nuevo Ajuste (-)',
            message: '',
            position: 'center',
            inputs: [
                ['<input id="motivopos" type="text" placeholder="Motivo" class="col-md-7">'],
                ['<input id="importepos" placeholder="Importe" class="col-md-3">']
            ],
            buttons: [
                ['<button><i class="fa fa-btn fa-floppy-o"></i> Guardar</button>', function (instance, toast, button, e, inputs) {
                    /*console.info(button);
                     console.info(e);
                     alert(inputs[0].value);*/
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    $.ajax({
                        type: "GET",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url:'/{{$urlkey}}/store/aj/-1',
                        data : { motivo : inputs[0].value, importe: inputs[1].value },
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data) {
                            iziToast.success({
                                timeout: 2000,
                                position:'topRight',
                                title: 'Listo!',
                                message: data.msg
                            });
                            $('#table1').bootstrapTable('append', data);
                            CalcularTotalCptes()

                            $("#ajneg").button('reset');
                            $("#ajneg").dequeue();
                            $('.overlay').addClass('d-none');
                        }
                    });
                }, false]
            ],
            onClosing: function(instance, toast, closedBy){
                $("#ajneg").button('reset');
                $("#ajneg").dequeue();
                $('.overlay').addClass('d-none');
                console.info('Closing | closedBy: ' + closedBy);
            },
            onClosed: function(instance, toast, closedBy){
                $("#ajneg").button('reset');
                $("#ajneg").dequeue();
                $('.overlay').addClass('d-none');
                console.info('Closed | closedBy: ' + closedBy);
            }
        });
    });

    $("#ingreso").click(function(){
        $(this).button('loading');

        iziToast.question({
            icon:'fa fa-btn fa-sliders',
            rtl: false,
            layout: 1,
            drag: false,
            timeout: false,
            close: true,
            overlay: true,
            displayMode: 1,
            id: 'question',
            progressBar: true,
            title: 'Ingreso de Efectivo',
            message: '',
            position: 'center',
            inputs: [
                ['<input type="text" placeholder="Motivo" class="col-md-7">'],
                ['<input placeholder="Importe" class="col-md-3">']
            ],
            buttons: [
                ['<button><i class="fa fa-btn fa-floppy-o"></i> Guardar</button>', function (instance, toast, button, e, inputs) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    $.ajax({
                        type: "GET",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url:'/{{$urlkey}}/store/mov/1',
                        data : { motivo : inputs[0].value, importe: inputs[1].value },
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data) {
                            iziToast.success({
                                timeout: 2000,
                                position:'topRight',
                                title: 'Listo!',
                                message: data.msg
                            });
                            $('#table1').bootstrapTable('append', data);
                            CalcularTotalCptes()

                            $("#ingreso").button('reset');
                            $("#ingreso").dequeue();
                            $('.overlay').addClass('d-none');
                        }
                    });
                }, false]
            ],
            onClosing: function(instance, toast, closedBy){
                $("#ingreso").button('reset');
                $("#ingreso").dequeue();
                $('.overlay').addClass('d-none');
                console.info('Closing | closedBy: ' + closedBy);
            },
            onClosed: function(instance, toast, closedBy){
                $("#ingreso").button('reset');
                $("#ingreso").dequeue();
                $('.overlay').addClass('d-none');
                console.info('Closed | closedBy: ' + closedBy);
            }
        });
    });

    $("#retiro").click(function(){
        $(this).button('loading');

        iziToast.question({
            icon:'fa fa-btn fa-sliders',
            color:'#ec971f',
            rtl: false,
            layout: 1,
            drag: false,
            timeout: false,
            close: true,
            overlay: true,
            displayMode: 1,
            id: 'question',
            progressBar: true,
            title: 'Retiro de Efectivo',
            message: '',
            position: 'center',
            inputs: [
                ['<input type="text" placeholder="Motivo" class="col-md-7">'],
                ['<input placeholder="Importe" class="col-md-3">']
            ],
            buttons: [
                ['<button><i class="fa fa-btn fa-floppy-o"></i> Guardar</button>', function (instance, toast, button, e, inputs) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    $.ajax({
                        type: "GET",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url:'/{{$urlkey}}/store/mov/-1',
                        data : { motivo : inputs[0].value, importe: inputs[1].value },
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data) {
                            iziToast.success({
                                timeout: 2000,
                                position:'topRight',
                                title: 'Listo!',
                                message: data.msg
                            });
                            $('#table1').bootstrapTable('append', data);
                            CalcularTotalCptes()

                            $("#retiro").button('reset');
                            $("#retiro").dequeue();
                            $('.overlay').addClass('d-none');
                        }
                    });
                }, false]
            ],
            onClosing: function(instance, toast, closedBy){
                console.info('Closing | closedBy: ' + closedBy);
            },
            onClosed: function(instance, toast, closedBy){
                console.info('Closed | closedBy: ' + closedBy);
            }
        });
    });

    $("#cerrar").click(function(){
        $(this).button('loading');
    });

    $table.on('expand-row.bs.table', function (e, index, row, $detail) {
        $idtabla2="tabla"+row.id;
        switch (row.tipocpte){
            case "REMITO":  case "FACTURA":  case "NOTA DE CREDITO":
                html='<div class="box"><table id='+$idtabla2+' class="table table-sm table-bordered table-hover table-striped table-condensed table-responsive" ' +
                    'data-search="true" data-strict-search="false" data-multiple-search="true" data-pagination="true">' +
                    '<thead class="thead-inverse-gray"><tr>' +
                    '<th data-field="id" data-align="right">Codigo</th>' +
                    '<th data-field="producto">Descripcion</th>' +
                    '<th data-field="marca">Marca</th>' +
                    '<th data-field="precio" data-align="right">Precio Unitario</th>' +
                    '<th data-field="cantidad" data-align="right">Cantidad</th>' +
                    '</tr></thead>' +
                    '</table></divS>';
                $detail.html(html);
                break;
            case "RECIBO":
                html='<div class="box pt-10"><table id='+$idtabla2+' class="table table-sm table-bordered table-hover table-striped table-condensed table-responsive">' +
                    '<thead class="thead-inverse-gray"><tr>' +
                    '<th data-field="id" data-align="right" class="text-uppercase hidden">Id</th>' +
                    '<th data-field="id_mediopago" class="text-uppercase hidden"></th>' +
                    '<th data-field="descripcion" class="text-uppercase col-md-2">Medio de Pago</th>' +
                    '<th data-field="numero_transaccion" data-align="right" class="text-uppercase">Numero</th>' +
                    '<th data-field="banco" data-align="right" class="text-uppercase">Banco</th>' +
                    '<th data-field="cuotas" data-align="right" class="text-uppercase">Cuotas</th>' +
                    '<th data-field="fecha_vto" data-align="right" class="text-uppercase">F.Rcbo</th>' +
                    '<th data-field="fecha_cobro" data-align="right" class="text-uppercase">F.Cbro</th>' +
                    '<th data-field="importe" data-align="right" class="col-md-3">IMPORTE</th>' +
                    '</tr></thead>' +
                    '</table></divS>';
                $detail.html(html);
                break;
            default:
                html='<div class="box pt-10"><table id='+$idtabla2+' class="table table-sm table-bordered table-hover table-striped table-condensed table-responsive">' +
                    '<thead class="thead-inverse-gray"><tr>' +
                    '<th data-field="observaciones"  class="text-uppercase  text-bold">MOTIVO</th>' +
                    '</tr></thead>' +
                    '</table></divS>';
                $detail.html(html);
                break;
        }
        $('#'+$idtabla2).bootstrapTable();
        $('#'+$idtabla2).bootstrapTable('showLoading');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'/cptes/detalle/'+row.id,
            data: row.id,
            success: function(datosdetalle) {
                $('#'+$idtabla2).bootstrapTable('append', datosdetalle);
                $('#'+$idtabla2).bootstrapTable('hideLoading');
            }
        });
    });

    function moneyFormatter(value, row, index) {
        return  "$ "+value;
    };

    function FunctionsFormatterArqueo(value, row, index) {
        return "<a class='text-danger' href='#' onclick='BorrarArqueo("+row.id+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>";
    };

    function FunctionsFormatter(value, row, index) {
        var cpte='';

        if(row.tipocpte==="REMITO"){
            cpte = 'rem';
        }else if (row.tipocpte==="FACTURA") {
            cpte = 'fc';
        }else if (row.tipocpte==="RECIBO") {
            cpte = 'rbo';
        }else{
            cpte = '';
        }

        var links = '';
        if (cpte === ''){
            links = "<a class='btn-outline-danger' href='#' onclick='Borrar("+row.id+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>"
        }else{
            links = "<a class='btn-outline-danger' href='#' onclick='Borrar("+row.id+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
                "<a class='btn-outline-warning' href='/cptes/edit/"+cpte+"/"+row.id+"'><i class='fa fa-fw fa-pencil'></i></a>" +
                "<a class='btn-outline-primary' href='/cptes/show/"+cpte+"/"+row.id+"'><i class='fa fa-fw fa-search'></i></a>";
/*
"<a class='text-primary' href='/cptes/print/"+cpte+"/"+row.id+"'><i class='fa fa-fw fa-print'></i></a>";
*/
        }

        return links;
    };

    function CalcularTotalCptes(){
        var cptes = $('#table1').bootstrapTable('getData');
        cptestotal = 0;
        cptes.forEach(function(element) {
            cptestotal = cptestotal + Number(element.total);
        });
        document.querySelector('.cptestotal').innerHTML = cptestotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');

        if (totalarqueo.toFixed(2) == cptestotal.toFixed(2)){
            $('.bg-color').removeClass('bg-gray');
            $('.bg-color').addClass('bg-green');
            $('#cerrar').removeClass('hidden');
            $('#ajpos').addClass('hidden');
            $('#ajneg').addClass('hidden');
        }else{
            $('.bg-color').removeClass('bg-green');
            $('.bg-color').addClass('bg-gray');
            $('#cerrar').addClass('hidden');
            $('#ajpos').removeClass('hidden');
            $('#ajneg').removeClass('hidden');
        }
    }
</script>
@include('utils.borrarcpte')
@include('utils.statusnotification')
@stop

