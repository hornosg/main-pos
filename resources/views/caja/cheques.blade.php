@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.cajasnavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar"></div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-unique-id='id'>
                            <thead class='thead-inverse'>
                                <tr>
                                    <th rowspan='2' data-field='id' data-align='right' class='hidden text-uppercase'>Id</th>
                                    <th rowspan='2' data-field='numero'>NUMERO</th>
                                    <th rowspan='2' data-field='banco' class='text-nowrap text-uppercase'>BANCO</th>
                                    <th rowspan='2' data-field='importe' data-align='right' data-formatter="moneyFormatter">IMPORTE</th>
                                    <th rowspan='2' data-field='fecha_cobro' data-align='right'  class='text-uppercase'>Recibido</th>
                                    <th rowspan='2' data-field='fecha_vto' data-align='right'  class='text-uppercase'>Fecha de Cobro</th>
                                    <th colspan='3' data-align='center' class='text-uppercase'>COMPROBANTE ORIGEN</th>
                                    <th colspan='3' data-align='center' class='text-uppercase'>COMPROBANTE DESTINO</th>
                                    <th rowspan='2' data-field='estado'>ESTADO</th>
                                </tr>
                                <tr>
                                    <th data-field='cpteorigen'>TIPO</th>
                                    <th data-field='nrocpteorigen' class='text-nowrap text-uppercase  text-bold'>NUMERO</th>
                                    <th data-field='titularorigen' class='text-uppercase text-bold'>TITULAR</th>
                                    <th data-field='cptedestino'>TIPO</th>
                                    <th data-field='numerocptedestino'  class='text-uppercase  text-bold'>NUMERO</th>
                                    <th data-field='titulardestino' class='text-nowrap text-uppercase text-bold'>TITULAR</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            {{--<div class="box-footer ptb-10">--}}
                {{--<div class="pull-right pr-5">--}}
                    {{--<a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script language='JavaScript' type='text/javascript'>
    var datos = <?php echo json_encode($items); ?>;
    $(document.body).addClass('sidebar-collapse');
    var $table = $('#table1');
    $table.bootstrapTable('showLoading');
    $table.bootstrapTable({
        data: datos,
        exportDataType:'all',
        exportOptions:{fileName: 'items'}
    });
    $table.bootstrapTable('hideLoading');

    function cpterelFormatter(value, row, index) {
        return  "<a href='/cptes/show/rem/"+row.idrel+"'>"+value+"</a>";
    };

    function clienteFormatter(value, row, index) {
        return  "<a href='/clientes/cuentacorriente/"+row.id_titular+"'>"+value+"</a>";
    };

    function clienterelFormatter(value, row, index) {
        return  "<a href='/clientes/cuentacorriente/"+row.id_titularrel+"'>"+value+"</a>";
    };

    function moneyFormatter(value, row, index) {
        return  "$ "+value;
    };

    function FunctionsStatusFormatter(value, row, index) {
        if(row.id_estado===2){
            return '<span class="badge bg-red">PENDIENTE</span>';
        }
        if(row.id_estado===3){
            return '<span class="badge bg-primary">REGISTRADO</span>';
        }
    };

    function FunctionsStatusrelFormatter(value, row, index) {
        if(row.id_estadorel===1){
            return '<span class="badge bg-green">PAGO</span>';
        }
        if(row.id_estadorel===2){
            return '<span class="badge bg-red">PENDIENTE</span>';
        }
        if(row.id_estadorel===3){
            return '<span class="badge bg-primary">REGISTRADO</span>';
        }
    };

</script>
@include('utils.borrarcpte')
@include('utils.borrarcptes')
@include('utils.statusnotification')
@stop
