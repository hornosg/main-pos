@extends('adminlte::page')
@section('css')
<link rel="stylesheet" href="/css/mainpos.css">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<style>
    .main-sidebar{
        display: none;
    }
    html body.skin-black-light.sidebar-mini.sidebar-collapse div.wrapper div.content-wrapper{
        margin-left: 0px !important;
        min-height: 555px !important;
    }
    html body.skin-black-light.sidebar-mini.sidebar-collapse div.wrapper footer.main-footer{
        margin-left: 0px !important;
    }
    .small-box h4 {
        font-size: 25px !important;
    }
    .small-box .icon {
         font-size: 80px;
    }

    .info-box{
        min-height: 67px !important;
        margin-bottom: 13px !important;
    }

    .info-box-icon{
        height:67px !important;
        line-height: 67px !important;
        width: 130px !important;
    }
    .info-box-text, .info-box-number{
        display: inline !important;
    }
    .info-box-number {
        font-size: 16px !important;
    }
    .info-box-content{
        padding: 5px 6px
    }
    .content{
        padding-top:20px !important;
    }
</style>
@stop
@section('title', 'MainPOS')

@php
if(empty($avgcptes)){
    $avgcptes=0;
}
if(empty($masde3xcpte)){
    $masde3xcpte=0;
}
@endphp

@section('content')
<div id="content" class="container-fluid row">
    @if (Auth::user()->id_rol==1)
        <div class="col-md-6 col-sm-12">
            @include('utils.homelinks')
        </div>
        <div class="col-xs-6">
            <div class="graph-container bg-navy-active">
                <h5 class="text-bold">Promedio de Comprobantes por hora (ultimos 7 dias) </h5>
                <div id="cptesxhora" class="graph"></div>
            </div>
            @include('utils.masvendidos')
        </div>
    @endif
    @if ( (Auth::user()->id_rol==2) || (Auth::user()->id_rol==3))
        <div class="col-md-2 hidden-xs hidden-sm">
            @include('utils.homeperformance')
        </div>
        @if (Auth::user()->id_rol==3)
            <div class="col-md-5 col-sm-12">
                @include('utils.homelinks')
            </div>
        @else
            <div class="col-md-5 col-sm-12">
                @include('utils.homelinks')
            </div>
            <div class="col-md-5 col-sm-12">
                <div class="graph-container bg-purple">
                    <h5 class="text-bold">Promedio de Comprobantes por hora (ultimos 7 dias) </h5>
                    <div id="cptesxhora" class="graph"></div>
                </div>
                @include('utils.masvendidos')
            </div>
        @endif
    @endif
</div>
@stop

@section('js')
    @if (Auth::user()->id_rol==5)
    <script language='JavaScript' type='text/javascript'>
        window.location.href = '{!!URL::to('/productos')!!}';
    </script>
    @endif
    <!-- Google Charts -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script language='JavaScript' type='text/javascript'>
        var cptesxhora = {!! $cptesxhora !!};
        if (cptesxhora.length > 1 ) {
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);
        }
        function drawChart() {
            var data = google.visualization.arrayToDataTable({!! $cptesxhora !!});
            var options = {
                legend: {position: 'none'}
            };
            var chart = new google.visualization.LineChart(document.getElementById('cptesxhora'));
            chart.draw(data,options);
        }
    </script>
@stop