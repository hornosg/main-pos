@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.provsnavbar',['titulo2'=>'','icon'=>'fa-truck'])
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-primary pt-50">
            <form id="form1" role="form" method="POST" action="/{{$urlkey}}/store">
                {{ csrf_field() }}
                    <div class="box-body">
                        <input id="id_tipotitular" type="hidden" name="id_tipotitular" value="3">

                        <div class="row">
                            <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-6  col-md-6 ">
                                <label for="descripcion" class="control-label">Nombre</label>
                                <input id="descripcion" type="text" class="form-control input-sm text-uppercase" name="descripcion" value="{{ old('descripcion') }}" autofocus>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-3 col-md-3 {{ $errors->has('id_tipoiva') ? ' has-error' : '' }}">
                                <label for="id_tipoiva" class="control-label">Categ. IVA
                                    <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="@lang('messages.help.tipoiva')">
                                        <i class="fa fa-info-circle"></i>
                                    </a>
                                </label>
                                <select id="id_tipoiva" name="id_tipoiva" class="form-control text-uppercase">
                                    <option value=""></option>
                                    @foreach(DB::table('sys_tipoiva')->where('activo', 1)->get() as $tipoiva)
                                    <option value="{{$tipoiva->id}}">{{$tipoiva->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group {{ $errors->has('cuit') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-3">
                                <label for="cuit" class="control-label">Cuit</label>
                                <input id="cuit" type="text" class="form-control input-sm text-uppercase" name="cuit" value="{{ old('cuit') }}" >
                            </div>
                            <div class="form-group {{ $errors->has('direccion') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-6">
                                <label for="direccion" class="control-label">Direccion</label>
                                <input id="direccion" type="text" class="form-control input-sm text-uppercase" name="direccion" value="{{ old('direccion') }}">
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-3 col-md-3{{ $errors->has('id_localidad') ? ' has-error' : '' }}">
                                <label for="id_localidad" class="control-label">Localidad</label>
                                <select id="id_localidad" name="id_localidad" class="form-control text-uppercase">
                                    <option value=""></option>
                                    @foreach(DB::table('conf_localidades')->where('activo', 1)->get() as $localidad)
                                    <option value="{{$localidad->id}}">{{$localidad->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="clearfix visible-lg-block visible-md-block"></div>
                            <div class="form-group {{ $errors->has('telefono') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-3">
                                <label for="telefono" class="control-label">Telefono</label>
                                <input id="telefono" type="text" class="form-control input-sm text-uppercase" name="telefono" value="{{ old('telefono') }}" >
                            </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-4">
                                <label for="email" class="control-label">Email</label>
                                <input id="email" type="text" class="form-control input-sm text-uppercase" name="email" value="{{ old('email') }}" >
                            </div>
                            <div class="form-group {{ $errors->has('web') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-5">
                                <label for="web" class="control-label">Web</label>
                                <input id="web" type="text" class="form-control input-sm text-uppercase" name="web" value="{{ old('web') }}" >
                            </div>
                            <div class="form-group{{ $errors->has('observaciones') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-5 col-md-5">
                                <label for="observaciones" class="control-label">Observaciones</label>
                                <input id="observaciones" type="text" class="form-control input-sm" name="observaciones" value="{{ old('observaciones') }}">
                            </div>
                            <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-1 col-md-1 pull-left">
                                <label for="activo" class="control-label">Activo</label>
                                <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" checked>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer pb-10 no-bt">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="/{{$urlkey}}"><i class="fa fa-btn fa-arrow-left"></i> Cancel</a>
                            <button type="submit" name="action" value="savecontinue" class="btn btn-primary">
                                <i class="fa fa-btn fa-floppy-o"></i> Guardar & <i class="fa fa-btn fa-repeat"></i> Continuar</a>
                            </button>
                            <button type="submit" name="action" value="save" class="btn btn-primary">
                                <i class="fa fa-btn fa-floppy-o"></i> Guardar</a>
                            </button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script type='text/javascript'>
    $('#activo').bootstrapToggle();
    $('#id_localidad').select2({
        placeholder: "",
        allowClear: true
    });
</script>
@stop
