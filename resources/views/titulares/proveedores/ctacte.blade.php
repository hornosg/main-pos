@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.provsnavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <span><strong>Nombre Apellido</strong>
                                <i class="fa fa-btn fa-long-arrow-right"></i> Saldo= $ 2600
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-pagination-first-text='Primera'
                               data-pagination-pre-text='Ant.'
                               data-pagination-next-text='Sig.'
                               data-pagination-last-text='Ultima'
                               data-sort-name='id'
                               data-sort-order='desc'>
                            <thead class='thead-inverse'>
                                <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                                <th data-field='fecha'   class='text-uppercase'>fecha</th>
                                <th data-field='comprobante'   class='text-uppercase'>comprobante</th>
                                <th data-field='numero'   class='text-uppercase'>numero</th>
                                <th data-field='importe'  class='text-uppercase'>importe</th>
                                <th data-field='estado'  class='text-uppercase' data-formatter='statusFormatter'>estado</th>
                                <th data-field='saldo' class='text-uppercase'>saldo</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<!--<script src="/js/bootstrap-table-filter.js"></script>-->
<!--<script src="/js/bootstrap-table-filter-control.js"></script>-->
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script language='JavaScript' type='text/javascript'>
    var $table = $('#table1');
    $(function () {
        var data = [
            {
                "id": 32,
                "fecha": "10/02/2018",
                "comprobante": "ORDEN DE COMPRA",
                "numero": "4562",
                "importe": "$100",
                "saldo": "$2600",
                "estado": "Pendiente"
            },
            {
                "id": 31,
                "fecha": "10/02/2018",
                "comprobante": "ORDEN DE COMPRA",
                "numero": "4562",
                "importe": "$100",
                "saldo": "$2500",
                "estado": "Pendiente"
            },            {
                "id": 30,
                "fecha": "10/02/2018",
                "comprobante": "ORDEN DE COMPRA",
                "numero": "4562",
                "importe": "$500",
                "saldo": "$2400",
                "estado": "Pendiente"
            },            {
                "id": 29,
                "fecha": "10/02/2018",
                "comprobante": "ORDEN DE COMPRA",
                "numero": "4562",
                "importe": "$450",
                "saldo": "$1900",
                "estado": "Pendiente"
            },            {
                "id": 28,
                "fecha": "10/02/2018",
                "comprobante": "ORDEN DE COMPRA",
                "numero": "4562",
                "importe": "$1450",
                "saldo": "$1450",
                "estado": "Pendiente"
            },            {
                "id": 27,
                "fecha": "10/02/2018",
                "comprobante": "PAGO",
                "numero": "4562",
                "importe": "$700",
                "saldo": "$0",
                "estado": "Acreditado"
            },            {
                "id": 26,
                "fecha": "21/02/2018",
                "comprobante": "ORDEN DE COMPRA",
                "numero": "45632",
                "importe": "$500",
                "saldo": "$700",
                "estado": "Pago"
            },            {
                "id": 25,
                "fecha": "20/02/2018",
                "comprobante": "PAGO",
                "numero": "4562",
                "importe": "$1800",
                "saldo": "$200",
                "estado": "Acreditado"
            },            {
                "id": 24,
                "fecha": "12/02/2018",
                "comprobante": "ORDEN DE COMPRA",
                "numero": "4562",
                "importe": "$1100",
                "saldo": "$2000",
                "estado": "Pago"
            },            {
                "id": 23,
                "fecha": "11/02/2018",
                "comprobante": "ORDEN DE COMPRA",
                "numero": "4563",
                "importe": "$450",
                "saldo": "$900",
                "estado": "Pago"
            },
            {
                "id": 22,
                "fecha": "10/02/2018",
                "comprobante": "ORDEN DE COMPRA",
                "numero": "4562",
                "importe": "$450",
                "saldo": "$450",
                "estado": "Pago"
            }
        ];
        $('#table1').bootstrapTable({data: data});
    });
    $(document.body).addClass('sidebar-collapse');

    function statusFormatter(value, row, index) {
        if(value==="Pago" || value==="Acreditado" ){
            return '<span class="badge bg-success">' + value + '</span>';
        }else{
            return '<span class="badge bg-danger">' + value + '</span>';
        }
    };

    $(window).on('load', function(event){
        var windowSize = $(window).width(); // Could've done $(this).width()
        if(windowSize < 350){
            //Add your javascript for larger screens here
            $('#table1').bootstrapTable('toggleView');
        }
    });

    $(window).on('resize', function(event){
        var windowSize = $(window).width(); // Could've done $(this).width()
        if(windowSize < 350){
            //Add your javascript for larger screens here
            $('#table1').bootstrapTable('toggleView');
        }
    });
</script>
@include('utils.statusnotification')
@stop
