@extends('adminlte::page')

@section('title', $titulo)

@php
if ($edit==0){
    $editable='readonly';
    $visible='d-none';
    $disabled='disabled';
    $accion = 'Visualizar';
}else{
    $editable=' ';
    $visible=' ';
    $disabled=' ';
    $accion = 'Editar';
}
$checked = $proveedor->activo == 1 ? 'checked':'';
@endphp

@section('content_header')
    @include('layouts.provsnavbar',['titulo2'=>'','icon'=>'fa-truck'])
@stop




@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-primary pt-50">
            <form id="form1" role="form" method="POST" action="/proveedores/update/{{$proveedor->id}}">
                {{ csrf_field() }}
                <div class="box-body">
                    <input id="id_tipotitular" type="hidden" name="id_tipotitular" value="3">

                    <div class="row">
                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-6">
                            <label for="descripcion" class="control-label">Nombre</label>
                            <input id="descripcion" type="text" class="form-control input-sm text-uppercase" name="descripcion" value="{{ $proveedor->descripcion }}" {{$editable}}>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-3 col-md-3{{ $errors->has('id_tipoiva') ? ' has-error' : '' }}">
                            <label for="id_tipoiva" class="control-label">Categ. IVA
                                <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="@lang('messages.help.tipoiva')">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                            </label>
                            <select id="id_tipoiva" name="id_tipoiva" type="button" class="form-control" data-toggle="dropdown" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::table('sys_tipoiva')->where('activo', 1)->get() as $tipoiva)
                            <option value="{{$tipoiva->id}}" {{ $proveedor->id_tipoiva ==  $tipoiva->id ? 'selected' : '' }}>{{$tipoiva->descripcion}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group {{ $errors->has('cuit') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-3">
                            <label for="cuit" class="control-label">Ciut</label>
                            <input id="cuit" type="text" class="form-control input-sm text-uppercase" name="cuit" value="{{ $proveedor->cuit }}"  {{$editable}}>
                        </div>
                        <div class="form-group {{ $errors->has('direccion') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-6">
                            <label for="direccion" class="control-label">Direccion</label>
                            <input id="direccion" type="text" class="form-control input-sm text-uppercase" name="direccion" value="{{ $proveedor->direccion }}" {{$editable}}>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-3 col-md-3 {{ $errors->has('id_localidad') ? ' has-error' : '' }}">
                            <label for="id_localidad" class="control-label">Localidad</label>
                            <select id="id_localidad" name="id_localidad" type="button" class="form-control" data-toggle="dropdown" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::table('conf_localidades')->where('activo', 1)->get() as $localidad)
                            <option value="{{$localidad->id}}" {{ $proveedor->id_localidad ==  $localidad->id ? 'selected' : '' }}>{{$localidad->descripcion}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="clearfix visible-lg-block visible-md-block"></div>
                        <div class="form-group {{ $errors->has('telefono') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-3">
                            <label for="telefono" class="control-label">Telefono</label>
                            <input id="telefono" type="text" class="form-control input-sm text-uppercase" name="telefono" value="{{ $proveedor->telefono }}"  {{$editable}}>
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-4">
                            <label for="email" class="control-label">Email</label>
                            <input id="email" type="text" class="form-control input-sm text-uppercase" name="email" value="{{ $proveedor->email }}" {{$editable}}>
                        </div>
                        <div class="form-group {{ $errors->has('web') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-5">
                            <label for="web" class="control-label">Web</label>
                            <input id="web" type="text" class="form-control input-sm" name="web" value="{{ $proveedor->web }}"  {{$editable}}>
                        </div>
                        <div class="form-group{{ $errors->has('observaciones') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-5 col-md-5">
                            <label for="observaciones" class="control-label">Observaciones</label>
                            <input id="observaciones" type="text" class="form-control input-sm" name="observaciones" value="{{ $proveedor->observaciones }} " {{$editable}}>
                        </div>
                        <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-1 col-md-1 pull-left">
                            <label for="activo" class="control-label">Activo</label>
                            <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" {{$checked}} {{$disabled}}>
                        </div>

                        <div class="form-group col-sm-6 col-md-4 col-md-3">
                            <label for="created_at" class="control-label">Fecha Alta</label>
                            <input id="created_at" type="text" class="form-control input-sm text-uppercase" name="created_at" readonly value="{{ $proveedor->created_at }}">
                        </div>

                        <div class="form-group col-sm-6 col-md-6 col-md-3">
                            <label for="updated_at" class="control-label">Fecha Ultima Edicion</label>
                            <input id="updated_at" type="text" class="form-control input-sm text-uppercase" name="updated_at" readonly value="{{ $proveedor->updated_at }}">
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <a class="btn btn-primary minw90" href="/{{$urlkey}}"><i class="fa fa-btn fa-arrow-left"></i> Salir</a>
                        @if ($edit==1)
                            <button type="submit" name="action" value="save" class="btn btn-primary minw90">
                                <i class="fa fa-btn fa-floppy-o"></i> Guardar</a>
                            </button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">

<style>
    .pagination-info{
        display: none !important;
    }
</style>
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script type='text/javascript'>
    $('#activo').bootstrapToggle();
    $('#id_tipoiva').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_localidad').select2({
        placeholder: "",
        allowClear: true
    });

    $(window).on('load', function(event){
        document.getElementById("descripcion").focus();
        //-----------------------------------------------------------------------------------------------------
    });
</script>
@include('utils.statusnotification')
@stop
