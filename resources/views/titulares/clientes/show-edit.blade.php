@extends('adminlte::page')

@section('title', $titulo)

@php
if ($edit==0){
    $editable='readonly';
    $visible='d-none';
    $disabled='disabled';
    $accion = 'Visualizar';
}else{
    $editable=' ';
    $visible=' ';
    $disabled=' ';
    $accion = 'Editar';
}
$checked = $cliente->activo == 1 ? 'checked':'';
@endphp

@section('content_header')
    @include('layouts.clientesnavbar')
@stop




@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-primary pt-50">
            <form id="form1" role="form" method="POST" action="/clientes/update/{{$cliente->id}}">
                {{ csrf_field() }}
                <div class="box-body">
                    <input id="id_tipotitular" type="hidden" name="id_tipotitular" value="3">

                    <div class="row">
                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-6 col-md-6">
                            <label for="descripcion" class="control-label">Nombre</label>
                            <input id="descripcion" type="text" class="form-control input-sm text-uppercase" name="descripcion" value="{{ $cliente->descripcion }}" {{$editable}}>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-3 col-md-3{{ $errors->has('tipo') ? ' has-error' : '' }}">
                            <label for="tipo" class="control-label">Tipo
                                <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="@lang('messages.help.tipocliente')">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                            </label>
                            <select id="tipo" name="tipo" type="button" class="form-control" data-toggle="dropdown" {{$disabled}}>
                                <option value=""></option>
                                @foreach(DB::table('sys_tipoclientes')->get() as $tipo)
                                <option  value="{{$tipo->id}}" {{ $cliente->id_tipocliente ==  $tipo->id ? 'selected' : '' }}>{{$tipo->descripcion}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-3 col-md-3{{ $errors->has('id_tipoiva') ? ' has-error' : '' }}">
                            <label for="id_tipoiva" class="control-label">Categ. IVA
                                <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="@lang('messages.help.tipoiva')">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                            </label>
                            <select id="id_tipoiva" name="id_tipoiva" type="button" class="form-control" data-toggle="dropdown" {{$disabled}}>
                                <option value=""></option>
                                @foreach(DB::table('sys_tipoiva')->where('activo', 1)->get() as $tipoiva)
                                <option value="{{$tipoiva->id}}" {{ $cliente->id_tipoiva ==  $tipoiva->id ? 'selected' : '' }}>{{$tipoiva->descripcion}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group {{ $errors->has('cuit') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-3">
                            <label for="cuit" class="control-label">Ciut</label>
                            <input id="cuit" type="text" class="form-control input-sm text-uppercase" name="cuit" value="{{ $cliente->cuit }}"  {{$editable}}>
                        </div>

                        <div class="form-group {{ $errors->has('direccion') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-4 col-md-4">
                            <label for="direccion" class="control-label">Direccion</label>
                            <input id="direccion" type="text" class="form-control input-sm text-uppercase" name="direccion" value="{{ $cliente->direccion }}" {{$editable}}>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-3 col-md-3{{ $errors->has('id_localidad') ? ' has-error' : '' }}">
                            <label for="id_localidad" class="control-label">Localidad</label>
                            <select id="id_localidad" name="id_localidad" type="button" class="form-control" data-toggle="dropdown" {{$disabled}}>
                                <option value=""></option>
                                @foreach(DB::table('conf_localidades')->where('activo', 1)->get() as $localidad)
                                <option value="{{$localidad->id}}" {{ $cliente->id_localidad ==  $localidad->id ? 'selected' : '' }}>{{$localidad->descripcion}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="clearfix visible-lg-block visible-md-block"></div>
                        <div class="form-group {{ $errors->has('telefono') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-3">
                            <label for="telefono" class="control-label">Telefono</label>
                            <input id="telefono" type="text" class="form-control input-sm text-uppercase" name="telefono" value="{{ $cliente->telefono }}"  {{$editable}}>
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-3">
                            <label for="email" class="control-label">Email</label>
                            <input id="email" type="text" class="form-control input-sm text-uppercase" name="email" value="{{ $cliente->email }}" {{$editable}}>
                        </div>
                        <div class="form-group {{ $errors->has('fecha_cumple') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-3">
                            <label for="fecha_cumple" class="control-label">F.Nacimiento / Cumpleaños</label>
                            <input id="fecha_cumple" type="date" class="form-control input-sm" name="fecha_cumple" value="{{ $cliente->fecha_cumple }}"  {{$editable}}>
                        </div>
                        <div class="clearfix visible-lg-block visible-md-block"></div>
                        <div class="form-group{{ $errors->has('observaciones') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-5 col-md-5">
                            <label for="observaciones" class="control-label">Observaciones</label>
                            <input id="observaciones" type="text" class="form-control input-sm" name="observaciones" value="{{ $cliente->observaciones }} " {{$editable}}>
                        </div>
                        <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-1 col-md-1 pull-left">
                            <label for="activo" class="control-label">Activo</label>
                            <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" {{$checked}} {{$disabled}}>
                        </div>

                        <div class="form-group col-sm-6 col-md-4 col-md-3">
                            <label for="created_at" class="control-label">Fecha Alta</label>
                            <input id="created_at" type="text" class="form-control input-sm text-uppercase" name="created_at" readonly value="{{ $cliente->created_at }}">
                        </div>

                        <div class="form-group col-sm-6 col-md-6 col-md-3">
                            <label for="updated_at" class="control-label">Fecha Ultima Edicion</label>
                            <input id="updated_at" type="text" class="form-control input-sm text-uppercase" name="updated_at" readonly value="{{ $cliente->updated_at }}">
                        </div>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <a class="btn btn-primary minw90" href="/{{$urlkey}}"><i class="fa fa-btn fa-arrow-left"></i> Salir</a>
                        @if ($edit==1)
                            <button type="submit" name="action" value="save" class="btn btn-primary minw90">
                                <i class="fa fa-btn fa-floppy-o"></i> Guardar</a>
                            </button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">

<style>
    .pagination-info{
        display: none !important;
    }
</style>
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script type='text/javascript'>
    $(function () {
        $('[data-toggle="popover"]').popover();
    });
    $('#activo').bootstrapToggle();
    $('#tipo').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_tipoiva').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_localidad').select2({
        placeholder: "",
        allowClear: true
    });
    $(window).on('load', function(event){
        document.getElementById("descripcion").focus();
    });
</script>
@include('utils.statusnotification')
@stop
