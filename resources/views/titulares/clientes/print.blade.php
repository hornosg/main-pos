<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>MainPOS</title>
    <style>
        body{
            font-size: 12px;
        }
        table{
            width: 100%;
            border-collapse: collapse;
        }
        td{
            border: 1px solid #000000;
            vertical-align: top;
        }
    </style>
</head>
<body>
<table>
    <tr>
        <td style="height:168px; width: 50%; text-align: center;padding: 10px;">
            <img src="/img/Logos/main_pos.png" style="width: 250px;height: auto;padding: 1%;text-align: center; background-color: #000000">
            <p>Calsasasale 1234, Lsasauján, Buenos Aies - Tel:0111540470815 - hornosg@gmail.com - www.mainpos.com.ar</p>
        </td>
        <td style="height:168px;">
            @php
                $date = new DateTime();
                $fecha =  $date->format('d-m-Y');
            @endphp
            <h2 style="top:0px; margin: 5px;">DETALLE DE MOVIMIENTOS EN CUENTA CORRIENTE</h2>
            <h3 style="float: right;padding-right: 10px;">FECHA: {{$fecha}}</h3>
        </td>
    </tr>
    <tr>
        <td style="height:60px;padding-left: 10px;" colspan="2">
            <p>Cliente: {{$titular->descripcion}}</p>
            <p>Domicilio: {{$titular->direccion}}</p>
<!--            <p>Iva: @if (!empty($titular->tipoiva)) {{$titular->tipoiva->descripcion}}@endif <span style="margin-left:100px;">CUIT:{{$titular->cuit}}</span></p>-->
        </td>
    </tr>
    <tr>
        <td style="height:595px;" colspan="2">
            <table>
                <tr>
                    <td style="font-weight: bold;border: 1px solid #000000;">ID</td>
                    <td style="font-weight: bold;border: 1px solid #000000;">FECHA</td>
                    <td style="font-weight: bold;border: 1px solid #000000;">COMPROBANTE</td>
                    <td style="font-weight: bold;border: 1px solid #000000;">NUMERO</td>
                    <td style="font-weight: bold;border: 1px solid #000000;">IMPORTE</td>
                    <td style="font-weight: bold;border: 1px solid #000000;">SALDO</td>
                </tr>
                @foreach ($items as $item)
                    <tr>
                        <td style="text-align: right;padding-right: 10px;border: 1px solid #000000;">{{$item->id}}</td>
                        <td style=";padding-left: 10px;border: 1px solid #000000;">{{$item->creado}}</td>
                        <td style="text-align: right;padding-right: 10px;border: 1px solid #000000;">{{$item->tipo}}</td>
                        <td style="text-align: right;padding-right: 10px;border: 1px solid #000000;"></td>
                        <td style="text-align: right;padding-right: 10px;border: 1px solid #000000;">{{$item->saldo}}</td>
                        <td style="text-align: right;padding-right: 10px;border: 1px solid #000000;">{{$item->saldototal}}</td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
    <tr>
        <td style="height:40px;" colspan="2">
               <h2>Saldo Total: $<span style="font-weight: bold">{{$saldo}}</span></h2>
               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
        </td>
    </tr>
</table>
<img src="/img/Logos/main_pos.png" style="width: auto;height: 15px;background-color:#000000; padding: 5px; margin-top: 5px;">
<span style="margin: 5px:">www.mainpos.com.ar</span>
</body>
</html>