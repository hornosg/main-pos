@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.clientesnavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pr-10 pt-10 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Saldo Total</span>
                                    <span class="info-box-number saldo"></span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 100%"></div>
                                    </div>
                                      <span class="progress-description">
                                        100%
                                      </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <div class="pr-10 pt-10 col-md-2 hidden-sm hidden-xs">
                            <div class="info-box bg-calor4">
                                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                                <div class="info-box-content">
                                        <span class="info-box-text">Incobrable
                                            <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="Moroso Nivel 3: Saldo vencido más de 6 Meses" style="color:white">
                                                <i class="fa fa-info-circle"></i>
                                            </a>
                                        </span>
                                    <span class="info-box-number saldov3"></span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 100%"></div>
                                    </div>
                                    <span class="progress-description pm3"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <div class="pr-10 pt-10 col-md-2 hidden-sm hidden-xs">
                            <div class="info-box bg-calor3">
                                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                                <div class="info-box-content">
                                        <span class="info-box-text">Moroso N2
                                            <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="Moroso Nivel 2: Saldo vencido entre 4 y 6 Meses" style="color:white">
                                                <i class="fa fa-info-circle"></i>
                                            </a>
                                        </span>
                                    <span class="info-box-number saldov2"></span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 100%"></div>
                                    </div>
                                    <span class="progress-description pm2"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <div class="pr-10 pt-10 col-md-2 hidden-sm hidden-xs">
                            <div class="info-box bg-calor2">
                                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                                <div class="info-box-content">
                                        <span class="info-box-text">Moroso N1
                                            <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="Moroso Nivel 1: Saldo vencido entre 1 y 3 Meses" style="color:white">
                                                <i class="fa fa-info-circle"></i>
                                            </a>
                                        </span>
                                    <span class="info-box-number saldov1"></span>
                                    <div class="progress">
                                        <div class="progress-bar" style="width: 100%"></div>
                                    </div>
                                    <span class="progress-description pm1"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <div class="pr-10 pt-10 col-md-2 hidden-sm hidden-xs">
                            <div class="info-box bg-calor1">
                                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Sin vencer</span>
                                    <span class="info-box-number saldoh15"></span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 100%"></div>
                                    </div>
                                    <span class="progress-description psv"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div id="toolbar"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-pagination-first-text='Primera'
                               data-pagination-pre-text='Ant.'
                               data-pagination-next-text='Sig.'
                               data-pagination-last-text='Ultima'
                               data-sort-name='id'
                               data-sort-order='desc'>
                            <thead class='thead-inverse'>
                                <tr>
                                    <th rowspan="2" data-field='id_titular' data-align='right' class='hidden'>Id</th>
                                    <th rowspan="2" data-field='cliente' class='text-uppercase'>Cliente</th>
                                    <th rowspan="2" data-field='sinvencer' data-align='right' class='text-uppercase'>Sin Vencer</th>
                                    <th colspan='3' data-align='center' class='text-uppercase'>Vencidos</th>
                                    <th rowspan="2" data-field='saldo' data-align='right' class='text-uppercase'>saldo</th>
                                    <th rowspan="2" data-align='center' data-formatter='FunctionsFormatter'></th>
                                </tr>
                                <tr>
                                    <th data-field='m1' data-align='right' class='text-uppercase'>[15-90] dias</th>
                                    <th data-field='m2' data-align='right' class='text-uppercase'>[91-180] dias</th>
                                    <th data-field='m3' data-align='right' class='text-uppercase'>[>180] dias</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<!--<script src="/js/bootstrap-table-filter.js"></script>-->
<!--<script src="/js/bootstrap-table-filter-control.js"></script>-->
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script language='JavaScript' type='text/javascript'>
    $('#table1').bootstrapTable({});

    $(window).on('load', function(event){
        $('#table1').bootstrapTable('showLoading');
        $('.overlay').removeClass('d-none');
        $.ajax('/data/vencidos/gettotales', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                var total = data.saldo;
                document.querySelector('.saldo').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                var totalsv = data.saldosv;
                document.querySelector('.saldoh15').innerHTML = totalsv.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                document.querySelector('.psv').innerHTML = data.porcsv + '%';
                var totalv1 = data.saldov1;
                document.querySelector('.saldov1').innerHTML = totalv1.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                document.querySelector('.pm1').innerHTML = data.porcv1 + '%';
                var totalv2 = data.saldov2;
                document.querySelector('.saldov2').innerHTML = totalv2.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                document.querySelector('.pm2').innerHTML = data.porcv2 + '%';
                var totalv3 = data.saldov3;
                document.querySelector('.saldov3').innerHTML = totalv3.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
                document.querySelector('.pm3').innerHTML = data.porcv3 + '%';
                $('#table1').bootstrapTable('append', data.items);
                $('#table1').bootstrapTable('hideLoading');
                $('.overlay').addClass('d-none');
            },
            error:function(data) {
                console.log(data);
                iziToast.error({
                    timeout: 2000,
                    position:'center',
                    title: 'Error:',
                    message: data.responseJSON.message
                });
                $('#table1').bootstrapTable('hideLoading');
                $('.overlay').addClass('d-none');
            }
        });
    });

    function FunctionsFormatter(value, row, index) {
        return "<a class='btn-outline-primary' href='/clientes/cuentacorriente/"+row.id_titular+"' target='_blank'><i class='fa fa-fw fa-search'></i></a>"
    };
</script>
@include('utils.statusnotification')
@stop
