@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.clientesnavbar')
@stop


@php
    //dd($saldosv);
    if ($saldo>0){
        $bgsaldo='bg-red';
    }else{
        $bgsaldo='bg-green';
    }
@endphp

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pr-10 pt-10 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box {{$bgsaldo}}">
                                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">SALDO TOTAL</span>
                                    <span class="info-box-number saldo"></span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 100%"></div>
                                    </div>
                                      <span class="progress-description">
                                        100%
                                      </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        @if ($saldov3>0)
                            <div class="pr-10 pt-10 col-md-2 hidden-sm hidden-xs">
                                <div class="info-box bg-calor4">
                                    <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Incobrable
                                            <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="Moroso Nivel 3: Saldo vencido más de 6 Meses" style="color:white">
                                                <i class="fa fa-info-circle"></i>
                                            </a>
                                        </span>
                                        <span class="info-box-number saldov3"></span>

                                        <div class="progress">
                                            <div class="progress-bar" style="width: {{$porcv3}}%"></div>
                                        </div>
                                        <span class="progress-description">
                                            {{$porcv3}}%
                                          </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        @endif
                        @if ($saldov2>0)
                            <div class="pr-10 pt-10 col-md-2 hidden-sm hidden-xs">
                                <div class="info-box bg-calor3">
                                    <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Moroso N2
                                            <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="Moroso Nivel 2: Saldo vencido entre 4 y 6 Meses" style="color:white">
                                                <i class="fa fa-info-circle"></i>
                                            </a>
                                        </span>
                                        <span class="info-box-number saldov2"></span>

                                        <div class="progress">
                                            <div class="progress-bar" style="width: {{$porcv2}}%"></div>
                                        </div>
                                        <span class="progress-description">
                                            {{$porcv2}}%
                                          </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        @endif
                        @if ($saldov1>0)
                            <div class="pr-10 pt-10 col-md-2 hidden-sm hidden-xs">
                                <div class="info-box bg-calor2">
                                    <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Moroso N1
                                            <a type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="Moroso Nivel 1: Saldo vencido entre 15 dias y 3 Meses" style="color:white">
                                                <i class="fa fa-info-circle"></i>
                                            </a>
                                        </span>
                                        <span class="info-box-number saldov1"></span>
                                        <div class="progress">
                                            <div class="progress-bar" style="width: {{$porcv1}}%"></div>
                                        </div>
                                          <span class="progress-description">
                                            {{$porcv1}}%
                                          </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        @endif
                        @if ($saldosv>0)
                            <div class="pr-10 pt-10 col-md-2 hidden-sm hidden-xs">
                                <div class="info-box bg-calor1">
                                    <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Sin vencer</span>
                                        <span class="info-box-number saldoh15"></span>

                                        <div class="progress">
                                            <div class="progress-bar" style="width: {{$porcsv}}%"></div>
                                        </div>
                                        <span class="progress-description">
                                            {{$porcsv}}%
                                          </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        @endif
                        @if (!empty($items))
                            <div class="pull-right pt-10 col-md-2 hidden-sm hidden-xs">
                                <h2><a class='pull-right btn-outline-primary' href='/clientes/cc/print/{{$idcliente}}' title="Imprimir"><i class='fa fa-print'></i></a></h2>
                                {{--@if ( (Auth::user()->id_rol==1) or (Auth::user()->id_rol==2) )--}}
                                    <h2><a class='pull-right btn-outline-danger' href='/clientes/cc/repararestados/{{$idcliente}}' title="Actualizar Estados de Comprobantes"><i class='fa fa-wrench'></i></a></h2>
                                    <h2><a class='pull-right btn-outline-danger' href='/clientes/cc/actualizarprecios/{{$idcliente}}' title="Actualizar Precios en Remitos Pendientes"><i class='fa fa-refresh'></i></a></h2>
                                {{--@endif--}}
                                <h2><a class='pull-right btn-outline-gray' href='#' title="Gestion de Cobranzas"><i class='fa fa-phone-square'></i></a></h2>

                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div id="toolbar">
                    <form class="form-inline">
                        <div class="col-300">
                            <select id="id_titular" name="id_titular" class="form-control text-uppercase">
                                <option value=""></option>
                                @if (!empty($idcliente))
                                    @foreach(DB::table('titulares')->where('id_tipotitular', 2)->where('activo', 1)->get() as $tipotitular)
                                        @if ( $idcliente == $tipotitular->id )
                                            <option value="{{ $tipotitular->id }}" selected>{{ $tipotitular->descripcion }}</option>
                                        @else
                                            <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                        @endif
                                    @endforeach
                                @else
                                    @foreach(DB::table('titulares')->where('id_tipotitular', 2)->where('activo', 1)->get() as $tipotitular)
                                        <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-buttons-align="right"
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size='50'
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-unique-id='id'>
                        <thead class='thead-inverse'>
                                <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                                <th data-field='creado'   class='text-uppercase'>fecha</th>
                                <th data-field='tipo'   class='text-uppercase'>comprobante</th>
                                <th data-field='numero'   class='text-uppercase'>numero</th>
                                <th data-field='estado'  class='text-uppercase' data-formatter='statusFormatter'>estado</th>
                                @if ($saldo>0)
                                <th data-field='pasaron'  data-align='right' class='text-uppercase' data-formatter='daysstatusFormatter'>dias</th>
                                @endif
                                <th data-field='debe'  data-align='right' data-formatter='moneyFormatter'>DEBE</th>
                                <th data-field='haber'  data-align='right' data-formatter='moneyFormatter'>HABER</th>
                                @if ($saldo>0)
                                <th data-field='saldopend' data-align='right' data-formatter='moneyFormatter'>SALDO COMPROBANTE</th>
                                @endif
                                <th data-field='saldototal' data-align='right' data-formatter='moneySaldoFormatter'>HISTORIAL SALDO</th>
                                <th data-formatter='FunctionsFormatter'></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer ptb-10">
                <div class="pull-right pr-5">
                    <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<style>
    .col-300{
        width: 300px;
    }
</style>
@stop

@section('js')
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
@include('layouts.s2multiplesearch')
<script language='JavaScript' type='text/javascript'>
    var $table = $('#table1');
    $(function () {
        var data =  <?php echo json_encode($items); ?>;
        $('#table1').bootstrapTable({data: data});
    });
    $(document.body).addClass('sidebar-collapse');
    $(function () {
        $('[data-toggle="popover"]').popover()
    });

    $("#id_titular").select2({ placeholder: "Buscar/Seleccionar Cliente", allowClear: true, style:"font-weight: 700;" });

    var total = <?php echo $saldo; ?>;
    document.querySelector('.saldo').innerHTML = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
    if ($('.saldoh15').length > 0) {
        var totalsv = <?php echo $saldosv; ?>;
        document.querySelector('.saldoh15').innerHTML = totalsv.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
    }
    if ($('.saldov1').length > 0) {
        var totalv1 = <?php echo $saldov1; ?>;
        document.querySelector('.saldov1').innerHTML = totalv1.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
    }
    if ($('.saldov2').length > 0) {
        var totalv2 = <?php echo $saldov2; ?>;
        document.querySelector('.saldov2').innerHTML = totalv2.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
    }
    if ($('.saldov3').length > 0) {
        var totalv3 = <?php echo $saldov3; ?>;
        document.querySelector('.saldov3').innerHTML = totalv3.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,');
    }

    function moneyFormatter(value, row, index) {
        if (value!=0){
            return  "$ "+value;
        }else{
            return '';
        }
    };
    function moneySaldoFormatter(value, row, index) {
        return  "$ "+value;
    };

    function FunctionsFormatter(value, row, index) {
        var cpte='';
        var links = '';

        if (row.id_cpterel > 1){
            if (row.tipo==="NOTA DE DEBITO") {
                cpte = 'nd';
            }else if (row.tipo==="NOTA DE CREDITO") {
                cpte = 'nc';
            }
            links = "<a class='btn-outline-primary' href='/cptes/show/"+cpte+"/"+row.id_cpterel+"' target='_blank'><i class='fa fa-fw fa-registered'></i></a>" +
                    "<a class='btn-outline-primary' href='/cptes/show/"+cpte+"/"+row.id+"' target='_blank'><i class='fa fa-fw fa-search'></i></a>";
        }else{
            if(row.tipo==="REMITO"){
                cpte = 'rem';
            }else if (row.tipo==="FACTURA") {
                cpte = 'fc';
            }else if (row.tipo==="RECIBO") {
                cpte = 'rbo';
            }else if (row.tipo==="NOTA DE CREDITO") {
                cpte = 'nc';
            }

            if (row.pasaron<16){
                if (row.id_estado===2){
                    links = "<a class='btn-outline-danger' href='#' onclick='Borrar("+row.id+")' title='Eliminar'><i class='fa fa-fw fa-times-circle'></i></a>" +
                            "<a class='btn-outline-warning' href='/cptes/edit/"+cpte+"/"+row.id+"' target='_blank'><i class='fa fa-fw fa-pencil'></i></a>";
                }
            }
            links = links + "<a class='btn-outline-primary' href='/cptes/show/"+cpte+"/"+row.id+"' target='_blank'><i class='fa fa-fw fa-search'></i></a>" +
                            "<a class='btn-outline-primary' href='/cptes/print/"+cpte+"/"+row.id+"'><i class='fa fa-fw fa-print'></i></a>";
        }
        return links;
    };

    function statusFormatter(value, row, index) {
        if(value==="PAGO"){
            return '<span class="badge bg-green">' + value + '</span>';
        }else if (value==="REGISTRADO") {
            return '<span class="badge bg-detfault">' + value + '</span>';
        }else if (value==="PENDIENTE") {
            var bg = '';
            if (row.pasaron < 15){
                bg = 'badge bg-calor1 text-black';
            }else{
                if (row.pasaron > 180){
                    bg = 'badge bg-calor4 text-black';
                }else{
                    if (row.pasaron > 90){
                        bg = 'badge bg-calor3 text-black';
                    }else{
                        bg = 'badge bg-calor2 text-black';
                    }
                }
            }
            return '<span class="' + bg + '">' + value + '</span>';
        }
    };

    function daysstatusFormatter(value, row, index) {
        if(row.id_estado===2){
            var bg = '';
            if (row.pasaron < 15){
                bg = 'badge bg-calor1 text-black';
            }else{
                if (row.pasaron > 180){
                    bg = 'badge bg-calor4 text-black';
                }else{
                    if (row.pasaron > 90){
                        bg = 'badge bg-calor3 text-black';
                    }else{
                        bg = 'badge bg-calor2 text-black';
                    }
                }
            }
            return '<span class="' + bg + '">' + value + '</span>';
        }else {
            return null;
        }
    };

    $("#id_titular").change(function(){
        var combocliente = document.getElementById("id_titular");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        $(location).attr('href','/clientes/cuentacorriente/'+idcliente);
    });

    function Borrar(idrow){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $.ajax({
                        type: "GET",
                        url:'/cptes/delete/'+idrow,
                        data: idrow,
                        success: function(data) {
                            window.location.href = "<?php echo URL::to('/clientes/cuentacorriente/'.$idcliente); ?>";
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });

                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }
</script>
@include('utils.statusnotification')
@stop
