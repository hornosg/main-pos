@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.clientesnavbar')
@stop

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <a href="/{{$urlkey}}/create" class="btn btn-primary"  title="Nuevo">
                                    <i class="fa fa-btn fa-plus text-white"></i>
                                </a>
                                <button id="btn-eliminar" class="btn btn-danger"  title="Eliminar Seleccion">
                                    <i class="fa fa-btn fa-trash text-white"></i>
                                </button>
                                <a href="/{{$urlkey}}/import" class="btn btn-info hidden-xs"  title="Importar Datos">
                                    <i class="fa fa-btn fa-upload text-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-pagination-first-text='Primera'
                               data-pagination-pre-text='Ant.'
                               data-pagination-next-text='Sig.'
                               data-pagination-last-text='Ultima'
                               data-sort-name='id'
                               data-sort-order='desc'>

                            <thead class='thead-inverse'>
                                <th data-checkbox='true'></th>
                                <th data-field='id' data-align='right' class='hidden text-uppercase'>Id</th>
                                <th data-field='descripcion'   class='col-md-2 text-uppercase'>Descripcion</th>
                                <th data-field='direccion'   class='col-md-2 text-uppercase'>Direccion</th>
                                <th data-field='localidad'   class='text-uppercase'>Localidad</th>
                                <th data-field='telefono'  class='stext-uppercase'>Telefono</th>
                                <th data-field='email'  class='text-uppercase'>Email</th>
                                <th data-field='tipo' class='col-md-1 text-uppercase'>Tipo</th>
                                <th data-field='activo' data-align='col-md-1 center' class='text-uppercase'>Activo</th>
                                <th data-align='center' class='col-md-2' data-formatter='FunctionsFormatter'></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
foreach ($clientes as $cliente) {
    $cliente->activo = $cliente->activo == 1 ? 'si':'no';
}
?>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<!--<script src="/js/bootstrap-table-filter.js"></script>-->
<!--<script src="/js/bootstrap-table-filter-control.js"></script>-->
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script language='JavaScript' type='text/javascript'>
    var datos = <?php echo json_encode($clientes); ?>;
    $('#table1').bootstrapTable({data: datos,exportDataType:'all',exportOptions:{fileName: 'clientes'}});
    $(document.body).addClass('sidebar-collapse');

    function FunctionsFormatter(value, row, index) {
        if (row.id!=2) {
            return "<a class='btn-outline-danger' href='/{{$urlkey}}/delete/"+row.id+"'><i class='fa fa-fw fa-times-circle'></i></a>" +
                "<a class='btn-outline-warning' href='/{{$urlkey}}/edit/"+row.id+"'><i class='fa fa-fw fa-pencil'></i></a>" +
                "<a class='btn-outline-primary' href='/{{$urlkey}}/show/"+row.id+"'><i class='fa fa-fw fa-search'></i></a>";
        }else{
            return "<a class='btn-outline-primary' href='/{{$urlkey}}/show/"+row.id+"'><i class='fa fa-fw fa-search'></i></a>";
        }
    };

    $(function () {
        $('#btn-eliminar').click(function (e) {
            e.preventDefault();
            var choice;
            iziToast.question({
                close: true,
                overlay: true,
                displayMode: 'once',
                id: 'question',
                zindex: 999,
                title: 'Atencion!',
                message: '¿Está seguro de Eliminar todos los items seleccionados?',
                position: 'center',
                buttons: [
                    ['<button><b>SI</b></button>', function (instance, toast) {
                        var ids = $.map($('#table1').bootstrapTable('getAllSelections'), function (row) {
                            return row.id;
                        });
                        $.ajax('/{{$urlkey}}/removeselected', {
                            type: 'POST',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: {datos:ids},
                            success: function(data){
                                $('#table1').bootstrapTable('remove', {
                                    field: 'id',
                                    values: ids
                                });
                                iziToast.success({
                                    timeout: 2000,
                                    position:'topCenter',
                                    title: 'Listo!',
                                    message: 'Items, Eliminados.'
                                });
                            },
                            error:function(data) {
                                console.log(data);
                                iziToast.error({
                                    timeout: 2000,
                                    position:'center',
                                    title: 'Error:',
                                    message: data.responseJSON.message
                                });
                            }
                        });
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    }, true],
                    ['<button>NO</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    }]
                ]
            });
        });
    });
</script>
@include('utils.statusnotification')
@stop
