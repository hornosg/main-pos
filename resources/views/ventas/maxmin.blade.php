@extends('adminlte::page')
@section('title', 'MainPOS')

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<style>
    #chart2,#chart3{
        height: 241px !important;
    }
    .small-box p{
        font-weight: bold;
        text-align: center;
        margin-bottom: 0 !important;
    }
    .small-box{
        margin-bottom: 5px !important;
        border-top: 3px solid #3c8dbc;
        border-top-width: 3px;
        border-top-style: solid;
    }
    .small-box:hover {
        color: black !important;
    }
</style>
@stop

@section('content_header')
    @include('layouts.ventasnavbar')
@stop

@section('content')
<!-- Main row -->
<div class="row">
    <div class="col-md-2">
        <div class="small-box bg-white">
            <p>Ticket Promedio</p>
            <div id="chart2"></div>
        </div>
        <div class="small-box bg-white">
            <p>Ticket Maximo</p>
            <div id="chart3"></div>
        </div>
    </div>
    <!-- Left col -->
    <div class="col-md-7">
        <div class="box box-primary">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar"></div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-detail-view='true'
                               data-unique-id='id'>

                            <thead class='thead-inverse'>
                            <th data-field='id' data-align='right' class='hidden'>Id</th>
                            <th data-field='creado' data-align='right'  class='text-uppercase'>Creado</th>
                            <th data-field='numero'>NUMERO</th>
                            <th data-field='id_titular' class='hidden'></th>
                            <th data-field='titular'  data-formatter="clienteFormatter" class='text-uppercase  text-bold'>Cliente</th>
                            <th data-field='tipocliente'  class='text-uppercase text-bold'>Cliente</th>
                            <th data-field='total' data-align='right' data-formatter="moneyFormatter" class='text-uppercase text-bold'>Importe</th>
                            <th data-field='usuario' data-align='left' class='text-uppercase'>Usuario</th>
                            <th data-field='id_estado' data-align='left' data-formatter='FunctionsStatusFormatter'>ESTADO</th>
                            <th data-align='center' data-formatter='FunctionsFormatter'></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col -->
    <div class="col-xs-12 col-md-3">
        <div class="graph-container bg-white">
            <div class="caption">Ventas por Rubro (Ultimos 12 meses)</div>
            <div id="chart1" class="graph"></div>
        </div>
    </div>
</div>
@stop


@section('js')
<!-- Bootstrap-Table -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<!-- Morris Chart JS-->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script language="javascript" type="text/javascript">
    var datos = <?php echo json_encode($items); ?>;
    var $table = $('#table1');
    $table.bootstrapTable({
        data: datos,
        exportDataType:'all',
        exportOptions:{fileName: 'items'}
    });

    var datos = <?php echo json_encode($rubros); ?>;
    if (datos.length > 1 ) {
        new Morris.Donut({
            element: 'chart1',
            colors: [
                '#605ca8',
                '#39CCCC',
                '#00c0ef',
                '#95D7BB'
            ],
            data: datos,
            resize: true
        });
    }

    datos2 = <?php echo json_encode($promedios); ?>;
    if (datos2.length > 1 ) {
        new Morris.Donut({
            element: 'chart2',
            colors: [
                '#605ca8',
                '#39CCCC',
                '#00c0ef',
                '#95D7BB'
            ],
            data: datos2,
            resize: true
        });
    }

    datos3 = <?php echo json_encode($maximos); ?>;
    if (datos3.length > 1 ) {
        new Morris.Donut({
            element: 'chart3',
            colors: [
                '#605ca8',
                '#39CCCC',
                '#00c0ef',
                '#95D7BB'
            ],
            data: datos3,
            resize: true
        });
    }

    function moneyFormatter(value, row, index) {
        return  "$ "+value;
    };

    function clienteFormatter(value, row, index) {
        return  "<a href='/clientes/cuentacorriente/"+row.id_titular+"'>"+value+"</a>";
    };

    function FunctionsFormatter(value, row, index) {
        var name='';
        if(row.id_tipocpte==5){
            name='rem';
        }
        if(row.id_tipocpte==6){
            name='fc';
        }
        return "<a class='btn-outline-primary' href='/cptes/show/"+name+"/"+row.id+"'><i class='fa fa-fw fa-search'></i></a>" +
            "<a class='btn-outline-primary' href='/cptes/print/"+name+"/"+row.id+"'><i class='fa fa-fw fa-print'></i></a>";
    };

    function FunctionsStatusFormatter(value, row, index) {
        if(row.id_estado===1){
            return '<span class="badge bg-green">PAGO</span>';
        }
        if(row.id_estado===2){
            return '<span class="badge bg-red">PEND</span>';
        }
        if(row.id_estado===3){
            return '<span class="badge bg-primary">REG</span>';
        }
    };

    $table.on('expand-row.bs.table', function (e, index, row, $detail) {
        $idtabla2="tabla"+row.id;
        html='<div class="box"><table id='+$idtabla2+' class="table table-sm table-bordered table-hover table-striped table-condensed table-responsive" ' +
            'data-search="true" data-strict-search="false" data-multiple-search="true" data-pagination="true">' +
            '<thead class="thead-inverse-gray"><tr>' +
            '<th data-field="id" data-align="right">Codigo</th>' +
            '<th data-field="descripcion">Descripcion</th>' +
            '<th data-field="marca">Marca</th>' +
            '<th data-field="preciouni" data-align="right">Precio Unitario</th>' +
            '<th data-field="cantidad" data-align="right">Cantidad</th>' +
            '</tr></thead>' +
            '</table></divS>';
        $detail.html(html);
        $('#'+$idtabla2).bootstrapTable();
        $('#'+$idtabla2).bootstrapTable('showLoading');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'/cptes/detalle/'+row.id,
            data: row.id,
            success: function(datosdetalle) {
                $('#'+$idtabla2).bootstrapTable('append', datosdetalle);
                $('#'+$idtabla2).bootstrapTable('hideLoading');
            }
        });
    });

    function Borrar(idrow){
        iziToast.question({
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Atencion!',
            message: 'Está seguro de Eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    $.ajax({
                        type: "GET",
                        url:'/cptes/delete/'+idrow,
                        data: idrow,
                        success: function(data) {
                            $('#table1').bootstrapTable('removeByUniqueId', idrow);
                            iziToast.success({
                                timeout: 2000,
                                position:'topCenter',
                                title: 'Listo!',
                                message: data.msg
                            });
                        }
                    });
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }]
            ]
        });
    }
</script>
@stop