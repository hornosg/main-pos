@extends('adminlte::page')
@section('title', 'MainPOS')

@php
    if(empty($totalvtas)){
        $totalvtas=0;
        $efectivo=0;
        $ctacte=0;
        $tarjetas=0;
        $otrosmp=0;
        $porc_efectivo=0;
        $porc_ctacte=0;
        $porc_tarjetas=0;
        $porc_otrosmp=0;
    }
    $itemnav='';
@endphp


@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<style>
    .info-box{
        min-height: 67px !important;
        margin-bottom: 13px !important;
    }
    .info-box-icon{
        height:67px !important;
        line-height: 67px !important;
        width: 60px !important;
    }
    .info-box-text, .info-box-number{
        display: inline !important;
    }
    .info-box-number {
        font-size: 16px !important;
    }
    .info-box-content{
        padding: 5px 6px
    }
    .bootstrap-table {
        padding-bottom: 0 !important;
    }
    .pt0{
        padding-top: 0 !important;
    }
    .pt10{
        padding-top: 10px !important;
    }
    .pb10{
        padding-bottom: 10px !important;
    }
</style>
@stop

@section('content_header')
    @include('layouts.ventasnavbar')
@stop

@section('content')
    @if (Auth::user()->id_rol==1)
    <div class="row pt-20">
        <div class="col-md-6">
            <!-- small box -->
            <!-- small box -->
            <div class="col-md-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>$ {{$totalvtas}}</h3>

                        <p class="text-bold">Total Ventas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-dashboard"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="small-box bg-maroon">
                    <div class="inner">
                        <h3>$ {{$promedio}}</h3>

                        <p class="text-bold">Ticket Promedio (Clientes Eventuales) </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-ticket"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="graph-container bg-white">
                    <div id="toolbar">
                        <div class="caption pb10">Totales por Tipo de Cliente y Estado</div>
                    </div>
                    <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                           data-toggle='table'
                           data-toolbar="#toolbar"
                           data-show-export='true'>
                        <thead class='thead-inverse'>
                        <th data-field='estado' class='text-uppercase text-bold'>ESTADO</th>
                        <th data-field='eventual' data-align='right' data-formatter="moneyFormatter">EVENTUAL</th>
                        <th data-field='moyorista' data-align='right' data-formatter="moneyFormatter">MAYORISTA</th>
                        <th data-field='empresa' data-align='right' data-formatter="moneyFormatter">EMPRESA</th>
                        <th data-field='profesional' data-align='right' data-formatter="moneyFormatter">PROFESIONAL</th>
                        <th data-field='total' data-align='right' data-formatter="moneyFormatter">TOTAL</th>
                        </thead>
                    </table>
                </div>
            </div>
            {{--<div class="info-box bg-yellow">--}}
                {{--<span class="info-box-icon"><i class="fa fa-bar-chart"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-number promedio3m">...</i></span>--}}
                    {{--<span class="progress-description">Promedio Ventas Ult. 3 Meses</span>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="small-box bg-yellow">--}}
                {{--<div class="inner">--}}
                    {{--<h3 class="diascalle">...</i></h3>--}}

                    {{--<p class="text-bold">Dias en la calle</p>--}}
                {{--</div>--}}
                {{--<div class="icon">--}}
                    {{--<i class="fa fa-road"></i>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
        <div class="col-md-3">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>$ {{$ingresos}}</h3>

                    <p class="text-bold">Total Ingresos</p>
                </div>
                <div class="icon">
                    <i class="fa fa-trophy"></i>
                </div>
            </div>
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Efectivo </span>
                    <span class="info-box-number">$ {{$efectivo}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{$porc_efectivo}}%"></div>
                    </div>
                  <span class="progress-description">
                    {{$porc_efectivo}}% del Total
                  </span>
                </div>
            </div>
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Cheques </span>
                    <span class="info-box-number">$ {{$cheques}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{$porc_cheques}}%"></div>
                    </div>
                  <span class="progress-description">
                    {{$porc_cheques}}% del Total
                  </span>
                </div>
            </div>
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-credit-card"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Tarjetas </span>
                    <span class="info-box-number">$ {{$tarjetas}}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {{$porc_tarjetas}}%"></div>
                    </div>
                  <span class="progress-description">
                    {{$porc_tarjetas}}% del Total
                  </span>
                </div>
            </div>
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-bank"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Dep./Transf. </span>
                    <span class="info-box-number">$ {{$otrosmp}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{$porc_otrosmp}}%"></div>
                    </div>
                  <span class="progress-description">
                    {{$porc_otrosmp}}% del Total
                  </span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="small-box bg-orange-active">
                <div class="inner">
                    <h3>$ {{$ctacte}}</h3>

                    <p class="text-bold">Total a Cuenta Corriente</p>
                </div>
                <div class="icon">
                    <i class="fa fa-pencil-square-o"></i>
                </div>
            </div>
            <div class="info-box bg-orange">
                <span class="info-box-icon"><i class="fa fa-thumbs-o-down"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Vencido del Mes</span>
                    <span class="info-box-number">$ {{$ctactevdo}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{$porc_ctactevdo}}%"></div>
                    </div>
                  <span class="progress-description">
                    {{$porc_ctactevdo}}% del Total
                  </span>
                </div>
            </div>
            <div class="info-box bg-orange disabled">
                <span class="info-box-icon"><i class="fa fa-exclamation-circle"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Sin Vencer </span>
                    <span class="info-box-number">$ {{$ctactesv}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{$porc_ctactesv}}%"></div>
                    </div>
                  <span class="progress-description">
                    {{$porc_ctactesv}}% del Total
                  </span>
                </div>
            </div>
            <div class="info-box bg-orange disabled">
                <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Pago </span>
                    <span class="info-box-number">$ {{$ctactepgo}}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {{$porc_ctactepgo}}%"></div>
                    </div>
                  <span class="progress-description">
                    {{$porc_ctactepgo}}% del Total
                  </span>
                </div>
            </div>
            {{--<div class="info-box bg-red">--}}
                {{--<span class="info-box-icon"><i class="fa fa-warning"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">... </span>--}}
                    {{--<br/>--}}
                    {{--<span class="info-box-number">...</i></span>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
        {{--<div class="col-md-3">--}}
            {{--<div class="small-box bg-red">--}}
                {{--<div class="inner">--}}
                    {{--<h3 class="ctactetotal">...</i></h3>--}}

                    {{--<p class="text-bold">Total Cuenta Corriente</p>--}}
                {{--</div>--}}
                {{--<div class="icon">--}}
                    {{--<i class="fa fa-pencil-square-o"></i>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="info-box bg-red">--}}
                {{--<span class="info-box-icon"><i class="fa fa-warning"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Total Vencido </span>--}}
                    {{--<br/>--}}
                    {{--<span class="info-box-number totalvencido">...</i></span>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="info-box bg-red">--}}
                {{--<span class="info-box-icon"><i class="fa fa-warning"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Total Sin Vencer </span>--}}
                    {{--<br/>--}}
                    {{--<span class="info-box-number totalsvencer">...</i></span>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="info-box bg-red">--}}
                {{--<span class="info-box-icon"><i class="fa fa-warning"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">... </span>--}}
                    {{--<br/>--}}
                    {{--<span class="info-box-number">...</i></span>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="info-box bg-red">--}}
                {{--<span class="info-box-icon"><i class="fa fa-warning"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">... </span>--}}
                    {{--<br/>--}}
                    {{--<span class="info-box-number">...</i></span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
    <div class="row">



    </div>
    @endif

    <div class="row">
        <!-- Porcentaje por Estado -->
        <div class="col-xs-12 col-md-3">
            <div class="graph-container bg-white">
                <div class="caption">Porcentaje por Tipos de Cliente</div>
                <div id="chart2" class="graph"></div>
            </div>
        </div>

        <!-- Porcentaje por Estado -->
        <div class="col-xs-12 col-md-3">
            <div class="graph-container bg-white">
                <div class="caption">Porcentaje de Estados de Comprobantes</div>
                <div id="chart1" class="graph"></div>
            </div>
        </div>

        <!-- Porcentaje por Estado -->
        <div class="col-xs-12 col-md-6">
            <div class="graph-container bg-white">
                <div class="caption">Estados por Tipos de Cliente</div>
                <div id="chart3" class="graph pt10"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="graph-container bg-white">
                <div class="caption">Ventas por Medio Pago</div>
                <div id="hero-graph" class="graph"></div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="graph-container bg-white">
                <div class="caption">Cuenta Corriente: Porcentajes de Pagos vs Pendientes</div>
                <div id="columnchart_material" class="graph"></div>
            </div>
        </div>
    </div>
@stop

@section('js')
<!-- Bootstrap-Table -->
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/vendor/select2/js/select2.full.min.js"></script>
<!-- Morris Chart JS-->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<!-- Google Charts -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script language="javascript" type="text/javascript">
    var datos = <?php echo json_encode($estadosxtipocl); ?>;
    var $table = $('#table1');
    $table.bootstrapTable({data: datos });
    $("#meses").select2({placeholder:"Mes"});
    $("#id_sucursal").select2({placeholder:"Sucursal"});


    function moneyFormatter(value, row, index) {
        if (value==null){
            return "";
        }else{
            return  "$ "+value;
        }
    };

    var vtasxmp = <?php echo json_encode($vtasxmp); ?>;
    if (vtasxmp.length > 1 ) {
        Morris.Line({
            element: 'hero-graph',
            data: vtasxmp,
            xkey: 'period',
            ykeys: ['efectivo', 'ctacte', 'tarjeta', 'otros'],
            labels: ['Efectivo', 'Cta Cte', 'Tarjeta', 'Otros'],
            lineColors: ['#00a65a', '#f39c12', '#0073b7', '#1aaaff'],
            hideHover:true
        });
    }

    var ctate = {!! $ctactevs !!};
    if (ctate.length > 1 ) {
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
    }
    function drawChart() {
        var data = google.visualization.arrayToDataTable({!! $ctactevs !!});
        var options = {
            legend: {position: 'none'}
        };
        var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_material'));
        chart.draw(data,options);
    }

    var datos = <?php echo json_encode($estados); ?>;
    if (datos.length > 1 ) {
        new Morris.Donut({
            element: 'chart1',
            colors: [
                '#605ca8',
                '#39CCCC',
                '#00c0ef',
                '#95D7BB'
            ],
            data: datos,
            resize: true
        });
    }

    var datos2 = <?php echo json_encode($tipoclientes); ?>;
    if (datos2.length > 1 ) {
        new Morris.Donut({
            element: 'chart2',
            colors: [
                '#605ca8',
                '#39CCCC',
                '#00c0ef',
                '#95D7BB'
            ],
            data: datos2,
            resize: true
        });
    }

    var $estadosxtipo;
    google.charts.load('current', {'packages':['treemap']});
    google.charts.setOnLoadCallback(drawChart2);
    function drawChart2() {
        $estadosxtipo = google.visualization.arrayToDataTable(<?php echo json_encode($estadosxtipo); ?>);
        tree = new google.visualization.TreeMap(document.getElementById('chart3'));
        tree.draw($estadosxtipo, {
            minColor: '#7FDBFF',
            midColor: '#39CCCC',
            maxColor: '#0074D9',
            headerHeight: 30,
            fontColor: 'white',
            height: 200,
            maxDepth:1,
            generateTooltip: showFullTooltip
        });
    };

    function showFullTooltip(row, size, value) {
        return '<div style="background:#fd9; padding:10px; border-style:solid">' +
            '<span style="font-family:Courier"><b>'+
            'Estados por Tipo Cliente: '+$estadosxtipo.getValue(row, 2)+'% '+
            '</b></span> </div>';
    }

    $("#meses").change(function(){
        var combomeses = document.getElementById("meses");
        var mes = combomeses.options[combomeses.selectedIndex].value;
        var combosucursal = document.getElementById("id_sucursal");
        var sucursal = combosucursal.options[combosucursal.selectedIndex].value;
        $(location).attr('href','/ventas/'+mes+'/'+sucursal);
    });

    $("#id_sucursal").change(function(){
        var combomeses = document.getElementById("meses");
        var mes = combomeses.options[combomeses.selectedIndex].value;
        var combosucursal = document.getElementById("id_sucursal");
        var sucursal = combosucursal.options[combosucursal.selectedIndex].value;
        $(location).attr('href','/ventas/'+mes+'/'+sucursal);
    });

    $(window).on('load', function(event){
//        var combomeses = document.getElementById("meses");
//        var mes = combomeses.options[combomeses.selectedIndex].value;
//        $.ajax('/ventas/getdatadashboards/'+mes, {
//            type: 'GET',
//            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
//            success: function(data) {
//                $('.promedio3m').html('$ '+data.promedio3m.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,'));
//                $('.ctactetotal').html('$ '+data.ctactetotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,'));
//                $('.diascalle').html(data.diascalle.toFixed(0));
//                $('.totalvencido').html('$ '+data.totalvencido.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,'));
//                $('.totalsvencer').html('$ '+data.totalsvencer.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$&,'));
//
//            }
//        });
    });
</script>
@stop