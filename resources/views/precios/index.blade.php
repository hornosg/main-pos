@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
@include('layouts.preciosnavbar')
@stop

@section('content')
    <div id="content" class="container-fluid row">
        <div class="index-links col-md-6">
            <div class="hidden-xs link-box bg-orange-active"><a href="/precios/actualizacion/create/1"><span class="info-box-icon bg-orange-active"><i class="fa fa-upload"></i></span><h4 class="text-center text-white text-bold">Importar Lista</h4></a></div>
            <div class="hidden-xs link-box bg-orange-active"><a href="/precios/actualizacion/create/2"><span class="info-box-icon bg-orange-active"><i class="fa fa-umbrella" style="padding: 0 50% !important;"></i></span><h4 class="text-center text-white text-bold">Actualizacion Masiva</h4></a></div>
            <div class="hidden-xs link-box bg-orange-active"><a href="/precios/actualizacion/create/3"><span class="info-box-icon bg-orange-active"><i class="fa fa-sliders"  style="padding: 0 50% !important;"></i></span><h4 class="text-center text-white text-bold">Actualizacion Manual</h4></a></div>
        </div>
        <div class="col-md-8 col-md-4">

        </div>
    </div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<style>
    html body.skin-black-light.sidebar-mini.sidebar-collapse div.wrapper div.content-wrapper{
        margin-left: 0px !important;
        min-height: 555px !important;
    }
    html body.skin-black-light.sidebar-mini.sidebar-collapse div.wrapper footer.main-footer{
        margin-left: 0px !important;
    }
    .small-box h4 {
        font-size: 25px !important;
    }
    .small-box .icon {
        font-size: 80px;
    }

    .info-box{
        min-height: 67px !important;
        margin-bottom: 13px !important;
    }

    .info-box-icon{
        height:67px !important;
        line-height: 67px !important;
        width: 130px !important;
    }
    .info-box-text, .info-box-number{
        display: inline !important;
    }
    .info-box-number {
        font-size: 16px !important;
    }
    .info-box-content{
        padding: 5px 6px
    }
</style>
@stop