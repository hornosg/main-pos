@extends('adminlte::page')

@section('title', $titulo)

@php
    if ($edit==0){
        $editable='readonly';
        $visible='d-none';
        $disabled='disabled';
    }else{
        $editable=' ';
        $visible=' ';
        $disabled=' ';
    }
    if (!empty($descint)){
        $checked = $descint->activo == 1 ? 'checked':'';
    }else{
        $checked = 'checked';
    }
@endphp

@section('content_header')
    @include('layouts.preciosnavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary pt-20">
            <div class="box-header with-border">
                <h3 class="box-title"><strong>{{$tituloform}}</strong></h3>
            </div>
            <form id="form1" role="form" method="POST" action="/{{$urlkey}}/store/3">
                {{ csrf_field() }}
                <div class="box-body">
                    <input type ="hidden" name="id_tipoactualizacion" value="3">
                    <div class="form-group col-md-2">
                        <label for="idtitular" class="control-label">Proveedor</label>
                        <select id="idtitular" name="idtitular" class="form-control text-uppercase" {{$disabled}}>
                        <option value=""></option>
                        @foreach(DB::table('titulares')->where('id_tipotitular', 3)->where('activo', 1)->get() as $tipotitular)
                        <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="id_marca" class="control-label" for="marcas">Marca</label>
                        <select id="id_marca" name="id_marca"  type="button" class="form-control">
                            <option value=""></option>
                            @foreach(DB::table('conf_marcas')->where('activo', 1)->get() as $marca)
                            <option value="{{$marca->id}}">{{$marca->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="id_rubro" class="control-label" for="rubros">Rubro</label>
                        <select id="id_rubro" name="id_rubro"  type="button" class="form-control">
                            <option value=""></option>
                            @foreach(DB::table('conf_rubros')->where('activo', 1)->get() as $rubro)
                            <option value="{{$rubro->id}}">{{$rubro->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="id_subrubro" class="control-label" for="subrubros">Subrubro</label><div id="loading" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>
                        <select id="id_subrubro" name="id_subrubro"  type="button" class="form-control" disabled>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="id_moneda" class="control-label" for="rubros">Moneda</label>
                        <select id="id_moneda" name="id_moneda"  type="button" class="form-control">
                            <option value=""></option>
                            @foreach(DB::table('sys_monedas')->where('activo', 1)->get() as $moneda)
                            <option value="{{$moneda->id}}">{{$moneda->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="pull-right mt-20">
                        <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                        <button id="buscar" type="button" class='btn btn-primary' onclick='BuscarItems()' data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>"> <i class="fa fa-btn fa-search text-white"></i> Buscar</button>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div id="contenttable" class="row">
                        <div class="col-xs-12">
                            <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                                   data-toggle='table'
                                   data-toolbar="#toolbar"
                                   data-search='true'
                                   data-strict-search="false"
                                   data-multiple-search='true'
                                   data-pagination='false'
                                   data-page-size='16'
                                   data-unique-id='id'>
                                <thead class='thead-inverse'>
                                <th data-field='id' class='hidden'></th>
                                <th data-field='codproducto' class='text-uppercase text-bold' data-editable="true">Cod.Prod.Prov</th>
                                <th data-field='producto' class='text-uppercase text-bold'>Producto</th>
                                <th data-field='moneda' class='text-uppercase text-bold'>Moneda</th>
                                <th data-field='precioLista' data-align='right' class='text-uppercase' data-editable="true">precioLista</th>
                                <th data-field='bonifProv' data-align='right' class='text-uppercase' data-editable="true">bonifProv</th>
                                <th data-field='divisor' data-align='right' class='text-uppercase' data-editable="true">Divisor</th>
                                <th data-field='iva' data-align='right' class='text-uppercase'>iva</th>
                                <th data-field='precioCosto' data-align='right' class='text-uppercase'>precioCosto</th>
                                <th data-field='ganancia' data-align='right' class='text-uppercase'data-editable="true">ganancia</th>
                                <th data-field='precio' data-align='right' class='text-uppercase'>precio</th>
                                <th data-field='status' class='hidden'></th>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="pull-right pr-5">
                        <button type="submit" name="action" value="save" class="btn btn-primary">
                            <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/css/bootstrap-editable.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<style>
    #loading{
        display: inline !important;
    }
</style>
@stop

@section('js')
<script src="/js/bootstrap-editable.min.js"></script>
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script src="/js/bootstrap-table-editable.js"></script>
<script src="/vendor/select2/js/select2.min.js"></script>
<script type='text/javascript'>
    $('#idtitular').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_marca').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_rubro').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_subrubro').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_moneda').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_linea').select2({
        placeholder: "",
        allowClear: true
    });
    $('#signo').select2({
        placeholder: "",
        allowClear: true
    });


    $("#id_rubro").change(function(){
        $("#id_subrubro").prop("disabled", true);
        $("#loading").button('loading');
        $("#loading").removeClass('hidden');
        $.ajax('/data/getsubrubros_rubro/'+$(this).val(), {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_subrubro').empty();
                if (data.length==1){
                    $("#id_subrubro").append(
                        '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                    );
                    $("#id_subrubro").prop("disabled", true);
                    $("#id_subrubro").val(data[0].id).trigger('change');
                }else{
                    $("#id_subrubro").append(
                        '<option value=""></option>'
                    );
                    $.each(data, function(index) {
                        $("#id_subrubro").append(
                            '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                        );
                    });
                    $("#id_subrubro").prop("disabled", false);
                }
                $("#loading").button('reset');
                $("#loading").addClass('hidden');
            }
        });
    });

    $("#buscar").click(function(){
        $(this).button('loading');
        $('#table1').bootstrapTable('showLoading');
    });

    $('#table1').bootstrapTable();
    function BuscarItems(){
        $.ajax('/precios/actualizacion/buscarproductos', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: $('#form1').serialize(),
            success: function(data) {
                scroll_to_anchor('contenttable');
                $('#table1').bootstrapTable('removeAll');
                $.each(data, function(index, value) {
                    var newreg = '';
                    newreg+= '"id":"'+ data[index].id + '",';
                    newreg+= '"codproducto":"'+ data[index].codproducto +'",';
                    newreg+= '"producto":"'+ data[index].producto +'",';
                    newreg+= '"moneda":"'+ data[index].moneda +'",';
                    newreg+= '"precioLista":"'+ data[index].precioLista +'",';
                    newreg+= '"bonifProv":"'+ data[index].bonifProv +'",';
                    newreg+= '"iva":"'+ data[index].iva +'",';
                    newreg+= '"divisor":"'+ data[index].divisor +'",';
                    newreg+= '"precioCosto":"'+ data[index].precioCosto +'",';
                    newreg+= '"ganancia":"'+ data[index].ganancia +'",';
                    newreg+= '"precio":"'+ data[index].precio +'",';
                    newreg+= '"status":0';
                    var newregarray=JSON.parse('{'+newreg+'}');
                    $('#table1').bootstrapTable('append', newregarray);
                });
                $("#buscar").button('reset');
                $("#buscar").dequeue();
                $("#buscar").prop("disabled", false);

                var combotitular = document.getElementById("idtitular");
                var idtitular = combotitular.options[combotitular.selectedIndex].value;
                if (idtitular > 0 ){
                    $('#table1 > tbody > tr > td:nth-child(2) > a').editable('enable');
                }else{
                    $('#table1 > tbody > tr > td:nth-child(2) > a').editable('disable');
                }

                $('#table1').bootstrapTable('hideLoading');
            },
            error:function(data) {
                console.log(data);
                iziToast.error({
                    timeout: 2000,
                    position:'center',
                    title: 'Error:',
                    message: data.responseJSON.message
                });
                $("#buscar").button('reset');
                $("#buscar").dequeue();
                $("#buscar").prop("disabled", false);
                $('#table1').bootstrapTable('hideLoading');
            }
        });
    };

    $('#table1').on('editable-save.bs.table', function (e, field, row, oldValue) {
        row.status=1;
        switch(field) {
            case 'precioLista':
                if (row.moneda=='DOLAR'){
                    pl=row.precioLista*dolar;
                }else{
                    pl=row.precioLista
                }
                if(Number(row.bonifProv) > 0 ){
                    row.precioCosto = pl - ((pl * row.bonifProv)/100) + ((pl-((pl * row.bonifProv)/100))*row.iva/100);
                    if(Number(row.divisor) > 1 ){
                        row.precioCosto = row.precioCosto / row.divisor;
                    }
                    row.precio = row.precioCosto + ((row.precioCosto*row.ganancia)/100);
                    row.precioCosto=Number(row.precioCosto).toFixed(2);
                    row.precio=Number(row.precio).toFixed(2);
                }else{
                    row.precioCosto = Number(pl) + Number(pl*row.iva/100);
                    if(Number(row.divisor) > 1 ){
                        row.precioCosto = row.precioCosto / row.divisor;
                    }
                    row.precio = row.precioCosto + ((row.precioCosto*row.ganancia)/100);
                    row.precioCosto=Number(row.precioCosto).toFixed(2);
                    row.precio=Number(row.precio).toFixed(2);
                }
                break;
            case 'bonifProv':
                if (row.moneda=='DOLAR'){
                    pl=row.precioLista*dolar;
                }else{
                    pl=row.precioLista
                }
                if(Number(row.bonifProv) > 0 ){
                    row.precioCosto = Number(pl - ((pl * row.bonifProv)/100) + ((pl-((pl * row.bonifProv)/100))*row.iva/100));
                    if(Number(row.divisor) > 1 ){
                        row.precioCosto = row.precioCosto / row.divisor;
                    }
                    row.precio = Number(row.precioCosto + ((row.precioCosto*row.ganancia)/100));
                    row.precioCosto=Number(row.precioCosto).toFixed(2);
                    row.precio=Number(row.precio).toFixed(2);
                }else{
                    row.precioCosto = Number(pl) + Number(pl*row.iva/100);
                    if(Number(row.divisor) > 1 ){
                        row.precioCosto = row.precioCosto / row.divisor;
                    }
                    row.precio = Number(row.precioCosto + ((row.precioCosto*row.ganancia)/100));
                    row.precioCosto=Number(row.precioCosto).toFixed(2);
                    row.precio=Number(row.precio).toFixed(2);
                }
                break;
            case 'divisor':
                if (row.moneda=='DOLAR'){
                    pl=row.precioLista*dolar;
                }else{
                    pl=row.precioLista
                }
                if(Number(row.bonifProv) > 0 ){
                    row.precioCosto = Number(pl - ((pl * row.bonifProv)/100) + ((pl-((pl * row.bonifProv)/100))*row.iva/100));
                    if(Number(row.divisor) > 1 ){
                        row.precioCosto = row.precioCosto / row.divisor;
                    }
                    row.precio = Number(row.precioCosto + ((row.precioCosto*row.ganancia)/100));
                    row.precioCosto=Number(row.precioCosto).toFixed(2);
                    row.precio=Number(row.precio).toFixed(2);
                }else{
                    row.precioCosto = Number(pl) + Number(pl*row.iva/100);
                    if(Number(row.divisor) > 1 ){
                        row.precioCosto = row.precioCosto / row.divisor;
                    }
                    row.precio = Number(row.precioCosto + ((row.precioCosto*row.ganancia)/100));
                    row.precioCosto=Number(row.precioCosto).toFixed(2);
                    row.precio=Number(row.precio).toFixed(2);
                }
                break;
            case 'ganancia':
                if(Number(row.ganancia) > 0 ){
                    row.precio=Number(row.precioCosto) + ((Number(row.precioCosto)*Number(row.ganancia))/100);
                    row.precio=row.precio.toFixed(2);
                }else{
                    row.precio = Number(row.precioCosto);
                    row.precio=row.precio.toFixed(2);
                }
        }
        $('#table1').bootstrapTable('updateByUniqueId', {id: row.id, row: row});
        $('#table1').bootstrapTable('resetView');
    });

    $('#form1').submit(function (event) {
        var params = [
            {
                name: "items",
                value: JSON.stringify($('#table1').bootstrapTable('getData'))
            }
        ];
        $(this).append($.map(params, function (param) {
            return   $('<input>', {
                type: 'hidden',
                name: param.name,
                value: param.value
            })
        }))
    });

    function scroll_to_anchor(anchor_id){
        var tag = $("#"+anchor_id+"");
        $('html,body').animate({scrollTop: tag.offset().top},'slow');
    }
</script>
@include('utils.statusnotification')
@stop
