@extends('adminlte::page')

@section('title', $titulo)

@php
    if ($edit==0){
        $editable='readonly';
        $visible='d-none';
        $disabled='disabled';
    }else{
        $editable=' ';
        $visible=' ';
        $disabled=' ';
    }
    if (!empty($descint)){
        $checked = $descint->activo == 1 ? 'checked':'';
    }else{
        $checked = 'checked';
    }
@endphp

@section('content_header')
    @include('layouts.preciosnavbar')
@stop

@section('content')
<div id="content" class="container-fluid row">
    <form id="form1" role="form" method="POST" enctype="multipart/form-data" action="/{{$urlkey}}/store/1">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary  pt-20">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>{{$tituloform}}</strong></h3>
                </div>
                <div class="box-body">
                    {{ csrf_field() }}
                    <input type ="hidden" name="id_tipoactualizacion" value="1">
                    <div class="form-group col-md-4">
                        <label for="id_titular" class="control-label">Proveedor</label>
                        <select id="id_titular" name="id_titular" class="form-control text-uppercase" {{$disabled}}>
                        <option value=""></option>
                        @foreach(DB::table('titulares')->where('id_tipotitular', 3)->where('activo', 1)->get() as $tipotitular)
                        <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="import_file">Lista de Precios en Excel</label>
                            <input type="file" name="import_file" class="form-control-file"/>
                        </div>
                    </div>

                    <div class="clearfix visible-lg-block visible-md-block"></div>

                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                        <button type="submit" id="action" name="action" value="save" class="btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> ...">
                            <i class="fa fa-btn fa-gears text-white"></i> Actualizar
                        </button>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Formato Esperado</label>
                            <img src="/img/FormatoListaPrecios.PNG" alt="...">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script type='text/javascript'>
    $('#id_titular').select2({
        placeholder: "",
        allowClear: true
    });
    $('#form1').submit(function (event) {
        $("#action").button('loading');
    });
</script>
@include('utils.statusnotification')
@stop
