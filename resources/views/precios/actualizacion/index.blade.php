@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
    @include('layouts.preciosnavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <a href="/{{$urlkey}}/create/1" class="btn btn-primary">
                                    <i class="fa fa-btn fa-upload"></i> Importar Lista
                                </a>
                                <a href="/{{$urlkey}}/create/2" class="btn btn-primary">
                                    <i class="fa fa-btn fa-umbrella"></i> Actualizacion Masiva
                                </a>
                                <a href="/{{$urlkey}}/create/3" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sliders"></i> Actualizacion Manual
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='false'
                               data-pagination='true'
                               data-page-size=15
                               data-sort-name='id'
                               data-sort-order='desc'
                               data-detail-view='true'
                               data-unique-id='id'>
                            <thead class='thead-inverse'>
                                <th data-field='id' class='hidden'>Id</th>
                                <th data-field='id_tipo' data-formatter="tipoFormatter" class='text-uppercase text-bold'>Tipo</th>
                                <th data-field='titular' class='text-uppercase'>Titular</th>
                                <th data-field='marca' class='text-uppercase'>Marca</th>
                                <th data-field='rubro' class='text-uppercase'>Rubro</th>
                                <th data-field='subrubro' class='text-uppercase'>Subrubro</th>
                                <th data-field='linea' class='text-uppercase'>Linea</th>
                                <th data-field='material' class='text-uppercase'>Material</th>
                                <th data-field='signo' data-formatter="signoFormatter" class='text-uppercase text-bold'>Operacion</th>
                                <th data-field='archivo'>FILE</th>
                                <th data-field='usuario' data-align='left' class='text-uppercase'>Usuario</th>
                                <th data-field='creado' data-align='right'  class='text-uppercase'>Creado</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
@stop

@section('js')
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script language='JavaScript' type='text/javascript'>
    var datos = <?php echo json_encode($items); ?>;
    var $table = $('#table1');
    $table.bootstrapTable({data: datos,exportDataType:'all',exportOptions:{fileName: 'actualizaciones'}});

    function tipoFormatter(value, row, index) {
        if(row.id_tipo===1){
            return 'IMPORTACION';
        }
        if(row.id_tipo===2){
            return 'ACT.MASIVA';
        }
        if(row.id_tipo===3){
            return 'ACT.MANUAL';
        }
        if(row.id_tipo===4){
            return 'EN PRODUCTO';
        }
    };

    function signoFormatter(value, row, index) {
        if(row.signo>0){
            return 'SUMAR';
        }
        if(row.signo<0){
            return 'RESTAR';
        }
    };

    $table.on('expand-row.bs.table', function (e, index, row, $detail) {
        $idtabla2="tabla"+row.id;
        html='<div class="box"><table id='+$idtabla2+' class="table table-sm table-bordered table-hover table-striped table-condensed table-responsive" ' +
            'data-search="true" data-strict-search="false" data-multiple-search="true" data-show-export="true" data-pagination="true">' +
            '<thead class="thead-inverse-gray"><tr>' +
            '<th data-field="id_item" data-align="right">Id</th>' +
            '<th data-field="codproducto" data-align="right">Cod.Prod.Prov</th>' +
            '<th data-field="descripcion">Descripcion</th>' +
            '<th data-field="oldprecioLista" data-align="right">P.Lista Ant.</th>' +
            '<th data-field="newprecioLista" data-align="right">P.Lista Post.</th>' +
            '<th data-field="oldbonifProv" data-align="right">Bonif.Prov. Ant.</th>' +
            '<th data-field="newbonifProv" data-align="right">Bonif.Prov. Post.</th>' +
            '<th data-field="oldganancia" data-align="right">Ganancia Ant.</th>' +
            '<th data-field="newganancia" data-align="right">Ganancia Post.</th>' +
            '<th data-field="precioCosto" data-align="right">Precio Costo Actual</th>' +
            '<th data-field="precio" data-align="right">Precio Actual</th>' +
            '</tr></thead>' +
            '</table></divS>';
        $detail.html(html);
        $('#'+$idtabla2).bootstrapTable();
        $('#'+$idtabla2).bootstrapTable('showLoading');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'/precios/actualizacion/detalle/'+row.id,
            data: row.id,
            success: function(datosdetalle) {
                $('#'+$idtabla2).bootstrapTable('append', datosdetalle);
                $('#'+$idtabla2).bootstrapTable('hideLoading');
            }
        });
    });
</script>
@include('utils.statusnotification')
@stop
