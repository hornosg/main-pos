@extends('adminlte::page')

@section('title', $titulo)

@php
    if ($edit==0){
        $editable='readonly';
        $visible='d-none';
        $disabled='disabled';
    }else{
        $editable=' ';
        $visible=' ';
        $disabled=' ';
    }
    if (!empty($descint)){
        $checked = $descint->activo == 1 ? 'checked':'';
    }else{
        $checked = 'checked';
    }
@endphp

@section('content_header')
    @include('layouts.preciosnavbar')
@stop

@section('content')
<div id="content" class="container-fluid row">
    <form id="form1" role="form" method="POST" action="/{{$urlkey}}/store/2">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-primary pt-20">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>{{$tituloform}}</strong></h3>
                </div>
                <div class="box-body">
                    {{ csrf_field() }}
                    <input type ="hidden" name="id_tipoactualizacion" value="2">
                    <div class="form-group col-md-4">
                        <label for="id_titular" class="control-label">Proveedor</label>
                        <select id="id_titular" name="id_titular" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @foreach(DB::table('titulares')->where('id_tipotitular', 3)->where('activo', 1)->get() as $tipotitular)
                                <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="id_marca" class="control-label" for="marcas">Marca</label>
                        <select id="id_marca" name="id_marca"  type="button" class="form-control">
                            <option value=""></option>
                            @foreach(DB::table('conf_marcas')->where('activo', 1)->get() as $marca)
                                <option value="{{$marca->id}}">{{$marca->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="id_rubro" class="control-label" for="rubros">Rubro</label>
                        <select id="id_rubro" name="id_rubro"  type="button" class="form-control">
                            <option value=""></option>
                            @foreach(DB::table('conf_rubros')->where('activo', 1)->get() as $rubro)
                                <option value="{{$rubro->id}}">{{$rubro->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix visible-lg-block visible-md-block"></div>

                    <div class="form-group col-md-4">
                        <label for="id_subrubro" class="control-label" for="subrubros">Subrubro</label><div id="loading" class="hidden" data-loading-text=" ... <i class='fa fa-circle-o-notch fa-spin'></i> cargando"></div>
                        <select id="id_subrubro" name="id_subrubro"  type="button" class="form-control" disabled>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="id_linea" class="control-label" for="lineaes">Linea</label>
                        <select id="id_linea" name="id_linea"  type="button" class="form-control">
                            <option value=""></option>
                            @foreach(DB::table('conf_lineas')->where('activo', 1)->get() as $linea)
                                <option value="{{$linea->id}}">{{$linea->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="id_material" class="control-label" for="materiales">Material</label>
                        <select id="id_material" name="id_material"  type="button" class="form-control">
                            <option value=""></option>
                            @foreach(DB::table('conf_materiales')->where('activo', 1)->get() as $material)
                                <option value="{{$material->id}}">{{$material->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix visible-lg-block visible-md-block"></div>

                    <div class="form-group col-md-4">
                        <label for="porcentaje_preciolista" class="control-label">% Sobre Precio Lista</label>
                        <input id="porcentaje_preciolista" type="number" class="form-control input-sm" name="porcentaje_preciolista" value=""  {{$editable}}>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="signo" class="control-label">Operacion</label>
                        <select id="signo" name="signo" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            <option value="1">SUMAR</option>
                            <option value="-1">RESTAR</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="porcentaje_ganancia" class="control-label">% Ganancia</label>
                        <input id="porcentaje_ganancia" type="number" class="form-control input-sm" name="porcentaje_ganancia" value=""  {{$editable}}>
                    </div>
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                        <button type="submit" name="action" value="save" class="btn btn-primary">
                            <i class="fa fa-btn fa-gears text-white"></i> Actualizar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
<style>
    #loading{
        display: inline !important;
    }
</style>
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script type='text/javascript'>
    $('#id_titular').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_marca').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_rubro').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_subrubro').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_material').select2({
        placeholder: "",
        allowClear: true
    });
    $('#id_linea').select2({
        placeholder: "",
        allowClear: true
    });
    $('#signo').select2({
        placeholder: "",
        allowClear: true
    });

    $("#id_rubro").change(function(){
        $("#id_subrubro").prop("disabled", true);
        $("#loading").button('loading');
        $("#loading").removeClass('hidden');
        $.ajax('/data/getsubrubros_rubro/'+$(this).val(), {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#id_subrubro').empty();
                if (data.length==1){
                    $("#id_subrubro").append(
                        '<option value="' + data[0].id + '">' + data[0].descripcion + '</option>'
                    );
                    $("#id_subrubro").prop("disabled", true);
                    $("#id_subrubro").val(data[0].id).trigger('change');
                }else{
                    $("#id_subrubro").append(
                        '<option value=""></option>'
                    );
                    $.each(data, function(index) {
                        $("#id_subrubro").append(
                            '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                        );
                    });
                    $("#id_subrubro").prop("disabled", false);
                }
                $("#loading").button('reset');
                $("#loading").addClass('hidden');
            }
        });
    });
</script>
@include('utils.statusnotification')
@stop
