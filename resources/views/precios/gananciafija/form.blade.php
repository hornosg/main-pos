@extends('adminlte::page')

@section('title', $titulo)

@php
    if ($edit==0){
        $editable='readonly';
        $visible='d-none';
        $disabled='disabled';
    }else{
        $editable=' ';
        $visible=' ';
        $disabled=' ';
    }
    if (!empty($item)){
        $checked = $item->activo == 1 ? 'checked':'';
    }else{
        $checked = 'checked';
    }
@endphp

@section('content_header')
    @include('layouts.preciosnavbar')
@stop

@section('content')
<div id="content" class="container-fluid row">
    <form id="form1" role="form" method="POST" action="/{{$urlkey}}/store">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-primary">
                {{ csrf_field() }}
                <div class="box-body">
                    @if (!empty($item))
                        <input type = "hidden" name = "id" value = "{{$item->id}}">
                    @else
                        <input type = "hidden" name = "id">
                    @endif
                    <div class="form-group {{ $errors->has('id_tipo') ? ' has-error' : '' }} col-md-3">
                        <label for="id_tipo" class="control-label">Tipo de Cliente</label>
                        <select id="id_tipo" name="id_tipo" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @if (!empty($item))
                                @foreach(DB::table('sys_tipoclientes as tc')
                                                ->where('id', $item->id_tipocliente)
                                                ->get() as $tiposcliente)
                                    @if ( $item->id_tipocliente == $tiposcliente->id )
                                        <option value="{{ $tiposcliente->id }}" selected>{{ $tiposcliente->descripcion }}</option>
                                    @endif
                                @endforeach
                            @else
                                @foreach(DB::select('select *
                                                       from sys_tipoclientes as tc
                                                      where tc.activo = 1
                                                        and tc.id not in (select distinct id_tipocliente from conf_ganancia_fija where activo=1)') as $tiposcliente)
                                    <option value="{{$tiposcliente->id}}">{{$tiposcliente->descripcion}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group {{ $errors->has('porcentaje') ? 'has-error' : '' }} col-xs-12 col-sm-12 col-md-3 col-md-3">
                        <label for="porcentaje" class="control-label">Porcentaje</label>
                        @if (!empty($item))
                            <input id="porcentaje" type="number" class="form-control input-sm" name="porcentaje" value="{{ $item->porcentaje }}"  {{$editable}}>
                        @else
                            <input id="porcentaje" type="number" class="form-control input-sm" name="porcentaje" value=""  {{$editable}}>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('activo') ? ' has-error' : '' }} col-xs-12 col-sm-12 col-md-1 col-md-1 pull-left">
                        <label for="activo" class="control-label">Activo</label>
                        <input id="activo" class="form-control input-sm" type="checkbox" data-toggle="toggle" data-on="true" data-off="false" name="activo" {{$checked}} {{$disabled}}>
                    </div>
                    <div class="clearfix visible-lg-block visible-md-block"></div>
                    @if (!empty($item))
                        <div class="form-group col-sm-6 col-md-4 col-md-3">
                            <label for="created_at" class="control-label">Fecha Alta</label>
                            <input id="created_at" type="text" class="form-control input-sm text-uppercase" name="created_at" readonly value="{{ $item->created_at }}">
                        </div>

                        <div class="form-group col-sm-6 col-md-6 col-md-3">
                            <label for="updated_at" class="control-label">Fecha Ultima Edicion</label>
                            <input id="updated_at" type="text" class="form-control input-sm text-uppercase" name="updated_at" readonly value="{{ $item->updated_at }}">
                        </div>
                    @endif
                </div>
                <div class="box-footer pb-10 no-bt">
                    <div class="pull-right">
                        @if ($edit==1)
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Cancelar</a>
                            <button type="submit" name="action" value="save" class="btn btn-primary">
                                <i class="fa fa-btn fa-floppy-o text-white"></i> Guardar
                            </button>
                        @else
                            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-btn fa-arrow-left text-white"></i> Volver</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop

@section('css')
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/vendor/select2/css/select2.min.css" rel="stylesheet">
@stop

@section('js')
<script src="/vendor/select2/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type='text/javascript'>
    $(function () {
        $('[data-toggle="popover"]').popover();
    });
    $('#id_tipo').select2({
        placeholder: "",
        allowClear: true
    });
    $('#form1').submit(function (event) {
        $("#id_tipo").prop("disabled", false);
    });
</script>
@include('utils.statusnotification')
@stop
