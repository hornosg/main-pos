@extends('adminlte::page')

@section('title', $titulo)

@section('content_header')
@include('layouts.preciosnavbar')
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div id="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="toolbar">
                            <div class="form-inline" role="form">
                                <a href="/{{$urlkey}}/create" class="btn btn-primary"  title="Nuevo">
                                    <i class="fa fa-btn fa-plus text-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <table id='table1' class='table table-sm table-bordered table-hover table-striped table-condensed table-responsive'
                               data-toggle='table'
                               data-toolbar="#toolbar"
                               data-search='true'
                               data-strict-search="false"
                               data-multiple-search='true'
                               data-show-export='true'
                               data-pagination='true'
                               data-page-size=15
                               data-pagination-first-text='Primera'
                               data-pagination-pre-text='Ant.'
                               data-pagination-next-text='Sig.'
                               data-pagination-last-text='Ultima'
                               data-sort-name='id'
                               data-sort-order='desc'>

                            <thead class='thead-inverse'>
                            <th data-field='id' data-align='right' class='text-uppercase'>Id</th>
                            <th data-field='creado'   class='text-uppercase'>CREADO</th>
                            <th data-field='descripcion'   class='text-uppercase'>DESCRIPCION</th>
                            <th data-field='nota'   class='text-uppercase'>NOTA</th>
                            <th data-field='porcentaje'  data-align='right' data-formatter="porcFormatter" class='text-uppercase'>PORCENTAJE</th>
                            <th data-field='activo' data-align='center' class='text-uppercase'>ACTIVO</th>
                            <th data-align='center' data-formatter='FunctionsFormatter'></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@php
foreach ($items as $item) {
    $item->activo = $item->activo == 1 ? 'si':'no';
}
@endphp
@stop

@section('css')
<!--<link rel="stylesheet" href="/css/admin_custom.css">-->
<link href="/css/mainpos.css" rel="stylesheet">
<link href="/css/bootstrap-table.css" rel="stylesheet">
@stop

@section('js')
<script src="/js/bootstrap-table.js"></script>
<script src="/js/bootstrap-table-multiple-search.js"></script>
<script src="/js/bootstrap-table-export.js"></script>
<script src="/js/tableExport.js"></script>
<script src="/js/bootstrap-table-es-AR.js"></script>
<script language='JavaScript' type='text/javascript'>
    var datos = <?php echo json_encode($items); ?>;
    $('#table1').bootstrapTable({data: datos,exportDataType:'all',exportOptions:{fileName: 'ajustesdeprecio'}});

    function porcFormatter(value, row, index) {
        return  value+" %";
    };

    function FunctionsFormatter(value, row, index) {
        return "<a class='btn-outline-warning' href='/{{$urlkey}}/edit/"+row.id+"'><i class='fa fa-fw fa-pencil'></i></a>" +
               "<a class='btn-outline-primary' href='/{{$urlkey}}/show/"+row.id+"'><i class='fa fa-fw fa-search'></i></a>";
    };
</script>
@include('utils.statusnotification')
@stop

