<?php

return [
    'help.tipocliente' => 'Clasificacion de Clientes, que permite agruparlos para definir diferentes reglas de precio por cada grupo.',
    'help.tipoiva' => 'Permite determinar el tipo de factura a realizar (A,B o C según Corresponda).'
];
