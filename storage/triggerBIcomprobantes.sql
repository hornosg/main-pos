DELIMITER |
CREATE TRIGGER getnumber BEFORE INSERT ON comprobantes FOR EACH ROW 
BEGIN
	DECLARE sucursal integer;
    SET sucursal := (SELECT id_sucursal FROM users WHERE id=NEW.created_us);
    SET NEW.NUMERO := (SELECT MAX(numero) FROM numeradores WHERE serie=NEW.serie AND id_tipocpte=NEW.id_tipocpte AND id_sucursal=sucursal);
END
|

