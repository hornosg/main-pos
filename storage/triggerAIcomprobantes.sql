DELIMITER |
CREATE TRIGGER setnumber AFTER INSERT ON comprobantes FOR EACH ROW 
BEGIN
	DECLARE sucursal integer;
    SET sucursal := (SELECT id_sucursal FROM users WHERE id=NEW.created_us);
    UPDATE numeradores SET numero = numero + 1 WHERE serie=NEW.serie AND id_tipocpte=NEW.id_tipocpte AND id_sucursal=sucursal;
END
|